﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNSI.Models
{
    public class BuscarGuiaRequest
    {
        public int? ien_entrada_p { get; set; }
        public string ien_guia_original_p { get; set; }
        public int? ien_con_nconsolidador_p { get; set; }
        public int? ien_aerolinea_p { get; set; }
    }

    public class CONSULTA_GUIAS
    {
        public string ien_entrada { get; set; }
        public string ien_manifiesto { get; set; }
        public string ien_guia { get; set; }
        public string ien_guia_original { get; set; }
        public string ien_con_nconsolidador { get; set; }
        public string ien_guia_master { get; set; }
        public string ien_imp_cedula { get; set; }
        public string ien_importador { get; set; }
        public string ien_estado { get; set; }
        public string ien_fch_digitacion { get; set; }
        public string ien_fch_manifiesto { get; set; }
        public string ien_fch_ingreso { get; set; }
        public string ien_bod_nbodega { get; set; }
        public string BODEGA { get; set; }
        public string ien_fun_funcionario { get; set; }
        public string FUNCIONARIO { get; set; }
        public string ien_aer_naerolinea { get; set; }
        public string AEROLINEA { get; set; }
        public string ien_pto_puerto { get; set; }
        public string COD_PUERTO { get; set; }
        public string PUERTO { get; set; }
        public string ien_tdo_doc_hija { get; set; }
        public string DOC_HIJO { get; set; }
        public string ien_din_tipo_documento { get; set; }
        public string DESDOC { get; set; }
        public string ien_aduana_recibe { get; set; }
        public string ADUANA_RECIBE { get; set; }
        public string ien_usuario_digito { get; set; }
        public string NOMUSUARIODIGITO { get; set; }
        public string ien_bultos_manifestados { get; set; }
        public string ien_bultos_realingreso { get; set; }
        public string BULTOS_DESPALETIZADOS { get; set; }
        public string BULTOS_RESERVADOS { get; set; }
        public string BULTOS_SALIDOS { get; set; }
        public string ien_kilos_manifestados { get; set; }
        public string MUESTRAS { get; set; }
        public string KILOS_DESPALETIZADOS { get; set; }
        public string KILOS_RESERVADOS { get; set; }
        public string KILOS_SALIDOS { get; set; }
        public string ien_descripcion_manif { get; set; }
        public string ien_antecesor { get; set; }
        public string ien_numero_viaje { get; set; }
        public string ien_observaciones { get; set; }
        public string ien_clase { get; set; }
        public string ien_boleta_clase { get; set; }
        public string ien_ubicacion_transito { get; set; }
        public string UBI_TRANSITO { get; set; }
        public string ien_tipo_duda_ref { get; set; }
        public string INDICACION_DESC { get; set; }
        public string ien_duda_refri { get; set; }
        public string DES_DUDA_REFRI { get; set; }
        public string ien_bloqueada { get; set; }
        public string ien_es_courier { get; set; }
    }

    public class Guias_Consulta
    {
        public string IEN_MANIFIESTO { get; set; }
        public string IEN_TMA_ID_MANIFIESTO { get; set; }
        public string IEN_GUIA { get; set; }
        public string IEN_GUIA_ORIGINAL { get; set; }
        public string IEN_ENTRADA { get; set; }
        public string IEN_FCH_MANIFIESTO { get; set; }
        public string IEN_FCH_INGRESO { get; set; }
        public string IEN_FCH_DIGITACION { get; set; }
        public string IEN_GUIA_MASTER { get; set; }
        public string IEN_DUA { get; set; }
        public string IEN_DUA_Z { get; set; }
        public string IEN_DUA_R { get; set; }
        public string IEN_AER_NAEROLINEA { get; set; }
        public string AEROLINEA { get; set; }
        public string IEN_PTO_PUERTO { get; set; }
        public string COD_PUERTO { get; set; }
        public string PUERTO { get; set; }
        public string IEN_TDO_DOC_HIJA { get; set; }
        public string DOC_HIJA { get; set; }
        public string IEN_DIN_TIPO_DOCUMENTO { get; set; }
        public string DESCDOC { get; set; }
        public string IEN_ANTECESOR { get; set; }
        public string IEN_FUN_FUNCIONARIO { get; set; }
        public string DESFUNCIONARIO { get; set; }
        public string IEN_CON_NCONSOLIDADOR { get; set; }
        public string CONSOLIDADOR { get; set; }
        public string IEN_CONSIGNATARIO { get; set; }
        public string IEN_ES_COURIER { get; set; }
        public string IEN_ES_FARMA { get; set; }
        public string IEN_IMP_CEDULA { get; set; }
        public string IEN_IMPORTADOR { get; set; }
        public string IEN_BULTOS_MANIFESTADOS { get; set; }
        public string IEN_KILOS_MANIFESTADOS { get; set; }
        public string IEN_VOLUMEN_MANIF { get; set; }
        public string IEN_BULTOS_INGRESADOS { get; set; }
        public string IEN_KILOS_INGRESADOS { get; set; }
        public string IEN_VOLUMEN_INGRESADO { get; set; }
        public string IEN_MODALIDAD_TRANSPORTE { get; set; }
        public string IEN_DESCRIPCION_MANIF { get; set; }
        public string IEN_OBSERVACIONES { get; set; }
        public string IEN_UBICACION_INGRESO { get; set; }
        public string UBICACION_INGRESO { get; set; }
        public string IEN_CLASE { get; set; }
        public string IEN_BOLETA_CLASE { get; set; }
        public string IEN_BOD_NBODEGA { get; set; }
        public string IEN_UBICACION_TRANSITO { get; set; }
        public string UBITRANSITO { get; set; }
        public string IEN_DUDA_REFRI { get; set; }
        public string IEN_TIPO_DUDA_REF { get; set; }
        public string DUDA_REF { get; set; }
        public string IEN_NUMERO_VIAJE { get; set; }
        public string IEN_ACTA_DESCARGA { get; set; }
        public string IEN_TEXTO_ACTA { get; set; }
        public string IEN_SOLICITUD { get; set; }
        public string IEN_PEND_DESCONSOLIDAR { get; set; }
        public string IEN_USU_DIGITO { get; set; }
        public string IEN_ESTADO { get; set; }
        public string BLOQUEO { get; set; }
        public string MUESTRA { get; set; }
        public string TOT_BULTOS { get; set; }
        public string TOT_KILOS { get; set; }
        public string IEN_PUESTO_CHEQUEO { get; set; }
        public string IEN_LINEA_GUIA { get; set; }
        public string TOT_VOLUMEN { get; set; }
        
    }

    public class Guias_Modificar
    {
        public string IEN_MANIFIESTO { get; set; }
        public string IEN_TMA_ID_MANIFIESTO { get; set; }
        public string IEN_GUIA { get; set; }
        public string IEN_GUIA_ORIGINAL { get; set; }
        public string IEN_ENTRADA { get; set; }
        public string IEN_FCH_MANIFIESTO { get; set; }
        public string IEN_FCH_INGRESO { get; set; }
        public string IEN_FCH_DIGITACION { get; set; }
        public string IEN_GUIA_MASTER { get; set; }
        public string IEN_DUA { get; set; }
        public string IEN_AER_NAEROLINEA { get; set; }
        public string AEROLINEA { get; set; }
        public string IEN_PTO_PUERTO { get; set; }
        public string COD_PUERTO { get; set; }
        public string PUERTO { get; set; }
        public string IEN_TDO_DOC_HIJA { get; set; }
        public string DOC_HIJA { get; set; }
        public string IEN_DIN_TIPO_DOCUMENTO { get; set; }
        public string DESCDOC { get; set; }
        public string IEN_ANTECESOR { get; set; }
        public string IEN_FUN_FUNCIONARIO { get; set; }
        public string DESFUNCIONARIO { get; set; }
        public string IEN_CON_NCONSOLIDADOR { get; set; }
        public string CONSOLIDADOR { get; set; }
        public string IEN_CONSIGNATARIO { get; set; }
        public string IEN_ES_COURIER { get; set; }
        public string IEN_ES_FARMA { get; set; }
        public string IEN_IMP_CEDULA { get; set; }
        public string IEN_IMPORTADOR { get; set; }
        public string IEN_BULTOS_MANIFESTADOS { get; set; }
        public string IEN_KILOS_MANIFESTADOS { get; set; }
        public string IEN_VOLUMEN_MANIF { get; set; }
        public string IEN_BULTOS_INGRESADOS { get; set; }
        public string IEN_KILOS_INGRESADOS { get; set; }
        public string IEN_VOLUMEN_INGRESADO { get; set; }
        public string IEN_MODALIDAD_TRANSPORTE { get; set; }
        public string IEN_DESCRIPCION_MANIF { get; set; }
        public string IEN_OBSERVACIONES { get; set; }
        public string IEN_UBICACION_INGRESO { get; set; }
        public string UBICACION_INGRESO { get; set; }
        public string IEN_CLASE { get; set; }
        public string IEN_BOLETA_CLASE { get; set; }
        public string IEN_BOD_NBODEGA { get; set; }
        public string IEN_UBICACION_TRANSITO { get; set; }
        public string UBITRANSITO { get; set; }
        public string IEN_DUDA_REFRI { get; set; }
        public string IEN_TIPO_DUDA_REF { get; set; }
        public string DUDA_REF { get; set; }
        public string IEN_NUMERO_VIAJE { get; set; }
        public string IEN_ACTA_DESCARGA { get; set; }
        public string IEN_TEXTO_ACTA { get; set; }
        public string IEN_SOLICITUD { get; set; }
        public string IEN_PEND_DESCONSOLIDAR { get; set; }
        public string IEN_USU_DIGITO { get; set; }
        public string IEN_ESTADO { get; set; }
        public string TOT_BULTOS { get; set; }
        public string TOT_KILOS { get; set; }
        public string IEN_PUESTO_CHEQUEO { get; set; }
        public string MUESTRA { get; set; }
        public string BLOQUEO { get; set; }
        public string IEN_LINEA_GUIA { get; set; }
        public string TOT_VOLUMEN { get; set; }
    }

    public class Guias_Modificar_post
    {
        public string IEN_TMA_ID_MANIFIESTO { get; set; }
        public string IEN_PUESTO_CHEQUEO { get; set; }
        public string IEN_ENTRADA { get; set; }
        public string IEN_GUIA_ORIGINAL { get; set; }
        public string IEN_GUIA { get; set; }
        public string IEN_GUIA_MASTER { get; set; }
        public string IEN_DIN_TIPO_DOCUMENTO { get; set; }
        public string IEN_DUA { get; set; }
        public string IEN_NUMERO_VIAJE { get; set; }
        public string IEN_ANTECESOR { get; set; }
        public string IEN_IMP_CEDULA { get; set; }
        public string CONSOLIDADOR { get; set; }
        public string IEN_CON_NCONSOLIDADOR { get; set; }
        public string IEN_BULTOS_MANIFESTADOS { get; set; }
        public string IEN_KILOS_MANIFESTADOS { get; set; }
        public string IEN_VOLUMEN_MANIF { get; set; }
        public string IEN_BULTOS_INGRESADOS { get; set; }
        public string IEN_KILOS_INGRESADOS { get; set; }
        public string IEN_VOLUMEN_INGRESADO { get; set; }
        public string IEN_DESCRIPCION_MANIF { get; set; }
        public string IEN_OBSERVACIONES { get; set; }
        public string IEN_UBICACION_INGRESO { get; set; }
        public string IEN_UBICACION_TRANSITO { get; set; }
        public string IEN_ES_COURIER { get; set; }
        public string IEN_DUDA_REFRI { get; set; }
        public string IEN_TIPO_DUDA_REF { get; set; }
        public string IEN_ES_FARMA { get; set; }
        public string IEN_CLASE { get; set; }
        public string IEN_BOLETA_CLASE { get; set; }
        public string IEN_ACTA_DESCARGA { get; set; }
        public string IEN_PEND_DESCONSOLIDAR { get; set; }
        public string IEN_SOLICITUD { get; set; }
        public string IEN_TEXTO_ACTA { get; set; }
        public string IEN_BOD_NBODEGA { get; set; }
        public string IEN_USER_MERX { get; set; }
        public string IEN_ESTADO { get; set; }
        public string IEN_MOT_MOTIVO { get; set; }
        public string IEN_SOLICITA_CAMBIO { get; set; }
        public string IEN_RESPONSABLE_ERROR { get; set; }
        public string IEN_DEPARTAMENTO { get; set; }
    }

    public class MercanciasRequest
    {
        public int mer_ien_entrada_p { get; set; }
    }

    public class MERCANCIAS
    {
        public string mer_mov_inventario { get; set; }
        public string mer_id_mercancia { get; set; }
        public string mer_estado { get; set; }
        public string mer_bultos_despaletizados { get; set; }
        public string mer_bultos_reservados { get; set; }
        public string mer_bultos_salidos { get; set; }
        public string KILOS { get; set; }
        public string mer_kilos_reservados { get; set; }
        public string mer_kilos_salidos { get; set; }
        public string BULTOSESC { get; set; }
        public string KILOSESC { get; set; }
        public string mer_tem_embalaje { get; set; }
        public string EMBALAJE { get; set; }
        public string mer_tme_mercancia { get; set; }
        public string TIPO_MERCANCIA { get; set; }
        public string mer_fch_digitacion { get; set; }
        public string mer_ave_averia { get; set; }
        public string AVERIA { get; set; }
        public string mer_imp_cedula { get; set; }
        public string NOMIMPORTADOR { get; set; }
        public string mer_fch_transmision { get; set; }
        public string mer_archivo_transmision { get; set; }
        public string mer_usuario_transmision { get; set; }
        public string mer_error { get; set; }
        public string mer_operacion_tica { get; set; }
        public string DES_OPER_TICA { get; set; }
        public string mer_accion { get; set; }
        public string DES_MER_ACCION { get; set; }
        public string mer_transmision { get; set; }
        public string DES_MER_TRANSMI { get; set; }
        public string mer_id_mercancia_ref { get; set; }
        public string mer_bultos_ref { get; set; }
        public string mer_armas { get; set; }
        public string mer_tip_codigo_iata { get; set; }
        public string mer_pel_peligrosa { get; set; }
        public string PELIGROSIDAD { get; set; }
        public string DESC_PELIG_IATA { get; set; }
        public string mer_codigo_un { get; set; }
        public string mer_indice_trans { get; set; }
        public string mer_descripcion { get; set; }
        public string mer_refrigerado { get; set; }
        public string mer_perecedero { get; set; }
        public string mer_loc_rack { get; set; }
        public string mer_loc_columna { get; set; }
        public string mer_loc_lado { get; set; }
        public string mer_loc_altura { get; set; }
        public string mer_observaciones { get; set; }
        public string mer_ter_error { get; set; }
        public string ERROR_TRANSMISION { get; set; }
    }

    public class MercanciasDetalleRequest
    {
        public int mer_id_mercancia_p { get; set; }
    }

    public class MERCANCIAS_DETALLE
    {
        public string mer_id_mercancia { get; set; }
        public string mer_tem_embalaje { get; set; }
        public string EMBALAJE { get; set; }
        public string mer_tme_mercancia { get; set; }
        public string TIPO_MERCANCIA { get; set; }
        public string mer_fch_digitacion { get; set; }
        public string mer_ave_averia { get; set; }
        public string AVERIA { get; set; }
        public string mer_imp_cedula { get; set; }
        public string NOMIMPORTADOR { get; set; }
        public string mer_fch_transmision { get; set; }
        public string mer_archivo_transmision { get; set; }
        public string mer_usuario_transmision { get; set; }
        public string mer_error { get; set; }
        public string mer_operacion_tica { get; set; }
        public string DES_OPER_TICA { get; set; }
        public string mer_accion { get; set; }
        public string DES_MER_ACCION { get; set; }
        public string mer_transmision { get; set; }
        public string DES_MER_TRANSMI { get; set; }
        public string mer_id_mercancia_ref { get; set; }
        public string mer_bultos_ref { get; set; }
        public string mer_armas { get; set; }
        public string mer_tip_codigo_iata { get; set; }
        public string mer_pel_peligrosa { get; set; }
        public string PELIGROSIDAD { get; set; }
        public string DESC_PELIG_IATA { get; set; }
        public string mer_codigo_un { get; set; }
        public string mer_indice_trans { get; set; }
        public string mer_descripcion { get; set; }
        public string mer_refrigerado { get; set; }
        public string mer_perecedero { get; set; }
        public string mer_loc_rack { get; set; }
        public string mer_loc_columna { get; set; }
        public string mer_loc_lado { get; set; }
        public string mer_loc_altura { get; set; }
        public string mer_observaciones { get; set; }
        public string mer_ter_error { get; set; }
        public string ERROR_TRANSMISION { get; set; }
    }

    public class TX_CONSULTA_GUIAS
    {
    }
}