﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNSI.Models
{
    public class CONCEPTOS_COBRO
    {
        public string CCB_CONCEPTO { get; set; }
        public string CCB_DESCRIPCION { get; set; }
        public string CCB_USU_REGISTRO { get; set; }
        public string CCB_FCH_REGISTRO { get; set; }
    }

    public class CONCEPTOS_COBRO_BUSQUEDA
    {
        public string B_CCB_DESCRIPCION { get; set; }
    }
}