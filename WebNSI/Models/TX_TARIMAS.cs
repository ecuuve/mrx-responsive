﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNSI.Models
{
    public class Tarimas_Consulta
    {
        public string MER_IEN_ENTRADA { get; set; }
        public string MER_MANIFIESTO { get; set; }
        public string MER_GUIA { get; set; }
        public string KILOS_INGRESADOS { get; set; }
        public string BULTOS_INGRESADOS { get; set; }
        public string MER_TME_MERCANCIA { get; set; }
        public string DES_MERCANCIA { get; set; }
        public string MER_MARCAS { get; set; }
        public string MER_TIP_CODIGO_IATA { get; set; }
        public string DESC_PELIG { get; set; }
        public string MER_INDICE_TRANS { get; set; }
        public string MER_CODIGO_UN { get; set; }
        public string MER_REFRIGERADO { get; set; }
        public string MER_ARMAS { get; set; }
        public string MER_AVE_AVERIA { get; set; }
        public string DESAVERIAS { get; set; }
        public string MER_TEM_EMBALAJE { get; set; }
        public string DESEMBALAJE { get; set; }
        public string MER_DESCRIPCION { get; set; }
        public string MER_OBSERVACIONES { get; set; }
        public string MER_BULTOS_DESPALETIZADOS { get; set; }
        public string MER_KILOS_BRUTO { get; set; }
        public string MER_KILOS_TARIMA { get; set; }
        public string MER_KILOS_NETO { get; set; }
        public string MER_ESTADO { get; set; }
        public string MER_OPERACION_TICA { get; set; }
        public string MER_ID_MERCANCIA { get; set; }
        public string MER_ID_MERCANCIA_REF { get; set; }
        public string MER_BULTOS_REF { get; set; }
        public string MER_LOC_RACK { get; set; }
        public string MER_LOC_COLUMNA { get; set; }
        public string MER_LOC_LADO { get; set; }
        public string MER_LOC_ALTURA { get; set; }
        public string MER_FCH_LOCALIZACION { get; set; }
        public string MER_PERECEDERO { get; set; }
        public string MER_LINEA_GUIA { get; set; }
        public string MER_PUESTO_CHEQUEO { get; set; }
        public string MER_MOV_INVENTARIO { get; set; }
        public string MER_FCH_DIGITACION { get; set; }
        public string MER_FCH_TRANSMISION { get; set; }
        public string MER_ARCHIVO_TRANSMISION { get; set; }
        public string MER_USUARIO_TRANSMISION { get; set; }
        public string MER_ERROR { get; set; }
        public string MER_ACCION { get; set; }
        public string MER_TRANSMISION { get; set; }
        public string MER_FCH_ENDOSO { get; set; }
        public string MER_LARGO { get; set; }
        public string MER_ALTO { get; set; }
        public string MER_ANCHO { get; set; }
        public string ESTADO { get; set; }
        public string DES_OPER_TICA { get; set; }
    }

    public class Tarimas_Modificar
    {
        public string MER_IEN_ENTRADA { get; set; }
        public string MER_MANIFIESTO { get; set; }
        public string MER_GUIA { get; set; }
        public string KILOS_INGRESADOS { get; set; }
        public string BULTOS_INGRESADOS { get; set; }
        public string MER_TME_MERCANCIA { get; set; }
        public string DES_MERCANCIA { get; set; }
        public string MER_MARCAS { get; set; }
        public string MER_TIP_CODIGO_IATA { get; set; }
        public string DESC_PELIG { get; set; }
        public string MER_INDICE_TRANS { get; set; }
        public string MER_CODIGO_UN { get; set; }
        public string MER_REFRIGERADO { get; set; }
        public string MER_ARMAS { get; set; }
        public string MER_AVE_AVERIA { get; set; }
        public string DESAVERIAS { get; set; }
        public string MER_TEM_EMBALAJE { get; set; }
        public string DESEMBALAJE { get; set; }
        public string MER_DESCRIPCION { get; set; }
        public string MER_OBSERVACIONES { get; set; }
        public string MER_BULTOS_DESPALETIZADOS { get; set; }
        public string MER_KILOS_BRUTO { get; set; }
        public string MER_KILOS_TARIMA { get; set; }
        public string MER_KILOS_NETO { get; set; }
        public string MER_ESTADO { get; set; }
        public string MER_OPERACION_TICA { get; set; }
        public string MER_ID_MERCANCIA { get; set; }
        public string MER_ID_MERCANCIA_REF { get; set; }
        public string MER_BULTOS_REF { get; set; }
        public string MER_LOC_RACK { get; set; }
        public string MER_LOC_COLUMNA { get; set; }
        public string MER_LOC_LADO { get; set; }
        public string MER_LOC_ALTURA { get; set; }
        public string MER_PERECEDERO { get; set; }
        public string MER_LINEA_GUIA { get; set; }
        public string MER_PUESTO_CHEQUEO { get; set; }
        public string MER_FCH_DIGITACION { get; set; }
        public string MER_TRANSMISION { get; set; }
        public string MER_ARCHIVO_TRANSMISION { get; set; }
        public string MER_FCH_TRANSMISION { get; set; }
        public string MER_USUARIO_TRANSMISION { get; set; }
        public string MER_FCH_ENDOSO { get; set; }
        public string MER_ACCION { get; set; }
        public string MER_MOV_INVENTARIO { get; set; }
        public string MER_LARGO { get; set; }
        public string MER_ALTO { get; set; }
        public string MER_ANCHO { get; set; }
        public string ESTADO { get; set; }
        
    }

    public class Tarimas_Modificar_post
    {
        public string MER_ID_MERCANCIA { get; set; }
        public string MER_PUESTO_CHEQUEO { get; set; }
        public string MER_BULTOS_DESPALETIZADOS { get; set; }
        public string MER_KILOS_BRUTO { get; set; }
        public string MER_KILOS_TARIMA { get; set; }
        public string MER_TME_MERCANCIA { get; set; }
        public string MER_TEM_EMBALAJE { get; set; }
        public string MER_AVE_AVERIA { get; set; }
        public string MER_LARGO { get; set; }
        public string MER_ALTO { get; set; }
        public string MER_ANCHO { get; set; }
        public string MER_TIP_CODIGO_IATA { get; set; }
        public string MER_CODIGO_UN { get; set; }
        public string MER_INDICE_TRANS { get; set; }
        public string MER_DESCRIPCION { get; set; }
        public string MER_MARCAS { get; set; }
        public string MER_LINEA_GUIA { get; set; }
        public string MER_OPERACION_TICA { get; set; }
        public string MER_OBSERVACIONES { get; set; }
        public string MER_PERECEDERO { get; set; }
        public string MER_REFRIGERADO { get; set; }
        public string MER_ARMAS { get; set; }
        public string MER_ID_MERCANCIA_REF { get; set; }
        public string MER_BULTOS_REF { get; set; }
        public string MER_LOC_RACK { get; set; }
        public string MER_LOC_COLUMNA { get; set; }
        public string MER_LOC_LADO { get; set; }
        public string MER_LOC_ALTURA { get; set; }
        public string MER_ESTADO { get; set; }
        public string MER_USER_MERX { get; set; }
        public string MER_MOT_ULTIMA_MODIFICACION { get; set; }
        public string MER_SOLICITA_CAMBIO { get; set; }
        public string MER_RESPONSABLE_ERROR { get; set; }
        public string MER_DEPARTAMENTO { get; set; }
    }
}