﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNSI.Models
{
    public class CAD_ControlAnden
    {
        public string CAD_ID { get; set; }
        public string CAD_FAC_FACTURA { get; set; }
        public string CAD_PLACA { get; set; }
        public string CAD_ESTADO { get; set; }
        public string CAD_FYH_COLA { get; set; }
        public string CAD_FYH_ASIGNADO { get; set; }
        public string CAD_FYH_INGRESO { get; set; }
        public string CAD_FYH_SALIDA { get; set; }
        public string CAD_FYH_ANULADO { get; set; }
        public string CAD_MOTIVO_ANULACION { get; set; }
        public string CAD_USU_REGISTRA { get; set; }
        public string CAD_POSICION { get; set; }
        public string CAD_TELEFONO { get; set; }
        public string CAD_TIPO_CAMION { get; set; }
        public string CAD_ANDEN { get; set; }
    }

    public class LV_ControlAnden
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class CAD_FAC_Detalle
    {
        public string FAC_NOMBRE_CUENTA { get; set; }
        public string FAC_CUENTA { get; set; }
        public string FAC_FACTURA { get; set; }
        public string FAC_AGA_NAGENCIA_ADUANAL { get; set; }
        public string AGA_NOMBRE { get; set; }
        public string FAC_NOMBRE_CHOFER { get; set; }
        public string FAC_CEDULA_CHOFER { get; set; }
        public string FAC_PLACA_VEHICULO { get; set; }
        public string FAC_NOMBRE_FACTURA { get; set; }
        public string FAC_CEDULA_FACTURA { get; set; }
        public string FAC_CONTRIBUYENTE { get; set; }
        public string FAC_NBR_CONTRIBUYENTE { get; set; }
        public string FAC_CEDULA_CONTRIB { get; set; }
    }

    public class FACXPLACA
    {
        public string FACTURA { get; set; }
        public string FAC_AGA_NAGENCIA_ADUANAL { get; set; }
        public string AGA_NOMBRE { get; set; }
        public string CHOFER { get; set; }
        public string FAC_CEDULA_CHOFER { get; set; }
        public string PLACA { get; set; }
        public string CLIENTE { get; set; }
        public string BLT_TOTALES { get; set; }
        public string KG_TOTALES { get; set; }
    }

    public class Control_Anden
    {
        public string CLIENTE { get; set; }
        public string CLIENTE_ID { get; set; }
        public string PLACA { get; set; }
        public string TIPO_CAMION { get; set; }
        public string TIPO_CAMION_COD { get; set; }
        public string FACTURAS { get; set; }
        public string TELEFONO { get; set; }
        public string POSICION { get; set; }
        public string ACCION { get; set; }
        public string ACCION_COD { get; set; }

        public bool mostrarboton { get; set; }

    }
}