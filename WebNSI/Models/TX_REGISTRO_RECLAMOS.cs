﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNSI.Models
{
    public class RRCL_Sistema
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class RRCL_Motivo
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class RRCL_Moneda
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class RRCL_Resolucion
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class RRCL_ResolucionOps
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class RRCL_Via
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class RRCL_Registro_Reclamos_Input
    {
        public long? s_rec_reclamo { get; set; }
        public string s_rec_fecha_presentacion { get; set; }
        public string s_rec_sistema { get; set; }
        public long? s_consecutivo { get; set; }
    }
    public class RRCL_Registro_Reclamos
    {
        public long REC_RECLAMO { get; set; }
        public string REC_SISTEMA { get; set; }
        public string DESC_SISTEMA { get; set; }        
        public string consecutivo { get; set; }
        public string MANIFIESTO { get; set; }
        public string GUIA_ORIGINAL { get; set; }
        public string CONSIGNATARIO { get; set; }
        public string AEROLINEA { get; set; }
        public string CONSOLIDADOR { get; set; }
        public string REC_MOTIVO { get; set; }
        public string DESC_MOTIVO { get; set; }
        public string REC_SOLICITADO { get; set; }
        public string REC_CONTACTO { get; set; }
        public decimal REC_MONTO { get; set; }
        public string REC_TIPO_MONEDA { get; set; }
        public string DESC_MONEDA { get; set; }
        public string REC_DESCRIP_RECLAMO { get; set; }
        public string REC_OBSERVACIONES { get; set; }
        public string REC_PREVIO_EXAMEN { get; set; }
        public string REC_USU_REGISTRA { get; set; }
        public string REC_FCH_REGISTRA { get; set; }
        public string REC_RESOLUCION { get; set; }
        public string DESC_RESOL { get; set; }
        public string REC_FCH_RECIBE_OPS { get; set; }
        public string REC_INVESTIGA_OPS { get; set; }
        public string REC_CAUSA_OPS { get; set; }
        public string REC_OBSERV_OPS { get; set; }
        public string REC_JUSTIFICA_ATRASO_OPS { get; set; }
        public string REC_USU_JUSTIF_OPS { get; set; }
        public string REC_FCH_JUSTIF_OPS { get; set; }
        public string REC_USU_INVESTIGA { get; set; }
        public string REC_FCH_RESOLUC_OPS { get; set; }
        public string REC_USU_RESOLUCION_OPS { get; set; }
        public string REC_RESOLUCION_OPS { get; set; }
        public string TIEMPO_INVESTIGA { get; set; }
        public string REC_REGISTRO_PNC { get; set; }
        public string DIAS_TRANSCURRIDOS { get; set; }
        public string REC_JUSTIFICA_ATRASO { get; set; }
        public string REC_PERSONA_NOTIF { get; set; }
        public string REC_FCH_NOTIF { get; set; }
        public string REC_VIA_NOTIF { get; set; }
        public string DESC_VIA_NOTIF { get; set; }
        public string REC_FCH_ENTREGA_CHEQUE { get; set; }
        public string REC_JUSTIF_RESOLUCION { get; set; }
        public string REC_APLICA_POLIZA { get; set; }
        public string REC_DECISION_INS { get; set; }
        public string REC_FCH_ENTREGA_CLIENTE { get; set; }
        public string REC_ENTRADA_EXP { get; set; }
        public string REC_OGU_GUIA { get; set; }
        public string REC_FECHA_PRESENTACION { get; set; }
        public string REC_FECHA_RESOLUCION { get; set; }
        public string REC_FCH_PASO_OPER { get; set; }
        public string REC_FCH_APR_DEN { get; set; }
        public string REC_TIPO { get; set; }
        public string REC_PRE_RECLAMO { get; set; }
        public string REC_FCH_PRERECLAMO { get; set; }
        public string REC_USU_PRERECLAMO { get; set; }
        public string REC_FCH_AMPLIACION { get; set; }
        public string REC_JUSTIF_AMPLIACION { get; set; }
        public string AGENCIA { get; set; }
        public string DUA { get; set; }
        public string FACTURA { get; set; }
        public string REGISTRO_PNC { get; set; }
        public string SIV { get; set; }
        public string PREVIO_EXAMEN { get; set; }
        public string DUA_INGRESO { get; set; }
        public string CLIENTE_NOMBRE { get; set; }
    }

    public class RRCL_Consecutivo_input
    {
        public string rec_sistema { get; set; }
        public string consecutivo { get; set; }
    }

    public class RRCL_Consecutivo
    {
        public string GUIA_ORIGINAL { get; set; }
        public string MANIFIESTO { get; set; }
        public string CONSIGNATARIO { get; set; }
        public string CONSOLIDADOR { get; set; }
        public string AEROLINEA { get; set; }
        public string AGENCIA { get; set; }
        public string DUA { get; set; }
        public string FACTURA { get; set; }
        public string REGISTRO_PNC { get; set; }
        public string SIV { get; set; }
        public string PREVIO_EXAMEN { get; set; }
        public string USUARIO_REGISTRA { get; set; }
        public string FECHA_REGISTRA { get; set; }
    }

    public class RRCL_Reclamo_input
    {
        public string REC_FECHA_PRESENTACION { get; set; }
        public string REC_SISTEMA { get; set; }
        public string consecutivo { get; set; }
        public string REC_MOTIVO { get; set; }
        public string REC_SOLICITADO { get; set; }
        public string REC_CONTACTO { get; set; }
        public string REC_MONTO { get; set; }
        public string REC_TIPO_MONEDA { get; set; }
        public string REC_DESCRIP_RECLAMO { get; set; }
        public string REC_OBSERVACIONES { get; set; }
        public string REC_RESOLUCION { get; set; }
        public string REC_FCH_RECIBE_OPS { get; set; }
        public string REC_INVESTIGA_OPS { get; set; }
        public string REC_CAUSA_OPS { get; set; }
        public string REC_OBSERV_OPS { get; set; }
        public string REC_JUSTIFICA_ATRASO_OPS { get; set; }
        public string REC_USU_JUSTIF_OPS { get; set; }
        public string REC_FCH_JUSTIF_OPS { get; set; }
        public string REC_USU_INVESTIGA { get; set; }
        public string REC_FCH_RESOLUC_OPS { get; set; }
        public string REC_USU_RESOLUCION { get; set; }
        public string REC_RESOLUCION_OPS { get; set; }
        public string REC_JUSTIFICA_ATRASO { get; set; }
        public string REC_PERSONA_NOTIF { get; set; }
        public string REC_FCH_NOTIF { get; set; }
        public string REC_VIA_NOTIF { get; set; }
        public string REC_FCH_ENTREGA_CHEQUE { get; set; }
        public string REC_JUSTIF_RESOLUCION { get; set; }
        public string REC_APLICA_POLIZA { get; set; }
        public string REC_DECISION_INS { get; set; }
        public string REC_FCH_ENTREGA_CLIENTE { get; set; }
        public string UserMERX { get; set; }
    }

    public class RRCL_Reclamo_update
    {
        public Int32 REC_RECLAMO { get; set; }
        public string REC_FECHA_PRESENTACION { get; set; }
        public string REC_SISTEMA { get; set; }
        public string consecutivo { get; set; }
        public string REC_MOTIVO { get; set; }
        public string REC_SOLICITADO { get; set; }
        public string REC_CONTACTO { get; set; }
        public string REC_MONTO { get; set; }
        public string REC_TIPO_MONEDA { get; set; }
        public string REC_DESCRIP_RECLAMO { get; set; }
        public string REC_OBSERVACIONES { get; set; }
        public string REC_RESOLUCION { get; set; }
        public string REC_FCH_RECIBE_OPS { get; set; }
        public string REC_INVESTIGA_OPS { get; set; }
        public string REC_CAUSA_OPS { get; set; }
        public string REC_OBSERV_OPS { get; set; }
        public string REC_JUSTIFICA_ATRASO_OPS { get; set; }
        public string REC_USU_JUSTIF_OPS { get; set; }
        public string REC_FCH_JUSTIF_OPS { get; set; }
        public string REC_USU_INVESTIGA { get; set; }
        public string REC_FCH_RESOLUC_OPS { get; set; }
        public string REC_USU_RESOLUCION { get; set; }
        public string REC_RESOLUCION_OPS { get; set; }
        public string REC_JUSTIFICA_ATRASO { get; set; }
        public string REC_PERSONA_NOTIF { get; set; }
        public string REC_FCH_NOTIF { get; set; }
        public string REC_VIA_NOTIF { get; set; }
        public string REC_FCH_ENTREGA_CHEQUE { get; set; }
        public string REC_JUSTIF_RESOLUCION { get; set; }
        public string REC_APLICA_POLIZA { get; set; }
        public string REC_DECISION_INS { get; set; }
        public string REC_FCH_ENTREGA_CLIENTE { get; set; }
        public string UserMERX { get; set; }
        
    }

    public class TX_REGISTRO_RECLAMOS
    {
    }
}