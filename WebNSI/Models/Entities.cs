﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace WebNSI.Models
{
    public class Entrada
    {
        public long IEN_Entrada { get; set; }

        public string IEN_Manifiesto { get; set; }

        public string IEN_Guia { get; set; }

        public string IEN_Estado { get; set; }

        public string DESC_Estado { get; set; }

        public string IEN_FCH_Ingreso { get; set; }

        public string IEN_FCH_Manifiesto { get; set; }

        public string IEN_FCH_Digitacion { get; set; }

        public decimal? IEN_GUIA_Numero { get; set; }

        public int IEN_BOD_Nbodega { get; set; }

        public string BOD_Nombre { get; set; }

        public string IEN_FUN_Funcionario { get; set; }

        public string FUN_Nombre { get; set; }

        public int IEN_AER_Naerolinea { get; set; }

        public string AER_Nombre { get; set; }

        public int IEN_PTO_Puerto { get; set; }

        public string PTO_Descripcion { get; set; }

        public string PTO_Codigo { get; set; }

        public string IEN_TDO_DOC_Hija { get; set; }

        public string TDO_Hija { get; set; }

        public short IEN_TDO_DOC_Madre { get; set; }

        public string TDO_Sia { get; set; }

        public int IEN_CON_NConsolidador { get; set; }

        public string CON_Nombre { get; set; }

        public byte IEN_ADUANA_Recibe { get; set; }

        public string ADU_Recibe { get; set; }

        public byte? IEN_ADUANA_Envia { get; set; }

        public string IEN_Observaciones { get; set; }

        public string IEN_GUIA_Master { get; set; }

        public int IEN_USUARIO_Digito { get; set; }

        public string USU_Nombre { get; set; }

        public string IEN_Consignatario { get; set; }

        public int? IEN_CSG_Consignatario { get; set; }

        public decimal IEN_KILOS_Manifestados { get; set; }

        public decimal? IEN_KILOS_Ingresados { get; set; }

        public decimal IEN_KILOS_Despaletizados { get; set; }

        public decimal? IEN_KILOS_Reservados { get; set; }

        public decimal IEN_KILOS_Salidos { get; set; }

        public decimal KG_Desp { get; set; }

        public decimal KG_Reser { get; set; }

        public decimal KG_Salid { get; set; }

        public decimal IEN_BULTOS_Manifestados { get; set; }

        public decimal? IEN_BULTOS_Ingresados { get; set; }

        public decimal? IEN_BULTOS_RealIngreso { get; set; }

        public decimal? BLT_Despaletizados { get; set; }

        public decimal? BLT_Reser { get; set; }

        public decimal? BLT_Salid { get; set; }

        public decimal? IEN_BULTOS_Despaletizados { get; set; }

        public decimal IEN_BULTOS_Reservados { get; set; }

        public decimal IEN_BULTOS_Salidos { get; set; }

        public short? IEN_MOT_Motivo { get; set; }

        public string IEN_DESCRIPCION_Manif { get; set; }

        public string IEN_MODALIDAD_Transporte { get; set; }

        public string IEN_UBICACION_Ingreso { get; set; }

        public string UBICACION_INGRESO { get; set; }

        public int? IEN_Autorizacion { get; set; }

        public string IEN_TEXTO_Acta { get; set; }

        public long? IEN_NUMERO_Viaje { get; set; }

        public string IEN_IMP_Cedula { get; set; }

        public string IEN_Importador { get; set; }

        public string IEN_Modif { get; set; }

        public string IEN_Bloqueada { get; set; }

        public int? IEN_ACTA_Numero { get; set; }

        public string IEN_ACTA_Texto { get; set; }

        public string IEN_GUIA_Original { get; set; }

        public string IEN_DIN_TIPO_Documento { get; set; }

        public string DIN_Descripcion { get; set; }

        public string IEN_Modalidad { get; set; }

        public string IEN_SOLICITA_Cambio { get; set; }

        public string IEN_RESPONSABLE_Error { get; set; }

        public DateTime? IEN_FCH_INV_Aduana { get; set; }

        public string IEN_Departamento { get; set; }

        public string IEN_Peredecero { get; set; }

        public string IEN_Clase { get; set; }

        public string IEN_UBICACION_Transito { get; set; }

        public string UBITRANSITO { get; set; }

        public string UBI_Destino { get; set; }

        public string IEN_DUDA_Refri { get; set; }

        public string DRF_Duda { get; set; }

        public string IEN_BOLETA_Clase { get; set; }

        public string IEN_TIPO_DUDA_Ref { get; set; }

        public string DUDA_REF { get; set; }

        public string IEN_Dut { get; set; }

        public string IEN_CNS_Consignatario { get; set; }

        public string IEN_ASOCIE_CED_Tica { get; set; }

        public DateTime? IEN_FCH_Cierre { get; set; }

        public DateTime? IEN_FCH_Asocie { get; set; }

        public long? IEN_Antecesor { get; set; }

        public DateTime? IEN_FCH_ULT_Bloqueo { get; set; }

        public string IEN_ES_Courier { get; set; }

        public string Muestras { get; set; }

        public Boolean Varios { get; set; }

        public string UserLog { get; set; }

        public long? IEN_PUESTO_Chequeo { get; set; }


    }

    public class MercanciaCORREC
    {
        public string MER_ID_Mercancia { get; set; }
        public string MER_IEN_Entrada { get; set; }
        public string MER_LINEA_Guia { get; set; }
        public string MER_Estado { get; set; }
        public string MER_OPERACION_Tica { get; set; }
        public string MER_ID_MERCANCIA_Ref { get; set; }
        public string MER_BULTOS_Ref { get; set; }
        public string MER_Manifiesto { get; set; }
        public string MER_Guia { get; set; }
        public string MER_DESCRIPCION { get; set; }
        public string MER_Transmision { get; set; }
        public string MER_ARCHIVO_Transmision { get; set; }
        public string MER_FCH_Transmision { get; set; }
        public string MER_USUARIO_Transmision { get; set; }
        public string MER_Accion { get; set; }
        public string MER_Error { get; set; }
        public string MER_EN_Tica { get; set; }
        public string MER_TER_Error { get; set; }
        public string MER_MOV_Inventario { get; set; }
        public string DESC_OPER_Tica { get; set; }
        public string consecutivo { get; set; }

        //public string DESC_Transmision { get; set; }
        //public string MER_ID_FUNCIONARIO_Aduana { get; set; }
        //public string MER_Observaciones { get; set; }
        //public string MER_Marcas { get; set; }
        //public string MER_FCH_Localizacion { get; set; }
        //public string MER_MOT_ULTIMA_Modificacion { get; set; }
        //public string MER_MOT_Borrado { get; set; }
        //public string MER_A_Aforo { get; set; }
        //public string MER_Facturar { get; set; }
        //public string MER_USUARIO_Localiza { get; set; }
        //public string MER_BOLETA_Remate { get; set; }
        //public string MER_IMP_Cedula { get; set; }
        //public string IMP_Nombre { get; set; }
        //public string MER_FCH_Endoso { get; set; }
        //public string MER_Selector { get; set; }
        //public string MER_BULTOS_Pre { get; set; }
        //public string MER_KILOS_Pre { get; set; }
        //public string MER_Viaje { get; set; }
        //public string DESC_Accion { get; set; }
        //public string MER_LINEA_GUIA { get; set; }
        //public string MER_OPERACION_TICA { get; set; }
        //public string MER_ID_MERCANCIA_REF { get; set; }
        //public string MER_BULTOS_REF { get; set; }
        //public string MER_AUTORIZ_Reemp { get; set; }
        //public string MER_Partida { get; set; }
        //public string MER_IDENTIF_Agente { get; set; }
        //public string MER_SOLICITA_Cambio { get; set; }
        //public string MER_RESPONSABLE_Error { get; set; }
        //public string MER_FCH_INV_Aduana { get; set; }
        //public string MER_Departamento { get; set; }
        //public string MER_Perecedero { get; set; }
        //public string MER_PUESTO_CHEQUEO { get; set; }
        //public string MER_PELIG_Iata { get; set; }
        //public string DESC_PELIG { get; set; }
        //public string MER_FCH_Finalizado { get; set; }
        //public string MER_CODIGO_un { get; set; }
        //public string MER_INDICE_TRANS { get; set; }
        //public string MER_TIP_CODIGO_IATA { get; set; }
        //public string MER_ARMAS { get; set; }
        //public string AVE_Descripcion { get; set; }
        //public string MOT_Descripcion { get; set; }
        //public string TIP_CODIGO_Iata { get; set; }
        //public string TIP_Descripcion { get; set; }
        //public string TER_Descripcion { get; set; }
        //public string Bultosesc { get; set; }
        //public string Kilosesc { get; set; }
        //public string error_p { get; set; }
        //public string MER_MARCAS { get; set; }
        //public string MER_CODIGO_UN { get; set; }
        //public string DESAVERIAS { get; set; }
        //public string DESEMBALAJE { get; set; }
        //public string MER_KILOS_NETOS { get; set; }
        //public string MER_PERECEDERO { get; set; }
        //public string DES_MERCANCIA { get; set; }
        //public string MER_LARGO { get; set; }
        //public string MER_ALTO { get; set; }
        //public string MER_ANCHO { get; set; }
        //public string MER_USUARIO { get; set; }

    }

    public class Mercancia
    {
        public string MER_ID_MERCANCIA { get; set; }
        public string IEN_ENTRADA { get; set; }
        public string MER_IEN_ENTRADA { get; set; }
        public string MER_Manifiesto { get; set; }
        public string MER_Guia { get; set; }
        public string MER_DESCRIPCION { get; set; }
        public string BULTOS_INGRESADOS { get; set; }
        public string MER_BULTOS_DESPALETIZADOS { get; set; }
        public string MER_BULTOS_Reservados { get; set; }
        public string MER_BULTOS_Salidos { get; set; }
        public string Existencias { get; set; }
        public string KILOS_INGRESADOS { get; set; }
        public string MER_KILOS_BRUTO { get; set; }
        public string MER_KILOS_TARIMA { get; set; }
        public string MER_KILOS_Reservados { get; set; }
        public string MER_KILOS_Salidos { get; set; }
        public string Kbruto { get; set; }
        public string MER_PEL_Peligrosa { get; set; }
        public string PEL_Descripcion { get; set; }
        public string MER_ESTADO { get; set; }
        public string DESC_Estado { get; set; }
        public string MER_FCH_Digitacion { get; set; }
        public string MER_REFRIGERADO { get; set; }
        public string MER_TEM_EMBALAJE { get; set; }
        public string TEM_Descripcion { get; set; }
        public string MER_AVE_AVERIA { get; set; }
        public string MER_TME_MERCANCIA { get; set; }
        public string TME_Descripcion { get; set; }
        public string MER_LOC_RACK { get; set; }
        public string MER_LOC_COLUMNA { get; set; }
        public string MER_LOC_ALTURA { get; set; }
        public string MER_LOC_LADO { get; set; }
        public string MER_Transmision { get; set; }
        public string DESC_Transmision { get; set; }
        public string MER_ARCHIVO_Transmision { get; set; }
        public string MER_FCH_Transmision { get; set; }
        public string MER_USUARIO_Transmision { get; set; }
        public string MER_ID_FUNCIONARIO_Aduana { get; set; }
        public string MER_Observaciones { get; set; }
        public string MER_Marcas { get; set; }
        public string MER_FCH_Localizacion { get; set; }
        public string MER_MOT_ULTIMA_Modificacion { get; set; }
        public string MER_MOT_Borrado { get; set; }
        public string MER_A_Aforo { get; set; }
        public string MER_Facturar { get; set; }
        public string MER_USUARIO_Localiza { get; set; }
        public string MER_BOLETA_Remate { get; set; }
        public string MER_IMP_Cedula { get; set; }
        public string IMP_Nombre { get; set; }
        public string MER_FCH_Endoso { get; set; }
        public string MER_Selector { get; set; }
        public string MER_BULTOS_Pre { get; set; }
        public string MER_KILOS_Pre { get; set; }
        public string MER_Viaje { get; set; }
        public string MER_Accion { get; set; }
        public string DESC_Accion { get; set; }
        public string MER_LINEA_GUIA { get; set; }
        public string MER_OPERACION_TICA { get; set; }
        public string MER_ID_MERCANCIA_REF { get; set; }
        public string MER_BULTOS_REF { get; set; }
        public string MER_Error { get; set; }
        public string MER_EN_Tica { get; set; }
        public string MER_TER_Error { get; set; }
        public string MER_MOV_Inventario { get; set; }
        public string MER_AUTORIZ_Reemp { get; set; }
        public string MER_Partida { get; set; }
        public string MER_IDENTIF_Agente { get; set; }
        public string MER_SOLICITA_Cambio { get; set; }
        public string MER_RESPONSABLE_Error { get; set; }
        public string MER_FCH_INV_Aduana { get; set; }
        public string MER_Departamento { get; set; }
        public string MER_Perecedero { get; set; }
        public string MER_PUESTO_CHEQUEO { get; set; }
        public string MER_PELIG_Iata { get; set; }
        public string DESC_PELIG { get; set; }
        public string MER_FCH_Finalizado { get; set; }
        public string MER_CODIGO_un { get; set; }
        public string MER_INDICE_TRANS { get; set; }
        public string MER_TIP_CODIGO_IATA { get; set; }
        public string MER_ARMAS { get; set; }
        public string AVE_Descripcion { get; set; }
        public string MOT_Descripcion { get; set; }
        public string TIP_CODIGO_Iata { get; set; }
        public string TIP_Descripcion { get; set; }
        public string DESC_OPER_Tica { get; set; }
        public string TER_Descripcion { get; set; }
        public string Bultosesc { get; set; }
        public string Kilosesc { get; set; }
        public string error_p { get; set; }
        public string consecutivo { get; set; }
        public string MER_MARCAS { get; set; }
        public string MER_CODIGO_UN { get; set; }
        public string DESAVERIAS { get; set; }
        public string DESEMBALAJE { get; set; }
        public string MER_KILOS_NETOS { get; set; }
        public string MER_PERECEDERO { get; set; }
        public string DES_MERCANCIA { get; set; }
        public string MER_LARGO { get; set; }
        public string MER_ALTO { get; set; }
        public string MER_ANCHO { get; set; }
        public string MER_USUARIO { get; set; }

    }

    public class Tarima
    {
        public long TARIMA { get; set; }     //MER_ID_Mercancia
        public long CONSECUTIVO { get; set; }     //MER_IEN_Entrada
        public string MANIFIESTO { get; set; }    //MER_Manifiesto
        public string GUIA { get; set; }    //MER_Guia
        public decimal BULTOS { get; set; }     //Bultosesc
        public decimal KBRUTO { get; set; }     //Kilosesc
        public string DESC_ESTADO { get; set; }      //DESC_Estado
    }

    #region Importador
    public class Importador
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class TipoRevision
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class TipoCliente
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class TipoPersona
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class Agencia
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class Agente
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }
    public class Autoridades
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }
    #endregion

    public class EntradasPrevio
    {
        public long? IEN_Entrada { get; set; }

        public string IEN_GUIA_Original { get; set; }

        public string IEN_Manifiesto { get; set; }

        public string IEN_IMP_Cedula { get; set; }

        public string IEN_Importador { get; set; }

        public decimal? IEN_BULTOS_Manifestados { get; set; }

        public decimal? IEN_BULTOS_Realingreso { get; set; }

        public decimal? BLT_Despaletizados { get; set; }

        public decimal? BLT_Reser { get; set; }

        public decimal? BLT_Salid { get; set; }

        public decimal? IEN_KILOS_Manifestados { get; set; }

        public decimal? KG_Desp { get; set; }

        public decimal? KG_Reser { get; set; }

        public decimal? KG_Salid { get; set; }

        public string IEN_Observaciones { get; set; }

        public string IEN_DESCRIPCION_Manif { get; set; }
    }

    public class EntradaBloqueo
    {
        public long? IEN_Entrada { get; set; }

        public string IEN_Guia { get; set; }

        public string IEN_Manifiesto { get; set; }

        public string IEN_FCH_Ingreso { get; set; }

        public string IEN_Importador { get; set; }

        public long? Existencias_p { get; set; }

        public string Error_p { get; set; }

        public string BLO_MOTIVO_Bloqueo { get; set; }

        public string BLO_TIPO_Bloqueo { get; set; }

        public string BLO_VIA_Solicitud { get; set; }

        public string BLO_Acta { get; set; }


    }

    public class DetalleMercanciaPrevio
    {
        public long? MER_ID_Mercancia { get; set; }
        public string MER_TME_Mercancia { get; set; }
        public string TME_Descripcion { get; set; }
        public string MER_Refrigerado { get; set; }
        public decimal MER_BULTOS_Despaletizados { get; set; }
        public decimal MER_BULTOS_Salidos { get; set; }
        public decimal KBruto { get; set; }
        public string MER_TEM_Embalaje { get; set; }
        public string TEM_Descripcion { get; set; }
        public string DESC_Estado { get; set; }
    }

    public class TiqueteSala
    {
        public long? ATS_Tiquete { get; set; }
        public string ATS_TIPO_Cola { get; set; }
        public string ATS_FCH_Impreso { get; set; }
        public string ATS_FCH_Actual { get; set; }
    }

    public class TipoBloqueo
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class ViaSolicitud
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class RCL_TipoCliente
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class RCL_Tipo
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class RCL_Area
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class RCL_Servicio
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class Reclamo
    {
        public long RCL_Id { get; set; }

        public string RCL_Fecha { get; set; }

        public string RCL_Tipo { get; set; }

        public string RCL_TIPO_Cliente { get; set; }

        public string RCL_Empresa { get; set; }

        public string RCL_Representante { get; set; }

        public string RCL_Email { get; set; }

        public string RCL_Telefono { get; set; }

        public string RCL_Area { get; set; }

        public string RCL_AREA_Otro { get; set; }

        public string RCL_Servicio { get; set; }

        public string RCL_SERVICIO_Otro { get; set; }

        public string RCL_Descripcion { get; set; }

        public string RCL_IEN_Entrada { get; set; }

        public string RCL_OGU_Guia { get; set; }

        public string RCL_USU_Registra { get; set; }

        public string RCL_FCH_Registra { get; set; }

        public string RCL_Observacion { get; set; }

        public string RCL_USU_Seguimiento { get; set; }

        public string RCL_FCH_Seguimiento { get; set; }

        public long rcl_id_p { get; set; }

        public string error_p { get; set; }

        public string UserMERX { set; get; }
    }

    public class Peligrosidad
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class Cajera
    {
        public string CJR_Cajera { get; set; }
        public string CJR_Principal { get; set; }
        public string CJR_Activo { get; set; }
        public string CJR_Doble_impr { get; set; }
        public string CJR_Usu_registro { get; set; }
        public string CJR_Fch_registro { get; set; }
        public string CJR_Fch_modifico { get; set; }
        public string CJR_Usu_modifico { get; set; }

    }

    public class Bitacora
    {
        public long BIT_Consecutivo { get; set; }
        public string BIT_Fecha { get; set; }
        public string BIT_GuiaOriginal { get; set; }
        public string BIT_Tarima { get; set; }
        public string BIT_Tarima_Desc { get; set; }
        public string BIT_Movimiento { get; set; }
        public long BIT_Evento { get; set; }
        public string BIT_Evento_Desc { get; set; }
        public string BIT_Usuario { get; set; }
        public string BIT_Descripcion { get; set; }

    }

    public class TipoEvento
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    #region Declarantes
    public class Declarante
    {
        public string DCT_Id { get; set; }
        public string DCT_Nombre { get; set; }
        public long? DCT_Cedula { get; set; }
        public string DCT_Telefono { get; set; }
        public string DCT_Email { get; set; }
        public string DCT_Estado_Tica { get; set; }
        public string DCT_Estado { get; set; }
        public string DCT_Usu_Registro { get; set; }
        public string DCT_Fch_Registro { get; set; }
        public string UserMERX { set; get; }

    }

    public class Funcionario
    {
        public string ADC_DCT_Id { get; set; }
        public long? ADC_Agente_Id { get; set; }
        public string ADC_Tipo { get; set; }
        public string DES_Tipo { get; set; }
        public string ADC_Nombre { get; set; }
        public long? ADC_Cedula { get; set; }
        public string ADC_Estado_Tica { get; set; }
        public string ADC_Telefono { get; set; }
        public string ADC_Email { get; set; }
        public string ADC_Estado_Tsm { get; set; }
        public string ADC_DES_ESTADO { get; set; }
        public string ADC_Fch_Registro { get; set; }
        public string ADC_Usu_Registro { get; set; }
        public string UserMERX { set; get; }
    }

    public class TipoEstadoTica
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class TipoFuncionario
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class EstadoTica
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }
    #endregion

    //Modelo con los campos que necesitamos para el combo de Resolución en mercancias perdidas.  
    public class TipoResolucion
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }
    //Modelo con los campos para mostrar la información de las mercancias perdidas.
    public class MercanciasPerdidas
    {

        public int MRP_ID { get; set; }
        public string MRP_MER_ID_MERCANCIA { get; set; }
        public string MRP_CONSIGNATARIO { get; set; }
        public string MRP_FCH_INICIO { get; set; }
        public string MRP_FCH_FINALIZA { get; set; }
        public string MRP_USU_REPORTA { get; set; }
        public string MRP_USU_FINALIZA { get; set; }
        public string MRP_TIPO_RESOL { get; set; }
        public string DES_TIPO_RESOL { get; set; }
        public string UserMERX { set; get; }
    }

    #region Bloqueos
    public class BloqueoGuia
    {
        public long BLO_IEN_ENTRADA { get; set; }
        public string BLO_ID_BLOQUEO { get; set; }
        public string FECHA_INGRESO { get; set; }
        public string MANIFIESTO { get; set; }
        public string GUIA { get; set; }
        public string EXISTENCIAS { get; set; }
        public string IMPORTADOR { get; set; }
        public string BLO_FACTURACION { get; set; }
        public string BLO_PREVIOS { get; set; }
        public string BLO_ORDENES_DESP { get; set; }
        public string BLO_TIPO_BLOQUEO { get; set; }
        public string BLO_VIA_SOLICITUD { get; set; }
        public string BLO_ACTA { get; set; }
        public string BLO_USU_BLOQUEA { get; set; }
        public string USUARIO_BLOQUEA { get; set; }
        public string BLO_FCH_BLOQUEO { get; set; }
        public string BLO_MOTIVO_BLOQUEO { get; set; }
        public string BLO_USU_DESBLOQUEA { get; set; }
        public string USUARIO_DESBLOQUEA { get; set; }
        public string BLO_FCH_DESBLOQUEO { get; set; }
        public string BLO_MOTIVO_DESBLOQUEO { get; set; }
        public string USER_MERX { get; set; }
    }

    public class Pre_Bloqueo
    {
        public string IPB_GUIA { get; set; }
        public string IPB_CSG_CONSIGNATARIO { get; set; }
        public string CONSIGNATARIO { get; set; }
        public string IPB_OBS { get; set; }
        public string USER_MERX { get; set; }
    }

    public class Consignatario
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Consecutivo
    {
        public long BLO_IEN_ENTRADA { get; set; }
        public string FECHA_INGRESO { get; set; }
        public string MANIFIESTO { get; set; }
        public string GUIA { get; set; }
        public string ORIGINAL { get; set; }
        public string IMPORTADOR { get; set; }
        public long EXISTENCIAS { get; set; }

    }
    #endregion

    #region Ingreso Mercancías
    public class Manifiesto_Busqueda
    {
        public int N_REGISTROS { get; set; }
        public List<Manifiesto> REGISTROS { get; set; }
    }

    public class Manifiesto
    {
        public string TMA_MANIFIESTO { get; set; }
        public string TMA_FCH_MANIFIESTO { get; set; }
        public string TMA_FCH_INGRESO { get; set; }
        public string TMA_FCH_REGIMEN { get; set; }       
        public long TMA_BOD_NBODEGA { get; set; }
        public long TMA_ADU_RECIBE { get; set; }
        public long TMA_ADU_ENVIA { get; set; }
        public string ADU_RECIBE { get; set; }
        public string ADU_ENVIA { get; set; }
        public long TMA_PTO_PUERTO { get; set; }
        public long? TMA_AER_NAEROLINEA { get; set; }
        public string AEROLINEA { get; set; }
        public string TMA_MOD_TRANSPORTE { get; set; }
        public long TMA_VIAJE { get; set; }
        public string TMA_VIAJE_CONTINGENCIA { get; set; }
        public string TMA_USU_REGISTRO { get; set; }
        public string TMA_FCH_REGISTRO { get; set; }
        public string TMA_ESTADO { get; set; }
        public string TMA_GUIA_MADRE { get; set; }
        public long TMA_MOT_MOTIVO { get; set; }
        public string TMA_USU_MODIFICO { get; set; }
        public string TMA_FCH_MODIFICO { get; set; }
        public string TMA_DEPARTAMENTO { get; set; }
        public string TMA_RESPONSABLE { get; set; }
        public string TMA_SOLICITANTE { get; set; }
        public string UserMERX { get; set; }
        public string TMA_COD_VUELO { get; set; }
        public string TMA_DUA { get; set; }
        public string TMA_CLIENTE_NOMBRE { get; set; }
        public string TMA_NUM_CONTENEDOR { get; set; }
        public string TMA_TIPO_CONTENEDOR { get; set; }
        public string TIPO_CONTENEDOR { get; set; }
        public long TMA_TAMANO { get; set; }
        public string TMA_TDO_DOCUMENTO { get; set; }
        public string TMA_CLIENTE_ID { get; set; }
        public string TMA_FUN_FUNCIONARIO { get; set; }
        public string TMA_ACTA_NUMERO { get; set; }
        public string TMA_CODIGO_VUELO { get; set; }
        public string TMA_PORTON { get; set; }
        public string TMA_CHEQUEADOR { get; set; }
        public string TMA_PAB { get; set; }
        public string TMA_PALETIZADO { get; set; }
        public string TMA_EQUIPO_ESPECIAL { get; set; }
        public string TMA_SEPARACIONES { get; set; }
        public string TMA_CLIENTE_TIPO { get; set; }
        public string TMA_ID_MANIFIESTO { get; set; }
        public string TMA_UBICACION_INGRESO { get; set; }
        public string TMA_ID_FUN_SUPERVISION { get; set; }
        public string TMA_ACTA_SUPERVISION { get; set; }
    }

    public class Contenedor
    {
        public long TCO_CONTENEDOR { get; set; }
        public long TCO_TDO_DOCUMENTO { get; set; }
        public string TCO_TMA_MANIFIESTO { get; set; }
        public long TCO_DUA { get; set; }
        public long TCO_ADU_ADUANA { get; set; }
        public string ADUANA { get; set; }
        public long TCO_PERIODO { get; set; }
        public long TCO_VIAJE { get; set; }
        public string TCO_NUM_CONTENEDOR { get; set; }
        public long TCO_TAMANO { get; set; }
        public long TCO_CON_NCONSOLIDADOR { get; set; }
        public string CONSOLIDADOR { get; set; }
        public string TCO_ESTADO { get; set; }
        public string TCO_CHEQUEADOR { get; set; }
        public string TCO_GRUPO_TRABAJO { get; set; }
        public string TCO_PUERTA { get; set; }
        public long TCO_PERSONAS_DESCARGA { get; set; }
        public string TCO_USU_REGISTRO { get; set; }
        public string TCO_FCH_REGISTRO { get; set; }
        public long TCO_MOT_MOTIVO { get; set; }
        public string TCO_USU_MODIFICO { get; set; }
        public string TCO_FCH_MODIFICO { get; set; }
        public string TCO_DEPARTAMENTO { get; set; }
        public string TCO_RESPONSABLE { get; set; }
        public string TCO_SOLICITANTE { get; set; }
        public string UserMERX { get; set; }
        public string TCO_FCH_INGRESO { get; set; }
        public string TCO_TIPO_CONTENEDOR { get; set; }
        public string TCO_FUNC_ADUANA { get; set; }
        public string TCO_ACTA_SUP { get; set; }
        public string TCO_PERMANENCIA { get; set; }
    }

    public class Guia_backup
    {
        public string IEN_Manifiesto { get; set; }
        public string IEN_Guia { get; set; }
        public string IEN_GUIA_Original { get; set; }
        public long IEN_Entrada { get; set; }
        public long? IEN_PUESTO_Chequeo { get; set; }
        public string IEN_FCH_Ingreso { get; set; }
        public string IEN_FCH_Digitacion { get; set; }
        public string IEN_GUIA_Master { get; set; }
        public string IEN_TDO_DOC_Hija { get; set; }
        public string IEN_DIN_TIPO_Documento { get; set; }
        public long? IEN_Antecesor { get; set; }
        public int IEN_CON_NConsolidador { get; set; }
        public string IEN_Consignatario { get; set; }
        public string IEN_ES_Courier { get; set; }
        public decimal IEN_BULTOS_Manifestados { get; set; }
        public decimal IEN_KILOS_Manifestados { get; set; }
        public decimal? IEN_BULTOS_Ingresados { get; set; }
        public decimal? IEN_KILOS_Ingresados { get; set; }
        public string IEN_DESCRIPCION_Manif { get; set; }
        public string IEN_Observaciones { get; set; }
        public string IEN_UBICACION_Ingreso { get; set; }
        public string IEN_Clase { get; set; }
        public string IEN_BOLETA_Clase { get; set; }
        public string IEN_UBICACION_Transito { get; set; }
        public string IEN_DUDA_Refri { get; set; }
        public string IEN_TIPO_DUDA_Ref { get; set; }
        public long? IEN_NUMERO_Viaje { get; set; }
        public int IEN_BOD_Nbodega { get; set; }
        public string UserMERX { get; set; }
        public string IEN_Estado { get; set; }
        public int IEN_USUARIO_Digito { get; set; }
        public string IEN_FCH_Manifiesto { get; set; }
        public int IEN_AER_Naerolinea { get; set; }
        public string AER_Nombre { get; set; }
        public int IEN_PTO_Puerto { get; set; }
        public string PTO_Codigo { get; set; }
        public string PTO_Descripcion { get; set; }
        public string TDO_Hija { get; set; }
        public string DIN_Descripcion { get; set; }
        public string CON_Nombre { get; set; }
        public string IEN_MODALIDAD_Transporte { get; set; }
        public string UBICACION_INGRESO { get; set; }
        public string UBITRANSITO { get; set; }
        public string DUDA_REF { get; set; }
    }

    public class Tarima_Ing
    {
        public long MER_IEN_ENTRADA { get; set; }
        public string MER_MANIFIESTO { get; set; }
        public string MER_GUIA { get; set; }
        public decimal KILOS_INGRESADOS { get; set; }
        public decimal BULTOS_INGRESADOS { get; set; }
        public short MER_TME_MERCANCIA { get; set; }
        public string DES_MERCANCIA { get; set; }
        public string MER_MARCAS { get; set; }
        public string MER_TIP_CODIGO_IATA { get; set; }
        public string DESC_PELIG { get; set; }
        public string MER_INDICE_TRANS { get; set; }
        public string MER_CODIGO_UN { get; set; }
        public string MER_REFRIGERADO { get; set; }
        public string MER_ARMAS { get; set; }
        public short MER_AVE_AVERIA { get; set; }
        public string DESAVERIAS { get; set; }
        public string MER_TEM_EMBALAJE { get; set; }
        public string DESEMBALAJE { get; set; }
        public string MER_DESCRIPCION { get; set; }
        public string MER_OBSERVACIONES { get; set; }
        public decimal MER_BULTOS_DESPALETIZADOS { get; set; }
        public decimal MER_KILOS_BRUTO { get; set; }
        public decimal MER_KILOS_TARIMA { get; set; }
        public decimal MER_KILOS_NETO { get; set; }
        public string MER_ESTADO { get; set; }
        public string MER_OPERACION_TICA { get; set; }
        public long? MER_ID_MERCANCIA { get; set; }
        public string MER_ID_MERCANCIA_REF { get; set; }
        public string MER_BULTOS_REF { get; set; }
        public decimal MER_LOC_RACK { get; set; }
        public decimal MER_LOC_COLUMNA { get; set; }
        public decimal MER_LOC_LADO { get; set; }
        public decimal MER_LOC_ALTURA { get; set; }
        public string MER_PERECEDERO { get; set; }
        public long? MER_LINEA_GUIA { get; set; }
        public long? MER_PUESTO_CHEQUEO { get; set; }
        public string MTA_USU_MODIFICO { get; set; }
        public string MTA_USU_INGRESO { get; set; }
        public string UserMERX { get; set; }
        public string MER_ETIQUETA { get; set; }

    }

    public class Prealerta
    {
        public string Existe { get; set; }
        public string Archivo { get; set; }
    }

    public class Puerto
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public int Pais { get; set; }
        public string NombrePais { get; set; }
        public string UsuarioRegistro { get; set; }
        public DateTime? FechaRegistro { get; set; }
    }

    public class Porton
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Consolidador
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Cedula { get; set; }
    }

    public class ConsolidadorUpt
    {
        public string CON_NCONSOLIDADOR { get; set; }
        public string CON_NOMBRE { get; set; }
        public string CON_CEDULA { get; set; }
        public string CON_ESTADO_ENTRADA { get; set; }
        public string CON_TELEFONO1 { get; set; }
        public string CON_TELEFONO2 { get; set; }
        public string CON_TELEFONO_FAX { get; set; }
        public string CON_EST_ESTADO { get; set; }
        public string CON_APARTADO { get; set; }
        public string CON_RECIBE_ESTADOS { get; set; }
        public string CON_EMAIL { get; set; }
        public string CON_FECHA_INGRESO { get; set; }
        public string CON_DIRECCION { get; set; }
        public string CON_OBSERVACIONES { get; set; }
        public string CON_MODIFICACIONES { get; set; }
        public string CON_FCH_RIGE { get; set; }
        public string CON_USU_MODIFICA { get; set; }
        public string CON_FCH_MODIFICA { get; set; }
        public string UserMERX { set; get; }
    }

    public class UbicacionConsolidador
    {
        public string COU_CON_NCONSOLIDADOR { get; set; }
        public string COU_UBI_UBICACION { get; set; }
        public string UBI_RAZON_SOCIAL { get; set; }
        public long CONSIGNATARIOS { get; set; }

    }

    public class ConsignatarioUbicacion
    {
        public string COC_ID { get; set; }
        public string COC_CON_NCONSOLIDADOR { get; set; }
        public string COC_UBI_UBICACION { get; set; }
        public string COC_CONSIGNATARIO { get; set; }

    }
    #region Listas de valores
    public class Mod_transporte
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Estados
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Documento
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Transportista
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Aduana
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Departamento
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }

    public class GruposTrabajo
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Clasificacion
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class DudasRefri
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class IndicRefri
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Localizacion
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class TipoMercancia
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class TipoPeligrosidadIATA
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Condicion
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Embalaje
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class OperacionTica
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Chekeador
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }
    #endregion

    #endregion

    //Login
    public class Usuario
    {
        public string User { get; set; }
        public string Password { get; set; }
    }


    #region Prealertas
    public class Prealertas
    {
        public string PLR_GUIA { get; set; }
        public string PLR_CON_NCONSOLIDADOR { get; set; }
        public string NOM_CONSOL { get; set; }
        public string PLR_ARCHIVO { get; set; }
        public string PLR_USU_REGISTRO { get; set; }
        public string PLR_FCH_REGISTRO { get; set; }

    }

    public class Cons_Prealertas
    {
        public string PLR_GUIA { get; set; }
        public string PLR_ARCHIVO { get; set; }
        public string PLR_CON_NCONSOLIDADOR { get; set; }
        public string NOM_CONSOL { get; set; }
        public string PLR_USU_REGISTRO { get; set; }
        public string PLR_FCH_REGISTRO { get; set; }
    }

    public class Desconsolidacion
    {
        public string VUELO { get; set; }
        public string CONSECUTIVO { get; set; }
        public string GUIA_ORIGINAL { get; set; }
        public string TOT_GUIAS { get; set; }
        public string CONT_GUIAS { get; set; }
        public string USUARIO { get; set; }
        public string CONSOLIDADOR { get; set; }
        public string ARCHIVO { get; set; }
        public string UserMERX { get; set; }

    }
    #endregion

    #region Colas
    public class ColaAforo
    {
        public string QUE_LINEA { get; set; }
        public string QUE_CONSIGNATARIO { get; set; }
        public string AGENCIA { get; set; }
        public string IMPORTADOR { get; set; }
        public string QUE_ESTADO { get; set; }
        public string DESC_ESTADO { get; set; }
        public string QUE_HORA_GENERO { get; set; }
        public string QUE_HORA_INICIO_ATENCION { get; set; }
        public string QUE_HORA_FIN_ATENCION { get; set; }
        public string QUE_EXAMEN_PREVIO { get; set; }
        public string QUE_IEN_ENTRADA { get; set; }
        public string QUE_AGA_NAGENCIA_ADUANAL { get; set; }
        public string QUE_IMPORTADOR { get; set; }
        public string QUE_TIPO_EXAMEN { get; set; }
        public string QUE_TIPO_CLIENTE { get; set; }
        public string QUE_TIPO_PERSONA { get; set; }
        public string QUE_AUTORIZADO { get; set; }
        public string QUE_COD_AGENTE { get; set; }
        public string QUE_USU_USUARIO_GENERO { get; set; }
        public string QUE_TIQUETE { get; set; }
        public string QUE_HORA_TIQUETE { get; set; }
        public string QUE_HORA_INICIO_SC { get; set; }
        public string QUE_TIPO_TIQUETE { get; set; }
        public string AUTORIDAD { get; set; }
        public string QUE_TIPO_COLA { get; set; }
        public string MANIFIESTO { get; set; }
        public string GUIA { get; set; }
        public bool PENDIENTE { get; set; }
        public bool ATENDIDO { get; set; }
        public bool FINALIZADO { get; set; }

    }

    public class ColaHistorico
    {
        public string QUH_IEN_ENTRADA { get; set; }
        public string QUH_HORA_GENERO { get; set; }
        public string QUH_USU_USUARIO_GENERO { get; set; }
        public string USU_GENERO { get; set; }
        public string QUH_HORA_INICIO_ATENCION { get; set; }
        public string QUH_HORA_FIN_ATENCION { get; set; }
        public string QUH_USU_USUARIO_ATENDIO { get; set; }
        public string USU_ATENDIO { get; set; }
        public string MANIFIESTO { get; set; }
        public string GUIA { get; set; }
        public string QUH_CONSIGNATARIO { get; set; }
        public string QUH_AGA_NAGENCIA_ADUANAL { get; set; }
        public string AGENCIA { get; set; }
        public string QUH_TIPO_EXAMEN { get; set; }
        public string DESC_TIP_EXAMEN { get; set; }
        public string QUH_COD_AGENTE { get; set; }
        public string AGENTE { get; set; }
        public string QUH_IMPORTADOR { get; set; }
        public string NOM_IMPORTADOR { get; set; }
    }

    public class AtencionSala
    {
        public string ATS_ID { set; get; }
        public string ATS_TIQUETE { set; get; }
        public string ATS_FCH_IMPRESO { set; get; }
        public string ATS_TIPO_COLA { set; get; }
        public string ATS_ESTADO { set; get; }
        public string DESC_ESTADO { set; get; }
        public string ATS_FCH_ATENDIDO { set; get; }
        public string ATS_USU_ATENDIO { set; get; }
        public string USUARIO_DISPLAY { set; get; }
        public string ATS_PRIORIDAD { set; get; }
        public bool PENDIENTE { get; set; }
        public bool ATENDIDO { get; set; }
        public bool FINALIZADO { get; set; }
        public bool NOPRESENTE { get; set; }

    }

    public class UsuarioAtencionSala
    {
        public string UAS_USU_USUARIO { set; get; }
        public string NOMBRE { set; get; }
        public string USUARIO { set; get; }
        public string UAS_OPCION { set; get; }
        public string DESC_UAS_OPCION { set; get; }
        public string UAS_PRIORIDAD { set; get; }
    }
    #endregion

    public class IndicacionesPosterior
    {
        public string AUM_ID { set; get; }
        public string AUM_FCH_EVENTO { set; get; }
        public string AUM_IEN_ENTRADA { set; get; }
        public string AUM_DETALLE { set; get; }
        public string AUM_FCH_INICIO_PREALERTA { set; get; }
        public string AUM_FCH_REGISTRA { set; get; }
        public string AUM_ESTADO { set; get; }
        public string AUM_USU_REGISTRA { set; get; }
        public string AUM_USU_FINALIZA { set; get; }
        public string AUM_FCH_FINALIZA { set; get; }
    }

    #region Producto NC
    public class ProductoNC
    {
        public string PNC_IDENTIFICADOR { get; set; }
        public string PNC_FECHA { get; set; }
        public string PNC_ESTADO { get; set; }
        public string PNC_SISTEMA { get; set; }
        public string pnC_consecutivo { get; set; }
        public string PNC_DEPARTAMENTO { get; set; }
        public string PNC_CANT_TARIMAS { get; set; }
        public string PNC_TARIMAS_NC { get; set; }
        public string PNC_DESCRIPCION_NC { get; set; }
        public string PNC_CLIENTE { get; set; }
        public string PNC_TIPO_HALLAZGO { get; set; }
        public string PNC_HALLADO_POR { get; set; }
        public string PNC_EVENTO_HALLAZGO { get; set; }
        public string PNC_OTRO_EVENTO { get; set; }
        public string PNC_DECISION_CLIENTE { get; set; }
        public string PNC_DESC_OTRO_DECISION { get; set; }
        public string PNC_REPRESENTANTE { get; set; }
        public string PNC_USU_REGISTRA { get; set; }
        public string PNC_FCH_REGISTRA { get; set; }
        public string PNC_INVESTIGACION { get; set; }
        public string PNC_TIPO_PROBLEMA { get; set; }
        public string PNC_EVID_MANIFIESTO { get; set; }
        public string PNC_EVID_FACTURA { get; set; }
        public string PNC_EVID_VIDEO { get; set; }
        public string PNC_EVID_FOTOS { get; set; }
        public string PNC_OTRO { get; set; }
        public string PNC_DESCRIP_ACCION { get; set; }
        public string PNC_RESPONSABLE { get; set; }
        public string PNC_FCH_LIMITE { get; set; }
        public string PNC_EMAIL_NOTIF { get; set; }
        public string PNC_USU_CALIDAD { get; set; }
        public string PNC_FCH_CALIDAD { get; set; }
        public string PNC_USU_INVESTIGA { get; set; }
        public string PNC_FCH_INVESTIGA { get; set; }
        public string PNC_USU_REVISION { get; set; }
        public string PNC_FCH_REVISION { get; set; }
        public string PNC_USU_APRUEBA { get; set; }
        public string PNC_FCH_APRUEBA { get; set; }
        public string PNC_AGENCIA { get; set; }
        public string PNC_CAUSA { get; set; }
        public string PNC_EVID_EXAMEN { get; set; }
        public string PNC_OGU_GUIA { get; set; }
        public string PNC_OBSERVACIONES { get; set; }
        public string PNC_UBICACION { get; set; }
        public string PNC_CATEGORIA { get; set; }
        public string GUIA { get; set; }
    }

    public class pnc_ubicacion
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class pnc_sistema
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class pnc_categoria
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class pnc_departamentos
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class pnc_tipo_problema
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class pnc_detectadoPor
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class pnc_evento_hallazgo
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class pnc_decision_cliente
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class pnc_consecutivo
    {
        public string GuiaOriginal { set; get; }
        public string Importador { set; get; }
    }
    #endregion

    public class CONSIGNATARIO
    {
        public string CNS_CEDULA { set; get; }
        public string CNS_NOMBRE { set; get; }
        public string CNS_USU_REGISTRO { set; get; }
        public string CNS_TIPO_CEDULA { set; get; }
        public string DESC_TIPO_CED { set; get; }
        public string CNS_FCH_REGISTRO { set; get; }
        public string CNS_ESTADO { set; get; }
    }
    public class REGCONSIGNATARIO
    {
        public string IEN_ENTRADA { set; get; }
        public string IEN_GUIA_ORIGINAL { set; get; }
        public string IEN_IMP_CEDULA { set; get; }
        public string IEN_IMPORTADOR { set; get; }
    }

    public class Permisos
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class tarimas
    {
        public string TNC_TARIMA { set; get; }
        public string TNC_BULTOS { set; get; }
        public string TNC_BULTOS_NC { set; get; }
        public string TNC_PNC_IDENTIFICADOR { set; get; }
        public string TNC_SECUENCIA { set; get; }
    }

    public class descripcionPNC
    {
        public string VNC_PNC_IDENTIFICADOR { set; get; }
        public string VNC_SECUENCIA { set; get; }
        public string VNC_FECHA { set; get; }
        public string VNC_DESCRIPCION { set; get; }
        public string VNC_USU_REGISTRA { set; get; }
        public string VNC_FCH_REGISTRA { set; get; }
    }

    public class Diferencia
    {
        public string DTF_ID { set; get; }
        public string DTF_MER_ID_MERCANCIA { set; get; }
        public string CONSECUTIVO { set; get; }
        public string DTF_BULTOS { set; get; }
        public string CONSIGNATARIO { set; get; }
        public string DTF_TIPO_DIFERENCIA { set; get; }
        public string DTF_LOCALIZACION { set; get; }
        public string DTF_ESTADO { set; get; }
        public string DESC_ESTADO { set; get; }
        public string DTF_ID_CAUSA { set; get; }
        public string DESC_CAUSA { set; get; }
        public string DTF_SOLUCION { set; get; }
        public string INVENTARIO { set; get; }
        public string UserMERX { set; get; }
    }

    public class Inventario
    {
        public string ITF_INVENTARIO { set; get; }
        public string INVENTARIO { set; get; }
    }

    public class TipoDiferencia
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class Causa
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class MonitoreoAnden
    {
        public string UT_ESPERA { set; get; }
        public string CIA_POSICION { set; get; }
        public string CIA_TIQUETE { set; get; }
        public string CIA_PLACA { set; get; }
        public string DESC_AGENCIA { set; get; }
        public string CIA_HORA_LLEGADA { set; get; }
        public string TIEMPO_ATENCION { set; get; }
    }

    #region Solicitud video

    public class TipoSolicitud
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class siv_sistema
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class siv_solicita
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }

    public class SolicitudVideo
    {
        public string SIV_TIPO { set; get; }
        public string DESC_TIPO { set; get; }
        public string SIV_SISTEMA { set; get; }
        public string SIV_IEN_ENTRADA { set; get; }
        public string SIV_PRIORIDAD { set; get; }
        public string SIV_VERIF_RECEPCION { set; get; }
        public string SIV_VERIF_DESPALET { set; get; }
        public string SIV_VERIF_CHEQUEO { set; get; }
        public string SIV_VERIF_AFORO { set; get; }
        public string SIV_VERIF_REFRIG { set; get; }
        public string SIV_VERIF_COURIER { set; get; }
        public string SIV_VERIF_RX { set; get; }
        public string SIV_VERIF_DESPACH { set; get; }
        public string SIV_VERIF_OTRO { set; get; }
        public string SIV_VERIF_DESCRP_OTRO { set; get; }
        public string SIV_OBSERVACIONES { set; get; }
        public string SIV_SOLICITA { set; get; }
        public string SIV_NOMBRE_SOLICITA { set; get; }
        public string SIV_USU_REGISTRA { set; get; }
        public string SIV_FCH_REGISTRA { set; get; }
        public string SIV_FCH_FINALIZA { set; get; }
        public string CONSIGNATARIO { set; get; }
        public string GUIA_MASTER { set; get; }
        public string GUIA_HIJA { set; get; }
        public string SIV_SOLICITUD { set; get; }
        public string AER_NOMBRE { set; get; }

    }

    #endregion

    #region AgenciaAduanal y agentes

    public class AgenciaAduanal
    {
        public string AGA_NAGENCIA_ADUANAL { set; get; }
        public string AGA_AGENCIA_ADUANAL { set; get; }
        public string AGA_NOMBRE { set; get; }
        public string AGA_EST_ESTADO { set; get; }
        public string AGA_CEDULA { set; get; }
        public string AGA_USUARIO { set; get; }
        public string AGA_RECIBE_ESTADOS { set; get; }
        public string AGA_TELEFONO1 { set; get; }
        public string AGA_TELEFONO2 { set; get; }
        public string AGA_TELEFONO_FAX { set; get; }
        public string AGA_DIRECCION { set; get; }
        public string AGA_APARTADO { set; get; }
        public string AGA_EMAIL { set; get; }
        public string AGA_FECHA_INGRESO { set; get; }
        public string USER_MERX { set; get; }
    }
    public class AgenteAduana
    {
        public long TAA_AGA_NAGENCIA { set; get; }
        public long TAA_CEDULA { set; get; }
        public string TAA_NOMBRE { set; get; }
        public string TAA_TIPO_AGENTE { set; get; }
        public string TAA_DIRECCION { set; get; }
        public string TAA_TELEFONO { set; get; }
        public long TAA_CODIGO { set; get; }
        public string TAA_ESTADO { set; get; }
        public string TAA_MOTIVO { set; get; }
        public string TAA_FCH_ACTUALIZA { set; get; }
        public string TAA_USU_ACTUALIZA { set; get; }
        public string USER_MERX { set; get; }
    }
    #endregion

    public class Cesiones
    {
        public string MER_ID_MERCANCIA { set; get; }
        public string MER_IEN_ENTRADA { set; get; }
        public string MER_OPERACION_TICA { set; get; }
        public string DESC_OPER_TICA { set; get; }
        public string MER_MOV_INVENTARIO { set; get; }
        public string MER_BULTOS_DESPALETIZADOS { set; get; }
        public string MER_BULTOS_SALIDOS { set; get; }
        public string KILOS_DESPALETIZADOS { set; get; }
        public string MER_KILOS_SALIDOS { set; get; }
        public string MER_ID_MERCANCIA_REF { set; get; }
        public string MER_BULTOS_REF { set; get; }
        public string MER_ESTADO { set; get; }
        public string DESC_ESTADO { set; get; }
    }

    public class EntradasCES
    {
        public string IEN_ENTRADA { set; get; }
        public string IEN_MANIFIESTO { set; get; }
        public string IEN_GUIA { set; get; }
        public string IEN_GUIA_ORIGINAL { set; get; }
        public string IEN_FCH_INGRESO { set; get; }
        public string IEN_CONSIGNATARIO { set; get; }
        public string IEN_NUMERO_VIAJE { set; get; }

    }
    #region INGRESO MERCANCIA

    public class Mov_Fracciona
    {
        public string MER_MOV_INVENTARIO { set; get; }
        public string BL { set; get; }
        public string OPERACION { set; get; }
        public string MER_ID_MERCANCIA { set; get; }
        public string MER_BULTOS_DESPALETIZADOS { set; get; }
        public string MER_KILOS_BRUTO { set; get; }
        public string MOV_REF { set; get; }
        public string BLT_REF { set; get; }

    }

    public class Tipo_contenedor
    {
        public string Id { set; get; }
        public string Nombre { set; get; }

    }
    public class Modalidad_Transporte
    {
        public string Id { set; get; }
        public string Nombre { set; get; }

    }
    public class Tamano
    {
        public string Id { set; get; }
        public string Nombre { set; get; }

    }
    public class Clientes
    {
        public string Id { set; get; }
        public string Nombre { set; get; }

    }

    public class CLIENTES_FILTER
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }
    public class Funcionarios
    {
        public string Id { set; get; }
        public string Nombre { set; get; }

    }
    public class Viajes
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
        public string MVI_USU_REGISTRO { set; get; }
    }
    public class Duas
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
        public string MDU_USU_REGISTRO { set; get; }
    }
    public class Ubicaciones
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }
    public class Guia
    {
        public string IEN_TMA_ID_MANIFIESTO { set; get; }
        public string IEN_PUESTO_CHEQUEO { set; get; }
        public string IEN_ENTRADA { set; get; }
        public string IEN_GUIA_ORIGINAL { set; get; }
        public string IEN_GUIA { set; get; }
        public string IEN_GUIA_MASTER { set; get; }
        public string IEN_DIN_TIPO_DOCUMENTO { set; get; }
        public string IEN_DUA { set; get; }
        public string IEN_DUA_Z { set; get; }
        public string IEN_DUA_R { set; get; }
        public string IEN_NUMERO_VIAJE { set; get; }
        public string IEN_ANTECESOR { set; get; }
        public string IEN_IMP_CEDULA { set; get; }
        public string IEN_CONSIGNATARIO { set; get; }
        public string IEN_CON_NCONSOLIDADOR { set; get; }
        public string CONSOLIDADOR { set; get; }
        public string IEN_BULTOS_MANIFESTADOS { set; get; }
        public string IEN_KILOS_MANIFESTADOS { set; get; }
        public string IEN_VOLUMEN_MANIF { set; get; }
        public string IEN_BULTOS_INGRESADOS { set; get; }
        public string IEN_KILOS_INGRESADOS { set; get; }
        public string IEN_VOLUMEN_INGRESADO { set; get; }
        public string IEN_DESCRIPCION_MANIF { set; get; }
        public string IEN_OBSERVACIONES { set; get; }
        public string IEN_UBICACION_INGRESO { set; get; }
        public string IEN_UBICACION_TRANSITO { set; get; }
        public string IEN_ES_COURIER { set; get; }
        public string IEN_DUDA_REFRI { set; get; }
        public string IEN_TIPO_DUDA_REF { set; get; }
        public string IEN_ES_FARMA { set; get; }
        public string IEN_CLASE { set; get; }
        public string IEN_BOLETA_CLASE { set; get; }
        public string IEN_ACTA_DESCARGA { set; get; }
        public string IEN_PEND_DESCONSOLIDAR { set; get; }
        public string IEN_SOLICITUD { set; get; }
        public string IEN_TEXTO_ACTA { set; get; }
        public string IEN_BOD_NBODEGA { set; get; }
        public string IEN_USU_DIGITO { set; get; }
        public string IEN_MANIFIESTO { set; get; }
        public string IEN_IMPORTADOR { set; get; }
        public string IEN_FCH_DIGITACION { set; get; }
        public string IEN_ESTADO { set; get; }
        public string TOT_BULTOS { get; set; }
        public string TOT_KILOS { get; set; }
        public string TOT_VOLUMEN { get; set; }
        public string CANT_TARIMAS { get; set; }
        public string CONTENEDOR { get; set; }
        public string TOT_BULTOS_Z { get; set; }
        public string TOT_KILOS_Z { get; set; }
        public string IEN_MANIFIESTO_Z { set; get; }
        public string IEN_GUIA_ORIGINAL_Z { set; get; }
        public string IEN_LINEA_GUIA { set; get; }
        

    }
    public class tipoMercancias
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }
    public class tipoAveria
    {
        public string Id { set; get; }
        public string Nombre { set; get; }
    }
    public class Tarimas_Mercancia
    {
        public string TARIMA { set; get; }
        public string BULTOS_TARIMA { set; get; }
        public string KILOS_BRUTO { set; get; }
        public string KILOS_NETO { set; get; }
        public string KILOS_TARIMA { set; get; }
        public string MER_ESTADO { set; get; }
        public string MER_DESCRIPCION { set; get; }
        public string DESAVERIAS { set; get; }
        public string DES_MERCANCIA { set; get; }
        public string MER_OPERACION_TICA { set; get; }
        public string DES_OPER_TICA { set; get; }
        public string MER_ID_MERCANCIA_REF { set; get; }
        public bool ABIERTA { set; get; }
    }

    public class Mercancia1
    {
        public string MER_USUARIO { get; set; }
        public string IEN_USU_DIGITO { get; set; }
        public string MER_PUESTO_CHEQUEO { get; set; }
        public string MER_BULTOS_DESPALETIZADOS { get; set; }
        public string MER_KILOS_BRUTO { get; set; }
        public string MER_KILOS_TARIMA { get; set; }
        public string MER_KILOS_NETO { get; set; }
        public string MER_TME_MERCANCIA_input { get; set; }
        public string MER_TME_MERCANCIA { get; set; }
        public string DES_MERCANCIA { get; set; }
        public string MER_TEM_EMBALAJE_input { get; set; }
        public string MER_TEM_EMBALAJE { get; set; }
        public string MER_AVE_AVERIA { get; set; }
        public string MER_TIP_CODIGO_IATA_input { get; set; }
        public string MER_CODIGO_UN { get; set; }
        public string MER_INDICE_TRANS { get; set; }
        public string MER_DESCRIPCION { get; set; }
        public string MER_MARCAS { get; set; }
        public string MER_LINEA_GUIA { get; set; }
        public string MER_OPERACION_TICA { get; set; }
        public string MER_OBSERVACIONES { get; set; }
        public string MER_ID_MERCANCIA_REF { get; set; }
        public string MER_BULTOS_REF { get; set; }
        public string MER_LOC_RACK { get; set; }
        public string MER_LOC_COLUMNA { get; set; }
        public string MER_LOC_LADO { get; set; }
        public string MER_LOC_ALTURA { get; set; }
        public string IEN_DUDA_REFRI { get; set; }
        public string MER_ESTADO { get; set; }
        public string MER_ID_MERCANCIA { get; set; }
        public string IEN_ENTRADA { get; set; }
        public string MER_LARGO { get; set; }
        public string MER_ALTO { get; set; }
        public string MER_ANCHO { get; set; }
        public string MER_TIP_CODIGO_IATA { get; set; }
        public string MER_PERECEDERO { get; set; }
        public string MER_REFRIGERADO { get; set; }
        public string MER_ARMAS { get; set; }
        public string MER_ACCION { get; set; }
        public string MER_MANIFIESTO { get; set; }
        public string MER_GUIA { get; set; }
        public string DES_ESTADO { get; set; }
        public string ESTADO { get; set; }
        public string MER_ETIQUETA { get; set; }
        public string MER_MOV_REF { get; set; }
        
    }
    public class Desconsolidacion1
    {
        public string DSC_ID { get; set; }
        public string DSC_IEN_ENTRADA { get; set; }
        public string IEN_GUIA_ORIGINAL { get; set; }
        public string CON_NOMBRE { get; set; }
        public string DSC_FCH_REGISTRA { get; set; }
        public string DSC_USU_REGISTRA { get; set; }
        public string USUARIO { get; set; }
        public string DSC_ARCHIVO { get; set; }
    }

    public class InfoConse
    {
        public string IEN_GUIA_MASTER { get; set; }
        public string IEN_GUIA_ORIGINAL { get; set; }
        public string IEN_IMPORTADOR { get; set; }
        public string IEN_AER_NAREOLINEA { get; set; }
        public string AER_NOMBRE { get; set; }
    }

    #endregion
}

