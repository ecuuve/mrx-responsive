﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNSI.Models
{
    public class RM_Registro_Muestras_Input
    {
        public string s_boleta { get; set; }
        public string s_consecutivo { get; set; }
        public string s_manifiesto { get; set; }
        public string s_guia { get; set; }
    }

    public class RM_Registro_Muestras
    {
        public string MUE_FECHA { get; set; }
        public string MUE_BOLETA { get; set; }
        public string MUE_IEN_ENTRADA { get; set; }
        public string MUE_MANIFIESTO { get; set; }
        public string MUE_GUIA { get; set; }
        public string MUE_CONSIGNATARIO { get; set; }
        public string MUE_FCH_MUESTRA { get; set; }
        public string BULTOS { get; set; }
        public string KILOS { get; set; }
        public string MUE_TIPO_MUESTRA { get; set; }
        public string DESC_TIPO_MUE { get; set; }
        public string MUE_DUA { get; set; }
        public string MUE_CODIGO_MOTIVO { get; set; }
        public string MUE_MOTIVO { get; set; }
        public string MUE_AGA_NAGENCIA { get; set; }
        public string NOMB_AGENCIA { get; set; }
        public string MUE_JEFE_AFORADOR { get; set; }
        public string MUE_AFORADOR { get; set; }
        public string MUE_RESPONS_ALMACEN { get; set; }
        public string MUE_LOC_RACK { get; set; }
        public string MUE_LOC_COLUMNA { get; set; }
        public string MUE_LOC_ALTURA { get; set; }
        public string MUE_LOC_LADO { get; set; }
        public string MUE_FCH_DEVOLUCION { get; set; }
        public string MUE_FUNCIONARIO_DEVOLVIO { get; set; }
        public string MUE_FUNC_RECIBE { get; set; }
        public string MUE_USU_USUARIO { get; set; }
    }



    public class RM_TipoMuestra
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class RM_Motivo
    {
        public string Id { get; set; }

        public string Nombre { get; set; }
    }

    public class RM_Consecutivo_input
    {
        public string consecutivo { get; set; }
    }

    public class RM_Consecutivo
    {
        public string MUE_MANIFIESTO { get; set; }
        public string MUE_GUIA { get; set; }
        public string MUE_CONSIGNATARIO { get; set; }
        public string TOTBULTOS { get; set; }
        public string TOTKILOS { get; set; }
    }

    public class RM_Detalle_input
    {
        public string boleta { get; set; }
    }

    public class RM_Detalle
    {
        public string BOLETA { get; set; }
        public string ITEM { get; set; }
        public string CANTIDAD { get; set; }
        public string DESCRIPCION { get; set; }
        public string KILOS { get; set; }
        public string CANTIDAD_DEV { get; set; }
        public string KILOS_DEV { get; set; }
    }

    public class RM_Muestras_input
    {
        public string MUE_BOLETA { get; set; }
        public string MUE_FCH_MUESTRA { get; set; }
        public string MUE_IEN_ENTRADA { get; set; }
        public string MUE_TIPO_MUESTRA { get; set; }
        public string MUE_DUA { get; set; }
        public string MUE_CODIGO_MOTIVO { get; set; }
        public string MUE_MOTIVO { get; set; }
        public string MUE_AGA_NAGENCIA { get; set; }
        public string MUE_RESPONS_ALMACEN { get; set; }
        public string MUE_AFORADOR { get; set; }
        public string MUE_JEFE_AFORADOR { get; set; }
        public string MUE_LOC_RACK { get; set; }
        public string MUE_LOC_COLUMNA { get; set; }
        public string MUE_LOC_ALTURA { get; set; }
        public string MUE_LOC_LADO { get; set; }
        public string MUE_FCH_DEVOLUCION { get; set; }
        public string MUE_FUNCIONARIO_DEVOLVIO { get; set; }
        public string MUE_FUNC_RECIBE { get; set; }
        public string MUE_GUIA { get; set; }
        public string MUE_MANIFIESTO { get; set; }
        public string MUE_CONSIGNATARIO { get; set; }      
        public string UserMERX { get; set; }
        
    }

    public class RM_DetalleMuestras_input
    {
        public string boleta { get; set; }
        public string DMU_ITEM { get; set; }
        public string DMU_CANTIDAD { get; set; }
        public string DMU_KILOS { get; set; }
        public string DMU_DESCRIPCION { get; set; }
        public string DMU_CANTIDAD_DEV { get; set; }
        public string DMU_KILOS_DEV { get; set; }

    }

    public class TX_REGISTRO_MUESTRAS
    {
    }
}