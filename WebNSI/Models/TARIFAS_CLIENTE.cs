﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNSI.Models
{
    public class TARIFAS_CLIENTE
    {
        public string TRC_CLIENTE_CONTENEDOR { get; set; }
        public string CLIENTE_CONTENEDOR { get; set; }
        public string TRC_TIPO_CLIENTE_CONTENEDOR { get; set; }
        public string TRC_CLIENTE_GUIA { get; set; }
        public string CLIENTE_GUIA { get; set; }
        public string TRC_TRF_ID { get; set; }
        public string TRC_USU_REGISTRO { get; set; }
        public string TRC_FCH_REGISTRO { get; set; }
    }

    public class TARIFAS_CLIENTE_POST
    {
        public string TRC_CLIENTE_CONTENEDOR { get; set; }
        public string TRC_CLIENTE_CONTENEDOR_TIPO { get; set; }
        public string TRC_CLIENTE_CONTENEDOR_NOMBRE { get; set; }
        public string TRC_CLIENTE_GUIA { get; set; }
        public string TRC_CLIENTE_GUIA_TIPO { get; set; }
        public string TRC_CLIENTE_GUIA_NOMBRE { get; set; }
        public string TRC_TRF_ID { get; set; }
        public string TRC_USU_REGISTRO { get; set; }
    }

    public class TARIFAS_CLIENTE_BUSQUEDA
    {
        public string B_TRC_TRF_ID { get; set; }
        public string B_TRC_CLIENTE_CONTENEDOR { get; set; }
        public string B_TRC_CLIENTE_GUIA { get; set; }
    }

    public class CONSOLIDADORES_POST
    {
        public string B_CONSOLIDADOR { get; set; }
        public string B_NOMBRE { get; set; }
    }

    public class CONSOLIDADORES
    {
        public string ID { get; set; }
        public string NOMBRE { get; set; }
        public string CEDULA { get; set; }
    }
}