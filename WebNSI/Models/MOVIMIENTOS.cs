﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNSI.Models
{
    public class MOVS_ENTRADA
    {
        public string MOV_ANNO { get; set; }
        public string MOV_MOVIMIENTO { get; set; }
        public string MOV_FCH_INGRESO { get; set; }
        public string MOV_DUA { get; set; }
        public string MOV_CONOCIMIENTO { get; set; }
        public string MOV_CONSIGNATARIO { get; set; }
        public string MOV_BLTS_INGRESADOS { get; set; }
        public string MOV_BLTS_EXISTENTES { get; set; }
        public string MOV_BLTS_DISPONIBLES { get; set; }
        public string MOV_PESO { get; set; }
        public string MOV_VOL_INGRESADO { get; set; }
        public string MOV_VOL_DISPONIBLE { get; set; }
        public string MOV_EMBALAJE { get; set; }
        public string MOV_VIAJE { get; set; }
        public string MOV_DESCRIPCION { get; set; }
        public string MOV_OBSERVACIONES { get; set; }
        public string MOV_CONSOLIDADOR { get; set; }
        public string MOV_ESTADO { get; set; }
        public string MOV_TPO_CONTENEDOR { get; set; }
        public string MOV_PALETIZADO { get; set; }
        public string MOV_A_PISO { get; set; }

    }

  }