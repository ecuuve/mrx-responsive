﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNSI.Models
{


    #region NUEVO TARIFARIO

    public class TARIFA_CLIENTE_BUSQUEDA
    {
        public string B_TTC_ID { get; set; }
        public string B_TTC_NOMBRE_CLIENTE { get; set; }
    }

    public class TARIFA_CLIENTE
    {
        public string TTC_ID { get; set; }
        public string TTC_TIPO_CLIENTE { get; set; }
        public string TTC_NOMBRE_CLIENTE { get; set; }
        public string TTC_CLIENTE_ID { get; set; }
        public string TTC_USU_REGISTRO { get; set; }
        public string TTC_FCH_REGISTRO { get; set; }
    }

    public class TARIFA_CONSOLID_CONSIG
    {
        public string TCC_ID { get; set; }
        public string TCC_TTC_ID { get; set; }
        public string TTC_CON_NCONSOLIDADOR { get; set; }
        public string TTC_IMP_CEDULA { get; set; }
        public string TTC_USU_REGISTRA { get; set; }
        public string TTC_FCH_REGISTRA { get; set; }
        public string CONSOLIDADOR { get; set; }
        public string IMPORTADOR { get; set; }

    }


    public class LV_TARIFARIO
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        
    }



    #endregion

    #region JAFET
    public class MONEDA
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class TIPO_PARCIAL
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class TARIFA
    {
        public long TRF_ID { get; set; }
        public string TRF_NOMBRE { get; set; }
        public string TRF_CONT_COMPLETO { get; set; }
        public long TRF_CONT_TAMANO { get; set; }
        public string TRF_A_PISO { get; set; }
        public long TRF_DIAS_MINIMO { get; set; }
        public decimal TRF_MONTO_DIAS_MINIMO { get; set; }
        public string TRF_MONEDA_DIAS_MINIMO { get; set; }
        public decimal TRF_DIAS_ADICIONALES { get; set; }
        public string TRF_MONEDA_DIAS_ADICIONALES { get; set; }
        public string TRF_TIPO_PARCIAL { get; set; }
        public decimal TRF_MONTO_PARCIAL { get; set; }
        public string TRF_MONEDA_MONTO_PARCIAL { get; set; }
        public string TRF_ES_DUT { get; set; }
        public string TRF_POR_MOVIMIENTO { get; set; }
        public string TRF_USU_REGISTRO { get; set; }
        public string TRF_FCH_REGISTRO { get; set; }
    }

    public class TARIFA_POST
    {
        public string TRF_ID { get; set; }
        public string TRF_NOMBRE { get; set; }
        public string TRF_CONT_COMPLETO { get; set; }
        public string TRF_CONT_TAMANO { get; set; }
        public string TRF_A_PISO { get; set; }
        public string TRF_DIAS_MINIMO { get; set; }
        public string TRF_MONTO_DIAS_MINIMO { get; set; }
        public string TRF_MONEDA_DIAS_MINIMO { get; set; }
        public string TRF_DIAS_ADICIONALES { get; set; }
        public string TRF_MONEDA_DIAS_ADICIONALES { get; set; }
        public string TRF_TIPO_PARCIAL { get; set; }
        public string TRF_MONTO_PARCIAL { get; set; }
        public string TRF_MONEDA_MONTO_PARCIAL { get; set; }
        public string TRF_ES_DUT { get; set; }
        public string TRF_POR_MOVIMIENTO { get; set; }
        public string TRF_USU_REGISTRO { get; set; }
    }

    public class TARIFA_BUSQUEDA
    {
        public string B_TRF_ID { get; set; }
        public string B_TRF_NOMBRE { get; set; }
    }
    #endregion


}