﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNSI.Models
{
    public class CONCEPTOS_TARIFA
    {
        public string CTR_TRF_ID { get; set; }
        public string CTR_CCB_CONCEPTO { get; set; }
        public decimal CTR_MONTO_FIJO { get; set; }
        public string CTR_MONEDA_MONTO_FIJO { get; set; }
        public decimal CTR_PCT_PRORRATEO { get; set; }
        public decimal CTR_CIF_MINIMO { get; set; }
        public decimal CTR_MONTO_MINIMO { get; set; }
        public string CTR_MONEDA_MONTO_MINIMO { get; set; }
    }

    public class CONCEPTOS_TARIFA_POST
    {
        public string CTR_CCB_CONCEPTO { get; set; }
        public decimal CTR_MONTO_FIJO { get; set; }
        public string CTR_MONEDA_MONTO_FIJO { get; set; }
        public decimal CTR_PCT_PRORRATEO { get; set; }
        public decimal CTR_CIF_MINIMO { get; set; }
        public decimal CTR_MONTO_MINIMO { get; set; }
        public string CTR_MONEDA_MONTO_MINIMO { get; set; }
        public int CTR_TRF_ID { get; set; }
    }

    public class CONCEPTOS_TARIFA_BUSQUEDA
    {
        public string B_CTR_CCB_CONCEPTO { get; set; }
        public string B_CTR_TRF_ID { get; set; }
    }
}