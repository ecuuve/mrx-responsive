﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNSI.Models
{

    public class LV_Facturacion
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class FAC_ENCABEZADO
    {
        public string FAC_TIPO_TIQUETE { get; set; }
        public long FAC_TIQUETE { get; set; }
        public string FAC_FCH_TIQUETE { get; set; }
        public string FAC_FCH_ATENDIO { get; set; }
        public string FAC_DUA_SALIDA { get; set; }
        public string FAC_VIAJE_TRANSITO { get; set; }
        public string FAC_FCH_DUA { get; set; }
        public long FAC_BULTOS_DUA { get; set; }
        public string FAC_DIN_TIPO_DOCUMENTO { get; set; }
        public decimal FAC_VALOR_CIF { get; set; }
        public decimal FAC_IMPUESTOS { get; set; }
        public decimal FAC_IMP_EXENTOS { get; set; }
        public string FAC_EXENTA { get; set; }
        public string FAC_NOTA_EXENCION { get; set; }
        public string FAC_DESDE_EXENCION { get; set; }
        public string FAC_HASTA_EXENCION { get; set; }
        public string FAC_UBI_UBICACION { get; set; }
        public string FAC_IMP_CEDULA { get; set; }
        public string FAC_IMPORTADOR { get; set; }
        public string FAC_MARCH_ELECTRONICO { get; set; }
        public string FAC_TRAMITE { get; set; }
        public string FAC_CLIENTE_DUA { get; set; }
        public decimal FAC_TIPO_CAMBIO { get; set; }
        public string FAC_DCT_DECLARANTE_ID { get; set; }
        public long FAC_ADC_AGENTE_ID { get; set; }
        public string FAC_CONTADO { get; set; }
        public long FAC_CUENTA { get; set; }
        public string FAC_CONTRIBUYENTE { get; set; }
        public string FAC_ESTADO { get; set; }
        public string FAC_USU_DIGITO { get; set; }
        public long FAC_FACTURA { get; set; }
        public decimal FAC_TOTAL_FACTURA { get; set; }
        public string FAC_ELECTRONICA { get; set; }       
        public string FAC_NOMBRE_CUENTA { get; set; }
        public string FAC_NOMBRE_CHOFER { get; set; }
        public string FAC_CEDULA_CHOFER { get; set; }
        public string FAC_PLACA_VEHICULO { get; set; }
        public string FAC_MARCHAMO1 { get; set; }
        public string FAC_MARCHAMO2 { get; set; }
        public string FAC_MARCHAMO3 { get; set; }
        public string FAC_MARCHAMO4 { get; set; }
        public string FAC_OBSERVACIONES { get; set; }
        public string FAC_NOMBRE_FACTURA { get; set; }
        public string FAC_CEDULA_FACTURA { get; set; }
        public string FAC_TELEFONO { get; set; }
        public string FAC_TRA_CHOFER { get; set; }
        public string FAC_CFR_FACTURA { get; set; }
        public string FAC_FCH_DIGITO { get; set; }
        public string FAC_NBR_CONTRIBUYENTE { get; set; }
        public string TAA_NOMBRE { get; set; }
        public string DCT_NOMBRE { get; set; }
        public string TDO_DESCRIPCION { get; set; }
        public string FAC_AUTORIZADO_POR { get; set; }
        public string CONTADO { get; set; }

        //Campos en BD que no se utilizan en la applicación

        //public string FAC_PAGA_BODEGAJE { get; set; }
        //public long FAC_AGA_NAGENCIA_ADUANAL { get; set; }
        //public long FAC_CAJ_CAJA { get; set; }
        //public string FAC_REFERENCIA { get; set; }
        //public long FAC_BULTOS_SALIDOS { get; set; }
        //public long FAC_SALDO_BULTOS { get; set; }
        //public string FAC_AUTORIZO_DESCU { get; set; }
        //public string FAC_AUTORIZO_TARIFA { get; set; }
        //public string FAC_AUTORIZO_BODEGAJE { get; set; }
        //public decimal FAC_EXENTO { get; set; }
        //public string FAC_FCH_DESPACHO { get; set; }
        //public string FAC_USU_DESPACHO { get; set; }
        //public string FAC_USU_MODIFICO { get; set; }
        //public string FAC_FCH_MODIFICO { get; set; }
        //public string FAC_USU_ANULO { get; set; }
        //public string FAC_FCH_ANULO { get; set; }
        //public long FAC_MOT_MOTIVO_ANULA { get; set; }
        //public long FAC_MONTO_OTROS { get; set; }
        //public long FAC_MONTO_DOC { get; set; }
        //public long FAC_MONTO_TRA { get; set; }
        //public string FAC_FCH_TRANSMISION { get; set; }
        //public string FAC_USU_TRANSMISION { get; set; }
        //public string FAC_ARCH_TRANSMISION { get; set; }
        //public string FAC_MENS_ERROR { get; set; }
        //public decimal FAC_MONTO_ASOCIE { get; set; }
        //public decimal FAC_MONTO_EXENCION { get; set; }
        //public string FAC_EMAIL_IMPORTADOR_P { get; set; }
        //public string FAC_CEDULA_CONTRIB { get; set; }
        //public decimal FAC_ENDOSO { get; set; }
        //public decimal FAC_CESION { get; set; }
        //public decimal FAC_APERTURA_ADU { get; set; }
        //public decimal FAC_MULTA { get; set; }
        //public long FAC_PCT_DESC_ALMAC { get; set; }
        //public decimal FAC_DESC_ALMAC { get; set; }
        //public long FAC_PCT_DESC_MANEJ { get; set; }
        //public decimal FAC_DESC_MANEJ { get; set; }
        //public long FAC_PCT_DESC_SEGUR { get; set; }
        //public decimal FAC_DESC_SEGUR_P { get; set; }
        //public string FAC_ANTICIPADA { get; set; }
        //public decimal FAC_IVA { get; set; }
        //public long FAC_BULTOS_TOTALES { get; set; }
        //public decimal FAC_KILOS_TOTALES { get; set; }
        //public decimal FAC_ALMACENAJE_TOTAL { get; set; }
        //public decimal FAC_MANEJO_TOTAL { get; set; }
        //public decimal FAC_DESCUENTO_TOTAL { get; set; }
        //public decimal FAC_SEGURO_TOTAL { get; set; }
        //public decimal FAC_IMP_VENTAS_TOTAL { get; set; }
        //public string FAC_CFR_TELEFONO { get; set; }
    }

    public class PARAM_Cuentas
    {
        public string tipo_p { get; set; }
        public string nombre_p { get; set; }
        public string cuenta_p { get; set; }
        public string forma_pago_p { get; set; }
        public string tipo_cta_p { get; set; }
        public string importador_p { get; set; }
    }

    public class FAC_MOVIMIENTO
    {
        public string FMV_ID { get; set; }
        public long FMV_FAC_FACTURA { get; set; }
        public long FMV_MOVIMIENTO { get; set; }
        public long FMV_IEN_ENTRADA { get; set; }
        public long FMV_BULTOS { get; set; }
        public decimal FMV_KILOS { get; set; }
        public decimal FMV_VOLUMEN { get; set; }
        public long FMV_TME_MERCANCIA { get; set; }
        public string TME_DESCRIPCION { get; set; }       
        public long FMV_ITT_TARIFA { get; set; }
        public string ITT_DESCRIPCION { get; set; }       
        public string FMV_BASE_DESCUENTO { get; set; }
        public decimal FMV_PCT_DESCUENTO { get; set; }
        public decimal FMV_MONTO_DESCUENTO { get; set; }
        public string FMV_ASOCIE { get; set; }
        public decimal FMV_MONTO_A_PAGAR { get; set; }
        public string FMV_USU_REGISTRO { get; set; }
        public string FMV_FCH_REGISTRO { get; set; }
        public decimal FMV_ALMACENAJE { get; set; }
        public decimal FMV_MANEJO { get; set; }
        public decimal FMV_SEGURO { get; set; }
        public decimal FMV_PCT_DESC_ALMAC { get; set; }
        public decimal FMV_DESC_ALMAC { get; set; }
        public decimal FMV_PCT_DESC_MANEJ { get; set; }
        public decimal FMV_DESC_MANEJ { get; set; }
        public decimal FMV_PCT_DESC_SEGUR { get; set; }
        public decimal FMV_DESC_SEGUR { get; set; }
        public decimal FMV_IVA_ALMACE { get; set; }
        public decimal FMV_IVA_MANEJO { get; set; }
        public decimal FMV_IVA_SEGURO { get; set; }
        public decimal FMV_IVA_ASOCIE { get; set; }
        public decimal FMV_TOTAL_IVA { get; set; }
        public decimal FMV_MONTO_ASOCIE { get; set; }
        public decimal FMV_IVA_ENDOSO { get; set; }
        public decimal TOTAL_FACTURA { get; set; }
        public string ESTADO_FAC { get; set; }
    }

    public class FAC_MOVS
    {       
        public long CONSECUTIVO { get; set; }
        public long BULTOS { get; set; }
        public decimal KILOS { get; set; }
        public decimal VOLUMEN { get; set; }
        public long TIPO_MERCANCIA { get; set; }
    }

    public class FAC_TARIMA
    {
        public long ID_TARIMA { get; set; }      
        public long ID_MOVIMIENTO { get; set; }
        public long ID_MERCANCIA { get; set; }
        public string DESCRIPCION_MERCANCIA { get; set; }
        public long BULTOS { get; set; }
        public decimal KILOS { get; set; }
        public decimal VOLUMEN { get; set; }
        public string OBSERVACIONES { get; set; }
        public string FTA_USU_REGISTRA_P { get; set; }
    }

    public class FAC_RUBRO
    {
        public long FCO_FAC_FACTURA { get; set; }
        public string FCO_RUB_RUBRO { get; set; }
        public decimal FCO_MONTO { get; set; }
        public decimal FCO_FMV_MOVIMIENTO { get; set; }
        public string FCO_USU_REGISTRO { get; set; }

    }

    public class FAC_CALCULOS
    {
        public decimal ADICIONALES { get; set; }
        public decimal DESCUENTO { get; set; }
        public decimal SUBTOTAL { get; set; }
        public decimal IVA { get; set; }
        public decimal TOTAL { get; set; }
    }

    public class FAC_PERSONA
    {
        public string CFR_CODIGO { get; set; }
        public string CFR_CEDULA { get; set; }
        public string CFR_NOMBRE { get; set; }
        public string CFR_TELEFONO { get; set; }
    }

    public class FAC_CONDUCTOR
    {
        public string TRA_TRANSPORTISTA { get; set; }
        public string TRA_NOMBRE { get; set; }
        public string TRA_CEDULA { get; set; }
        public string TRA_TELEFONO { get; set; }
    }

    public class FAC_CALCULOS_MOV
    {
        public decimal ALMACENAJE { get; set; }
        public decimal MANEJO { get; set; }
        public decimal SEGURO { get; set; }
        public decimal DESCUENTO { get; set; }
        public decimal IMP_IVA { get; set; }
        public decimal TOTAL { get; set; }
    }

    public class FAC_CLONACION
    {
        public string FAC_FACTURA_CLO { get; set; }
        public string FAC_CUENTA_CLO { get; set; }
        public string FAC_DIN_TIPO_DOCUMENTO_CLO { get; set; }
        public string FAC_DCT_DECLARANTE_ID_CLO { get; set; }
        public string FAC_ADC_AGENTE_ID_CLO { get; set; }
        public string FAC_CONTRIBUYENTE_CLO { get; set; }
        public string FAC_DUA_SALIDA_CLO { get; set; }       
        public string FAC_USU_DIGITO_CLO { get; set; }
        
    }
}