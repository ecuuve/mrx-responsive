﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebNSI.Models
{
    public class Motivos
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Departamentos
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }

    public class Manifiesto_Modificar
    {
        public string TMA_MANIFIESTO { get; set; }
        public string TMA_FCH_MANIFIESTO { get; set; }
        public string TMA_FCH_INGRESO { get; set; }
        public string TMA_FCH_REGIMEN { get; set; }
        public long TMA_BOD_NBODEGA { get; set; }
        public long TMA_ADU_RECIBE { get; set; }
        public long TMA_ADU_ENVIA { get; set; }
        public string ADU_RECIBE { get; set; }
        public string ADU_ENVIA { get; set; }
        public long TMA_PTO_PUERTO { get; set; }
        public long TMA_AER_NAEROLINEA { get; set; }
        public string AEROLINEA { get; set; }
        public string TMA_MOD_TRANSPORTE { get; set; }
        public long TMA_VIAJE { get; set; }
        public string TMA_VIAJE_CONTINGENCIA { get; set; }
        public string TMA_USU_REGISTRO { get; set; }
        public string TMA_FCH_REGISTRO { get; set; }
        public string TMA_ESTADO { get; set; }
        public string TMA_GUIA_MADRE { get; set; }
        public long TMA_MOT_MOTIVO { get; set; }
        public string TMA_USU_MODIFICO { get; set; }
        public string TMA_FCH_MODIFICO { get; set; }
        public string TMA_DEPARTAMENTO { get; set; }
        public string TMA_RESPONSABLE { get; set; }
        public string TMA_SOLICITANTE { get; set; }
        public string UserMERX { get; set; }
        public string TMA_COD_VUELO { get; set; }
        public string TMA_DUA { get; set; }
        public string TMA_CLIENTE_NOMBRE { get; set; }
        public string TMA_NUM_CONTENEDOR { get; set; }
        public string TMA_TIPO_CONTENEDOR { get; set; }
        public string TIPO_CONTENEDOR { get; set; }
        public long TMA_TAMANO { get; set; }
        public string TMA_TDO_DOCUMENTO { get; set; }
        public string TMA_CLIENTE_ID { get; set; }
        public string TMA_FUN_FUNCIONARIO { get; set; }
        public string TMA_ACTA_NUMERO { get; set; }
        public string TMA_CODIGO_VUELO { get; set; }
        public string TMA_PORTON { get; set; }
        public string TMA_CHEQUEADOR { get; set; }
        public string TMA_PAB { get; set; }
        public string TMA_PALETIZADO { get; set; }
        public string TMA_EQUIPO_ESPECIAL { get; set; }
        public string TMA_SEPARACIONES { get; set; }
        public string TMA_CLIENTE_TIPO { get; set; }
        public string TMA_ID_MANIFIESTO { get; set; }
        public string TMA_UBICACION_INGRESO { get; set; }
        public string TMA_ID_FUN_SUPERVISION { get; set; }
        public string TMA_ACTA_SUPERVISION { get; set; }

    }

    public class Manifiesto_Modificar_post
    {
        public string TMA_MANIFIESTO { get; set; }
        public string TMA_FCH_MANIFIESTO { get; set; }
        public string TMA_FCH_INGRESO { get; set; }
        public string TMA_FCH_REGIMEN { get; set; }
        public string TMA_BOD_NBODEGA { get; set; }
        public string TMA_ADU_RECIBE { get; set; }
        public string TMA_ADU_ENVIA { get; set; }
        public string TMA_PTO_PUERTO { get; set; }
        public string TMA_AER_NAEROLINEA { get; set; }
        public string TMA_MOD_TRANSPORTE { get; set; }
        public string TMA_VIAJE_CONTINGENCIA { get; set; }
        public string TMA_USU_MODIFICO { get; set; }
        public string TMA_GUIA_MADRE { get; set; }
        public string TMA_NUM_CONTENEDOR { get; set; }
        public string TMA_TIPO_CONTENEDOR { get; set; }
        public string TMA_TAMANO { get; set; }
        public string TMA_TDO_DOCUMENTO { get; set; }
        public string TMA_CLIENTE_ID { get; set; }
        public string TMA_CLIENTE_NOMBRE { get; set; }
        public string TMA_CLIENTE_TIPO { get; set; }
        public string TMA_FUN_FUNCIONARIO { get; set; }
        public string TMA_ACTA_NUMERO { get; set; }
        public string TMA_CODIGO_VUELO { get; set; }
        public string TMA_PORTON { get; set; }
        public string TMA_CHEQUEADOR { get; set; }
        public string TMA_PAB { get; set; }
        public string TMA_PALETIZADO { get; set; }
        public string TMA_EQUIPO_ESPECIAL { get; set; }
        public string TMA_SEPARACIONES { get; set; }
        public string TMA_ESTADO { get; set; }
        public string TMA_ID_MANIFIESTO { get; set; }
        public string TMA_MOT_MOTIVO { get; set; }
        public string TMA_DEPARTAMENTO { get; set; }
        public string TMA_RESPONSABLE { get; set; }
        public string TMA_SOLICITANTE { get; set; }
        public string TMA_ID_FUN_SUPERVISION { get; set; }
        public string TMA_ACTA_SUPERVISION { get; set; }
    }

    public class Manifiesto_Consulta
    {
        public string TMA_MANIFIESTO { get; set; }
        public string TMA_FCH_MANIFIESTO { get; set; }
        public string TMA_FCH_INGRESO { get; set; }
        public string TMA_FCH_REGIMEN { get; set; }
        public long TMA_BOD_NBODEGA { get; set; }
        public long TMA_ADU_RECIBE { get; set; }
        public long TMA_ADU_ENVIA { get; set; }
        public string ADU_RECIBE { get; set; }
        public string ADU_ENVIA { get; set; }
        public long TMA_PTO_PUERTO { get; set; }
        public long TMA_AER_NAEROLINEA { get; set; }
        public string AEROLINEA { get; set; }
        public string TMA_MOD_TRANSPORTE { get; set; }
        public long TMA_VIAJE { get; set; }
        public string TMA_VIAJE_CONTINGENCIA { get; set; }
        public string TMA_USU_REGISTRO { get; set; }
        public string TMA_FCH_REGISTRO { get; set; }
        public string TMA_ESTADO { get; set; }
        public string TMA_GUIA_MADRE { get; set; }
        public long TMA_MOT_MOTIVO { get; set; }
        public string TMA_USU_MODIFICO { get; set; }
        public string TMA_FCH_MODIFICO { get; set; }
        public string TMA_DEPARTAMENTO { get; set; }
        public string TMA_RESPONSABLE { get; set; }
        public string TMA_SOLICITANTE { get; set; }
        public string UserMERX { get; set; }
        public string TMA_COD_VUELO { get; set; }
        public string TMA_DUA { get; set; }
        public string TMA_CLIENTE_NOMBRE { get; set; }
        public string TMA_NUM_CONTENEDOR { get; set; }
        public string TMA_TIPO_CONTENEDOR { get; set; }
        public string TIPO_CONTENEDOR { get; set; }
        public long TMA_TAMANO { get; set; }
        public string TMA_TDO_DOCUMENTO { get; set; }
        public string TMA_CLIENTE_ID { get; set; }
        public string TMA_FUN_FUNCIONARIO { get; set; }
        public string TMA_ACTA_NUMERO { get; set; }
        public string TMA_CODIGO_VUELO { get; set; }
        public string TMA_PORTON { get; set; }
        public string TMA_CHEQUEADOR { get; set; }
        public string TMA_PAB { get; set; }
        public string TMA_PALETIZADO { get; set; }
        public string TMA_EQUIPO_ESPECIAL { get; set; }
        public string TMA_SEPARACIONES { get; set; }
        public string TMA_CLIENTE_TIPO { get; set; }
        public string TMA_ID_MANIFIESTO { get; set; }
        public string TMA_UBICACION_INGRESO { get; set; }
        public string TMA_ID_FUN_SUPERVISION { get; set; }
        public string TMA_ACTA_SUPERVISION { get; set; }
    }

    public class Manifiesto_Consulta_General
    {
        public string manifiesto { get; set; }
        public string consecutivo { get; set; }
        public string contenedor { get; set; }
        public string bl { get; set; }
        public string dua { get; set; }
        public string cliente { get; set; }
        public string viaje { get; set; }
        public string consolidador { get; set; }
        public string movimiento { get; set; }
        public string consignatario { get; set; }
        

    }
}