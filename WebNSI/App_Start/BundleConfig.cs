﻿using System.Web;
using System.Web.Optimization;
namespace WebNSI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region CSS

            bundles.Add(new StyleBundle("~/Content/css_login").Include(
                "~/assets/Bootstrap4/css/bootstrap.min.css",
                "~/assets/font-awesome/css/fontawesome-all.min.css",
                "~/assets/Content/sweetalert.css",
                "~/assets/css/login.css",
                "~/assets/css/funcionalidad.css"));

            bundles.Add(new StyleBundle("~/Content/css_menu").Include(
                "~/assets/Content/Menu.css"));

            bundles.Add(new StyleBundle("~/Content/css_master").Include(
                "~/assets/Bootstrap4/css/bootstrap.min.css",
                 "~/assets/Content/languages.css",
                 "~/assets/font-awesome/css/fontawesome-all.min.css",
                 "~/assets/Content/jquery.timepicker.min.css",
                 "~/assets/css/main.css",
                 "~/assets/css/funcionalidad.css",
                 "~/assets/Content/sweetalert.css"
                 ));

            bundles.Add(new StyleBundle("~/Content/kendo/css").Include(
                "~/Content/Kendo/kendo.common.min.css",
                "~/Content/Kendo/kendo.default.min.css",
                "~/Content/Kendo/kendo.default.mobile.min.css"));


            #endregion

            #region JAVASCRIPT general
            
            bundles.Add(new ScriptBundle("~/bundles/js_master").Include(
                "~/assets/js/js_materialize.js",
                 "~/Scripts/handlebars.min.js",
                "~/assets/js/js_keyboardFunctions.js",
                "~/assets/js/js_principal.js",
                "~/assets/Scripts/sweetalert.min.js",
                "~/assets/Scripts/jquery.masknumber.min.js", 
                "~/assets/Scripts/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_login").Include(
               "~/assets/Scripts/mdb.min.js",
                "~/assets/Scripts/sweetalert.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_mdb").Include(
               "~/Scripts/handlebars.min.js",
               //"~/assets/Scripts/tether.min.js",
               "~/assets/Scripts/mdb.min.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                      "~/assets/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/assets/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/assets/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
              "~/assets/Bootstrap4/js/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Kendo").Include(
               "~/assets/Scripts/kendo.all.min.js",
               "~/assets/js/kendo.aspnetmvc.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/cookie").Include(
              "~/assets/cookie/js.cookie.js"));


            #endregion

            #region JAVASCRIPT pantallas

            //MANIFIESTOS

            bundles.Add(new ScriptBundle("~/bundles/js_ingreso_mercancias").Include(
                "~/assets/js/js_ingreso_mercancia.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_manifiestocontenedor_con").Include(
               "~/assets/js/js_manifiestocontenedor_con.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_mercancia_viajes").Include(
               "~/assets/js/js_manifiesto_viaje.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_mercancia_duas").Include(
                "~/assets/js/js_manifiesto_duas.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_manifiestocontenedor_mod").Include(
               "~/assets/js/js_manifiestocontenedor_mod.js"));

           

            //CONOCIMIENTOS

            bundles.Add(new ScriptBundle("~/bundles/js_conocimientos").Include(
                "~/assets/js/js_conocimientos.js",
                "~/assets/js/js_sumatoria.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_conocimientos_consulta").Include(
                "~/assets/js/js_conocimientos_consulta.js",
                "~/assets/js/js_sumatoria.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_conocimientos_modificar").Include(
                "~/assets/js/js_conocimientos_modificar.js",
                "~/assets/js/js_sumatoria.js"));


            //TARIMAS

            bundles.Add(new ScriptBundle("~/bundles/js_tarimas").Include(
                "~/assets/js/js_tarimas_ingreso.js",
                "~/assets/js/js_sumatoria.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_tarimas_consultas").Include(
                "~/assets/js/js_tarimas_consultas.js",
                "~/assets/js/js_sumatoria.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_tarimas_modificar").Include(
                "~/assets/js/js_tarimas_modificar.js",
                "~/assets/js/js_sumatoria.js"));

            //REIMPRESIÓN
            bundles.Add(new ScriptBundle("~/bundles/js_consultas").Include(
               "~/assets/js/js_entrada.js",
               "~/assets/js/js_mercancia.js"));

            //LOCALIZACIÓN
            bundles.Add(new ScriptBundle("~/bundles/js_localizaEspecial").Include(
              "~/assets/js/js_localizaEspecial.js"));

            //MUESTRAS
            bundles.Add(new ScriptBundle("~/bundles/js_registro_muestras").Include(
                "~/assets/js/js_registro_muestras.js"));

            //FACTURACIÓN 
            bundles.Add(new ScriptBundle("~/bundles/js_facturacion").Include(
              "~/assets/js/js_facturacion.js"));

            //BLOQUEO GUIAS
            bundles.Add(new ScriptBundle("~/bundles/js_bloqueoGuias").Include(
                "~/assets/js/js_bloqueoGuias.js"));

            //PREALERTAS
            bundles.Add(new ScriptBundle("~/bundles/js_prealertas").Include(
                "~/assets/js/js_prealertas.js"));

            //PREVIOS
            bundles.Add(new ScriptBundle("~/bundles/js_previos").Include(
               "~/assets/js/js_previos.js"));

            //ATENCION SALA
            bundles.Add(new ScriptBundle("~/bundles/js_atencion_sala").Include(
             "~/assets/js/js_atencion_sala.js"));

            //CORRECCIONES
            bundles.Add(new ScriptBundle("~/bundles/js_correctransmisiones").Include(
              "~/assets/js/js_correctransmisiones.js"));

            //CONSIGNATARIOS
            bundles.Add(new ScriptBundle("~/bundles/js_consignatarios").Include(
               "~/assets/js/js_consignatarios.js"));

            //RECLAMOS SC
            bundles.Add(new ScriptBundle("~/bundles/js_registro_reclamos").Include(
               "~/assets/js/js_registro_reclamos.js"));

            //RECLAMACIONES
            bundles.Add(new ScriptBundle("~/bundles/js_reclamaciones").Include(
                "~/assets/js/js_reclamaciones.js"));

            //CESIONES
            bundles.Add(new ScriptBundle("~/bundles/js_mercancia_cesion_derechos").Include(
               "~/assets/js/js_mercancia_cesion_derechos.js"));

            //SOLICITUD VIDEO
            bundles.Add(new ScriptBundle("~/bundles/js_solicitud_video").Include(
               "~/assets/js/js_solicitud_video.js"));

            //AGENCIA ADUANAL
            bundles.Add(new ScriptBundle("~/bundles/js_agencia_aduanal").Include(
               "~/assets/js/js_agencia_aduanal.js"));

            //CONSOLIDADOR
            bundles.Add(new ScriptBundle("~/bundles/js_consolidador").Include(
              "~/assets/js/js_consolidador.js"));

            //COLA AFORO
            bundles.Add(new ScriptBundle("~/bundles/js_cola_aforo").Include(
               "~/assets/js/js_cola_aforo.js"));

            //COLA CARRUSEL
            bundles.Add(new ScriptBundle("~/bundles/js_cola_carrusel").Include(
             "~/assets/js/js_cola_carrusel.js"));

            //BLOQUEOS OPS
            bundles.Add(new ScriptBundle("~/bundles/js_bloqueos").Include(
               "~/assets/js/js_bloqueos.js"));

            //MERCANCIAS PERDIDAS
            bundles.Add(new ScriptBundle("~/bundles/js_mercancias_perdidas").Include(
                "~/assets/js/js_mercancias_perdidas.js"));

            //INDICACIONES A POSTERIOR
            bundles.Add(new ScriptBundle("~/bundles/js_indc_posterior").Include(
               "~/assets/js/js_Indic_Posterior.js"));

            //PRODUCTO NO CONFORME
            bundles.Add(new ScriptBundle("~/bundles/js_producto_nc").Include(
                "~/assets/js/js_producto_nc.js",
                "~/assets/js/js_productoNC_detalle.js",
                "~/assets/js/js_productoNC_investigacion.js",
                "~/assets/js/js_productoNC_verificacion.js"));

            //AJUSTES DE INVENTARIO
            bundles.Add(new ScriptBundle("~/bundles/js_diferencias").Include(
                "~/assets/js/js_diferencias.js"));

            //CONS BITACORA
            bundles.Add(new ScriptBundle("~/bundles/js_bitacora").Include(
               "~/assets/js/js_bitacora.js"));

            //CONS PREALERTAS
            bundles.Add(new ScriptBundle("~/bundles/js_cons_prealertas").Include(
              "~/assets/js/js_cons_prealertas.js"));

            //CONS COLAS
            bundles.Add(new ScriptBundle("~/bundles/js_consulta_colas").Include(
              "~/assets/js/js_consulta_colas.js"));

            //HIST COLAS
            bundles.Add(new ScriptBundle("~/bundles/js_historico_colas").Include(
                "~/assets/js/js_historico_colas.js"));

            //CONS FACTURAS
            bundles.Add(new ScriptBundle("~/bundles/js_facturacion_consulta").Include(
              "~/assets/js/js_facturacion_consulta.js"));

            //DESCONSOLIDACIONES
            bundles.Add(new ScriptBundle("~/bundles/js_desconsolidaciones").Include(
              "~/assets/js/js_cons_desconsolidacion.js"));

            //DECLARANTES
            bundles.Add(new ScriptBundle("~/bundles/js_declarantes").Include(
                "~/assets/js/js_declarantes.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_funcionarios").Include(
              "~/assets/js/js_dcl_funcionarios.js"));

            //TARIFARIOS
            bundles.Add(new ScriptBundle("~/bundles/js_tarifario").Include(
                "~/assets/js/js_tarifario.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_tarifas_cliente").Include(
                "~/assets/js/js_tarifas_cliente.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_conceptos_tarifa").Include(
                "~/assets/js/js_conceptos_tarifa.js"));

            //CAJERAS
            bundles.Add(new ScriptBundle("~/bundles/js_cajeras").Include(
              "~/assets/js/js_cajeras.js"));

            //MONITOREO ANDEN
            bundles.Add(new ScriptBundle("~/bundles/js_ingreso_anden").Include(
                "~/assets/js/js_ingreso_anden.js"));

            //CONTROL ANDEN 
            bundles.Add(new ScriptBundle("~/bundles/js_control_anden").Include(
                "~/assets/js/js_control_anden.js"));

            #endregion

            #region NO SE SI SE USAN

           

            bundles.Add(new ScriptBundle("~/bundles/js_guiascerradas").Include(
               "~/assets/js/js_guias.js",
               "~/assets/js/js_manifiesto.js",
               "~/assets/js/js_tarimas.js",
               "~/assets/js/js_guiasCerradas_edicion.js"));

            #endregion


            
            bundles.Add(new ScriptBundle("~/bundles/js_movimientos_consulta").Include(
              "~/assets/js/js_movimientos_consulta.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_mercancias_consultas").Include(
              "~/assets/js/js_mercancias_consultas.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_mercancias_modificar").Include(
              "~/assets/js/js_mercancias_modificar.js"));


            bundles.Add(new ScriptBundle("~/bundles/js_conocimientos_consulta2").Include(
               "~/assets/js/js_conocimientos_consulta - Copy.js",
               "~/assets/js/js_sumatoria.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_conocimientos_modificar2").Include(
                "~/assets/js/js_conocimientos_modificar - Copy.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_tarimas_consultas2").Include(
                "~/assets/js/js_tarimas_consultas - Copy.js",
                "~/assets/js/js_sumatoria.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_tarimas_modificar2").Include(
                "~/assets/js/js_tarimas_modificar - Copy.js",
                "~/assets/js/js_sumatoria.js"));

        }
    }
}
