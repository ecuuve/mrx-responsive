﻿using System.Web;
using System.Web.Mvc;

namespace WebNSI
{
    public class FilterConfig
    {
        public class CustomAuthentication : AuthorizeAttribute
        {
            protected override bool AuthorizeCore(HttpContextBase httpContext)
            {
                if (httpContext.Request.Url.LocalPath.ToLower().StartsWith("/login")) return true;
                if (httpContext.Session["@Autenticado"] == null)
                {
                    return false;
                }
                return true;
            }

            protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
            {
                filterContext.Result = new RedirectResult("/Login");
            }
        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomAuthentication());
        }
    }
}
