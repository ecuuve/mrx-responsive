﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;

namespace WebNSI
{
    public class HandleDbErrorAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is System.Data.Entity.Infrastructure.DbUpdateException)
            {
                var innerException = context.Exception; var errors = new ModelStateDictionary();
                if (innerException.InnerException is System.Data.DataException)
                {
                    errors.AddModelError("general", innerException.InnerException.InnerException);
                    context.Response = context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
            }
            else if (context.Exception is System.Data.Entity.Validation.DbEntityValidationException)
            {
                var errors = new ModelStateDictionary();
                var dbValidation = context.Exception as System.Data.Entity.Validation.DbEntityValidationException;
                foreach (var validationSummary in dbValidation.EntityValidationErrors)
                {
                    foreach (var error in validationSummary.ValidationErrors)
                    {
                        errors.AddModelError(error.PropertyName ?? "general", error.ErrorMessage);
                    }
                }
                context.Response = context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
            }
        }
    }

}