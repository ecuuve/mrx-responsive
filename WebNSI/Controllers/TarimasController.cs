﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebNSI.Controllers
{
    public class TarimasController : Controller
    {
       

        // GET: MX_TARIMAS_TB
        public ActionResult Index(string id)
        {
            string[] tuplas = id.Split(',');
            foreach (string tuplasdet in tuplas)
            {
                if (tuplasdet.ToString() != "")
                {
                    if (ViewBag.Manifiesto == null)
                    {
                        ViewBag.Manifiesto = tuplasdet.ToString();
                    }
                    else if (ViewBag.Id == null)
                    {
                        ViewBag.Id= tuplasdet.ToString();
                    }
                    else
                    {
                        ViewBag.GuiaOriginal = tuplasdet.ToString();
                    }

                }

            }
            return View();

        }
    }
}