﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebNSI.Models;

namespace WebNSI.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        //public string get_session()
        //{
        //    string user = "";
        //    var session = System.Web.HttpContext.Current.Session;

        //    if (session != null)
        //    {
        //        if (session["user"] != null)
        //        {
        //            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
        //            OracleConnection cn = new OracleConnection(connectionString);
        //            cn.Open();
        //            OracleCommand objCmd = new OracleCommand();
        //            objCmd.Connection = cn;
        //            objCmd.CommandText = "mrx.tx_qry_usuario_pr";
        //            objCmd.CommandType = CommandType.StoredProcedure;
        //            objCmd.Parameters.Add("usuario_p", OracleDbType.Varchar2).Value = session["user"].ToString();
        //            objCmd.Parameters.Add("pass_p", OracleDbType.Varchar2).Value = "";
        //            objCmd.Parameters.Add("usu_nombre_p", OracleDbType.Varchar2).Value = "";
        //            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

        //            objCmd.ExecuteNonQuery();

        //            DataTable datos2 = new DataTable();

        //            OracleDataAdapter objAdapter = new OracleDataAdapter();
        //            objAdapter.SelectCommand = objCmd;
        //            objAdapter.Fill(datos2);

        //            cn.Close();

        //            foreach (DataRow e in datos2.Rows)
        //            {
        //                user = e["USU_NOMBRE"].ToString();
        //            }

        //        }
        //    }

        //    return user;

        //}

        //public string get_sessionuser()
        //{
        //    string user = "";
        //    var session = System.Web.HttpContext.Current.Session;
        //    if (session != null)
        //    {
        //        if (session["user"] != null)
        //        {
        //            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
        //            OracleConnection cn = new OracleConnection(connectionString);
        //            cn.Open();
        //            OracleCommand objCmd = new OracleCommand();
        //            objCmd.Connection = cn;
        //            objCmd.CommandText = "mrx.tx_qry_usuario_pr";
        //            objCmd.CommandType = CommandType.StoredProcedure;
        //            objCmd.Parameters.Add("usuario_p", OracleDbType.Varchar2).Value = session["user"].ToString();
        //            objCmd.Parameters.Add("pass_p", OracleDbType.Varchar2).Value = "";
        //            objCmd.Parameters.Add("usu_nombre_p", OracleDbType.Varchar2).Value = "";
        //            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

        //            objCmd.ExecuteNonQuery();

        //            DataTable datos2 = new DataTable();

        //            OracleDataAdapter objAdapter = new OracleDataAdapter();
        //            objAdapter.SelectCommand = objCmd;
        //            objAdapter.Fill(datos2);

        //            cn.Close();

        //            foreach (DataRow e in datos2.Rows)
        //            {
        //                user = e["USU_CODIGO"].ToString();
        //            }

        //        }
        //    }

        //    return user;

        //}

        [HttpPost]
        public ActionResult Authorization(Usuario loginModel)
        {
            // Encriptador crypto = new Encriptador();
            if (ModelState.IsValid)
            {
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.tx_qry_usuario_pr";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("usuario_p", OracleDbType.Varchar2).Value = loginModel.User;
                objCmd.Parameters.Add("pass_p", OracleDbType.Varchar2).Value = loginModel.Password;
                objCmd.Parameters.Add("usu_nombre_p", OracleDbType.Varchar2).Value = "";
                objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                try
                {
                    objCmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "E"; //"Usuario o contraseña incorrectos: " + ex.Message;
                    //ViewBag.Message = "Usuario o contraseña incorrectos: " + ex.Message;
                    //TempData["msg"] = "<script>alert('Usuario o contraseña incorrectos');</script>" + ex.Message;
                    return View("Login");
                    //return RedirectToAction("Login");
                }


                DataTable datos2 = new DataTable();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();

                if (datos2.Rows.Count > 0)
                {
                    foreach (DataRow e in datos2.Rows)
                    {
                        Session["userName"] = e["USU_NOMBRE"].ToString();
                        Session["user"] = e["USU_CODIGO"].ToString();
                        if (Session["user"] != null)
                        {
                            TempData["msg"] = "<script>alert('Autenticado');</script>";
                            Session["@Autenticado"] = true;
                        }

                    }
                    TempData["msg"] = "<script>alert('Redireccionando');</script>";
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["msg"] = "<script>alert('Usuario o contraseña incorrectos');</script>";
                    return View("Login");
                }
            }
            else
            {
                TempData["msg"] = "<script>alert('Model Invalid');</script>";
                return Redirect("/Login");
                //return View("Login");
            }

        }
    }
}