﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebNSI.Controllers
{
    public class ContenedorController : Controller
    {
        // GET: Cajeras
        public ActionResult Index( string id)
        {
            ViewBag.Id = "";
            string[] tuplas = id.Split(',');
            foreach (string tuplasdet in tuplas)
            {
                if (tuplasdet.ToString() != "")
                {
                    if (ViewBag.Manifiesto == null)
                    {
                        ViewBag.Manifiesto = tuplasdet.ToString();
                    }
                    else
                    {
                        ViewBag.Id = tuplasdet.ToString();
                    }

                }

            }
            return View();
        }
    }
}