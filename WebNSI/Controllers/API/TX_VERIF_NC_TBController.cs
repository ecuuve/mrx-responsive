﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_VERIF_NC_TBController : ApiController
    {
       
        [Route("api/TX_VERIF_NC_TB/Descripciones")]
        [ResponseType(typeof(descripcionPNC))]
        public IHttpActionResult GetProductos(long? identificador)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_PNC_VERIFICACION_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("vnc_pnc_identificador_p", OracleDbType.Int32).Value = identificador;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<descripcionPNC> _productos = new List<descripcionPNC>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _productos.Add(new descripcionPNC
                    {
                        VNC_PNC_IDENTIFICADOR = e["VNC_PNC_IDENTIFICADOR"].ToString(),
                        VNC_SECUENCIA = e["VNC_SECUENCIA"].ToString(),
                        VNC_FECHA = e["VNC_FECHA"].ToString(),
                        VNC_DESCRIPCION = e["VNC_DESCRIPCION"].ToString(),
                        VNC_USU_REGISTRA = e["VNC_USU_REGISTRA"].ToString(),
                        VNC_FCH_REGISTRA = e["VNC_FCH_REGISTRA"].ToString()
                    });
                }
                return Ok(_productos);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_VERIF_NC_TB/GuardarVerificacion")]
        public IHttpActionResult PostGuardarProductoNC(descripcionPNC producto_p)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_PNC_VERIFICACION_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("PNC_IDENTIFICADOR_P", OracleDbType.Int32).Value = producto_p.VNC_PNC_IDENTIFICADOR;
            objCmd.Parameters.Add("VNC_FECHA_P", OracleDbType.Varchar2).Value = producto_p.VNC_FECHA;
            objCmd.Parameters.Add("VNC_DESCRIPCION_P", OracleDbType.Varchar2).Value = producto_p.VNC_DESCRIPCION;
            objCmd.Parameters.Add("VNC_USU_REGISTRA_P", OracleDbType.Varchar2).Value = producto_p.VNC_USU_REGISTRA;

            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        

    }
}
