﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_TARIFARIO_TBController : ApiController
    {

        #region NUEVO TARIFARIO


        [Route("api/TX_TARIFARIO_TB/busquedaClientesTarifa")]
        public IHttpActionResult Busqueda(TARIFA_CLIENTE_BUSQUEDA post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CLIENTE_TARIFAS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ttc_id_p", OracleDbType.Int32).Value = post.B_TTC_ID;
            objCmd.Parameters.Add("ttc_nombre_cliente_p", OracleDbType.Varchar2).Value = post.B_TTC_NOMBRE_CLIENTE;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();

                DataTable table = new DataTable();
                List<TARIFA_CLIENTE> data = new List<TARIFA_CLIENTE>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(table);

                cn.Close();

                foreach (DataRow r in table.Rows)
                {
                    data.Add(new TARIFA_CLIENTE
                    {
                        TTC_ID = r["TTC_ID"].ToString(),//long.Parse(r["TTC_ID"].ToString()),
                        TTC_TIPO_CLIENTE = r["TTC_TIPO_CLIENTE"].ToString(),
                        TTC_NOMBRE_CLIENTE = r["TTC_NOMBRE_CLIENTE"].ToString(),
                        TTC_CLIENTE_ID = r["TTC_CLIENTE_ID"].ToString()//long.Parse(r["TRF_CONT_TAMANO"].ToString()),
                        //TTC_USU_REGISTRO = r["TTC_USU_REGISTRO"].ToString(),
                        //TTC_FCH_REGISTRO = r["TTC_FCH_REGISTRO"].ToString() //long.Parse(r["TRF_DIAS_MINIMO"].ToString())

                    });
                }

                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/TX_TARIFARIO_TB/insertarClienteTarifa")]
        public IHttpActionResult TarifarioInsertar(TARIFA_CLIENTE post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_CLIENTE_TARIFA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ttc_tipo_cliente_p", OracleDbType.Varchar2).Value = post.TTC_TIPO_CLIENTE;
            objCmd.Parameters.Add("ttc_cliente_id_p", OracleDbType.Varchar2).Value = post.TTC_CLIENTE_ID;
            objCmd.Parameters.Add("ttc_usu_registra_p", OracleDbType.Varchar2).Value = post.TTC_USU_REGISTRO;


            try
            {
                objCmd.ExecuteNonQuery();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/TX_TARIFARIO_TB/BorrarClienteTarifa")]
        public IHttpActionResult Delete(long id, string user)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_DEL_CLIENTE_TARIFA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ttc_id_p", OracleDbType.Int32).Value = id;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = user;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [Route("api/TX_TARIFARIO_TB/insertarClntTrfConsolConssig")]
        public IHttpActionResult TarifarioInsertConsolConsig(TARIFA_CONSOLID_CONSIG post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_CONSOL_CONSIG_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tcc_ttc_id_p", OracleDbType.Varchar2).Value = post.TCC_TTC_ID;
            objCmd.Parameters.Add("ttc_con_nconsolidador_p", OracleDbType.Varchar2).Value = post.TTC_CON_NCONSOLIDADOR;
            objCmd.Parameters.Add("ttc_imp_cedula_p", OracleDbType.Varchar2).Value = post.TTC_IMP_CEDULA;
            objCmd.Parameters.Add("ttc_usu_registra_p", OracleDbType.Varchar2).Value = post.TTC_USU_REGISTRA;

            try
            {
                objCmd.ExecuteNonQuery();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/TX_TARIFARIO_TB/busquedaConsolidConsigna")]
        public IHttpActionResult BusquedaConsolidConsigna(TARIFA_CONSOLID_CONSIG post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONSOL_CONSIG_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ttc_id_p", OracleDbType.Int32).Value = post.TCC_ID;
            objCmd.Parameters.Add("tcc_ttc_id_p", OracleDbType.Int32).Value = post.TCC_TTC_ID;
            objCmd.Parameters.Add("ttc_con_nconsolidador_p", OracleDbType.Int32).Value = post.TTC_CON_NCONSOLIDADOR;
            objCmd.Parameters.Add("ttc_imp_cedula_p", OracleDbType.Varchar2).Value = post.TTC_IMP_CEDULA;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();

                DataTable table = new DataTable();
                List<TARIFA_CONSOLID_CONSIG> data = new List<TARIFA_CONSOLID_CONSIG>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(table);

                cn.Close();

                foreach (DataRow r in table.Rows)
                {
                    data.Add(new TARIFA_CONSOLID_CONSIG
                    {
                        TCC_ID = r["TCC_ID"].ToString(),
                        TCC_TTC_ID = r["TCC_TTC_ID"].ToString(),
                        TTC_CON_NCONSOLIDADOR = r["TTC_CON_NCONSOLIDADOR"].ToString(),
                        TTC_IMP_CEDULA = r["TTC_IMP_CEDULA"].ToString(),
                        IMPORTADOR = r["IMPORTADOR"].ToString(),
                        CONSOLIDADOR = r["CONSOLIDADOR"].ToString()

                    });
                }

                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/TX_TARIFARIO_TB/borrarConsolidConsigna")]
        public IHttpActionResult DeleteConsConsig(long id, string user)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_DEL_CONSOL_CONSIG_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tcc_id_p", OracleDbType.Int32).Value = id;
            objCmd.Parameters.Add("ttc_usu_registra_p", OracleDbType.Varchar2).Value = user;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [Route("api/TX_TARIFARIO_TB/TiposTarifa")]
        public IHttpActionResult GetTiposTarifas()
        {
            List<LV_TARIFARIO> _tarifas = new List<LV_TARIFARIO>();
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_TIPO_TARIFA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _tarifas.Add(new LV_TARIFARIO
                    {
                        Id = e["Id"].ToString(),
                        Nombre = e["Nombre"].ToString()
                    });
                }
                return Ok(_tarifas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_TARIFARIO_TB/TiposContenedor")]
        public IHttpActionResult GetTiposContenedor()
        {
            List<LV_TARIFARIO> _tipocontenedor = new List<LV_TARIFARIO>();
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_TIPO_CONTENEDOR_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _tipocontenedor.Add(new LV_TARIFARIO
                    {
                        Id = e["Id"].ToString(),
                        Nombre = e["Nombre"].ToString()
                    });
                }
                return Ok(_tipocontenedor);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_TARIFARIO_TB/TamanosContenedor")]
        public IHttpActionResult GetTamnosContenedor()
        {
            List<LV_TARIFARIO> _tamanocontenedor = new List<LV_TARIFARIO>();
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_TAMANO_CONTENEDOR_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _tamanocontenedor.Add(new LV_TARIFARIO
                    {
                        Id = e["Id"].ToString(),
                        Nombre = e["Nombre"].ToString()
                    });
                }
                return Ok(_tamanocontenedor);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_TARIFARIO_TB/TiposDescarga")]
        public IHttpActionResult GetTiposDescarga()
        {
            List<LV_TARIFARIO> _tiposdescargo = new List<LV_TARIFARIO>();
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_TIPO_DESCARGA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _tiposdescargo.Add(new LV_TARIFARIO
                    {
                        Id = e["Id"].ToString(),
                        Nombre = e["Nombre"].ToString()
                    });
                }
                return Ok(_tiposdescargo);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        #endregion

        #region JAFET

        [Route("api/TX_TARIFARIO_TB/monedas")]
        public IHttpActionResult GetMonedas()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_MONEDA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable table = new DataTable();
            List<MONEDA> data = new List<MONEDA>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(table);

            cn.Close();

            foreach (DataRow r in table.Rows)
            {
                data.Add(new MONEDA
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(data);
        }

        [Route("api/TX_TARIFARIO_TB/tipoparcial")]
        public IHttpActionResult GetTipoParcial()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_TIPO_PARCIAL_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable table = new DataTable();
            List<TIPO_PARCIAL> data = new List<TIPO_PARCIAL>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(table);

            cn.Close();

            foreach (DataRow r in table.Rows)
            {
                data.Add(new TIPO_PARCIAL
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Descripcion"].ToString()
                });
            }

            return Ok(data);
        }

        [Route("api/TX_TARIFARIO_TB/TarifarioInsertar")]
        public IHttpActionResult TarifarioInsertar(TARIFA_POST post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_TARIFARIO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("TRF_NOMBRE_P", OracleDbType.Varchar2).Value = post.TRF_NOMBRE;
            objCmd.Parameters.Add("TRF_CONT_COMPLETO_P", OracleDbType.Varchar2).Value = (post.TRF_CONT_COMPLETO == null)?"N":post.TRF_CONT_COMPLETO;
            objCmd.Parameters.Add("TRF_CONT_TAMANO_P", OracleDbType.Int32).Value = post.TRF_CONT_TAMANO.AsInt();
            objCmd.Parameters.Add("TRF_A_PISO_P", OracleDbType.Varchar2).Value = (post.TRF_A_PISO == null)?"N":post.TRF_A_PISO;
            objCmd.Parameters.Add("TRF_DIAS_MINIMO_P", OracleDbType.Int32).Value = post.TRF_DIAS_MINIMO.AsInt();
            objCmd.Parameters.Add("TRF_MONTO_DIAS_MINIMO_P", OracleDbType.Int32).Value = post.TRF_MONTO_DIAS_MINIMO.AsInt();
            objCmd.Parameters.Add("TRF_MONEDA_DIAS_MINIMO_P", OracleDbType.Varchar2).Value = post.TRF_MONEDA_DIAS_MINIMO;
            objCmd.Parameters.Add("TRF_DIAS_ADICIONALES_P", OracleDbType.Int32).Value = post.TRF_DIAS_ADICIONALES.AsInt();
            objCmd.Parameters.Add("TRF_MONEDA_DIAS_ADICIONALES_P", OracleDbType.Varchar2).Value = post.TRF_MONEDA_DIAS_ADICIONALES;
            objCmd.Parameters.Add("TRF_TIPO_PARCIAL_P", OracleDbType.Varchar2).Value = post.TRF_TIPO_PARCIAL;
            objCmd.Parameters.Add("TRF_MONTO_PARCIAL_P", OracleDbType.Int32).Value = post.TRF_MONTO_PARCIAL.AsInt();
            objCmd.Parameters.Add("TRF_MONEDA_MONTO_PARCIAL_P", OracleDbType.Varchar2).Value = post.TRF_MONEDA_MONTO_PARCIAL;
            objCmd.Parameters.Add("TRF_ES_DUT_P", OracleDbType.Varchar2).Value = (post.TRF_ES_DUT == null)?"N":post.TRF_ES_DUT;
            objCmd.Parameters.Add("TRF_POR_MOVIMIENTO_P", OracleDbType.Varchar2).Value = (post.TRF_POR_MOVIMIENTO == null)?"N":post.TRF_POR_MOVIMIENTO;
            objCmd.Parameters.Add("TRF_USU_REGISTRO_P", OracleDbType.Varchar2).Value = post.TRF_USU_REGISTRO;

            try
            {
                objCmd.ExecuteNonQuery();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        
        [Route("api/TX_TARIFARIO_TB/TarifarioActualizar")]
        public IHttpActionResult TarifarioActualizar(TARIFA_POST post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_TARIFARIO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("TRF_ID_P", OracleDbType.Int32).Value = post.TRF_ID;
            objCmd.Parameters.Add("TRF_NOMBRE_P", OracleDbType.Varchar2).Value = post.TRF_NOMBRE;
            objCmd.Parameters.Add("TRF_CONT_COMPLETO_P", OracleDbType.Varchar2).Value = (post.TRF_CONT_COMPLETO == null) ? "N" : post.TRF_CONT_COMPLETO;
            objCmd.Parameters.Add("TRF_CONT_TAMANO_P", OracleDbType.Int32).Value = post.TRF_CONT_TAMANO.AsInt();
            objCmd.Parameters.Add("TRF_A_PISO_P", OracleDbType.Varchar2).Value = (post.TRF_A_PISO == null) ? "N" : post.TRF_A_PISO;
            objCmd.Parameters.Add("TRF_DIAS_MINIMO_P", OracleDbType.Int32).Value = post.TRF_DIAS_MINIMO.AsInt();
            objCmd.Parameters.Add("TRF_MONTO_DIAS_MINIMO_P", OracleDbType.Int32).Value = post.TRF_MONTO_DIAS_MINIMO.AsInt();
            objCmd.Parameters.Add("TRF_MONEDA_DIAS_MINIMO_P", OracleDbType.Varchar2).Value = post.TRF_MONEDA_DIAS_MINIMO;
            objCmd.Parameters.Add("TRF_DIAS_ADICIONALES_P", OracleDbType.Int32).Value = post.TRF_DIAS_ADICIONALES.AsInt();
            objCmd.Parameters.Add("TRF_MONEDA_DIAS_ADICIONALES_P", OracleDbType.Varchar2).Value = post.TRF_MONEDA_DIAS_ADICIONALES;
            objCmd.Parameters.Add("TRF_TIPO_PARCIAL_P", OracleDbType.Varchar2).Value = post.TRF_TIPO_PARCIAL;
            objCmd.Parameters.Add("TRF_MONTO_PARCIAL_P", OracleDbType.Int32).Value = post.TRF_MONTO_PARCIAL.AsInt();
            objCmd.Parameters.Add("TRF_MONEDA_MONTO_PARCIAL_P", OracleDbType.Varchar2).Value = post.TRF_MONEDA_MONTO_PARCIAL;
            objCmd.Parameters.Add("TRF_ES_DUT_P", OracleDbType.Varchar2).Value = (post.TRF_ES_DUT == null) ? "N" : post.TRF_ES_DUT;
            objCmd.Parameters.Add("TRF_POR_MOVIMIENTO_P", OracleDbType.Varchar2).Value = (post.TRF_POR_MOVIMIENTO == null) ? "N" : post.TRF_POR_MOVIMIENTO;

            try
            {
                objCmd.ExecuteNonQuery();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/TX_TARIFARIO_TB/busqueda")]
        public IHttpActionResult Busqueda(TARIFA_BUSQUEDA post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_TARIFARIO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("NOMBRE_P", OracleDbType.Varchar2).Value = post.B_TRF_NOMBRE;
            objCmd.Parameters.Add("TARIFA_P", OracleDbType.Int32).Value = post.B_TRF_ID;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();

                DataTable table = new DataTable();
                List<TARIFA> data = new List<TARIFA>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(table);

                cn.Close();

                foreach (DataRow r in table.Rows)
                {
                    data.Add(new TARIFA
                    {
                        TRF_ID = long.Parse(r["TRF_ID"].ToString()),
                        TRF_NOMBRE = r["TRF_NOMBRE"].ToString(),
                        TRF_CONT_COMPLETO = r["TRF_CONT_COMPLETO"].ToString(),
                        TRF_CONT_TAMANO = long.Parse(r["TRF_CONT_TAMANO"].ToString()),
                        TRF_A_PISO = r["TRF_A_PISO"].ToString(),
                        TRF_DIAS_MINIMO = long.Parse(r["TRF_DIAS_MINIMO"].ToString()),
                        TRF_MONTO_DIAS_MINIMO = decimal.Parse(r["TRF_MONTO_DIAS_MINIMO"].ToString()),
                        TRF_MONEDA_DIAS_MINIMO = r["TRF_MONEDA_DIAS_MINIMO"].ToString(),
                        TRF_DIAS_ADICIONALES = decimal.Parse(r["TRF_DIAS_ADICIONALES"].ToString()),
                        TRF_MONEDA_DIAS_ADICIONALES = r["TRF_MONEDA_DIAS_ADICIONALES"].ToString(),
                        TRF_TIPO_PARCIAL = r["TRF_TIPO_PARCIAL"].ToString(),
                        TRF_MONTO_PARCIAL = decimal.Parse(r["TRF_MONTO_PARCIAL"].ToString()),
                        TRF_MONEDA_MONTO_PARCIAL = r["TRF_MONEDA_MONTO_PARCIAL"].ToString(),
                        TRF_ES_DUT = r["TRF_ES_DUT"].ToString(),
                        TRF_POR_MOVIMIENTO = r["TRF_POR_MOVIMIENTO"].ToString(),
                        TRF_USU_REGISTRO = r["TRF_USU_REGISTRO"].ToString(),
                        TRF_FCH_REGISTRO = r["TRF_FCH_REGISTRO"].ToString(),
                    });
                }

                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion
    }
}