﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_MANIFIESTOS_TBController : ApiController
    {
        #region GET de LV

        // GET: 
        /// <summary>
        /// Obtiene puertos
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/PuertosTotal")]
        [ResponseType(typeof(Puerto))]
        public IHttpActionResult GetPuertosTotal()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_puertos_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("cod_puerto_p", OracleDbType.Varchar2).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Puerto> _Puertos = new List<Puerto>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Puertos.Add(new Puerto
                {
                    Id = r["PTO_PUERTO"].ToString(),
                    Nombre = r["DESPTO"].ToString()
                });
            }

            return Ok(_Puertos);
        }

        [Route("api/TX_MANIFIESTOS_TB/Portones")]
        [ResponseType(typeof(Puerto))]
        public IHttpActionResult GetPortones(string tipo_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_PORTONES_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tipo_p", OracleDbType.Varchar2).Value = tipo_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Porton> _Portones = new List<Porton>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Portones.Add(new Porton
                {
                    Id = r["ID"].ToString(),
                    Nombre = r["NOMBRE"].ToString()
                });
            }

            return Ok(_Portones);
        }

        /// <summary>
        /// Obtiene transportistas
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/Transportista")]
        [ResponseType(typeof(Transportista))]
        public IHttpActionResult GetTransportistas()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_aerolinea_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("n_naerolinea_p", OracleDbType.Varchar2).Value = null;
            objCmd.Parameters.Add("mod_transporte_p", OracleDbType.Varchar2).Value = 4;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Transportista> _Transportistas = new List<Transportista>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Transportistas.Add(new Transportista
                {
                    Id = r["AER_NAEROLINEA"].ToString(),
                    Nombre = r["AER_NOMBRE"].ToString()
                });
            }

            return Ok(_Transportistas);
        }

        ///// <summary>
        ///// Obtiene Aduanas
        ///// </summary>
        ///// <returns></returns>
        //[Route("api/TX_MANIFIESTOS_TB/Aduanas")]
        //[ResponseType(typeof(Aduana))]
        //public IHttpActionResult GetAduanas()
        //{
        //    //Get the connection string from the app.config            
        //    var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
        //    OracleConnection cn = new OracleConnection(connectionString);
        //    cn.Open();
        //    OracleCommand objCmd = new OracleCommand();
        //    objCmd.Connection = cn;
        //    objCmd.CommandText = "mrx.tx_qry_aduanas_pr";
        //    objCmd.CommandType = CommandType.StoredProcedure;
        //    objCmd.Parameters.Add("adu_aduana_p", OracleDbType.Varchar2).Value = null;
        //    objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

        //    objCmd.ExecuteNonQuery();

        //    DataTable datos2 = new DataTable();
        //    List<Aduana> _Aduanas = new List<Aduana>();

        //    OracleDataAdapter objAdapter = new OracleDataAdapter();
        //    objAdapter.SelectCommand = objCmd;
        //    objAdapter.Fill(datos2);

        //    cn.Close();

        //    foreach (DataRow r in datos2.Rows)
        //    {
        //        _Aduanas.Add(new Aduana
        //        {
        //            Id = r["ADU_ADUANA"].ToString(),
        //            Nombre = r["ADU_DESCRIPCION"].ToString()
        //        });
        //    }

        //    return Ok(_Aduanas);

        //}


        /// <summary>
        /// Obtiene Estados
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/Estados")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetEstados()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_ESTADOS_MANIF_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Estados> _Estados = new List<Estados>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();

                foreach (DataRow r in datos2.Rows)
                {
                    _Estados.Add(new Estados
                    {
                        Id = r["Id"].ToString(),
                        Nombre = r["Nombre"].ToString()
                    });
                }

                return Ok(_Estados);
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }
        //Lista los Motivos
        [Route("api/TX_MANIFIESTOS_TB/Motivos")]
        [ResponseType(typeof(Motivos))]
        public IHttpActionResult GetMotivos()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_motivos_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mot_motivo_p", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Motivos> _estados = new List<Motivos>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _estados.Add(new Motivos
                    {
                        Id = e["codigo"].ToString(),
                        Nombre = e["descripcion"].ToString()

                    });
                }
                return Ok(_estados);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        //Lista los Departamentos
        [Route("api/TX_MANIFIESTOS_TB/Departamentos")]
        [ResponseType(typeof(Departamentos))]
        public IHttpActionResult GetDepartamentos()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_DEPARTAMENTOS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dep_descripcion_p", OracleDbType.Varchar2).Value = null;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Departamentos> _estados = new List<Departamentos>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _estados.Add(new Departamentos
                    {
                        Id = e["codigo"].ToString(),
                        Nombre = e["descripcion"].ToString()

                    });
                }
                return Ok(_estados);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Obtiene modalidades de transporte
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/ModalidadTransporte")]
        [ResponseType(typeof(Mod_transporte))]
        public IHttpActionResult GetMod_transporte()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_modal_trans_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Mod_transporte> _ModTrans = new List<Mod_transporte>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _ModTrans.Add(new Mod_transporte
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_ModTrans);
        }

        #endregion

        #region NUEVA PANTALLA INGRESO MERC
        /// <summary>
        /// Lista los tipos de contenedor
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/Tipo_Contenedor")]
        [ResponseType(typeof(Tipo_contenedor))]
        public IHttpActionResult GetTipo_Contenedor()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_TIPO_CONTENEDOR_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Tipo_contenedor> _tipos_contenedor = new List<Tipo_contenedor>();
            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);
            cn.Close();
            foreach (DataRow e in datos2.Rows)
            {
                _tipos_contenedor.Add(new Tipo_contenedor
                {

                    Id = e["Id"].ToString(),
                    Nombre = e["Nombre"].ToString()
                });
            }
            return Ok(_tipos_contenedor);
        }

        /// <summary>
        /// Lista las modalidades de transporte
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/ModalidadesTransporte")]
        [ResponseType(typeof(Modalidad_Transporte))]
        public IHttpActionResult GetModalidadesTransporte()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_modal_trans_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Modalidad_Transporte> _modalidades_transporte = new List<Modalidad_Transporte>();
            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);
            cn.Close();
            foreach (DataRow e in datos2.Rows)
            {
                _modalidades_transporte.Add(new Modalidad_Transporte
                {

                    Id = e["Id"].ToString(),
                    Nombre = e["Nombre"].ToString()
                });
            }
            return Ok(_modalidades_transporte);
        }

        /// <summary>
        /// Lista los tamaños
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/Tamano")]
        [ResponseType(typeof(Tamano))]
        public IHttpActionResult GetTamano()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_TAMANO_CONTENEDOR_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Tamano> _tamanos = new List<Tamano>();
            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);
            cn.Close();
            foreach (DataRow e in datos2.Rows)
            {
                _tamanos.Add(new Tamano
                {

                    Id = e["Id"].ToString(),
                    Nombre = e["Nombre"].ToString()
                });
            }
            return Ok(_tamanos);
        }

        /// <summary>
        /// Lista los documentos
        /// </summary>
        /// <param name="tipo_documento"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/Documentos")]
        [ResponseType(typeof(Documento))]
        public IHttpActionResult GetDocumentos(long? tipo_documento)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_tdo_doc_DIN_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("din_tipo_doc_p", OracleDbType.Int32).Value = tipo_documento;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Documento> _tamanos = new List<Documento>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _tamanos.Add(new Documento
                    {

                        Id = e["TIPO"].ToString(),
                        Nombre = e["DESCRIPCION"].ToString()
                    });
                }
                return Ok(_tamanos);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Lista los tipos de clientes
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/TiposClientes")]
        [ResponseType(typeof(Clientes))]
        public IHttpActionResult GetTiposClientes()
        {
            List<Clientes> _clientes = new List<Clientes>();
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_TIPO_CLIENTE_MANIF_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _clientes.Add(new Clientes
                    {
                        Id = e["Id"].ToString(),
                        Nombre = e["Nombre"].ToString()
                    });
                }
                return Ok(_clientes);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Lista los clientes
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="tipo_cliente"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/Clientes")]
        [ResponseType(typeof(Clientes))]
        public IHttpActionResult GetClientes(string cliente, string tipo_cliente)
        {
            List<Clientes> _clientes = new List<Clientes>();
            if (tipo_cliente != null)
            {
                //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_QRY_CLIENTE_MANIF_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("cliente_p", OracleDbType.Varchar2).Value = cliente;
                objCmd.Parameters.Add("tipo_cliente_p", OracleDbType.Varchar2).Value = tipo_cliente;
                objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                try
                {
                    objCmd.ExecuteNonQuery();
                    DataTable datos2 = new DataTable();
                    OracleDataAdapter objAdapter = new OracleDataAdapter();
                    objAdapter.SelectCommand = objCmd;
                    objAdapter.Fill(datos2);
                    cn.Close();
                    foreach (DataRow e in datos2.Rows)
                    {
                        _clientes.Add(new Clientes
                        {

                            Id = e["CON_CEDULA"].ToString(),
                            Nombre = e["CON_NOMBRE"].ToString()
                        });
                    }
                    return Ok(_clientes);
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
            else
            {
                return Ok(_clientes);
            }
            
        }

        /*
        /// <summary>
        /// Lista los clientes
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="tipo_cliente"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/Clientes2")]
        [ResponseType(typeof(Clientes))]
        public IHttpActionResult Clientes2(string tipo_cliente, CLIENTES_FILTER post)
        {
            if (post == null) post = new CLIENTES_FILTER();
            List<Clientes> _clientes = new List<Clientes>();
            if (tipo_cliente != null)
            {
                //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_QRY_CLIENTE_MANIF_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("cliente_p", OracleDbType.Varchar2).Value = post.Nombre;
                objCmd.Parameters.Add("tipo_cliente_p", OracleDbType.Varchar2).Value = tipo_cliente;
                objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                try
                {
                    objCmd.ExecuteNonQuery();
                    DataTable datos2 = new DataTable();
                    OracleDataAdapter objAdapter = new OracleDataAdapter();
                    objAdapter.SelectCommand = objCmd;
                    objAdapter.Fill(datos2);
                    cn.Close();
                    foreach (DataRow e in datos2.Rows)
                    {
                        _clientes.Add(new Clientes
                        {

                            Id = e["CON_CEDULA"].ToString(),
                            Nombre = e["CON_NOMBRE"].ToString()
                        });
                    }
                    return Ok(_clientes);
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
            else
            {
                return Ok(_clientes);
            }

        }*/

        /// <summary>
        /// Lista los transportistas
        /// </summary>
        /// <param name="mod_transporte"></param>
        /// <param name="aerolinea"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/Transportistas")]
        [ResponseType(typeof(Transportista))]
        public IHttpActionResult GetTransportista(string mod_transporte, string aerolinea)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_aerolinea_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("n_naerolinea_p", OracleDbType.Varchar2).Value = aerolinea;
            objCmd.Parameters.Add("mod_transporte_p", OracleDbType.Varchar2).Value = mod_transporte;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Transportista> _transportistas= new List<Transportista>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _transportistas.Add(new Transportista
                    {

                        Id = e["aer_naerolinea"].ToString(),
                        Nombre = e["aer_nombre"].ToString()
                    });
                }
                return Ok(_transportistas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Lista los transpotistas para cargalo en los filtros del listado
        /// </summary>
        /// <param name="aerolinea"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/TransportistasFiltro")]
        [ResponseType(typeof(Transportista))]
        public IHttpActionResult GetTransportistaFiltro(string aerolinea)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_LIST_AEROLINEA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("n_naerolinea_p", OracleDbType.Varchar2).Value = aerolinea;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Transportista> _transportistas = new List<Transportista>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _transportistas.Add(new Transportista
                    {

                        Id = e["aer_naerolinea"].ToString(),
                        Nombre = e["aer_nombre"].ToString()
                    });
                }
                return Ok(_transportistas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Lista los funcionarios
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/Funcionarios")]
        [ResponseType(typeof(Funcionarios))]
        public IHttpActionResult GetFuncionarios(string id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_func_aduana_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("id_funcionario_p", OracleDbType.Varchar2).Value = id;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Funcionarios> _funcionarios = new List<Funcionarios>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _funcionarios.Add(new Funcionarios
                    {

                        Id = e["CODIGO"].ToString(),
                        Nombre = e["NOMBRE"].ToString()
                    });
                }
                return Ok(_funcionarios);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Lista los puertos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/Puertos")]
        [ResponseType(typeof(Puerto))]
        public IHttpActionResult GetPuertos(string id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_puertos_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("cod_puerto_p", OracleDbType.Varchar2).Value = id;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Puerto> _puertos = new List<Puerto>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _puertos.Add(new Puerto
                    {

                        Id = e["pto_puerto"].ToString(),
                        Nombre = e["despto"].ToString()
                    });
                }
                return Ok(_puertos);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [Route("api/TX_MANIFIESTOS_TB/Chekeadores")]
        [ResponseType(typeof(Chekeador))]
        public IHttpActionResult GetChekeadores(string nombre_p, string usuario_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CHEQUEADOR_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("nombre_p", OracleDbType.Varchar2).Value = nombre_p;
            objCmd.Parameters.Add("usuario_p", OracleDbType.Varchar2).Value = usuario_p;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Chekeador> _chekers = new List<Chekeador>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _chekers.Add(new Chekeador
                    {

                        Id = e["ID"].ToString(),
                        Nombre = e["NOMBRE"].ToString()
                    });
                }
                return Ok(_chekers);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Lista las Aduanas
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/Aduanas")]
        [ResponseType(typeof(Aduana))]
        public IHttpActionResult GetAduanas(string id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_ADUANAS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("adu_aduana_p", OracleDbType.Varchar2).Value = id;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Aduana> _aduanas = new List<Aduana>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _aduanas.Add(new Aduana
                    {

                        Id = e["adu_aduana"].ToString(),
                        Nombre = e["adu_descripcion"].ToString()
                    });
                }
                return Ok(_aduanas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Obtiene datos de manifiestos
        /// </summary>
        /// <param name="tma_manifiesto_p"></param>
        /// <param name="tma_fch_manifiesto_p"></param>
        /// <param name="tma_aer_naerolinea_p"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/Manifiesto")]
        [ResponseType(typeof(Manifiesto))]
        public IHttpActionResult GetManifiestos(long? manifiesto_id, string tma_manifiesto_p, string tma_fch_manifiesto_p, long? tma_aer_naerolinea_p, string cliente, string num_contenedor) //, int page, int offset)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_MANIFIESTOS_PR"; //_PAG
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tma_id_manifiesto_p", OracleDbType.Int32).Value = manifiesto_id;
            objCmd.Parameters.Add("tma_manifiesto_p", OracleDbType.Varchar2).Value = tma_manifiesto_p;
            objCmd.Parameters.Add("tma_fch_manifiesto_p", OracleDbType.Varchar2).Value = tma_fch_manifiesto_p;
            objCmd.Parameters.Add("tma_aer_naerolinea_p", OracleDbType.Int32).Value = tma_aer_naerolinea_p == 0 ? null : tma_aer_naerolinea_p;
            objCmd.Parameters.Add("tma_cliente_nombre", OracleDbType.Varchar2).Value = cliente;
            objCmd.Parameters.Add("tma_num_contenedor_p", OracleDbType.Varchar2).Value = num_contenedor;
            //objCmd.Parameters.Add("page", OracleDbType.Int32).Value = page;
            //objCmd.Parameters.Add("offset", OracleDbType.Int32).Value = offset;

            /*OracleParameter n_recs = new OracleParameter();
            n_recs.OracleDbType = OracleDbType.Int64;
            n_recs.Direction = ParameterDirection.Output;
            n_recs.Size = 1000;*/

            OracleParameter p_resultSet = new OracleParameter();
            p_resultSet.OracleDbType = OracleDbType.RefCursor;
            p_resultSet.Direction = ParameterDirection.Output;

            //objCmd.Parameters.Add(n_recs);
            objCmd.Parameters.Add(p_resultSet);
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable table = new DataTable();
                //Manifiesto_Busqueda resultado = new Manifiesto_Busqueda();
                //resultado.N_REGISTROS = n_recs.Value.AsInt();
                //resultado.REGISTROS = new List<Manifiesto>();
                List<Manifiesto> resultado = new List<Manifiesto>();
                OracleRefCursor refCursor = (OracleRefCursor)p_resultSet.Value;
                table.Load(refCursor.GetDataReader());
                cn.Close();
                foreach (DataRow e in table.Rows)
                {
                    //resultado.REGISTROS.Add
                    resultado.Add(new Manifiesto
                    {
                        TMA_MANIFIESTO = e["TMA_MANIFIESTO"].ToString(),
                        TMA_FCH_MANIFIESTO = e["TMA_FCH_MANIFIESTO"].AsFormattedDate(),
                        TMA_FCH_INGRESO = e["TMA_FCH_INGRESO"].AsFormattedDate(),
                        TMA_FCH_REGIMEN = e["TMA_FCH_REGIMEN"].AsFormattedDate(),
                        TMA_BOD_NBODEGA = long.Parse(e["TMA_BOD_NBODEGA"].ToString()),
                        TMA_ADU_RECIBE = long.Parse(e["TMA_ADU_RECIBE"].ToString()),
                        TMA_ADU_ENVIA = long.Parse(e["TMA_ADU_ENVIA"].ToString()),
                        TMA_PTO_PUERTO = long.Parse(e["TMA_PTO_PUERTO"].ToString()),
                        //TMA_AER_NAEROLINEA = long.Parse(e["TMA_AER_NAEROLINEA"].ToString()),
                        TMA_MOD_TRANSPORTE = e["TMA_MOD_TRANSPORTE"].ToString(),
                        TMA_VIAJE_CONTINGENCIA = e["TMA_VIAJE_CONTINGENCIA"].ToString(),
                        TMA_USU_REGISTRO = e["TMA_USU_REGISTRO"].ToString(),
                        TMA_FCH_REGISTRO = e["TMA_FCH_REGISTRO"].AsFormattedDate(),
                        TMA_ESTADO = e["TMA_ESTADO"].ToString(),
                        TMA_GUIA_MADRE = e["TMA_GUIA_MADRE"].ToString(),
                        TMA_MOT_MOTIVO = string.IsNullOrWhiteSpace(e["TMA_MOT_MOTIVO"].ToString()) ? -1 : long.Parse(e["TMA_MOT_MOTIVO"].ToString()),
                        TMA_USU_MODIFICO = e["TMA_USU_MODIFICO"].ToString(),
                        TMA_FCH_MODIFICO = e["TMA_FCH_MODIFICO"].AsFormattedDate(),
                        TMA_DEPARTAMENTO = e["TMA_DEPARTAMENTO"].ToString(),
                        TMA_RESPONSABLE = e["TMA_RESPONSABLE"].ToString(),
                        TMA_SOLICITANTE = e["TMA_SOLICITANTE"].ToString(),
                        TMA_CLIENTE_NOMBRE = e["TMA_CLIENTE_NOMBRE"].ToString(),
                        TMA_NUM_CONTENEDOR = e["tma_num_contenedor"].ToString(),
                        TMA_TIPO_CONTENEDOR = e["TMA_TIPO_CONTENEDOR"].ToString(),
                        TIPO_CONTENEDOR = e["TIPO_CONTENEDOR"].ToString(),
                        TMA_ID_MANIFIESTO = e["TMA_ID_MANIFIESTO"].ToString(),
                        TMA_TAMANO = long.Parse( e["TMA_TAMANO"].ToString()),
                        TMA_TDO_DOCUMENTO = e["TMA_TDO_DOCUMENTO"].ToString(),
                        TMA_CLIENTE_TIPO = e["TMA_CLIENTE_TIPO"].ToString(),
                        TMA_CLIENTE_ID = e["TMA_CLIENTE_ID"].ToString(),
                        TMA_FUN_FUNCIONARIO = e["TMA_FUN_FUNCIONARIO"].ToString(),
                        TMA_ACTA_NUMERO = e["TMA_ACTA_NUMERO"].ToString(),
                        TMA_CODIGO_VUELO = e["TMA_CODIGO_VUELO"].ToString(),
                        TMA_PORTON = e["TMA_PORTON"].ToString(),
                        TMA_CHEQUEADOR = e["TMA_CHEQUEADOR"].ToString(),
                        TMA_PAB = e["TMA_PAB"].ToString(),
                        TMA_PALETIZADO = e["TMA_PALETIZADO"].ToString(),
                        TMA_EQUIPO_ESPECIAL = e["TMA_EQUIPO_ESPECIAL"].ToString(),
                        TMA_SEPARACIONES = e["TMA_SEPARACIONES"].ToString(),
                        TMA_UBICACION_INGRESO = e["TMA_UBICACION_INGRESO"].ToString(),
                        TMA_ID_FUN_SUPERVISION = e["TMA_ID_FUN_SUPERVISION"].ToString(),
                        TMA_ACTA_SUPERVISION = e["TMA_ACTA_SUPERVISION"].ToString()
                        //AEROLINEA = e["AEROLINEA"].ToString()

                    });
                }
                return Ok(resultado);
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }   
        }

        // POST:
        /// <summary>
        /// Agrega un manifiesto
        /// </summary>
        /// <param name="manifiesto_p"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/GuardarManifiesto")]
        [ResponseType(typeof(Manifiesto))]
        public IHttpActionResult PostTX_MANIFIESTOS_TB(Manifiesto manifiesto_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_MANIFIESTO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tma_manifiesto_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_MANIFIESTO;
            objCmd.Parameters.Add("tma_fch_manifiesto_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_FCH_MANIFIESTO;
            objCmd.Parameters.Add("tma_fch_ingreso_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_FCH_INGRESO;
            objCmd.Parameters.Add("tma_bod_nbodega_p", OracleDbType.Int32).Value = 1;
            objCmd.Parameters.Add("tma_adu_recibe_p", OracleDbType.Int32).Value = manifiesto_p.TMA_ADU_RECIBE;
            objCmd.Parameters.Add("tma_adu_envia_p", OracleDbType.Int32).Value = manifiesto_p.TMA_ADU_ENVIA;
            objCmd.Parameters.Add("tma_pto_puerto_p", OracleDbType.Int32).Value = manifiesto_p.TMA_PTO_PUERTO;
            objCmd.Parameters.Add("tma_aer_naerolinea_p", OracleDbType.Int32).Value = manifiesto_p.TMA_AER_NAEROLINEA == 0 ? null : manifiesto_p.TMA_AER_NAEROLINEA;
            objCmd.Parameters.Add("tma_mod_transporte_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_MOD_TRANSPORTE;
            objCmd.Parameters.Add("tma_viaje_contingencia_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_VIAJE_CONTINGENCIA != null ? "S" : "N";
            objCmd.Parameters.Add("tma_usu_registro_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_USU_REGISTRO;
            objCmd.Parameters.Add("tma_guia_madre_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_GUIA_MADRE;
            objCmd.Parameters.Add("tma_num_contenedor_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_NUM_CONTENEDOR;
            objCmd.Parameters.Add("tma_tipo_contenedor_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_TIPO_CONTENEDOR;
            objCmd.Parameters.Add("tma_tamano_p", OracleDbType.Int32).Value = manifiesto_p.TMA_TAMANO;
            objCmd.Parameters.Add("tma_tdo_documento_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_TDO_DOCUMENTO;
            objCmd.Parameters.Add("tma_cliente_id_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_CLIENTE_ID;
            objCmd.Parameters.Add("tma_cliente_nombre_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_CLIENTE_NOMBRE;
            objCmd.Parameters.Add("tma_cliente_tipo_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_CLIENTE_TIPO;
            objCmd.Parameters.Add("tma_fun_funcionario_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_FUN_FUNCIONARIO;
            objCmd.Parameters.Add("tma_acta_numero_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_ACTA_NUMERO;
            objCmd.Parameters.Add("tma_codigo_vuelo_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_CODIGO_VUELO;
            objCmd.Parameters.Add("tma_porton_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_PORTON;
            objCmd.Parameters.Add("tma_chequeador_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_CHEQUEADOR;
            objCmd.Parameters.Add("tma_pab_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_PAB != null ? "S" : "N";
            objCmd.Parameters.Add("tma_paletizado_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_PALETIZADO != null ? "S" : "N";
            objCmd.Parameters.Add("tma_equipo_especial_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_EQUIPO_ESPECIAL != null ? "S" : "N";
            objCmd.Parameters.Add("tma_separaciones_p", OracleDbType.Varchar2).Value = manifiesto_p.TMA_SEPARACIONES != null ? "S" : "N";
            objCmd.Parameters.Add("tma_id_manifiesto_p", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("TMA_UBICACION_INGRESO_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_UBICACION_INGRESO;
            objCmd.Parameters.Add("TMA_FCH_REGIMEN_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_FCH_REGIMEN;
            objCmd.Parameters.Add("TMA_ID_FUN_SUPERVISION_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_ID_FUN_SUPERVISION;
            objCmd.Parameters.Add("TMA_ACTA_SUPERVISION_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_ACTA_SUPERVISION;
            try
            {
                objCmd.ExecuteNonQuery();
                string id = objCmd.Parameters["tma_id_manifiesto_p"].Value.ToString();
                cn.Close();
                return Ok(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

           
        }

        // PUT: 
        /// <summary>
        /// Actualiza manifiesto
        /// </summary>
        /// <param name="id"></param>
        /// <param name="manifiesto_p"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/ActualizarManifiesto")]
        [ResponseType(typeof(Manifiesto))]
        public IHttpActionResult PutTX_MANIFIESTOS_TB(long id, Manifiesto manifiesto_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_MANIFIESTO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("TMA_MANIFIESTO_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_MANIFIESTO;
            objCmd.Parameters.Add("TMA_FCH_MANIFIESTO_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_FCH_MANIFIESTO;
            objCmd.Parameters.Add("TMA_FCH_INGRESO_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_FCH_INGRESO;
            objCmd.Parameters.Add("TMA_BOD_NBODEGA_P", OracleDbType.Int32).Value = 1;
            objCmd.Parameters.Add("TMA_ADU_RECIBE_P", OracleDbType.Int32).Value = manifiesto_p.TMA_ADU_RECIBE;
            objCmd.Parameters.Add("TMA_ADU_ENVIA_P", OracleDbType.Int32).Value = manifiesto_p.TMA_ADU_ENVIA;
            objCmd.Parameters.Add("TMA_PTO_PUERTO_P", OracleDbType.Int32).Value = manifiesto_p.TMA_PTO_PUERTO;
            objCmd.Parameters.Add("TMA_AER_NAEROLINEA_P", OracleDbType.Int32).Value = manifiesto_p.TMA_AER_NAEROLINEA;
            objCmd.Parameters.Add("TMA_MOD_TRANSPORTE_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_MOD_TRANSPORTE;
            objCmd.Parameters.Add("TMA_VIAJE_CONTINGENCIA_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_VIAJE_CONTINGENCIA != null ? "S" : "N";
            objCmd.Parameters.Add("TMA_USU_MODIFICO_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_USU_MODIFICO;
            objCmd.Parameters.Add("TMA_GUIA_MADRE_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_GUIA_MADRE;
            objCmd.Parameters.Add("TMA_NUM_CONTENEDOR_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_NUM_CONTENEDOR;
            objCmd.Parameters.Add("TMA_TIPO_CONTENEDOR_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_TIPO_CONTENEDOR;
            objCmd.Parameters.Add("TMA_TAMANO_P", OracleDbType.Int32).Value =  manifiesto_p.TMA_TAMANO;
            objCmd.Parameters.Add("TMA_TDO_DOCUMENTO_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_TDO_DOCUMENTO;
            objCmd.Parameters.Add("TMA_CLIENTE_ID_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_CLIENTE_ID;
            objCmd.Parameters.Add("TMA_CLIENTE_NOMBRE_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_CLIENTE_NOMBRE;
            objCmd.Parameters.Add("TMA_CLIENTE_TIPO_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_CLIENTE_TIPO;
            objCmd.Parameters.Add("TMA_FUN_FUNCIONARIO_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_FUN_FUNCIONARIO;
            objCmd.Parameters.Add("TMA_ACTA_NUMERO_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_ACTA_NUMERO;//== "" ? null : manifiesto_p.TMA_ACTA_NUMERO;
            objCmd.Parameters.Add("TMA_CODIGO_VUELO_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_CODIGO_VUELO == "" ? null : manifiesto_p.TMA_CODIGO_VUELO;
            objCmd.Parameters.Add("TMA_PORTON_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_PORTON == "" ? null : manifiesto_p.TMA_PORTON;
            objCmd.Parameters.Add("TMA_CHEQUEADOR_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_CHEQUEADOR == "" ? null : manifiesto_p.TMA_CHEQUEADOR;
            objCmd.Parameters.Add("TMA_PAB_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_PAB != null ? "S" : "N";
            objCmd.Parameters.Add("TMA_PALETIZADO_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_PALETIZADO != null ? "S" : "N";
            objCmd.Parameters.Add("TMA_EQUIPO_ESPECIAL_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_EQUIPO_ESPECIAL != null ? "S" : "N";
            objCmd.Parameters.Add("TMA_SEPARACIONES_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_SEPARACIONES != null ? "S" : "N";
            objCmd.Parameters.Add("TMA_ESTADO_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_ESTADO;
            objCmd.Parameters.Add("TMA_ID_MANIFIESTO_P", OracleDbType.Int32).Value = long.Parse(manifiesto_p.TMA_ID_MANIFIESTO);
            objCmd.Parameters.Add("TMA_UBICACION_INGRESO_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_UBICACION_INGRESO;
            objCmd.Parameters.Add("TMA_FCH_REGIMEN_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_FCH_REGIMEN;
            objCmd.Parameters.Add("TMA_ID_FUN_SUPERVISION_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_ID_FUN_SUPERVISION;
            objCmd.Parameters.Add("TMA_ACTA_SUPERVISION_P", OracleDbType.Varchar2).Value = manifiesto_p.TMA_ACTA_SUPERVISION;
            try
            {
                objCmd.ExecuteNonQuery();
                return Ok(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [Route("api/TX_MANIFIESTOS_TB/NotificacionFinDescarga")]
        public IHttpActionResult GetEnviarNotificacionFinDescarga(int manifiesto)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_NOTIFICA_FIN_DESCAR_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("manifiesto_p", OracleDbType.Int32).Value = manifiesto;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok(manifiesto);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }





        #endregion

        #region CONSULTA MANIFIESTO/MERCANCIAS

        /// <summary>
        /// Obtiene datos de manifiestos
        /// </summary>
        /// <param name="tma_manifiesto_p"></param>
        /// <param name="tma_fch_manifiesto_p"></param>
        /// <param name="tma_aer_naerolinea_p"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/ManifiestoConsulta")]
        [ResponseType(typeof(Manifiesto_Consulta))]
        public IHttpActionResult GetManifiestos_Consulta(long? manifiesto_id, string tma_manifiesto_p, string tma_fch_manifiesto_p, long? tma_aer_naerolinea_p, string cliente, string num_contenedor)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CON_MANIFIESTO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tma_id_manifiesto_p", OracleDbType.Int32).Value = manifiesto_id;
            objCmd.Parameters.Add("tma_manifiesto_p", OracleDbType.Varchar2).Value = tma_manifiesto_p;
            objCmd.Parameters.Add("tma_fch_manifiesto_p", OracleDbType.Varchar2).Value = tma_fch_manifiesto_p;
            objCmd.Parameters.Add("tma_aer_naerolinea_p", OracleDbType.Int32).Value = tma_aer_naerolinea_p == 0 ? null : tma_aer_naerolinea_p;
            objCmd.Parameters.Add("tma_cliente_nombre", OracleDbType.Varchar2).Value = cliente;
            objCmd.Parameters.Add("tma_num_contenedor_p", OracleDbType.Varchar2).Value = num_contenedor;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Manifiesto_Consulta> _Manifiestos = new List<Manifiesto_Consulta>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _Manifiestos.Add(new Manifiesto_Consulta
                    {
                        TMA_MANIFIESTO = e["TMA_MANIFIESTO"].ToString(),
                        TMA_FCH_MANIFIESTO = e["TMA_FCH_MANIFIESTO"].AsFormattedDate(),
                        TMA_FCH_INGRESO = e["TMA_FCH_INGRESO"].AsFormattedDate(),
                        TMA_FCH_REGIMEN = e["TMA_FCH_REGIMEN"].AsFormattedDate(),
                        TMA_BOD_NBODEGA = long.Parse(e["TMA_BOD_NBODEGA"].ToString()),
                        TMA_ADU_RECIBE = long.Parse(e["TMA_ADU_RECIBE"].ToString()),
                        TMA_ADU_ENVIA = long.Parse(e["TMA_ADU_ENVIA"].ToString()),
                        TMA_PTO_PUERTO = long.Parse(e["TMA_PTO_PUERTO"].ToString()),
                        //TMA_AER_NAEROLINEA = long.Parse(e["TMA_AER_NAEROLINEA"].ToString()),
                        TMA_MOD_TRANSPORTE = e["TMA_MOD_TRANSPORTE"].ToString(),
                        TMA_VIAJE_CONTINGENCIA = e["TMA_VIAJE_CONTINGENCIA"].ToString(),
                        TMA_USU_REGISTRO = e["TMA_USU_REGISTRO"].ToString(),
                        TMA_FCH_REGISTRO = e["TMA_FCH_REGISTRO"].AsFormattedDate(),
                        TMA_ESTADO = e["TMA_ESTADO"].ToString(),
                        TMA_GUIA_MADRE = e["TMA_GUIA_MADRE"].ToString(),
                        TMA_MOT_MOTIVO = string.IsNullOrWhiteSpace(e["TMA_MOT_MOTIVO"].ToString()) ? -1 : long.Parse(e["TMA_MOT_MOTIVO"].ToString()),
                        TMA_USU_MODIFICO = e["TMA_USU_MODIFICO"].ToString(),
                        TMA_FCH_MODIFICO = e["TMA_FCH_MODIFICO"].AsFormattedDate(),
                        TMA_DEPARTAMENTO = e["TMA_DEPARTAMENTO"].ToString(),
                        TMA_RESPONSABLE = e["TMA_RESPONSABLE"].ToString(),
                        TMA_SOLICITANTE = e["TMA_SOLICITANTE"].ToString(),
                        TMA_CLIENTE_NOMBRE = e["TMA_CLIENTE_NOMBRE"].ToString(),
                        TMA_NUM_CONTENEDOR = e["tma_num_contenedor"].ToString(),
                        TMA_TIPO_CONTENEDOR = e["TMA_TIPO_CONTENEDOR"].ToString(),
                        TIPO_CONTENEDOR = e["TIPO_CONTENEDOR"].ToString(),
                        TMA_ID_MANIFIESTO = e["TMA_ID_MANIFIESTO"].ToString(),
                        TMA_TAMANO = long.Parse(e["TMA_TAMANO"].ToString()),
                        TMA_TDO_DOCUMENTO = e["TMA_TDO_DOCUMENTO"].ToString(),
                        TMA_CLIENTE_TIPO = e["TMA_CLIENTE_TIPO"].ToString(),
                        TMA_CLIENTE_ID = e["TMA_CLIENTE_ID"].ToString(),
                        TMA_FUN_FUNCIONARIO = e["TMA_FUN_FUNCIONARIO"].ToString(),
                        TMA_ACTA_NUMERO = e["TMA_ACTA_NUMERO"].ToString(),
                        TMA_CODIGO_VUELO = e["TMA_CODIGO_VUELO"].ToString(),
                        TMA_PORTON = e["TMA_PORTON"].ToString(),
                        TMA_CHEQUEADOR = e["TMA_CHEQUEADOR"].ToString(),
                        TMA_PAB = e["TMA_PAB"].ToString(),
                        TMA_PALETIZADO = e["TMA_PALETIZADO"].ToString(),
                        TMA_EQUIPO_ESPECIAL = e["TMA_EQUIPO_ESPECIAL"].ToString(),
                        TMA_SEPARACIONES = e["TMA_SEPARACIONES"].ToString(),
                        TMA_UBICACION_INGRESO = e["TMA_UBICACION_INGRESO"].ToString(),
                        TMA_ID_FUN_SUPERVISION = e["TMA_ID_FUN_SUPERVISION"].ToString(),
                        TMA_ACTA_SUPERVISION = e["TMA_ACTA_SUPERVISION"].ToString()
                        //AEROLINEA = e["AEROLINEA"].ToString()
                    });
                }
                return Ok(_Manifiestos);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [Route("api/TX_MANIFIESTOS_TB/ManifiestoConsultaGeneral")]
        [ResponseType(typeof(Manifiesto_Consulta_General))]
        public IHttpActionResult GetManifiestos_ConsultaGeneral(long? manifiesto, string contenedor, string bl, long? movimiento, string consignatario, string consecutivo)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CON_GENERAL_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("contenedor_p", OracleDbType.Varchar2).Value = contenedor;
            objCmd.Parameters.Add("guia_p", OracleDbType.Varchar2).Value = bl;
            objCmd.Parameters.Add("consignatario_p", OracleDbType.Varchar2).Value = consignatario;
            objCmd.Parameters.Add("movimiento_p", OracleDbType.Int32).Value = movimiento;
            objCmd.Parameters.Add("consecutivo_p", OracleDbType.Int32).Value = consecutivo;
            objCmd.Parameters.Add("manifiesto_p", OracleDbType.Int32).Value = manifiesto;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Manifiesto_Consulta_General> _Manifiestos = new List<Manifiesto_Consulta_General>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _Manifiestos.Add(new Manifiesto_Consulta_General
                    {
                        manifiesto = e["manifiesto"].ToString(),
                        consecutivo = e["consecutivo"].ToString(),
                        contenedor = e["contenedor"].ToString(),
                        bl = e["bl"].ToString(),
                        dua = e["dua"].ToString(),
                        cliente = e["cliente"].ToString(),
                        viaje = e["viaje"].ToString(),
                        consolidador = e["consolidador"].ToString(),
                        movimiento = e["movimiento"].ToString(),
                        //TMA_MOT_MOTIVO = string.IsNullOrWhiteSpace(e["TMA_MOT_MOTIVO"].ToString()) ? -1 : long.Parse(e["TMA_MOT_MOTIVO"].ToString()),                    
                        //TMA_TAMANO = long.Parse(e["TMA_TAMANO"].ToString()),
                        //e["TMA_FCH_MANIFIESTO"].AsFormattedDate(),
                    });
                }
                return Ok(_Manifiestos);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }



        #endregion

        #region MODIFICACION MANIFIESTO/MERCANCIAS
        /// <summary>
        /// Obtiene datos de manifiestos
        /// </summary>
        /// <param name="tma_manifiesto_p"></param>
        /// <param name="tma_fch_manifiesto_p"></param>
        /// <param name="tma_aer_naerolinea_p"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTOS_TB/ManifiestoModificar")]
        [ResponseType(typeof(Manifiesto_Modificar))]
        public IHttpActionResult GetManifiestos_Modificar(long? manifiesto_id, string tma_manifiesto_p, string tma_fch_manifiesto_p, long? tma_aer_naerolinea_p, string cliente, string num_contenedor)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_MODI_MANIFIESTO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tma_id_manifiesto_p", OracleDbType.Int32).Value = manifiesto_id;
            objCmd.Parameters.Add("tma_manifiesto_p", OracleDbType.Varchar2).Value = tma_manifiesto_p;
            objCmd.Parameters.Add("tma_fch_manifiesto_p", OracleDbType.Varchar2).Value = tma_fch_manifiesto_p;
            objCmd.Parameters.Add("tma_aer_naerolinea_p", OracleDbType.Int32).Value = tma_aer_naerolinea_p == 0 ? null : tma_aer_naerolinea_p;
            objCmd.Parameters.Add("tma_cliente_nombre", OracleDbType.Varchar2).Value = cliente;
            objCmd.Parameters.Add("tma_num_contenedor_p", OracleDbType.Varchar2).Value = num_contenedor;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Manifiesto_Modificar> _Manifiestos = new List<Manifiesto_Modificar>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _Manifiestos.Add(new Manifiesto_Modificar
                    {
                        TMA_MANIFIESTO = e["TMA_MANIFIESTO"].ToString(),
                        TMA_FCH_MANIFIESTO = e["TMA_FCH_MANIFIESTO"].AsFormattedDate(),
                        TMA_FCH_INGRESO = e["TMA_FCH_INGRESO"].AsFormattedDate(),
                        TMA_FCH_REGIMEN = e["TMA_FCH_REGIMEN"].AsFormattedDate(),
                        TMA_BOD_NBODEGA = long.Parse(e["TMA_BOD_NBODEGA"].ToString()),
                        TMA_ADU_RECIBE = long.Parse(e["TMA_ADU_RECIBE"].ToString()),
                        TMA_ADU_ENVIA = long.Parse(e["TMA_ADU_ENVIA"].ToString()),
                        TMA_PTO_PUERTO = long.Parse(e["TMA_PTO_PUERTO"].ToString()),
                        //TMA_AER_NAEROLINEA = long.Parse(e["TMA_AER_NAEROLINEA"].ToString()),
                        TMA_MOD_TRANSPORTE = e["TMA_MOD_TRANSPORTE"].ToString(),
                        TMA_VIAJE_CONTINGENCIA = e["TMA_VIAJE_CONTINGENCIA"].ToString(),
                        TMA_USU_REGISTRO = e["TMA_USU_REGISTRO"].ToString(),
                        TMA_FCH_REGISTRO = e["TMA_FCH_REGISTRO"].AsFormattedDate(),
                        TMA_ESTADO = e["TMA_ESTADO"].ToString(),
                        TMA_GUIA_MADRE = e["TMA_GUIA_MADRE"].ToString(),
                        //TMA_MOT_MOTIVO = string.IsNullOrWhiteSpace(e["TMA_MOT_MOTIVO"].ToString()) ? -1 : long.Parse(e["TMA_MOT_MOTIVO"].ToString()),
                        TMA_USU_MODIFICO = e["TMA_USU_MODIFICO"].ToString(),
                        TMA_FCH_MODIFICO = e["TMA_FCH_MODIFICO"].AsFormattedDate(),
                        //TMA_DEPARTAMENTO = e["TMA_DEPARTAMENTO"].ToString(),
                        //TMA_RESPONSABLE = e["TMA_RESPONSABLE"].ToString(),
                        //TMA_SOLICITANTE = e["TMA_SOLICITANTE"].ToString(),
                        TMA_CLIENTE_NOMBRE = e["TMA_CLIENTE_NOMBRE"].ToString(),
                        TMA_NUM_CONTENEDOR = e["tma_num_contenedor"].ToString(),
                        TMA_TIPO_CONTENEDOR = e["TMA_TIPO_CONTENEDOR"].ToString(),
                        TIPO_CONTENEDOR = e["TIPO_CONTENEDOR"].ToString(),
                        TMA_ID_MANIFIESTO = e["TMA_ID_MANIFIESTO"].ToString(),
                        TMA_TAMANO = long.Parse(e["TMA_TAMANO"].ToString()),
                        TMA_TDO_DOCUMENTO = e["TMA_TDO_DOCUMENTO"].ToString(),
                        TMA_CLIENTE_TIPO = e["TMA_CLIENTE_TIPO"].ToString(),
                        TMA_CLIENTE_ID = e["TMA_CLIENTE_ID"].ToString(),
                        TMA_FUN_FUNCIONARIO = e["TMA_FUN_FUNCIONARIO"].ToString(),
                        TMA_ACTA_NUMERO = e["TMA_ACTA_NUMERO"].ToString(),
                        TMA_CODIGO_VUELO = e["TMA_CODIGO_VUELO"].ToString(),
                        TMA_PORTON = e["TMA_PORTON"].ToString(),
                        TMA_CHEQUEADOR = e["TMA_CHEQUEADOR"].ToString(),
                        TMA_PAB = e["TMA_PAB"].ToString(),
                        TMA_PALETIZADO = e["TMA_PALETIZADO"].ToString(),
                        TMA_EQUIPO_ESPECIAL = e["TMA_EQUIPO_ESPECIAL"].ToString(),
                        TMA_SEPARACIONES = e["TMA_SEPARACIONES"].ToString(),
                        TMA_UBICACION_INGRESO = e["TMA_UBICACION_INGRESO"].ToString(),
                        TMA_ID_FUN_SUPERVISION = e["TMA_ID_FUN_SUPERVISION"].ToString(),
                        TMA_ACTA_SUPERVISION = e["TMA_ACTA_SUPERVISION"].ToString()
                        //AEROLINEA = e["AEROLINEA"].ToString()
                    });
                }
                return Ok(_Manifiestos);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_MANIFIESTOS_TB/ManifiestoModificar_Post")]
        [ResponseType(typeof(Manifiesto_Modificar_post))]
        public IHttpActionResult PutManifiestos_Modificar_Post(long id, Manifiesto_Modificar_post post)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_MODI_MANIFIESTO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("TMA_MANIFIESTO_P", OracleDbType.Varchar2).Value = post.TMA_MANIFIESTO;
            objCmd.Parameters.Add("TMA_FCH_MANIFIESTO_P", OracleDbType.Varchar2).Value = post.TMA_FCH_MANIFIESTO;
            objCmd.Parameters.Add("TMA_FCH_INGRESO_P", OracleDbType.Varchar2).Value = post.TMA_FCH_INGRESO;
            objCmd.Parameters.Add("TMA_BOD_NBODEGA_P", OracleDbType.Int32).Value = 1;//post.TMA_BOD_NBODEGA;
            objCmd.Parameters.Add("TMA_ADU_RECIBE_P", OracleDbType.Int32).Value = post.TMA_ADU_RECIBE;
            objCmd.Parameters.Add("TMA_ADU_ENVIA_P", OracleDbType.Int32).Value = post.TMA_ADU_ENVIA;
            objCmd.Parameters.Add("TMA_PTO_PUERTO_P", OracleDbType.Int32).Value = post.TMA_PTO_PUERTO;
            objCmd.Parameters.Add("TMA_AER_NAEROLINEA_P", OracleDbType.Int32).Value = post.TMA_AER_NAEROLINEA;
            objCmd.Parameters.Add("TMA_MOD_TRANSPORTE_P", OracleDbType.Int32).Value = post.TMA_MOD_TRANSPORTE;
            objCmd.Parameters.Add("TMA_VIAJE_CONTINGENCIA_P", OracleDbType.Varchar2).Value = (post.TMA_VIAJE_CONTINGENCIA == null)?"N": post.TMA_VIAJE_CONTINGENCIA;
            objCmd.Parameters.Add("TMA_USU_MODIFICO_P", OracleDbType.Varchar2).Value = post.TMA_USU_MODIFICO;
            objCmd.Parameters.Add("TMA_GUIA_MADRE_P", OracleDbType.Varchar2).Value = post.TMA_GUIA_MADRE;
            objCmd.Parameters.Add("TMA_NUM_CONTENEDOR_P", OracleDbType.Varchar2).Value = post.TMA_NUM_CONTENEDOR;
            objCmd.Parameters.Add("TMA_TIPO_CONTENEDOR_P", OracleDbType.Varchar2).Value = post.TMA_TIPO_CONTENEDOR;
            objCmd.Parameters.Add("TMA_TAMANO_P", OracleDbType.Int32).Value = post.TMA_TAMANO;
            objCmd.Parameters.Add("TMA_TDO_DOCUMENTO_P", OracleDbType.Varchar2).Value = post.TMA_TDO_DOCUMENTO;
            objCmd.Parameters.Add("TMA_CLIENTE_ID_P", OracleDbType.Varchar2).Value = post.TMA_CLIENTE_ID;
            objCmd.Parameters.Add("TMA_CLIENTE_NOMBRE_P", OracleDbType.Varchar2).Value = post.TMA_CLIENTE_NOMBRE;
            objCmd.Parameters.Add("TMA_CLIENTE_TIPO_P", OracleDbType.Varchar2).Value = post.TMA_CLIENTE_TIPO;
            objCmd.Parameters.Add("TMA_FUN_FUNCIONARIO_P", OracleDbType.Varchar2).Value = post.TMA_FUN_FUNCIONARIO;
            objCmd.Parameters.Add("TMA_ACTA_NUMERO_P", OracleDbType.Varchar2).Value = post.TMA_ACTA_NUMERO;
            objCmd.Parameters.Add("TMA_CODIGO_VUELO_P", OracleDbType.Varchar2).Value = post.TMA_CODIGO_VUELO;
            objCmd.Parameters.Add("TMA_PORTON_P", OracleDbType.Varchar2).Value = post.TMA_PORTON;
            objCmd.Parameters.Add("TMA_CHEQUEADOR_P", OracleDbType.Varchar2).Value = post.TMA_CHEQUEADOR;
            objCmd.Parameters.Add("TMA_PAB_P", OracleDbType.Varchar2).Value = post.TMA_PAB != null ? "S" : "N"; //(post.TMA_PAB == null) ? "N" : post.TMA_PAB;
            objCmd.Parameters.Add("TMA_PALETIZADO_P", OracleDbType.Varchar2).Value = post.TMA_PALETIZADO != null ? "S" : "N";  //(post.TMA_PALETIZADO == null) ? "N" : post.TMA_PALETIZADO;
            objCmd.Parameters.Add("TMA_EQUIPO_ESPECIAL_P", OracleDbType.Varchar2).Value = post.TMA_EQUIPO_ESPECIAL != null ? "S" : "N";//(post.TMA_EQUIPO_ESPECIAL == null) ? "N" : post.TMA_EQUIPO_ESPECIAL;
            objCmd.Parameters.Add("TMA_SEPARACIONES_P", OracleDbType.Varchar2).Value = post.TMA_SEPARACIONES != null ? "S" : "N";//(post.TMA_SEPARACIONES == null) ? "N" : post.TMA_SEPARACIONES;
            objCmd.Parameters.Add("TMA_ESTADO_P", OracleDbType.Varchar2).Value = post.TMA_ESTADO;
            objCmd.Parameters.Add("TMA_ID_MANIFIESTO_P", OracleDbType.Int32).Value = post.TMA_ID_MANIFIESTO;
            objCmd.Parameters.Add("TMA_MOT_MOTIVO_P", OracleDbType.Int32).Value = post.TMA_MOT_MOTIVO;
            objCmd.Parameters.Add("TMA_DEPARTAMENTO_P", OracleDbType.Varchar2).Value = post.TMA_DEPARTAMENTO;
            objCmd.Parameters.Add("TMA_RESPONSABLE_P", OracleDbType.Varchar2).Value = post.TMA_RESPONSABLE;
            objCmd.Parameters.Add("TMA_SOLICITANTE_P", OracleDbType.Varchar2).Value = post.TMA_SOLICITANTE;
            objCmd.Parameters.Add("TMA_FCH_REGIMEN_P", OracleDbType.Varchar2).Value = post.TMA_FCH_REGIMEN;
            objCmd.Parameters.Add("TMA_ID_FUN_SUPERVISION_P", OracleDbType.Varchar2).Value = post.TMA_ID_FUN_SUPERVISION;
            objCmd.Parameters.Add("TMA_ACTA_SUPERVISION_P", OracleDbType.Varchar2).Value = post.TMA_ACTA_SUPERVISION;

            try
            {
                objCmd.ExecuteNonQuery();
                return Ok(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion

        #region NUEVA PANTALLA CONOCIMIENTOS

        [Route("api/TX_MANIFIESTOS_TB/IndicacionRefrigeracion")]
        [ResponseType(typeof(IndicRefri))]
        public IHttpActionResult GetIndicacionRefrigeracion()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_indic_refri_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<IndicRefri> _IndicacionRefrigeracion = new List<IndicRefri>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _IndicacionRefrigeracion.Add(new IndicRefri
                    {

                        Id = e["Id"].ToString(),
                        Nombre = e["Nombre"].ToString()
                    });
                }
                return Ok(_IndicacionRefrigeracion);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_MANIFIESTOS_TB/Clasificacion")]
        [ResponseType(typeof(Clasificacion))]
        public IHttpActionResult GetClasificacion()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_clase_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Clasificacion> _clasificacion = new List<Clasificacion>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _clasificacion.Add(new Clasificacion
                    {

                        Id = e["Id"].ToString(),
                        Nombre = e["Nombre"].ToString()
                    });
                }
                return Ok(_clasificacion);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_MANIFIESTOS_TB/EstadosGuia")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetEstadosGuia()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_estados_guia_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Estados> _estados = new List<Estados>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _estados.Add(new Estados
                    {

                        Id = e["Id"].ToString(),
                        Nombre = e["Nombre"].ToString()
                    });
                }
                return Ok(_estados);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        #endregion
    }

}
