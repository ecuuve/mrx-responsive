﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_MANIFIESTO_VIAJES_TBController : ApiController
    {
        public class lista_viajes
        {
            public List<Viajes> viajes { set; get; }
            public string id { set; get; }
        }
        /// <summary>
        /// Inserta los viajes en la tabla manifiesto viajes
        /// </summary>
        /// <param name="viaje"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTO_VIAJES_TB/GuardarViaje")]
        public IHttpActionResult PostTX_MANIFIESTOS_TB(lista_viajes viajes)
        {
            if(viajes.viajes != null)
            {
                string error = "";
                foreach (Viajes viaje in viajes.viajes)
                {
                    //Get the connection string from the app.config            
                    var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                    OracleConnection cn = new OracleConnection(connectionString);
                    cn.Open();
                    OracleCommand objCmd = new OracleCommand();
                    objCmd.Connection = cn;
                    objCmd.CommandText = "mrx.TX_INS_MANIFIESTO_VIAJES_PR";
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("MVI_TMA_ID_MANIFIESTO_P", OracleDbType.Int32).Value = viaje.Id;
                    objCmd.Parameters.Add("MVI_VIAJE_P", OracleDbType.Varchar2).Value = viaje.Nombre;
                    objCmd.Parameters.Add("MVI_USU_REGISTRO_P", OracleDbType.Varchar2).Value = viaje.MVI_USU_REGISTRO;
                    try
                    {
                        objCmd.ExecuteNonQuery();
                        cn.Close();
                    }
                    catch (Exception ex)
                    {
                        error += ex.Message;
                    }
                }
                if (error == "")
                    return Ok(viajes.id);
                else
                    return BadRequest(error);
            }
            else
            {
                return Ok(viajes.id);
            }
            
        }

        [Route("api/TX_MANIFIESTO_VIAJES_TB/Viajes")]
        [ResponseType(typeof(Viajes))]
        public IHttpActionResult GetViajes(long? id_manifiesto, string viaje)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_MANIFIESTO_VIAJES_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("MVI_TMA_ID_MANIFIESTO_P", OracleDbType.Int32).Value = id_manifiesto;
            objCmd.Parameters.Add("MVI_VIAJE_P", OracleDbType.Varchar2).Value = viaje;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Viajes> _viajes = new List<Viajes>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _viajes.Add(new Viajes
                    {

                        Id = e["MVI_TMA_ID_MANIFIESTO"].ToString(),
                        Nombre = e["MVI_VIAJE"].ToString()
                    });
                }
                return Ok(_viajes);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        //Delete
        [Route("api/TX_MANIFIESTO_VIAJES_TB/BorrarViaje")]
        public IHttpActionResult PostBorrar(lista_viajes viajes)
        {
            if (viajes.id != null)
            {
                    //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_DEL_MANIFIESTO_VIAJES_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("MVI_TMA_ID_MANIFIESTO_P ", OracleDbType.Int32).Value = viajes.id;
                objCmd.Parameters.Add("MVI_VIAJE_P ", OracleDbType.Varchar2).Value = "";
                try
                {
                    objCmd.ExecuteNonQuery();
                    cn.Close();
                    return Ok(viajes.id);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Ok(viajes.id);
            }
        }
    }
}
