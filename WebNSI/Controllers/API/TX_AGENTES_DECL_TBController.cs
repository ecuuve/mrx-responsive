﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_AGENTES_DECL_TBController : ApiController
    {
        // GET: api/TX_AGENTES_DECL_TB
        [Route("api/TX_AGENTES_DECL_TB/GetFuncionarios")]
        [ResponseType(typeof(Funcionario))]
        public IHttpActionResult GetFuncionarios(string cod_declarante)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_fun_declarantes_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("adc_dct_declarante_id_p", OracleDbType.Varchar2).Value = cod_declarante;
           // objCmd.Parameters.Add("dct_nombre_p", OracleDbType.Varchar2).Value = nombre;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Funcionario> _Funcionarios = new List<Funcionario>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Funcionarios.Add(new Funcionario
                {
                    ADC_DCT_Id = e["ADC_DCT_DECLARANTE_ID"].ToString(),
                    ADC_Agente_Id = long.Parse(e["ADC_AGENTE_ID"].ToString()),
                    ADC_Nombre = e["ADC_NOMBRE"].ToString(),                  
                    ADC_DES_ESTADO= e["DES_ESTADO"].ToString(),
                });
            }
            return Ok(_Funcionarios);
        }

        [Route("api/TX_AGENTES_DECL_TB/GetFuncionarioDetalle")]
        [ResponseType(typeof(Funcionario))]
        public IHttpActionResult GetFuncionario(string cod_declarante, string cod_funcionario)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_det_fun_declarantes_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("adc_dct_declarante_id_p", OracleDbType.Varchar2).Value = cod_declarante;
            objCmd.Parameters.Add("adc_agente_id_p", OracleDbType.Varchar2).Value = cod_funcionario;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Funcionario> _Funcionarios = new List<Funcionario>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Funcionarios.Add(new Funcionario
                {
                    ADC_DCT_Id = e["ADC_DCT_DECLARANTE_ID"].ToString(),
                    ADC_Nombre = e["ADC_NOMBRE"].ToString(),
                    ADC_Telefono = e["ADC_TELEFONO"].ToString(),
                    ADC_Cedula = long.Parse(e["ADC_CEDULA"].ToString()),
                    ADC_Tipo = e["ADC_TIPO"].ToString(),
                    DES_Tipo = e["DES_TIPO"].ToString(),
                    ADC_Email = e["ADC_EMAIL"].ToString(),
                    ADC_Agente_Id = long.Parse( e["ADC_AGENTE_ID"].ToString()),
                    ADC_Estado_Tsm = e["ADC_ESTADO_TSM"].ToString(),
                    ADC_Estado_Tica = e["ADC_ESTADO_TICA"].ToString(),
                    ADC_Fch_Registro = e["ADC_FCH_REGISTRO"].ToString(),
                    ADC_Usu_Registro = e["ADC_USU_REGISTRO"].ToString(),

                });
            }
            return Ok(_Funcionarios);
        }

        [Route("api/TX_AGENTES_DECL_TB/GetTiposFuncionario")]
        [ResponseType(typeof(TipoFuncionario))]
        public IHttpActionResult GetTipoFuncionario()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_adc_tipo_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<TipoFuncionario> _TipoFuncionario = new List<TipoFuncionario>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TipoFuncionario.Add(new TipoFuncionario
                {
                    Id = r["id"].ToString(),
                    Nombre = r["nombre"].ToString()
                });
            }

            return Ok(_TipoFuncionario);
        }

        [Route("api/TX_AGENTES_DECL_TB/GetEstadoTica")]
        [ResponseType(typeof(EstadoTica))]
        public IHttpActionResult GetEstadoTica()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_dct_estado_tica_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<EstadoTica> _EstadoTica = new List<EstadoTica>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _EstadoTica.Add(new EstadoTica
                {
                    Id = r["id"].ToString(),
                    Nombre = r["nombre"].ToString()
                });
            }

            return Ok(_EstadoTica);
        }


        [ResponseType(typeof(Funcionario))]
        public IHttpActionResult PutTX_AGENTES_DECL_TB(string id, Funcionario func_p)
        {
            if (func_p.ADC_Estado_Tsm == null)
            {
                func_p.ADC_Estado_Tsm = "I";
            }

            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;

            objCmd.CommandText = "mrx.tx_upd_fun_declarantes_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("adc_dct_declarante_id_p", OracleDbType.Varchar2).Value = func_p.ADC_DCT_Id;
            objCmd.Parameters.Add("adc_estado_tica_p", OracleDbType.Varchar2).Value = func_p.ADC_Estado_Tica;
            objCmd.Parameters.Add("adc_cedula_p", OracleDbType.Long).Value = func_p.ADC_Cedula;

            objCmd.Parameters.Add("adc_estado_tsm_p", OracleDbType.Varchar2).Value = func_p.ADC_Estado_Tsm;
            objCmd.Parameters.Add("adc_agente_id_p", OracleDbType.Long).Value = func_p.ADC_Agente_Id;
            objCmd.Parameters.Add("adc_tipo_p", OracleDbType.Varchar2).Value = func_p.ADC_Tipo;

            objCmd.Parameters.Add("adc_nombre_p", OracleDbType.Varchar2).Value = func_p.ADC_Nombre;
            objCmd.Parameters.Add("adc_email_p", OracleDbType.Varchar2).Value = func_p.ADC_Email;
            objCmd.Parameters.Add("adc_telefono_p", OracleDbType.Long).Value = func_p.ADC_Telefono;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();

            Funcionario _Funcionario = new Funcionario();


            return Ok(_Funcionario);

        }

        // POST: api/TX_DECLARANTES_TB
        [ResponseType(typeof(Declarante))]
        public IHttpActionResult PostTX_AGENTES_DECL_TB(Funcionario func_p)
        {
            if (func_p.ADC_Estado_Tsm == null)
            {
                func_p.ADC_Estado_Tsm = "I";
            }
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_fun_declarantes_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("adc_dct_declarante_id_p", OracleDbType.Varchar2).Value = func_p.ADC_DCT_Id;
            objCmd.Parameters.Add("adc_estado_tica_p", OracleDbType.Varchar2).Value = func_p.ADC_Estado_Tica;
            objCmd.Parameters.Add("adc_cedula_p", OracleDbType.Long).Value = func_p.ADC_Cedula;

            objCmd.Parameters.Add("adc_estado_tsm_p", OracleDbType.Varchar2).Value = func_p.ADC_Estado_Tsm;
            objCmd.Parameters.Add("adc_agente_id_p", OracleDbType.Long).Value = null;
            objCmd.Parameters.Add("adc_tipo_p", OracleDbType.Varchar2).Value = func_p.ADC_Tipo;

            objCmd.Parameters.Add("dct_nombre_p", OracleDbType.Varchar2).Value = func_p.ADC_Nombre;
            objCmd.Parameters.Add("dct_email_p", OracleDbType.Varchar2).Value = func_p.ADC_Email;
            objCmd.Parameters.Add("dct_telefono_p", OracleDbType.Long).Value = func_p.ADC_Telefono;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = func_p.UserMERX;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Declarante _DeclaranteIn = new Declarante();

            return Ok(_DeclaranteIn);
        }

    }
}
