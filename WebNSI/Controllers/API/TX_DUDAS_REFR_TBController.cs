﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_DUDAS_REFR_TBController : ApiController
    {
        [Route("api/TX_DUDAS_REFR_TB/DudaRefrigeracion")]
        [ResponseType(typeof(DudasRefri))]
        public IHttpActionResult GetIndicacionRefrigeracion(long? tipo)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_dudas_refri_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tipo_duda_p", OracleDbType.Int32).Value = tipo;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<DudasRefri> _DudaRefrigeracion = new List<DudasRefri>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _DudaRefrigeracion.Add(new DudasRefri
                    {

                        Id = e["CODIGO"].ToString(),
                        Nombre = e["DESCRIPCION"].ToString()
                    });
                }
                return Ok(_DudaRefrigeracion);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
