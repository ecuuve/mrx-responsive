﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_TARIFAS_CLIENTE_TBController: ApiController
    {
        [Route("api/TX_TARIFAS_CLIENTE_TB/TarifaClienteInsertar")]
        public IHttpActionResult TarifaClienteInsertar(TARIFAS_CLIENTE_POST post)
        {
            if (post.TRC_CLIENTE_GUIA_NOMBRE.Length == 0)
            {
                return BadRequest("Cliente Guia/BL invalido");
            }

            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_TARIFAS_CLIENTE_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("TRC_CLIENTE_CONTENEDOR_P", OracleDbType.Varchar2).Value = post.TRC_CLIENTE_CONTENEDOR;
            objCmd.Parameters.Add("TRC_CLIENTE_GUIA_P", OracleDbType.Varchar2).Value = post.TRC_CLIENTE_GUIA;
            objCmd.Parameters.Add("TRC_TRF_ID_P", OracleDbType.Int32).Value = post.TRC_TRF_ID;
            objCmd.Parameters.Add("TRC_TIPO_CLIENTE_CONTENEDOR_P", OracleDbType.Varchar2).Value = post.TRC_CLIENTE_CONTENEDOR_TIPO;
            objCmd.Parameters.Add("TRC_USU_REGISTRO_P", OracleDbType.Varchar2).Value = post.TRC_USU_REGISTRO;

            try
            {
                objCmd.ExecuteNonQuery();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        [Route("api/TX_TARIFAS_CLIENTE_TB/busqueda")]
        public IHttpActionResult Busqueda(TARIFAS_CLIENTE_BUSQUEDA post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_TARIFA_CLIENTE_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("TRC_TRF_ID_P", OracleDbType.Int32).Value = post.B_TRC_TRF_ID;
            objCmd.Parameters.Add("TRC_CLIENTE_CONTENEDOR_P", OracleDbType.Int32).Value = post.B_TRC_CLIENTE_CONTENEDOR;
            objCmd.Parameters.Add("TRC_CLIENTE_GUIA_P", OracleDbType.Int32).Value = post.B_TRC_CLIENTE_GUIA;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();

                DataTable table = new DataTable();
                List<TARIFAS_CLIENTE> data = new List<TARIFAS_CLIENTE>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(table);

                cn.Close();

                foreach (DataRow r in table.Rows)
                {
                    data.Add(new TARIFAS_CLIENTE
                    {
                        TRC_CLIENTE_CONTENEDOR = r["TRC_CLIENTE_CONTENEDOR"].ToString(),
                        CLIENTE_CONTENEDOR = r["CLIENTE_CONTENEDOR"].ToString(),
                        TRC_TIPO_CLIENTE_CONTENEDOR = r["TRC_TIPO_CLIENTE_CONTENEDOR"].ToString(),
                        TRC_CLIENTE_GUIA = r["TRC_CLIENTE_GUIA"].ToString(),
                        CLIENTE_GUIA = r["CLIENTE_GUIA"].ToString(),
                        TRC_TRF_ID = r["TRC_TRF_ID"].ToString(),
                        TRC_USU_REGISTRO = r["TRC_USU_REGISTRO"].ToString(),
                        TRC_FCH_REGISTRO = r["TRC_FCH_REGISTRO"].ToString(),
                    });
                }

                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        
        [Route("api/TX_TARIFAS_CLIENTE_TB/TarifaClienteEliminar")]
        public IHttpActionResult TarifaClienteEliminar(TARIFAS_CLIENTE_POST post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_DEL_TARIFA_CLIENTE_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("TRC_CLIENTE_CONTENEDOR_P", OracleDbType.Varchar2).Value = post.TRC_CLIENTE_CONTENEDOR;
            objCmd.Parameters.Add("TRC_CLIENTE_GUIA_P", OracleDbType.Varchar2).Value = post.TRC_CLIENTE_GUIA;
            objCmd.Parameters.Add("TRC_TRF_ID_P", OracleDbType.Int32).Value = post.TRC_TRF_ID;

            try
            {
                objCmd.ExecuteNonQuery();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/TX_TARIFAS_CLIENTE_TB/consolidadores")]
        public IHttpActionResult Consolidadores(CONSOLIDADORES_POST post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONSOLIDADORES_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("CONSOLIDADOR_P", OracleDbType.Int32).Value = post.B_CONSOLIDADOR;
            objCmd.Parameters.Add("CON_NOMBRE_P", OracleDbType.Int32).Value = post.B_NOMBRE;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();

                DataTable table = new DataTable();
                List<CONSOLIDADORES> data = new List<CONSOLIDADORES>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(table);

                cn.Close();

                foreach (DataRow r in table.Rows)
                {
                    data.Add(new CONSOLIDADORES
                    {
                        ID = r["ID"].ToString(),
                        NOMBRE = r["NOMBRE"].ToString(),
                        CEDULA = r["CEDULA"].ToString(),
                    });
                }

                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}