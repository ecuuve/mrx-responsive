﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_PRE_BLOQUEOS_TBController : ApiController
    {
        [ResponseType(typeof(Pre_Bloqueo))]
        public IHttpActionResult PostTX_PRE_BLOQUEOS_TB_Guias(Pre_Bloqueo prebloqueo_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_pre_bloqueos_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Ipb_Guia_p", OracleDbType.Varchar2).Value = prebloqueo_p.IPB_GUIA;
            objCmd.Parameters.Add("Ipb_Csg_Consignatario_p", OracleDbType.Long).Value = prebloqueo_p.IPB_CSG_CONSIGNATARIO;
            objCmd.Parameters.Add("Ipb_Obs_p", OracleDbType.Varchar2).Value = prebloqueo_p.IPB_OBS;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = prebloqueo_p.USER_MERX;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();

            return Ok(prebloqueo_p);
        }


        [Route("api/TX_PRE_BLOQUEOS_TB/Consignatario")]
        [ResponseType(typeof(Consignatario))]
        public IHttpActionResult GetConsignatario(long? consignatario_p, string nomconsignatario_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_pre_consigna_tsm_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("csg_consignatario_p", OracleDbType.Int32).Value = consignatario_p;
            objCmd.Parameters.Add("csg_nombre_p", OracleDbType.Varchar2).Value = nomconsignatario_p; 
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Consignatario> _Consignatario = new List<Consignatario>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Consignatario.Add(new Consignatario
                {
                    Id = r["csg_consignatario"].ToString(),
                    Nombre = r["consignatario"].ToString()
                });
            }

            return Ok(_Consignatario);
        }

        [Route("api/TX_PRE_BLOQUEOS_TB/PrealertaBloqueo")]
        public IHttpActionResult GetPrealertaBloqueo(string guia)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_pre_bloq_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ien_guia_original_p", OracleDbType.Long).Value = guia;
            objCmd.Parameters.Add("existe_p", OracleDbType.Char, 1).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                string existe = objCmd.Parameters["existe_p"].Value.ToString();
                if (existe == "S")
                {
                    return Ok("S");
                }
                else
                {
                    return Ok("N");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
