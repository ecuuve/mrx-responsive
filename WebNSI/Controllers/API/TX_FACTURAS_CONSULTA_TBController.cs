﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_FACTURAS_CONSULTA_TBController : ApiController
    {

        #region "CONSULTAS"

        /// <summary>
        /// Consulta datos de la factura
        /// </summary>
        /// <param name="factura"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_CONSULTA_TB/GetObtieneFactura")]
        [ResponseType(typeof(FAC_ENCABEZADO))]
        public IHttpActionResult GetObtieneFactura(long? factura, string viaje, long? dua)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONS_FACTURA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("fac_factura_p", OracleDbType.Int32).Value = factura;
            objCmd.Parameters.Add("fac_viaje_transito_p", OracleDbType.Varchar2).Value = viaje;
            objCmd.Parameters.Add("fac_dua_salida_p", OracleDbType.Int32).Value = dua;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<FAC_ENCABEZADO> _DetEncab = new List<FAC_ENCABEZADO>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _DetEncab.Add(new FAC_ENCABEZADO
                {
                    FAC_FACTURA = e["FAC_FACTURA"].ToString() != "" ? long.Parse(e["FAC_FACTURA"].ToString()) : 0, 
                    FAC_DUA_SALIDA = e["FAC_DUA_SALIDA"].ToString(),
                    FAC_VIAJE_TRANSITO = e["FAC_VIAJE_TRANSITO"].ToString(),
                    FAC_FCH_DUA = e["FAC_FCH_DUA"].ToString(),
                    FAC_BULTOS_DUA = e["FAC_BULTOS_DUA"].ToString() != "" ? long.Parse(e["FAC_BULTOS_DUA"].ToString()) : 0,                   
                    FAC_DIN_TIPO_DOCUMENTO = e["FAC_BULTOS_DUA"].ToString(),
                    FAC_VALOR_CIF = e["FAC_VALOR_CIF"].ToString() != "" ? decimal.Parse(e["FAC_VALOR_CIF"].ToString()) : 0,                  
                    FAC_IMPUESTOS = e["FAC_IMPUESTOS"].ToString() != "" ? decimal.Parse(e["FAC_IMPUESTOS"].ToString()) : 0,
                    FAC_IMP_EXENTOS = e["FAC_IMP_EXENTOS"].ToString() != "" ? decimal.Parse(e["FAC_IMP_EXENTOS"].ToString()) : 0,
                    FAC_EXENTA = e["FAC_EXENTA"].ToString(),
                    FAC_NOTA_EXENCION = e["FAC_NOTA_EXENCION"].ToString(),
                    FAC_DESDE_EXENCION = e["FAC_DESDE_EXENCION"].ToString(),
                    FAC_HASTA_EXENCION = e["FAC_HASTA_EXENCION"].ToString(),
                    FAC_UBI_UBICACION = e["FAC_UBI_UBICACION"].ToString(),
                    FAC_MARCH_ELECTRONICO = e["FAC_MARCH_ELECTRONICO"].ToString(),
                    FAC_TRAMITE = e["FAC_TRAMITE"].ToString(),
                    FAC_CLIENTE_DUA = e["FAC_CLIENTE_DUA"].ToString(),
                    FAC_TIPO_CAMBIO = e["FAC_TIPO_CAMBIO"].ToString() != "" ? decimal.Parse(e["FAC_TIPO_CAMBIO"].ToString()) : 0,
                    FAC_DCT_DECLARANTE_ID = e["FAC_DCT_DECLARANTE_ID"].ToString(),
                    FAC_ADC_AGENTE_ID = e["FAC_ADC_AGENTE_ID"].ToString() != "" ? long.Parse(e["FAC_ADC_AGENTE_ID"].ToString()) : 0,
                    FAC_CUENTA = e["FAC_CUENTA"].ToString() != "" ? long.Parse(e["FAC_CUENTA"].ToString()) : 0,
                    FAC_CONTRIBUYENTE = e["FAC_CONTRIBUYENTE"].ToString(),
                    FAC_ESTADO = e["ESTADO"].ToString(),
                    FAC_IMPORTADOR = e["FAC_IMPORTADOR"].ToString(),
                    FAC_NOMBRE_CUENTA = e["FAC_NOMBRE_CUENTA"].ToString(),
                    FAC_NBR_CONTRIBUYENTE = e["FAC_NBR_CONTRIBUYENTE"].ToString(),
                    TAA_NOMBRE = e["TAA_NOMBRE"].ToString(),
                    DCT_NOMBRE = e["DCT_NOMBRE"].ToString(),
                    TDO_DESCRIPCION = e["TDO_DESCRIPCION"].ToString(),
                    FAC_FCH_DIGITO = e["FAC_FCH_DIGITO"].ToString(),
                    FAC_ELECTRONICA = e["FAC_ELECTRONICA"].ToString(),
                    FAC_NOMBRE_CHOFER = e["FAC_NOMBRE_CHOFER"].ToString(),
                    FAC_CEDULA_CHOFER = e["FAC_CEDULA_CHOFER"].ToString(),
                    FAC_PLACA_VEHICULO = e["FAC_PLACA_VEHICULO"].ToString(),
                    FAC_MARCHAMO1 = e["FAC_MARCHAMO1"].ToString(),
                    FAC_MARCHAMO2 = e["FAC_MARCHAMO2"].ToString(),
                    FAC_MARCHAMO3 = e["FAC_MARCHAMO3"].ToString(),
                    FAC_MARCHAMO4 = e["FAC_MARCHAMO4"].ToString(),
                    FAC_OBSERVACIONES = e["FAC_OBSERVACIONES"].ToString(),
                    FAC_CEDULA_FACTURA = e["FAC_CEDULA_FACTURA"].ToString(),
                    FAC_AUTORIZADO_POR = e["FAC_AUTORIZADO_POR"].ToString(),
                    FAC_CFR_FACTURA = e["FAC_CFR_FACTURA"].ToString(),
                    FAC_TRA_CHOFER = e["FAC_TRA_CHOFER"].ToString(),
                    FAC_TELEFONO = e["FAC_TRA_CHOFER"].ToString(),
                    FAC_NOMBRE_FACTURA = e["FAC_NOMBRE_FACTURA"].ToString(),
                    CONTADO = e["CONTADO"].ToString(),
                    FAC_TOTAL_FACTURA = e["FAC_TOTAL_FACTURA"].ToString() != "" ? decimal.Parse(e["FAC_TOTAL_FACTURA"].ToString()) : 0
                });
            }
            return Ok(_DetEncab);
        }

        /// <summary>
        /// Obtiene los movimientos de la factura
        /// </summary>
        /// <param name="factura"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_CONSULTA_TB/GetMovimientosFact")]
        [ResponseType(typeof(FAC_MOVIMIENTO))]
        public IHttpActionResult GetMovimientosFact(long factura)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONS_FACT_MOVS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("FMV_ID_P", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("FMV_FAC_FACTURA_P", OracleDbType.Int32).Value = factura;
            objCmd.Parameters.Add("FMV_MOVIMIENTO_P", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("FMV_IEN_ENTRADA_P", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<FAC_MOVIMIENTO> _DetMov = new List<FAC_MOVIMIENTO>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _DetMov.Add(new FAC_MOVIMIENTO
                {
                    FMV_ID = e["ID_MOVIMIENTO"].ToString(),
                    FMV_FAC_FACTURA = long.Parse(e["NUMERO_FACTURA"].ToString()),
                    FMV_MOVIMIENTO = long.Parse(e["NUMERO_MOVIMIENTO"].ToString()),
                    FMV_IEN_ENTRADA = long.Parse(e["NUMERO_ENTRADA"].ToString()),
                    FMV_BULTOS = long.Parse(e["BULTOS"].ToString()),
                    FMV_KILOS = decimal.Parse(e["KILOS"].ToString()),
                    FMV_VOLUMEN = decimal.Parse(e["VOLUMEN"].ToString()),
                    FMV_TME_MERCANCIA = long.Parse(e["CODIGO_MERCANCIA"].ToString()),
                    TME_DESCRIPCION = e["DESCRIPCION_MERCANCIA"].ToString(),
                    FMV_ITT_TARIFA = long.Parse(e["TIPO_TARIFA"].ToString()),
                    ITT_DESCRIPCION = e["DESCRIPCION_TIPO_TARIFA"].ToString(),
                    FMV_BASE_DESCUENTO = e["BASE_DESCUENTO"].ToString(),
                    FMV_PCT_DESCUENTO = decimal.Parse(e["PORCENTAJE_DESCUENTO"].ToString()),
                    FMV_MONTO_DESCUENTO = decimal.Parse(e["MONTO_DESCUENTO"].ToString()),
                    FMV_ASOCIE = e["ASOCIE"].ToString(),
                    FMV_MONTO_A_PAGAR = decimal.Parse(e["MONTO_A_PAGAR"].ToString()),
                    FMV_USU_REGISTRO = e["USUARIO_REGISTRO"].ToString(),
                    FMV_FCH_REGISTRO = e["FECHA_REGISTRO"].ToString(),
                });
            }
            return Ok(_DetMov);
        }

        /// <summary>
        /// Obtiene las tarimas del movimiento de la factura dada
        /// </summary>
        /// <param name="factura_p"></param>
        /// <param name="id_mov_p"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_CONSULTA_TB/GetTarimas")]
        [ResponseType(typeof(FAC_TARIMA))]
        public IHttpActionResult GetTarimas(long factura_p, long id_mov_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONS_FACT_TARIMAS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("FMV_FAC_FACTURA_P", OracleDbType.Int32).Value = factura_p;
            objCmd.Parameters.Add("FTA_FMV_ID_P", OracleDbType.Int32).Value = id_mov_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<FAC_TARIMA> _Tarimas = new List<FAC_TARIMA>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Tarimas.Add(new FAC_TARIMA
                {
                    ID_TARIMA = long.Parse(e["ID_TARIMA"].ToString()),
                    ID_MOVIMIENTO = long.Parse(e["ID_MOVIMIENTO"].ToString()),
                    ID_MERCANCIA = long.Parse(e["ID_MERCANCIA"].ToString()),
                    DESCRIPCION_MERCANCIA = e["DESCRIPCION_MERCANCIA"].ToString(),
                    BULTOS = long.Parse(e["BULTOS"].ToString()),
                    KILOS = decimal.Parse(e["KILOS"].ToString()),
                    VOLUMEN = decimal.Parse(e["VOLUMEN"].ToString()),
                    OBSERVACIONES = e["OBSERVACIONES"].ToString()
                });
            }
            return Ok(_Tarimas);
        }

        /// <summary>
        /// Obtiene los adicionales de la factura
        /// </summary>
        /// <param name="factura"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_CONSULTA_TB/GetAdicionales")]
        [ResponseType(typeof(FAC_RUBRO))]
        public IHttpActionResult GetAdicionales(long factura)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONS_FACT_RUBRO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("factura_p", OracleDbType.Int32).Value = factura;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<FAC_RUBRO> _Adicionales = new List<FAC_RUBRO>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Adicionales.Add(new FAC_RUBRO
                {
                    FCO_RUB_RUBRO = e["FCO_RUB_RUBRO"].ToString(),
                    FCO_MONTO = decimal.Parse(e["FCO_MONTO"].ToString())
                });
            }
            return Ok(_Adicionales);
        }

        /// <summary>
        /// Obtiene las facturas por movimiento
        /// </summary>
        /// <param name="movimiento"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_CONSULTA_TB/GetObtFactxMov")]
        [ResponseType(typeof(FAC_MOVIMIENTO))]
        public IHttpActionResult GetObtFactxMov(long movimiento)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_FACT_X_MOV_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("movimiento_p", OracleDbType.Int32).Value = movimiento;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<FAC_MOVIMIENTO> _DetMov = new List<FAC_MOVIMIENTO>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _DetMov.Add(new FAC_MOVIMIENTO
                {
                    FMV_FAC_FACTURA = long.Parse(e["FMV_FAC_FACTURA"].ToString()),
                    FMV_BULTOS = long.Parse(e["FMV_BULTOS"].ToString()),
                    FMV_KILOS = decimal.Parse(e["FMV_KILOS"].ToString()),
                    FMV_VOLUMEN = decimal.Parse(e["FMV_VOLUMEN"].ToString()),
                    TOTAL_FACTURA = decimal.Parse(e["FAC_TOTAL_FACTURA"].ToString()),
                    ESTADO_FAC = e["FAC_ESTADO"].ToString(),
                    FMV_FCH_REGISTRO = e["FMV_FCH_REGISTRO"].ToString()
                });
            }
            return Ok(_DetMov);
        }

        /// <summary>
        /// Realiza la clonación de la factura dada
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_CONSULTA_TB/ClonarFactura")]
        [ResponseType(typeof(string))]
        public IHttpActionResult Post_ClonarFactura(FAC_CLONACION post)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_CLONA_FACTURA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("factura_p", OracleDbType.Int32).Value = post.FAC_FACTURA_CLO;
            objCmd.Parameters.Add("FAC_CUENTA_P", OracleDbType.Int32).Value = post.FAC_CUENTA_CLO;
            objCmd.Parameters.Add("FAC_DUA_SALIDA_P", OracleDbType.Varchar2).Value = post.FAC_DUA_SALIDA_CLO;
            objCmd.Parameters.Add("FAC_DIN_TIPO_DOCUMENTO_P", OracleDbType.Varchar2).Value = post.FAC_DIN_TIPO_DOCUMENTO_CLO;
            objCmd.Parameters.Add("FAC_DCT_DECLARANTE_ID_P", OracleDbType.Varchar2).Value = post.FAC_DCT_DECLARANTE_ID_CLO;
            objCmd.Parameters.Add("FAC_ADC_AGENTE_ID_P", OracleDbType.Int32).Value = post.FAC_ADC_AGENTE_ID_CLO;
            objCmd.Parameters.Add("FAC_CONTRIBUYENTE_P", OracleDbType.Varchar2).Value = post.FAC_CONTRIBUYENTE_CLO;
            objCmd.Parameters.Add("FAC_USU_DIGITE_P", OracleDbType.Varchar2).Value = post.FAC_USU_DIGITO_CLO;
            objCmd.Parameters.Add("nueva_fact_p", OracleDbType.Int32).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
               
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

         
            cn.Close();

            var nueva_fac = objCmd.Parameters["nueva_fact_p"].Value.ToString();

            if (nueva_fac == "null")
            {
                nueva_fac = null;
            }

            return Ok(nueva_fac);
        }


        [Route("api/TX_FACTURAS_CONSULTA_TB/REVERSAR")]
        [ResponseType(typeof(string))]
        public IHttpActionResult Post_REVERSAR(long fac_p, string estado_p, string user_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_ESTADO_FACT_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("fac_factura_p", OracleDbType.Int32).Value = fac_p;
            objCmd.Parameters.Add("fac_estado_p", OracleDbType.Varchar2).Value = estado_p;
            objCmd.Parameters.Add("mot_motivo_anul_p", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("usu_modif_p", OracleDbType.Varchar2).Value = user_p;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();
            return Ok("Se reversó la factura");
        }
        #endregion

    }
}