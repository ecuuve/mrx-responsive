﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_IMPORTADORES_TBController : ApiController
    {
        [Route("api/TX_IMPORTADORES_TB/Importador")]
        [ResponseType(typeof(Importador))]
        public IHttpActionResult GetImportador(string id, string tipo)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_importadores_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("imp_cedula_p", OracleDbType.Long).Value = id;
            objCmd.Parameters.Add("imp_nombre_p", OracleDbType.Varchar2).Value = tipo;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
                //HttpResponseMessage responseMessage = ex.Message;
                //return new ResponseMessageResult(responseMessage);
                //throw new NotImplementedException();

            }
            

            DataTable datos2 = new DataTable();
            List<Importador> _Importador = new List<Importador>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Importador.Add(new Importador
                {
                    Id = r["IMP_CEDULA"].ToString(),
                    Nombre = r["NOMIMPORT"].ToString()
                });
            }

            return Ok(_Importador);
        }

        [Route("api/TX_IMPORTADORES_TB/Importadores")]
        [ResponseType(typeof(Importador))]
        public IHttpActionResult GetImportadores(string id, string nombre)
        {
            //string id = "";
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_importadores_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("imp_cedula_p", OracleDbType.Varchar2).Value = id;
            objCmd.Parameters.Add("imp_nombre_p", OracleDbType.Varchar2).Value = nombre;
            
            //objCmd.Parameters.Add("tipo_consulta_p", OracleDbType.Varchar2).Value = 'T';
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Importador> _Importador = new List<Importador>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();

                foreach (DataRow r in datos2.AsEnumerable().Take(300))
                {
                    _Importador.Add(new Importador
                    {
                        Id = r["IMP_CEDULA"].ToString(),
                        Nombre = r["NOMIMPORT"].ToString()
                    });
                }

                return Ok(_Importador);
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_IMPORTADORES_TB/TipoRevision")]
        [ResponseType(typeof(TipoRevision))]
        public IHttpActionResult GetTipoRevision()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_lv_tipo_rev_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<TipoRevision> _TipoRevision = new List<TipoRevision>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TipoRevision.Add(new TipoRevision
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_TipoRevision);
        }

        [Route("api/TX_IMPORTADORES_TB/TipoCliente")]
        [ResponseType(typeof(TipoCliente))]
        public IHttpActionResult GetTipoCliente()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_lv_tipo_cliente_pre";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<TipoCliente> _TipoCliente = new List<TipoCliente>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TipoCliente.Add(new TipoCliente
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_TipoCliente);
        }

        [Route("api/TX_IMPORTADORES_TB/TipoPersona")]
        [ResponseType(typeof(TipoPersona))]
        public IHttpActionResult GetTipoPersona()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_lv_tipo_persona_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<TipoPersona> _TipoPersona = new List<TipoPersona>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TipoPersona.Add(new TipoPersona
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_TipoPersona);
        }

        [Route("api/TX_IMPORTADORES_TB/Agencia")]
        [ResponseType(typeof(Agencia))]
        public IHttpActionResult GetAgencia(string id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_agencia_adu_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("agencia_p", OracleDbType.Long).Value = id;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Agencia> _Agencia = new List<Agencia>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Agencia.Add(new Agencia
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Agencia);
        }

        [Route("api/TX_IMPORTADORES_TB/Agente")]
        [ResponseType(typeof(Agente))]
        public IHttpActionResult GetAgente( string id, string agencia, string tipo)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_agentes_adu_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("agente_p", OracleDbType.Long).Value = id;
            objCmd.Parameters.Add("agencia_p", OracleDbType.Long).Value = agencia;
            objCmd.Parameters.Add("tipo_persona_p", OracleDbType.Varchar2).Value = tipo;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Agente> _Agente = new List<Agente>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Agente.Add(new Agente
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Agente);
        }


        [Route("api/TX_IMPORTADORES_TB/Autoridades")]
        [ResponseType(typeof(Autoridades))]
        public IHttpActionResult GetAutoridades()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_AUTORIDADES_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Autoridades> _Autoridad= new List<Autoridades>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Autoridad.Add(new Autoridades
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Autoridad);
        }
    }
}