﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using WebNSI.Models;



namespace WebNSI.Controllers.API
{
    public class TX_ENTRADAS_TB_IngresoController : ApiController
    {
       

        #region Consultas de guía

        /// <summary>
        /// Consulta de Guías
        /// </summary>
        /// <param name="manifiesto"></param>
        /// <param name="guiaOrig"></param>
        /// <param name="id"></param>
        /// <param name="aerolinea"></param>
        /// <returns></returns>
        [Route("api/TX_ENTRADAS_TB_Ingreso/Guias")]
        [ResponseType(typeof(Guia))]
        public IHttpActionResult GetGuias(string manifiesto, string guiaOrig, long? entrada, long? consolidador, string fecha, string estado)
        {
            if (manifiesto != "undefined")
            {
                //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.tx_qry_ingreso_guia_pr";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("ien_manifiesto_p", OracleDbType.Varchar2).Value = manifiesto;
                objCmd.Parameters.Add("ien_guia_original_p", OracleDbType.Varchar2).Value = guiaOrig;
                objCmd.Parameters.Add("ien_entrada_p", OracleDbType.Int32).Value = entrada;
                objCmd.Parameters.Add("ien_con_nconsolidador_p", OracleDbType.Int32).Value = consolidador;
                objCmd.Parameters.Add("ien_fch_digitacion_p", OracleDbType.Varchar2).Value = fecha;
                objCmd.Parameters.Add("ien_estado", OracleDbType.Varchar2).Value = estado;
                objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                try
                {
                    objCmd.ExecuteNonQuery();
                    DataTable datos2 = new DataTable();
                    List<Guia> _Entradas = new List<Guia>();

                    OracleDataAdapter objAdapter = new OracleDataAdapter();
                    objAdapter.SelectCommand = objCmd;
                    objAdapter.Fill(datos2);

                    cn.Close();
                    foreach (DataRow r in datos2.Rows)
                    {
                        _Entradas.Add(new Guia
                        {
                            IEN_TMA_ID_MANIFIESTO = r["IEN_TMA_ID_MANIFIESTO"].ToString(),
                            IEN_PUESTO_CHEQUEO = r["IEN_PUESTO_CHEQUEO"].ToString(),
                            IEN_ENTRADA = r["IEN_ENTRADA"].ToString(),
                            IEN_GUIA_ORIGINAL = r["IEN_GUIA_ORIGINAL"].ToString(),
                            IEN_GUIA = r["IEN_GUIA"].ToString(),
                            IEN_GUIA_MASTER = r["IEN_GUIA_MASTER"].ToString(),
                            IEN_DIN_TIPO_DOCUMENTO = r["IEN_DIN_TIPO_DOCUMENTO"].ToString(),
                            IEN_DUA = r["IEN_DUA"].ToString(),
                            IEN_DUA_R = r["DUA"].ToString(),
                            IEN_DUA_Z = r["DUA"].ToString(),
                            IEN_NUMERO_VIAJE = r["IEN_NUMERO_VIAJE"].ToString(),
                            IEN_ANTECESOR = r["IEN_ANTECESOR"].ToString(),
                            IEN_IMP_CEDULA = r["IEN_IMP_CEDULA"].ToString(),
                            IEN_IMPORTADOR = r["IEN_IMPORTADOR"].ToString(),
                            IEN_CON_NCONSOLIDADOR = r["IEN_CON_NCONSOLIDADOR"].ToString(),
                            CONSOLIDADOR = r["CONSOLIDADOR"].ToString(),
                            IEN_BULTOS_MANIFESTADOS = r["IEN_BULTOS_MANIFESTADOS"].ToString(),
                            IEN_KILOS_MANIFESTADOS = r["IEN_KILOS_MANIFESTADOS"].ToString(),
                            IEN_VOLUMEN_MANIF = r["IEN_VOLUMEN_MANIF"].ToString(),
                            IEN_BULTOS_INGRESADOS = r["IEN_BULTOS_INGRESADOS"].ToString(),
                            IEN_KILOS_INGRESADOS = r["IEN_KILOS_INGRESADOS"].ToString(),
                            IEN_VOLUMEN_INGRESADO = r["IEN_VOLUMEN_INGRESADO"].ToString(),
                            IEN_DESCRIPCION_MANIF = r["IEN_DESCRIPCION_MANIF"].ToString(),
                            IEN_OBSERVACIONES = r["IEN_OBSERVACIONES"].ToString(),
                            IEN_UBICACION_INGRESO = r["IEN_UBICACION_INGRESO"].ToString(),
                            IEN_UBICACION_TRANSITO = r["IEN_UBICACION_TRANSITO"].ToString(),
                            IEN_ES_COURIER = r["IEN_ES_COURIER"].ToString(),
                            IEN_DUDA_REFRI = r["IEN_DUDA_REFRI"].ToString(),
                            IEN_TIPO_DUDA_REF = r["IEN_TIPO_DUDA_REF"].ToString(),
                            IEN_ES_FARMA = r["IEN_ES_FARMA"].ToString(),
                            IEN_CLASE = r["IEN_CLASE"].ToString(),
                            IEN_BOLETA_CLASE = r["IEN_BOLETA_CLASE"].ToString(),
                            IEN_ACTA_DESCARGA = r["IEN_ACTA_DESCARGA"].ToString(),
                            IEN_PEND_DESCONSOLIDAR = r["IEN_PEND_DESCONSOLIDAR"].ToString(),
                            IEN_SOLICITUD = r["IEN_SOLICITUD"].ToString(),
                            IEN_TEXTO_ACTA = r["IEN_TEXTO_ACTA"].ToString(),
                            IEN_BOD_NBODEGA = r["IEN_BOD_NBODEGA"].ToString(),
                            IEN_USU_DIGITO = r["IEN_USU_DIGITO"].ToString(),
                            IEN_FCH_DIGITACION = r["IEN_FCH_DIGITACION"].ToString(),
                            IEN_ESTADO = r["IEN_ESTADO"].ToString(),
                            TOT_BULTOS = r["TOT_BULTOS"].ToString(),
                            TOT_KILOS = r["TOT_KILOS"].ToString(),
                            TOT_VOLUMEN = r["TOT_VOLUMEN"].ToString(),
                            IEN_MANIFIESTO = r["IEN_MANIFIESTO"].ToString(),
                            IEN_MANIFIESTO_Z = r["IEN_MANIFIESTO"].ToString(),                         
                            CONTENEDOR = r["CONTENEDOR"].ToString(),
                            IEN_GUIA_ORIGINAL_Z = r["IEN_GUIA_ORIGINAL"].ToString(),
                            CANT_TARIMAS = r["CANT_TARIMAS"].ToString(),
                            TOT_BULTOS_Z = r["TOT_BULTOS"].ToString(),
                            TOT_KILOS_Z = r["TOT_KILOS"].ToString(),
                            IEN_LINEA_GUIA = r["IEN_LINEA_GUIA"].ToString()

                        });
                    }
                    return Ok(_Entradas);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);

                }
            }
            return BadRequest("Debe indicar un manifiesto");
        }

        /// <summary>
        /// Consulta de Guías
        /// </summary>
        /// <param name="manifiesto"></param>
        /// <param name="guiaOrig"></param>
        /// <param name="id"></param>
        /// <param name="aerolinea"></param>
        /// <returns></returns>
        [Route("api/TX_ENTRADAS_TB_Ingreso/Guias_Consulta")]
        [ResponseType(typeof(Guias_Consulta))]
        public IHttpActionResult GetGuiasConsulta(string manifiesto, string guiaOrig, long? entrada, long? consolidador, string fecha, string estado)
        {
            if (manifiesto != "undefined")
            {
                //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_QRY_CON_GUIAS_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("ien_manifiesto_p", OracleDbType.Varchar2).Value = manifiesto;
                objCmd.Parameters.Add("ien_guia_original_p", OracleDbType.Varchar2).Value = guiaOrig;
                objCmd.Parameters.Add("ien_entrada_p", OracleDbType.Int32).Value = entrada;
                objCmd.Parameters.Add("ien_con_nconsolidador_p", OracleDbType.Int32).Value = consolidador;
                objCmd.Parameters.Add("ien_fch_digitacion_p", OracleDbType.Varchar2).Value = fecha;
                objCmd.Parameters.Add("ien_estado", OracleDbType.Varchar2).Value = estado;
                objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                try
                {
                    objCmd.ExecuteNonQuery();
                    DataTable datos2 = new DataTable();
                    List<Guias_Consulta> _list = new List<Guias_Consulta>();

                    OracleDataAdapter objAdapter = new OracleDataAdapter();
                    objAdapter.SelectCommand = objCmd;
                    objAdapter.Fill(datos2);

                    cn.Close();
                    foreach (DataRow r in datos2.Rows)
                    {
                        _list.Add(new Guias_Consulta
                        {
                            IEN_MANIFIESTO = r["IEN_MANIFIESTO"].ToString(),
                            IEN_TMA_ID_MANIFIESTO = r["IEN_TMA_ID_MANIFIESTO"].ToString(),
                            IEN_GUIA = r["IEN_GUIA"].ToString(),
                            IEN_GUIA_ORIGINAL = r["IEN_GUIA_ORIGINAL"].ToString(),
                            IEN_ENTRADA = r["IEN_ENTRADA"].ToString(),
                            IEN_FCH_MANIFIESTO = r["IEN_FCH_MANIFIESTO"].ToString(),
                            IEN_FCH_INGRESO = r["IEN_FCH_INGRESO"].ToString(),
                            IEN_FCH_DIGITACION = r["IEN_FCH_DIGITACION"].ToString(),
                            IEN_GUIA_MASTER = r["IEN_GUIA_MASTER"].ToString(),
                            IEN_DUA = r["IEN_DUA"].ToString(),
                            IEN_DUA_R = r["DUA"].ToString(),
                            IEN_DUA_Z = r["DUA"].ToString(),
                            IEN_AER_NAEROLINEA = r["IEN_AER_NAEROLINEA"].ToString(),
                            AEROLINEA = r["AEROLINEA"].ToString(),
                            IEN_PTO_PUERTO = r["IEN_PTO_PUERTO"].ToString(),
                            COD_PUERTO = r["COD_PUERTO"].ToString(),
                            PUERTO = r["PUERTO"].ToString(),
                            IEN_TDO_DOC_HIJA = r["IEN_TDO_DOC_HIJA"].ToString(),
                            DOC_HIJA = r["DOC_HIJA"].ToString(),
                            IEN_DIN_TIPO_DOCUMENTO = r["IEN_DIN_TIPO_DOCUMENTO"].ToString(),
                            DESCDOC = r["DESCDOC"].ToString(),
                            IEN_ANTECESOR = r["IEN_ANTECESOR"].ToString(),
                            IEN_FUN_FUNCIONARIO = r["IEN_FUN_FUNCIONARIO"].ToString(),
                            DESFUNCIONARIO = r["DESFUNCIONARIO"].ToString(),
                            IEN_CON_NCONSOLIDADOR = r["IEN_CON_NCONSOLIDADOR"].ToString(),
                            CONSOLIDADOR = r["CONSOLIDADOR"].ToString(),
                            IEN_CONSIGNATARIO = r["IEN_CONSIGNATARIO"].ToString(),
                            IEN_ES_COURIER = r["IEN_ES_COURIER"].ToString(),
                            IEN_ES_FARMA = r["IEN_ES_FARMA"].ToString(),
                            IEN_IMP_CEDULA = r["IEN_IMP_CEDULA"].ToString(),
                            IEN_IMPORTADOR = r["IEN_IMPORTADOR"].ToString(),
                            IEN_BULTOS_MANIFESTADOS = r["IEN_BULTOS_MANIFESTADOS"].ToString(),
                            IEN_KILOS_MANIFESTADOS = r["IEN_KILOS_MANIFESTADOS"].ToString(),
                            IEN_VOLUMEN_MANIF = r["IEN_VOLUMEN_MANIF"].ToString(),
                            IEN_BULTOS_INGRESADOS = r["IEN_BULTOS_INGRESADOS"].ToString(),
                            IEN_KILOS_INGRESADOS = r["IEN_KILOS_INGRESADOS"].ToString(),
                            IEN_VOLUMEN_INGRESADO = r["IEN_VOLUMEN_INGRESADO"].ToString(),
                            IEN_MODALIDAD_TRANSPORTE = r["IEN_MODALIDAD_TRANSPORTE"].ToString(),
                            IEN_DESCRIPCION_MANIF = r["IEN_DESCRIPCION_MANIF"].ToString(),
                            IEN_OBSERVACIONES = r["IEN_OBSERVACIONES"].ToString(),
                            IEN_UBICACION_INGRESO = r["IEN_UBICACION_INGRESO"].ToString(),
                            UBICACION_INGRESO = r["UBICACION_INGRESO"].ToString(),
                            IEN_CLASE = r["IEN_CLASE"].ToString(),
                            IEN_BOLETA_CLASE = r["IEN_BOLETA_CLASE"].ToString(),
                            IEN_BOD_NBODEGA = r["IEN_BOD_NBODEGA"].ToString(),
                            IEN_UBICACION_TRANSITO = r["IEN_UBICACION_TRANSITO"].ToString(),
                            UBITRANSITO = r["UBITRANSITO"].ToString(),
                            IEN_DUDA_REFRI = r["IEN_DUDA_REFRI"].ToString(),
                            IEN_TIPO_DUDA_REF = r["IEN_TIPO_DUDA_REF"].ToString(),
                            DUDA_REF = r["DUDA_REF"].ToString(),
                            IEN_NUMERO_VIAJE = r["IEN_NUMERO_VIAJE"].ToString(),
                            IEN_ACTA_DESCARGA = r["IEN_ACTA_DESCARGA"].ToString(),
                            IEN_TEXTO_ACTA = r["IEN_TEXTO_ACTA"].ToString(),
                            IEN_SOLICITUD = r["IEN_SOLICITUD"].ToString(),
                            IEN_PEND_DESCONSOLIDAR = r["IEN_PEND_DESCONSOLIDAR"].ToString(),
                            IEN_USU_DIGITO = r["IEN_USU_DIGITO"].ToString(),
                            IEN_ESTADO = r["IEN_ESTADO"].ToString(),
                            BLOQUEO = r["BLOQUEO"].ToString(),
                            MUESTRA = r["MUESTRA"].ToString(),
                            TOT_BULTOS = r["TOT_BULTOS"].ToString(),
                            TOT_KILOS = r["TOT_KILOS"].ToString(),
                            IEN_PUESTO_CHEQUEO = r["IEN_PUESTO_CHEQUEO"].ToString(),
                            IEN_LINEA_GUIA = r["IEN_LINEA_GUIA"].ToString(),
                            TOT_VOLUMEN = r["TOT_VOLUMEN"].ToString()
                        });
                    }
                    return Ok(_list);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);

                }
            }
            return BadRequest("Debe indicar un manifiesto");
        }

        /// <summary>
        /// Consulta de Guías para Modificar
        /// </summary>
        /// <param name="manifiesto"></param>
        /// <param name="guiaOrig"></param>
        /// <param name="id"></param>
        /// <param name="aerolinea"></param>
        /// <returns></returns>
        [Route("api/TX_ENTRADAS_TB_Ingreso/Guias_Modificar")]
        [ResponseType(typeof(Guias_Modificar))]
        public IHttpActionResult GetGuiasModificar(string manifiesto, string guiaOrig, long? entrada, long? consolidador, string fecha, string estado)
        {
            if (manifiesto != "undefined")
            {
                //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_QRY_MODI_GUIAS_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("ien_manifiesto_p", OracleDbType.Varchar2).Value = manifiesto;
                objCmd.Parameters.Add("ien_guia_original_p", OracleDbType.Varchar2).Value = guiaOrig;
                objCmd.Parameters.Add("ien_entrada_p", OracleDbType.Int32).Value = entrada;
                objCmd.Parameters.Add("ien_con_nconsolidador_p", OracleDbType.Int32).Value = consolidador;
                objCmd.Parameters.Add("ien_fch_digitacion_p", OracleDbType.Varchar2).Value = fecha;
                objCmd.Parameters.Add("ien_estado", OracleDbType.Varchar2).Value = estado;
                objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                try
                {
                    objCmd.ExecuteNonQuery();
                    DataTable datos2 = new DataTable();
                    List<Guias_Modificar> _list = new List<Guias_Modificar>();

                    OracleDataAdapter objAdapter = new OracleDataAdapter();
                    objAdapter.SelectCommand = objCmd;
                    objAdapter.Fill(datos2);

                    cn.Close();
                    foreach (DataRow r in datos2.Rows)
                    {
                        _list.Add(new Guias_Modificar
                        {
                            IEN_MANIFIESTO = r["IEN_MANIFIESTO"].ToString(),
                            IEN_TMA_ID_MANIFIESTO = r["IEN_TMA_ID_MANIFIESTO"].ToString(),
                            IEN_GUIA = r["IEN_GUIA"].ToString(),
                            IEN_GUIA_ORIGINAL = r["IEN_GUIA_ORIGINAL"].ToString(),
                            IEN_ENTRADA = r["IEN_ENTRADA"].ToString(),
                            IEN_FCH_MANIFIESTO = r["IEN_FCH_MANIFIESTO"].ToString(),
                            IEN_FCH_INGRESO = r["IEN_FCH_INGRESO"].ToString(),
                            IEN_FCH_DIGITACION = r["IEN_FCH_DIGITACION"].ToString(),
                            IEN_GUIA_MASTER = r["IEN_GUIA_MASTER"].ToString(),
                            IEN_DUA = r["IEN_DUA"].ToString(),
                            IEN_AER_NAEROLINEA = r["IEN_AER_NAEROLINEA"].ToString(),
                            AEROLINEA = r["AEROLINEA"].ToString(),
                            IEN_PTO_PUERTO = r["IEN_PTO_PUERTO"].ToString(),
                            COD_PUERTO = r["COD_PUERTO"].ToString(),
                            PUERTO = r["PUERTO"].ToString(),
                            IEN_TDO_DOC_HIJA = r["IEN_TDO_DOC_HIJA"].ToString(),
                            DOC_HIJA = r["DOC_HIJA"].ToString(),
                            IEN_DIN_TIPO_DOCUMENTO = r["IEN_DIN_TIPO_DOCUMENTO"].ToString(),
                            DESCDOC = r["DESCDOC"].ToString(),
                            IEN_ANTECESOR = r["IEN_ANTECESOR"].ToString(),
                            IEN_FUN_FUNCIONARIO = r["IEN_FUN_FUNCIONARIO"].ToString(),
                            DESFUNCIONARIO = r["DESFUNCIONARIO"].ToString(),
                            IEN_CON_NCONSOLIDADOR = r["IEN_CON_NCONSOLIDADOR"].ToString(),
                            CONSOLIDADOR = r["CONSOLIDADOR"].ToString(),
                            IEN_CONSIGNATARIO = r["IEN_CONSIGNATARIO"].ToString(),
                            IEN_ES_COURIER = r["IEN_ES_COURIER"].ToString(),
                            IEN_ES_FARMA = r["IEN_ES_FARMA"].ToString(),
                            IEN_IMP_CEDULA = r["IEN_IMP_CEDULA"].ToString(),
                            IEN_IMPORTADOR = r["IEN_IMPORTADOR"].ToString(),
                            IEN_BULTOS_MANIFESTADOS = r["IEN_BULTOS_MANIFESTADOS"].ToString(),
                            IEN_KILOS_MANIFESTADOS = r["IEN_KILOS_MANIFESTADOS"].ToString(),
                            IEN_VOLUMEN_MANIF = r["IEN_VOLUMEN_MANIF"].ToString(),
                            IEN_BULTOS_INGRESADOS = r["IEN_BULTOS_INGRESADOS"].ToString(),
                            IEN_KILOS_INGRESADOS = r["IEN_KILOS_INGRESADOS"].ToString(),
                            IEN_VOLUMEN_INGRESADO = r["IEN_VOLUMEN_INGRESADO"].ToString(),
                            IEN_MODALIDAD_TRANSPORTE = r["IEN_MODALIDAD_TRANSPORTE"].ToString(),
                            IEN_DESCRIPCION_MANIF = r["IEN_DESCRIPCION_MANIF"].ToString(),
                            IEN_OBSERVACIONES = r["IEN_OBSERVACIONES"].ToString(),
                            IEN_UBICACION_INGRESO = r["IEN_UBICACION_INGRESO"].ToString(),
                            UBICACION_INGRESO = r["UBICACION_INGRESO"].ToString(),
                            IEN_CLASE = r["IEN_CLASE"].ToString(),
                            IEN_BOLETA_CLASE = r["IEN_BOLETA_CLASE"].ToString(),
                            IEN_BOD_NBODEGA = r["IEN_BOD_NBODEGA"].ToString(),
                            IEN_UBICACION_TRANSITO = r["IEN_UBICACION_TRANSITO"].ToString(),
                            UBITRANSITO = r["UBITRANSITO"].ToString(),
                            IEN_DUDA_REFRI = r["IEN_DUDA_REFRI"].ToString(),
                            IEN_TIPO_DUDA_REF = r["IEN_TIPO_DUDA_REF"].ToString(),
                            DUDA_REF = r["DUDA_REF"].ToString(),
                            IEN_NUMERO_VIAJE = r["IEN_NUMERO_VIAJE"].ToString(),
                            IEN_ACTA_DESCARGA = r["IEN_ACTA_DESCARGA"].ToString(),
                            IEN_TEXTO_ACTA = r["IEN_TEXTO_ACTA"].ToString(),
                            IEN_SOLICITUD = r["IEN_SOLICITUD"].ToString(),
                            IEN_PEND_DESCONSOLIDAR = r["IEN_PEND_DESCONSOLIDAR"].ToString(),
                            IEN_USU_DIGITO = r["IEN_USU_DIGITO"].ToString(),
                            IEN_ESTADO = r["IEN_ESTADO"].ToString(),
                            TOT_BULTOS = r["TOT_BULTOS"].ToString(),
                            TOT_KILOS = r["TOT_KILOS"].ToString(),
                            IEN_PUESTO_CHEQUEO = r["IEN_PUESTO_CHEQUEO"].ToString(),
                            MUESTRA = r["MUESTRA"].ToString(),
                            BLOQUEO = r["BLOQUEO"].ToString(),
                            IEN_LINEA_GUIA = r["IEN_LINEA_GUIA"].ToString(),
                            TOT_VOLUMEN = r["TOT_VOLUMEN"].ToString()
                        });
                    }
                    return Ok(_list);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);

                }
            }
            return BadRequest("Debe indicar un manifiesto");
        }

        [Route("api/TX_ENTRADAS_TB_Ingreso/Guias_Modificar_Post")]
        public IHttpActionResult PostManifiestos_Modificar_Post(long id, Guias_Modificar_post post)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_MODI_GUIAS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("IEN_TMA_ID_MANIFIESTO_P", OracleDbType.Int32).Value = post.IEN_TMA_ID_MANIFIESTO.AsInt();
            objCmd.Parameters.Add("IEN_PUESTO_CHEQUEO_P", OracleDbType.Int32).Value = post.IEN_PUESTO_CHEQUEO.AsInt();
            objCmd.Parameters.Add("IEN_ENTRADA_P", OracleDbType.Int32).Value = post.IEN_ENTRADA.AsInt();
            objCmd.Parameters.Add("IEN_GUIA_ORIGINAL_P", OracleDbType.Varchar2).Value = post.IEN_GUIA_ORIGINAL;
            objCmd.Parameters.Add("IEN_GUIA_P", OracleDbType.Varchar2).Value = post.IEN_GUIA;
            objCmd.Parameters.Add("IEN_GUIA_MASTER_P", OracleDbType.Varchar2).Value = post.IEN_GUIA_MASTER;
            objCmd.Parameters.Add("IEN_DIN_TIPO_DOCUMENTO_P", OracleDbType.Varchar2).Value = post.IEN_DIN_TIPO_DOCUMENTO;
            objCmd.Parameters.Add("IEN_DUA_P", OracleDbType.Varchar2).Value = post.IEN_DUA;
            objCmd.Parameters.Add("IEN_NUMERO_VIAJE_P", OracleDbType.Int32).Value = post.IEN_NUMERO_VIAJE.AsInt();
            objCmd.Parameters.Add("IEN_ANTECESOR_P", OracleDbType.Int32).Value = post.IEN_ANTECESOR.AsInt();
            objCmd.Parameters.Add("IEN_IMP_CEDULA_P", OracleDbType.Varchar2).Value = post.IEN_IMP_CEDULA;
            objCmd.Parameters.Add("IEN_CONSIGNATARIO_P", OracleDbType.Varchar2).Value = post.CONSOLIDADOR;
            objCmd.Parameters.Add("IEN_CON_NCONSOLIDADOR_P", OracleDbType.Int32).Value = post.IEN_CON_NCONSOLIDADOR.AsInt();
            objCmd.Parameters.Add("IEN_BULTOS_MANIFESTADOS_P", OracleDbType.Int32).Value = post.IEN_BULTOS_MANIFESTADOS.AsInt();
            objCmd.Parameters.Add("IEN_KILOS_MANIFESTADOS_P", OracleDbType.Int32).Value = post.IEN_KILOS_MANIFESTADOS.AsInt();
            objCmd.Parameters.Add("IEN_VOLUMEN_MANIFIESTADO_P", OracleDbType.Decimal).Value = post.IEN_VOLUMEN_MANIF.AsDecimal();
            objCmd.Parameters.Add("IEN_BULTOS_INGRESADOS_P", OracleDbType.Int32).Value = post.IEN_BULTOS_INGRESADOS.AsInt();
            objCmd.Parameters.Add("IEN_KILOS_INGRESADOS_P", OracleDbType.Int32).Value = post.IEN_KILOS_INGRESADOS.AsInt();
            objCmd.Parameters.Add("IEN_VOLUMEN_INGRESADO_P", OracleDbType.Decimal).Value = post.IEN_VOLUMEN_INGRESADO.AsDecimal();
            objCmd.Parameters.Add("IEN_DESCRIPCION_MANIF_P", OracleDbType.Varchar2).Value = post.IEN_DESCRIPCION_MANIF;
            objCmd.Parameters.Add("IEN_OBSERVACIONES_P", OracleDbType.Varchar2).Value = post.IEN_OBSERVACIONES;
            objCmd.Parameters.Add("IEN_UBICACION_INGRESO_P", OracleDbType.Varchar2).Value = post.IEN_UBICACION_INGRESO;
            objCmd.Parameters.Add("IEN_UBICACION_TRANSITO_P", OracleDbType.Varchar2).Value = post.IEN_UBICACION_TRANSITO;
            objCmd.Parameters.Add("IEN_ES_COURIER_P", OracleDbType.Varchar2).Value = post.IEN_ES_COURIER;
            objCmd.Parameters.Add("IEN_DUDA_REFRI_P", OracleDbType.Varchar2).Value = post.IEN_DUDA_REFRI;
            objCmd.Parameters.Add("IEN_TIPO_DUDA_REF_P", OracleDbType.Int32).Value = post.IEN_TIPO_DUDA_REF.AsInt();
            objCmd.Parameters.Add("IEN_ES_FARMA_P", OracleDbType.Varchar2).Value = post.IEN_ES_FARMA;
            objCmd.Parameters.Add("IEN_CLASE_P", OracleDbType.Varchar2).Value = post.IEN_CLASE;
            objCmd.Parameters.Add("IEN_BOLETA_CLASE_P", OracleDbType.Varchar2).Value = post.IEN_BOLETA_CLASE;
            objCmd.Parameters.Add("IEN_ACTA_DESCARGA_P", OracleDbType.Varchar2).Value = post.IEN_ACTA_DESCARGA;
            objCmd.Parameters.Add("IEN_PEND_DESCONSOLIDAR_P", OracleDbType.Varchar2).Value = post.IEN_PEND_DESCONSOLIDAR;
            objCmd.Parameters.Add("IEN_SOLICITUD_P", OracleDbType.Varchar2).Value = post.IEN_SOLICITUD;
            objCmd.Parameters.Add("IEN_TEXTO_ACTA_P", OracleDbType.Varchar2).Value = post.IEN_TEXTO_ACTA;
            objCmd.Parameters.Add("IEN_BOD_NBODEGA_P", OracleDbType.Int32).Value = 1;//post.IEN_BOD_NBODEGA.AsInt();
            objCmd.Parameters.Add("IEN_USER_MERX_P", OracleDbType.Varchar2).Value = post.IEN_USER_MERX;
            objCmd.Parameters.Add("IEN_ESTADO_P", OracleDbType.Varchar2).Value = post.IEN_ESTADO;
            objCmd.Parameters.Add("IEN_MOT_MOTIVO_P", OracleDbType.Int32).Value = post.IEN_MOT_MOTIVO.AsInt();
            objCmd.Parameters.Add("IEN_SOLICITA_CAMBIO_P", OracleDbType.Varchar2).Value = post.IEN_SOLICITA_CAMBIO;
            objCmd.Parameters.Add("IEN_RESPONSABLE_ERROR_P", OracleDbType.Varchar2).Value = post.IEN_RESPONSABLE_ERROR;
            objCmd.Parameters.Add("IEN_DEPARTAMENTO_P", OracleDbType.Varchar2).Value = post.IEN_DEPARTAMENTO;

            try
            {
                objCmd.ExecuteNonQuery();
                return Ok(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Validaciones en pantalla

        [Route("api/TX_ENTRADAS_TB_Ingreso/ValidaGuia")]
        [ResponseType(typeof(Prealerta))]
        public IHttpActionResult GetValidaGuia(string guia_p, string manifiesto_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONSEC_GUIA_MAN_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("guia_p", OracleDbType.Varchar2).Value = guia_p;
            objCmd.Parameters.Add("manifiesto_p", OracleDbType.Varchar2).Value = manifiesto_p;
            

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);

            }


            cn.Close();

             Prealerta _Prealerta = new Prealerta();

            return Ok(_Prealerta);
        }

        [Route("api/TX_ENTRADAS_TB_Ingreso/Prealerta")]
        [ResponseType(typeof(Prealerta))]
        public IHttpActionResult GetPrealerta(string guia_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_BUSCA_PREALERTA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("guia_p", OracleDbType.Varchar2).Value = guia_p;
            objCmd.Parameters.Add("existe_p", OracleDbType.Char,100).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("archivo_p", OracleDbType.Char,100).Direction = ParameterDirection.Output;
            objCmd.ExecuteNonQuery();

            cn.Close();

            string paramArch = objCmd.Parameters["archivo_p"].Value.ToString();
            string paramEx = objCmd.Parameters["existe_p"].Value.ToString();
            Prealerta _Prealerta = new Prealerta();
            _Prealerta.Archivo = paramArch.Trim();
            _Prealerta.Existe = paramEx.Trim();


            return Ok(_Prealerta);
        }

        [Route("api/TX_ENTRADAS_TB_Ingreso/AlertasAut")]
        [ResponseType(typeof(int))]
        public IHttpActionResult GetAlertasAut()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_alertas_aut_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("cant_alert_p", OracleDbType.Char,100).Direction = ParameterDirection.Output;
            objCmd.ExecuteNonQuery();

            cn.Close();

            string paramcant = objCmd.Parameters["cant_alert_p"].Value.ToString();
            string _cantidad = paramcant.Trim();

            return Ok(_cantidad);
        }

        [Route("api/TX_ENTRADAS_TB_Ingreso/Prebloqueo")]
        [ResponseType(typeof(string))]
        public IHttpActionResult GetPrebloqueo(string guia_original_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_pre_bloq_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("guia_original_p", OracleDbType.Varchar2).Value = guia_original_p;
            objCmd.Parameters.Add("existe_p", OracleDbType.Char,100).Direction = ParameterDirection.Output;
            objCmd.ExecuteNonQuery();

            cn.Close();

            string paramExiste = objCmd.Parameters["existe_p"].Value.ToString();
            string _Existe = paramExiste.Trim();

            return Ok(_Existe);
        }


        #endregion

        #region Carga de LVs

        /// <summary>
        /// Obtiene Estados
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_ENTRADAS_TB_Ingreso/Estados")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetEstados()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_estados_guia_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Estados> _Estados = new List<Estados>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Estados.Add(new Estados
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Estados);
        }

        /// <summary>
        /// Obtiene Documentos
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_ENTRADAS_TB_Ingreso/DocumentosTICA")]
        [ResponseType(typeof(Documento))]
        public IHttpActionResult GetDocumentos()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_tdo_doc_DIN_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("din_tipo_doc_p", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Documento> _Documentos = new List<Documento>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Documentos.Add(new Documento
                {
                    Id = r["TIPO"].ToString(),
                    Nombre = r["DESCRIPCION"].ToString()
                });
            }

            return Ok(_Documentos);
        }

        /// <summary>
        /// Obtiene Dudas refrigeración
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_ENTRADAS_TB_Ingreso/DudasRefri")]
        [ResponseType(typeof(DudasRefri))]
        public IHttpActionResult GetDudasRefri()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_dudas_refri_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tipo_duda_p", OracleDbType.Long).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<DudasRefri> _Dudas = new List<DudasRefri>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Dudas.Add(new DudasRefri
                {
                    Id = r["codigo"].ToString(),
                    Nombre = r["descripcion"].ToString()
                });
            }

            return Ok(_Dudas);
        }

        /// <summary>
        /// Obtiene Indic refrigeración
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_ENTRADAS_TB_Ingreso/IndicRefri")]
        [ResponseType(typeof(IndicRefri))]
        public IHttpActionResult GetIndicRefri()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_indic_refri_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<IndicRefri> _Indics = new List<IndicRefri>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Indics.Add(new IndicRefri
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Indics);
        }

        /// <summary>
        /// Obtiene clasificacion
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_ENTRADAS_TB_Ingreso/Clasificacion")]
        [ResponseType(typeof(Clasificacion))]
        public IHttpActionResult GetClasificacion()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_clase_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Clasificacion> _Clases = new List<Clasificacion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Clases.Add(new Clasificacion
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Clases);
        }

        /// <summary>
        /// Obtiene localizaciones
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_ENTRADAS_TB_Ingreso/Localizaciones")]
        [ResponseType(typeof(Localizacion))]
        public IHttpActionResult GetLocalizaciones()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_ubicaciones_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("id_ubicacion_p", OracleDbType.Varchar2).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Localizacion> _Localizaciones = new List<Localizacion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Localizaciones.Add(new Localizacion
                {
                    Id = r["codigo"].ToString(),
                    Nombre = r["descripcion"].ToString()
                });
            }

            return Ok(_Localizaciones);
        }


        [Route("api/TX_ENTRADAS_TB_Ingreso/Consolidadores")]
        [ResponseType(typeof(Clientes))]
        public IHttpActionResult GetConsolidadores(string cliente, string tipo_cliente)
        {
            List<Clientes> _clientes = new List<Clientes>();
            if (tipo_cliente != null)
            {
                //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_QRY_CONSOLIDADORES_GUIA_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("cliente_p", OracleDbType.Varchar2).Value = cliente;
                objCmd.Parameters.Add("tipo_cliente_p", OracleDbType.Varchar2).Value = tipo_cliente;
                objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                try
                {
                    objCmd.ExecuteNonQuery();
                    DataTable datos2 = new DataTable();
                    OracleDataAdapter objAdapter = new OracleDataAdapter();
                    objAdapter.SelectCommand = objCmd;
                    objAdapter.Fill(datos2);
                    cn.Close();
                    foreach (DataRow e in datos2.Rows)
                    {
                        _clientes.Add(new Clientes
                        {

                            Id = e["CON_CEDULA"].ToString(),
                            Nombre = e["CON_NOMBRE"].ToString()
                        });
                    }
                    return Ok(_clientes);
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
            else
            {
                return Ok(_clientes);
            }

        }

        #endregion

        /// <summary>
        /// Ingreso de Guías
        /// </summary>
        /// <param name="entrada_p"></param>
        /// <returns></returns>
        [Route("api/TX_ENTRADAS_TB_Ingreso/Guardar")]
        [ResponseType(typeof(Guia))]
        public IHttpActionResult PostTX_ENTRADAS_TB_Ingreso(Guia entrada_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_ingreso_guia_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ien_tma_id_manifiesto_p", OracleDbType.Int32).Value = long.Parse(entrada_p.IEN_TMA_ID_MANIFIESTO);
            objCmd.Parameters.Add("ien_puesto_chequeo_p ", OracleDbType.Int32).Value = entrada_p.IEN_PUESTO_CHEQUEO == null ? 0 : long.Parse(entrada_p.IEN_PUESTO_CHEQUEO); //long.Parse(entrada_p.IEN_PUESTO_CHEQUEO);
            objCmd.Parameters.Add("ien_guia_original_p", OracleDbType.Varchar2).Value = entrada_p.IEN_GUIA_ORIGINAL;
            objCmd.Parameters.Add("ien_guia_p", OracleDbType.Varchar2).Value = entrada_p.IEN_GUIA;
            objCmd.Parameters.Add("ien_guia_master_p", OracleDbType.Varchar2).Value = entrada_p.IEN_GUIA_MASTER;
            objCmd.Parameters.Add("ien_din_tipo_documento_p", OracleDbType.Varchar2).Value = entrada_p.IEN_DIN_TIPO_DOCUMENTO;
            objCmd.Parameters.Add("ien_dua_p", OracleDbType.Varchar2).Value = entrada_p.IEN_DUA;
            objCmd.Parameters.Add("ien_numero_viaje_p", OracleDbType.Int32).Value = entrada_p.IEN_NUMERO_VIAJE == null ? 0 : long.Parse(entrada_p.IEN_NUMERO_VIAJE); //long.Parse(entrada_p.IEN_NUMERO_VIAJE);
            objCmd.Parameters.Add("ien_antecesor_p", OracleDbType.Int32).Value = entrada_p.IEN_ANTECESOR; //rev
            objCmd.Parameters.Add("ien_imp_cedula_p", OracleDbType.Varchar2).Value = entrada_p.IEN_IMP_CEDULA;
            objCmd.Parameters.Add("ien_consignatario_p", OracleDbType.Varchar2).Value = entrada_p.IEN_IMPORTADOR;
            objCmd.Parameters.Add("ien_con_nconsolidador_p", OracleDbType.Int32).Value = entrada_p.IEN_CON_NCONSOLIDADOR;
            objCmd.Parameters.Add("ien_bultos_manifestados_p", OracleDbType.Int32).Value = entrada_p.IEN_BULTOS_MANIFESTADOS;
            objCmd.Parameters.Add("ien_kilos_manifestados_p", OracleDbType.Decimal).Value = entrada_p.IEN_KILOS_MANIFESTADOS.AsDecimal();//float.Parse(entrada_p.IEN_KILOS_MANIFESTADOS);
            objCmd.Parameters.Add("ien_volumen_manifiestado_p", OracleDbType.Decimal).Value = entrada_p.IEN_VOLUMEN_MANIF.AsDecimal();
            objCmd.Parameters.Add("ien_bultos_ingresados_p", OracleDbType.Int32).Value = entrada_p.IEN_BULTOS_INGRESADOS;
            objCmd.Parameters.Add("ien_kilos_ingresados_p", OracleDbType.Decimal).Value = entrada_p.IEN_KILOS_INGRESADOS.AsDecimal(); //float.Parse(entrada_p.IEN_KILOS_INGRESADOS);
            objCmd.Parameters.Add("ien_volumen_ingresado_p", OracleDbType.Decimal).Value = entrada_p.IEN_VOLUMEN_INGRESADO.AsDecimal();
            objCmd.Parameters.Add("ien_descripcion_manif_p", OracleDbType.Varchar2).Value = entrada_p.IEN_DESCRIPCION_MANIF;
            objCmd.Parameters.Add("ien_observaciones_p", OracleDbType.Varchar2).Value = entrada_p.IEN_OBSERVACIONES;
            objCmd.Parameters.Add("ien_ubicacion_ingreso_p", OracleDbType.Varchar2).Value = entrada_p.IEN_UBICACION_INGRESO;
            objCmd.Parameters.Add("ien_ubicacion_transito_p", OracleDbType.Varchar2).Value = entrada_p.IEN_UBICACION_TRANSITO;
            objCmd.Parameters.Add("ien_es_courier_p", OracleDbType.Varchar2).Value = entrada_p.IEN_ES_COURIER != null ? "S" : "N";
            objCmd.Parameters.Add("ien_duda_refri_p", OracleDbType.Varchar2).Value = entrada_p.IEN_DUDA_REFRI;
            objCmd.Parameters.Add("ien_tipo_duda_ref_p", OracleDbType.Int32).Value = entrada_p.IEN_TIPO_DUDA_REF;
            objCmd.Parameters.Add("ien_es_farma_p", OracleDbType.Varchar2).Value = entrada_p.IEN_ES_FARMA != null ? "S" : "N";
            objCmd.Parameters.Add("ien_clase_p", OracleDbType.Varchar2).Value = entrada_p.IEN_CLASE;
            objCmd.Parameters.Add("ien_boleta_clase_p", OracleDbType.Varchar2).Value = entrada_p.IEN_BOLETA_CLASE;
            objCmd.Parameters.Add("ien_acta_descarga_p", OracleDbType.Varchar2).Value = entrada_p.IEN_ACTA_DESCARGA;
            objCmd.Parameters.Add("ien_pend_desconsolidar_p", OracleDbType.Varchar2).Value = entrada_p.IEN_PEND_DESCONSOLIDAR != null ? "S" : "N";
            objCmd.Parameters.Add("ien_solicitud_p", OracleDbType.Varchar2).Value = entrada_p.IEN_SOLICITUD;
            objCmd.Parameters.Add("ien_texto_acta_p", OracleDbType.Varchar2).Value = entrada_p.IEN_TEXTO_ACTA;
            objCmd.Parameters.Add("ien_bod_nbodega_p", OracleDbType.Int32).Value = 1;//entrada_p.IEN_BOD_Nbodega;
            objCmd.Parameters.Add("ien_user_merx_p", OracleDbType.Varchar2).Value = entrada_p.IEN_USU_DIGITO;
            objCmd.Parameters.Add("ien_linea_guia_p", OracleDbType.Int32).Value = entrada_p.IEN_LINEA_GUIA == null ? 0 : entrada_p.IEN_LINEA_GUIA.AsInt();
            objCmd.Parameters.Add("ien_entrada_p", OracleDbType.Int32).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                string entrada = objCmd.Parameters["ien_entrada_p"].Value.ToString();
                cn.Close();
                return Ok(entrada);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/TX_ENTRADAS_TB_Ingreso/Actualizar")]
        [ResponseType(typeof(Guia))]
        public IHttpActionResult PutTX_ENTRADAS_TB_Ingreso(string id, Guia entrada_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();

            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_upd_ingreso_guia_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ien_tma_id_manifiesto_p", OracleDbType.Int32).Value = entrada_p.IEN_TMA_ID_MANIFIESTO;
            objCmd.Parameters.Add("ien_puesto_chequeo_p ", OracleDbType.Int32).Value = entrada_p.IEN_PUESTO_CHEQUEO;
            objCmd.Parameters.Add("ien_entrada_p", OracleDbType.Int32).Value = entrada_p.IEN_ENTRADA;
            objCmd.Parameters.Add("ien_guia_original_p", OracleDbType.Varchar2).Value = entrada_p.IEN_GUIA_ORIGINAL;
            objCmd.Parameters.Add("ien_guia_p", OracleDbType.Varchar2).Value = entrada_p.IEN_GUIA;
            objCmd.Parameters.Add("ien_guia_master_p", OracleDbType.Varchar2).Value = entrada_p.IEN_GUIA_MASTER;
            objCmd.Parameters.Add("ien_din_tipo_documento_p", OracleDbType.Varchar2).Value = entrada_p.IEN_DIN_TIPO_DOCUMENTO;
            objCmd.Parameters.Add("ien_dua_p", OracleDbType.Varchar2).Value = entrada_p.IEN_DUA;
            objCmd.Parameters.Add("ien_numero_viaje_p", OracleDbType.Int32).Value = entrada_p.IEN_NUMERO_VIAJE;
            objCmd.Parameters.Add("ien_antecesor_p", OracleDbType.Int32).Value = entrada_p.IEN_ANTECESOR;
            objCmd.Parameters.Add("ien_imp_cedula_p", OracleDbType.Varchar2).Value = entrada_p.IEN_IMP_CEDULA;
            objCmd.Parameters.Add("ien_consignatario_p", OracleDbType.Varchar2).Value = entrada_p.IEN_IMPORTADOR;
            objCmd.Parameters.Add("ien_con_nconsolidador_p", OracleDbType.Int32).Value = entrada_p.IEN_CON_NCONSOLIDADOR;
            objCmd.Parameters.Add("ien_bultos_manifestados_p", OracleDbType.Int32).Value = entrada_p.IEN_BULTOS_MANIFESTADOS;// != null ? entrada_p.IEN_BULTOS_MANIFESTADOS.Replace('.',' ').AsInt() : 0 ;
            objCmd.Parameters.Add("ien_kilos_manifestados_p", OracleDbType.Decimal).Value = entrada_p.IEN_KILOS_MANIFESTADOS.AsDecimal();
            objCmd.Parameters.Add("ien_volumen_manifiestado_p", OracleDbType.Decimal).Value = entrada_p.IEN_VOLUMEN_MANIF.AsDecimal();
            objCmd.Parameters.Add("ien_bultos_ingresados_p", OracleDbType.Int32).Value = entrada_p.IEN_BULTOS_INGRESADOS;
            objCmd.Parameters.Add("ien_kilos_ingresados_p", OracleDbType.Decimal).Value = entrada_p.IEN_KILOS_INGRESADOS.AsDecimal();
            objCmd.Parameters.Add("ien_volumen_ingresado_p", OracleDbType.Decimal).Value = entrada_p.IEN_VOLUMEN_INGRESADO.AsDecimal();
            objCmd.Parameters.Add("ien_descripcion_manif_p", OracleDbType.Varchar2).Value = entrada_p.IEN_DESCRIPCION_MANIF;
            objCmd.Parameters.Add("ien_observaciones_p", OracleDbType.Varchar2).Value = entrada_p.IEN_OBSERVACIONES;
            objCmd.Parameters.Add("ien_ubicacion_ingreso_p", OracleDbType.Varchar2).Value = entrada_p.IEN_UBICACION_INGRESO;
            objCmd.Parameters.Add("ien_ubicacion_transito_p", OracleDbType.Varchar2).Value = entrada_p.IEN_UBICACION_TRANSITO;
            objCmd.Parameters.Add("ien_es_courier_p", OracleDbType.Varchar2).Value = entrada_p.IEN_ES_COURIER != null ? "S" : "N";
            objCmd.Parameters.Add("ien_duda_refri_p", OracleDbType.Varchar2).Value = entrada_p.IEN_DUDA_REFRI;
            objCmd.Parameters.Add("ien_tipo_duda_ref_p", OracleDbType.Int32).Value = entrada_p.IEN_TIPO_DUDA_REF;
            objCmd.Parameters.Add("ien_es_farma_p", OracleDbType.Varchar2).Value = entrada_p.IEN_ES_FARMA != null ? "S" : "N";
            objCmd.Parameters.Add("ien_clase_p", OracleDbType.Varchar2).Value = entrada_p.IEN_CLASE;
            objCmd.Parameters.Add("ien_boleta_clase_p", OracleDbType.Varchar2).Value = entrada_p.IEN_BOLETA_CLASE;
            objCmd.Parameters.Add("ien_acta_descarga_p", OracleDbType.Varchar2).Value = entrada_p.IEN_ACTA_DESCARGA;
            objCmd.Parameters.Add("ien_pend_desconsolidar_p", OracleDbType.Varchar2).Value = entrada_p.IEN_PEND_DESCONSOLIDAR != null ? "S" : "N";
            objCmd.Parameters.Add("ien_solicitud_p", OracleDbType.Varchar2).Value = entrada_p.IEN_SOLICITUD;
            objCmd.Parameters.Add("ien_texto_acta_p", OracleDbType.Varchar2).Value = entrada_p.IEN_TEXTO_ACTA;
            objCmd.Parameters.Add("ien_bod_nbodega_p", OracleDbType.Int32).Value = 1;//entrada_p.IEN_BOD_Nbodega;
            objCmd.Parameters.Add("ien_user_merx_p", OracleDbType.Varchar2).Value = entrada_p.IEN_USU_DIGITO;
            objCmd.Parameters.Add("ien_linea_guia_p", OracleDbType.Int32).Value = entrada_p.IEN_LINEA_GUIA == null ? 0 : entrada_p.IEN_LINEA_GUIA.AsInt();
            objCmd.Parameters.Add("ien_estado_p", OracleDbType.Varchar2).Value = entrada_p.IEN_ESTADO;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/TX_ENTRADAS_TB_Ingreso/Borrar")]
        [ResponseType(typeof(Guia))]
        public IHttpActionResult Delete(long id, string user)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_DEL_INGRESO_GUIA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("entrada_p ", OracleDbType.Int32).Value = id;
            objCmd.Parameters.Add("ien_user_merx_p ", OracleDbType.Varchar2).Value = user;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();

            return Ok();
        }

        [Route("api/TX_ENTRADAS_TB_Ingreso/AplicarDatosMultidep")]
        public IHttpActionResult GetAplicarDatosMultidep(int consecutivo)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_TRANSM_BL_GENERAL_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("entrada_p", OracleDbType.Int32).Value = consecutivo;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok(consecutivo);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
    }
}