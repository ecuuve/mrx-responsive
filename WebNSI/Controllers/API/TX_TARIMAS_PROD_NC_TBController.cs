﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_TARIMAS_PROD_NC_TBController : ApiController
    {
        [Route("api/TX_TARIMAS_PROD_NC_TB/Tarimas")]
        [ResponseType(typeof(tarimas))]
        public IHttpActionResult GetTarimas(long? identificador, long? secuencia)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_PNC_TARIMAS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tnc_pnc_identificador_p", OracleDbType.Int32).Value = identificador;
            objCmd.Parameters.Add("tnc_secuencia_p", OracleDbType.Int32).Value = secuencia;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<tarimas> _tarimas = new List<tarimas>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _tarimas.Add(new tarimas
                    {

                        TNC_TARIMA = e["TNC_TARIMA"].ToString(),
                        TNC_BULTOS = e["TNC_BULTOS"].ToString(),
                        TNC_BULTOS_NC = e["TNC_BULTOS_NC"].ToString(),
                        TNC_SECUENCIA = e["TNC_SECUENCIA"].ToString(),
                        TNC_PNC_IDENTIFICADOR = e["TNC_PNC_IDENTIFICADOR"].ToString()
                    });
                }
                return Ok(_tarimas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_TARIMAS_PROD_NC_TB/GuardarDetallePNC")]
        public IHttpActionResult PostGuardarDetallePNC(tarimas tarimas)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_PNC_TARIMAS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("TNC_TARIMA_P", OracleDbType.Int32).Value = tarimas.TNC_TARIMA;
            objCmd.Parameters.Add("TNC_BULTOS_P", OracleDbType.Int32).Value = tarimas.TNC_BULTOS;
            objCmd.Parameters.Add("TNC_BULTOS_NC_P", OracleDbType.Int32).Value = tarimas.TNC_BULTOS_NC;
            objCmd.Parameters.Add("TNC_PNC_IDENTIFICADOR_P", OracleDbType.Int32).Value = tarimas.TNC_PNC_IDENTIFICADOR;

            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [ResponseType(typeof(tarimas))]
        public IHttpActionResult PutTX_TARIMAS_PROD_NC_TB(tarimas tarima)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_PNC_TARIMAS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("TNC_PNC_IDENTIFICADOR_P", OracleDbType.Int32).Value = tarima.TNC_PNC_IDENTIFICADOR;
            objCmd.Parameters.Add("TNC_TARIMA_P", OracleDbType.Varchar2).Value = tarima.TNC_TARIMA;
            objCmd.Parameters.Add("TNC_BULTOS_P", OracleDbType.Varchar2).Value = tarima.TNC_BULTOS;
            objCmd.Parameters.Add("TNC_BULTOS_NC_P", OracleDbType.Varchar2).Value = tarima.TNC_BULTOS_NC;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                tarimas _tarimas = new tarimas();
                return Ok(_tarimas);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
