﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_MERCANCIAS_TBCorrecController : ApiController
    {
        public class mercancias
        {
            public string idM { set; get; }
            public string estadoM { set; get; }
        }

        [Route("api/TX_MERCANCIAS_TBCorrec/MercanciaDetalle")]
        [ResponseType(typeof(MercanciaCORREC))]
        public IHttpActionResult GetMercancia(long id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_correc_mer2_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Long).Value = id;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            MercanciaCORREC _Mercancia = new MercanciaCORREC();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Mercancia.MER_ID_Mercancia = e["MER_ID_MERCANCIA"].ToString();//long.Parse(e["MER_ID_MERCANCIA"].ToString());
                _Mercancia.MER_IEN_Entrada = e["MER_IEN_ENTRADA"].ToString();//long.Parse(e["MER_IEN_ENTRADA"].ToString());
                _Mercancia.MER_Manifiesto = e["MER_MANIFIESTO"].ToString();
                _Mercancia.MER_Guia = e["MER_GUIA"].ToString();
                _Mercancia.MER_LINEA_Guia = e["MER_LINEA_GUIA"].ToString();//long.Parse(e["MER_LINEA_GUIA"].ToString());
                _Mercancia.MER_Estado = e["DES_ESTADO"].ToString();
                _Mercancia.MER_Transmision = e["MER_TRANSMISION"].ToString();
                _Mercancia.MER_ARCHIVO_Transmision = e["MER_ARCHIVO_TRANSMISION"].ToString();
                _Mercancia.MER_FCH_Transmision = e["MER_FCH_TRANSMISION"].ToString();
                _Mercancia.MER_USUARIO_Transmision = e["MER_USUARIO_TRANSMISION"].ToString();
                _Mercancia.MER_Accion = e["DES_ACCION"].ToString();
                _Mercancia.MER_OPERACION_Tica = e["MER_OPERACION_TICA"].ToString();
                _Mercancia.DESC_OPER_Tica = e["DES_OPERACION_TICA"].ToString();
                _Mercancia.MER_ID_MERCANCIA_Ref = e["MER_ID_MERCANCIA_REF"].ToString();//!string.IsNullOrEmpty(e["MER_ID_MERCANCIA_REF"].ToString()) ? long.Parse(e["MER_ID_MERCANCIA_REF"].ToString()) : 0;
                _Mercancia.MER_BULTOS_Ref = e["MER_BULTOS_REF"].ToString();//!string.IsNullOrEmpty(e["MER_BULTOS_REF"].ToString()) ? decimal.Parse(e["MER_BULTOS_REF"].ToString()) : 0;
                _Mercancia.MER_Error = e["MER_ERROR"].ToString();
                _Mercancia.MER_EN_Tica = e["MER_EN_TICA"].ToString();
                _Mercancia.MER_TER_Error = e["MER_TER_ERROR"].ToString();
                _Mercancia.MER_MOV_Inventario = e["MER_MOV_INVENTARIO"].ToString();//!string.IsNullOrEmpty(e["MER_MOV_INVENTARIO"].ToString()) ? long.Parse(e["MER_MOV_INVENTARIO"].ToString()) : 0;


            }

            return Ok(_Mercancia);
        }


        [Route("api/TX_MERCANCIAS_TBCorrec/GetMercanciasCorrec")]
        [ResponseType(typeof(MercanciaCORREC))]
        public IHttpActionResult GetMercanciasCorrec(long? entrada_p, string manifiesto_p, string guia_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_correc_mer_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_ien_entrada_p", OracleDbType.Int32).Value = entrada_p;
            objCmd.Parameters.Add("mer_manifiesto_p", OracleDbType.Varchar2).Value = manifiesto_p;
            objCmd.Parameters.Add("mer_guia_p", OracleDbType.Varchar2).Value = guia_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<MercanciaCORREC> _Mercancias = new List<MercanciaCORREC>();
            //Mercancia _Mercancia = new Mercancia();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Mercancias.Add(new MercanciaCORREC
                {
                    MER_ID_Mercancia = e["MER_ID_MERCANCIA"].ToString(),//long.Parse(e["MER_ID_MERCANCIA"].ToString()),
                    MER_IEN_Entrada = e["MER_IEN_ENTRADA"].ToString(), //long.Parse(e["MER_IEN_ENTRADA"].ToString()),
                    MER_Manifiesto = e["MER_MANIFIESTO"].ToString(),
                    MER_Guia = e["MER_GUIA"].ToString(),
                    MER_LINEA_Guia = e["MER_LINEA_GUIA"].ToString(),//long.Parse(e["MER_LINEA_GUIA"].ToString()),
                    MER_Estado = e["MER_ESTADO"].ToString(),
                    MER_Transmision = e["MER_TRANSMISION"].ToString(),
                    MER_ARCHIVO_Transmision = e["MER_ARCHIVO_TRANSMISION"].ToString(),
                    MER_FCH_Transmision = e["MER_FCH_TRANSMISION"].ToString(),
                    MER_USUARIO_Transmision = e["MER_USUARIO_TRANSMISION"].ToString(),
                    MER_Accion = e["MER_ACCION"].ToString(),
                    MER_OPERACION_Tica = e["MER_OPERACION_TICA"].ToString(),
                    DESC_OPER_Tica = e["DES_OPERACION_TICA"].ToString(),
                    MER_ID_MERCANCIA_Ref = e["MER_ID_MERCANCIA_REF"].ToString(),//!string.IsNullOrEmpty(e["MER_ID_MERCANCIA_REF"].ToString()) ? long.Parse(e["MER_ID_MERCANCIA_REF"].ToString()) : 0,
                    MER_BULTOS_Ref = e["MER_BULTOS_REF"].ToString(), //!string.IsNullOrEmpty(e["MER_BULTOS_REF"].ToString()) ? decimal.Parse(e["MER_BULTOS_REF"].ToString()) : 0,
                    MER_Error = e["MER_ERROR"].ToString(),
                    MER_EN_Tica = e["MER_EN_TICA"].ToString(),
                    MER_TER_Error = e["MER_TER_ERROR"].ToString(),
                    MER_MOV_Inventario = e["MER_MOV_INVENTARIO"].ToString(),//!string.IsNullOrEmpty(e["MER_MOV_INVENTARIO"].ToString()) ? long.Parse(e["MER_MOV_INVENTARIO"].ToString()) : 0,
                    consecutivo = ""
                });

            }

            return Ok(_Mercancias);
        }


        [Route("api/TX_MERCANCIAS_TBCorrec/ActualizaCorrec")]
        [ResponseType(typeof(Mercancia))]
        public IHttpActionResult PostTX_MERCANCIAS_TB(List<mercancias> mercancias)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
           

            int cont = 0;
            string merc = "";
            foreach (var x in mercancias)
            {
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                //Extrae el estado anterior de la mercancia especificada
                objCmd.CommandText = "mrx.tx_qry_estado_merc_pr";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("tarima_p", OracleDbType.Int32).Value = x.idM;
                objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                objCmd.ExecuteNonQuery();
                DataTable datos = new DataTable();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos);
                string estadoAnterior = "";
                if (datos.Rows.Count >= 1)
                {
                    estadoAnterior = datos.Rows[0][1].ToString();
                }

                //Compara en cada mercancia el viejo y nuevo estado para ver si se puede o no actualizar
                OracleCommand objCmd1 = new OracleCommand();
                objCmd1.Connection = cn;
                objCmd1.CommandText = "mrx.TX_VALIDAR_ESTADO_MER_PR";
                objCmd1.CommandType = CommandType.StoredProcedure;
                objCmd1.Parameters.Add("old_p", OracleDbType.Varchar2).Value = estadoAnterior.ToString();
                objCmd1.Parameters.Add("new_p", OracleDbType.Varchar2).Value = x.estadoM.ToString();
                objCmd1.Parameters.Add("mensaje_p", OracleDbType.Char,50).Direction = ParameterDirection.Output;

                objCmd1.ExecuteNonQuery();
                string mensaje = objCmd1.Parameters["mensaje_p"].Value.ToString();

                if(mensaje.Contains("Cambio de estado no permitido de E a T"))
                {
                    cont++;
                    merc += (" " + x.idM);
                }
            }
             //Si alguna mercancia no puede actualizarse devuelve mensaje de error
            if(cont > 0)
            {
                return BadRequest("Existen restricciones de cambio en las siguientes mercancias: " + merc);
            }
            else
            {
                //Actualiza la informacion de cada una de las mercancias
                foreach (var x in mercancias)
                {
                    OracleCommand objCmd2 = new OracleCommand();
                    objCmd2.Connection = cn;

                    objCmd2.CommandText = "mrx.tx_upd_correc_mer_pr";
                    objCmd2.CommandType = CommandType.StoredProcedure;
                    objCmd2.Parameters.Add("mer_id_mercancia_p", OracleDbType.Int32).Value = x.idM;
                    objCmd2.Parameters.Add("mer_estado_p", OracleDbType.Varchar2).Value = x.estadoM;
                    objCmd2.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = "MRX";

                    try
                    {
                        objCmd2.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(ex.Message);
                    } 
                }
            }
            cn.Close();
            return Ok();
        }

    }
}