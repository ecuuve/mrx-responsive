﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_AGENCIA_ADUANAL_TBController : ApiController
    {

        [Route("api/TX_AGENCIA_ADUANAL_TB/GetAgenciaAduanal")]
        [ResponseType(typeof(AgenciaAduanal))]
        public IHttpActionResult GetAgenciaAduanal()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_age_aduanal_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("aga_nagencia_aduanal_p", OracleDbType.Varchar2).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<AgenciaAduanal> _AgencAdu = new List<AgenciaAduanal>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _AgencAdu.Add(new AgenciaAduanal
                {
                    AGA_AGENCIA_ADUANAL = e["AGA_AGENCIA_ADUANAL"].ToString(),
                    AGA_NOMBRE = e["AGA_NOMBRE"].ToString(),
                    AGA_EST_ESTADO = e["ESTADO"].ToString(),
                    AGA_CEDULA = e["AGA_CEDULA"].ToString()
                });
            }
            return Ok(_AgencAdu);
        }

        [Route("api/TX_AGENCIA_ADUANAL_TB/GetAgenciaAduanalDetalle")]
        [ResponseType(typeof(AgenciaAduanal))]
        public IHttpActionResult GetAgenciaAduanalDetalle(long aga_nagencia_aduanal_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_DET_AGE_ADUANAL_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("adc_dct_declarante_id_p", OracleDbType.Varchar2).Value = aga_nagencia_aduanal_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<AgenciaAduanal> _AgenciaAduanal = new List<AgenciaAduanal>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _AgenciaAduanal.Add(new AgenciaAduanal
                {
                    AGA_NAGENCIA_ADUANAL = e["AGA_NAGENCIA_ADUANAL"].ToString(),
                    AGA_AGENCIA_ADUANAL = e["AGA_AGENCIA_ADUANAL"].ToString(),
                    AGA_NOMBRE = e["AGA_NOMBRE"].ToString(),
                    AGA_EST_ESTADO = e["AGA_EST_ESTADO"].ToString(),
                    AGA_CEDULA = e["AGA_CEDULA"].ToString(),
                    AGA_USUARIO = e["AGA_USUARIO"].ToString(),
                    AGA_RECIBE_ESTADOS = e["AGA_RECIBE_ESTADOS"].ToString(),
                    AGA_TELEFONO1 = e["AGA_TELEFONO1"].ToString(),
                    AGA_TELEFONO2 = e["AGA_TELEFONO2"].ToString(),
                    AGA_TELEFONO_FAX = e["AGA_TELEFONO_FAX"].ToString(),
                    AGA_DIRECCION = e["AGA_DIRECCION"].ToString(),
                    AGA_APARTADO = e["AGA_APARTADO"].ToString(),
                    AGA_EMAIL = e["AGA_EMAIL"].ToString(),
                    AGA_FECHA_INGRESO = DateTime.Parse(e["AGA_FECHA_INGRESO"].ToString()).ToString("dd/MM/yyyy")
                });
            }
            return Ok(_AgenciaAduanal);
        }

        [Route("api/TX_AGENCIA_ADUANAL_TB/GetRecibeEstados")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetRecibeEstados()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_aga_recibe_est_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Estados> _Estados = new List<Estados>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Estados.Add(new Estados
                {
                    Id = r["id"].ToString(),
                    Nombre = r["nombre"].ToString()
                });
            }

            return Ok(_Estados);
        }

        [Route("api/TX_AGENCIA_ADUANAL_TB/GetEstados")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetEstados()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_aga_estado_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Estados> _Estados = new List<Estados>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Estados.Add(new Estados
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Estados);
        }

        [ResponseType(typeof(AgenciaAduanal))]
        public IHttpActionResult PutTX_AGENCIA_ADUANAL_TB(string id, AgenciaAduanal agencia_p)
        {

            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;

            objCmd.CommandText = "mrx.tx_upd_age_aduanal_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("aga_nagencia_aduanal_p", OracleDbType.Long).Value = agencia_p.AGA_NAGENCIA_ADUANAL;
            objCmd.Parameters.Add("aga_est_estado_p", OracleDbType.Varchar2).Value = agencia_p.AGA_EST_ESTADO;
            objCmd.Parameters.Add("aga_nombre_p", OracleDbType.Varchar2).Value = agencia_p.AGA_NOMBRE;
            objCmd.Parameters.Add("aga_cedula_p", OracleDbType.Varchar2).Value = agencia_p.AGA_CEDULA;
            objCmd.Parameters.Add("aga_usuario_p", OracleDbType.Varchar2).Value = agencia_p.USER_MERX;
            objCmd.Parameters.Add("aga_recibe_estados_p", OracleDbType.Varchar2).Value = agencia_p.AGA_RECIBE_ESTADOS;
            objCmd.Parameters.Add("aga_telefono1_p", OracleDbType.Varchar2).Value = agencia_p.AGA_TELEFONO1;
            objCmd.Parameters.Add("aga_telefono2_p", OracleDbType.Varchar2).Value = agencia_p.AGA_TELEFONO2;
            objCmd.Parameters.Add("aga_telefono_fax_p", OracleDbType.Varchar2).Value = agencia_p.AGA_TELEFONO_FAX;
            objCmd.Parameters.Add("aga_direccion_p", OracleDbType.Varchar2).Value = agencia_p.AGA_DIRECCION;
            objCmd.Parameters.Add("aga_apartado_p", OracleDbType.Varchar2).Value = agencia_p.AGA_APARTADO;
            objCmd.Parameters.Add("aga_email_p", OracleDbType.Varchar2).Value = agencia_p.AGA_EMAIL;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();

            AgenciaAduanal _AgenciaAduanalUP = new AgenciaAduanal();


            return Ok(_AgenciaAduanalUP);

        }

        [ResponseType(typeof(AgenciaAduanal))]
        public IHttpActionResult PostTX_AGENCIA_ADUANAL_TB(AgenciaAduanal agencia_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_age_aduanal_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("aga_est_estado_p", OracleDbType.Varchar2).Value = agencia_p.AGA_EST_ESTADO;
            objCmd.Parameters.Add("aga_nombre_p", OracleDbType.Varchar2).Value = agencia_p.AGA_NOMBRE;
            objCmd.Parameters.Add("aga_cedula_p", OracleDbType.Varchar2).Value = agencia_p.AGA_CEDULA;
            objCmd.Parameters.Add("aga_usuario_p", OracleDbType.Varchar2).Value = agencia_p.USER_MERX;
            objCmd.Parameters.Add("aga_recibe_estados_p", OracleDbType.Varchar2).Value = agencia_p.AGA_RECIBE_ESTADOS;
            objCmd.Parameters.Add("aga_telefono1_p", OracleDbType.Varchar2).Value = agencia_p.AGA_TELEFONO1;
            objCmd.Parameters.Add("aga_telefono2_p", OracleDbType.Varchar2).Value = agencia_p.AGA_TELEFONO2;
            objCmd.Parameters.Add("aga_telefono_fax_p", OracleDbType.Varchar2).Value = agencia_p.AGA_TELEFONO_FAX;
            objCmd.Parameters.Add("aga_direccion_p", OracleDbType.Varchar2).Value = agencia_p.AGA_DIRECCION;
            objCmd.Parameters.Add("aga_apartado_p", OracleDbType.Varchar2).Value = agencia_p.AGA_APARTADO;
            objCmd.Parameters.Add("aga_email_p", OracleDbType.Varchar2).Value = agencia_p.AGA_EMAIL;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            AgenciaAduanal _AgenciaAduanalIn = new AgenciaAduanal();

            return Ok(_AgenciaAduanalIn);
        }

    }
}
