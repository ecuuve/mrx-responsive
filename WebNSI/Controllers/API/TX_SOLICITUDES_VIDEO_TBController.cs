﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_SOLICITUDES_VIDEO_TBController : ApiController
    {
        #region GETS

        /// <summary>
        /// Obtiene solicitudes según filtros
        /// </summary>
        /// <param name="solicitud"></param>
        /// <param name="estado"></param>
        /// <param name="entrada"></param>
        /// <param name="sistema"></param>
        /// <returns></returns>
        [Route("api/TX_SOLICITUDES_VIDEO_TB/Solicitudes")]
        [ResponseType(typeof(SolicitudVideo))]
        public IHttpActionResult GetSolicitudes(long? solicitud, string estado, long? entrada, string sistema)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_cons_solic_video_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("siv_solicitud_p", OracleDbType.Int32).Value = solicitud;
            objCmd.Parameters.Add("siv_estado_p", OracleDbType.Varchar2).Value = estado == "0" || estado== "null" ? null : estado;
            objCmd.Parameters.Add("siv_ien_entrada_p", OracleDbType.Int32).Value = entrada;
            objCmd.Parameters.Add("siv_sistema_p", OracleDbType.Varchar2).Value = sistema == "0" || sistema == "null"  ? null : sistema;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<SolicitudVideo> _solicitudes = new List<SolicitudVideo>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _solicitudes.Add(new SolicitudVideo
                    {

                        SIV_TIPO = e["SIV_TIPO"].ToString(),
                        DESC_TIPO = e["DESC_TIPO"].ToString(),
                        SIV_SISTEMA = e["SIV_SISTEMA"].ToString(),
                        SIV_IEN_ENTRADA = e["SIV_IEN_ENTRADA"].ToString(),
                        SIV_PRIORIDAD = e["SIV_PRIORIDAD"].ToString(),
                        SIV_VERIF_RECEPCION = e["SIV_VERIF_RECEPCION"].ToString(),
                        SIV_VERIF_DESPALET = e["SIV_VERIF_DESPALET"].ToString(),
                        SIV_VERIF_CHEQUEO = e["SIV_VERIF_CHEQUEO"].ToString(),
                        SIV_VERIF_AFORO = e["SIV_VERIF_AFORO"].ToString(),
                        SIV_VERIF_REFRIG = e["SIV_VERIF_REFRIG"].ToString(),
                        SIV_VERIF_COURIER = e["SIV_VERIF_COURIER"].ToString(),
                        SIV_VERIF_RX = e["SIV_VERIF_RX"].ToString(),
                        SIV_VERIF_DESPACH = e["SIV_VERIF_DESPACH"].ToString(),
                        SIV_VERIF_OTRO = e["SIV_VERIF_OTRO"].ToString(),
                        SIV_VERIF_DESCRP_OTRO = e["SIV_VERIF_DESCRP_OTRO"].ToString(),
                        SIV_OBSERVACIONES = e["SIV_OBSERVACIONES"].ToString(),
                        SIV_SOLICITA = e["SIV_SOLICITA"].ToString(),
                        SIV_NOMBRE_SOLICITA = e["SIV_NOMBRE_SOLICITA"].ToString(),
                        SIV_USU_REGISTRA = e["SIV_USU_REGISTRA"].ToString(),
                        SIV_FCH_REGISTRA = e["SIV_FCH_REGISTRA"].ToString(),
                        SIV_FCH_FINALIZA = e["SIV_FCH_FINALIZA"].ToString(),
                        CONSIGNATARIO = e["CONSIGNATARIO"].ToString(),
                        SIV_SOLICITUD = e["SIV_SOLICITUD"].ToString(),
                        GUIA_MASTER = e["GUIA_MASTER"].ToString(),
                        GUIA_HIJA = e["GUIA_HIJA"].ToString(),
                        AER_NOMBRE = e["AER_NOMBRE"].ToString()

                    });
                }
                return Ok(_solicitudes);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

       
        [Route("api/TX_SOLICITUDES_VIDEO_TB/InfoConse")]
        [ResponseType(typeof(InfoConse))]
        public IHttpActionResult GetInfoConsecutivo(long? consecutivo, string sistema)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONS_DET_SOLIC_VIDEO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("siv_ien_entrada_p", OracleDbType.Int32).Value = consecutivo;
            objCmd.Parameters.Add("siv_sistema_p", OracleDbType.Varchar2).Value = sistema;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<InfoConse> _infoC = new List<InfoConse>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {
                    _infoC.Add(new InfoConse
                    {
                        //IEN_AER_NAREOLINEA = r["IEN_AER_NAREOLINEA"].ToString(),
                        IEN_GUIA_MASTER = r["IEN_GUIA_MASTER"].ToString(),
                        IEN_GUIA_ORIGINAL = r["IEN_GUIA_ORIGINAL"].ToString(),
                        IEN_IMPORTADOR = r["IEN_IMPORTADOR"].ToString(),
                        AER_NOMBRE = r["AER_NOMBRE"].ToString()
                    });
                }
                return Ok(_infoC);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        /// <summary>
        /// Lista los tipos de solicitud
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_SOLICITUDES_VIDEO_TB/TipoSolicitud")]
        [ResponseType(typeof(TipoSolicitud))]
        public IHttpActionResult GetTipoSolicitud()
    {
        //Get the connection string from the app.config            
        var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
        OracleConnection cn = new OracleConnection(connectionString);
        cn.Open();
        OracleCommand objCmd = new OracleCommand();
        objCmd.Connection = cn;
        objCmd.CommandText = "mrx.tx_lv_siv_tipo_pr";
        objCmd.CommandType = CommandType.StoredProcedure;
        objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
        try
        {
            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<TipoSolicitud> _tipos = new List<TipoSolicitud>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();
            foreach (DataRow r in datos2.Rows)
            {
                _tipos.Add(new TipoSolicitud
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }
            return Ok(_tipos);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }

    }

        /// <summary>
        /// Lista los sistemas
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_SOLICITUDES_VIDEO_TB/Sistemas")]
        [ResponseType(typeof(siv_sistema))]
        public IHttpActionResult GetSistemas()
    {
        //Get the connection string from the app.config            
        var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
        OracleConnection cn = new OracleConnection(connectionString);
        cn.Open();
        OracleCommand objCmd = new OracleCommand();
        objCmd.Connection = cn;
        objCmd.CommandText = "mrx.tx_lv_siv_sistema_pr";
        objCmd.CommandType = CommandType.StoredProcedure;
        objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
        try
        {
            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<siv_sistema> _tipos = new List<siv_sistema>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();
            foreach (DataRow r in datos2.Rows)
            {
                _tipos.Add(new siv_sistema
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }
            return Ok(_tipos);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }

    }
            
        /// <summary>
        /// Lista los solicitantes
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_SOLICITUDES_VIDEO_TB/Solicitantes")]
        [ResponseType(typeof(siv_solicita))]
        public IHttpActionResult GetSolucitantes()
    {
        //Get the connection string from the app.config            
        var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
        OracleConnection cn = new OracleConnection(connectionString);
        cn.Open();
        OracleCommand objCmd = new OracleCommand();
        objCmd.Connection = cn;
        objCmd.CommandText = "mrx.tx_lv_siv_solicita_pr";
        objCmd.CommandType = CommandType.StoredProcedure;
        objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
        try
        {
            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<siv_solicita> _solicitante = new List<siv_solicita>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();
            foreach (DataRow r in datos2.Rows)
            {
                _solicitante.Add(new siv_solicita
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }
            return Ok(_solicitante);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }

    }
            
        /// <summary>
        /// Lista los estados
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_SOLICITUDES_VIDEO_TB/Estados")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetEstados()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_solvideo_estado_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Estados> _solicitante = new List<Estados>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {
                    _solicitante.Add(new Estados
                    {
                        Id = r["Id"].ToString(),
                        Nombre = r["Nombre"].ToString()
                    });
                }
                return Ok(_solicitante);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        #endregion

        /// <summary>
        /// Guarda nueva solicitud de video
        /// </summary>
        /// <param name="solicitud"></param>
        /// <returns></returns>
        [Route("api/TX_SOLICITUDES_VIDEO_TB/GuardarSolicitudVideo")]
        public IHttpActionResult PostGuardarSolicitudVideo(SolicitudVideo solicitud)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_solic_video_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("SIV_TIPO_P", OracleDbType.Varchar2).Value = solicitud.SIV_TIPO == "0" ? null : solicitud.SIV_TIPO;
            objCmd.Parameters.Add("SIV_SISTEMA_P", OracleDbType.Varchar2).Value = solicitud.SIV_SISTEMA == "0" ? null : solicitud.SIV_SISTEMA;
            objCmd.Parameters.Add("SIV_IEN_ENTRADA_P", OracleDbType.Int32).Value = solicitud.SIV_IEN_ENTRADA == "" ? null : solicitud.SIV_IEN_ENTRADA;
            objCmd.Parameters.Add("SIV_PRIORIDAD_P", OracleDbType.Int32).Value = solicitud.SIV_PRIORIDAD == "" ? null : solicitud.SIV_PRIORIDAD;
            objCmd.Parameters.Add("SIV_VERIF_RECEPCION_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_RECEPCION != null ? "S" : "N"; 
            objCmd.Parameters.Add("SIV_VERIF_DESPALET_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_DESPALET != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_CHEQUEO_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_CHEQUEO != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_AFORO_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_AFORO != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_REFRIG_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_REFRIG != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_COURIER_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_COURIER != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_RX_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_RX != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_DESPACH_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_DESPACH != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_OTRO_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_OTRO != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_DESCRP_OTRO_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_DESCRP_OTRO;
            objCmd.Parameters.Add("SIV_OBSERVACIONES_P", OracleDbType.Varchar2).Value = solicitud.SIV_OBSERVACIONES;
            objCmd.Parameters.Add("SIV_SOLICITA_P", OracleDbType.Varchar2).Value = solicitud.SIV_SOLICITA == "0" ? null : solicitud.SIV_SOLICITA;
            objCmd.Parameters.Add("SIV_NOMBRE_SOLICITA_P", OracleDbType.Varchar2).Value = solicitud.SIV_NOMBRE_SOLICITA;
            objCmd.Parameters.Add("SIV_USU_REGISTRA_P", OracleDbType.Varchar2).Value = solicitud.SIV_USU_REGISTRA;

            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        
        /// <summary>
        /// Actualiza solicitud de video
        /// </summary>
        /// <param name="id"></param>
        /// <param name="solicitud"></param>
        /// <returns></returns>
        [Route("api/TX_SOLICITUDES_VIDEO_TB/ActualizaSolicitud")]
        [ResponseType(typeof(SolicitudVideo))]
        public IHttpActionResult PutTX_SOLICITUDES_VIDEO_TB(long? id, SolicitudVideo solicitud)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_upd_solic_video_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("SIV_SOLICITUD_P", OracleDbType.Int32).Value = id;
            objCmd.Parameters.Add("SIV_TIPO_P", OracleDbType.Varchar2).Value = solicitud.SIV_TIPO == "0" ? null : solicitud.SIV_TIPO;
            objCmd.Parameters.Add("SIV_SISTEMA_P", OracleDbType.Varchar2).Value = solicitud.SIV_SISTEMA == "0" ? null : solicitud.SIV_SISTEMA;
            objCmd.Parameters.Add("SIV_IEN_ENTRADA_P", OracleDbType.Int32).Value = solicitud.SIV_IEN_ENTRADA == "" ? null : solicitud.SIV_IEN_ENTRADA;
            objCmd.Parameters.Add("SIV_PRIORIDAD_P", OracleDbType.Int32).Value = solicitud.SIV_PRIORIDAD == "" ? null : solicitud.SIV_PRIORIDAD;
            objCmd.Parameters.Add("SIV_VERIF_RECEPCION_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_RECEPCION != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_DESPALET_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_DESPALET != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_CHEQUEO_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_CHEQUEO != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_AFORO_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_AFORO != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_REFRIG_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_REFRIG != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_COURIER_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_COURIER != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_RX_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_RX != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_DESPACH_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_DESPACH != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_OTRO_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_OTRO != null ? "S" : "N";
            objCmd.Parameters.Add("SIV_VERIF_DESCRP_OTRO_P", OracleDbType.Varchar2).Value = solicitud.SIV_VERIF_DESCRP_OTRO;
            objCmd.Parameters.Add("SIV_OBSERVACIONES_P", OracleDbType.Varchar2).Value = solicitud.SIV_OBSERVACIONES;
            objCmd.Parameters.Add("SIV_SOLICITA_P", OracleDbType.Varchar2).Value = solicitud.SIV_SOLICITA == "0" ? null : solicitud.SIV_SOLICITA;
            objCmd.Parameters.Add("SIV_NOMBRE_SOLICITA_P", OracleDbType.Varchar2).Value = solicitud.SIV_NOMBRE_SOLICITA;

            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                SolicitudVideo _solicitud = new SolicitudVideo();
                return Ok(_solicitud);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
