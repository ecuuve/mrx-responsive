﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_CAJERAS_TBController : ApiController
    {
        /// <summary>
        /// Obtiene las cajeras existentes
        /// </summary>
        /// <param name="cod_cajera"></param>
        /// <returns></returns>
        [Route("api/TX_CAJERAS_TB/GetCajeras")]
        [ResponseType(typeof(Cajera))]
        public IHttpActionResult GetCajeras(string cod_cajera)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CAJERAS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("cjr_cajera_p", OracleDbType.Varchar2).Value = cod_cajera;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Cajera> _Cajeras = new List<Cajera>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();


       
                foreach (DataRow e in datos2.Rows)
                {
                    string CJR_fch_mod;
                    if (e["cjr_fch_modifico"].ToString() != "")
                    {
                         CJR_fch_mod = DateTime.Parse(e["cjr_fch_modifico"].ToString()).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        CJR_fch_mod = "";
                    }

                    _Cajeras.Add(new Cajera
                    {
                        CJR_Cajera = e["cjr_cajera"].ToString(),
                        CJR_Principal = e["cjr_principal"].ToString(),
                        CJR_Activo = e["cjr_activo"].ToString(),
                        CJR_Doble_impr = e["cjr_doble_impr"].ToString(),
                        CJR_Usu_registro = e["cjr_usu_registro"].ToString(),
                        CJR_Fch_registro = DateTime.Parse(e["cjr_fch_registro"].ToString()).ToString("dd/MM/yyyy"),
                        CJR_Usu_modifico = e["cjr_usu_modifico"].ToString(),
                        CJR_Fch_modifico = CJR_fch_mod
                     
                               
                    });
                }
            
           
               
           

            return Ok(_Cajeras);
        }

        
        /// <summary>
        /// Actualiza la cajera
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cajera_p"></param>
        /// <returns></returns>
        [ResponseType(typeof(Cajera))]
        public IHttpActionResult PutTX_CAJERAS_TB(string id, Cajera cajera_p)
        {
            if (cajera_p.CJR_Principal == null)
            {
                cajera_p.CJR_Principal = "N";
            }
            if (cajera_p.CJR_Activo == null)
            {
                cajera_p.CJR_Activo = "N";
            }
            if (cajera_p.CJR_Doble_impr == null)
            {
                cajera_p.CJR_Doble_impr = "N";
            }
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;

            objCmd.CommandText = "mrx.tx_upd_cajeras_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("cjr_cajera_p", OracleDbType.Varchar2).Value = cajera_p.CJR_Cajera;
            objCmd.Parameters.Add("cjr_principal_p", OracleDbType.Varchar2).Value = cajera_p.CJR_Principal;
            objCmd.Parameters.Add("cjr_activo_p", OracleDbType.Varchar2).Value = cajera_p.CJR_Activo;
            objCmd.Parameters.Add("cjr_doble_impr_p", OracleDbType.Varchar2).Value = cajera_p.CJR_Doble_impr;
            objCmd.Parameters.Add("user_p", OracleDbType.Varchar2).Value = cajera_p.CJR_Usu_modifico;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Cajera _CajeraIn = new Cajera();
            return Ok(_CajeraIn);

        }

        
        /// <summary>
        /// Agrega una nueva cajera
        /// </summary>
        /// <param name="cajera_p"></param>
        /// <returns></returns>
        [ResponseType(typeof(Cajera))]
        public IHttpActionResult PostTX_CAJERAS_TB(Cajera cajera_p)
        {
            if (cajera_p.CJR_Principal == null)
            {
                cajera_p.CJR_Principal = "N";
            }
            if (cajera_p.CJR_Activo == null)
            {
                cajera_p.CJR_Activo = "N";
            }
            if (cajera_p.CJR_Doble_impr == null)
            {
                cajera_p.CJR_Doble_impr = "N";
            }
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_CAJERAS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("cjr_cajera_p", OracleDbType.Varchar2).Value = cajera_p.CJR_Cajera;
            objCmd.Parameters.Add("cjr_principal_p", OracleDbType.Varchar2).Value = cajera_p.CJR_Principal;
            objCmd.Parameters.Add("cjr_activo_p", OracleDbType.Varchar2).Value = cajera_p.CJR_Activo;
            objCmd.Parameters.Add("cjr_doble_impr_p", OracleDbType.Varchar2).Value = cajera_p.CJR_Doble_impr;
            objCmd.Parameters.Add("user_p", OracleDbType.Varchar2).Value = cajera_p.CJR_Usu_registro;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Cajera _CajeraIn = new Cajera();

            return Ok(_CajeraIn);
        }

        
        /// <summary>
        /// Borra la cajera 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(Cajera))]
        public IHttpActionResult Delete(string id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_DEL_CAJERAS_PR ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("cjr_cajera_p", OracleDbType.Varchar2).Value = id;
            objCmd.Parameters.Add("user_p", OracleDbType.Varchar2).Value = "MRX";
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Cajera _CajeraIn = new Cajera();

            return Ok(_CajeraIn);


        }
    }
}
