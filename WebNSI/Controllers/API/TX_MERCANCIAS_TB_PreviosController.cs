﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_MERCANCIAS_TB_PreviosController : ApiController
    {
        [Route("api/TX_MERCANCIAS_TB_Previos/EntradaPrevio")]
        [ResponseType(typeof(EntradasPrevio))]
        public IHttpActionResult GetEntradaPrevio(long? id, string guia, long? movimiento)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_guia_previo_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("entrada_p", OracleDbType.Long).Value = id;
            objCmd.Parameters.Add("guia_p", OracleDbType.Varchar2).Value = guia;
            objCmd.Parameters.Add("movimiento_p", OracleDbType.Long).Value = movimiento;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;



            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


            DataTable datos2 = new DataTable();
            List<EntradasPrevio> _EntradaPrevio = new List<EntradasPrevio>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _EntradaPrevio.Add(new EntradasPrevio
                {
                    IEN_Entrada= long.Parse(r["IEN_ENTRADA"].ToString()),
                    IEN_GUIA_Original= r["IEN_GUIA_ORIGINAL"].ToString(),
                    IEN_Manifiesto = r["IEN_MANIFIESTO"].ToString(),
                    IEN_IMP_Cedula= r["IEN_IMP_CEDULA"].ToString(),
                    IEN_Importador= r["IEN_IMPORTADOR"].ToString(),
                    IEN_BULTOS_Manifestados=decimal.Parse(r["IEN_BULTOS_MANIFESTADOS"].ToString()),
                    //IEN_BULTOS_Realingreso=decimal.Parse(r["IEN_BULTOS_REALINGRESO"].ToString()),
                    BLT_Despaletizados=decimal.Parse(r["BLT_DESP"].ToString()),
                    BLT_Reser=decimal.Parse(r["BLT_RESER"].ToString()),
                    BLT_Salid=decimal.Parse(r["BLT_SALID"].ToString()),
                    IEN_KILOS_Manifestados=decimal.Parse(r["IEN_KILOS_MANIFESTADOS"].ToString()),
                    KG_Desp=decimal.Parse(r["KG_DESP"].ToString()),
                    KG_Reser = decimal.Parse(r["KG_RESER"].ToString()),
                    KG_Salid = decimal.Parse(r["KG_SALID"].ToString()),
                    //IEN_DESCRIPCION_Manif= r["IEN_DESCRIPCION_MANIF"].ToString(),
                    //IEN_Observaciones= r["IEN_OBSERVACIONES"].ToString()
                });
            }
            
            return Ok(_EntradaPrevio);
        }

        [Route("api/TX_MERCANCIAS_TB_Previos/EntradaDetallePrevio")]
        [ResponseType(typeof(DetalleMercanciaPrevio))]
        public IHttpActionResult GetEntradaDetallePrevio(long? id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_tarimas_previo_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("entrada_p", OracleDbType.Long).Value = id;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<DetalleMercanciaPrevio> _DetalleMercanciaPrevio = new List<DetalleMercanciaPrevio>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();

                foreach (DataRow r in datos2.Rows)
                {
                    _DetalleMercanciaPrevio.Add(new DetalleMercanciaPrevio
                    {
                        MER_ID_Mercancia = long.Parse(r["MER_ID_MERCANCIA"].ToString()),
                        MER_TME_Mercancia = r["MER_TME_MERCANCIA"].ToString(),
                        MER_Refrigerado = r["MER_REFRIGERADO"].ToString(),
                        MER_BULTOS_Despaletizados = decimal.Parse(r["MER_BULTOS_DESPALETIZADOS"].ToString()),
                        MER_BULTOS_Salidos = decimal.Parse(r["MER_BULTOS_SALIDOS"].ToString()),
                        KBruto = decimal.Parse(r["Kbruto"].ToString()),
                        MER_TEM_Embalaje = r["MER_TEM_EMBALAJE"].ToString(),
                        TEM_Descripcion = r["desc_mercancia"].ToString(),
                        DESC_Estado = r["DES_ESTADO"].ToString()
                    });
                }

                return Ok(_DetalleMercanciaPrevio);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            //return Ok();
        }

        [Route("api/TX_MERCANCIAS_TB_Previos/TiqueteSala")]
        [ResponseType(typeof(TiqueteSala))]
        public IHttpActionResult GetTiqueteSala(string id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_tiq_sala_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("entrada_p", OracleDbType.Varchar2).Value = id;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<TiqueteSala> _TiqueteSala = new List<TiqueteSala>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();

                foreach (DataRow r in datos2.Rows)
                {
                    _TiqueteSala.Add(new TiqueteSala
                    {
                        ATS_Tiquete = long.Parse(r["ATS_TIQUETE"].ToString()),
                        ATS_TIPO_Cola = r["ATS_TIPO_COLA"].ToString(),
                        ATS_FCH_Impreso = r["FCH_IMPRESO"].ToString(),//DateTime.Parse(r["FCH_IMPRESO"].ToString()),
                        ATS_FCH_Actual = r["FECHA"].ToString()//DateTime.Parse(r["FECHA"].ToString())
                    }) ;
                }

                return Ok(_TiqueteSala);
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_MERCANCIAS_TB_Previos/GuardarCola")]
        public IHttpActionResult PostGuardarCola(ColaAforo colaAforo)
        {
            if (colaAforo.QUE_IEN_ENTRADA != "")
            {
                //DateTime myDate = DateTime.Parse(colaAforo.QUE_HORA_TIQUETE);
                //DateTime myDate1 = DateTime.Parse(colaAforo.QUE_HORA_INICIO_SC);
                //string a = myDate.ToString("dd/MM/yyyy H:mm"); 
                //string b = myDate1.ToString("dd/MM/yyyy H:mm");
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_INS_COLA_AF_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("QUE_IEN_ENTRADA_P", OracleDbType.Int32).Value = colaAforo.QUE_IEN_ENTRADA;
                objCmd.Parameters.Add("QUE_AGA_NAGENCIA_ADUANAL_P", OracleDbType.Int32).Value = colaAforo.QUE_AGA_NAGENCIA_ADUANAL != "" ? colaAforo.QUE_AGA_NAGENCIA_ADUANAL : null;
                objCmd.Parameters.Add("QUE_CONSIGNATARIO_p", OracleDbType.Varchar2).Value = colaAforo.QUE_CONSIGNATARIO; 
                objCmd.Parameters.Add("QUE_IMPORTADOR_P", OracleDbType.Varchar2).Value = colaAforo.QUE_IMPORTADOR;
                objCmd.Parameters.Add("QUE_TIPO_EXAMEN_P", OracleDbType.Int32).Value = colaAforo.QUE_TIPO_EXAMEN != "" ? colaAforo.QUE_TIPO_EXAMEN : null;
                objCmd.Parameters.Add("QUE_TIPO_CLIENTE_P", OracleDbType.Int32).Value = colaAforo.QUE_TIPO_CLIENTE != "" ? colaAforo.QUE_TIPO_CLIENTE : null;
                objCmd.Parameters.Add("QUE_TIPO_PERSONA_P", OracleDbType.Int32).Value = colaAforo.QUE_TIPO_PERSONA != "" ? colaAforo.QUE_TIPO_PERSONA : null;
                objCmd.Parameters.Add("QUE_AUTORIZADO_P", OracleDbType.Varchar2).Value = colaAforo.QUE_AUTORIZADO;
                objCmd.Parameters.Add("QUE_COD_AGENTE_P", OracleDbType.Int32).Value = colaAforo.QUE_COD_AGENTE != "" ? colaAforo.QUE_COD_AGENTE : null;
                objCmd.Parameters.Add("QUE_USU_USUARIO_GENERO_P", OracleDbType.Varchar2).Value = colaAforo.QUE_USU_USUARIO_GENERO;
                objCmd.Parameters.Add("QUE_TIQUETE_P", OracleDbType.Int32).Value = colaAforo.QUE_TIQUETE != "" ? colaAforo.QUE_TIQUETE : null;
                objCmd.Parameters.Add("QUE_HORA_TIQUETE_P", OracleDbType.Varchar2).Value = colaAforo.QUE_HORA_TIQUETE;//myDate.ToString("dd/MM/yyyy H:mm");
                objCmd.Parameters.Add("QUE_HORA_INICIO_SC_P", OracleDbType.Varchar2).Value = colaAforo.QUE_HORA_INICIO_SC;//myDate1.ToString("dd/MM/yyyy H:mm");
                objCmd.Parameters.Add("QUE_TIPO_TIQUETE_P", OracleDbType.Varchar2).Value = colaAforo.QUE_TIPO_TIQUETE;
                objCmd.Parameters.Add("AUTORIDAD_P", OracleDbType.Varchar2).Value = colaAforo.AUTORIDAD;
                try
                {
                    objCmd.ExecuteNonQuery();
                    cn.Close();
                    return Ok(colaAforo.QUE_IEN_ENTRADA);
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
            else
            {
                return BadRequest("Debe existir un número de consecutivo");
            }
        }

        [Route("api/TX_MERCANCIAS_TB_Previos/ActualizaTarimas")]
        public IHttpActionResult PostActualizaTarimas(string[] tarimas)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            foreach (var id in tarimas)
            {
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_UPD_TARIMA_COLA_AF_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Varchar2).Value = id;
                try
                {
                    objCmd.ExecuteNonQuery();
                }
                catch(Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
            cn.Close();
            return Ok("Tarimas actualizadas correctamente");
        }
    }
}