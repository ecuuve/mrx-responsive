﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_BLOQUEOS_TB_GuiasController : ApiController
    {
        [Route("api/TX_BLOQUEOS_TB_Guias/TipoBloqueo")]
        [ResponseType(typeof(TipoBloqueo))]
        public IHttpActionResult GetTipoBloqueo()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_blo_tipo_bloqueo_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<TipoBloqueo> _TipoBloqueo = new List<TipoBloqueo>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TipoBloqueo.Add(new TipoBloqueo
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_TipoBloqueo);
        }

        [Route("api/TX_BLOQUEOS_TB_Guias/ViaSolicitud")]
        [ResponseType(typeof(ViaSolicitud))]
        public IHttpActionResult GetViaSolicitud()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_blo_via_solic_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<ViaSolicitud> _ViaSolicitud = new List<ViaSolicitud>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _ViaSolicitud.Add(new ViaSolicitud
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_ViaSolicitud);
        }

        [Route("api/TX_BLOQUEOS_TB_Guias/GetDatosGuiaBloqueo")]
        [ResponseType(typeof(BloqueoGuia))]
        public IHttpActionResult GetDatosGuiaBloqueo(long? entrada_p, string ien_guia_original_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_bloqueo_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("entrada_p", OracleDbType.Long).Value = entrada_p;
            objCmd.Parameters.Add("ien_guia_original_p", OracleDbType.Varchar2).Value = ien_guia_original_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


            DataTable datos2 = new DataTable();
            List<BloqueoGuia> _BloqueoGuia = new List<BloqueoGuia>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _BloqueoGuia.Add(new BloqueoGuia
                {
                    BLO_IEN_ENTRADA = long.Parse(r["BLO_IEN_ENTRADA"].ToString()),
                    BLO_ID_BLOQUEO = r["BLO_ID_BLOQUEO"].ToString(),
                    BLO_TIPO_BLOQUEO = r["BLO_TIPO_BLOQUEO"].ToString(),
                    BLO_VIA_SOLICITUD = r["BLO_VIA_SOLICITUD"].ToString(),
                    BLO_ACTA = r["BLO_ACTA"].ToString(),
                    BLO_USU_BLOQUEA = r["BLO_USU_BLOQUEA"].ToString(),
                    USUARIO_BLOQUEA = r["USUARIO_BLOQUEA"].ToString(),
                    BLO_FCH_BLOQUEO = r["BLO_FCH_BLOQUEO"].ToString(),
                    BLO_MOTIVO_BLOQUEO = r["BLO_MOTIVO_BLOQUEO"].ToString(),
                    BLO_USU_DESBLOQUEA = r["BLO_USU_DESBLOQUEA"].ToString(),
                    USUARIO_DESBLOQUEA = r["USUARIO_DESBLOQUEA"].ToString(),
                    BLO_FCH_DESBLOQUEO = r["BLO_FCH_DESBLOQUEO"].ToString(),
                    BLO_MOTIVO_DESBLOQUEO = r["BLO_MOTIVO_DESBLOQUEO"].ToString(),
                    BLO_FACTURACION = r["BLO_FACTURACION"].ToString(),
                    BLO_PREVIOS = r["BLO_PREVIOS"].ToString(),
                    BLO_ORDENES_DESP = r["BLO_ORDENES_DESP"].ToString(),
                    GUIA = r["GUIA"].ToString(),
                    MANIFIESTO = r["MANIFIESTO"].ToString(),
                    FECHA_INGRESO = r["FECHA_INGRESO"].ToString(),
                    EXISTENCIAS = r["EXISTENCIAS"].ToString(),
                    IMPORTADOR = r["IMPORTADOR"].ToString()

                });
            }

            return Ok(_BloqueoGuia);
        }

        [Route("api/TX_BLOQUEOS_TB_Guias/GetDatosBloqueoDetalle")]
        [ResponseType(typeof(BloqueoGuia))]
        public IHttpActionResult GetDatosBloqueoDetalle(long blo_ien_entrada_p, long blo_id_bloqueo_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_bloqueo_det_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("blo_ien_entrada_p", OracleDbType.Long).Value = blo_ien_entrada_p;
            objCmd.Parameters.Add("blo_id_bloqueo_p", OracleDbType.Varchar2).Value = blo_id_bloqueo_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


            DataTable datos2 = new DataTable();
            List<BloqueoGuia> _BloqueoGuia = new List<BloqueoGuia>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _BloqueoGuia.Add(new BloqueoGuia
                {
                    BLO_IEN_ENTRADA = long.Parse(r["BLO_IEN_ENTRADA"].ToString()),
                    BLO_ID_BLOQUEO = r["BLO_ID_BLOQUEO"].ToString(),
                    BLO_TIPO_BLOQUEO = r["BLO_TIPO_BLOQUEO"].ToString(),
                    BLO_VIA_SOLICITUD = r["BLO_VIA_SOLICITUD"].ToString(),
                    BLO_ACTA = r["BLO_ACTA"].ToString(),
                    BLO_USU_BLOQUEA = r["BLO_USU_BLOQUEA"].ToString(),
                    USUARIO_BLOQUEA = r["USUARIO_BLOQUEA"].ToString(),
                    BLO_FCH_BLOQUEO = r["BLO_FCH_BLOQUEO"].ToString(),
                    BLO_MOTIVO_BLOQUEO = r["BLO_MOTIVO_BLOQUEO"].ToString(),
                    BLO_USU_DESBLOQUEA = r["BLO_USU_DESBLOQUEA"].ToString(),
                    USUARIO_DESBLOQUEA = r["USUARIO_DESBLOQUEA"].ToString(),
                    BLO_FCH_DESBLOQUEO = r["BLO_FCH_DESBLOQUEO"].ToString(),
                    BLO_MOTIVO_DESBLOQUEO = r["BLO_MOTIVO_DESBLOQUEO"].ToString(),
                    BLO_FACTURACION = r["BLO_FACTURACION"].ToString(),
                    BLO_PREVIOS = r["BLO_PREVIOS"].ToString(),
                    BLO_ORDENES_DESP = r["BLO_ORDENES_DESP"].ToString(),
                    GUIA = r["GUIA"].ToString(),
                    MANIFIESTO = r["MANIFIESTO"].ToString(),
                    FECHA_INGRESO = r["FECHA_INGRESO"].ToString(),
                    EXISTENCIAS = r["EXISTENCIAS"].ToString(),
                    IMPORTADOR = r["IMPORTADOR"].ToString()

                });
            }

            return Ok(_BloqueoGuia);
        }

        [Route("api/TX_BLOQUEOS_TB_Guias/GetDatosConsecutivos")]
        [ResponseType(typeof(Consecutivo))]
        public IHttpActionResult GetDatosConsecutivos(string ien_guia_original_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_conse_bloqueos_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ien_guia_original_p", OracleDbType.Varchar2).Value = ien_guia_original_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


            DataTable datos2 = new DataTable();
            List<Consecutivo> _Consecutivo = new List<Consecutivo>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Consecutivo.Add(new Consecutivo
                {
                    BLO_IEN_ENTRADA = long.Parse(r["IEN_ENTRADA"].ToString()),
                    MANIFIESTO = r["IEN_MANIFIESTO"].ToString(),
                    ORIGINAL = r["ORIGINAL"].ToString()

                });
            }

            return Ok(_Consecutivo);
        }

        [Route("api/TX_BLOQUEOS_TB_Guias/GetDatosConsecutivoDetalle")]
        [ResponseType(typeof(Consecutivo))]
        public IHttpActionResult GetDatosConsecutivoDetalle(long blo_ien_entrada_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_conse_det_bloqueos_pr ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("blo_ien_entrada_p", OracleDbType.Long).Value = blo_ien_entrada_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


            DataTable datos2 = new DataTable();
            List<Consecutivo> _Consecutivo = new List<Consecutivo>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Consecutivo.Add(new Consecutivo
                {
                    BLO_IEN_ENTRADA = long.Parse(r["IEN_ENTRADA"].ToString()),
                    FECHA_INGRESO = r["IEN_FCH_INGRESO"].ToString(),
                    MANIFIESTO = r["IEN_MANIFIESTO"].ToString(),
                    GUIA = r["IEN_GUIA"].ToString(),
                    IMPORTADOR = r["IEN_IMPORTADOR"].ToString(),
                    EXISTENCIAS = long.Parse(r["EXISTENCIAS"].ToString())

                });
            }

            return Ok(_Consecutivo);
        }


        [ResponseType(typeof(BloqueoGuia))]
        public IHttpActionResult PostTX_BLOQUEOS_TB_Guias(BloqueoGuia bloqueo_p)
        {
            if (bloqueo_p.BLO_PREVIOS == null) {
                bloqueo_p.BLO_PREVIOS = "N";
            }
            if (bloqueo_p.BLO_ORDENES_DESP == null){
                bloqueo_p.BLO_ORDENES_DESP = "N";
            }
            if (bloqueo_p.BLO_FACTURACION == null){
                bloqueo_p.BLO_FACTURACION = "N";
            }
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_BLOQUEO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("blo_ien_entrada_p", OracleDbType.Long).Value = bloqueo_p.BLO_IEN_ENTRADA;
            objCmd.Parameters.Add("blo_facturacion_p", OracleDbType.Varchar2).Value = bloqueo_p.BLO_FACTURACION;
            objCmd.Parameters.Add("blo_previos_p", OracleDbType.Varchar2).Value = bloqueo_p.BLO_PREVIOS;
            objCmd.Parameters.Add("blo_ordenes_desp_p", OracleDbType.Varchar2).Value = bloqueo_p.BLO_ORDENES_DESP;
            objCmd.Parameters.Add("blo_tipo_bloqueo_p", OracleDbType.Varchar2).Value = bloqueo_p.BLO_TIPO_BLOQUEO;
            objCmd.Parameters.Add("blo_via_solicitud_p", OracleDbType.Varchar2).Value = bloqueo_p.BLO_VIA_SOLICITUD;
            objCmd.Parameters.Add("blo_acta_p", OracleDbType.Varchar2).Value = bloqueo_p.BLO_ACTA;
            objCmd.Parameters.Add("blo_motivo_bloqueo_p", OracleDbType.Varchar2).Value = bloqueo_p.BLO_MOTIVO_BLOQUEO;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = bloqueo_p.USER_MERX;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();

            return Ok(bloqueo_p);
        }

        [ResponseType(typeof(BloqueoGuia))]
        public IHttpActionResult PutTX_BLOQUEOS_TB_Guias(string id, BloqueoGuia bloqueo_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;

            objCmd.CommandText = "mrx.TX_UPD_BLOQUEO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("blo_ien_entrada_p", OracleDbType.Long).Value = bloqueo_p.BLO_IEN_ENTRADA;
            objCmd.Parameters.Add("blo_id_bloqueo_p", OracleDbType.Varchar2).Value = bloqueo_p.BLO_ID_BLOQUEO;
            objCmd.Parameters.Add("blo_motivo_desbloqueo_p", OracleDbType.Varchar2).Value = bloqueo_p.BLO_MOTIVO_DESBLOQUEO;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = bloqueo_p.USER_MERX;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

           

        }
    }


}
