﻿using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_DESCONSOLIDACIONES_TBController : ApiController
    {
        [Route("api/TX_DESCONSOLIDACIONES_TB/Desconsolidaciones")]
        [ResponseType(typeof(Desconsolidacion1))]
        public IHttpActionResult GetDesconsolidadores(long? entrada, string guia)
        {

            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_desconsolida_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dsc_ien_entrada_p", OracleDbType.Long).Value = entrada;
            objCmd.Parameters.Add("dsc_guia_original_p", OracleDbType.Varchar2).Value = guia;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Desconsolidacion1> _Entradas = new List<Desconsolidacion1>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {
                    _Entradas.Add(new Desconsolidacion1
                    {
                        DSC_ID = r["DSC_ID"].ToString(),
                        DSC_IEN_ENTRADA = r["DSC_IEN_ENTRADA"].ToString(),
                        IEN_GUIA_ORIGINAL = r["IEN_GUIA_ORIGINAL"].ToString(),
                        CON_NOMBRE = r["CON_NOMBRE"].ToString(),
                        DSC_FCH_REGISTRA = r["DSC_FCH_REGISTRA"].ToString(),
                        DSC_USU_REGISTRA = r["DSC_USU_REGISTRA"].ToString(),
                        DSC_ARCHIVO = r["DSC_ARCHIVO"].ToString()
                        //DSC_ARCHIVO = System.Web.Configuration.WebConfigurationManager.AppSettings["Mypath"].ToString() + r["DSC_ARCHIVO"].ToString(),
                    });
                }
                return Ok(_Entradas);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/TX_DESCONSOLIDACIONES_TB/Actualizar")]
        [ResponseType(typeof(Desconsolidacion1))]
        public IHttpActionResult PutTX_ENTRADAS_TB_Ingreso(string id, Desconsolidacion1 desconsolidacion)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_upd_desconsolida_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dsc_id_p", OracleDbType.Int32).Value = desconsolidacion.DSC_ID;
            objCmd.Parameters.Add("dsc_usu_imprime_p", OracleDbType.Varchar2).Value = desconsolidacion.USUARIO;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/TX_DESCONSOLIDACIONES_TB/Imprimir")]
        public IHttpActionResult GetImprimir(long? entrada, string guia)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_desconsolida_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dsc_ien_entrada_p", OracleDbType.Long).Value = entrada;
            objCmd.Parameters.Add("dsc_guia_original_p", OracleDbType.Varchar2).Value = guia;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                Desconsolidacion1 _Entradas = new Desconsolidacion1();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                if (datos2.Rows.Count == 1)
                {
                    foreach (DataRow r in datos2.Rows)
                    {
                        _Entradas.DSC_ID = r["DSC_ID"].ToString();
                        _Entradas.DSC_IEN_ENTRADA = r["DSC_IEN_ENTRADA"].ToString();
                        _Entradas.IEN_GUIA_ORIGINAL = r["IEN_GUIA_ORIGINAL"].ToString();
                        _Entradas.CON_NOMBRE = r["CON_NOMBRE"].ToString();
                        _Entradas.DSC_FCH_REGISTRA = r["DSC_FCH_REGISTRA"].ToString();
                        _Entradas.DSC_USU_REGISTRA = r["DSC_USU_REGISTRA"].ToString();
                        _Entradas.DSC_ARCHIVO = r["DSC_ARCHIVO"].ToString();
                    }
                }
                ////ELIMINA TODOS LOS ARCHIVOS QUE HAY EN LA CARPETA
                //string carpetaLocal = HttpContext.Current.Server.MapPath("~\\assets\\PruebaPrealertas\\");
                //string[] Total_files = Directory.GetFiles(carpetaLocal);
                //int total = Total_files.Length;
                //int i = 0;
                //int current = total - 1;
                //while (current >= i)
                //{
                //    string file = Path.GetFileName(Total_files[current]);
                //    System.IO.File.Delete(Total_files[current]);
                //    current = current - 1;
                //}

                ////COPIA EL ARCHIVO DEL SERVIDOR A LA CARPETA LOCAL
                //string pathfileTo = HttpContext.Current.Server.MapPath("~\\assets\\PruebaPrealertas\\" + _Entradas.DSC_ARCHIVO);
                //string path = System.Web.Configuration.WebConfigurationManager.AppSettings["Mypath"].ToString() + _Entradas.DSC_ARCHIVO;
                //bool Exist_rep = File.Exists(pathfileTo + _Entradas.DSC_ARCHIVO);
                //if (Exist_rep == false)
                //{
                //    System.IO.File.Copy(path, pathfileTo);
                //}
                //_Entradas.DSC_ARCHIVO = "/assets/PruebaPrealertas/" + _Entradas.DSC_ARCHIVO;
                //return Ok(_Entradas);

                string serverFolder = "\\\\Dpsavfss\\COMPARTIDOS\\PUBLICO\\PREALERTASALSATEST\\";
                string destinationFilename = Path.Combine(
                           serverFolder + _Entradas.DSC_ARCHIVO);
                bool extfile = File.Exists(destinationFilename);
                if (extfile)
                {
                    System.Diagnostics.Process.Start(destinationFilename);
                    //window.open(destinationFilename, '_blank');
                    return Ok(_Entradas);
                }
                else
                {
                    string str = "No existe el archivo.";
                   
                    return BadRequest(str);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}
