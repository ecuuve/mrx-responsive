﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_DIFERENCIAS_TOMA_TBController : ApiController
    {
        /// <summary>
        /// Listar las diferencias 
        /// </summary>
        /// <param name="cod_inventario"></param>
        /// <returns></returns>
        [Route("api/TX_DIFERENCIAS_TOMA_TB/GetDiferencias")]
        [ResponseType(typeof(Diferencia))]
        public IHttpActionResult GetDiferencias(long cod_inventario)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_dif_tomas_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dtf_itf_inventario_p", OracleDbType.Int32).Value = cod_inventario;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Diferencia> _Diferencias = new List<Diferencia>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _Diferencias.Add(new Diferencia
                    {
                        DTF_ID = e["DTF_ID"].ToString(),
                        DTF_MER_ID_MERCANCIA = e["DTF_MER_ID_MERCANCIA"].ToString(),
                        CONSECUTIVO = e["CONSECUTIVO"].ToString(),
                        DTF_BULTOS = e["DTF_BULTOS"].ToString(),
                        CONSIGNATARIO = e["CONSIGNATARIO"].ToString(),
                        DTF_TIPO_DIFERENCIA = e["DTF_TIPO_DIFERENCIA"].ToString(),  //DateTime.Parse(e["cjr_fch_registro"].ToString()).ToString("dd/MM/yyyy"),
                        DTF_LOCALIZACION = e["DTF_LOCALIZACION"].ToString(),
                        DTF_ESTADO = e["DTF_ESTADO"].ToString(),
                        DESC_ESTADO = e["DESC_ESTADO"].ToString(),
                        DESC_CAUSA = e["DESC_CAUSA"].ToString(),
                        DTF_SOLUCION = e["DTF_SOLUCION"].ToString(),
                        INVENTARIO = e["INVENTARIO"].ToString()
                    });
                }

                return Ok(_Diferencias);
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
            
        }


        /// <summary>
        /// Obtiene detalle de diferencia
        /// </summary>
        /// <param name="cod_diferencia"></param>
        /// <returns></returns>
        [Route("api/TX_DIFERENCIAS_TOMA_TB/GetDiferenciaDetalle")]
        [ResponseType(typeof(Diferencia))]
        public IHttpActionResult GetDiferenciaDetalle(long cod_diferencia)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_dif_tomas_det_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dtf_id_p", OracleDbType.Int32).Value = cod_diferencia;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Diferencia> _Diferencias = new List<Diferencia>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();
            foreach (DataRow e in datos2.Rows)
            {
                _Diferencias.Add(new Diferencia
                {
                    DTF_ID = e["DTF_ID"].ToString(),
                    DTF_MER_ID_MERCANCIA = e["DTF_MER_ID_MERCANCIA"].ToString(),
                    CONSIGNATARIO = e["CONSIGNATARIO"].ToString(),
                    CONSECUTIVO = e["CONSECUTIVO"].ToString(),
                    DTF_BULTOS = e["DTF_BULTOS"].ToString(),
                    DTF_LOCALIZACION = e["DTF_LOCALIZACION"].ToString(),
                    DTF_TIPO_DIFERENCIA = e["DTF_TIPO_DIFERENCIA"].ToString(),  //DateTime.Parse(e["cjr_fch_registro"].ToString()).ToString("dd/MM/yyyy"),
                    DTF_ID_CAUSA = e["DTF_ID_CAUSA"].ToString(),
                    DESC_CAUSA = e["DESC_CAUSA"].ToString(),
                    DTF_SOLUCION = e["DTF_SOLUCION"].ToString(),
                    DTF_ESTADO = e["DTF_ESTADO"].ToString()  
                });
            }

            return Ok(_Diferencias);
        }


        /// <summary>
        /// Lista las causas 
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_DIFERENCIAS_TOMA_TB/GetCausas")]
        [ResponseType(typeof(Causa))]
        public IHttpActionResult GetCausas()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_dt_causas_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("id_causa_p", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Causa> _Causas = new List<Causa>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();
            foreach (DataRow e in datos2.Rows)
            {
                _Causas.Add(new Causa
                {
                    Id = e["PTF_ID_CAUSA"].ToString(),
                    Nombre = e["DESC_CAUSA"].ToString()
                });
            }

            return Ok(_Causas);
        }


        /// <summary>
        /// Lista los inventarios disponibles
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_DIFERENCIAS_TOMA_TB/GetInventarios")]
        [ResponseType(typeof(Inventario))]
        public IHttpActionResult GetInventarios()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_dt_inventarios_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dtf_itf_inventario_p", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Inventario> _Inventarios = new List<Inventario>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();
            foreach (DataRow e in datos2.Rows)
            {
                _Inventarios.Add(new Inventario
                {
                    ITF_INVENTARIO = e["ITF_INVENTARIO"].ToString(),
                    INVENTARIO = e["INVENTARIO"].ToString()
                });
            }

            return Ok(_Inventarios);
        }

        /// <summary>
        /// Obtiena la lista de valores de tipos
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_DIFERENCIAS_TOMA_TB/GetTiposDif")]
        [ResponseType(typeof(TipoDiferencia))]
        public IHttpActionResult GetTiposDif()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_dtf_tipo_dif_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<TipoDiferencia> _TipoDiferencia = new List<TipoDiferencia>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TipoDiferencia.Add(new TipoDiferencia
                {
                    Id = r["ID"].ToString(),
                    Nombre = r["NOMBRE"].ToString()
                });
            }

            return Ok(_TipoDiferencia);
        }

        /// <summary>
        /// Obtiene la lista de valores de estado
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_DIFERENCIAS_TOMA_TB/GetEstados")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetEstados()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_dtf_estado_pr ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Estados> _Estados = new List<Estados>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Estados.Add(new Estados
                {
                    Id = r["ID"].ToString(),
                    Nombre = r["NOMBRE"].ToString()
                });
            }

            return Ok(_Estados);
        }

        /// <summary>
        /// Actualiza diferencias 
        /// </summary>
        /// <param name="dif_p"></param>
        /// <returns></returns>
        [ResponseType(typeof(Diferencia))]
        public IHttpActionResult PutTX_DIFERENCIAS_TOMA_TB(Diferencia dif_p)
        {           
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;

            objCmd.CommandText = "mrx.tx_upd_dif_tomas_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dtf_id_p", OracleDbType.Int32).Value = dif_p.DTF_ID;
            objCmd.Parameters.Add("dtf_estado_p", OracleDbType.Varchar2).Value = dif_p.DTF_ESTADO;
            objCmd.Parameters.Add("dtf_id_causa_p", OracleDbType.Int32).Value = dif_p.DTF_ID_CAUSA;
            objCmd.Parameters.Add("dtf_solucion_p", OracleDbType.Varchar2).Value = dif_p.DTF_SOLUCION;
            objCmd.Parameters.Add("dtf_usu_modifico_p", OracleDbType.Varchar2).Value = dif_p.UserMERX;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Diferencia _DiferenciaUP = new Diferencia();
            return Ok(_DiferenciaUP);

        }

    }
}
