﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_ATENCION_SALA_TBController : ApiController
    {
        /// <summary>
        /// Lista las colas de atención
        /// </summary>
        /// <param name="tipoCola"></param>
        /// <returns></returns>
        [Route("api/TX_ATENCION_SALA_TB/GetColasAtencion")]
        [ResponseType(typeof(AtencionSala))]
        public IHttpActionResult GetColasAtencion(string tipoCola)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_atencion_sala_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ats_tipo_cola_p", OracleDbType.Varchar2).Value = tipoCola;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<AtencionSala> _ColaAtencionSala = new List<AtencionSala>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();


       
                foreach (DataRow e in datos2.Rows)
                {
                _ColaAtencionSala.Add(new AtencionSala
                    {
                        ATS_ID = e["ATS_ID"].ToString(),
                        ATS_TIQUETE = e["ATS_TIQUETE"].ToString(),
                        ATS_FCH_IMPRESO = e["ATS_FCH_IMPRESO"].ToString(),
                        ATS_TIPO_COLA = e["ATS_TIPO_COLA"].ToString(),
                        ATS_ESTADO = e["ATS_ESTADO"].ToString(),
                        DESC_ESTADO = e["DESC_ESTADO"].ToString(),
                        ATS_FCH_ATENDIDO = e["ATS_FCH_ATENDIDO"].ToString(),
                        USUARIO_DISPLAY = e["USUARIO_DISPLAY"].ToString(),//DateTime.Parse(e["cjr_fch_registro"].ToString()).ToString("dd/MM/yyyy"),
                        ATS_PRIORIDAD = e["ATS_PRIORIDAD"].ToString()             
                    });
                }

            foreach (AtencionSala cola in _ColaAtencionSala)
            {
                switch (cola.ATS_ESTADO)
                {
                    case "P":
                        cola.PENDIENTE = true;
                        cola.ATENDIDO = false;
                        cola.FINALIZADO = false;
                        cola.NOPRESENTE = false;
                        break;
                    case "A":
                        cola.PENDIENTE = false;
                        cola.ATENDIDO = true;
                        cola.FINALIZADO = false;
                        cola.NOPRESENTE = false;
                        break;
                    case "F":
                        cola.PENDIENTE = false;
                        cola.ATENDIDO = false;
                        cola.FINALIZADO = true;
                        cola.NOPRESENTE = false;
                        break;
                    case "N":
                        cola.PENDIENTE = false;
                        cola.ATENDIDO = false;
                        cola.FINALIZADO = false;
                        cola.NOPRESENTE = true;
                        break;
                    default:
                        //Console.WriteLine("Default case");
                        break;
                }

            }

            return Ok(_ColaAtencionSala);
        }


        /// <summary>
        /// Lista Usuarios
        /// </summary>
        /// <param name="usuario_p"></param>
        /// <param name="opcion_p"></param>
        /// <returns></returns>
        [Route("api/TX_ATENCION_SALA_TB/GetUsuarios")]
        [ResponseType(typeof(UsuarioAtencionSala))]
        public IHttpActionResult GetUsuarios(string usuario_p, string opcion_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_ats_permisos_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("uas_usuario_p", OracleDbType.Varchar2).Value = usuario_p;
            objCmd.Parameters.Add("uas_opcion_p", OracleDbType.Varchar2).Value = opcion_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<UsuarioAtencionSala> _UsuariosAtSala = new List<UsuarioAtencionSala>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();

                foreach (DataRow e in datos2.Rows)
                {
                    _UsuariosAtSala.Add(new UsuarioAtencionSala
                    {
                        UAS_USU_USUARIO = e["UAS_USU_USUARIO"].ToString(),
                        NOMBRE = e["NOMBRE"].ToString(),
                        USUARIO = e["USUARIO"].ToString(),
                        UAS_OPCION = e["UAS_OPCION"].ToString(),
                        DESC_UAS_OPCION = e["DESC_UAS_OPCION"].ToString(),
                        UAS_PRIORIDAD = e["UAS_PRIORIDAD"].ToString()
                    });
                }

                return Ok(_UsuariosAtSala);
            }
            catch (Exception ex)
            {
                cn.Close();
                return Ok(ex.Message);
            }
            
        }


        /// <summary>
        /// Lista los permisos de usuarios
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_ATENCION_SALA_TB/Permisos")]
        [ResponseType(typeof(Permisos))]
        public IHttpActionResult GetPermisos()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_ats_uas_opcion_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Permisos> _Permisos = new List<Permisos>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Permisos.Add(new Permisos
                {
                    Id = r["ID"].ToString(),
                    Nombre = r["NOMBRE"].ToString()
                });
            }

            return Ok(_Permisos);
        }

        /// <summary>
        /// Inserta permisos a usuario
        /// </summary>
        /// <param name="usuario_p"></param>
        /// <param name="opcion_p"></param>
        /// <param name="prioridad_p"></param>
        /// <returns></returns>
        [ResponseType(typeof(Permisos))]
        public IHttpActionResult PostTX_ATENCION_SALA_TB(string usuario_p,string opcion_p,string prioridad_p)
        {

            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_ats_permisos_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("uas_usuario_p", OracleDbType.Varchar2).Value = usuario_p;
            objCmd.Parameters.Add("uas_opcion_p", OracleDbType.Varchar2).Value = opcion_p;
            objCmd.Parameters.Add("uas_prioridad_p", OracleDbType.Varchar2).Value = prioridad_p;        
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Permisos _PermisosIn = new Permisos();

            return Ok(_PermisosIn);
        }


         /// <summary>
         /// Actualiza el tiquete de atención sala (cambio de estado)
         /// </summary>
         /// <param name="tipo_p"></param>
         /// <param name="id_p"></param>
         /// <param name="estado_p"></param>
         /// <param name="prior_p"></param>
         /// <param name="usu_p"></param>
         /// <returns></returns>
        [ResponseType(typeof(Permisos))]
        public IHttpActionResult PutTX_ATENCION_SALA_TB(string tipo_p, int id_p, string estado_p, string prior_p, string usu_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;

            objCmd.CommandText = "mrx.tx_upd_atencion_sala_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ats_tipo_cola_p", OracleDbType.Varchar2).Value = tipo_p;
            objCmd.Parameters.Add("ats_id_p", OracleDbType.Int32).Value = id_p;
            objCmd.Parameters.Add("ats_estado_p", OracleDbType.Varchar2).Value = estado_p;
            objCmd.Parameters.Add("ats_prioridad_p", OracleDbType.Varchar2).Value = prior_p;
            objCmd.Parameters.Add("ats_usu_atendio_p", OracleDbType.Varchar2).Value = usu_p;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            AtencionSala _AtencionSalaUP = new AtencionSala();
            return Ok(_AtencionSalaUP);

        }


        /// <summary>
        /// Elimina el permiso asignado al usuario
        /// </summary>
        /// <param name="usuario_p"></param>
        /// <param name="opcion_p"></param>
        /// <returns></returns>
        [ResponseType(typeof(Permisos))]
        public IHttpActionResult Delete(string usuario_p, string opcion_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_del_ats_permisos_pr ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("uas_usuario_p", OracleDbType.Varchar2).Value = usuario_p;
            objCmd.Parameters.Add("uas_opcion_p", OracleDbType.Varchar2).Value = opcion_p;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Permisos _PermisosIn = new Permisos();

            return Ok(_PermisosIn);


        }

    }
}
