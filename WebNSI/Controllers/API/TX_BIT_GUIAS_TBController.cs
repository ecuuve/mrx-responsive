﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_BIT_GUIAS_TBController : ApiController
    {
        [Route("api/TX_BIT_GUIAS_TB/GetBitacora")]
        [ResponseType(typeof(Bitacora))]
        public IHttpActionResult GetBitacora(int? consecutivo, int? movimiento, int? tipoEvento, string guia)
        {
            if(tipoEvento == 0)
            {
                tipoEvento = null;
            }
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_bit_guias_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ibg_ien_entrada_p", OracleDbType.Int32).Value = consecutivo;
            objCmd.Parameters.Add("ibg_teb_tipo_evento_p", OracleDbType.Int32).Value = tipoEvento;
            objCmd.Parameters.Add("ibg_mer_mov_inventario_p", OracleDbType.Int32).Value = movimiento;
            objCmd.Parameters.Add("ibg_ien_guia_original_p", OracleDbType.Varchar2).Value = guia;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            //objCmd.ExecuteNonQuery();
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            DataTable datos2 = new DataTable();
            List<Bitacora> _RegsBitacora = new List<Bitacora>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();



            foreach (DataRow e in datos2.Rows)
            {
                _RegsBitacora.Add(new Bitacora
                {
                    BIT_Consecutivo = long.Parse(e["ibg_ien_entrada"].ToString()),
                    BIT_Descripcion = e["ibg_detalle"].ToString(),
                    BIT_Evento = long.Parse(e["ibg_teb_tipo_evento"].ToString()),
                    BIT_Evento_Desc = e["desc_evento"].ToString(),
                    BIT_Fecha = e["ibg_fch_evento"].ToString(),
                    BIT_GuiaOriginal = e["ibg_ien_guia_original"].ToString(),
                //    if (e["ibg_mer_mov_inventario"].ToString() != "")
                //{
                    BIT_Movimiento = e["ibg_mer_mov_inventario"].ToString(),//long.Parse(e["ibg_mer_mov_inventario"].ToString())
                //}

                    BIT_Tarima = e["ibg_mer_id_mercancia"].ToString(),//long.Parse(e["ibg_mer_id_mercancia"].ToString()),
                    BIT_Tarima_Desc = e["desc_mercancia"].ToString(),
                    BIT_Usuario = e["ibg_usu_evento"].ToString()


                });
            }





            return Ok(_RegsBitacora);
        }


        [Route("api/TX_BIT_GUIAS_TB/TipoEvento")]
        [ResponseType(typeof(TipoEvento))]
        public IHttpActionResult GetTipoEvento()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_tipo_evento_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<TipoEvento> _TipoEvento = new List<TipoEvento>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TipoEvento.Add(new TipoEvento
                {
                    Id = r["id"].ToString(),
                    Nombre = r["descripcion"].ToString()
                });
            }

            return Ok(_TipoEvento);
        }


    }
}
