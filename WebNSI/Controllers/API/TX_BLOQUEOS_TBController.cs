﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_BLOQUEOS_TBController : ApiController
    {
        [Route("api/TX_BLOQUEOS_TB/GetDatosGuiaBloqueo")]
        [ResponseType(typeof(EntradaBloqueo))]
        public IHttpActionResult GetDatosGuiaBloqueo(long? id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_datos_bloqueo_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("entrada_p", OracleDbType.Long).Value = id;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("existencias_p", OracleDbType.Decimal).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
          

            DataTable datos2 = new DataTable();
            List<EntradaBloqueo> _Entrada = new List<EntradaBloqueo>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();
            long paramExist = long.Parse(objCmd.Parameters["existencias_p"].Value.ToString());
            //string paramErr = objCmd.Parameters["error_p"].Value.ToString();

            foreach (DataRow r in datos2.Rows)
            {
                _Entrada.Add(new EntradaBloqueo
                {
                    IEN_Entrada = long.Parse(r["IEN_ENTRADA"].ToString()),
                    IEN_Guia = r["IEN_GUIA"].ToString(),
                    IEN_Manifiesto = r["IEN_MANIFIESTO"].ToString(),
                    IEN_FCH_Ingreso = r["IEN_FCH_INGRESO"].ToString(),
                    IEN_Importador = r["IEN_IMPORTADOR"].ToString(),
                    Existencias_p = paramExist,
                    //Error_p = paramErr

                });
            }

            return Ok(_Entrada);
        }

        [Route("api/TX_BLOQUEOS_TB/TipoBloqueo")]
        [ResponseType(typeof(TipoBloqueo))]
        public IHttpActionResult GetTipoBloqueo()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_blo_tipo_bloqueo_ops_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<TipoBloqueo> _TipoBloqueo = new List<TipoBloqueo>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TipoBloqueo.Add(new TipoBloqueo
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_TipoBloqueo);
        }

        [Route("api/TX_BLOQUEOS_TB/ViaSolicitud")]
        [ResponseType(typeof(ViaSolicitud))]
        public IHttpActionResult GetViaSolicitud()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_blo_via_solic_ops_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<ViaSolicitud> _ViaSolicitud = new List<ViaSolicitud>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _ViaSolicitud.Add(new ViaSolicitud
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_ViaSolicitud);
        }

        [ResponseType(typeof(EntradaBloqueo))]
        public IHttpActionResult PostTX_BLOQUEOS_TB(EntradaBloqueo bloqueo_p)
        {

            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_BLOQUEO_OPS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("blo_ien_entrada_p", OracleDbType.Long).Value = bloqueo_p.IEN_Entrada;
            objCmd.Parameters.Add("blo_motivo_bloqueo_p", OracleDbType.Varchar2).Value = bloqueo_p.BLO_MOTIVO_Bloqueo;
            objCmd.Parameters.Add("blo_tipo_bloqueo_p", OracleDbType.Varchar2).Value = bloqueo_p.BLO_TIPO_Bloqueo;
            objCmd.Parameters.Add("blo_via_solicitud_p", OracleDbType.Varchar2).Value = bloqueo_p.BLO_VIA_Solicitud;
            objCmd.Parameters.Add("blo_acta_p", OracleDbType.Varchar2).Value = bloqueo_p.BLO_Acta;
            objCmd.Parameters.Add("user_p", OracleDbType.Varchar2).Value = "MRX";

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {              
                return Ok(ex.Message);
            }

            cn.Close();

            return Ok(bloqueo_p);
        }

    }
}