﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_REGISTRO_MUESTRAS_TBController : ApiController
    {
        [Route("api/TX_REGISTRO_MUESTRAS_TB/TipoMuestra")]
        [ResponseType(typeof(RM_TipoMuestra))]
        public IHttpActionResult GetTipoMuestra()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_mue_tipo_muestra_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<RM_TipoMuestra> data = new List<RM_TipoMuestra>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                data.Add(new RM_TipoMuestra
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(data);
        }

        [Route("api/TX_REGISTRO_MUESTRAS_TB/Motivo")]
        [ResponseType(typeof(RM_Motivo))]
        public IHttpActionResult GetMotivo()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_mue_motivo_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<RM_Motivo> data = new List<RM_Motivo>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                data.Add(new RM_Motivo
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(data);
        }

        [Route("api/TX_REGISTRO_MUESTRAS_TB/BuscarRegistroMuestras")]
        [ResponseType(typeof(RM_Registro_Muestras))]
        public IHttpActionResult BuscarRegistroMuestras([FromBody] RM_Registro_Muestras_Input input)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_muestras_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mue_boleta_p", OracleDbType.Varchar2).Value = input.s_boleta;
            objCmd.Parameters.Add("mue_ien_entrada_p", OracleDbType.Varchar2).Value = input.s_consecutivo;
            objCmd.Parameters.Add("mue_manifiesto_p", OracleDbType.Int32).Value = input.s_manifiesto;
            objCmd.Parameters.Add("mue_guia", OracleDbType.Varchar2).Value = input.s_guia;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            DataTable datos2 = new DataTable();
            List<RM_Registro_Muestras> data = new List<RM_Registro_Muestras>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                data.Add(new RM_Registro_Muestras
                {
                    MUE_FECHA = r["mue_fecha"].AsFormattedDate(),
                    MUE_BOLETA = r["mue_boleta"].ToString(),
                    MUE_IEN_ENTRADA = r["mue_ien_entrada"].ToString(),
                    MUE_MANIFIESTO = r["mue_manifiesto"].ToString(),
                    MUE_GUIA = r["mue_guia"].ToString(),
                    MUE_CONSIGNATARIO = r["mue_consignatario"].ToString(),
                    MUE_FCH_MUESTRA = r["mue_fch_muestra"].AsFormattedDate(),
                    BULTOS = r["bultos"].ToString(),
                    KILOS = r["kilos"].ToString(),
                    MUE_TIPO_MUESTRA = r["mue_tipo_muestra"].ToString(),
                    DESC_TIPO_MUE = r["DESC_TIPO_MUE"].ToString(),
                    MUE_DUA = r["mue_dua"].ToString(),
                    MUE_CODIGO_MOTIVO = r["mue_codigo_motivo"].ToString(),
                    MUE_MOTIVO = r["mue_motivo"].ToString(),
                    MUE_AGA_NAGENCIA = r["mue_aga_nagencia"].ToString(),
                    NOMB_AGENCIA = r["NOMB_AGENCIA"].ToString(),
                    MUE_JEFE_AFORADOR = r["mue_jefe_aforador"].ToString(),
                    MUE_AFORADOR = r["mue_aforador"].ToString(),
                    MUE_RESPONS_ALMACEN = r["mue_respons_almacen"].ToString(),
                    MUE_LOC_RACK = r["mue_loc_rack"].ToString(),
                    MUE_LOC_COLUMNA = r["mue_loc_columna"].ToString(),
                    MUE_LOC_ALTURA = r["mue_loc_altura"].ToString(),
                    MUE_LOC_LADO = r["mue_loc_lado"].ToString(),
                    MUE_FCH_DEVOLUCION = r["mue_fch_devolucion"].AsFormattedDate(),
                    MUE_FUNCIONARIO_DEVOLVIO = r["mue_funcionario_devolvio"].ToString(),
                    MUE_FUNC_RECIBE = r["mue_func_recibe"].ToString(),
                    MUE_USU_USUARIO = r["mue_usu_usuario"].ToString(),
                });
            }

            return Ok(data);
        }

        [Route("api/TX_REGISTRO_MUESTRAS_TB/Consecutivo")]
        [ResponseType(typeof(RM_Consecutivo))]
        public IHttpActionResult Consecutivo([FromBody] RM_Consecutivo_input input)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_MUE_GUIA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("consecutivo_p", OracleDbType.Int32).Value = input.consecutivo;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            RM_Consecutivo consecutivoData = null;
            DataTable datos2 = new DataTable();
            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                consecutivoData = new RM_Consecutivo()
                {
                    MUE_MANIFIESTO = r["ien_manifiesto"].ToString(),
                    MUE_GUIA = r["ien_guia_original"].ToString(),
                    MUE_CONSIGNATARIO = r["ien_consignatario"].ToString(),
                    TOTBULTOS = r["bultos"].ToString(),
                    TOTKILOS = r["kilos"].ToString(),
                };
                break;
            }

            return Ok(consecutivoData);
        }

        [Route("api/TX_REGISTRO_MUESTRAS_TB/DetalleRegistroMuestras")]
        public IHttpActionResult GetDetalleRegistroMuestras(string boleta)
        {
        //    [FromBody]
        //RM_Detalle_input input
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_DET_MUESTRA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dmu_mue_boleta_p", OracleDbType.Varchar2).Value = boleta;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            List<RM_Detalle> data = new List<RM_Detalle>();
            DataTable datos2 = new DataTable();
            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                data.Add(new RM_Detalle()
                {
                    BOLETA = r["dmu_mue_boleta"].ToString(),
                    ITEM = r["dmu_item"].ToString(),
                    CANTIDAD = r["dmu_cantidad"].ToString(),
                    DESCRIPCION = r["dmu_descripcion"].ToString(),
                    KILOS = r["dmu_kilos"].ToString(),
                    CANTIDAD_DEV = r["dmu_cantidad_dev"].ToString(),
                    KILOS_DEV = r["dmu_kilos_dev"].ToString(),
                });
            }

            return Ok(data);
        }

        [Route("api/TX_REGISTRO_MUESTRAS_TB/Guardar")]
        public IHttpActionResult Guardar([FromBody] RM_Muestras_input input)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_muestras_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mue_boleta_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_BOLETA;
            objCmd.Parameters.Add("mue_guia_p", OracleDbType.Varchar2, ParameterDirection.Input).Value =  input.MUE_GUIA;
            objCmd.Parameters.Add("mue_manifiesto_p", OracleDbType.Varchar2, ParameterDirection.Input).Value =  input.MUE_MANIFIESTO;
            objCmd.Parameters.Add("mue_tipo_muestra_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_TIPO_MUESTRA;
            objCmd.Parameters.Add("mue_fch_muestra_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_FCH_MUESTRA;
            objCmd.Parameters.Add("mue_codigo_motivo_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.MUE_CODIGO_MOTIVO;
            objCmd.Parameters.Add("mue_ien_entrada_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.MUE_IEN_ENTRADA;
            objCmd.Parameters.Add("mue_respons_almacen_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_RESPONS_ALMACEN;
            objCmd.Parameters.Add("mue_aforador_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_AFORADOR;
            objCmd.Parameters.Add("mue_jefe_aforador_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_JEFE_AFORADOR;
            objCmd.Parameters.Add("mue_consignatario_p", OracleDbType.Varchar2, ParameterDirection.Input).Value =  input.MUE_CONSIGNATARIO;
            objCmd.Parameters.Add("mue_motivo_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_MOTIVO;
            objCmd.Parameters.Add("mue_dua_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_DUA;
            objCmd.Parameters.Add("mue_loc_lado_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.MUE_LOC_LADO;
            objCmd.Parameters.Add("mue_loc_altura_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.MUE_LOC_ALTURA;
            objCmd.Parameters.Add("mue_fch_devolucion_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_FCH_DEVOLUCION;
            objCmd.Parameters.Add("mue_loc_rack_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.MUE_LOC_RACK;
            objCmd.Parameters.Add("mue_loc_columna_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.MUE_LOC_COLUMNA;
            objCmd.Parameters.Add("mue_aga_nagencia_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.MUE_AGA_NAGENCIA;
            objCmd.Parameters.Add("mue_func_recibe_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_FUNC_RECIBE;
            objCmd.Parameters.Add("mue_funcionario_devolvio_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_FUNCIONARIO_DEVOLVIO;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2, ParameterDirection.Input).Value =  input.UserMERX;


            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
            RM_Muestras_input _MuestrasIns = new RM_Muestras_input();
            return Ok(_MuestrasIns);
            //return Ok("");
        }

        [Route("api/TX_REGISTRO_MUESTRAS_TB/Actualizar")]
        public IHttpActionResult Actualizar([FromBody] RM_Muestras_input input)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_upd_muestras_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mue_boleta_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_BOLETA;
            objCmd.Parameters.Add("mue_tipo_muestra_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_TIPO_MUESTRA;
            objCmd.Parameters.Add("mue_fch_muestra_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_FCH_MUESTRA;
            objCmd.Parameters.Add("mue_codigo_motivo_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_CODIGO_MOTIVO;
            objCmd.Parameters.Add("mue_respons_almacen_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_RESPONS_ALMACEN;
            objCmd.Parameters.Add("mue_aforador_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_AFORADOR;
            objCmd.Parameters.Add("mue_jefe_aforador_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_JEFE_AFORADOR;
            objCmd.Parameters.Add("mue_dua_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_DUA;
            objCmd.Parameters.Add("mue_fch_devolucion_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_FCH_DEVOLUCION;
            objCmd.Parameters.Add("mue_loc_rack_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_LOC_RACK;
            objCmd.Parameters.Add("mue_loc_columna_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_LOC_COLUMNA;
            objCmd.Parameters.Add("mue_loc_altura_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_LOC_ALTURA;
            objCmd.Parameters.Add("mue_loc_lado_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_LOC_LADO;
            objCmd.Parameters.Add("mue_aga_nagencia_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_AGA_NAGENCIA;
            objCmd.Parameters.Add("mue_func_recibe_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_FUNC_RECIBE;
            objCmd.Parameters.Add("mue_funcionario_devolvio_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_FUNCIONARIO_DEVOLVIO;
            objCmd.Parameters.Add("mue_ien_entrada_p", OracleDbType.Int64, ParameterDirection.Input).Value = input.MUE_IEN_ENTRADA;
            objCmd.Parameters.Add("mue_motivo_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.MUE_MOTIVO;

            try
            {
                objCmd.ExecuteNonQuery();
                return Ok("");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
        }

        [Route("api/TX_REGISTRO_MUESTRAS_TB/InsertarDetalleMuestra")]
        public IHttpActionResult InsertarDetalleMuestra([FromBody] RM_DetalleMuestras_input input)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_DET_MUESTRA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;

            objCmd.Parameters.Add("dmu_mue_boleta_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.boleta;
            //objCmd.Parameters.Add("DMU_ITEM_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.DMU_ITEM;
            objCmd.Parameters.Add("DMU_CANTIDAD_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.DMU_CANTIDAD;
            objCmd.Parameters.Add("DMU_DESCRIPCION_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.DMU_DESCRIPCION;
            objCmd.Parameters.Add("DMU_KILOS_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.DMU_KILOS;
            objCmd.Parameters.Add("DMU_CANTIDAD_DEV_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.DMU_CANTIDAD_DEV;
            objCmd.Parameters.Add("DMU_KILOS_DEV_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.DMU_KILOS_DEV;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
            return Ok("");
        }

        [Route("api/TX_REGISTRO_MUESTRAS_TB/ActualizarDetalleMuestra")]
        public IHttpActionResult ActualizarDetalleMuestra([FromBody] RM_DetalleMuestras_input input)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_upd_det_muestra_pr";
            objCmd.CommandType = CommandType.StoredProcedure;

            objCmd.Parameters.Add("dmu_mue_boleta_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.boleta;
            objCmd.Parameters.Add("DMU_ITEM_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.DMU_ITEM;
            objCmd.Parameters.Add("DMU_CANTIDAD_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.DMU_CANTIDAD;
            objCmd.Parameters.Add("DMU_KILOS_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.DMU_KILOS;
            objCmd.Parameters.Add("DMU_DESCRIPCION_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.DMU_DESCRIPCION;        
            objCmd.Parameters.Add("DMU_CANTIDAD_DEV_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.DMU_CANTIDAD_DEV;
            objCmd.Parameters.Add("DMU_KILOS_DEV_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.DMU_KILOS_DEV;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
            return Ok("");
        }

    }
}