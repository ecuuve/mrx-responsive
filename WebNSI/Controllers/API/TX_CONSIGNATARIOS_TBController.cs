﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_CONSIGNATARIOS_TBController : ApiController
    {

        /// <summary>
        /// Obtiene los consignatarios según los filtros dados
        /// </summary>
        /// <param name="movi"></param>
        /// <param name="guia"></param>
        /// <param name="vuelo"></param>
        /// <param name="consolid"></param>
        /// <returns></returns>
        [Route("api/TX_CONSIGNATARIOS_TB/GetConsignatarios")]
        [ResponseType(typeof(REGCONSIGNATARIO))]
        public IHttpActionResult GetConsignatarios(long? movi, string guia, string vuelo, long? consolid)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_REG_CONSIG_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("movimiento_p", OracleDbType.Int32).Value = movi;
            objCmd.Parameters.Add("guia_p", OracleDbType.Varchar2).Value = guia;
            objCmd.Parameters.Add("vuelo_p", OracleDbType.Varchar2).Value = vuelo;
            objCmd.Parameters.Add("consolid_p", OracleDbType.Int32).Value = consolid;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<REGCONSIGNATARIO> _Consig = new List<REGCONSIGNATARIO>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

                foreach (DataRow e in datos2.Rows)
                {
                    _Consig.Add(new REGCONSIGNATARIO
                    {
                        IEN_ENTRADA = e["IEN_ENTRADA"].ToString(),
                        IEN_GUIA_ORIGINAL = e["IEN_GUIA_ORIGINAL"].ToString(),
                        IEN_IMP_CEDULA = e["IEN_IMP_CEDULA"].ToString(),
                        IEN_IMPORTADOR = e["IEN_IMPORTADOR"].ToString()                                             
                    });
                }          
            return Ok(_Consig);
        }

        /// <summary>
        /// Obtiene la lista de importadores según los filtros dados
        /// </summary>
        /// <param name="guia"></param>
        /// <param name="vuelo"></param>
        /// <param name="consolid"></param>
        /// <returns></returns>
        [Route("api/TX_CONSIGNATARIOS_TB/GetImportadores")]
        [ResponseType(typeof(Importador))]
        public IHttpActionResult GetImportadores(string id, string nombre)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONSIG_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("importador_p", OracleDbType.Varchar2).Value = id;
            objCmd.Parameters.Add("nombre_p", OracleDbType.Varchar2).Value = nombre;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Importador> _Importadores = new List<Importador>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Importadores.Add(new Importador
                {
                    Id = e["IMP_CEDULA"].ToString(),
                    Nombre = e["IMP_NOMBRE"].ToString()
                });
            }
            return Ok(_Importadores);
        }

        /// <summary>
        /// Actualiza el consignatario, con la cédula del importador
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cedula"></param>
        /// <returns></returns>
        [Route("api/TX_CONSIGNATARIOS_TB/PutConsignatario")]
        public IHttpActionResult PutConsignatario(long id, string cedula)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;

            objCmd.CommandText = "mrx.tx_upd_reg_consigna_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ien_entrada_p", OracleDbType.Int32).Value = id;
            objCmd.Parameters.Add("cedula_p", OracleDbType.Varchar2).Value = cedula;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();
           
            return Ok();

        }

    }
}
