﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using WebNSI.Models;



namespace WebNSI.Controllers.API
{
    public class TX_ENTRADAS_TBController : ApiController
    {
        [Route("api/TX_ENTRADAS_TB/Entrada")]
        [ResponseType(typeof(Entrada))]
        public IHttpActionResult GetEntrada(long? id, string guia, long? consolidador, long? aerolinea )
        {

            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_guia_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("entrada_p", OracleDbType.Long).Value = id;
            objCmd.Parameters.Add("guia_p", OracleDbType.Varchar2).Value = guia;
            objCmd.Parameters.Add("consolidador_p", OracleDbType.Long).Value = consolidador;
            objCmd.Parameters.Add("aerolinea_p", OracleDbType.Long).Value = aerolinea;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                return Ok(ex.Message);
                //HttpResponseMessage responseMessage = ex.Message;
                //return new ResponseMessageResult(responseMessage);
                //throw new NotImplementedException();

            }
           
            
            DataTable datos2 = new DataTable();
            Entrada _Entrada = new Entrada();
            List<Entrada> _Entradas = new List<Entrada>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();
            if (datos2.Rows.Count > 1 )
            {
                foreach (DataRow r in datos2.Rows)
                {
                    _Entradas.Add(new Entrada
                    {
                        IEN_Entrada = long.Parse(r["IEN_ENTRADA"].ToString()),
                        IEN_Manifiesto = r["IEN_MANIFIESTO"].ToString(),
                        IEN_Guia = r["IEN_GUIA"].ToString(),
                        IEN_GUIA_Original = r["IEN_GUIA_ORIGINAL"].ToString(),
                        IEN_CON_NConsolidador = int.Parse(r["IEN_CON_NCONSOLIDADOR"].ToString()),
                        CON_Nombre = r["CON_NOMBRE"].ToString(),
                        IEN_GUIA_Master = r["IEN_GUIA_MASTER"].ToString(),
                        IEN_IMP_Cedula = r["IEN_IMP_CEDULA"].ToString(),
                        IEN_Importador = r["IEN_IMPORTADOR"].ToString(),
                        IEN_Estado = r["IEN_ESTADO"].ToString(),
                        IEN_FCH_Digitacion = DateTime.Parse(r["IEN_FCH_DIGITACION"].ToString()).ToString("dd/MM/yyyy"),
                        IEN_FCH_Manifiesto = DateTime.Parse(r["IEN_FCH_MANIFIESTO"].ToString()).ToString("dd/MM/yyyy"),
                        IEN_FCH_Ingreso = DateTime.Parse(r["IEN_FCH_INGRESO"].ToString()).ToString("dd/MM/yyyy"),
                        IEN_BOD_Nbodega = int.Parse(r["IEN_BOD_NBODEGA"].ToString()),
                        BOD_Nombre = r["BOD_NOMBRE"].ToString(),
                        IEN_AER_Naerolinea = int.Parse(r["IEN_AER_NAEROLINEA"].ToString()),
                        AER_Nombre = r["AER_NOMBRE"].ToString(),
                        IEN_TDO_DOC_Hija = r["IEN_TDO_DOC_HIJA"].ToString(),
                        TDO_Hija = r["TDO_HIJA"].ToString(),
                        IEN_ADUANA_Recibe = byte.Parse(r["IEN_ADUANA_RECIBE"].ToString()),
                        ADU_Recibe = r["ADU_RECIBE"].ToString(),
                        IEN_FUN_Funcionario = r["IEN_FUN_FUNCIONARIO"].ToString(),
                        FUN_Nombre = r["FUN_NOMBRE"].ToString(),
                        IEN_PTO_Puerto = int.Parse(r["IEN_PTO_PUERTO"].ToString()),
                        PTO_Descripcion = r["PTO_DESCRIPCION"].ToString(),
                        IEN_DIN_TIPO_Documento = r["IEN_DIN_TIPO_DOCUMENTO"].ToString(),
                        DIN_Descripcion = r["DIN_DESCRIPCION"].ToString(),
                        IEN_USUARIO_Digito = int.Parse(r["IEN_USUARIO_DIGITO"].ToString()),
                        IEN_BULTOS_Manifestados = decimal.Parse(r["IEN_BULTOS_Manifestados"].ToString()),
                        IEN_BULTOS_Ingresados = decimal.Parse(r["IEN_BULTOS_Ingresados"].ToString()),
                        IEN_BULTOS_Despaletizados = decimal.Parse(r["BLT_DESP"].ToString()),
                        IEN_BULTOS_Reservados = decimal.Parse(r["BLT_RESER"].ToString()),
                        IEN_BULTOS_Salidos = decimal.Parse(r["BLT_SALID"].ToString()),
                        IEN_KILOS_Manifestados = decimal.Parse(r["IEN_BULTOS_Manifestados"].ToString()),
                        IEN_KILOS_Despaletizados = decimal.Parse(r["KG_DESP"].ToString()),
                        IEN_KILOS_Reservados = decimal.Parse(r["KG_RESER"].ToString()),
                        IEN_KILOS_Salidos = decimal.Parse(r["KG_SALID"].ToString()),
                        IEN_DESCRIPCION_Manif = r["IEN_DESCRIPCION_MANIF"].ToString(),                 
                        IEN_Antecesor =!string.IsNullOrEmpty(r["IEN_ANTECESOR"].ToString()) ? long.Parse(r["IEN_ANTECESOR"].ToString()) : 0,                     
                        IEN_NUMERO_Viaje = long.Parse(r["IEN_NUMERO_VIAJE"].ToString()),
                        IEN_Observaciones = r["IEN_OBSERVACIONES"].ToString(),
                        IEN_Clase = r["IEN_CLASE"].ToString(),
                        IEN_BOLETA_Clase = r["IEN_BOLETA_CLASE"].ToString(),
                        IEN_UBICACION_Transito = r["IEN_UBICACION_TRANSITO"].ToString(),
                        IEN_DUDA_Refri = r["IEN_DUDA_REFRI"].ToString(),
                        IEN_Bloqueada = r["IEN_BLOQUEADA"].ToString(),
                        IEN_ES_Courier = r["IEN_ES_COURIER"].ToString(),
                        UBI_Destino = r["UBI_DESTINO"].ToString(),
                        Muestras = r["MUESTRAS"].ToString()

                });
                }

                return Ok(_Entradas);

            }
            else
            {
                foreach (DataRow r in datos2.Rows)
                {
                    _Entrada.IEN_Entrada = long.Parse(r["IEN_ENTRADA"].ToString());
                    _Entrada.IEN_Manifiesto = r["IEN_MANIFIESTO"].ToString();
                    _Entrada.IEN_Guia = r["IEN_GUIA"].ToString();
                    _Entrada.IEN_GUIA_Original = r["IEN_GUIA_ORIGINAL"].ToString();
                    _Entrada.IEN_CON_NConsolidador = int.Parse(r["IEN_CON_NCONSOLIDADOR"].ToString());
                    _Entrada.CON_Nombre = r["CON_NOMBRE"].ToString();
                    _Entrada.IEN_GUIA_Master = r["IEN_GUIA_MASTER"].ToString();
                    _Entrada.IEN_IMP_Cedula = r["IEN_IMP_CEDULA"].ToString();
                    _Entrada.IEN_Importador = r["IEN_IMPORTADOR"].ToString();
                    _Entrada.IEN_Estado = r["IEN_ESTADO"].ToString();
                    _Entrada.IEN_FCH_Digitacion = DateTime.Parse(r["IEN_FCH_DIGITACION"].ToString()).ToString("dd/MM/yyyy");
                    _Entrada.IEN_FCH_Manifiesto = DateTime.Parse(r["IEN_FCH_MANIFIESTO"].ToString()).ToString("dd/MM/yyyy");
                    _Entrada.IEN_FCH_Ingreso = DateTime.Parse(r["IEN_FCH_INGRESO"].ToString()).ToString("dd/MM/yyyy");
                    _Entrada.IEN_BOD_Nbodega = int.Parse(r["IEN_BOD_NBODEGA"].ToString());
                    _Entrada.BOD_Nombre = r["BOD_NOMBRE"].ToString();
                    _Entrada.IEN_AER_Naerolinea = int.Parse(r["IEN_AER_NAEROLINEA"].ToString());
                    _Entrada.AER_Nombre = r["AER_NOMBRE"].ToString();
                    _Entrada.IEN_TDO_DOC_Hija = r["IEN_TDO_DOC_HIJA"].ToString();
                    _Entrada.TDO_Hija = r["TDO_HIJA"].ToString();
                    _Entrada.IEN_ADUANA_Recibe = byte.Parse(r["IEN_ADUANA_RECIBE"].ToString());
                    _Entrada.ADU_Recibe = r["ADU_RECIBE"].ToString();
                    _Entrada.IEN_FUN_Funcionario = r["IEN_FUN_FUNCIONARIO"].ToString();
                    _Entrada.FUN_Nombre = r["FUN_NOMBRE"].ToString();
                    _Entrada.IEN_PTO_Puerto = int.Parse(r["IEN_PTO_PUERTO"].ToString());
                    _Entrada.PTO_Descripcion = r["PTO_DESCRIPCION"].ToString();
                    _Entrada.IEN_DIN_TIPO_Documento = r["IEN_DIN_TIPO_DOCUMENTO"].ToString();
                    _Entrada.DIN_Descripcion = r["DIN_DESCRIPCION"].ToString();
                    _Entrada.IEN_USUARIO_Digito = int.Parse(r["IEN_USUARIO_DIGITO"].ToString());
                    _Entrada.IEN_BULTOS_Manifestados = decimal.Parse(r["IEN_BULTOS_Manifestados"].ToString());
                    _Entrada.IEN_BULTOS_Ingresados = decimal.Parse(r["IEN_BULTOS_Ingresados"].ToString());
                    _Entrada.IEN_BULTOS_Despaletizados = decimal.Parse(r["BLT_DESP"].ToString());
                    _Entrada.IEN_BULTOS_Reservados = decimal.Parse(r["BLT_RESER"].ToString());
                    _Entrada.IEN_BULTOS_Salidos = decimal.Parse(r["BLT_SALID"].ToString());
                    _Entrada.IEN_KILOS_Manifestados = decimal.Parse(r["IEN_BULTOS_Manifestados"].ToString());
                    _Entrada.IEN_KILOS_Despaletizados = decimal.Parse(r["KG_DESP"].ToString());
                    _Entrada.IEN_KILOS_Reservados = decimal.Parse(r["KG_RESER"].ToString());
                    _Entrada.IEN_KILOS_Salidos = decimal.Parse(r["KG_SALID"].ToString());
                    _Entrada.IEN_DESCRIPCION_Manif = r["IEN_DESCRIPCION_MANIF"].ToString();
                    if (r["IEN_ANTECESOR"].ToString() != "")
                    {
                        _Entrada.IEN_Antecesor = long.Parse(r["IEN_ANTECESOR"].ToString());
                    }
                    _Entrada.IEN_NUMERO_Viaje = long.Parse(r["IEN_NUMERO_VIAJE"].ToString());
                    _Entrada.IEN_Observaciones = r["IEN_OBSERVACIONES"].ToString();
                    _Entrada.IEN_Clase = r["IEN_CLASE"].ToString();
                    _Entrada.IEN_BOLETA_Clase = r["IEN_BOLETA_CLASE"].ToString();
                    _Entrada.IEN_UBICACION_Transito = r["IEN_UBICACION_TRANSITO"].ToString();
                    _Entrada.IEN_DUDA_Refri = r["IEN_DUDA_REFRI"].ToString();
                    _Entrada.IEN_Bloqueada = r["IEN_BLOQUEADA"].ToString();
                    _Entrada.IEN_ES_Courier = r["IEN_ES_COURIER"].ToString();
                    _Entrada.UBI_Destino = r["UBI_DESTINO"].ToString();
                    _Entrada.Muestras = r["MUESTRAS"].ToString();
                }

                return Ok(_Entrada);

            }
            
        }

        #region PANTALLA CONOCIMIENTOS

        /// <summary>
        /// Pregunta si existe ya la guia digitada
        /// </summary>
        /// <param name="guia"></param>
        /// <param name="manifiesto"></param>
        /// <returns></returns>
        [Route("api/TX_ENTRADAS_TB/GuiaExiste")]
        public IHttpActionResult GetGuiaExiste(string guia, string manifiesto)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONSEC_GUIA_MAN_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("guia_p", OracleDbType.Varchar2).Value = guia;
            objCmd.Parameters.Add("manifiesto_p", OracleDbType.Varchar2).Value = manifiesto;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion

    }
}