﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_REGISTRO_RECLAMOS_TBController : ApiController
    {
        [Route("api/TX_REGISTRO_RECLAMOS_TB/Sistema")]
        [ResponseType(typeof(RRCL_Sistema))]
        public IHttpActionResult GetTipo()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_sc_rec_sistema_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<RRCL_Sistema> data = new List<RRCL_Sistema>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                data.Add(new RRCL_Sistema
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(data);
        }

        [Route("api/TX_REGISTRO_RECLAMOS_TB/Motivo")]
        [ResponseType(typeof(RRCL_Motivo))]
        public IHttpActionResult GetMotivo()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_sc_rec_motivo_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<RRCL_Motivo> data = new List<RRCL_Motivo>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                data.Add(new RRCL_Motivo
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(data);
        }

        [Route("api/TX_REGISTRO_RECLAMOS_TB/Moneda")]
        [ResponseType(typeof(RRCL_Moneda))]
        public IHttpActionResult GetMoneda()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_sc_rec_moneda_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<RRCL_Moneda> data = new List<RRCL_Moneda>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                data.Add(new RRCL_Moneda
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(data);
        }

        [Route("api/TX_REGISTRO_RECLAMOS_TB/Resolucion")]
        [ResponseType(typeof(RRCL_Resolucion))]
        public IHttpActionResult GetResolucion()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_sc_rec_resolucion_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<RRCL_Resolucion> data = new List<RRCL_Resolucion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                data.Add(new RRCL_Resolucion
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(data);
        }

        [Route("api/TX_REGISTRO_RECLAMOS_TB/ResolucionOps")]
        [ResponseType(typeof(RRCL_ResolucionOps))]
        public IHttpActionResult GetResolucionOps()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_sc_rec_resol_ops_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<RRCL_ResolucionOps> data = new List<RRCL_ResolucionOps>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                data.Add(new RRCL_ResolucionOps
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(data);
        }

        [Route("api/TX_REGISTRO_RECLAMOS_TB/Via")]
        [ResponseType(typeof(RRCL_Via))]
        public IHttpActionResult GetVia()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_sc_rec_via_notif_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<RRCL_Via> data = new List<RRCL_Via>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                data.Add(new RRCL_Via
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(data);
        }

        [Route("api/TX_REGISTRO_RECLAMOS_TB/BuscarRegistroReclamos")]
        [ResponseType(typeof(RRCL_Registro_Reclamos))]
        public IHttpActionResult BuscarRegistroReclamos([FromBody] RRCL_Registro_Reclamos_Input input)
        {
            if (input.s_rec_fecha_presentacion == "day/month/year")
            {
                input.s_rec_fecha_presentacion = null;
            }
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_reclamos_sc_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("rec_reclamo_p", OracleDbType.Int32).Value = input.s_rec_reclamo;
            objCmd.Parameters.Add("rec_sistema_p", OracleDbType.Varchar2).Value = input.s_rec_sistema;
            objCmd.Parameters.Add("conse_p", OracleDbType.Int32).Value = input.s_consecutivo;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = "";
            objCmd.Parameters.Add("rec_fch_registra_p", OracleDbType.Varchar2).Value = input.s_rec_fecha_presentacion;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            DataTable datos2 = new DataTable();
            List<RRCL_Registro_Reclamos> data = new List<RRCL_Registro_Reclamos>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                data.Add(new RRCL_Registro_Reclamos
                {
                    REC_RECLAMO = (long)r["REC_RECLAMO"],
                    REC_SISTEMA = r["REC_SISTEMA"].ToString(),
                    DESC_SISTEMA = r["DESC_SISTEMA"].ToString(),               
                    consecutivo = r["consecutivo"].ToString(),
                    MANIFIESTO = r["MANIFIESTO"].ToString(),
                    GUIA_ORIGINAL = r["GUIA_ORIGINAL"].ToString(),
                    CONSIGNATARIO = r["CONSIGNATARIO"].ToString(),
                    AEROLINEA = r["AEROLINEA"].ToString(),
                    CONSOLIDADOR = r["CONSOLIDADOR"].ToString(),
                    REC_MOTIVO = r["REC_MOTIVO"].ToString(),
                    DESC_MOTIVO = r["DESC_MOTIVO"].ToString(),
                    REC_SOLICITADO = r["REC_SOLICITADO"].ToString(),
                    REC_CONTACTO = r["REC_CONTACTO"].ToString(),
                    REC_MONTO = (decimal)r["REC_MONTO"],
                    REC_TIPO_MONEDA = r["REC_TIPO_MONEDA"].ToString(),
                    DESC_MONEDA = r["DESC_MONEDA"].ToString(),
                    REC_DESCRIP_RECLAMO = r["REC_DESCRIP_RECLAMO"].ToString(),
                    REC_OBSERVACIONES = r["REC_OBSERVACIONES"].ToString(),
                    REC_PREVIO_EXAMEN = r["REC_PREVIO_EXAMEN"].ToString(),
                    REC_USU_REGISTRA = r["REC_USU_REGISTRA"].ToString(),
                    REC_FCH_REGISTRA = r["REC_FCH_REGISTRA"].AsFormattedDate(),
                    REC_RESOLUCION = r["REC_RESOLUCION"].ToString(),
                    DESC_RESOL = r["DESC_RESOL"].ToString(),
                    REC_FCH_RECIBE_OPS = r["REC_FCH_RECIBE_OPS"].AsFormattedDate(),
                    REC_INVESTIGA_OPS = r["REC_INVESTIGA_OPS"].ToString(),
                    REC_CAUSA_OPS = r["REC_CAUSA_OPS"].ToString(),
                    REC_OBSERV_OPS = r["REC_OBSERV_OPS"].ToString(),
                    REC_JUSTIFICA_ATRASO_OPS = r["REC_JUSTIFICA_ATRASO_OPS"].ToString(),
                    REC_USU_JUSTIF_OPS = r["REC_USU_JUSTIF_OPS"].ToString(),
                    REC_FCH_JUSTIF_OPS = r["REC_FCH_JUSTIF_OPS"].AsFormattedDate(),
                    REC_USU_INVESTIGA = r["REC_USU_INVESTIGA"].ToString(),
                    REC_FCH_RESOLUC_OPS = r["REC_FCH_RESOLUC_OPS"].AsFormattedDate(),
                    REC_USU_RESOLUCION_OPS = r["REC_USU_RESOLUCION_OPS"].ToString(),
                    REC_RESOLUCION_OPS = r["REC_RESOLUCION_OPS"].ToString(),
                    TIEMPO_INVESTIGA = r["TIEMPO_INVESTIGA"].ToString(),
                    REC_REGISTRO_PNC = r["REC_REGISTRO_PNC"].ToString(),
                    DIAS_TRANSCURRIDOS = r["DIAS_TRANSCURRIDOS"].ToString(),
                    REC_JUSTIFICA_ATRASO = r["REC_JUSTIFICA_ATRASO"].ToString(),
                    REC_PERSONA_NOTIF = r["REC_PERSONA_NOTIF"].ToString(),
                    REC_FCH_NOTIF = r["REC_FCH_NOTIF"].AsFormattedDate(),
                    REC_VIA_NOTIF = r["REC_VIA_NOTIF"].ToString(),
                    DESC_VIA_NOTIF = r["DESC_VIA_NOTIF"].ToString(),
                    REC_FCH_ENTREGA_CHEQUE = r["REC_FCH_ENTREGA_CHEQUE"].AsFormattedDate(),
                    REC_JUSTIF_RESOLUCION = r["REC_JUSTIF_RESOLUCION"].ToString(),
                    REC_APLICA_POLIZA = r["REC_APLICA_POLIZA"].ToString(),
                    REC_DECISION_INS = r["REC_DECISION_INS"].ToString(),
                    REC_FCH_ENTREGA_CLIENTE = r["REC_FCH_ENTREGA_CLIENTE"].AsFormattedDate(),
                    REC_ENTRADA_EXP = r["REC_ENTRADA_EXP"].ToString(),
                    REC_OGU_GUIA = r["REC_OGU_GUIA"].ToString(),
                    REC_FECHA_PRESENTACION = r["REC_FECHA_PRESENTACION"].AsFormattedDate(),
                    REC_FECHA_RESOLUCION = r["REC_FECHA_RESOLUCION"].AsFormattedDate(),
                    REC_FCH_PASO_OPER = r["REC_FCH_PASO_OPER"].AsFormattedDate(),
                    REC_FCH_APR_DEN = r["REC_FCH_APR_DEN"].AsFormattedDate(),
                    REC_TIPO = r["REC_TIPO"].ToString(),
                    REC_PRE_RECLAMO = r["REC_PRE_RECLAMO"].ToString(),
                    REC_FCH_PRERECLAMO = r["REC_FCH_PRERECLAMO"].AsFormattedDate(),
                    REC_USU_PRERECLAMO = r["REC_USU_PRERECLAMO"].ToString(),
                    REC_FCH_AMPLIACION = r["REC_FCH_AMPLIACION"].AsFormattedDate(),
                    REC_JUSTIF_AMPLIACION = r["REC_JUSTIF_AMPLIACION"].ToString(),
                    AGENCIA = r["AGENCIA"].ToString(),
                    DUA = r["DUA"].ToString(),
                    FACTURA = r["FACTURA"].ToString(),
                    REGISTRO_PNC = r["PNC_IDENTIFICADOR"].ToString(),
                    SIV = r["SIV_SOLICITUD"].ToString(),
                    PREVIO_EXAMEN = r["EXAMEN_PREVIO"].ToString(),
                    DUA_INGRESO = r["DUA_INGRESO"].ToString(),
                    CLIENTE_NOMBRE = r["CLIENTE_NOMBRE"].ToString()

                });
            }

            return Ok(data);
        }

        [Route("api/TX_REGISTRO_RECLAMOS_TB/ExisteConsecutivo")]
        public IHttpActionResult ExisteConsecutivo([FromBody] RRCL_Consecutivo_input input)
        {
            string result = "";
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_EXISTE_RECLAMO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("consecutivo_p", OracleDbType.Int32).Value = input.consecutivo;
            objCmd.Parameters.Add("sistema_p", OracleDbType.Varchar2).Value = input.rec_sistema;
            objCmd.Parameters.Add("existe_p", OracleDbType.Varchar2, 255).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch(Exception e)
            {
                return Ok(e.Message);
            }

            result = objCmd.Parameters["existe_p"].Value.ToString();

            return Ok(result);
        }

        [Route("api/TX_REGISTRO_RECLAMOS_TB/Consecutivo")]
        [ResponseType(typeof(RRCL_Consecutivo))]
        public IHttpActionResult Consecutivo([FromBody] RRCL_Consecutivo_input input)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_rec_consecutivo_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ien_entrada_p", OracleDbType.Int32).Value = input.consecutivo;
            objCmd.Parameters.Add("rec_sistema_p", OracleDbType.Varchar2).Value = input.rec_sistema;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            RRCL_Consecutivo consecutivoData = null;
            DataTable datos2 = new DataTable();
            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                consecutivoData = new RRCL_Consecutivo()
                {
                    GUIA_ORIGINAL = r["guia"].ToString(),
                    MANIFIESTO = r["manifiesto"].ToString(),
                    CONSIGNATARIO = r["consignatario"].ToString(),
                    CONSOLIDADOR = r["consolidador"].ToString(),
                    AEROLINEA = r["aerolinea"].ToString(),
                    AGENCIA = r["agencia"].ToString(),
                    DUA = r["dua"].ToString(),
                    FACTURA = r["factura"].ToString(),
                    REGISTRO_PNC = r["registro_pnc"].ToString(),
                    SIV = r["siv"].ToString(),
                    PREVIO_EXAMEN = r["previo_examen"].ToString(),
                    USUARIO_REGISTRA = r["usuario_registra"].ToString(),
                    FECHA_REGISTRA = r["fecha_registra"].ToString(),
                };
                break;
            }

            return Ok(consecutivoData);
        }

        [Route("api/TX_REGISTRO_RECLAMOS_TB/Guardar")]
        public IHttpActionResult Guardar([FromBody] RRCL_Reclamo_input input)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_RECLAMOS_SC_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("rec_fecha_presentacion_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FECHA_PRESENTACION;
            objCmd.Parameters.Add("rec_sistema_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_SISTEMA;
            objCmd.Parameters.Add("rec_motivo_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_MOTIVO;
            objCmd.Parameters.Add("rec_solicitado_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_SOLICITADO;
            objCmd.Parameters.Add("rec_contacto_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_CONTACTO;
            objCmd.Parameters.Add("rec_monto_p", OracleDbType.Decimal, ParameterDirection.Input).Value = input.REC_MONTO;
            objCmd.Parameters.Add("rec_tipo_moneda_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_TIPO_MONEDA;
            objCmd.Parameters.Add("rec_descrip_reclamo_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_DESCRIP_RECLAMO;
            objCmd.Parameters.Add("rec_observaciones_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_OBSERVACIONES;
            objCmd.Parameters.Add("rec_previo_examen_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = "";// input.REC_PREVIO_EXAMEN;
            objCmd.Parameters.Add("rec_resolucion_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_RESOLUCION;
            objCmd.Parameters.Add("rec_fch_recibe_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FCH_RECIBE_OPS;
            objCmd.Parameters.Add("rec_investiga_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_INVESTIGA_OPS;
            objCmd.Parameters.Add("rec_causa_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_CAUSA_OPS;
            objCmd.Parameters.Add("rec_observ_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_OBSERV_OPS;
            objCmd.Parameters.Add("rec_justifica_atraso_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_JUSTIFICA_ATRASO_OPS;
            objCmd.Parameters.Add("rec_usu_justif_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_USU_JUSTIF_OPS;
            objCmd.Parameters.Add("rec_fch_justif_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FCH_JUSTIF_OPS;
            objCmd.Parameters.Add("rec_usu_investiga_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_USU_INVESTIGA;
            objCmd.Parameters.Add("rec_fch_resoluc_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FCH_RESOLUC_OPS;
            objCmd.Parameters.Add("rec_usu_resolucion_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_USU_RESOLUCION;
            objCmd.Parameters.Add("rec_resolucion_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_RESOLUCION_OPS;
            objCmd.Parameters.Add("rec_justifica_atraso_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_JUSTIFICA_ATRASO;
            objCmd.Parameters.Add("rec_persona_notif_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_PERSONA_NOTIF;
            objCmd.Parameters.Add("rec_fch_notif_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FCH_NOTIF;
            objCmd.Parameters.Add("rec_via_notif_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_VIA_NOTIF;
            objCmd.Parameters.Add("rec_fch_entrega_cheque_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FCH_ENTREGA_CHEQUE;
            objCmd.Parameters.Add("rec_justif_resolucion_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_JUSTIF_RESOLUCION;
            objCmd.Parameters.Add("rec_aplica_poliza_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_APLICA_POLIZA;
            objCmd.Parameters.Add("rec_decision_ins_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_DECISION_INS;
            objCmd.Parameters.Add("rec_fch_entrega_cliente_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FCH_ENTREGA_CLIENTE;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.UserMERX;
            objCmd.Parameters.Add("rec_ien_entrada_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.consecutivo;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
            RRCL_Reclamo_input _ReclamoUIns = new RRCL_Reclamo_input();
            return Ok(_ReclamoUIns);
        }

        [Route("api/TX_REGISTRO_RECLAMOS_TB/Actualizar")]
        public IHttpActionResult Actualizar([FromBody] RRCL_Reclamo_update input)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_RECLAMOS_SC_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("rec_reclamo_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.REC_RECLAMO;
            objCmd.Parameters.Add("rec_fecha_presentacion_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FECHA_PRESENTACION;
            objCmd.Parameters.Add("rec_sistema_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_SISTEMA;
            objCmd.Parameters.Add("rec_motivo_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_MOTIVO;
            objCmd.Parameters.Add("rec_solicitado_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_SOLICITADO;
            objCmd.Parameters.Add("rec_contacto_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_CONTACTO;
            objCmd.Parameters.Add("rec_monto_p", OracleDbType.Decimal, ParameterDirection.Input).Value = input.REC_MONTO;
            objCmd.Parameters.Add("rec_tipo_moneda_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_TIPO_MONEDA;
            objCmd.Parameters.Add("rec_descrip_reclamo_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_DESCRIP_RECLAMO;
            objCmd.Parameters.Add("rec_observaciones_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_OBSERVACIONES;
            //objCmd.Parameters.Add("rec_previo_examen_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = "";// input.REC_PREVIO_EXAMEN;
            objCmd.Parameters.Add("rec_resolucion_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_RESOLUCION;
            objCmd.Parameters.Add("rec_fch_recibe_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FCH_RECIBE_OPS;
            objCmd.Parameters.Add("rec_investiga_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_INVESTIGA_OPS;
            objCmd.Parameters.Add("rec_causa_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_CAUSA_OPS;
            objCmd.Parameters.Add("rec_observ_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_OBSERV_OPS;
            objCmd.Parameters.Add("rec_justifica_atraso_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_JUSTIFICA_ATRASO_OPS;
            objCmd.Parameters.Add("rec_usu_justif_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_USU_JUSTIF_OPS;
            objCmd.Parameters.Add("rec_fch_justif_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FCH_JUSTIF_OPS;
            objCmd.Parameters.Add("rec_usu_investiga_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_USU_INVESTIGA;
           // objCmd.Parameters.Add("rec_fch_resoluc_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FCH_RESOLUC_OPS;
            //objCmd.Parameters.Add("rec_usu_resolucion_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_USU_RESOLUCION;
            objCmd.Parameters.Add("rec_resolucion_ops_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_RESOLUCION_OPS;
            objCmd.Parameters.Add("rec_justifica_atraso_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_JUSTIFICA_ATRASO;
            objCmd.Parameters.Add("rec_persona_notif_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_PERSONA_NOTIF;
            objCmd.Parameters.Add("rec_fch_notif_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FCH_NOTIF;
            objCmd.Parameters.Add("rec_via_notif_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_VIA_NOTIF;
            objCmd.Parameters.Add("rec_fch_entrega_cheque_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FCH_ENTREGA_CHEQUE;
            objCmd.Parameters.Add("rec_justif_resolucion_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_JUSTIF_RESOLUCION;
            objCmd.Parameters.Add("rec_aplica_poliza_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_APLICA_POLIZA != null ? "S" : "N";//input.REC_APLICA_POLIZA == "S"?"S":"N";
            objCmd.Parameters.Add("rec_decision_ins_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_DECISION_INS != null ? "A" : "D";//input.REC_DECISION_INS == "A"?"A":"D";
            objCmd.Parameters.Add("rec_fch_entrega_cliente_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.REC_FCH_ENTREGA_CLIENTE;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2, ParameterDirection.Input).Value = input.UserMERX;
            objCmd.Parameters.Add("rec_ien_entrada_p", OracleDbType.Int32, ParameterDirection.Input).Value = input.consecutivo;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
            RRCL_Reclamo_update _ReclamoUp = new RRCL_Reclamo_update();
            return Ok(_ReclamoUp);
        }
    }
}