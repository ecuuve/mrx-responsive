﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_MERCANCIAS_TB_TarimasController : ApiController
    {
        #region Carga de LV

        /// <summary>
        /// Obtiene tipo de mercancia
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB_Tarimas/TipoMercancia")]
        [ResponseType(typeof(TipoMercancia))]
        public IHttpActionResult GetTipoMercancia()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_tipo_merc_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tipo_mer_p", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<TipoMercancia> _TiposMercancia = new List<TipoMercancia>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TiposMercancia.Add(new TipoMercancia
                {
                    Id = r["codigo"].ToString(),
                    Nombre = r["descripcion"].ToString()
                });
            }

            return Ok(_TiposMercancia);
        }

        /// <summary>
        /// Obtiene tipo de peligrosidad
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB_Tarimas/TipoPeligrosidadIATA")]
        [ResponseType(typeof(TipoPeligrosidadIATA))]
        public IHttpActionResult GetTipoPeligrosidadIATA()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_iata_pelig_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tipo_codigo_iata_p", OracleDbType.Varchar2).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<TipoPeligrosidadIATA> _TiposPeligrosidadIATA = new List<TipoPeligrosidadIATA>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TiposPeligrosidadIATA.Add(new TipoPeligrosidadIATA
                {
                    Id = r["codigo"].ToString(),
                    Nombre = r["descripcion"].ToString()
                });
            }

            return Ok(_TiposPeligrosidadIATA);
        }

        /// <summary>
        /// Obtiene condiciones de mercancia
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB_Tarimas/Condicion")]
        [ResponseType(typeof(Condicion))]
        public IHttpActionResult GetCondicion()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_tipos_averia_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tipo_averia_p", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Condicion> _CondicionesMercancia = new List<Condicion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _CondicionesMercancia.Add(new Condicion
                {
                    Id = r["codigo"].ToString(),
                    Nombre = r["descripcion"].ToString()
                });
            }

            return Ok(_CondicionesMercancia);
        }


        /// <summary>
        /// Obtiene embalajes
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB_Tarimas/Embalajes")]
        [ResponseType(typeof(Embalaje))]
        public IHttpActionResult GetEmbalajes()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_embalaje_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("embalaje_p", OracleDbType.Varchar2).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Embalaje> _Embalajes = new List<Embalaje>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Embalajes.Add(new Embalaje
                {
                    Id = r["codigo"].ToString(),
                    Nombre = r["descripcion"].ToString()
                });
            }

            return Ok(_Embalajes);
        }


        /// <summary>
        /// Obtiene Estados
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB_Tarimas/Estados")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetEstados()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_estados_mer_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Estados> _Estados = new List<Estados>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Estados.Add(new Estados
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Estados);
        }


        /// <summary>
        /// Obtiene Operacion Tica
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB_Tarimas/OperacionTica")]
        [ResponseType(typeof(OperacionTica))]
        public IHttpActionResult GetOperacionTica()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_oper_tica_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<OperacionTica> _OperacionTica = new List<OperacionTica>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _OperacionTica.Add(new OperacionTica
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_OperacionTica);
        }

        #endregion

        #region "Ingreso de Mercancias"

        /// <summary>
        /// Obtiene mercancías
        /// </summary>
        /// <param name="entrada"></param>
        /// <param name="mercancia"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB_Tarimas/GetMercanciasEntrada")]
        [ResponseType(typeof(Tarima_Ing))]
        public IHttpActionResult GetMercanciasEntrada(long? entrada, long? mercancia)
        {
            try
            {


                //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.tx_qry_ingreso_merca_pr";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("mer_ien_entrada_p", OracleDbType.Int32).Value = entrada;
                objCmd.Parameters.Add("mer_id_mercancia_p" +
                    "", OracleDbType.Int32).Value = mercancia;
                objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Tarima_Ing> _Mercancias = new List<Tarima_Ing>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {

                    _Mercancias.Add(new Tarima_Ing
                    {
                        MER_IEN_ENTRADA = long.Parse(e["MER_IEN_ENTRADA"].ToString()),
                        MER_MANIFIESTO = e["MER_MANIFIESTO"].ToString(),
                        MER_GUIA = e["MER_GUIA"].ToString(),
                        KILOS_INGRESADOS = decimal.Parse(e["KILOS_INGRESADOS"].ToString()),
                        BULTOS_INGRESADOS = decimal.Parse(e["BULTOS_INGRESADOS"].ToString()),
                        MER_TME_MERCANCIA = short.Parse(e["MER_TME_MERCANCIA"].ToString()),
                        DES_MERCANCIA = e["DES_MERCANCIA"].ToString(),
                        MER_MARCAS = e["MER_MARCAS"].ToString(),
                        MER_TIP_CODIGO_IATA = e["MER_TIP_CODIGO_IATA"].ToString(),
                        DESC_PELIG = e["DESC_PELIG"].ToString(),
                        MER_INDICE_TRANS = e["MER_INDICE_TRANS"].ToString(),
                        MER_CODIGO_UN = e["MER_CODIGO_UN"].ToString(),// !string.IsNullOrEmpty(e["MER_CODIGO_UN"].ToString()) ? int.Parse(e["MER_CODIGO_UN"].ToString()) : 0,
                        MER_REFRIGERADO = e["MER_REFRIGERADO"].ToString(),
                        MER_ARMAS = e["MER_ARMAS"].ToString(),
                        MER_AVE_AVERIA = short.Parse(e["MER_AVE_AVERIA"].ToString()),
                        DESAVERIAS = e["DESAVERIAS"].ToString(),
                        MER_TEM_EMBALAJE = e["MER_TEM_EMBALAJE"].ToString(),
                        DESEMBALAJE = e["DESEMBALAJE"].ToString(),
                        MER_DESCRIPCION = e["MER_DESCRIPCION"].ToString(),
                        MER_OBSERVACIONES = e["MER_OBSERVACIONES"].ToString(),
                        MER_BULTOS_DESPALETIZADOS = decimal.Parse(e["MER_BULTOS_DESPALETIZADOS"].ToString()),
                        MER_KILOS_BRUTO = decimal.Parse(e["MER_KILOS_BRUTO"].ToString()),
                        MER_KILOS_TARIMA = decimal.Parse(e["MER_KILOS_TARIMA"].ToString()),
                        MER_KILOS_NETO = decimal.Parse(e["MER_KILOS_NETO"].ToString()),
                        MER_ESTADO = e["MER_ESTADO"].ToString(),
                        MER_OPERACION_TICA = e["MER_OPERACION_TICA"].ToString(),
                        MER_ID_MERCANCIA = long.Parse(e["MER_ID_MERCANCIA"].ToString()),
                        MER_ID_MERCANCIA_REF = e["MER_ID_MERCANCIA_REF"].ToString(),//!string.IsNullOrEmpty(e["MER_ID_MERCANCIA_REF"].ToString()) ? long.Parse(e["MER_ID_MERCANCIA_REF"].ToString()) : 0,
                        MER_BULTOS_REF = e["MER_BULTOS_REF"].ToString(),//!string.IsNullOrEmpty(e["MER_BULTOS_REF"].ToString()) ? decimal.Parse(e["MER_BULTOS_REF"].ToString()) : 0,
                        MER_LOC_RACK = decimal.Parse(e["MER_LOC_RACK"].ToString()),
                        MER_LOC_COLUMNA = decimal.Parse(e["MER_LOC_COLUMNA"].ToString()),
                        MER_LOC_LADO = decimal.Parse(e["MER_LOC_LADO"].ToString()),
                        MER_LOC_ALTURA = decimal.Parse(e["MER_LOC_ALTURA"].ToString()),
                        MER_PERECEDERO = e["MER_PERECEDERO"].ToString(),
                        MER_LINEA_GUIA = long.Parse(e["MER_LINEA_GUIA"].ToString()),
                        MER_PUESTO_CHEQUEO = long.Parse(e["MER_PUESTO_CHEQUEO"].ToString()),
                       
                    });
                }

                return Ok(_Mercancias);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>
        /// Ingreso de tarima
        /// </summary>
        /// <param name="tarima_p"></param>
        /// <returns></returns>
        [ResponseType(typeof(Tarima_Ing))]
        public IHttpActionResult PostTX_MERCANCIAS_TB_Ingreso(Tarima_Ing tarima_p)
        {
            if(tarima_p.MER_PERECEDERO == null)
            {
                tarima_p.MER_PERECEDERO = "N";
            }
            if (tarima_p.MER_REFRIGERADO == null)
            {
                tarima_p.MER_REFRIGERADO = "N";
            }
            if (tarima_p.MER_ARMAS == null)
            {
                tarima_p.MER_ARMAS = "N";
            }
            if (tarima_p.MER_CODIGO_UN == "")
            {
                tarima_p.MER_CODIGO_UN = null;
            }
            if (tarima_p.MER_INDICE_TRANS == "")
            {
                tarima_p.MER_INDICE_TRANS = null;
            }
            if (tarima_p.MER_ID_MERCANCIA_REF == "")
            {
                tarima_p.MER_ID_MERCANCIA_REF = null;
            }
            if (tarima_p.MER_BULTOS_REF == "")
            {
                tarima_p.MER_BULTOS_REF = null;
            }
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_ingreso_merca_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Int32).Value = tarima_p.MER_ID_MERCANCIA;
            objCmd.Parameters.Add("mer_puesto_chequeo_p", OracleDbType.Int32).Value = tarima_p.MER_PUESTO_CHEQUEO;
            objCmd.Parameters.Add("mer_manifiesto_p", OracleDbType.Varchar2).Value = tarima_p.MER_MANIFIESTO;
            objCmd.Parameters.Add("mer_guia_p", OracleDbType.Varchar2).Value = tarima_p.MER_GUIA;
            objCmd.Parameters.Add("mer_ien_entrada_p", OracleDbType.Int32).Value = tarima_p.MER_IEN_ENTRADA;
            objCmd.Parameters.Add("mer_tme_mercancia_p", OracleDbType.Int32).Value = tarima_p.MER_TME_MERCANCIA;
            objCmd.Parameters.Add("mer_marcas_p", OracleDbType.Varchar2).Value = tarima_p.MER_MARCAS;
            objCmd.Parameters.Add("mer_tip_codigo_iata_p", OracleDbType.Varchar2).Value = tarima_p.MER_TIP_CODIGO_IATA;
            objCmd.Parameters.Add("mer_indice_trans_p", OracleDbType.Int32).Value = tarima_p.MER_INDICE_TRANS;
            objCmd.Parameters.Add("mer_codigo_un_p", OracleDbType.Int32).Value = tarima_p.MER_CODIGO_UN;
            objCmd.Parameters.Add("mer_refrigerado_p", OracleDbType.Varchar2).Value = tarima_p.MER_REFRIGERADO;
            objCmd.Parameters.Add("mer_armas_p", OracleDbType.Varchar2).Value = tarima_p.MER_ARMAS;
            objCmd.Parameters.Add("mer_ave_averia_p", OracleDbType.Int32).Value = tarima_p.MER_AVE_AVERIA;
            objCmd.Parameters.Add("mer_tem_embalaje_p", OracleDbType.Varchar2).Value = tarima_p.MER_TEM_EMBALAJE;
            objCmd.Parameters.Add("mer_descripcion_p", OracleDbType.Varchar2).Value = tarima_p.MER_DESCRIPCION;
            objCmd.Parameters.Add("mer_observaciones_p", OracleDbType.Varchar2).Value = tarima_p.MER_OBSERVACIONES;
            objCmd.Parameters.Add("mer_bultos_despaletizados_p", OracleDbType.Int32).Value = tarima_p.MER_BULTOS_DESPALETIZADOS;
            objCmd.Parameters.Add("mer_kilos_bruto_p", OracleDbType.Int32).Value = tarima_p.MER_KILOS_BRUTO;
            objCmd.Parameters.Add("mer_kilos_tarima_p", OracleDbType.Int32).Value = tarima_p.MER_KILOS_TARIMA;
            objCmd.Parameters.Add("mer_estado_p", OracleDbType.Varchar2).Value = tarima_p.MER_ESTADO;
            objCmd.Parameters.Add("mer_operacion_tica_p", OracleDbType.Varchar2).Value = tarima_p.MER_OPERACION_TICA;
            objCmd.Parameters.Add("mer_id_mercancia_ref_p", OracleDbType.Int32).Value = tarima_p.MER_ID_MERCANCIA_REF;
            objCmd.Parameters.Add("mer_bultos_ref_p", OracleDbType.Int32).Value = tarima_p.MER_BULTOS_REF;
            objCmd.Parameters.Add("mer_loc_rack_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_RACK;
            objCmd.Parameters.Add("mer_loc_columna_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_COLUMNA;
            objCmd.Parameters.Add("mer_loc_lado_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_LADO;
            objCmd.Parameters.Add("mer_loc_altura_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_ALTURA;
            objCmd.Parameters.Add("mer_perecedero_p", OracleDbType.Varchar2).Value = tarima_p.MER_PERECEDERO;
            objCmd.Parameters.Add("mer_linea_guia_p", OracleDbType.Int32).Value = tarima_p.MER_LINEA_GUIA;
            objCmd.Parameters.Add("mer_user_merx_p", OracleDbType.Varchar2).Value = tarima_p.UserMERX;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Mercancia _TarimaIn = new Mercancia();

            return Ok(_TarimaIn);
        }

        /// <summary>
        /// Actualizar Tarima
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tarima_p"></param>
        /// <returns></returns>
        [ResponseType(typeof(Tarima_Ing))]
        public IHttpActionResult PutTX_MERCANCIAS_TB_Ingreso(string id, Tarima_Ing tarima_p)
        {
            if (tarima_p.MER_PERECEDERO == null)
            {
                tarima_p.MER_PERECEDERO = "N";
            }
            if (tarima_p.MER_REFRIGERADO == null)
            {
                tarima_p.MER_REFRIGERADO = "N";
            }
            if (tarima_p.MER_ARMAS == null)
            {
                tarima_p.MER_ARMAS = "N";
            }
            if (tarima_p.MER_CODIGO_UN == "")
            {
                tarima_p.MER_CODIGO_UN = null;
            }
            if (tarima_p.MER_INDICE_TRANS == "")
            {
                tarima_p.MER_INDICE_TRANS = null;
            }
            if (tarima_p.MER_ID_MERCANCIA_REF == "")
            {
                tarima_p.MER_ID_MERCANCIA_REF = null;
            }
            if (tarima_p.MER_BULTOS_REF == "")
            {
                tarima_p.MER_BULTOS_REF = null;
            }
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_upd_ingreso_merca_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Int32).Value = tarima_p.MER_ID_MERCANCIA;
            objCmd.Parameters.Add("mer_tme_mercancia_p", OracleDbType.Int32).Value = tarima_p.MER_TME_MERCANCIA;
            objCmd.Parameters.Add("mer_marcas_p", OracleDbType.Varchar2).Value = tarima_p.MER_MARCAS;
            objCmd.Parameters.Add("mer_tip_codigo_iata_p", OracleDbType.Varchar2).Value = tarima_p.MER_TIP_CODIGO_IATA;
            objCmd.Parameters.Add("mer_indice_trans_p", OracleDbType.Int32).Value = tarima_p.MER_INDICE_TRANS;
            objCmd.Parameters.Add("mer_codigo_un_p", OracleDbType.Int32).Value = tarima_p.MER_CODIGO_UN;
            objCmd.Parameters.Add("mer_refrigerado_p", OracleDbType.Varchar2).Value = tarima_p.MER_REFRIGERADO;
            objCmd.Parameters.Add("mer_armas_p", OracleDbType.Varchar2).Value = tarima_p.MER_ARMAS;
            objCmd.Parameters.Add("mer_ave_averia_p", OracleDbType.Int32).Value = tarima_p.MER_AVE_AVERIA;
            objCmd.Parameters.Add("mer_tem_embalaje_p", OracleDbType.Varchar2).Value = tarima_p.MER_TEM_EMBALAJE;
            objCmd.Parameters.Add("mer_descripcion_p", OracleDbType.Varchar2).Value = tarima_p.MER_DESCRIPCION;
            objCmd.Parameters.Add("mer_observaciones_p", OracleDbType.Varchar2).Value = tarima_p.MER_OBSERVACIONES;
            objCmd.Parameters.Add("mer_bultos_despaletizados_p", OracleDbType.Int32).Value = tarima_p.MER_BULTOS_DESPALETIZADOS;
            objCmd.Parameters.Add("mer_kilos_bruto_p", OracleDbType.Int32).Value = tarima_p.MER_KILOS_BRUTO;
            objCmd.Parameters.Add("mer_kilos_tarima_p", OracleDbType.Int32).Value = tarima_p.MER_KILOS_TARIMA;
            objCmd.Parameters.Add("mer_estado_p", OracleDbType.Varchar2).Value = tarima_p.MER_ESTADO;
            objCmd.Parameters.Add("mer_operacion_tica_p", OracleDbType.Varchar2).Value = tarima_p.MER_OPERACION_TICA;
            objCmd.Parameters.Add("mer_id_mercancia_ref_p", OracleDbType.Int32).Value = tarima_p.MER_ID_MERCANCIA_REF;
            objCmd.Parameters.Add("mer_bultos_ref_p", OracleDbType.Int32).Value = tarima_p.MER_BULTOS_REF;
            objCmd.Parameters.Add("mer_loc_rack_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_RACK;
            objCmd.Parameters.Add("mer_loc_columna_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_COLUMNA;
            objCmd.Parameters.Add("mer_loc_lado_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_LADO;
            objCmd.Parameters.Add("mer_loc_altura_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_ALTURA;
            objCmd.Parameters.Add("mer_perecedero_p", OracleDbType.Varchar2).Value = tarima_p.MER_PERECEDERO;
            objCmd.Parameters.Add("mer_linea_guia_p", OracleDbType.Int32).Value = tarima_p.MER_LINEA_GUIA;
            objCmd.Parameters.Add("mer_user_merx_p", OracleDbType.Varchar2).Value = tarima_p.UserMERX;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Mercancia _TarimaUp = new Mercancia();

            return Ok(_TarimaUp);
        }

        #endregion
    }
}