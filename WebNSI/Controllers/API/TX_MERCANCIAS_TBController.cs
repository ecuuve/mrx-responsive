﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_MERCANCIAS_TBController : ApiController
    {

        public class mercancias
        {
            public string idM { set; get; }
            public string user { set; get; }
        }

        #region NO SE SI SE USAN 

        //No sé si se usan - Fsuarez 02/10/2019
        [Route("api/TX_MERCANCIAS_TB/Mercancia")]
        [ResponseType(typeof(Mercancia))]
        public IHttpActionResult GetMercancia(long id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_det_tarima_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tarima_p", OracleDbType.Long).Value = id;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            Mercancia _Mercancia = new Mercancia();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Mercancia.MER_ID_MERCANCIA = e["MER_ID_MERCANCIA"].ToString();
                _Mercancia.IEN_ENTRADA = e["MER_IEN_ENTRADA"].ToString();
                _Mercancia.MER_Manifiesto = e["MER_MANIFIESTO"].ToString();
                _Mercancia.MER_Guia = e["MER_GUIA"].ToString();
                _Mercancia.MER_DESCRIPCION = e["MER_DESCRIPCION"].ToString();
                _Mercancia.MER_BULTOS_DESPALETIZADOS = e["MER_BULTOS_DESPALETIZADOS"].ToString();
                _Mercancia.MER_BULTOS_Reservados = e["MER_BULTOS_RESERVADOS"].ToString();
                _Mercancia.MER_BULTOS_Salidos = e["MER_BULTOS_SALIDOS"].ToString();
                _Mercancia.Existencias = e["EXISTENCIAS"].ToString();
                _Mercancia.MER_BULTOS_DESPALETIZADOS = e["MER_BULTOS_DESPALETIZADOS"].ToString();
                _Mercancia.MER_KILOS_TARIMA = e["MER_KILOS_TARIMA"].ToString();
                _Mercancia.MER_KILOS_Reservados = e["MER_KILOS_RESERVADOS"].ToString();
                _Mercancia.MER_KILOS_Salidos = e["MER_KILOS_SALIDOS"].ToString();
                _Mercancia.Kbruto = e["KBRUTO"].ToString();
                _Mercancia.MER_PEL_Peligrosa = e["MER_PEL_PELIGROSA"].ToString();
                _Mercancia.PEL_Descripcion = e["PEL_DESCRIPCION"].ToString();
                _Mercancia.MER_ESTADO = e["MER_ESTADO"].ToString();
                _Mercancia.DESC_Estado = e["DESC_ESTADO"].ToString();
                _Mercancia.MER_FCH_Digitacion = DateTime.Parse(e["MER_FCH_DIGITACION"].ToString()).ToString("dd/MM/yyyy");
                _Mercancia.MER_REFRIGERADO = e["MER_REFRIGERADO"].ToString();
                _Mercancia.MER_TEM_EMBALAJE = e["MER_TEM_EMBALAJE"].ToString();
                _Mercancia.TEM_Descripcion = e["TEM_DESCRIPCION"].ToString();
                _Mercancia.MER_AVE_AVERIA = e["MER_AVE_AVERIA"].ToString();
                _Mercancia.MER_TME_MERCANCIA = e["MER_TME_MERCANCIA"].ToString();
                _Mercancia.TME_Descripcion = e["TME_DESCRIPCION"].ToString();
                _Mercancia.MER_LOC_RACK = e["MER_LOC_RACK"].ToString();
                _Mercancia.MER_LOC_COLUMNA = e["MER_LOC_COLUMNA"].ToString();
                _Mercancia.MER_LOC_ALTURA = e["MER_LOC_ALTURA"].ToString();
                _Mercancia.MER_LOC_LADO = e["MER_LOC_LADO"].ToString();
                _Mercancia.MER_Transmision = e["MER_TRANSMISION"].ToString();
                _Mercancia.DESC_Transmision = e["DESC_TRANSMISION"].ToString();
                _Mercancia.MER_ARCHIVO_Transmision = e["MER_ARCHIVO_TRANSMISION"].ToString();
                _Mercancia.MER_FCH_Transmision = e["MER_FCH_TRANSMISION"].ToString();
                _Mercancia.MER_USUARIO_Transmision = e["MER_USUARIO_TRANSMISION"].ToString();
                _Mercancia.MER_ID_FUNCIONARIO_Aduana = e["MER_ID_FUNCIONARIO_Aduana"].ToString();
                _Mercancia.MER_Observaciones = e["MER_OBSERVACIONES"].ToString();
                _Mercancia.MER_Marcas = e["MER_MARCAS"].ToString();
                _Mercancia.MER_FCH_Localizacion = e["MER_FCH_LOCALIZACION"].ToString();

                // MER_MOT_ULTIMA_Modificacion = short.Parse(e["MER_MOT_ULTIMA_MODIFICACION"].ToString()),
                //MER_MOT_Borrado = short.Parse(e["MER_MOT_BORRADO"].ToString()),
                _Mercancia.MER_MOT_Borrado = e["MER_MOT_BORRADO"].ToString();
                _Mercancia.MER_MOT_ULTIMA_Modificacion = e["MER_MOT_ULTIMA_MODIFICACION"].ToString();

                _Mercancia.MER_A_Aforo = e["MER_A_AFORO"].ToString();
                _Mercancia.MER_Facturar = e["MER_FACTURAR"].ToString();
                _Mercancia.MER_USUARIO_Localiza = e["MER_USUARIO_LOCALIZA"].ToString();

                _Mercancia.MER_BOLETA_Remate = e["MER_BOLETA_REMATE"].ToString();
                _Mercancia.MER_IMP_Cedula = e["MER_IMP_CEDULA"].ToString();
                _Mercancia.IMP_Nombre = e["IMP_NOMBRE"].ToString();
                _Mercancia.MER_FCH_Endoso = e["MER_FCH_ENDOSO"].ToString();
                _Mercancia.MER_Selector = e["MER_SELECTOR"].ToString();

                _Mercancia.MER_BULTOS_Pre = e["MER_BULTOS_PRE"].ToString();
                _Mercancia.MER_KILOS_Pre = e["MER_KILOS_PRE"].ToString();

                _Mercancia.MER_Viaje = e["MER_VIAJE"].ToString();


                _Mercancia.MER_Accion = e["MER_ACCION"].ToString();
                _Mercancia.DESC_Accion = e["DESC_ACCION"].ToString();
                _Mercancia.MER_OPERACION_TICA = e["MER_OPERACION_TICA"].ToString();

                _Mercancia.MER_ID_MERCANCIA_REF = e["MER_ID_MERCANCIA_REF"].ToString();

                _Mercancia.MER_BULTOS_REF = e["MER_BULTOS_REF"].ToString();


                _Mercancia.MER_Error = e["MER_ERROR"].ToString();
                _Mercancia.MER_EN_Tica = e["MER_EN_TICA"].ToString();
                _Mercancia.MER_TER_Error = e["MER_TER_ERROR"].ToString();

                _Mercancia.MER_MOV_Inventario = e["MER_MOV_INVENTARIO"].ToString();

                _Mercancia.MER_AUTORIZ_Reemp = e["MER_AUTORIZ_REEMP"].ToString();


                _Mercancia.MER_Partida = e["MER_PARTIDA"].ToString();
                _Mercancia.MER_IDENTIF_Agente = e["MER_IDENTIF_AGENTE"].ToString();
                _Mercancia.MER_SOLICITA_Cambio = e["MER_SOLICITA_CAMBIO"].ToString();
                _Mercancia.MER_RESPONSABLE_Error = e["MER_RESPONSABLE_ERROR"].ToString();
                _Mercancia.MER_FCH_INV_Aduana = e["MER_FCH_INV_ADUANA"].ToString();
                _Mercancia.MER_Departamento = e["MER_DEPARTAMENTO"].ToString();
                _Mercancia.MER_Perecedero = e["MER_PERECEDERO"].ToString();

                _Mercancia.MER_PUESTO_CHEQUEO = e["MER_PUESTO_CHEQUEO"].ToString();

                _Mercancia.MER_PELIG_Iata = e["MER_PELIG_IATA"].ToString();


                _Mercancia.MER_FCH_Finalizado = e["MER_FCH_FINALIZADO"].ToString();

                _Mercancia.MER_CODIGO_un = e["MER_CODIGO_UN"].ToString();

                _Mercancia.MER_INDICE_TRANS = e["MER_INDICE_TRANS"].ToString();


                _Mercancia.MER_TIP_CODIGO_IATA = e["MER_TIP_CODIGO_IATA"].ToString();
                _Mercancia.MER_ARMAS = e["MER_ARMAS"].ToString();
                _Mercancia.AVE_Descripcion = e["AVE_DESCRIPCION"].ToString();
                _Mercancia.MOT_Descripcion = e["MOT_DESCRIPCION"].ToString();
                _Mercancia.TIP_CODIGO_Iata = e["TIP_CODIGO_IATA"].ToString();
                _Mercancia.TIP_Descripcion = e["TIP_DESCRIPCION"].ToString();
                _Mercancia.DESC_OPER_Tica = e["DESC_OPER_TICA"].ToString();
                _Mercancia.TER_Descripcion = e["TER_DESCRIPCION"].ToString();

                _Mercancia.Bultosesc = e["BULTOSESC"].ToString();

                _Mercancia.Kilosesc = e["KILOSESC"].ToString();
            }

            return Ok(_Mercancia);
        }
        [ResponseType(typeof(Mercancia))]
        public IHttpActionResult PostTX_MERCANCIAS_TB_Ingreso(Mercancia tarima_p)
        {
            //if (cajera_p.CJR_Principal == null)
            //{
            //    cajera_p.CJR_Principal = "N";
            //}
            //if (cajera_p.CJR_Activo == null)
            //{
            //    cajera_p.CJR_Activo = "N";
            //}
            //if (cajera_p.CJR_Doble_impr == null)
            //{
            //    cajera_p.CJR_Doble_impr = "N";
            //}
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_ingreso_guia_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_puesto_chequeo_p", OracleDbType.Long).Value = tarima_p.MER_PUESTO_CHEQUEO;
            objCmd.Parameters.Add("mer_manifiesto_p", OracleDbType.Varchar2).Value = tarima_p.MER_Manifiesto;
            objCmd.Parameters.Add("mer_guia_p", OracleDbType.Varchar2).Value = tarima_p.MER_Guia;
            objCmd.Parameters.Add("mer_ien_entrada_p", OracleDbType.Int32).Value = tarima_p.IEN_ENTRADA;
            objCmd.Parameters.Add("mer_tme_mercancia_p", OracleDbType.Int32).Value = tarima_p.MER_TME_MERCANCIA;
            objCmd.Parameters.Add("mer_marcas_p", OracleDbType.Varchar2).Value = tarima_p.MER_MARCAS;
            objCmd.Parameters.Add("mer_tip_codigo_iata_p", OracleDbType.Varchar2).Value = tarima_p.MER_TIP_CODIGO_IATA;
            objCmd.Parameters.Add("mer_indice_trans_p", OracleDbType.Int32).Value = tarima_p.MER_INDICE_TRANS;
            objCmd.Parameters.Add("mer_codigo_un_p", OracleDbType.Int32).Value = tarima_p.MER_CODIGO_UN;
            objCmd.Parameters.Add("mer_refrigerado_p", OracleDbType.Varchar2).Value = tarima_p.MER_REFRIGERADO;
            objCmd.Parameters.Add("mer_armas_p", OracleDbType.Varchar2).Value = tarima_p.MER_ARMAS;
            objCmd.Parameters.Add("mer_ave_averia_p", OracleDbType.Int32).Value = tarima_p.MER_AVE_AVERIA;
            objCmd.Parameters.Add("mer_tem_embalaje_p", OracleDbType.Varchar2).Value = tarima_p.MER_TEM_EMBALAJE;
            objCmd.Parameters.Add("mer_descripcion_p", OracleDbType.Varchar2).Value = tarima_p.MER_DESCRIPCION;
            objCmd.Parameters.Add("mer_observaciones_p", OracleDbType.Varchar2).Value = tarima_p.MER_Observaciones;
            objCmd.Parameters.Add("mer_bultos_despaletizados_p", OracleDbType.Int32).Value = tarima_p.MER_BULTOS_DESPALETIZADOS;
            objCmd.Parameters.Add("mer_kilos_bruto_p", OracleDbType.Int32).Value = tarima_p.MER_KILOS_BRUTO;
            objCmd.Parameters.Add("mer_kilos_tarima_p", OracleDbType.Int32).Value = tarima_p.MER_KILOS_TARIMA;
            objCmd.Parameters.Add("mer_estado_p", OracleDbType.Varchar2).Value = tarima_p.MER_ESTADO;
            objCmd.Parameters.Add("mer_operacion_tica_p", OracleDbType.Varchar2).Value = tarima_p.MER_OPERACION_TICA;
            objCmd.Parameters.Add("mer_id_mercancia_ref_p", OracleDbType.Int32).Value = tarima_p.MER_ID_MERCANCIA_REF;
            objCmd.Parameters.Add("mer_bultos_ref_p", OracleDbType.Int32).Value = tarima_p.MER_BULTOS_REF;
            objCmd.Parameters.Add("mer_loc_rack_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_RACK;
            objCmd.Parameters.Add("mer_loc_columna_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_COLUMNA;
            objCmd.Parameters.Add("mer_loc_lado_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_LADO;
            objCmd.Parameters.Add("mer_loc_altura_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_ALTURA;
            objCmd.Parameters.Add("mer_perecedero_p", OracleDbType.Varchar2).Value = tarima_p.MER_PERECEDERO;
            objCmd.Parameters.Add("mer_linea_guia_p", OracleDbType.Int32).Value = tarima_p.MER_LINEA_GUIA;
            objCmd.Parameters.Add("mer_user_merx_p", OracleDbType.Varchar2).Value = "MRX";
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Mercancia _TarimaIn = new Mercancia();

            return Ok(_TarimaIn);
        }
        [ResponseType(typeof(Mercancia))]
        public IHttpActionResult PutTX_MERCANCIAS_TB_Ingreso(string id, Mercancia tarima_p)
        {
            //if (cajera_p.CJR_Principal == null)
            //{
            //    cajera_p.CJR_Principal = "N";
            //}
            //if (cajera_p.CJR_Activo == null)
            //{
            //    cajera_p.CJR_Activo = "N";
            //}
            //if (cajera_p.CJR_Doble_impr == null)
            //{
            //    cajera_p.CJR_Doble_impr = "N";
            //}
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_upd_ingreso_merca_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_puesto_chequeo_p", OracleDbType.Long).Value = tarima_p.MER_PUESTO_CHEQUEO;
            objCmd.Parameters.Add("mer_manifiesto_p", OracleDbType.Varchar2).Value = tarima_p.MER_Manifiesto;
            objCmd.Parameters.Add("mer_guia_p", OracleDbType.Varchar2).Value = tarima_p.MER_Guia;
            objCmd.Parameters.Add("mer_ien_entrada_p", OracleDbType.Int32).Value = tarima_p.IEN_ENTRADA;
            objCmd.Parameters.Add("mer_tme_mercancia_p", OracleDbType.Int32).Value = tarima_p.MER_TME_MERCANCIA;
            objCmd.Parameters.Add("mer_marcas_p", OracleDbType.Varchar2).Value = tarima_p.MER_MARCAS;
            objCmd.Parameters.Add("mer_tip_codigo_iata_p", OracleDbType.Varchar2).Value = tarima_p.MER_TIP_CODIGO_IATA;
            objCmd.Parameters.Add("mer_indice_trans_p", OracleDbType.Int32).Value = tarima_p.MER_INDICE_TRANS;
            objCmd.Parameters.Add("mer_codigo_un_p", OracleDbType.Int32).Value = tarima_p.MER_CODIGO_UN;
            objCmd.Parameters.Add("mer_refrigerado_p", OracleDbType.Varchar2).Value = tarima_p.MER_REFRIGERADO;
            objCmd.Parameters.Add("mer_armas_p", OracleDbType.Varchar2).Value = tarima_p.MER_ARMAS;
            objCmd.Parameters.Add("mer_ave_averia_p", OracleDbType.Int32).Value = tarima_p.MER_AVE_AVERIA;
            objCmd.Parameters.Add("mer_tem_embalaje_p", OracleDbType.Varchar2).Value = tarima_p.MER_TEM_EMBALAJE;
            objCmd.Parameters.Add("mer_descripcion_p", OracleDbType.Varchar2).Value = tarima_p.MER_DESCRIPCION;
            objCmd.Parameters.Add("mer_observaciones_p", OracleDbType.Varchar2).Value = tarima_p.MER_Observaciones;
            objCmd.Parameters.Add("mer_bultos_despaletizados_p", OracleDbType.Int32).Value = tarima_p.MER_BULTOS_DESPALETIZADOS;
            objCmd.Parameters.Add("mer_kilos_bruto_p", OracleDbType.Int32).Value = tarima_p.MER_KILOS_BRUTO;
            objCmd.Parameters.Add("mer_kilos_tarima_p", OracleDbType.Int32).Value = tarima_p.MER_KILOS_TARIMA;
            objCmd.Parameters.Add("mer_estado_p", OracleDbType.Varchar2).Value = tarima_p.MER_ESTADO;
            objCmd.Parameters.Add("mer_operacion_tica_p", OracleDbType.Varchar2).Value = tarima_p.MER_OPERACION_TICA;
            objCmd.Parameters.Add("mer_id_mercancia_ref_p", OracleDbType.Int32).Value = tarima_p.MER_ID_MERCANCIA_REF;
            objCmd.Parameters.Add("mer_bultos_ref_p", OracleDbType.Int32).Value = tarima_p.MER_BULTOS_REF;
            objCmd.Parameters.Add("mer_loc_rack_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_RACK;
            objCmd.Parameters.Add("mer_loc_columna_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_COLUMNA;
            objCmd.Parameters.Add("mer_loc_lado_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_LADO;
            objCmd.Parameters.Add("mer_loc_altura_p", OracleDbType.Int32).Value = tarima_p.MER_LOC_ALTURA;
            objCmd.Parameters.Add("mer_perecedero_p", OracleDbType.Varchar2).Value = tarima_p.MER_PERECEDERO;
            objCmd.Parameters.Add("mer_linea_guia_p", OracleDbType.Int32).Value = tarima_p.MER_LINEA_GUIA;
            objCmd.Parameters.Add("mer_user_merx_p", OracleDbType.Varchar2).Value = "MRX";
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Mercancia _TarimaUp = new Mercancia();

            return Ok(_TarimaUp);
        }
        
        #endregion

        #region Reimpresión

        /// <summary>
        /// Lista las tarimas totales para pantalla de reimpresión
        /// </summary>
        /// <param name="entrada"></param>
        /// <param name="tarima"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/GetMercanciasReimp")]
        [ResponseType(typeof(Tarima))]
        public IHttpActionResult GetMercanciasReimp(long? entrada, long? tarima)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_tarimas_reimp_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("entrada_p", OracleDbType.Long).Value = entrada;
            objCmd.Parameters.Add("tarima_p", OracleDbType.Long).Value = tarima;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            //objCmd.ExecuteNonQuery();
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            DataTable datos2 = new DataTable();
            List<Tarima> _Tarimas = new List<Tarima>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();
            foreach (DataRow e in datos2.Rows)
            {

                _Tarimas.Add(new Tarima
                {
                    TARIMA = long.Parse(e["TARIMA"].ToString()),
                    CONSECUTIVO = long.Parse(e["CONSECUTIVO"].ToString()),
                    MANIFIESTO = e["MANIFIESTO"].ToString(),
                    GUIA = e["GUIA"].ToString(),
                    BULTOS = !string.IsNullOrEmpty(e["BULTOS"].ToString()) ? decimal.Parse(e["BULTOS"].ToString()) : 0,
                    KBRUTO = !string.IsNullOrEmpty(e["KBRUTO"].ToString()) ? decimal.Parse(e["KBRUTO"].ToString()) : 0,
                    DESC_ESTADO = e["DESC_ESTADO"].ToString()
                });
            }

            return Ok(_Tarimas);
        }
        
        #endregion

        #region "Ingreso de Mercancias"
        
        /// <summary>
        /// Valida tarima referencia y devuelve bultos
        /// </summary>
        /// <param name="mercancia"></param>
        /// <param name="operaTica"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/GetTarimaRef")]
        [ResponseType(typeof(string))]
        public IHttpActionResult GetTarimaRef(long? mercancia, string operaTica)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_merc_ref_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_id_mercancia_ref_p", OracleDbType.Int32).Value = mercancia;
            objCmd.Parameters.Add("mer_operacion_tica_p", OracleDbType.Varchar2).Value = operaTica;
            objCmd.Parameters.Add("mer_bultos_p", OracleDbType.Int32).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();

                var bultos = objCmd.Parameters["mer_bultos_p"].Value.ToString();

                if (bultos == "null")
                {
                    bultos = null;
                }

                return Ok(bultos);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
               
        }
        
        /// <summary>
        /// Lista las tarimas
        /// </summary>
        /// <param name="guia"></param>
        /// <param name="mercancia"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/GetMercanciasIngreso")]
        [ResponseType(typeof(Mercancia1))]
        public IHttpActionResult GetMercanciasIngreso(long? guia, long? mercancia, long? tipomercancia)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_ingreso_merca_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_ien_entrada_p", OracleDbType.Varchar2).Value = guia;
            objCmd.Parameters.Add("mer_tme_mercancia_p", OracleDbType.Long).Value = mercancia;
            objCmd.Parameters.Add("mer_tme_mercancia_p", OracleDbType.Long).Value = tipomercancia;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Mercancia1> _Mercancias = new List<Mercancia1>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {

                    _Mercancias.Add(new Mercancia1
                    {
                        MER_ID_MERCANCIA = e["MER_ID_MERCANCIA"].ToString(),
                        IEN_ENTRADA = e["MER_IEN_ENTRADA"].ToString(),
                        MER_MANIFIESTO = e["MER_MANIFIESTO"].ToString(),
                        MER_GUIA = e["MER_GUIA"].ToString(),
                        MER_DESCRIPCION = e["MER_DESCRIPCION"].ToString(),
                        MER_BULTOS_DESPALETIZADOS = e["MER_BULTOS_DESPALETIZADOS"].ToString(),
                        MER_KILOS_BRUTO = e["MER_KILOS_BRUTO"].ToString(),
                        MER_KILOS_TARIMA = e["MER_KILOS_TARIMA"].ToString(),
                        MER_LINEA_GUIA = e["MER_LINEA_GUIA"].ToString(),
                        MER_ESTADO = e["MER_ESTADO"].ToString(),
                        DES_ESTADO = e["DESAVERIAS"].ToString(),
                        MER_REFRIGERADO = e["MER_REFRIGERADO"].ToString(),
                        MER_TEM_EMBALAJE = e["MER_TEM_EMBALAJE"].ToString(),
                        MER_AVE_AVERIA = e["MER_AVE_AVERIA"].ToString(),
                        MER_TME_MERCANCIA = e["MER_TME_MERCANCIA"].ToString(),
                        DES_MERCANCIA = e["DES_MERCANCIA"].ToString(),
                        MER_LOC_RACK = e["MER_LOC_RACK"].ToString(),
                        MER_LOC_COLUMNA = e["MER_LOC_COLUMNA"].ToString(),
                        MER_LOC_ALTURA = e["MER_LOC_ALTURA"].ToString(),
                        MER_LOC_LADO = e["MER_LOC_LADO"].ToString(),
                        MER_OBSERVACIONES = e["MER_OBSERVACIONES"].ToString(),
                        MER_MARCAS = e["MER_MARCAS"].ToString(),
                        MER_TIP_CODIGO_IATA = e["MER_TIP_CODIGO_IATA"].ToString(),
                        MER_INDICE_TRANS = e["MER_INDICE_TRANS"].ToString(),
                        MER_CODIGO_UN = e["MER_CODIGO_UN"].ToString(),
                        MER_ARMAS = e["MER_ARMAS"].ToString(),
                        MER_OPERACION_TICA = e["MER_OPERACION_TICA"].ToString(),
                        MER_ID_MERCANCIA_REF = e["MER_ID_MERCANCIA_REF"].ToString(),
                        MER_BULTOS_REF = e["MER_BULTOS_REF"].ToString(),
                        MER_PERECEDERO = e["MER_PERECEDERO"].ToString(),
                        MER_PUESTO_CHEQUEO = e["MER_PUESTO_CHEQUEO"].ToString(),
                        MER_LARGO = e["MER_LARGO"].ToString(),
                        MER_ALTO = e["MER_ALTO"].ToString(),
                        MER_ANCHO = e["MER_ANCHO"].ToString(),
                        ESTADO = e["ESTADO"].ToString(),
                        MER_ETIQUETA = e["MER_ETIQUETA"].ToString(),
                        MER_MOV_REF = e["MER_ID_MERCANCIA_REF"].ToString()                     
                    });

                                    }
                return Ok(_Mercancias);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }     

        /// <summary>
        /// Lista las mercancías de resumen o sumatoria
        /// </summary>
        /// <param name="guia"></param>
        /// <param name="mercancia"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/MercanciasResumen")]
        [ResponseType(typeof(Tarimas_Mercancia))]
        public IHttpActionResult GetMercanciasResumen(long? guia, long? mercancia, long? tipomercancia)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_resumen_merca_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_ien_entrada_p", OracleDbType.Varchar2).Value = guia;
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Varchar2).Value = mercancia;
            objCmd.Parameters.Add("mer_tme_mercancia_p", OracleDbType.Long).Value = tipomercancia;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Tarimas_Mercancia> _Mercancias = new List<Tarimas_Mercancia>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _Mercancias.Add(new Tarimas_Mercancia
                    {
                        TARIMA = e["TARIMA"].ToString(),
                        BULTOS_TARIMA = e["BULTOS_TARIMA"].ToString(),
                        KILOS_BRUTO = e["KILOS_BRUTO"].ToString(),
                        KILOS_NETO = e["KILOS_NETO"].ToString(),
                        KILOS_TARIMA = e["KILOS_TARIMA"].ToString(),
                        MER_ESTADO = e["ESTADO"].ToString(),
                        ABIERTA = e["ESTADO"].ToString() == "Abierta" ? true: false,
                        DES_MERCANCIA = e["DES_MERCANCIA"].ToString(),
                        MER_DESCRIPCION = e["MER_DESCRIPCION"].ToString(),
                        DES_OPER_TICA = e["DES_OPER_TICA"].ToString(),
                        MER_ID_MERCANCIA_REF = e["MER_ID_MERCANCIA_REF"].ToString(),
                        DESAVERIAS = e["DESAVERIAS"].ToString()
                    });
                }
                return Ok(_Mercancias);
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Cierra Tarimas
        /// </summary>
        /// <param name="mercancias"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/CerrarTarimas")]
        [ResponseType(typeof(string))]
        public IHttpActionResult PostTX_MERCANCIAS_TB_Cerrar(List<mercancias> mercancias)
        {
            int cont = 0;
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();

            foreach (var x in mercancias)
            {
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_UPD_CERRAR_MERC_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("mer_id_mercancias_p ", OracleDbType.Int32).Value = x.idM;
                objCmd.Parameters.Add("mer_user_merx_p  ", OracleDbType.Varchar2).Value = x.user;
                try
                {
                    objCmd.ExecuteNonQuery();
                    cont++;

                }
                catch (Exception ex)
                {
                    return BadRequest("Error al cerrar tarima: " + x.idM + ". " + ex.Message);
                }

            }
            cn.Close();
            return Ok("Exito");
        }

        #endregion

        #region Modificación y Consulta

        [Route("api/TX_MERCANCIAS_TB/GetMercanciasConsulta")]
        [ResponseType(typeof(Tarimas_Consulta))]
        public IHttpActionResult GetMercanciasConsulta(long? guia, long? mercancia, long? tipomercancia)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CON_MERCA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_ien_entrada_p", OracleDbType.Varchar2).Value = guia;
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Varchar2).Value = mercancia;
            objCmd.Parameters.Add("mer_tme_mercancia_p", OracleDbType.Long).Value = tipomercancia;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Tarimas_Consulta> _list = new List<Tarimas_Consulta>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {

                    _list.Add(new Tarimas_Consulta
                    {
                        MER_IEN_ENTRADA = r["MER_IEN_ENTRADA"].ToString(),
                        MER_MANIFIESTO = r["MER_MANIFIESTO"].ToString(),
                        MER_GUIA = r["MER_GUIA"].ToString(),
                        KILOS_INGRESADOS = r["KILOS_INGRESADOS"].ToString(),
                        BULTOS_INGRESADOS = r["BULTOS_INGRESADOS"].ToString(),
                        MER_TME_MERCANCIA = r["MER_TME_MERCANCIA"].ToString(),
                        DES_MERCANCIA = r["DES_MERCANCIA"].ToString(),
                        MER_MARCAS = r["MER_MARCAS"].ToString(),
                        MER_TIP_CODIGO_IATA = r["MER_TIP_CODIGO_IATA"].ToString(),
                        DESC_PELIG = r["DESC_PELIG"].ToString(),
                        MER_INDICE_TRANS = r["MER_INDICE_TRANS"].ToString(),
                        MER_CODIGO_UN = r["MER_CODIGO_UN"].ToString(),
                        MER_REFRIGERADO = r["MER_REFRIGERADO"].ToString(),
                        MER_ARMAS = r["MER_ARMAS"].ToString(),
                        MER_AVE_AVERIA = r["MER_AVE_AVERIA"].ToString(),
                        DESAVERIAS = r["DESAVERIAS"].ToString(),
                        MER_TEM_EMBALAJE = r["MER_TEM_EMBALAJE"].ToString(),
                        DESEMBALAJE = r["DESEMBALAJE"].ToString(),
                        MER_DESCRIPCION = r["MER_DESCRIPCION"].ToString(),
                        MER_OBSERVACIONES = r["MER_OBSERVACIONES"].ToString(),
                        MER_BULTOS_DESPALETIZADOS = r["MER_BULTOS_DESPALETIZADOS"].ToString(),
                        MER_KILOS_BRUTO = r["MER_KILOS_BRUTO"].ToString(),
                        MER_KILOS_TARIMA = r["MER_KILOS_TARIMA"].ToString(),
                        MER_KILOS_NETO = r["MER_KILOS_NETO"].ToString(),
                        MER_ESTADO = r["MER_ESTADO"].ToString(),
                        MER_OPERACION_TICA = r["MER_OPERACION_TICA"].ToString(),
                        MER_ID_MERCANCIA = r["MER_ID_MERCANCIA"].ToString(),
                        MER_ID_MERCANCIA_REF = r["MER_ID_MERCANCIA_REF"].ToString(),
                        MER_BULTOS_REF = r["MER_BULTOS_REF"].ToString(),
                        MER_LOC_RACK = r["MER_LOC_RACK"].ToString(),
                        MER_LOC_COLUMNA = r["MER_LOC_COLUMNA"].ToString(),
                        MER_LOC_LADO = r["MER_LOC_LADO"].ToString(),
                        MER_LOC_ALTURA = r["MER_LOC_ALTURA"].ToString(),
                        MER_FCH_LOCALIZACION = r["MER_FCH_LOCALIZACION"].ToString(),
                        MER_PERECEDERO = r["MER_PERECEDERO"].ToString(),
                        MER_LINEA_GUIA = r["MER_LINEA_GUIA"].ToString(),
                        MER_PUESTO_CHEQUEO = r["MER_PUESTO_CHEQUEO"].ToString(),
                        MER_MOV_INVENTARIO = r["MER_MOV_INVENTARIO"].ToString(),
                        MER_FCH_DIGITACION = r["MER_FCH_DIGITACION"].ToString(),
                        MER_FCH_TRANSMISION = r["MER_FCH_TRANSMISION"].ToString(),
                        MER_ARCHIVO_TRANSMISION = r["MER_ARCHIVO_TRANSMISION"].ToString(),
                        MER_USUARIO_TRANSMISION = r["MER_USUARIO_TRANSMISION"].ToString(),
                        MER_ERROR = r["MER_ERROR"].ToString(),
                        MER_ACCION = r["MER_ACCION"].ToString(),
                        MER_TRANSMISION = r["MER_TRANSMISION"].ToString(),
                        MER_FCH_ENDOSO = r["MER_FCH_ENDOSO"].ToString(),
                        MER_LARGO = r["MER_LARGO"].ToString(),
                        MER_ALTO = r["MER_ALTO"].ToString(),
                        MER_ANCHO = r["MER_ANCHO"].ToString(),
                        ESTADO = r["ESTADO"].ToString(),
                        DES_OPER_TICA = r["DES_OPER_TICA"].ToString()
                    });
                }
                return Ok(_list);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_MERCANCIAS_TB/GetMercanciasModificar")]
        [ResponseType(typeof(Tarimas_Consulta))]
        public IHttpActionResult GetMercanciasModificar(long? guia, long? mercancia, long? tipomercancia)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_MODI_MERCA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_ien_entrada_p", OracleDbType.Varchar2).Value = guia;
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Varchar2).Value = mercancia;
            objCmd.Parameters.Add("mer_tme_mercancia_p", OracleDbType.Long).Value = tipomercancia;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Tarimas_Modificar> _list = new List<Tarimas_Modificar>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {

                    _list.Add(new Tarimas_Modificar
                    {
                        MER_IEN_ENTRADA = r["MER_IEN_ENTRADA"].ToString(),
                        MER_MANIFIESTO = r["MER_MANIFIESTO"].ToString(),
                        MER_GUIA = r["MER_GUIA"].ToString(),
                        KILOS_INGRESADOS = r["KILOS_INGRESADOS"].ToString(),
                        BULTOS_INGRESADOS = r["BULTOS_INGRESADOS"].ToString(),
                        MER_TME_MERCANCIA = r["MER_TME_MERCANCIA"].ToString(),
                        DES_MERCANCIA = r["DES_MERCANCIA"].ToString(),
                        MER_MARCAS = r["MER_MARCAS"].ToString(),
                        MER_TIP_CODIGO_IATA = r["MER_TIP_CODIGO_IATA"].ToString(),
                        DESC_PELIG = r["DESC_PELIG"].ToString(),
                        MER_INDICE_TRANS = r["MER_INDICE_TRANS"].ToString(),
                        MER_CODIGO_UN = r["MER_CODIGO_UN"].ToString(),
                        MER_REFRIGERADO = r["MER_REFRIGERADO"].ToString(),
                        MER_ARMAS = r["MER_ARMAS"].ToString(),
                        MER_AVE_AVERIA = r["MER_AVE_AVERIA"].ToString(),
                        DESAVERIAS = r["DESAVERIAS"].ToString(),
                        MER_TEM_EMBALAJE = r["MER_TEM_EMBALAJE"].ToString(),
                        DESEMBALAJE = r["DESEMBALAJE"].ToString(),
                        MER_DESCRIPCION = r["MER_DESCRIPCION"].ToString(),
                        MER_OBSERVACIONES = r["MER_OBSERVACIONES"].ToString(),
                        MER_BULTOS_DESPALETIZADOS = r["MER_BULTOS_DESPALETIZADOS"].ToString(),
                        MER_KILOS_BRUTO = r["MER_KILOS_BRUTO"].ToString(),
                        MER_KILOS_TARIMA = r["MER_KILOS_TARIMA"].ToString(),
                        MER_KILOS_NETO = r["MER_KILOS_NETO"].ToString(),
                        MER_ESTADO = r["MER_ESTADO"].ToString(),
                        MER_OPERACION_TICA = r["MER_OPERACION_TICA"].ToString(),
                        MER_ID_MERCANCIA = r["MER_ID_MERCANCIA"].ToString(),
                        MER_ID_MERCANCIA_REF = r["MER_ID_MERCANCIA_REF"].ToString(),
                        MER_BULTOS_REF = r["MER_BULTOS_REF"].ToString(),
                        MER_LOC_RACK = r["MER_LOC_RACK"].ToString(),
                        MER_LOC_COLUMNA = r["MER_LOC_COLUMNA"].ToString(),
                        MER_LOC_LADO = r["MER_LOC_LADO"].ToString(),
                        MER_LOC_ALTURA = r["MER_LOC_ALTURA"].ToString(),
                        MER_PERECEDERO = r["MER_PERECEDERO"].ToString(),
                        MER_LINEA_GUIA = r["MER_LINEA_GUIA"].ToString(),
                        MER_PUESTO_CHEQUEO = r["MER_PUESTO_CHEQUEO"].ToString(),
                        MER_FCH_DIGITACION = r["MER_FCH_DIGITACION"].ToString(),
                        MER_TRANSMISION = r["MER_TRANSMISION"].ToString(),
                        MER_ARCHIVO_TRANSMISION = r["MER_ARCHIVO_TRANSMISION"].ToString(),
                        MER_FCH_TRANSMISION = r["MER_FCH_TRANSMISION"].ToString(),
                        MER_USUARIO_TRANSMISION = r["MER_USUARIO_TRANSMISION"].ToString(),
                        MER_FCH_ENDOSO = r["MER_FCH_ENDOSO"].ToString(),
                        MER_ACCION = r["MER_ACCION"].ToString(),
                        MER_MOV_INVENTARIO = r["MER_MOV_INVENTARIO"].ToString(),
                        MER_LARGO = r["MER_LARGO"].ToString(),
                        MER_ALTO = r["MER_ALTO"].ToString(),
                        MER_ANCHO = r["MER_ANCHO"].ToString(),
                        ESTADO = r["ESTADO"].ToString()
                    });
                }
                return Ok(_list);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_MERCANCIAS_TB/Mercancias_Modificar_Post")]
        public IHttpActionResult PostManifiestos_Modificar_Post(Tarimas_Modificar_post post)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_MODI_MERCA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("MER_ID_MERCANCIA_P", OracleDbType.Int32).Value = post.MER_ID_MERCANCIA;
            objCmd.Parameters.Add("MER_PUESTO_CHEQUEO_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_PUESTO_CHEQUEO) ? (int?)null : int.Parse(post.MER_PUESTO_CHEQUEO);  
            objCmd.Parameters.Add("MER_BULTOS_DESPALETIZADOS_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_BULTOS_DESPALETIZADOS) ? (int?)null : int.Parse(post.MER_BULTOS_DESPALETIZADOS); 
            objCmd.Parameters.Add("MER_KILOS_BRUTO_P", OracleDbType.Decimal).Value = string.IsNullOrEmpty(post.MER_KILOS_BRUTO) ? (decimal?)null : Decimal.Parse(post.MER_KILOS_BRUTO); 
            objCmd.Parameters.Add("MER_KILOS_TARIMA_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_KILOS_TARIMA) ? (int?)null : int.Parse(post.MER_KILOS_TARIMA); 
            objCmd.Parameters.Add("MER_TME_MERCANCIA_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_TME_MERCANCIA) ? (int?)null : int.Parse(post.MER_TME_MERCANCIA);
            objCmd.Parameters.Add("MER_TEM_EMBALAJE_P", OracleDbType.Varchar2).Value = post.MER_TEM_EMBALAJE;
            objCmd.Parameters.Add("MER_AVE_AVERIA_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_AVE_AVERIA) ? (int?)null : int.Parse(post.MER_AVE_AVERIA);  
            objCmd.Parameters.Add("MER_LARGO_P", OracleDbType.Decimal).Value = string.IsNullOrEmpty(post.MER_LARGO) ? (decimal?)null : Decimal.Parse(post.MER_LARGO);
            objCmd.Parameters.Add("MER_ALTO_P", OracleDbType.Decimal).Value = string.IsNullOrEmpty(post.MER_ALTO) ? (decimal?)null : Decimal.Parse(post.MER_ALTO);
            objCmd.Parameters.Add("MER_ANCHO_P", OracleDbType.Decimal).Value = string.IsNullOrEmpty(post.MER_ANCHO) ? (decimal?)null : Decimal.Parse(post.MER_ANCHO); 
            objCmd.Parameters.Add("MER_TIP_CODIGO_IATA_P", OracleDbType.Varchar2).Value = post.MER_TIP_CODIGO_IATA;
            objCmd.Parameters.Add("MER_CODIGO_UN_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_CODIGO_UN) ? (int?)null : int.Parse(post.MER_CODIGO_UN); 
            objCmd.Parameters.Add("MER_INDICE_TRANS_P", OracleDbType.Decimal).Value = string.IsNullOrEmpty(post.MER_INDICE_TRANS) ? (decimal?)null : Decimal.Parse(post.MER_INDICE_TRANS); 
            objCmd.Parameters.Add("MER_DESCRIPCION_P", OracleDbType.Varchar2).Value = post.MER_DESCRIPCION;
            objCmd.Parameters.Add("MER_MARCAS_P", OracleDbType.Varchar2).Value = post.MER_MARCAS;
            objCmd.Parameters.Add("MER_LINEA_GUIA_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_LINEA_GUIA) ? (int?)null : int.Parse(post.MER_LINEA_GUIA); 
            objCmd.Parameters.Add("MER_OPERACION_TICA_P", OracleDbType.Varchar2).Value = post.MER_OPERACION_TICA;
            objCmd.Parameters.Add("MER_OBSERVACIONES_P", OracleDbType.Varchar2).Value = post.MER_OBSERVACIONES;
            objCmd.Parameters.Add("MER_PERECEDERO_P", OracleDbType.Varchar2).Value = post.MER_PERECEDERO != null ? "S" : "N"; 
            objCmd.Parameters.Add("MER_REFRIGERADO_P", OracleDbType.Varchar2).Value = post.MER_REFRIGERADO != null ? "S" : "N";
            objCmd.Parameters.Add("MER_ARMAS_P", OracleDbType.Varchar2).Value = post.MER_ARMAS != null ? "S" : "N";
            objCmd.Parameters.Add("MER_ID_MERCANCIA_REF_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_ID_MERCANCIA_REF) ? (int?)null : int.Parse(post.MER_ID_MERCANCIA_REF);
            objCmd.Parameters.Add("MER_BULTOS_REF_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_BULTOS_REF) ? (int?)null : int.Parse(post.MER_BULTOS_REF);
            objCmd.Parameters.Add("MER_LOC_RACK_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_LOC_RACK) ? (int?)null : int.Parse(post.MER_LOC_RACK);
            objCmd.Parameters.Add("MER_LOC_COLUMNA_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_LOC_COLUMNA) ? (int?)null : int.Parse(post.MER_LOC_COLUMNA);
            objCmd.Parameters.Add("MER_LOC_LADO_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_LOC_LADO) ? (int?)null : int.Parse(post.MER_LOC_LADO); 
            objCmd.Parameters.Add("MER_LOC_ALTURA_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_LOC_ALTURA) ? (int?)null : int.Parse(post.MER_LOC_ALTURA);
            objCmd.Parameters.Add("MER_ESTADO_P", OracleDbType.Varchar2).Value = post.MER_ESTADO;
            objCmd.Parameters.Add("MER_USER_MERX_P", OracleDbType.Varchar2).Value = post.MER_USER_MERX;
            objCmd.Parameters.Add("MER_MOT_ULTIMA_MODIFICACION_P", OracleDbType.Int32).Value = string.IsNullOrEmpty(post.MER_MOT_ULTIMA_MODIFICACION) ? (int?)null : int.Parse(post.MER_MOT_ULTIMA_MODIFICACION);
            objCmd.Parameters.Add("MER_SOLICITA_CAMBIO_P", OracleDbType.Varchar2).Value = post.MER_SOLICITA_CAMBIO;
            objCmd.Parameters.Add("MER_RESPONSABLE_ERROR_P", OracleDbType.Varchar2).Value = post.MER_RESPONSABLE_ERROR;
            objCmd.Parameters.Add("MER_DEPARTAMENTO_P", OracleDbType.Varchar2).Value = post.MER_DEPARTAMENTO;

            try
            {
                objCmd.ExecuteNonQuery();
                return Ok("Ok");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion

        #region Mercancia con Cesión de Derechos

        /// <summary>
        /// Extrae los datos de la cesion de derechos que se va a colocar en los campos informativos
        /// </summary>
        /// <param name="entrada"></param>
        /// <param name="manifiesto"></param>
        /// <param name="guia"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/CesionDerechos")]
        [ResponseType(typeof(EntradasCES))]
        public IHttpActionResult GetCesionDerechos(long? entrada, string manifiesto, string guia, string consignatario)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
         objCmd.CommandText = "mrx.tx_qry_ces_entrada_pr  ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ien_entrada_p", OracleDbType.Int32).Value = entrada;
            objCmd.Parameters.Add("ien_manifiesto_p", OracleDbType.Varchar2).Value = manifiesto;
            objCmd.Parameters.Add("ien_guia_original_p", OracleDbType.Varchar2).Value = guia;
            objCmd.Parameters.Add("ien_consignatario_p", OracleDbType.Varchar2).Value = consignatario;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<EntradasCES> _cesiones = new List<EntradasCES>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();

                //if(datos2.Rows.Count == 1)
                //{
                foreach (DataRow e in datos2.Rows)
                {
                    _cesiones.Add(new EntradasCES
                    {
                        IEN_ENTRADA = e["IEN_ENTRADA"].ToString(),
                        IEN_MANIFIESTO = e["IEN_MANIFIESTO"].ToString(),
                        IEN_GUIA = e["IEN_GUIA"].ToString(),
                        IEN_GUIA_ORIGINAL = e["IEN_GUIA_ORIGINAL"].ToString(),
                        IEN_FCH_INGRESO = e["IEN_FCH_INGRESO"].ToString(),
                        IEN_CONSIGNATARIO = e["IEN_CONSIGNATARIO"].ToString(),
                        IEN_NUMERO_VIAJE = e["IEN_NUMERO_VIAJE"].ToString()
                    });
                        
                    }
                //}
                return Ok(_cesiones);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
           
        }

        /// <summary>
        /// Extrae los datos de cesion de derechos y su listado correspondiente a la tabla a llenar
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/CESMercancias")]
        [ResponseType(typeof(Cesiones))]
        public IHttpActionResult GetSolicitudes(long? entrada, long? mercancia)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_ces_mercancias_pr  ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ien_entrada_p", OracleDbType.Int32).Value = entrada;
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Int32).Value = mercancia;        
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Cesiones> _solicitudes = new List<Cesiones>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _solicitudes.Add(new Cesiones
                    {
                        MER_ID_MERCANCIA = e["MER_ID_MERCANCIA"].ToString(),
                        MER_IEN_ENTRADA = e["MER_IEN_ENTRADA"].ToString(),
                        MER_OPERACION_TICA = e["MER_OPERACION_TICA"].ToString(),
                        DESC_OPER_TICA = e["DESC_OPER_TICA"].ToString(),
                        MER_MOV_INVENTARIO = e["MER_MOV_INVENTARIO"].ToString(),
                        MER_BULTOS_DESPALETIZADOS = e["MER_BULTOS_DESPALETIZADOS"].ToString(),
                        MER_BULTOS_SALIDOS = e["MER_BULTOS_SALIDOS"].ToString(),
                        KILOS_DESPALETIZADOS = e["KILOS_DESPALETIZADOS"].ToString(),
                        MER_KILOS_SALIDOS = e["MER_KILOS_SALIDOS"].ToString(),
                        MER_ID_MERCANCIA_REF = e["MER_ID_MERCANCIA_REF"].ToString(),
                        MER_BULTOS_REF = e["MER_BULTOS_REF"].ToString(),
                        MER_ESTADO = e["MER_ESTADO"].ToString(),
                        DESC_ESTADO = e["DESC_ESTADO"].ToString()

                    });
                }
                return Ok(_solicitudes);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        /// <summary>
        /// Realiza el update de bultos y kilos de la cesion de derechos
        /// </summary>
        /// <param name="id"></param>
        /// <param name="bultos_salida"></param>
        /// <param name="kilos_salidos"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/ActualizaCES")]
        [ResponseType(typeof(Mercancia))]
        public IHttpActionResult PutCESMercancias(long? id, long? bultos_salida, decimal? kilos_salidos, string user)
        {           
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_upd_ces_mercancias_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Int32).Value = id;
            objCmd.Parameters.Add("mer_bultos_salidos_p", OracleDbType.Int32).Value = bultos_salida;
            objCmd.Parameters.Add("mer_kilos_salidos_p", OracleDbType.Int32).Value = kilos_salidos;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = user;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            
        }

        #endregion

        #region Tarimas Ingreso

        /// <summary>
        /// Lista los tipos de mercancía
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/tiposMercancias")]
        [ResponseType(typeof(tipoMercancias))]
        public IHttpActionResult GetTiposMercancia(long? tipo)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_tipo_merc_pr  ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tipo_mer_p", OracleDbType.Int32).Value = tipo;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<tipoMercancias> _tiposMercancias = new List<tipoMercancias>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _tiposMercancias.Add(new tipoMercancias
                    {
                        Id = e["CODIGO"].ToString(),
                        Nombre = e["DESCRIPCION"].ToString()

                    });
                }
                return Ok(_tiposMercancias);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Obtiene tipos de embalaje
        /// </summary>
        /// <param name="embalaje"></param>
        /// <param name="entrada"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/tiposEmbalaje")]
        [ResponseType(typeof(Embalaje))]
        public IHttpActionResult GetEmbalajes(string embalaje, long? entrada)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_embalaje_pr  ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("embalaje_p", OracleDbType.Varchar2).Value = embalaje;
            objCmd.Parameters.Add("entrada_p", OracleDbType.Int32).Value = entrada;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Embalaje> _tiposEmbalaje = new List<Embalaje>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _tiposEmbalaje.Add(new Embalaje
                    {
                        Id = e["CODIGO"].ToString(),
                        Nombre = e["DESCRIPCION"].ToString()

                    });
                }
                return Ok(_tiposEmbalaje);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Lista los tipos de averia
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/tiposAveria")]
        [ResponseType(typeof(tipoAveria))]
        public IHttpActionResult GetTiposAveria(long? tipo)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_tipos_averia_pr  ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tipo_averia_p", OracleDbType.Int32).Value = tipo;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<tipoAveria> _tiposAveria = new List<tipoAveria>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _tiposAveria.Add(new tipoAveria
                    {
                        Id = e["CODIGO"].ToString(),
                        Nombre = e["DESCRIPCION"].ToString()

                    });
                }
                return Ok(_tiposAveria);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_MERCANCIAS_TB/peligrocidadIata")]
        [ResponseType(typeof(Peligrosidad))]
        public IHttpActionResult GetPeligrosidad(string tipo)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_iata_pelig_pr  ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tipo_codigo_iata_p", OracleDbType.Varchar2).Value = tipo;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Peligrosidad> _peligrosidad = new List<Peligrosidad>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _peligrosidad.Add(new Peligrosidad
                    {
                        Id = e["CODIGO"].ToString(),
                        Nombre = e["DESCRIPCION"].ToString()

                    });
                }
                return Ok(_peligrosidad);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Lista los estados
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/Estados")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetEstados()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_estados_mer_pr  ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Estados> _estados = new List<Estados>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _estados.Add(new Estados
                    {
                        Id = e["Id"].ToString(),
                        Nombre = e["Nombre"].ToString()

                    });
                }
                return Ok(_estados);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Lista las operaciones tica
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/OperacionTica")]
        [ResponseType(typeof(OperacionTica))]
        public IHttpActionResult GetOperacionTica()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
        objCmd.CommandText = "tx_lv_merc_opera_tica_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<OperacionTica> _operaTica = new List<OperacionTica>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                _operaTica.Add(new OperacionTica
                    {
                        Id = e["Id"].ToString(),
                        Nombre = e["Nombre"].ToString()

                    });
                }
                return Ok(_operaTica);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_MERCANCIAS_TB/TarimasReferencia")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetTarimasReferencia(long conse)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_MERC_VIRTUALES_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("entrada_p", OracleDbType.Varchar2).Value = conse;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Estados> _estados = new List<Estados>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _estados.Add(new Estados
                    {
                        Id = e["Id"].ToString(),
                        Nombre = e["Nombre"].ToString()

                    });
                }
                return Ok(_estados);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_MERCANCIAS_TB/MovimientosReferencia")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetMovimientosReferencia(long conse)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_MOVS_REF_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("entrada_p", OracleDbType.Varchar2).Value = conse;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Estados> _estados = new List<Estados>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _estados.Add(new Estados
                    {
                        Id = e["Id"].ToString(),
                        Nombre = e["Nombre"].ToString()

                    });
                }
                return Ok(_estados);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }





        [Route("api/TX_MERCANCIAS_TB/MovsFracciona")]
        [ResponseType(typeof(Mov_Fracciona))]
        public IHttpActionResult GetMovsFracciona(long conse, long tarimaref)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_MOVS_FRAC_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("entrada_p", OracleDbType.Int32).Value = conse;
            objCmd.Parameters.Add("tarima_ref_p", OracleDbType.Int32).Value = tarimaref;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Mov_Fracciona> _Movs = new List<Mov_Fracciona>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _Movs.Add(new Mov_Fracciona
                    {
                        MER_MOV_INVENTARIO = e["MER_MOV_INVENTARIO"].ToString(),
                        BL = e["BL"].ToString(),
                        OPERACION = e["OPERACION"].ToString(),
                        MER_ID_MERCANCIA = e["MER_ID_MERCANCIA"].ToString(),
                        MER_BULTOS_DESPALETIZADOS = e["MER_BULTOS_DESPALETIZADOS"].ToString(),
                        MER_KILOS_BRUTO = e["MER_KILOS_BRUTO"].ToString(),
                        MOV_REF = e["MOV_REF"].ToString(),
                        BLT_REF = e["BLT_REF"].ToString()

                    });
                }
                return Ok(_Movs);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_MERCANCIAS_TB/CalculaBultosRef")]
        [ResponseType(typeof(Mov_Fracciona))]
        public IHttpActionResult PostCalculaBultosRef(long conse, long tarimaref, long bultosref, string user)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_CALCULA_BLT_REF_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("entrada_p", OracleDbType.Int32).Value = conse;
            objCmd.Parameters.Add("tarima_ref_p", OracleDbType.Int32).Value = tarimaref;
            objCmd.Parameters.Add("bultos_p", OracleDbType.Int32).Value = bultosref;
            objCmd.Parameters.Add("mer_usu_ult_modif_p", OracleDbType.Varchar2).Value = user;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Guarda Tarima 
        /// </summary>
        /// <param name="mercancia"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/GuardarMercancia")]
        [ResponseType(typeof(Mercancia1))]
        public IHttpActionResult PostTX_MERCANCIAS_TB(Mercancia1 mercancia)
        {
            if (mercancia.MER_MOV_REF != "" && mercancia.MER_MOV_REF != null)
            {
                mercancia.MER_ID_MERCANCIA_REF = mercancia.MER_MOV_REF;
            }
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_ingreso_merca_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_puesto_chequeo_p", OracleDbType.Int32).Value = mercancia.MER_PUESTO_CHEQUEO;
            objCmd.Parameters.Add("mer_ien_entrada_p", OracleDbType.Int32).Value = mercancia.IEN_ENTRADA;
            objCmd.Parameters.Add("mer_bultos_despaletizados_p", OracleDbType.Int32).Value = mercancia.MER_BULTOS_DESPALETIZADOS;
            objCmd.Parameters.Add("mer_kilos_bruto_p", OracleDbType.Decimal).Value = mercancia.MER_KILOS_BRUTO.AsDecimal();
            objCmd.Parameters.Add("mer_kilos_tarima_p", OracleDbType.Int32).Value = mercancia.MER_KILOS_TARIMA;
            objCmd.Parameters.Add("mer_tme_mercancia_p", OracleDbType.Int32).Value = mercancia.MER_TME_MERCANCIA;
            objCmd.Parameters.Add("mer_tem_embalaje_p", OracleDbType.Varchar2).Value = mercancia.MER_TEM_EMBALAJE;
            objCmd.Parameters.Add("mer_ave_averia_p", OracleDbType.Int32).Value = mercancia.MER_AVE_AVERIA;
            objCmd.Parameters.Add("mer_largo_p", OracleDbType.Decimal).Value = string.IsNullOrEmpty(mercancia.MER_LARGO) ? (decimal?)null : Decimal.Parse(mercancia.MER_LARGO);
            objCmd.Parameters.Add("mer_alto_p", OracleDbType.Decimal).Value = string.IsNullOrEmpty(mercancia.MER_ALTO) ? (decimal?)null : Decimal.Parse(mercancia.MER_ALTO); 
            objCmd.Parameters.Add("mer_ancho_p", OracleDbType.Decimal).Value = string.IsNullOrEmpty(mercancia.MER_ANCHO) ? (decimal?)null : Decimal.Parse(mercancia.MER_ANCHO);
            objCmd.Parameters.Add("mer_tip_codigo_iata_p", OracleDbType.Varchar2).Value = mercancia.MER_TIP_CODIGO_IATA;
            objCmd.Parameters.Add("mer_codigo_un_p", OracleDbType.Int32).Value = mercancia.MER_CODIGO_UN;
            objCmd.Parameters.Add("mer_indice_trans_p", OracleDbType.Decimal).Value = string.IsNullOrEmpty(mercancia.MER_INDICE_TRANS) ? (decimal?)null : Decimal.Parse(mercancia.MER_INDICE_TRANS);
            objCmd.Parameters.Add("mer_descripcion_p", OracleDbType.Varchar2).Value = mercancia.MER_DESCRIPCION;
            objCmd.Parameters.Add("mer_marcas_p", OracleDbType.Varchar2).Value = mercancia.MER_MARCAS;
            objCmd.Parameters.Add("mer_linea_guia_p", OracleDbType.Int32).Value = mercancia.MER_LINEA_GUIA;
            objCmd.Parameters.Add("mer_operacion_tica_p", OracleDbType.Varchar2).Value = mercancia.MER_OPERACION_TICA;
            objCmd.Parameters.Add("mer_observaciones_p", OracleDbType.Varchar2).Value = mercancia.MER_OBSERVACIONES;
            objCmd.Parameters.Add("mer_perecedero_p", OracleDbType.Varchar2).Value = mercancia.MER_PERECEDERO != null ? "S" : "N"; 
            objCmd.Parameters.Add("mer_refrigerado_p", OracleDbType.Varchar2).Value = mercancia.MER_REFRIGERADO != null ? "S" : "N";
            objCmd.Parameters.Add("mer_armas_p", OracleDbType.Varchar2).Value = mercancia.MER_ARMAS != null ? "S" : "N";
            objCmd.Parameters.Add("mer_id_mercancia_ref_p", OracleDbType.Int32).Value = mercancia.MER_ID_MERCANCIA_REF;
            objCmd.Parameters.Add("mer_bultos_ref_p", OracleDbType.Int32).Value = mercancia.MER_BULTOS_REF;
            objCmd.Parameters.Add("mer_loc_rack_p", OracleDbType.Int32).Value = mercancia.MER_LOC_RACK;
            objCmd.Parameters.Add("mer_loc_columna_p", OracleDbType.Int32).Value = mercancia.MER_LOC_COLUMNA;
            objCmd.Parameters.Add("mer_loc_lado_p", OracleDbType.Int32).Value = mercancia.MER_LOC_LADO;
            objCmd.Parameters.Add("mer_loc_altura_p", OracleDbType.Int32).Value = mercancia.MER_LOC_ALTURA;
            objCmd.Parameters.Add("mer_estado_p", OracleDbType.Varchar2).Value = mercancia.MER_ESTADO;
            objCmd.Parameters.Add("mer_etiqueta_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_ETIQUETA) ? (int?)null : int.Parse(mercancia.MER_ETIQUETA);
            objCmd.Parameters.Add("mer_user_merx_p", OracleDbType.Varchar2).Value = mercancia.MER_USUARIO;
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Int32).Direction = ParameterDirection.Output;
        try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                var tarima = objCmd.Parameters["mer_id_mercancia_p"].Value.ToString();
                return Ok(tarima);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Actualiza Tarima
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="mercancia"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/Actualizar")]
        [ResponseType(typeof(Mercancia1))]
        public IHttpActionResult PutTX_ENTRADAS_TB_Ingreso(string usuario, Mercancia1 mercancia)
        {
            if (mercancia.MER_MOV_REF != "")
            {
                mercancia.MER_ID_MERCANCIA_REF = mercancia.MER_MOV_REF;
            }
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_upd_ingreso_merca_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Int32).Value = mercancia.MER_ID_MERCANCIA;
            objCmd.Parameters.Add("mer_puesto_chequeo_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_PUESTO_CHEQUEO) ? (int?)null : int.Parse(mercancia.MER_PUESTO_CHEQUEO);
            objCmd.Parameters.Add("mer_bultos_despaletizados_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_BULTOS_DESPALETIZADOS) ? (int?)null : int.Parse(mercancia.MER_BULTOS_DESPALETIZADOS);
            objCmd.Parameters.Add("mer_kilos_bruto_p", OracleDbType.Decimal).Value = string.IsNullOrEmpty(mercancia.MER_KILOS_BRUTO) ? (decimal?)null : Decimal.Parse(mercancia.MER_KILOS_BRUTO);
            objCmd.Parameters.Add("mer_kilos_tarima_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_KILOS_TARIMA) ? (int?)null : int.Parse(mercancia.MER_KILOS_TARIMA);
            objCmd.Parameters.Add("mer_tme_mercancia_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_TME_MERCANCIA) ? (int?)null : int.Parse(mercancia.MER_TME_MERCANCIA);
            objCmd.Parameters.Add("mer_tem_embalaje_p", OracleDbType.Varchar2).Value = mercancia.MER_TEM_EMBALAJE;
            objCmd.Parameters.Add("mer_ave_averia_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_AVE_AVERIA) ? (int?)null : int.Parse(mercancia.MER_AVE_AVERIA);
            objCmd.Parameters.Add("mer_largo_p", OracleDbType.Decimal).Value = string.IsNullOrEmpty(mercancia.MER_LARGO) ? (decimal?)null : Decimal.Parse(mercancia.MER_LARGO);
            objCmd.Parameters.Add("mer_alto_p", OracleDbType.Decimal).Value = string.IsNullOrEmpty(mercancia.MER_ALTO) ? (decimal?)null : Decimal.Parse(mercancia.MER_ALTO);
            objCmd.Parameters.Add("mer_ancho_p", OracleDbType.Decimal).Value = string.IsNullOrEmpty(mercancia.MER_ANCHO) ? (decimal?)null : Decimal.Parse(mercancia.MER_ANCHO);
            objCmd.Parameters.Add("mer_tip_codigo_iata_p", OracleDbType.Varchar2).Value = mercancia.MER_TIP_CODIGO_IATA;
            objCmd.Parameters.Add("mer_codigo_un_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_CODIGO_UN) ? (int?)null : int.Parse(mercancia.MER_CODIGO_UN);
            objCmd.Parameters.Add("mer_indice_trans_p", OracleDbType.Decimal).Value = string.IsNullOrEmpty(mercancia.MER_INDICE_TRANS) ? (decimal?)null : Decimal.Parse(mercancia.MER_INDICE_TRANS);
            objCmd.Parameters.Add("mer_descripcion_p", OracleDbType.Varchar2).Value = mercancia.MER_DESCRIPCION;
            objCmd.Parameters.Add("mer_marcas_p", OracleDbType.Varchar2).Value = mercancia.MER_MARCAS;
            objCmd.Parameters.Add("mer_linea_guia_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_LINEA_GUIA) ? (int?)null : int.Parse(mercancia.MER_LINEA_GUIA);
            objCmd.Parameters.Add("mer_operacion_tica_p", OracleDbType.Varchar2).Value = mercancia.MER_OPERACION_TICA;
            objCmd.Parameters.Add("mer_observaciones_p", OracleDbType.Varchar2).Value = mercancia.MER_OBSERVACIONES;
            objCmd.Parameters.Add("mer_perecedero_p", OracleDbType.Varchar2).Value = mercancia.MER_PERECEDERO != null ? "S" : "N";
            objCmd.Parameters.Add("mer_refrigerado_p", OracleDbType.Varchar2).Value = mercancia.MER_REFRIGERADO != null ? "S" : "N";
            objCmd.Parameters.Add("mer_armas_p", OracleDbType.Varchar2).Value = mercancia.MER_ARMAS != null ? "S" : "N";
            objCmd.Parameters.Add("mer_id_mercancia_ref_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_ID_MERCANCIA_REF) ? (int?)null : int.Parse(mercancia.MER_ID_MERCANCIA_REF);
            objCmd.Parameters.Add("mer_bultos_ref_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_BULTOS_REF) ? (int?)null : int.Parse(mercancia.MER_BULTOS_REF);
            objCmd.Parameters.Add("mer_loc_rack_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_LOC_RACK) ? (int?)null : int.Parse(mercancia.MER_LOC_RACK);
            objCmd.Parameters.Add("mer_loc_columna_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_LOC_COLUMNA) ? (int?)null : int.Parse(mercancia.MER_LOC_COLUMNA);
            objCmd.Parameters.Add("mer_loc_lado_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_LOC_LADO) ? (int?)null : int.Parse(mercancia.MER_LOC_LADO);
            objCmd.Parameters.Add("mer_loc_altura_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_LOC_ALTURA) ? (int?)null : int.Parse(mercancia.MER_LOC_ALTURA);
            objCmd.Parameters.Add("mer_estado_p", OracleDbType.Varchar2).Value = mercancia.MER_ESTADO;
            objCmd.Parameters.Add("mer_etiqueta_p", OracleDbType.Int32).Value = string.IsNullOrEmpty(mercancia.MER_ETIQUETA) ? (int?)null : int.Parse(mercancia.MER_ETIQUETA);
            objCmd.Parameters.Add("mer_user_merx_p", OracleDbType.Varchar2).Value = usuario;

            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Elimina la tarima 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_TB/Borrar")]
        [ResponseType(typeof(Mercancia1))]
        public IHttpActionResult Delete(long id, string user)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_DEL_INGRESO_MERCA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Int32).Value = id;
            objCmd.Parameters.Add("mer_user_merx_p", OracleDbType.Varchar2).Value = user;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();

            return Ok();


        }

        #endregion
    }
}