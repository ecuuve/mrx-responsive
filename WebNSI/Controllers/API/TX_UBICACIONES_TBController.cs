﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_UBICACIONES_TBController : ApiController
    {
        [Route("api/TX_UBICACIONES_TB/Ubicaciones")]
        [ResponseType(typeof(Ubicaciones))]
        public IHttpActionResult GetModalidadesTransporte(string nombre)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_ubicaciones_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("id_ubicacion_p", OracleDbType.Varchar2).Value = nombre;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Ubicaciones> _ubicaciones = new List<Ubicaciones>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _ubicaciones.Add(new Ubicaciones
                    {

                        Id = e["CODIGO"].ToString(),
                        Nombre = e["DESCRIPCION"].ToString()
                    });
                }
                return Ok(_ubicaciones);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
