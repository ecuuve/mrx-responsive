﻿using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_PREALERTAS_TBController : ApiController
    {
        /// <summary>
        /// DEVUELVE EL NOMBRE DEL CONSOLIDADOR AL  RECIBIR COMO PARAMETRO EL CODIGO DEL MISMO
        /// </summary>
        /// <param name="consolidador"></param>
        /// <returns></returns>
        [Route("api/TX_PREALERTAS_TB/GetConsolidador")]
        public string GetConsolidador(string consolidador)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_PRE_CONSOLIDADOR_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("consolidador_p", OracleDbType.Varchar2).Value = consolidador;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();
            DataTable datos = new DataTable();
            List<Cons_Prealertas> _Prealertas = new List<Cons_Prealertas>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos);

            cn.Close();
            if(datos.Rows.Count != 0)
            {
                DataRow row = datos.Rows[0];
                string nombre = row["CON_NOMBRE"].ToString();
                return nombre;
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// LISTA LAS PREALERTAS QUE YA SE ENCUENTRAN REGISTRADAS EN LA BASE DE DATOS, EXTRAE SOLAMENTE GUIA, CONSOLIDADOR Y NOMBRE CONSOLIDADOR
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_PREALERTAS_TB/GetPrealertas")]
        public IHttpActionResult GetPrealertas()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_pre_alert_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("plr_guia_p", OracleDbType.Varchar2).Value = "";
            objCmd.Parameters.Add("plr_consolida_p", OracleDbType.Varchar2).Value = "";
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Prealertas> _Prealertas = new List<Prealertas>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();
            int cont = 0;
            foreach (DataRow e in datos2.Rows)
            {
                if (cont < 15)
                {
                    _Prealertas.Add(new Prealertas
                    {
                        PLR_GUIA = e["PLR_GUIA"].ToString(),
                        //PLR_CON_NCONSOLIDADOR = e["PLR_CON_NCONSOLIDADOR"].ToString(),
                        NOM_CONSOL = e["NOM_CONSOL"].ToString(),
                        PLR_ARCHIVO = e["PLR_ARCHIVO"].ToString(),
                        PLR_USU_REGISTRO = e["PLR_USU_REGISTRO"].ToString(),
                        PLR_FCH_REGISTRO = e["PLR_FCH_REGISTRO"].ToString()
                    });
                }
                cont++;
            }

            return Ok(_Prealertas);
        }

        [Route("api/TX_PREALERTAS_TB/AbrirPDF")]
        public IHttpActionResult GetArchivo(string pdf)
        {
            //string serverFolder = "\\\\Dpsavfss\\COMPARTIDOS\\PUBLICO\\PREALERTASALSATEST\\";
            //string serverFolder = "\\\\pasqui - fs\\EMPRESAS\\PUBLICO\\PREALERTASALSATEST";
            string serverFolder = "\\\\dssavfs1.grupopasqui.com\\CompartidosSitioAlterno\\PUBLICO\\PREALERTASALSA\\";
            string destinationFilename = Path.Combine(
                       serverFolder + pdf);
            bool extfile = File.Exists(destinationFilename);
            if (extfile)
            {
                System.Diagnostics.Process.Start(destinationFilename);
                // window.open(destinationFilename, '_blank');   


                return Ok("Se abre el archivo:" + pdf);

               

            }
            else
            {
                return BadRequest("El archivo: " + pdf + " no existe en la carpeta.");
            }
           
        }




        /// <summary>
        /// LISTA TODAS LAS PREALERTAS REGISTRADAS EN LA BASE DE DATOS CON TODOS LOS CAMPOS QUE POSEE EN LA TABLA
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_PREALERTAS_TB/GetConsPrealertas")]
        public IHttpActionResult GetConsPrealertas(string p_guia)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_pre_alert_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("plr_guia_p", OracleDbType.Varchar2).Value = p_guia;
            objCmd.Parameters.Add("plr_consolida_p", OracleDbType.Varchar2).Value = "";
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Cons_Prealertas> _Prealertas = new List<Cons_Prealertas>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();
            //int cont = 0;
            foreach (DataRow e in datos2.Rows)
            {
                //if (cont < 100)
                //{
                    _Prealertas.Add(new Cons_Prealertas
                    {
                        PLR_GUIA = e["PLR_GUIA"].ToString(),
                        PLR_CON_NCONSOLIDADOR = e["PLR_CON_NCONSOLIDADOR"].ToString(),
                        NOM_CONSOL = e["NOM_CONSOL"].ToString(),
                        PLR_USU_REGISTRO = e["PLR_USU_REGISTRO"].ToString(),
                        PLR_FCH_REGISTRO = e["PLR_FCH_REGISTRO"].ToString(),
                        PLR_ARCHIVO = e["PLR_ARCHIVO"].ToString()
                    });
                //}
                //cont++;
            }

            return Ok(_Prealertas);
        }

        /// <summary>
        /// EVALUA SI LA GUIA ESTÁ, CUANTAS FILAS TIENE Y TRAE INFORMACION DE LA MISMA
        /// </summary>
        /// <param name="guia"></param>
        /// <returns></returns>
        [Route("api/TX_PREALERTAS_TB/PostEvaluaGuia")]
        public IHttpActionResult PostEvaluaGuia(Desconsolidacion info)
        {
            var info1 = HttpContext.Current.Request.Params["json"];
            // var archivopdf = HttpContext.Current.Request.Params["pdf"];

            info = JsonConvert.DeserializeObject<Desconsolidacion>(info1);



            if (info.GUIA_ORIGINAL != "" && info.CONSECUTIVO == null)
            {
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_QRY_GUIA_ORIGINAL_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("GUIA_ORIGINAL_P", OracleDbType.Varchar2).Value = info.GUIA_ORIGINAL;
                objCmd.Parameters.Add("CONSECUTIVO_IN_P", OracleDbType.Int32).Value = info.CONSECUTIVO;
                objCmd.Parameters.Add("CANTIDAD_P", OracleDbType.Char, 100).Direction = ParameterDirection.Output;
                objCmd.Parameters.Add("CONSECUTIVO_P", OracleDbType.Char, 100).Direction = ParameterDirection.Output;
                objCmd.Parameters.Add("ESTADO_P", OracleDbType.Char, 1).Direction = ParameterDirection.Output;
                objCmd.Parameters.Add("MANIFIESTO_P", OracleDbType.Char, 100).Direction = ParameterDirection.Output;
                objCmd.Parameters.Add("ERROR_P", OracleDbType.Char, 100).Direction = ParameterDirection.Output;

                try
                {
                    objCmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    //Si ya  tiene una prealerta registrada envía el mensaje
                    return BadRequest(e.Message);
                }

                //Guarda informacion reelevante a la misma, cantidad de lineas, el consecutivo, el estado y el manifiesto
                string cantidad = objCmd.Parameters["CANTIDAD_P"].Value.ToString();
                string consecutivo = objCmd.Parameters["CONSECUTIVO_P"].Value.ToString();
                string estado = objCmd.Parameters["ESTADO_P"].Value.ToString();
                string manifiesto = objCmd.Parameters["MANIFIESTO_P"].Value.ToString();
                string error = objCmd.Parameters["ERROR_P"].Value.ToString();
                
                //Si no tiene cantidad es porque no tiene prealertas entonces inserta
                if (cantidad == "null")
                {
                    //inserta prealerta
                    if (info.CONSOLIDADOR != "") //Si no tiene consolidador se le debe enviar un mensaje al usuario para indicarle que lo dijite
                    {
                        if (info.ARCHIVO == "S")
                        {
                            HttpRequest request = HttpContext.Current.Request;
                            //string serverFolder = "\\\\Dpsavfss\\COMPARTIDOS\\PUBLICO\\PREALERTASALSATEST\\";
                            //string serverFolder = "\\\\pasqui - fs\\EMPRESAS\\PUBLICO\\PREALERTASALSATEST";
                            string serverFolder = "\\\\dssavfs1.grupopasqui.com\\CompartidosSitioAlterno\\PUBLICO\\PREALERTASALSATEST\\";
                            HttpFileCollection files = request.Files;
                            HttpPostedFile postedFile = files["pdf"];

                            if (postedFile == null)
                            {
                                //Error
                                return BadRequest("No se cargó ningún archivo .pdf");
                            }

                            if (postedFile.ContentType != "application/pdf")
                            {
                                //Error
                                return BadRequest("Solo se acepta el tipo de archivo .pdf");
                            }

                            string destinationFilename = Path.Combine(
                                            serverFolder, info.GUIA_ORIGINAL.Trim() + "_Prealerta.pdf");

                            bool extfile = File.Exists(destinationFilename);

                            if (!extfile)
                            {
                                //Guarda en la carpeta el archivo pdf de prealerta.
                                postedFile.SaveAs(destinationFilename);
                            }
                            else
                            {
                                return BadRequest(" Ya existe registro de Prealerta para la guía: " + info.GUIA_ORIGINAL + ".");
                            }

                            OracleCommand objCmd1 = new OracleCommand();
                            objCmd1.Connection = cn;
                            objCmd1.CommandText = "mrx.tx_ins_pre_alert_pr";
                            objCmd1.CommandType = CommandType.StoredProcedure;
                            objCmd1.Parameters.Add("desconsolidar_p", OracleDbType.Varchar2).Value = "N";
                            objCmd1.Parameters.Add("entrada_p", OracleDbType.Int32).Value = null;
                            objCmd1.Parameters.Add("plr_guia_p", OracleDbType.Varchar2).Value = info.GUIA_ORIGINAL;
                            objCmd1.Parameters.Add("plr_con_nconsolidador_p", OracleDbType.Int32).Value = info.CONSOLIDADOR;
                            objCmd1.Parameters.Add("plr_usuario", OracleDbType.Varchar2).Value = info.UserMERX;
                            objCmd1.Parameters.Add("plr_archivo_p", OracleDbType.Varchar2).Value = info.GUIA_ORIGINAL.Trim() + "_Prealerta.pdf";
                            
                            try
                            {
                                objCmd1.ExecuteNonQuery();
                                return Ok("La prealerta se ha insertado correctamente");
                            }
                            catch (Exception ex)
                            {
                                return BadRequest(ex.Message);
                            }
                        }
                        else
                        {
                            return BadRequest("Debe agregar un archivo .pdf de la prealerta");
                        }
                       
                    }
                    else
                    {
                        return BadRequest("Debe digitar un consolidador");
                    }
                }
                else
                {
                    //Si solo posee una linea se  consulta el estado, si está en n o m se pregunta al usuario si desea hacer una desconsolidación
                    if (Convert.ToInt32(cantidad) == 1)
                    {
                        if (estado == "N" || estado == "M")
                        {
                            Desconsolidacion desconsolidador = new Desconsolidacion();
                            desconsolidador.CONSECUTIVO = consecutivo.ToString();
                            desconsolidador.GUIA_ORIGINAL = info.GUIA_ORIGINAL;
                            desconsolidador.TOT_GUIAS = cantidad.ToString();
                            desconsolidador.USUARIO = info.UserMERX;//"MRX";
                            return Ok(desconsolidador);
                        }
                        else
                        {
                            return BadRequest("La guía ya fue digitada en otro estado que no es N o M");
                        }
                    }
                    else
                    {
                        return BadRequest("La guía aparece en más de un manifiesto");
                    }
                }
            }
            else if (info.CONSECUTIVO != "" && info.USUARIO != "" && info.TOT_GUIAS != "" && info.VUELO != "" && info.CONSOLIDADOR != "")
            {
                //Valida que el vuelo digitado sea el correcto
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_QRY_GUIA_MANIFIESTO_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("GUIA_ORIGINAL_P", OracleDbType.Varchar2).Value = info.GUIA_ORIGINAL;
                objCmd.Parameters.Add("MANIFIESTO_P", OracleDbType.Varchar2).Value = info.VUELO;
                objCmd.Parameters.Add("CONSECUTIVO_P", OracleDbType.Char,50).Direction = ParameterDirection.Output;
                objCmd.Parameters.Add("error_p", OracleDbType.Char,100).Direction = ParameterDirection.Output;
                try
                {
                    objCmd.ExecuteNonQuery();
                    //Si se valido correctamente insesrta la desconsolidación
                    if (info.ARCHIVO == "S")
                    {
                        HttpRequest request = HttpContext.Current.Request;
                        //string serverFolder = "\\\\Dpsavfss\\COMPARTIDOS\\PUBLICO\\PREALERTASALSATEST\\";
                        //string serverFolder = "\\\\pasqui-fs\\EMPRESAS\\PUBLICO\\PREALERTASALSATEST";
                        string serverFolder = "\\\\dssavfs1.grupopasqui.com\\CompartidosSitioAlterno\\PUBLICO\\PREALERTASALSATEST";
                        HttpFileCollection files = request.Files;
                        HttpPostedFile postedFile = files["pdf"];

                        if (postedFile == null)
                        {
                            //Error
                            return BadRequest("No se cargó ningún archivo .pdf");
                        }

                        if (postedFile.ContentType != "application/pdf")
                        {
                            //Error
                            return BadRequest("Solo se acepta el tipo de archivo .pdf");
                        }

                        string destinationFilename = Path.Combine(
                                        serverFolder,  info.CONSECUTIVO.Trim() + "_Desconsolida.pdf");

                        bool extfile = File.Exists(destinationFilename);

                        if (!extfile)
                        {
                            //Guarda en la carpeta el archivo pdf de prealerta.
                            postedFile.SaveAs(destinationFilename);
                        }
                        else
                        {
                            return BadRequest(" Ya existe registro de Desconsolidación para la guía: " + info.GUIA_ORIGINAL + ".");
                        }




                        if (objCmd.Parameters["CONSECUTIVO_P"].Value.ToString() != "" || objCmd.Parameters["CONSECUTIVO_P"].Value.ToString() != null)
                        {
                            OracleCommand objCmd3 = new OracleCommand();
                            objCmd3.Connection = cn;
                            objCmd3.CommandText = "mrx.tx_ins_pre_alert_pr";
                            objCmd3.CommandType = CommandType.StoredProcedure;
                            objCmd3.Parameters.Add("desconsolidar_p", OracleDbType.Varchar2).Value = "S";
                            objCmd3.Parameters.Add("entrada_p", OracleDbType.Int32).Value = info.CONSECUTIVO;
                            objCmd3.Parameters.Add("plr_guia_p", OracleDbType.Varchar2).Value = info.GUIA_ORIGINAL;
                            objCmd3.Parameters.Add("plr_con_nconsolidador_p", OracleDbType.Int32).Value = info.CONSOLIDADOR;
                            objCmd3.Parameters.Add("plr_usuario", OracleDbType.Varchar2).Value = info.UserMERX;
                            objCmd3.Parameters.Add("plr_archivo_p", OracleDbType.Varchar2).Value = info.CONSECUTIVO.Trim() + "_Desconsolida.pdf";
                            try
                            {
                                objCmd3.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                return BadRequest(ex.Message);
                            }
                        }
                    }
                    else
                    {
                        return BadRequest("Debe agregar el archivo .pdf de desconsolidación.");
                    }
                    
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
                cn.Close();
                return Ok("Desconsolidación Ingresada correctamente");
            }
            else
            {
                return BadRequest("Debe ingresar datos en consolidador y vuelo");
            }
            //return Ok();
        }

        /// <summary>
        /// INSERTAR LA DESCONSOLIDACION EN LA BASE DE DATOS
        /// </summary>
        /// <param name="desconsolidacion"></param>
        /// <returns></returns>
        //[Route("api/TX_PREALERTAS_TB/PostDesconsolidacion")]
        //public IHttpActionResult PostDesconsolidacion(Desconsolidacion desconsolidacion)
        //{
        //}

        public IHttpActionResult PostTX_PREALERTAS_TB(Prealertas prealerta_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_pre_alert_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("desconsolidar_p", OracleDbType.Varchar2).Value = "";
            objCmd.Parameters.Add("entrada_p", OracleDbType.Int32).Value = "";
            objCmd.Parameters.Add("plr_guia_p", OracleDbType.Varchar2).Value = prealerta_p.PLR_GUIA;
            objCmd.Parameters.Add("plr_con_nconsolidador_p", OracleDbType.Int32).Value = prealerta_p.PLR_CON_NCONSOLIDADOR;
            objCmd.Parameters.Add("error_p", OracleDbType.Varchar2).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Prealertas _PrealertaIn = new Prealertas();

            return Ok(_PrealertaIn);
        }


        #region PANTALLA CONOCIMIENTOS

            /// <summary>
            /// Pregunta si existen prealertas para esa guia y devuelve el archivo
            /// </summary>
            /// <param name="guia"></param>
            /// <returns></returns>
            [Route("api/TX_PREALERTAS_TB/GetPrealerta")]
            public IHttpActionResult GetPrealerta(string guia)
            {
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_BUSCA_PREALERTA_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("guia_p", OracleDbType.Varchar2).Value = guia;
                objCmd.Parameters.Add("existe_p", OracleDbType.Char, 1).Value = ParameterDirection.Output;
                objCmd.Parameters.Add("archivo_p", OracleDbType.Char, 100).Direction = ParameterDirection.Output;
                try
                {
                    objCmd.ExecuteNonQuery();
                    cn.Close();
                    string existe = objCmd.Parameters["existe_p"].Value.ToString();
                    string archivo = objCmd.Parameters["archivo_p"].Value.ToString();
                    if (existe == "S")
                    {
                        return Ok(archivo);
                    }
                    else
                    {
                        return Ok("N");
                    }
                }
                catch(Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
        #endregion
    }
}
