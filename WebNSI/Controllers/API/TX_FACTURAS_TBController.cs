﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_FACTURAS_TBController : ApiController
    {
        #region "Carga de datos inicial"

            /// <summary>
            /// Obtiene tipo de cambio
            /// </summary>
            /// <returns></returns>
            [Route("api/TX_FACTURAS_TB/TipoCambio")]
            [ResponseType(typeof(string))]
            public IHttpActionResult GetTipoCambio()
            {
                //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_OBT_TIPO_CAMBIO_FAC_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
               // objCmd.Parameters.Add("FECHA_P", OracleDbType.Varchar2).Value = "31/12/2018";
                objCmd.Parameters.Add("TIPO_CAMBIO_P", OracleDbType.Int32).Direction = ParameterDirection.Output;

                objCmd.ExecuteNonQuery();         
                cn.Close();

                decimal tipoCambio = decimal.Parse(objCmd.Parameters["TIPO_CAMBIO_P"].Value.ToString());
                return Ok(tipoCambio);
            }          

            /// <summary>
            /// Obtiene la cédula del importador según la ubicación
            /// </summary>
            /// <param name="codigo"></param>
            /// <returns></returns>
            [Route("api/TX_FACTURAS_TB/GetUbicacion")]
            [ResponseType(typeof(string))]
            public IHttpActionResult GetUbicacion(string codigo)
            {
                //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_QRY_UBIC_FACT_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("id_ubicacion_p", OracleDbType.Varchar2).Value = codigo;
                objCmd.Parameters.Add("ced_import_p", OracleDbType.Int32).Direction = ParameterDirection.Output;

                objCmd.ExecuteNonQuery();
                cn.Close();

                var cedUbic = objCmd.Parameters["ced_import_p"].Value.ToString();

                if (cedUbic == "null")
                {
                    cedUbic = null;
                }

                return Ok(cedUbic);
            }

        #endregion


        #region "Carga de LV"

        //TAB PREFACTURA

        /// <summary>
        /// Lista los agentes del declarante dado
        /// </summary>
        /// <param name="declarante"></param>
        /// <param name="agente"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetAgentesDecl")]
        [ResponseType(typeof(LV_Facturacion))]
        public IHttpActionResult GetAgentesDecl(long declarante)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_AGENTES_DECL_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("declarante_p", OracleDbType.Int32).Value = declarante;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<LV_Facturacion> _AgentesDecl = new List<LV_Facturacion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _AgentesDecl.Add(new LV_Facturacion
                {
                    Id = e["ID"].ToString(),
                    Nombre = e["NOMBRE"].ToString()
                });
            }
            return Ok(_AgentesDecl);
        }

        /// <summary>
        /// Obtiene los declarantes
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetDeclarantes")]
        [ResponseType(typeof(LV_Facturacion))]
        public IHttpActionResult GetDeclarantes(string id, string nombre)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_declarantes_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dct_declarante_id_p", OracleDbType.Varchar2).Value = id;
            objCmd.Parameters.Add("dct_nombre_p", OracleDbType.Varchar2).Value = nombre;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<LV_Facturacion> _Declarantes = new List<LV_Facturacion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Declarantes.Add(new LV_Facturacion
                {
                    Id = e["DCT_CEDULA"].ToString(),
                    Nombre = e["DCT_NOMBRE"].ToString()
                });
            }
            return Ok(_Declarantes);
        }

        /// <summary>
        /// Obtiene tipo de documento
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/TipoDocumento")]
        [ResponseType(typeof(LV_Facturacion))]
        public IHttpActionResult GetTipoDocumento()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_tipo_documento_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tipo_doc_p", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<LV_Facturacion> _TiposDocumento = new List<LV_Facturacion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TiposDocumento.Add(new LV_Facturacion
                {
                    Id = r["id"].ToString(),
                    Nombre = r["nombre"].ToString()
                });
            }

            return Ok(_TiposDocumento);
        }

        /// <summary>
        /// Obtiene cuentas 
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/BuscarCuentas")]
        [ResponseType(typeof(LV_Facturacion))]
        public IHttpActionResult BuscarCuentas([FromBody] PARAM_Cuentas parametros)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CUENTA_CLIENTE_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tipo_p", OracleDbType.Varchar2).Value = parametros.tipo_p;
            objCmd.Parameters.Add("nombre_p", OracleDbType.Varchar2).Value = parametros.nombre_p;
            objCmd.Parameters.Add("cuenta_p", OracleDbType.Varchar2).Value = parametros.cuenta_p;
            objCmd.Parameters.Add("forma_pago_p", OracleDbType.Varchar2).Value = parametros.forma_pago_p;
            objCmd.Parameters.Add("tipo_cta_p", OracleDbType.Varchar2).Value = parametros.tipo_cta_p;
            objCmd.Parameters.Add("importador_p", OracleDbType.Varchar2).Value = parametros.importador_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<LV_Facturacion> _Cuentas = new List<LV_Facturacion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Cuentas.Add(new LV_Facturacion
                {
                    Id = e["CLIENTE"].ToString(),
                    Nombre = e["NOMBRE"].ToString()
                });
            }
            return Ok(_Cuentas);
        }


        //TAB DE MOVIMIENTOS

        /// <summary>
        /// Trae las tarifas para movimientos
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="tarifa"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetTarifas")]
        [ResponseType(typeof(LV_Facturacion))]
        public IHttpActionResult GetTarifas(string nombre, long? tarifa)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_TARIFA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("NOMBRE_P", OracleDbType.Varchar2).Value = nombre;
            objCmd.Parameters.Add("TARIFA_P", OracleDbType.Int32).Value = tarifa;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<LV_Facturacion> _Tarifas = new List<LV_Facturacion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Tarifas.Add(new LV_Facturacion
                {
                    Id = e["TRF_ID"].ToString(),
                    Nombre = e["TRF_NOMBRE"].ToString()
                });
            }
            return Ok(_Tarifas);
        }
            
        /// <summary>
        /// Lista las bases de descuento
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetBases")]
        [ResponseType(typeof(LV_Facturacion))]
        public IHttpActionResult GetBases()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_base_cobro_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<LV_Facturacion> _Bases = new List<LV_Facturacion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Bases.Add(new LV_Facturacion
                {
                    Id = e["ID"].ToString(),
                    Nombre = e["NOMBRE"].ToString()
                });
            }
            return Ok(_Bases);
        }

        //TAB ADICIONALES

        /// <summary>
        /// Lista los conceptos
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/Conceptos")]
        [ResponseType(typeof(LV_Facturacion))]
        public IHttpActionResult GetConceptos()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_RUBROS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            //objCmd.Parameters.Add("tipo_doc_p", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<LV_Facturacion> _Conceptos = new List<LV_Facturacion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Conceptos.Add(new LV_Facturacion
                {
                    Id = r["ID"].ToString(),
                    Nombre = r["NOMBRE"].ToString()
                });
            }

            return Ok(_Conceptos);
        }
       
        #endregion


        #region "TAB PREFACTURA"

        /// <summary>
        /// Guarda encabezado de factura
        /// </summary>
        /// <param name="encabezado_p"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GuardarEncabezado")]
        [ResponseType(typeof(string))]
        public IHttpActionResult PostTX_FACTURAS_TB(FAC_ENCABEZADO encabezado_p)
        {
            if (encabezado_p.FAC_EXENTA == null)
            {
                encabezado_p.FAC_EXENTA = "N";
                encabezado_p.FAC_DESDE_EXENCION = null;
                encabezado_p.FAC_HASTA_EXENCION = null;
            }
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_FACTURA_ENCAB_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("FAC_TIPO_TIQUETE", OracleDbType.Varchar2).Value = encabezado_p.FAC_TIPO_TIQUETE;
            objCmd.Parameters.Add("FAC_TIQUETE", OracleDbType.Int32).Value = encabezado_p.FAC_TIQUETE;
            objCmd.Parameters.Add("FAC_FCH_TIQUETE", OracleDbType.Varchar2).Value = encabezado_p.FAC_FCH_TIQUETE;
            objCmd.Parameters.Add("FAC_FCH_ATENDIO", OracleDbType.Varchar2).Value = encabezado_p.FAC_FCH_ATENDIO;
            objCmd.Parameters.Add("FAC_DUA_SALIDA", OracleDbType.Varchar2).Value = encabezado_p.FAC_DUA_SALIDA;
            objCmd.Parameters.Add("FAC_VIAJE_TRANSITO", OracleDbType.Varchar2).Value = encabezado_p.FAC_VIAJE_TRANSITO;
            objCmd.Parameters.Add("FAC_FCH_DUA", OracleDbType.Varchar2).Value = encabezado_p.FAC_FCH_DUA;
            objCmd.Parameters.Add("FAC_BULTOS_DUA", OracleDbType.Int32).Value = encabezado_p.FAC_BULTOS_DUA;
            objCmd.Parameters.Add("FAC_DIN_TIPO_DOCUMENTO", OracleDbType.Varchar2).Value = encabezado_p.FAC_DIN_TIPO_DOCUMENTO;
            objCmd.Parameters.Add("FAC_VALOR_CIF", OracleDbType.Decimal).Value = encabezado_p.FAC_VALOR_CIF;
            objCmd.Parameters.Add("FAC_IMPUESTOS_P", OracleDbType.Decimal).Value = encabezado_p.FAC_IMPUESTOS;
            objCmd.Parameters.Add("FAC_IMP_EXENTOS", OracleDbType.Decimal).Value = encabezado_p.FAC_IMP_EXENTOS;
            objCmd.Parameters.Add("FAC_EXENTA", OracleDbType.Varchar2).Value = encabezado_p.FAC_EXENTA;
            objCmd.Parameters.Add("FAC_NOTA_EXENCION", OracleDbType.Varchar2).Value = encabezado_p.FAC_NOTA_EXENCION;
            objCmd.Parameters.Add("FAC_DESDE_EXENCION", OracleDbType.Varchar2).Value = encabezado_p.FAC_DESDE_EXENCION;
            objCmd.Parameters.Add("FAC_HASTA_EXENCION", OracleDbType.Varchar2).Value = encabezado_p.FAC_HASTA_EXENCION;
            objCmd.Parameters.Add("FAC_UBI_UBICACION", OracleDbType.Varchar2).Value = encabezado_p.FAC_UBI_UBICACION;
            objCmd.Parameters.Add("FAC_IMP_CEDULA", OracleDbType.Varchar2).Value = encabezado_p.FAC_IMP_CEDULA;
            objCmd.Parameters.Add("FAC_MARCH_ELECTRONICO", OracleDbType.Varchar2).Value = encabezado_p.FAC_MARCH_ELECTRONICO;
            objCmd.Parameters.Add("FAC_TRAMITE", OracleDbType.Varchar2).Value = encabezado_p.FAC_TRAMITE;
            objCmd.Parameters.Add("FAC_CLIENTE_DUA", OracleDbType.Varchar2).Value = encabezado_p.FAC_CLIENTE_DUA;
            objCmd.Parameters.Add("FAC_TIPO_CAMBIO", OracleDbType.Decimal).Value = encabezado_p.FAC_TIPO_CAMBIO;
            objCmd.Parameters.Add("FAC_DCT_DECLARANTE_ID", OracleDbType.Varchar2).Value = encabezado_p.FAC_DCT_DECLARANTE_ID;
            objCmd.Parameters.Add("FAC_ADC_AGENTE_ID", OracleDbType.Int32).Value = encabezado_p.FAC_ADC_AGENTE_ID;
            objCmd.Parameters.Add("FAC_CONTADO", OracleDbType.Varchar2).Value = encabezado_p.FAC_CONTADO;
            objCmd.Parameters.Add("FAC_CUENTA", OracleDbType.Int32).Value = encabezado_p.FAC_CUENTA;
            objCmd.Parameters.Add("FAC_CONTRIBUYENTE", OracleDbType.Varchar2).Value = encabezado_p.FAC_CONTRIBUYENTE;
            objCmd.Parameters.Add("FAC_ESTADO", OracleDbType.Varchar2).Value = encabezado_p.FAC_ESTADO;
            objCmd.Parameters.Add("FAC_USU_DIGITO", OracleDbType.Varchar2).Value = encabezado_p.FAC_USU_DIGITO;
            objCmd.Parameters.Add("FAC_FACTURA_P", OracleDbType.Int32).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            

            var numFactura = objCmd.Parameters["FAC_FACTURA_P"].Value.ToString();

            if (numFactura == "null")
            {
                numFactura = null;
            }
            cn.Close();
            return Ok(numFactura);
        }

            
        /// <summary>
        /// Anula el encabezado 
        /// </summary>
        /// <param name="factura_p"></param>
        /// <param name="user_p"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/ANULAR_ENCAB")]
        [ResponseType(typeof(string))]
        public IHttpActionResult Post_ANULAR(long factura_p, string user_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_FACT_ANUL_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("fac_factura_p", OracleDbType.Int32).Value = factura_p;
            objCmd.Parameters.Add("usu_modif_p", OracleDbType.Varchar2).Value = user_p;
            try
            {
                objCmd.ExecuteNonQuery();              
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();
            return Ok("Se anuló encabezado");
        }
        
        #endregion


        #region "TAB DE MOVIMIENTOS"

        /// <summary>
        /// Obtiene datos del encabezado con el # de factura generado
        /// </summary>
        /// <param name="factura"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetObtieneEncabezado")]
        [ResponseType(typeof(FAC_ENCABEZADO))]
        public IHttpActionResult GetObtieneEncabezado(long factura)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_FACTURA_ENCAB_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("FAC_FACTURA_P", OracleDbType.Int32).Value = factura;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<FAC_ENCABEZADO> _DetEncab = new List<FAC_ENCABEZADO>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _DetEncab.Add(new FAC_ENCABEZADO
                {
                    FAC_FACTURA = long.Parse(e["FAC_FACTURA"].ToString()),
                    FAC_DUA_SALIDA = e["FAC_DUA_SALIDA"].ToString(),
                    FAC_VIAJE_TRANSITO = e["FAC_VIAJE_TRANSITO"].ToString(),
                    FAC_FCH_DUA = e["FAC_FCH_DUA"].ToString(),
                    FAC_BULTOS_DUA = long.Parse(e["FAC_BULTOS_DUA"].ToString()),
                    FAC_DIN_TIPO_DOCUMENTO = e["FAC_BULTOS_DUA"].ToString(),
                    FAC_VALOR_CIF = decimal.Parse(e["FAC_VALOR_CIF"].ToString()),
                    FAC_IMPUESTOS = decimal.Parse(e["FAC_IMPUESTOS"].ToString()),
                    FAC_IMP_EXENTOS = decimal.Parse(e["FAC_IMP_EXENTOS"].ToString()),
                    FAC_EXENTA = e["FAC_EXENTA"].ToString(),
                    FAC_NOTA_EXENCION = e["FAC_NOTA_EXENCION"].ToString(),
                    FAC_DESDE_EXENCION = e["FAC_DESDE_EXENCION"].ToString(),
                    FAC_HASTA_EXENCION = e["FAC_HASTA_EXENCION"].ToString(),
                    FAC_UBI_UBICACION = e["FAC_UBI_UBICACION"].ToString(),
                    FAC_MARCH_ELECTRONICO = e["FAC_MARCH_ELECTRONICO"].ToString(),
                    FAC_TRAMITE = e["FAC_TRAMITE"].ToString(),
                    FAC_CLIENTE_DUA = e["FAC_CLIENTE_DUA"].ToString(),
                    FAC_TIPO_CAMBIO = decimal.Parse(e["FAC_TIPO_CAMBIO"].ToString()),
                    FAC_DCT_DECLARANTE_ID = e["FAC_DCT_DECLARANTE_ID"].ToString(),
                    FAC_ADC_AGENTE_ID = long.Parse(e["FAC_ADC_AGENTE_ID"].ToString()),
                    FAC_CUENTA = long.Parse(e["FAC_CUENTA"].ToString()),
                    FAC_CONTRIBUYENTE = e["FAC_CONTRIBUYENTE"].ToString()
                });
            }
            return Ok(_DetEncab);
        }

        /// <summary>
        /// Obtiene datos del movimiento e inserta movimiento a la factura
        /// </summary>
        /// <param name="movimiento"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetObtMovimiento")]
        [ResponseType(typeof(string))]
        public IHttpActionResult GetObtMovimiento(long factura, long movimiento, string user)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_OBT_FACTURA_MOVS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("FACTURA_P", OracleDbType.Int32).Value = factura;
            objCmd.Parameters.Add("MOVIMIENTO_P", OracleDbType.Int32).Value = movimiento;
            objCmd.Parameters.Add("USU_P", OracleDbType.Varchar2).Value = user;


            try
            {
                objCmd.ExecuteNonQuery();              
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();
            return Ok("Movimiento Agregado");
        }

        /// <summary>
        /// Consulta los movimientos de la factura
        /// </summary>
        /// <param name="factura"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetObtMovimientosFact")]
        [ResponseType(typeof(FAC_MOVIMIENTO))]
        public IHttpActionResult GetObtMovimientosFact(long factura,long? mov)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_FACT_MOVIMIENTO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("FMV_ID_P", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("FMV_FAC_FACTURA_P", OracleDbType.Int32).Value = factura;
            objCmd.Parameters.Add("FMV_MOVIMIENTO_P", OracleDbType.Int32).Value = mov;
            objCmd.Parameters.Add("FMV_IEN_ENTRADA_P", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<FAC_MOVIMIENTO> _DetMov = new List<FAC_MOVIMIENTO>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _DetMov.Add(new FAC_MOVIMIENTO
                {
                    FMV_ID = e["ID_MOVIMIENTO"].ToString(),
                    FMV_FAC_FACTURA = long.Parse(e["NUMERO_FACTURA"].ToString()),
                    FMV_MOVIMIENTO = long.Parse(e["NUMERO_MOVIMIENTO"].ToString()),
                    FMV_IEN_ENTRADA = long.Parse(e["NUMERO_ENTRADA"].ToString()),
                    FMV_BULTOS = long.Parse(e["BULTOS"].ToString()),
                    FMV_KILOS = decimal.Parse(e["KILOS"].ToString()),
                    FMV_VOLUMEN = decimal.Parse(e["VOLUMEN"].ToString()),
                    FMV_TME_MERCANCIA = long.Parse(e["CODIGO_MERCANCIA"].ToString()),
                    TME_DESCRIPCION = e["DESCRIPCION_MERCANCIA"].ToString(),
                    FMV_ITT_TARIFA = long.Parse(e["TIPO_TARIFA"].ToString()),
                    ITT_DESCRIPCION = e["DESCRIPCION_TIPO_TARIFA"].ToString(),
                    FMV_BASE_DESCUENTO = e["BASE_DESCUENTO"].ToString(),
                    FMV_PCT_DESCUENTO = decimal.Parse(e["PORCENTAJE_DESCUENTO"].ToString()),
                    FMV_MONTO_DESCUENTO = decimal.Parse(e["MONTO_DESCUENTO"].ToString()),
                    FMV_ASOCIE = e["ASOCIE"].ToString(),
                    FMV_MONTO_A_PAGAR = decimal.Parse(e["MONTO_A_PAGAR"].ToString()),
                    FMV_USU_REGISTRO = e["USUARIO_REGISTRO"].ToString(),
                    FMV_FCH_REGISTRO = e["FECHA_REGISTRO"].ToString(),


                    FMV_ALMACENAJE = string.IsNullOrWhiteSpace(e["ALMACENAJE"].ToString()) ? 0 : decimal.Parse(e["ALMACENAJE"].ToString()), //decimal.Parse(e["ALMACENAJE"].ToString()),
                    FMV_MANEJO = string.IsNullOrWhiteSpace(e["MANEJO"].ToString()) ? 0 : decimal.Parse(e["MANEJO"].ToString()), //decimal.Parse(e["MANEJO"].ToString()),
                    FMV_SEGURO = string.IsNullOrWhiteSpace(e["SEGURO"].ToString()) ? 0 : decimal.Parse(e["SEGURO"].ToString()), //decimal.Parse(e["SEGURO"].ToString()),
                    FMV_PCT_DESC_ALMAC = string.IsNullOrWhiteSpace(e["PCT_ALMACE"].ToString()) ? 0 : decimal.Parse(e["PCT_ALMACE"].ToString()), // decimal.Parse(e["PCT_ALMACE"].ToString()),
                    FMV_DESC_ALMAC = string.IsNullOrWhiteSpace(e["DESC_ALMAC"].ToString()) ? 0 : decimal.Parse(e["DESC_ALMAC"].ToString()), //decimal.Parse(e["DESC_ALMAC"].ToString()),
                    FMV_PCT_DESC_MANEJ = string.IsNullOrWhiteSpace(e["PCT_MANEJO"].ToString()) ? 0 : decimal.Parse(e["PCT_MANEJO"].ToString()), // decimal.Parse(e["PCT_MANEJO"].ToString()),
                    FMV_DESC_MANEJ = string.IsNullOrWhiteSpace(e["DESC_MANEJ"].ToString()) ? 0 : decimal.Parse(e["DESC_MANEJ"].ToString()), //decimal.Parse(e["DESC_MANEJ"].ToString()),
                    FMV_PCT_DESC_SEGUR = string.IsNullOrWhiteSpace(e["PCT_SEGUR"].ToString()) ? 0 : decimal.Parse(e["PCT_SEGUR"].ToString()), //decimal.Parse(e["PCT_SEGUR"].ToString()),
                    FMV_DESC_SEGUR = string.IsNullOrWhiteSpace(e["DESC_SEGUR"].ToString()) ? 0 : decimal.Parse(e["DESC_SEGUR"].ToString()), // decimal.Parse(e["DESC_SEGUR"].ToString()),
                    FMV_IVA_ALMACE = string.IsNullOrWhiteSpace(e["IVA_ALMACE"].ToString()) ? 0 : decimal.Parse(e["IVA_ALMACE"].ToString()), //decimal.Parse(e["IVA_ALMACE"].ToString()),
                    FMV_IVA_MANEJO = string.IsNullOrWhiteSpace(e["IVA_MANEJO"].ToString()) ? 0 : decimal.Parse(e["IVA_MANEJO"].ToString()), //decimal.Parse(e["IVA_MANEJO"].ToString()),
                    FMV_IVA_SEGURO = string.IsNullOrWhiteSpace(e["IVA_SEGURO"].ToString()) ? 0 : decimal.Parse(e["IVA_SEGURO"].ToString()), //decimal.Parse(e["IVA_SEGURO"].ToString()),
                    FMV_IVA_ASOCIE = string.IsNullOrWhiteSpace(e["IVA_ASOCIE"].ToString()) ? 0 : decimal.Parse(e["IVA_ASOCIE"].ToString()), //decimal.Parse(e["IVA_ASOCIE"].ToString()),
                    FMV_TOTAL_IVA = string.IsNullOrWhiteSpace(e["TOTAL_IVA"].ToString()) ? 0 : decimal.Parse(e["TOTAL_IVA"].ToString()), //decimal.Parse(e["TOTAL_IVA"].ToString())                 
                });
            }
            return Ok(_DetMov);
        }

        /// <summary>
        /// Elimina el movimiento
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/DeleteMovimiento")]
        [ResponseType(typeof(String))]
        public IHttpActionResult DeleteMovimiento(long id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_DEL_FACT_MOVIMIENTO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("MOVIMIENTO_ID_P", OracleDbType.Int32).Value = id;
            // objCmd.Parameters.Add("user_p", OracleDbType.Varchar2).Value = "MRX";
            try
            {
                objCmd.ExecuteNonQuery();              
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            cn.Close();
            return Ok("Movimiento Eliminado");
        }
            
        /// <summary>
        /// Obtiene la cédula endoso del movimiento
        /// </summary>
        /// <param name="mov_p"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetCedEndoso")]
        [ResponseType(typeof(string))]
        public IHttpActionResult GetCedEndoso(long mov_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CED_ENDOSO_FACT_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("movimiento_p", OracleDbType.Int32).Value = mov_p;
            objCmd.Parameters.Add("cedula_p", OracleDbType.Int32).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();
            cn.Close();

            var cedEnd = objCmd.Parameters["cedula_p"].Value.ToString();

            if (cedEnd == "null")
            {
                cedEnd = null;
            }

            return Ok(cedEnd);
        }

        /// <summary>
        /// Obtiene los valores luego de hacer calculos
        /// </summary>
        /// <param name="mov_p"></param>
        /// <param name="pct_desc_p"></param>
        /// <param name="base_p"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetCalculaPct")]
        [ResponseType(typeof(FAC_CALCULOS_MOV))]
        public IHttpActionResult GetCalculaPct(long mov_p, decimal pct_desc_p, string base_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_CALCULA_DESCT_MOV_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("FMV_ID_P", OracleDbType.Int32).Value = mov_p;
            objCmd.Parameters.Add("PCT_DESCT_P", OracleDbType.Int32).Value = pct_desc_p;
            objCmd.Parameters.Add("BASE_P", OracleDbType.Varchar2).Value = base_p;
            //objCmd.Parameters.Add("PCT_IVA_P", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("ALMACENAJE_P", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("DESC_ALMA_P", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("IVA_ALMA_P", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("MANEJO_P", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("DESC_MANEJ_P", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("IVA_MANEJ_P", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("SEGURO_P", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("DESCUENTO_P", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("IVA_P", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("TOTAL_LINEA_P", OracleDbType.Int32).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();



            var almacenaje = objCmd.Parameters["ALMACENAJE_P"].Value.ToString();
            var manejo = objCmd.Parameters["MANEJO_P"].Value.ToString();
            var seguro = objCmd.Parameters["SEGURO_P"].Value.ToString();
            var descuento = objCmd.Parameters["DESCUENTO_P"].Value.ToString();
            var imp_iva = objCmd.Parameters["IVA_P"].Value.ToString();
            var total_linea = objCmd.Parameters["TOTAL_LINEA_P"].Value.ToString();


            cn.Close();



            //List<FAC_CALCULOS> _Calculos = new List<FAC_CALCULOS>();
            FAC_CALCULOS_MOV _Calculo = new FAC_CALCULOS_MOV();
            _Calculo.ALMACENAJE = decimal.Parse(almacenaje);
            _Calculo.MANEJO = decimal.Parse(manejo);
            _Calculo.SEGURO = decimal.Parse(seguro);
            _Calculo.DESCUENTO = decimal.Parse(descuento);
            _Calculo.IMP_IVA = decimal.Parse(imp_iva);
            _Calculo.TOTAL = decimal.Parse(total_linea);

            // _Calculos.Add(_Calculo);

            return Ok(_Calculo);
        }

        /// <summary>
        /// Obtiene las tarimas del movimiento
        /// </summary>
        /// <param name="factura_p"></param>
        /// <param name="id_mov_p"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetTarimas")]
        [ResponseType(typeof(FAC_TARIMA))]
        public IHttpActionResult GetTarimas(long factura_p, long id_mov_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_FACT_TARIMAS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("FMV_FAC_FACTURA_P", OracleDbType.Int32).Value = factura_p;
            objCmd.Parameters.Add("FTA_FMV_ID_P", OracleDbType.Int32).Value = id_mov_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<FAC_TARIMA> _Tarimas = new List<FAC_TARIMA>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Tarimas.Add(new FAC_TARIMA
                {
                    ID_TARIMA = long.Parse(e["ID_TARIMA"].ToString()),
                    ID_MOVIMIENTO = long.Parse(e["ID_MOVIMIENTO"].ToString()),
                    ID_MERCANCIA = long.Parse(e["ID_MERCANCIA"].ToString()),
                    DESCRIPCION_MERCANCIA = e["DESCRIPCION_MERCANCIA"].ToString(),
                    BULTOS = long.Parse(e["BULTOS"].ToString()),
                    KILOS = decimal.Parse(e["KILOS"].ToString()),
                    VOLUMEN = decimal.Parse(e["VOLUMEN"].ToString()),
                    OBSERVACIONES = e["OBSERVACIONES"].ToString()
                });
            }
            return Ok(_Tarimas);
        }

        /// <summary>
        /// Borra tarima de movimiento
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/DeleteTarimaMovimiento")]
        [ResponseType(typeof(String))]
        public IHttpActionResult DeleteTarimaMovimiento(long id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_DEL_FACT_MOVIMIENTO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("FTA_ID_P", OracleDbType.Int32).Value = id;
            // objCmd.Parameters.Add("user_p", OracleDbType.Varchar2).Value = "MRX";
            try
            {
                objCmd.ExecuteNonQuery();              
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();
            return Ok("Tarima de movimiento se ha eliminado");
        }

        /// <summary>
        /// Actualiza tarima de movimiento
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/ActualizarTarima")]
        [ResponseType(typeof(string))]
        public IHttpActionResult PostTX_FACTURA_TARIMAS_TB_ACT(FAC_TARIMA post)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_FACT_TARIMA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("FTA_ID_P", OracleDbType.Int32).Value = post.ID_TARIMA;
            objCmd.Parameters.Add("FTA_BULTOS_P", OracleDbType.Int32).Value = post.BULTOS;
            objCmd.Parameters.Add("FTA_KILOS_P", OracleDbType.Decimal).Value = post.KILOS;
            objCmd.Parameters.Add("FTA_VOLUMEN_P", OracleDbType.Decimal).Value = post.VOLUMEN;
            try
            {
                objCmd.ExecuteNonQuery();             
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();
            return Ok("Se actualizó tarima");
        }

        /// <summary>
        /// Actualiza tarifa de movimiento
        /// </summary>
        /// <param name="tarifa_p"></param>
        /// <param name="id_p"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/ACTUALIZA_TARIFA")]
        [ResponseType(typeof(string))]
        public IHttpActionResult Post_ACTUALIZA_TARIFA(long tarifa_p, long id_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_FACT_MOVS_TARIFA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("FMV_ID_P", OracleDbType.Int32).Value = id_p;
            objCmd.Parameters.Add("FMV_ITT_TARIFA_P", OracleDbType.Int32).Value = tarifa_p;
            try
            {
                objCmd.ExecuteNonQuery();               
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();
            return Ok("Se actualizó tarifa");
        }

        #endregion


        #region "TAB ADICIONALES"

        /// <summary>
        /// Agrega el adicional 
        /// </summary>
        /// <param name="encabezado_p"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/AgregarAdicional")]
        [ResponseType(typeof(string))]
        public IHttpActionResult PostAgregaAdicional(long fac, string rubro, float monto, string user)
            {           
                //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_INS_FACT_RUBRO_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("fco_fac_factura_p", OracleDbType.Int32).Value = fac;
                objCmd.Parameters.Add("fco_rub_rubro_p", OracleDbType.Varchar2).Value = rubro;
                objCmd.Parameters.Add("fco_monto_p", OracleDbType.Decimal).Value = monto;
                //objCmd.Parameters.Add("fco_fmv_movimiento_p", OracleDbType.Int32).Value = encabezado_p.FAC_FCH_ATENDIO;
                objCmd.Parameters.Add("fco_usu_registro_p", OracleDbType.Varchar2).Value = user;
                try
                {
                    objCmd.ExecuteNonQuery();                 
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }

                cn.Close();
                return Ok("Se agregó el adicional");

        }

        /// <summary>
        /// Lista adicionales de la factura
        /// </summary>
        /// <param name="factura"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetAdicionales")]
        [ResponseType(typeof(FAC_RUBRO))]
        public IHttpActionResult GetAdicionales(long factura)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_FACT_RUBRO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("factura_p", OracleDbType.Int32).Value = factura;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<FAC_RUBRO> _Adicionales = new List<FAC_RUBRO>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Adicionales.Add(new FAC_RUBRO
                {
                    FCO_RUB_RUBRO = e["FCO_RUB_RUBRO"].ToString(),
                    FCO_MONTO = decimal.Parse(e["FCO_MONTO"].ToString())
                });
            }
            return Ok(_Adicionales);
        }
            
        /// <summary>
        /// Elimina adicional de la factura
        /// </summary>
        /// <param name="fac"></param>
        /// <param name="rub"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/DeleteAdicional")]
        [ResponseType(typeof(String))]
        public IHttpActionResult DeleteAdicional(long fac, string rub)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_DEL_FACT_RUBRO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("fco_fac_factura_p", OracleDbType.Int32).Value = fac;
            objCmd.Parameters.Add("fco_rub_rubro_p", OracleDbType.Varchar2).Value = rub;
            try
            {
                objCmd.ExecuteNonQuery();             
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();
            return Ok("Adicional Eliminado");
        }

        #endregion


        #region "TAB DETALLES"

        /// <summary>
        /// Obtiene la lista de personas
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetPersonas")]
        [ResponseType(typeof(FAC_PERSONA))]
        public IHttpActionResult GetPersonas(string id, string nombre,long? cod)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_PERSONA_FACT_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("cfr_nombre_p", OracleDbType.Varchar2).Value = nombre;
            objCmd.Parameters.Add("cfr_cedula_p", OracleDbType.Varchar2).Value = id;
            objCmd.Parameters.Add("cfr_codigo_p", OracleDbType.Varchar2).Value = cod;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<FAC_PERSONA> _Personas = new List<FAC_PERSONA>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Personas.Add(new FAC_PERSONA
                {
                    CFR_CODIGO = e["CFR_CODIGO"].ToString(),
                    CFR_CEDULA = e["CFR_CEDULA"].ToString(),
                    CFR_NOMBRE = e["CFR_NOMBRE"].ToString(),
                    CFR_TELEFONO = e["CFR_TELEFONO"].ToString()
                });
            }
            return Ok(_Personas);
        }

        /// <summary>
        /// Obtiene la lista de conductores
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetConductores")]
        [ResponseType(typeof(FAC_CONDUCTOR))]
        public IHttpActionResult GetConductores(string id, string nombre, long? cod)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONDUCTOR_FACT_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tra_nombre_p", OracleDbType.Varchar2).Value = nombre;
            objCmd.Parameters.Add("tra_cedula_p", OracleDbType.Varchar2).Value = id;
            objCmd.Parameters.Add("tra_transportista_p", OracleDbType.Int32).Value = cod;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<FAC_CONDUCTOR> _Conductores = new List<FAC_CONDUCTOR>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Conductores.Add(new FAC_CONDUCTOR
                {
                    TRA_TRANSPORTISTA = e["TRA_TRANSPORTISTA"].ToString(),
                    TRA_CEDULA = e["TRA_CEDULA"].ToString(),
                    TRA_NOMBRE = e["TRA_NOMBRE"].ToString(),
                    TRA_TELEFONO = e["TRA_TELEFONO"].ToString()
                });
            }
            return Ok(_Conductores);
        }

        #endregion


        #region TAB TOTALES

            /// <summary>
            /// Lista los totales de la factura
            /// </summary>
            /// <param name="factura"></param>
            /// <returns></returns>
            [Route("api/TX_FACTURAS_TB/GetTotales")]
            [ResponseType(typeof(LV_Facturacion))]
            public IHttpActionResult GetTotales(long factura)
            {
                //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_TOTAL_RUBROS_FACT_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("factura_p", OracleDbType.Int32).Value = factura;
                objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<LV_Facturacion> _Totales = new List<LV_Facturacion>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();

                foreach (DataRow e in datos2.Rows)
                {
                    _Totales.Add(new LV_Facturacion
                    {
                        Id = e["FTT_TEXTO"].ToString(),
                        Nombre = e["FTT_TEXTO"].ToString()
                    });
                }
                return Ok(_Totales);
            }

        #endregion

        #region TAB TOTALES TARIFA

        /// <summary>
        /// Lista los totales de la factura por tarifa
        /// </summary>
        /// <param name="factura"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetTotalesTarifa")]
        [ResponseType(typeof(LV_Facturacion))]
        public IHttpActionResult GetTotalesTarifa(long factura)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_TOTAL_TARIFAS_FACT_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("factura_p", OracleDbType.Int32).Value = factura;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<LV_Facturacion> _Totales = new List<LV_Facturacion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Totales.Add(new LV_Facturacion
                {
                    Id = e["FTT_TEXTO"].ToString(),
                    Nombre = e["FTT_TEXTO"].ToString()
                });
            }
            return Ok(_Totales);
        }

        #endregion

        #region "Calcular y Facturar"

        /// <summary>
        /// Obtiene los cálculos
        /// </summary>
        /// <param name="factura"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/GetCalculos")]
        [ResponseType(typeof(FAC_CALCULOS))]
        public IHttpActionResult GetCalculos(long factura)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_CALCULA_TOTALES_FACT_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("factura_p", OracleDbType.Int32).Value = factura;
            objCmd.Parameters.Add("adicionales_p", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("descuento_p", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("subtotal_p", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("iva_p", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objCmd.Parameters.Add("total_p", OracleDbType.Int32).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            

            var adic = objCmd.Parameters["adicionales_p"].Value.ToString();
            var desc = objCmd.Parameters["descuento_p"].Value.ToString();
            var sub = objCmd.Parameters["subtotal_p"].Value.ToString();
            var iva = objCmd.Parameters["iva_p"].Value.ToString();
            var total = objCmd.Parameters["total_p"].Value.ToString();


            cn.Close();



            List<FAC_CALCULOS> _Calculos = new List<FAC_CALCULOS>();
            FAC_CALCULOS _Calculo = new FAC_CALCULOS();
            _Calculo.ADICIONALES = decimal.Parse(adic);
            _Calculo.DESCUENTO = decimal.Parse(desc);
            _Calculo.SUBTOTAL = decimal.Parse(sub);
            _Calculo.IVA = decimal.Parse(iva);
            _Calculo.TOTAL = decimal.Parse(total);

            _Calculos.Add(_Calculo);

            return Ok(_Calculos);
        }

        /// <summary>
        /// Actualiza datos de encabezado y factura
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/ActualizarEncabezado")]
        [ResponseType(typeof(string))]
        public IHttpActionResult PostTX_FACTURAS_TB_Act(FAC_ENCABEZADO post)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_FACTURA_ENCAB_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("FAC_FACTURA_P", OracleDbType.Varchar2).Value = post.FAC_FACTURA;
            objCmd.Parameters.Add("FAC_TRA_CHOFER_P", OracleDbType.Int32).Value = post.FAC_TRA_CHOFER;
            objCmd.Parameters.Add("FAC_NOMBRE_CHOFER_P", OracleDbType.Varchar2).Value = post.FAC_NOMBRE_CHOFER;
            objCmd.Parameters.Add("FAC_CEDULA_CHOFER_P", OracleDbType.Varchar2).Value = post.FAC_CEDULA_CHOFER;
            objCmd.Parameters.Add("FAC_PLACA_VEHICULO_P", OracleDbType.Varchar2).Value = post.FAC_PLACA_VEHICULO;
            objCmd.Parameters.Add("FAC_CFR_FACTURA_P", OracleDbType.Int32).Value = post.FAC_CFR_FACTURA;
            objCmd.Parameters.Add("FAC_NOMBRE_FACTURA_P", OracleDbType.Varchar2).Value = post.FAC_NOMBRE_FACTURA;
            objCmd.Parameters.Add("FAC_CEDULA_FACTURA_P", OracleDbType.Varchar2).Value = post.FAC_CEDULA_FACTURA;
            objCmd.Parameters.Add("FAC_TELEFONO_P", OracleDbType.Varchar2).Value = post.FAC_TELEFONO;
            objCmd.Parameters.Add("FAC_MARCHAMO1_P", OracleDbType.Varchar2).Value = post.FAC_MARCHAMO1;
            objCmd.Parameters.Add("FAC_MARCHAMO2_P", OracleDbType.Varchar2).Value = post.FAC_MARCHAMO2;
            objCmd.Parameters.Add("FAC_MARCHAMO3_P", OracleDbType.Varchar2).Value = post.FAC_MARCHAMO3;
            objCmd.Parameters.Add("FAC_MARCHAMO4_P", OracleDbType.Varchar2).Value = post.FAC_MARCHAMO4;
            objCmd.Parameters.Add("FAC_OBSERVACIONES_P", OracleDbType.Varchar2).Value = post.FAC_OBSERVACIONES;
            objCmd.Parameters.Add("FAC_AUTORIZADO_POR_P", OracleDbType.Varchar2).Value = post.FAC_AUTORIZADO_POR;
            objCmd.Parameters.Add("FAC_USUARIO_P", OracleDbType.Varchar2).Value = post.FAC_USU_DIGITO;

            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok("Facturó Correctamente");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion

        #region "TIQUETE ATENCIÓN SALA"

        /// <summary>
        /// Obtiene el siguiente tiquete para el usuario logueado
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [Route("api/TX_FACTURAS_TB/Tiquete")]
        [ResponseType(typeof(LV_Facturacion))]
        public IHttpActionResult GetTiquete()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_TIQ_SALA_WEB_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("proceso_p", OracleDbType.Varchar2).Value = "F";
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<LV_Facturacion> _Tiquetes = new List<LV_Facturacion>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();

                foreach (DataRow e in datos2.Rows)
                {
                    _Tiquetes.Add(new LV_Facturacion
                    {
                        Id = e["ATS_TIQUETE"].ToString(),
                        Nombre = e["ATS_TIPO_COLA"].ToString()
                    });
                }
                return Ok(_Tiquetes);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }          
        }


        [Route("api/TX_FACTURAS_TB/CANCELA_TIQUETE")]
        [ResponseType(typeof(string))]
        public IHttpActionResult Post_CANCELAR_TIQ(long id_p, string estado_p, string tipo_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_TIQ_SALA_WEB_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ats_id_p", OracleDbType.Int32).Value = id_p;
            objCmd.Parameters.Add("ats_estado_p", OracleDbType.Varchar2).Value = estado_p;
            objCmd.Parameters.Add("ats_tipo_cola_p ", OracleDbType.Varchar2).Value = tipo_p;
            try
            {
                objCmd.ExecuteNonQuery();              
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();
            return Ok("Se canceló el tiquete");
        }



        #endregion
    }
}