﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_AGENTES_ADUANA_TBController : ApiController
    {

        [Route("api/TX_AGENTES_ADUANA_TB/GetAgentesAduana")]
        [ResponseType(typeof(AgenteAduana))]
        public IHttpActionResult GetAgentesAduana(long taa_aga_nagencia_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_agente_adua_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("taa_aga_nagencia_p", OracleDbType.Varchar2).Value = taa_aga_nagencia_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<AgenteAduana> _AgenteAduana = new List<AgenteAduana>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _AgenteAduana.Add(new AgenteAduana
                {
                    TAA_AGA_NAGENCIA = long.Parse(e["TAA_AGA_NAGENCIA"].ToString()),
                    TAA_CEDULA = long.Parse(e["TAA_CEDULA"].ToString()),
                    TAA_NOMBRE = e["TAA_NOMBRE"].ToString(),
                    TAA_TIPO_AGENTE = e["TAA_TIPO_AGENTE"].ToString(),
                    TAA_CODIGO = long.Parse(e["TAA_CODIGO"].ToString()),
                    TAA_ESTADO = e["TAA_ESTADO"].ToString()
                });
            }
            return Ok(_AgenteAduana);
        }

        [Route("api/TX_AGENTES_ADUANA_TB/GetAgentesAduanalDetalle")]
        [ResponseType(typeof(AgenteAduana))]
        public IHttpActionResult GetAgentesAduanalDetalle(long taa_aga_nagencia_p, long taa_codigo_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_DET_AGENTE_ADUA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("taa_aga_nagencia_p", OracleDbType.Long).Value = taa_aga_nagencia_p;
            objCmd.Parameters.Add("taa_codigo_p", OracleDbType.Long).Value = taa_codigo_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<AgenteAduana> _AgenteAduana = new List<AgenteAduana>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _AgenteAduana.Add(new AgenteAduana
                {
                    TAA_AGA_NAGENCIA = long.Parse(e["TAA_AGA_NAGENCIA"].ToString()),
                    TAA_CEDULA = long.Parse(e["TAA_CEDULA"].ToString()),
                    TAA_NOMBRE = e["TAA_NOMBRE"].ToString(),
                    TAA_TIPO_AGENTE = e["TAA_TIPO_AGENTE"].ToString(),
                    TAA_DIRECCION = e["TAA_DIRECCION"].ToString(),
                    TAA_TELEFONO = e["TAA_TELEFONO"].ToString(),
                    TAA_CODIGO = long.Parse(e["TAA_CODIGO"].ToString()),
                    TAA_ESTADO = e["TAA_ESTADO"].ToString(),
                    TAA_MOTIVO = e["TAA_MOTIVO"].ToString(),//long.Parse(e["TAA_MOTIVO"].ToString()),
                    TAA_FCH_ACTUALIZA = DateTime.Parse(e["TAA_FCH_ACTUALIZA"].ToString()).ToString("dd/MM/yyyy"),
                    TAA_USU_ACTUALIZA = e["TAA_USU_ACTUALIZA"].ToString()
                });
            }
            return Ok(_AgenteAduana);
        }

        [Route("api/TX_AGENTES_ADUANA_TB/GetMotivoEstados")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetMotivoEstados()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_taa_motivo";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Estados> _MotivosEstados = new List<Estados>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _MotivosEstados.Add(new Estados
                {
                    Id = r["id"].ToString(),
                    Nombre = r["nombre"].ToString()
                });
            }

            return Ok(_MotivosEstados);
        }

        [Route("api/TX_AGENTES_ADUANA_TB/GetEstados")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetEstados()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_taa_estado_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Estados> _Estados = new List<Estados>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Estados.Add(new Estados
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Estados);
        }

        [Route("api/TX_AGENTES_ADUANA_TB/GetTipoAgente")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetTipoAgente()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_taa_t_agente_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Estados> _TipoAgente = new List<Estados>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TipoAgente.Add(new Estados
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_TipoAgente);
        }

        [ResponseType(typeof(AgenteAduana))]
        public IHttpActionResult PutTX_AGENTES_ADUANA_TB(string id, AgenteAduana agente_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;

            objCmd.CommandText = "mrx.tx_upd_agente_adua_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("taa_aga_nagencia_p", OracleDbType.Long).Value = agente_p.TAA_AGA_NAGENCIA;
            objCmd.Parameters.Add("taa_codigo_p", OracleDbType.Long).Value = agente_p.TAA_CODIGO;
            objCmd.Parameters.Add("taa_cedula_p", OracleDbType.Long).Value = agente_p.TAA_CEDULA;
            objCmd.Parameters.Add("taa_nombre_p", OracleDbType.Varchar2).Value = agente_p.TAA_NOMBRE;
            objCmd.Parameters.Add("taa_tipo_agente_p", OracleDbType.Varchar2).Value = agente_p.TAA_TIPO_AGENTE;
            objCmd.Parameters.Add("taa_direccion_p", OracleDbType.Varchar2).Value = agente_p.TAA_DIRECCION;
            objCmd.Parameters.Add("taa_telefono_p", OracleDbType.Varchar2).Value = agente_p.TAA_TELEFONO;
            objCmd.Parameters.Add("taa_estado_p", OracleDbType.Varchar2).Value = agente_p.TAA_ESTADO;
            objCmd.Parameters.Add("taa_motivo_p", OracleDbType.Long).Value = agente_p.TAA_MOTIVO;
            objCmd.Parameters.Add("taa_usu_actualiza_p", OracleDbType.Varchar2).Value = agente_p.USER_MERX;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();

            AgenteAduana _AgenteAduanaUP = new AgenteAduana();


            return Ok(_AgenteAduanaUP);

        }

        [ResponseType(typeof(AgenteAduana))]
        public IHttpActionResult PostTX_AGENTES_ADUANA_TB(AgenteAduana agente_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_agente_adua_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("taa_aga_nagencia_p", OracleDbType.Long).Value = agente_p.TAA_AGA_NAGENCIA;
            objCmd.Parameters.Add("taa_cedula_p", OracleDbType.Varchar2).Value = agente_p.TAA_CEDULA;
            objCmd.Parameters.Add("taa_nombre_p", OracleDbType.Varchar2).Value = agente_p.TAA_NOMBRE;
            objCmd.Parameters.Add("taa_tipo_agente_p", OracleDbType.Varchar2).Value = agente_p.TAA_TIPO_AGENTE;
            objCmd.Parameters.Add("taa_direccion_p", OracleDbType.Varchar2).Value = agente_p.TAA_DIRECCION;
            objCmd.Parameters.Add("taa_telefono_p", OracleDbType.Varchar2).Value = agente_p.TAA_TELEFONO;
            objCmd.Parameters.Add("taa_estado_p", OracleDbType.Varchar2).Value = agente_p.TAA_ESTADO;
            objCmd.Parameters.Add("taa_motivo_p", OracleDbType.Varchar2).Value = agente_p.TAA_MOTIVO;
            objCmd.Parameters.Add("taa_usu_actualiza_p", OracleDbType.Varchar2).Value = agente_p.USER_MERX;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            AgenteAduana _AgenteAduanaIn = new AgenteAduana();

            return Ok(_AgenteAduanaIn);
        }

    }
}
