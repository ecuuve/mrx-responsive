﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_CONSOLIDADORES_TBController : ApiController
    {
        /// <summary>
        /// Lista consolidadores (No update)
        /// </summary>
        /// <param name="consolidador"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        [Route("api/TX_CONSOLIDADORES_TB/Consolidadores")]
        [ResponseType(typeof(Consolidador))]
        public IHttpActionResult GetConsolidadores(long? consolidador, string nombre)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONSOLIDADORES_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("consolidador_p", OracleDbType.Long).Value = consolidador;
            objCmd.Parameters.Add("con_nombre_p", OracleDbType.Long).Value = nombre;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Consolidador> _consolidadores = new List<Consolidador>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _consolidadores.Add(new Consolidador
                    {

                        Id = Convert.ToInt32(e["Id"]),
                        Nombre = e["Nombre"].ToString(),
                        Cedula = e["Cedula"].ToString()
                    });
                }
                return Ok(_consolidadores);
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Obtiene los datos del consolidador para actualizar
        /// </summary>
        /// <param name="consolidador"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        [Route("api/TX_CONSOLIDADORES_TB/GetConsolidadores")]
        [ResponseType(typeof(ConsolidadorUpt))]
        public IHttpActionResult GetConsolidadoresUpt(long? consolidador, string nombre)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONSOLIDADOR_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("con_nconsolidador_p", OracleDbType.Long).Value = consolidador;
            objCmd.Parameters.Add("con_nombre_p", OracleDbType.Long).Value = nombre;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<ConsolidadorUpt> _consolidadores = new List<ConsolidadorUpt>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _consolidadores.Add(new ConsolidadorUpt
                    {
                        CON_NCONSOLIDADOR = e["CON_NCONSOLIDADOR"].ToString(),
                        CON_NOMBRE = e["CON_NOMBRE"].ToString(),
                        CON_CEDULA = e["CON_CEDULA"].ToString(),
                        CON_ESTADO_ENTRADA = e["CON_ESTADO_ENTRADA"].ToString(),
                        CON_TELEFONO1 = e["CON_TELEFONO1"].ToString(),
                        CON_TELEFONO2 = e["CON_TELEFONO2"].ToString(),
                        CON_TELEFONO_FAX = e["CON_TELEFONO_FAX"].ToString(),
                        CON_EST_ESTADO = e["CON_EST_ESTADO"].ToString(),
                        CON_APARTADO = e["CON_APARTADO"].ToString(),
                        CON_RECIBE_ESTADOS = e["CON_RECIBE_ESTADOS"].ToString(),
                        CON_EMAIL = e["CON_EMAIL"].ToString(),
                        CON_FECHA_INGRESO = e["CON_FECHA_INGRESO"].AsFormattedDate(),
                        CON_DIRECCION = e["CON_DIRECCION"].ToString(),
                        CON_OBSERVACIONES = e["CON_OBSERVACIONES"].ToString(),
                        CON_MODIFICACIONES = e["CON_MODIFICACIONES"].ToString(),
                        CON_FCH_RIGE = e["CON_FCH_RIGE"].AsFormattedDate(),
                        CON_USU_MODIFICA = e["CON_USU_MODIFICA"].ToString(),
                        CON_FCH_MODIFICA =e["CON_FCH_MODIFICA"].AsFormattedDate()
                    });
                }
                return Ok(_consolidadores);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Actualiza el consolidador
        /// </summary>
        /// <param name="id"></param>
        /// <param name="consolidador_p"></param>
        /// <returns></returns>
        [ResponseType(typeof(ConsolidadorUpt))]
        public IHttpActionResult PutTX_CONSOLIDADORES_TB(string id, ConsolidadorUpt consolidador_p)
        {

            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;

            objCmd.CommandText = "mrx.tx_upd_consolidador_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("con_nconsolidador_p", OracleDbType.Int32).Value = consolidador_p.CON_NCONSOLIDADOR;
            objCmd.Parameters.Add("con_telefono1_p", OracleDbType.Varchar2).Value = consolidador_p.CON_TELEFONO1;
            objCmd.Parameters.Add("con_telefono2_p", OracleDbType.Varchar2).Value = consolidador_p.CON_TELEFONO2;
            objCmd.Parameters.Add("con_telefono_fax_p", OracleDbType.Varchar2).Value = consolidador_p.CON_TELEFONO_FAX;
            objCmd.Parameters.Add("con_apartado_p", OracleDbType.Varchar2).Value = consolidador_p.CON_APARTADO;
            objCmd.Parameters.Add("con_recibe_estados_p", OracleDbType.Varchar2).Value = consolidador_p.CON_RECIBE_ESTADOS;
            objCmd.Parameters.Add("con_email_p", OracleDbType.Varchar2).Value = consolidador_p.CON_EMAIL;
            objCmd.Parameters.Add("con_direccion_p", OracleDbType.Varchar2).Value = consolidador_p.CON_DIRECCION;
            objCmd.Parameters.Add("con_observaciones_p", OracleDbType.Varchar2).Value = consolidador_p.CON_OBSERVACIONES;
            objCmd.Parameters.Add("con_modificaciones_p", OracleDbType.Varchar2).Value = consolidador_p.CON_MODIFICACIONES;
            objCmd.Parameters.Add("con_fch_rige_p", OracleDbType.Varchar2).Value = consolidador_p.CON_FCH_RIGE;
            objCmd.Parameters.Add("con_usu_modifica_p", OracleDbType.Varchar2).Value = consolidador_p.UserMERX;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// Obtiene estados de entrada para select
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_CONSOLIDADORES_TB/EstadosEnt")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetEstadosEnt()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_CON_ESTADO_ENTRADA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Estados> _Estados = new List<Estados>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Estados.Add(new Estados
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Estados);
        }

        /// <summary>
        /// Obtiene estados consolidador para select
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_CONSOLIDADORES_TB/Estados")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetEstados()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_edo_consolidador_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Estados> _Estados = new List<Estados>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Estados.Add(new Estados
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Estados);
        }

        /// <summary>
        /// Obtiene opciones de recibe estados para select
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_CONSOLIDADORES_TB/RecibeEstados")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetRecibeEstados()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_CON_RECIBE_ESTADOS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Estados> _Estados = new List<Estados>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Estados.Add(new Estados
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Estados);
        }

        /// <summary>
        /// Obtiene ubicaciones del consolidador para select
        /// </summary>
        /// <param name="consolidador_p"></param>
        /// <returns></returns>
        [Route("api/TX_CONSOLIDADORES_TB/UbicacionesConsolidador")]
        [ResponseType(typeof(UbicacionConsolidador))]
        public IHttpActionResult GetUbicacionesConsolidador(long consolidador_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONSOLID_UBIC_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("con_nconsolidador_p", OracleDbType.Int32).Value = consolidador_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<UbicacionConsolidador> _Destinos = new List<UbicacionConsolidador>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Destinos.Add(new UbicacionConsolidador
                {
                    COU_CON_NCONSOLIDADOR = r["COU_CON_NCONSOLIDADOR"].ToString(),
                    COU_UBI_UBICACION = r["COU_UBI_UBICACION"].ToString(),
                    UBI_RAZON_SOCIAL = r["UBI_RAZON_SOCIAL"].ToString(),
                    CONSIGNATARIOS = long.Parse(r["CONSIGNATARIOS"].ToString())
                });
            }

            return Ok(_Destinos);
        }

        /// <summary>
        /// Obtiene las ubicaciones o destinos
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_CONSOLIDADORES_TB/Ubicaciones")]
        [ResponseType(typeof(Ubicaciones))]
        public IHttpActionResult GetUbicaciones()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_ubicaciones_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("id_ubicacion_p", OracleDbType.Varchar2).Value = "";
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Ubicaciones> _Destinos = new List<Ubicaciones>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Destinos.Add(new Ubicaciones
                {
                    Id = r["CODIGO"].ToString(),
                    Nombre = r["DESCRIPCION"].ToString()
                });
            }

            return Ok(_Destinos);
        }

        /// <summary>
        /// Obtiene los consignatarios del destino
        /// </summary>
        /// <param name="consolidador_p"></param>
        /// <param name="ubicacion_p"></param>
        /// <returns></returns>
        [Route("api/TX_CONSOLIDADORES_TB/ConsignatariosUbicacion")]
        [ResponseType(typeof(ConsignatarioUbicacion))]
        public IHttpActionResult GetConsignatariosUbicacion(long consolidador_p, string ubicacion_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONSIG_CONSOLID_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("cou_con_nconsolidador_p", OracleDbType.Int32).Value = consolidador_p;
            objCmd.Parameters.Add("cou_ubi_ubicacion_p", OracleDbType.Varchar2).Value = ubicacion_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<ConsignatarioUbicacion> _Consignatarios = new List<ConsignatarioUbicacion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Consignatarios.Add(new ConsignatarioUbicacion
                {
                    COC_ID = r["COC_ID"].ToString(),
                    COC_CON_NCONSOLIDADOR = r["COC_CON_NCONSOLIDADOR"].ToString(),
                    COC_UBI_UBICACION = r["COC_UBI_UBICACION"].ToString(),
                    COC_CONSIGNATARIO = r["COC_CONSIGNATARIO"].ToString()
                });
            }

            return Ok(_Consignatarios);
        }

        /// <summary>
        /// Agrega destino al consolidador
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ubi"></param>
        /// <returns></returns>
        [Route("api/TX_CONSOLIDADORES_TB/PostDestino")]
        public IHttpActionResult PostDestino(long id, string ubi)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_consolid_ubic_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("con_nconsolidador_p", OracleDbType.Int32).Value = id;
            objCmd.Parameters.Add("ubi_ubicacion_p", OracleDbType.Varchar2).Value = ubi;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


            cn.Close();

            return Ok();
        }

        /// <summary>
        /// Agrega consignatario al destino
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ubi"></param>
        /// <param name="consig"></param>
        /// <returns></returns>
        [Route("api/TX_CONSOLIDADORES_TB/PostConsig")]
        public IHttpActionResult PostConsig(long id, string ubi, string consig)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_CONSIG_CONSOLID_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("con_nconsolidador_p", OracleDbType.Int32).Value = id;
            objCmd.Parameters.Add("ubi_ubicacion_p", OracleDbType.Varchar2).Value = ubi;
            objCmd.Parameters.Add("coc_consignatario_p", OracleDbType.Varchar2).Value = consig;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();

            return Ok();
        }

        /// <summary>
        /// Elimina el destino del consolidador
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ubi"></param>
        /// <returns></returns>
        [Route("api/TX_CONSOLIDADORES_TB/DeleteDestino")]
        public IHttpActionResult DeleteDestino(long id, string ubi)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_del_consolid_ubic_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("con_nconsolidador_p", OracleDbType.Int32).Value = id;
            objCmd.Parameters.Add("ubi_ubicacion_p", OracleDbType.Varchar2).Value = ubi;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();
            
            return Ok();
        }

        /// <summary>
        /// Elimina el consignatario del destino
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/TX_CONSOLIDADORES_TB/DeleteConsignatario")]
        public IHttpActionResult DeleteConsignatario(long id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_del_consig_consolid_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("coc_id_p", OracleDbType.Int32).Value = id;
            //objCmd.Parameters.Add("ubi_ubicacion_p", OracleDbType.Varchar2).Value = ubi;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            cn.Close();

            return Ok();
        }

    }
}
