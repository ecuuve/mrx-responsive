﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_CONTROL_ANDEN_TBController : ApiController
    {

        #region Carga de LV

        /// <summary>
        /// Lista los motivos de anulación
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_CONTROL_ANDEN_TB/GetMotivosAnula")]
        [ResponseType(typeof(LV_ControlAnden))]
        public IHttpActionResult GetMotivosAnula()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_MOT_ANUL_ANDEN_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<LV_ControlAnden> _Motivos = new List<LV_ControlAnden>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Motivos.Add(new LV_ControlAnden
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Motivos);
        }

        /// <summary>
        /// Lista los tipos de camión
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_CONTROL_ANDEN_TB/GetTiposCamion")]
        [ResponseType(typeof(LV_ControlAnden))]
        public IHttpActionResult GetTiposCamion()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_TIPO_CAMION_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<LV_ControlAnden> _TiposCamion = new List<LV_ControlAnden>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TiposCamion.Add(new LV_ControlAnden
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_TiposCamion);
        }

        /// <summary>
        /// Lista los controles de anden
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_CONTROL_ANDEN_TB/GetControlAndenLV")]
        [ResponseType(typeof(LV_ControlAnden))]
        public IHttpActionResult GetControlAndenLV()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_CONTROL_ANDEN_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<LV_ControlAnden> _ControlAnden = new List<LV_ControlAnden>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _ControlAnden.Add(new LV_ControlAnden
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_ControlAnden);
        }

        /// <summary>
        /// Lista las colas
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_CONTROL_ANDEN_TB/GetColas")]
        [ResponseType(typeof(LV_ControlAnden))]
        public IHttpActionResult GetColas()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_COLA_CONTROL_ANDEN_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<LV_ControlAnden> _Colas = new List<LV_ControlAnden>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Colas.Add(new LV_ControlAnden
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Colas);
        }

        #endregion

        #region "Control Anden GET"

        /// <summary>
        /// Obtiene la lista de facturas pendientes 
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_CONTROL_ANDEN_TB/GetFactPendientes")]
        [ResponseType(typeof(CAD_FAC_Detalle))]
        public IHttpActionResult GetFactPendientes()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_FAC_CONTROL_ANDEN_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;         
            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<CAD_FAC_Detalle> _FACSControlAnden = new List<CAD_FAC_Detalle>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _FACSControlAnden.Add(new CAD_FAC_Detalle
                {
                    FAC_NOMBRE_CUENTA = e["FAC_NOMBRE_CUENTA"].ToString(),
                    FAC_AGA_NAGENCIA_ADUANAL = e["FAC_AGA_NAGENCIA_ADUANAL"].ToString(),
                    FAC_CEDULA_CHOFER = e["FAC_CEDULA_CHOFER"].ToString(),
                    FAC_CEDULA_CONTRIB = e["FAC_CEDULA_CONTRIB"].ToString(),
                    FAC_CEDULA_FACTURA = e["FAC_CEDULA_FACTURA"].ToString(),
                    FAC_CONTRIBUYENTE = e["FAC_CONTRIBUYENTE"].ToString(),
                    FAC_CUENTA = e["FAC_CUENTA"].ToString(),
                    FAC_FACTURA = e["FAC_FACTURA"].ToString(),
                    FAC_NBR_CONTRIBUYENTE = e["FAC_NBR_CONTRIBUYENTE"].ToString(),
                    FAC_NOMBRE_CHOFER = e["FAC_NOMBRE_CHOFER"].ToString(),
                    FAC_NOMBRE_FACTURA = e["FAC_NOMBRE_FACTURA"].ToString(),
                    FAC_PLACA_VEHICULO = e["FAC_PLACA_VEHICULO"].ToString(),
                    AGA_NOMBRE = e["AGA_NOMBRE"].ToString()
                });
            }
            return Ok(_FACSControlAnden);
        }

        /// <summary>
        /// Obtiene facturas por placa
        /// </summary>
        /// <param name="placa_p"></param>
        /// <returns></returns>
        [Route("api/TX_CONTROL_ANDEN_TB/GetFactPorPlaca")]
        [ResponseType(typeof(FACXPLACA))]
        public IHttpActionResult GetFactPorPlaca(string placa_p, string cliente_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_FAC_X_PLACA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("placa_p", OracleDbType.Varchar2).Value = placa_p;
            objCmd.Parameters.Add("cliente_id", OracleDbType.Varchar2).Value = cliente_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<FACXPLACA> _FACSPlaca = new List<FACXPLACA>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _FACSPlaca.Add(new FACXPLACA
                {
                    FACTURA = e["FACTURA"].ToString(),
                    FAC_AGA_NAGENCIA_ADUANAL = e["FAC_AGA_NAGENCIA_ADUANAL"].ToString(),
                    AGA_NOMBRE = e["AGA_NOMBRE"].ToString(),
                    CHOFER = e["CHOFER"].ToString(),
                    FAC_CEDULA_CHOFER = e["FAC_CEDULA_CHOFER"].ToString(),
                    PLACA = e["PLACA"].ToString(),
                    CLIENTE = e["CLIENTE"].ToString(),
                    BLT_TOTALES = e["BLT_TOTALES"].ToString(),
                    KG_TOTALES = e["KG_TOTALES"].ToString()
                });
            }
            return Ok(_FACSPlaca);
        }

        /// <summary>
        /// Obtiene la lista de facturas de cada andén
        /// </summary>
        /// <param name="anden_p"></param>
        /// <param name="cola_p"></param>
        /// <returns></returns>
        [Route("api/TX_CONTROL_ANDEN_TB/GetControlAnden")]
        [ResponseType(typeof(Control_Anden))]
        public IHttpActionResult GetControlAnden(string anden_p, long cola_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONTROL_ANDEN_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("ANDEN_P", OracleDbType.Varchar2).Value = anden_p;
            objCmd.Parameters.Add("COLA_P", OracleDbType.Varchar2).Value = cola_p;           
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Control_Anden> _Lista = new List<Control_Anden>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Lista.Add(new Control_Anden
                {
                    CLIENTE = e["CLIENTE"].ToString(),
                    CLIENTE_ID = e["CLIENTE_ID"].ToString(),
                    PLACA = e["PLACA"].ToString(),
                    TIPO_CAMION = e["TIPO_CAMION"].ToString(),
                    TIPO_CAMION_COD = e["TIPO_CAMION_COD"].ToString(),
                    TELEFONO = e["TELEFONO"].ToString(),
                    POSICION = e["POSICION"].ToString(),
                    ACCION = e["ACCION"].ToString(),
                    ACCION_COD = e["ACCION_COD"].ToString(),
                    FACTURAS = e["FACTURAS"].ToString()                                
                });
            }

            foreach (Control_Anden cola in _Lista)
            {
                switch (cola.ACCION_COD)
                {
                    case "4":
                        cola.mostrarboton = false;
                        break;
                    default:
                        cola.mostrarboton = true;
                        break;
                }

            }
            return Ok(_Lista);
        }



        #endregion

        #region "Control Anden POST PUT"

        /// <summary>
        /// Inserta registro de control anden
        /// </summary>
        /// <param name="factura_p"></param>
        /// <param name="anden_p"></param>
        /// <param name="user_MERX"></param>
        /// <returns></returns>
        [Route("api/TX_CONTROL_ANDEN_TB/InsertaControlAnden")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult PostTX_CONTROL_ANDEN_TB(long factura_p, string anden_p, string user_MERX )
        {
            bool inserta;
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_FACT_CONTROL_ANDEN_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("factura_p", OracleDbType.Int32).Value = factura_p;
            objCmd.Parameters.Add("anden_p", OracleDbType.Varchar2).Value = anden_p;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = user_MERX;

            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                inserta = true;
                return Ok(inserta);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }         
        }

        /// <summary>
        /// Actualiza el control anden
        /// </summary>
        /// <param name="placa_p"></param>
        /// <param name="accion_p"></param>
        /// <param name="tipo_camion_p"></param>
        /// <param name="userMERX_p"></param>
        /// <returns></returns>
        [Route("api/TX_CONTROL_ANDEN_TB/ActualizaControlAnden")]
        public IHttpActionResult PostActualizaControlAnden(string placa_p, long accion_p, string tipo_camion_p, string userMERX_p)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_CONTROL_ANDEN_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("placa_p", OracleDbType.Varchar2).Value = placa_p;
            objCmd.Parameters.Add("accion_p", OracleDbType.Int32).Value = accion_p;
            objCmd.Parameters.Add("tipo_camion_p", OracleDbType.Varchar2).Value = tipo_camion_p;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = userMERX_p;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
            cn.Close();
            return Ok("Control Anden actualizado correctamente.");
        }

        /// <summary>
        /// Actualiza el # de teléfono
        /// </summary>
        /// <param name="placa_p"></param>
        /// <param name="telefono_p"></param>
        /// <param name="userMERX_p"></param>
        /// <returns></returns>
        [Route("api/TX_CONTROL_ANDEN_TB/ActualizaTelefonoControlAnden")]
        public IHttpActionResult PostActualizaTelefonoControlAnden(string placa_p, string telefono_p, string userMERX_p)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_TEL_CONTROL_ANDEN_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("placa_p", OracleDbType.Varchar2).Value = placa_p;
            objCmd.Parameters.Add("TELEFONO_P", OracleDbType.Varchar2).Value = telefono_p;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = userMERX_p;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            cn.Close();
            return Ok("Teléfono Control Anden actualizado correctamente.");
        }

        /// <summary>
        /// Actualiza el tipo de camión 
        /// </summary>
        /// <param name="placa_p"></param>
        /// <param name="tipo_camion_p"></param>
        /// <param name="userMERX_p"></param>
        /// <returns></returns>
        [Route("api/TX_CONTROL_ANDEN_TB/ActualizaTipoCamion")]
        public IHttpActionResult PostActualizaTipoCamion(string placa_p, string tipo_camion_p, string userMERX_p)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_TIPO_CAMION_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("placa_p", OracleDbType.Varchar2).Value = placa_p;
            objCmd.Parameters.Add("tipo_camion_p", OracleDbType.Varchar2).Value = tipo_camion_p;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = userMERX_p;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            cn.Close();
            return Ok("Tipo camión actualizado correctamente.");
        }

        /// <summary>
        /// Actualiza la posición del camión en el anden.
        /// </summary>
        /// <param name="placa_p"></param>
        /// <param name="posicion_p"></param>
        /// <param name="userMERX_p"></param>
        /// <returns></returns>
        [Route("api/TX_CONTROL_ANDEN_TB/ActualizaPosicCamionAnden")]
        public IHttpActionResult PostActualizaPosicCamionAnden(string placa_p, long posicion_p, string userMERX_p)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_ANDEN_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("placa_p", OracleDbType.Varchar2).Value = placa_p;
            objCmd.Parameters.Add("posicion_p", OracleDbType.Int32).Value = posicion_p;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = userMERX_p;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            cn.Close();
            return Ok("Ubicación de camión actualizada correctamente.");
        }

        [Route("api/TX_CONTROL_ANDEN_TB/Anular")]
        public IHttpActionResult PostAnular(string placa_p, long mot_anulacion_p, string userMERX_p)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_ANUL_CONTROL_ANDEN_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("placa_p", OracleDbType.Varchar2).Value = placa_p;
            objCmd.Parameters.Add("mot_anulacion_p", OracleDbType.Varchar2).Value = mot_anulacion_p;
            //objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = userMERX_p;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            cn.Close();
            return Ok("Anulación Exitosa");
        }
        #endregion
    }
}