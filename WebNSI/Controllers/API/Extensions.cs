﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace WebNSI.Controllers.API
{
    public static class Extensions
    {
        public static DateTime? AsDateTimeNullable(this object item, DateTime? defaultDateTime = null)
        {
            if (item == null || string.IsNullOrEmpty(item.ToString()))
                return null;

            DateTime result;
            if (!DateTime.TryParse(item.ToString(), out result))
                return defaultDateTime;

            return result;
        }

        public static string AsFormattedDate(this object item, string defaultFormattedDate = "")
        {
            if (item == null || string.IsNullOrEmpty(item.ToString()))
                return null;

            DateTime result;
            if (!DateTime.TryParse(item.ToString(), out result))
                return defaultFormattedDate;

            return result.ToString("dd/MM/yyyy");
        }

        public static int AsInt(this object item, int defaultInt = 0)
        {
            int value = defaultInt;

            if (item != null && !int.TryParse(item.ToString(), out value))
            {
                value = defaultInt;
            }
            return value;
        }

        public static decimal AsDecimal(this object item, decimal defaultDecimal = 0)
        {
            decimal value = defaultDecimal;

            if (item != null && !decimal.TryParse(item.ToString(), out value))
            {
                value = defaultDecimal;
            }
            return value;
        }
        public static string DataRowToJson(DataRow datarow)
        {
            var dict = new Dictionary<string, string>();
            foreach (DataColumn col in datarow.Table.Columns)
            {
                dict.Add(col.ColumnName, datarow[col].ToString());
            }

            var jsSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            return jsSerializer.Serialize(dict);
        }
    }
}