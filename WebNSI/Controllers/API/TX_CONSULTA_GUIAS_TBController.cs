﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_CONSULTA_GUIAS_TBController : ApiController
    {
        [Route("api/TX_CONSULTA_GUIAS_TB/BuscarGuia")]
        public IHttpActionResult BuscarGuia([FromBody] BuscarGuiaRequest input)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_con_guias_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            //ien_entrada_p IN number,  ien_guia_original_p IN varchar2, ien_con_nconsolidador_p IN number, ien_aerolinea_p IN number, recordset_p OUT sys_refcursor
            objCmd.Parameters.Add("ien_entrada_p", OracleDbType.Int32).Value = input.ien_entrada_p;
            objCmd.Parameters.Add("ien_guia_original_p", OracleDbType.Varchar2).Value = input.ien_guia_original_p;
            objCmd.Parameters.Add("ien_con_nconsolidador_p", OracleDbType.Int32).Value = input.ien_con_nconsolidador_p;
            objCmd.Parameters.Add("ien_aerolinea_p", OracleDbType.Int32).Value = input.ien_aerolinea_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }


            DataTable dt = new DataTable();
            CONSULTA_GUIAS data = new CONSULTA_GUIAS();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(dt);
            cn.Close();

            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];

                data = new CONSULTA_GUIAS()
                {
                    ien_entrada = row["ien_entrada"].ToString(),
                    ien_manifiesto = row["ien_manifiesto"].ToString(),
                    ien_guia = row["ien_guia"].ToString(),
                    ien_guia_original = row["ien_guia_original"].ToString(),
                    ien_con_nconsolidador = row["ien_con_nconsolidador"].ToString(),
                    ien_guia_master = row["ien_guia_master"].ToString(),
                    ien_imp_cedula = row["ien_imp_cedula"].ToString(),
                    ien_importador = row["ien_importador"].ToString(),
                    ien_estado = row["ien_estado"].ToString(),
                    ien_fch_digitacion = row["ien_fch_digitacion"].ToString(),
                    ien_fch_manifiesto = row["ien_fch_manifiesto"].ToString(),
                    ien_fch_ingreso = row["ien_fch_ingreso"].ToString(),
                    ien_bod_nbodega = row["ien_bod_nbodega"].ToString(),
                    BODEGA = row["BODEGA"].ToString(),
                    ien_fun_funcionario = row["ien_fun_funcionario"].ToString(),
                    FUNCIONARIO = row["FUNCIONARIO"].ToString(),
                    ien_aer_naerolinea = row["ien_aer_naerolinea"].ToString(),
                    AEROLINEA = row["AEROLINEA"].ToString(),
                    ien_pto_puerto = row["ien_pto_puerto"].ToString(),
                    COD_PUERTO = row["COD_PUERTO"].ToString(),
                    PUERTO = row["PUERTO"].ToString(),
                    ien_tdo_doc_hija = row["ien_tdo_doc_hija"].ToString(),
                    DOC_HIJO = row["DOC_HIJO"].ToString(),
                    ien_din_tipo_documento = row["ien_din_tipo_documento"].ToString(),
                    DESDOC = row["DESDOC"].ToString(),
                    ien_aduana_recibe = row["ien_aduana_recibe"].ToString(),
                    ADUANA_RECIBE = row["ADUANA_RECIBE"].ToString(),
                    ien_usuario_digito = row["ien_usuario_digito"].ToString(),
                    NOMUSUARIODIGITO = row["NOMUSUARIODIGITO"].ToString(),
                    ien_bultos_manifestados = row["ien_bultos_manifestados"].ToString(),
                    ien_bultos_realingreso = row["ien_bultos_realingreso"].ToString(),
                    BULTOS_DESPALETIZADOS = row["BULTOS_DESPALETIZADOS"].ToString(),
                    BULTOS_RESERVADOS = row["BULTOS_RESERVADOS"].ToString(),
                    BULTOS_SALIDOS = row["BULTOS_SALIDOS"].ToString(),
                    ien_kilos_manifestados = row["ien_kilos_manifestados"].ToString(),
                    MUESTRAS = row["MUESTRAS"].ToString(),
                    KILOS_DESPALETIZADOS = row["KILOS_DESPALETIZADOS"].ToString(),
                    KILOS_RESERVADOS = row["KILOS_RESERVADOS"].ToString(),
                    KILOS_SALIDOS = row["KILOS_SALIDOS"].ToString(),
                    ien_descripcion_manif = row["ien_descripcion_manif"].ToString(),
                    ien_antecesor = row["ien_antecesor"].ToString(),
                    ien_numero_viaje = row["ien_numero_viaje"].ToString(),
                    ien_observaciones = row["ien_observaciones"].ToString(),
                    ien_clase = row["ien_clase"].ToString(),
                    ien_boleta_clase = row["ien_boleta_clase"].ToString(),
                    ien_ubicacion_transito = row["ien_ubicacion_transito"].ToString(),
                    UBI_TRANSITO = row["UBI_TRANSITO"].ToString(),
                    ien_tipo_duda_ref = row["ien_tipo_duda_ref"].ToString(),
                    INDICACION_DESC = row["INDICACION_DESC"].ToString(),
                    ien_duda_refri = row["ien_duda_refri"].ToString(),
                    DES_DUDA_REFRI = row["DES_DUDA_REFRI"].ToString(),
                    ien_bloqueada = row["ien_bloqueada"].ToString(),
                    ien_es_courier = row["ien_es_courier"].ToString(),
                };
            }
            return Ok(data);
        }

        [Route("api/TX_CONSULTA_GUIAS_TB/BuscarMercancias")]
        public IHttpActionResult BuscarMercancias([FromBody] MercanciasRequest input)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_cg_mercancias_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            //ien_entrada_p IN number,  ien_guia_original_p IN varchar2, ien_con_nconsolidador_p IN number, ien_aerolinea_p IN number, recordset_p OUT sys_refcursor
            objCmd.Parameters.Add("mer_ien_entrada_p", OracleDbType.Int32).Value = input.mer_ien_entrada_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }


            DataTable dt = new DataTable();
            MERCANCIAS_DETALLE data = new MERCANCIAS_DETALLE();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(dt);
            cn.Close();

            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                data = new MERCANCIAS_DETALLE()
                {
                    mer_id_mercancia = row["mer_id_mercancia"].ToString(),
                    mer_tem_embalaje = row["mer_tem_embalaje"].ToString(),
                    EMBALAJE = row["EMBALAJE"].ToString(),
                    mer_tme_mercancia = row["mer_tme_mercancia"].ToString(),
                    TIPO_MERCANCIA = row["TIPO_MERCANCIA"].ToString(),
                    mer_fch_digitacion = row["mer_fch_digitacion"].ToString(),
                    mer_ave_averia = row["mer_ave_averia"].ToString(),
                    AVERIA = row["AVERIA"].ToString(),
                    mer_imp_cedula = row["mer_imp_cedula"].ToString(),
                    NOMIMPORTADOR = row["NOMIMPORTADOR"].ToString(),
                    mer_fch_transmision = row["mer_fch_transmision"].ToString(),
                    mer_archivo_transmision = row["mer_archivo_transmision"].ToString(),
                    mer_usuario_transmision = row["mer_usuario_transmision"].ToString(),
                    mer_error = row["mer_error"].ToString(),
                    mer_operacion_tica = row["mer_operacion_tica"].ToString(),
                    DES_OPER_TICA = row["DES_OPER_TICA"].ToString(),
                    mer_accion = row["mer_accion"].ToString(),
                    DES_MER_ACCION = row["DES_MER_ACCION"].ToString(),
                    mer_transmision = row["mer_transmision"].ToString(),
                    DES_MER_TRANSMI = row["DES_MER_TRANSMI"].ToString(),
                    mer_id_mercancia_ref = row["mer_id_mercancia_ref"].ToString(),
                    mer_bultos_ref = row["mer_bultos_ref"].ToString(),
                    mer_armas = row["mer_armas"].ToString(),
                    mer_tip_codigo_iata = row["mer_tip_codigo_iata"].ToString(),
                    mer_pel_peligrosa = row["mer_pel_peligrosa"].ToString(),
                    PELIGROSIDAD = row["PELIGROSIDAD"].ToString(),
                    DESC_PELIG_IATA = row["DESC_PELIG_IATA"].ToString(),
                    mer_codigo_un = row["mer_codigo_un"].ToString(),
                    mer_indice_trans = row["mer_indice_trans"].ToString(),
                    mer_descripcion = row["mer_descripcion"].ToString(),
                    mer_refrigerado = row["mer_refrigerado"].ToString(),
                    mer_perecedero = row["mer_perecedero"].ToString(),
                    mer_loc_rack = row["mer_loc_rack"].ToString(),
                    mer_loc_columna = row["mer_loc_columna"].ToString(),
                    mer_loc_lado = row["mer_loc_lado"].ToString(),
                    mer_loc_altura = row["mer_loc_altura"].ToString(),
                    mer_observaciones = row["mer_observaciones"].ToString(),
                    mer_ter_error = row["mer_ter_error"].ToString(),
                    ERROR_TRANSMISION = row["ERROR_TRANSMISION"].ToString(),
                };
            }

            return Ok(data);
        }

        [Route("api/TX_CONSULTA_GUIAS_TB/BuscarMercanciaDetalle")]
        public IHttpActionResult BuscarMercanciaDetalle([FromBody] MercanciasDetalleRequest input)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_cg_det_merca_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            //ien_entrada_p IN number,  ien_guia_original_p IN varchar2, ien_con_nconsolidador_p IN number, ien_aerolinea_p IN number, recordset_p OUT sys_refcursor
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Int32).Value = input.mer_id_mercancia_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }


            DataTable dt = new DataTable();
            MERCANCIAS_DETALLE data = new MERCANCIAS_DETALLE();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(dt);
            cn.Close();

            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];

                data = new MERCANCIAS_DETALLE()
                {
                    mer_id_mercancia = row["mer_id_mercancia"].ToString(),
                    mer_tem_embalaje = row["mer_tem_embalaje"].ToString(),
                    EMBALAJE = row["EMBALAJE"].ToString(),
                    mer_tme_mercancia = row["mer_tme_mercancia"].ToString(),
                    TIPO_MERCANCIA = row["TIPO_MERCANCIA"].ToString(),
                    mer_fch_digitacion = row["mer_fch_digitacion"].ToString(),
                    mer_ave_averia = row["mer_ave_averia"].ToString(),
                    AVERIA = row["AVERIA"].ToString(),
                    mer_imp_cedula = row["mer_imp_cedula"].ToString(),
                    NOMIMPORTADOR = row["NOMIMPORTADOR"].ToString(),
                    mer_fch_transmision = row["mer_fch_transmision"].ToString(),
                    mer_archivo_transmision = row["mer_archivo_transmision"].ToString(),
                    mer_usuario_transmision = row["mer_usuario_transmision"].ToString(),
                    mer_error = row["mer_error"].ToString(),
                    mer_operacion_tica = row["mer_operacion_tica"].ToString(),
                    DES_OPER_TICA = row["DES_OPER_TICA"].ToString(),
                    mer_accion = row["mer_accion"].ToString(),
                    DES_MER_ACCION = row["DES_MER_ACCION"].ToString(),
                    mer_transmision = row["mer_transmision"].ToString(),
                    DES_MER_TRANSMI = row["DES_MER_TRANSMI"].ToString(),
                    mer_id_mercancia_ref = row["mer_id_mercancia_ref"].ToString(),
                    mer_bultos_ref = row["mer_bultos_ref"].ToString(),
                    mer_armas = row["mer_armas"].ToString(),
                    mer_tip_codigo_iata = row["mer_tip_codigo_iata"].ToString(),
                    mer_pel_peligrosa = row["mer_pel_peligrosa"].ToString(),
                    PELIGROSIDAD = row["PELIGROSIDAD"].ToString(),
                    DESC_PELIG_IATA = row["DESC_PELIG_IATA"].ToString(),
                    mer_codigo_un = row["mer_codigo_un"].ToString(),
                    mer_indice_trans = row["mer_indice_trans"].ToString(),
                    mer_descripcion = row["mer_descripcion"].ToString(),
                    mer_refrigerado = row["mer_refrigerado"].ToString(),
                    mer_perecedero = row["mer_perecedero"].ToString(),
                    mer_loc_rack = row["mer_loc_rack"].ToString(),
                    mer_loc_columna = row["mer_loc_columna"].ToString(),
                    mer_loc_lado = row["mer_loc_lado"].ToString(),
                    mer_loc_altura = row["mer_loc_altura"].ToString(),
                    mer_observaciones = row["mer_observaciones"].ToString(),
                    mer_ter_error = row["mer_ter_error"].ToString(),
                    ERROR_TRANSMISION = row["ERROR_TRANSMISION"].ToString(),
                };
            }
            return Ok(data);
        }
    }
}