﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_PROD_NC_TBController : ApiController
    {
        #region Gets

        /// <summary>
        /// Lista las ubicaciones para producto no conforme existente
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_PROD_NC_TB/GetUbicacion")]
        [ResponseType(typeof(pnc_ubicacion))]
        public IHttpActionResult GetUbicacion()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_pnc_ubica_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<pnc_ubicacion> _ubicaciones = new List<pnc_ubicacion>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {
                    _ubicaciones.Add(new pnc_ubicacion
                    {
                        Id = r["Id"].ToString(),
                        Nombre = r["Nombre"].ToString()
                    });
                }
                return Ok(_ubicaciones);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        /// <summary>
        /// Lista los sistemas para producto no conforme existentes
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_PROD_NC_TB/GetSistema")]
        [ResponseType(typeof(pnc_sistema))]
        public IHttpActionResult GetSistema()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_sistema_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<pnc_sistema> _sistemas = new List<pnc_sistema>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {
                    _sistemas.Add(new pnc_sistema
                    {
                        Id = r["Id"].ToString(),
                        Nombre = r["Nombre"].ToString()
                    });
                }
                return Ok(_sistemas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        /// <summary>
        /// Lista las categorías para producto no conforme existentes
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_PROD_NC_TB/GetCategorias")]
        [ResponseType(typeof(pnc_categoria))]
        public IHttpActionResult GetCategorias()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_pnc_categoria_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<pnc_categoria> _sistemas = new List<pnc_categoria>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {
                    _sistemas.Add(new pnc_categoria
                    {
                        Id = r["Id"].ToString(),
                        Nombre = r["Nombre"].ToString()
                    });
                }
                return Ok(_sistemas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        /// <summary>
        /// Lista los departamentos para producto no conforme existentes
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_PROD_NC_TB/GetDepartamentos")]
        [ResponseType(typeof(pnc_departamentos))]
        public IHttpActionResult GetDepartamentos()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_pnc_dpto_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<pnc_departamentos> _sistemas = new List<pnc_departamentos>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {
                    _sistemas.Add(new pnc_departamentos
                    {
                        Id = r["Id"].ToString(),
                        Nombre = r["Nombre"].ToString()
                    });
                }
                return Ok(_sistemas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        /// <summary>
        /// Lista los tipos de problema para la pantalla producto no conforme existentes en base de datos
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_PROD_NC_TB/GetTipoProblema")]
        [ResponseType(typeof(pnc_tipo_problema))]
        public IHttpActionResult GetTipoProblema()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_pnc_tipo_prob_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<pnc_tipo_problema> _sistemas = new List<pnc_tipo_problema>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {
                    _sistemas.Add(new pnc_tipo_problema
                    {
                        Id = r["Id"].ToString(),
                        Nombre = r["Nombre"].ToString()
                    });
                }
                return Ok(_sistemas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        /// <summary>
        /// Lista los Detectado por, necesarios en la pantalla producto no conforme
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_PROD_NC_TB/GetDetectadoPor")]
        [ResponseType(typeof(pnc_detectadoPor))]
        public IHttpActionResult GetDetectadoPor()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_pnc_tipo_halla_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<pnc_detectadoPor> _sistemas = new List<pnc_detectadoPor>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {
                    _sistemas.Add(new pnc_detectadoPor
                    {
                        Id = r["Id"].ToString(),
                        Nombre = r["Nombre"].ToString()
                    });
                }
                return Ok(_sistemas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        /// <summary>
        /// Lista los eventos hallazgo
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_PROD_NC_TB/GetEventoHallazgo")]
        [ResponseType(typeof(pnc_evento_hallazgo))]
        public IHttpActionResult GetEventoHallazgo()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_pnc_evento_halla_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<pnc_evento_hallazgo> _sistemas = new List<pnc_evento_hallazgo>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {
                    _sistemas.Add(new pnc_evento_hallazgo
                    {
                        Id = r["Id"].ToString(),
                        Nombre = r["Nombre"].ToString()
                    });
                }
                return Ok(_sistemas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        /// <summary>
        /// Lista las opciones del combobox decision cliente
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_PROD_NC_TB/GetDecisionCliente")]
        [ResponseType(typeof(pnc_decision_cliente))]
        public IHttpActionResult GetDecisionCliente()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_pnc_decision_cli_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<pnc_decision_cliente> _sistemas = new List<pnc_decision_cliente>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {
                    _sistemas.Add(new pnc_decision_cliente
                    {
                        Id = r["Id"].ToString(),
                        Nombre = r["Nombre"].ToString()
                    });
                }
                return Ok(_sistemas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [Route("api/TX_PROD_NC_TB/GetEstado")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetEstados(string pantalla)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_PNC_ESTADO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tab_p", OracleDbType.Varchar2).Value = pantalla;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<Estados> _estados = new List<Estados>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);

                cn.Close();
                foreach (DataRow r in datos2.Rows)
                {
                    _estados.Add(new Estados
                    {
                        Id = r["Id"].ToString(),
                        Nombre = r["Nombre"].ToString()
                    });
                }
                return Ok(_estados);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
        
        #endregion

        /// <summary>
        /// Recibe el consecutivo y el sistema y valida que si ya existe un producto no conforme, si existe pregunta si quiere aun asi ingresar otro sino carga guia y cliente.
        /// </summary>
        /// <param name="consecutivo"></param>
        /// <param name="sistema"></param>
        /// <returns></returns>
        [Route("api/TX_PROD_NC_TB/ValidacionConsecutivo")]
        [ResponseType(typeof(pnc_decision_cliente))]
        public IHttpActionResult GetValidacionConsecutivo(long? consecutivo, string sistema)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_EXISTE_PNC_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("consecutivo_p", OracleDbType.Int32).Value = consecutivo;
            objCmd.Parameters.Add("existe_p", OracleDbType.Char,1).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                string existe = objCmd.Parameters["existe_p"].Value.ToString();
                if(existe == "S")
                {
                    return BadRequest("true");
                }
                else
                {
                    return BadRequest("false");
                }               
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Mediante el consecutivo y sistema extrae la guia y el importador o cliente correspondientes a estos.
        /// </summary>
        /// <param name="consecutivo"></param>
        /// <param name="sistema"></param>
        /// <returns></returns>
        [Route("api/TX_PROD_NC_TB/InfoConsecutivo")]
        public IHttpActionResult GetInfosecutivo(long? consecutivo, string sistema)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_PNC_CONSEC_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("consecutivo_p", OracleDbType.Int32).Value = consecutivo;
            objCmd.Parameters.Add("sistema_p", OracleDbType.Varchar2).Value = sistema;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                pnc_consecutivo _consecutivo = new pnc_consecutivo();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _consecutivo.GuiaOriginal = e["IEN_GUIA_ORIGINAL"].ToString();
                    _consecutivo.Importador = e["IEN_IMPORTADOR"].ToString();
                }
                return Ok(_consecutivo);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [Route("api/TX_PROD_NC_TB/Productos")]
        [ResponseType(typeof(ProductoNC))]
        public IHttpActionResult GetProductos(long? identificador, string sistema, long? consecutivo)
        {
            //Get the connection string from the app.config            
           var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_prod_no_confor_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("pnc_ident_p", OracleDbType.Int32).Value = identificador;
            objCmd.Parameters.Add("sistema_p", OracleDbType.Varchar2).Value = sistema == "0" ?  null : sistema;
            objCmd.Parameters.Add("consecutivo_p", OracleDbType.Int32).Value = consecutivo;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<ProductoNC> _productos = new List<ProductoNC>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _productos.Add(new ProductoNC
                    {

                        PNC_IDENTIFICADOR = e["PNC_IDENTIFICADOR"].ToString(),
                        pnC_consecutivo = e["CONSECUTIVO"].ToString(),
                        PNC_CLIENTE = e["PNC_CLIENTE"].ToString(),
                        PNC_USU_REGISTRA = e["PNC_USU_REGISTRA"].ToString(),
                        PNC_FCH_REGISTRA = e["PNC_FCH_REGISTRA"].ToString(),
                        PNC_FECHA = e["PNC_FECHA"].ToString(),
                        PNC_ESTADO = e["PNC_ESTADO"].ToString(),
                        PNC_SISTEMA = e["PNC_SISTEMA"].ToString(),
                        PNC_DEPARTAMENTO = e["PNC_DEPARTAMENTO"].ToString(),
                        PNC_CANT_TARIMAS = e["PNC_CANT_TARIMAS"].ToString(),
                        PNC_TARIMAS_NC = e["PNC_TARIMAS_NC"].ToString(),
                        PNC_DESCRIPCION_NC = e["PNC_DESCRIPCION_NC"].ToString(),
                        PNC_TIPO_HALLAZGO = e["PNC_TIPO_HALLAZGO"].ToString(),
                        PNC_HALLADO_POR = e["PNC_HALLADO_POR"].ToString(),
                        PNC_EVENTO_HALLAZGO = e["PNC_EVENTO_HALLAZGO"].ToString(),
                        PNC_OTRO_EVENTO = e["PNC_OTRO_EVENTO"].ToString(),
                        PNC_DECISION_CLIENTE = e["PNC_DECISION_CLIENTE"].ToString(),
                        PNC_DESC_OTRO_DECISION = e["PNC_DESC_OTRO_DECISION"].ToString(),
                        PNC_REPRESENTANTE = e["PNC_REPRESENTANTE"].ToString(),
                        PNC_TIPO_PROBLEMA = e["PNC_TIPO_PROBLEMA"].ToString(),
                        PNC_AGENCIA = e["PNC_AGENCIA"].ToString(),
                        PNC_OGU_GUIA = e["PNC_OGU_GUIA"].ToString(),
                        PNC_OBSERVACIONES = e["PNC_OBSERVACIONES"].ToString(),
                        PNC_UBICACION =  e["PNC_UBICACION"].ToString() == "" ? "0" : e["PNC_UBICACION"].ToString(),
                        PNC_CATEGORIA = e["PNC_CATEGORIA"].ToString(),
                        GUIA = e["GUIA"].ToString(),
                        PNC_EVID_MANIFIESTO = e["PNC_EVID_MANIFIESTO"].ToString(),
                        PNC_EVID_FACTURA = e["PNC_EVID_FACTURA"].ToString(),
                        PNC_EVID_VIDEO = e["PNC_EVID_VIDEO"].ToString(),
                        PNC_EVID_FOTOS = e["PNC_EVID_FOTOS"].ToString(),
                        PNC_EVID_EXAMEN = e["PNC_EVID_EXAMEN"].ToString(),
                        PNC_OTRO = e["PNC_OTRO"].ToString(),
                        PNC_INVESTIGACION = e["PNC_INVESTIGACION"].ToString(),
                        PNC_CAUSA = e["PNC_CAUSA"].ToString(),
                        PNC_DESCRIP_ACCION = e["PNC_DESCRIP_ACCION"].ToString(),
                        PNC_RESPONSABLE = e["PNC_RESPONSABLE"].ToString(),
                        PNC_FCH_LIMITE = e["PNC_FCH_LIMITE"].ToString(),
                        PNC_EMAIL_NOTIF = e["PNC_EMAIL_NOTIF"].ToString(),
                        PNC_USU_CALIDAD = e["PNC_USU_CALIDAD"].ToString(),
                        PNC_USU_INVESTIGA = e["PNC_USU_INVESTIGA"].ToString(),
                        PNC_FCH_INVESTIGA = e["PNC_FCH_INVESTIGA"].ToString(),
                        PNC_USU_REVISION = e["PNC_USU_REVISION"].ToString(),
                        PNC_FCH_REVISION = e["PNC_FCH_REVISION"].ToString(),
                        PNC_USU_APRUEBA = e["PNC_USU_APRUEBA"].ToString(),
                        PNC_FCH_APRUEBA = e["PNC_FCH_REVISION"].ToString(),
                        PNC_FCH_CALIDAD = e["PNC_FCH_CALIDAD"].ToString()

                    });
                }
                return Ok(_productos);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        /// <summary>
        /// Inserta la información de producto no conforme en la base de datos
        /// </summary>
        /// <param name="productoNC"></param>
        /// <returns></returns>
        [Route("api/TX_PROD_NC_TB/GuardarProductoNC")]
        public IHttpActionResult PostGuardarProductoNC(ProductoNC productoNC)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_prod_no_confor_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            //objCmd.Parameters.Add("PNC_IDENTIFICADOR_P", OracleDbType.Int32).Value = productoNC.PNC_IDENTIFICADOR;
            objCmd.Parameters.Add("PNC_FECHA_P", OracleDbType.Varchar2).Value = productoNC.PNC_FECHA;
            objCmd.Parameters.Add("PNC_ESTADO_P", OracleDbType.Varchar2).Value = productoNC.PNC_ESTADO;
            objCmd.Parameters.Add("PNC_SISTEMA_P", OracleDbType.Varchar2).Value = productoNC.PNC_SISTEMA;
            objCmd.Parameters.Add("pnC_consecutivo_P", OracleDbType.Int32).Value = productoNC.pnC_consecutivo;
            objCmd.Parameters.Add("PNC_DEPARTAMENTO_P", OracleDbType.Varchar2).Value = productoNC.PNC_DEPARTAMENTO;
            objCmd.Parameters.Add("PNC_CANT_TARIMAS_P", OracleDbType.Int32).Value = productoNC.PNC_CANT_TARIMAS;
            objCmd.Parameters.Add("PNC_TARIMAS_NC_P", OracleDbType.Int32).Value = productoNC.PNC_TARIMAS_NC;
            objCmd.Parameters.Add("PNC_DESCRIPCION_NC_P", OracleDbType.Varchar2).Value = productoNC.PNC_DESCRIPCION_NC;
            objCmd.Parameters.Add("PNC_CLIENTE_P", OracleDbType.Varchar2).Value = productoNC.PNC_CLIENTE;
            objCmd.Parameters.Add("PNC_TIPO_HALLAZGO_P", OracleDbType.Varchar2).Value = productoNC.PNC_TIPO_HALLAZGO;
            objCmd.Parameters.Add("PNC_HALLADO_POR_P", OracleDbType.Varchar2).Value = productoNC.PNC_HALLADO_POR;
            objCmd.Parameters.Add("PNC_EVENTO_HALLAZGO_P", OracleDbType.Varchar2).Value = productoNC.PNC_EVENTO_HALLAZGO;
            objCmd.Parameters.Add("PNC_OTRO_EVENTO_P", OracleDbType.Varchar2).Value = productoNC.PNC_OTRO_EVENTO;
            objCmd.Parameters.Add("PNC_DECISION_CLIENTE_P", OracleDbType.Varchar2).Value = productoNC.PNC_DECISION_CLIENTE;
            objCmd.Parameters.Add("PNC_DESC_OTRO_DECISION_P", OracleDbType.Varchar2).Value = productoNC.PNC_DESC_OTRO_DECISION;
            objCmd.Parameters.Add("PNC_REPRESENTANTE_P", OracleDbType.Varchar2).Value = productoNC.PNC_REPRESENTANTE;
            objCmd.Parameters.Add("PNC_USU_REGISTRA_P", OracleDbType.Varchar2).Value = productoNC.PNC_USU_REGISTRA;
            objCmd.Parameters.Add("PNC_TIPO_PROBLEMA_P", OracleDbType.Varchar2).Value = productoNC.PNC_TIPO_PROBLEMA;
            objCmd.Parameters.Add("PNC_AGENCIA_P", OracleDbType.Varchar2).Value = productoNC.PNC_AGENCIA;
            objCmd.Parameters.Add("PNC_OGU_GUIA_P", OracleDbType.Varchar2).Value = productoNC.PNC_OGU_GUIA;
            objCmd.Parameters.Add("PNC_OBSERVACIONES_P", OracleDbType.Varchar2).Value = productoNC.PNC_OBSERVACIONES;
            objCmd.Parameters.Add("PNC_UBICACION_P", OracleDbType.Varchar2).Value = productoNC.PNC_UBICACION;
            objCmd.Parameters.Add("PNC_CATEGORIA_P", OracleDbType.Varchar2).Value = productoNC.PNC_CATEGORIA;
            objCmd.Parameters.Add("PNC_IDENTIFICADOR_P", OracleDbType.Int32).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
                string id = objCmd.Parameters["PNC_IDENTIFICADOR_P"].Value.ToString();
                cn.Close();
                return Ok(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [Route("api/TX_PROD_NC_TB/ActualizaEncabezado")]
        [ResponseType(typeof(ProductoNC))]
        public IHttpActionResult PutTX_PROD_NC_TB(string id, ProductoNC producto_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_upd_prod_no_confor_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("PNC_IDENTIFICADOR_P", OracleDbType.Int32).Value = producto_p.PNC_IDENTIFICADOR;
            objCmd.Parameters.Add("PNC_FECHA_P", OracleDbType.Varchar2).Value = producto_p.PNC_FECHA;
            objCmd.Parameters.Add("PNC_ESTADO_P", OracleDbType.Varchar2).Value = producto_p.PNC_ESTADO;
            objCmd.Parameters.Add("PNC_SISTEMA_P", OracleDbType.Varchar2).Value = producto_p.PNC_SISTEMA;
            objCmd.Parameters.Add("pnC_consecutivo_P", OracleDbType.Int32).Value = producto_p.pnC_consecutivo;
            objCmd.Parameters.Add("PNC_DEPARTAMENTO_P", OracleDbType.Varchar2).Value = producto_p.PNC_DEPARTAMENTO;
            objCmd.Parameters.Add("PNC_CANT_TARIMAS_P", OracleDbType.Int32).Value = producto_p.PNC_CANT_TARIMAS;
            objCmd.Parameters.Add("PNC_TARIMAS_NC_P", OracleDbType.Int32).Value = producto_p.PNC_TARIMAS_NC;
            objCmd.Parameters.Add("PNC_DESCRIPCION_NC_P", OracleDbType.Varchar2).Value = producto_p.PNC_DESCRIPCION_NC;
            objCmd.Parameters.Add("PNC_CLIENTE_P", OracleDbType.Varchar2).Value = producto_p.PNC_CLIENTE;
            objCmd.Parameters.Add("PNC_TIPO_HALLAZGO_P", OracleDbType.Varchar2).Value = producto_p.PNC_TIPO_HALLAZGO;
            objCmd.Parameters.Add("PNC_HALLADO_POR_P", OracleDbType.Varchar2).Value = producto_p.PNC_HALLADO_POR;
            objCmd.Parameters.Add("PNC_EVENTO_HALLAZGO_P", OracleDbType.Varchar2).Value = producto_p.PNC_EVENTO_HALLAZGO;
            objCmd.Parameters.Add("PNC_OTRO_EVENTO_P", OracleDbType.Varchar2).Value = producto_p.PNC_OTRO_EVENTO;
            objCmd.Parameters.Add("PNC_DECISION_CLIENTE_P", OracleDbType.Varchar2).Value = producto_p.PNC_DECISION_CLIENTE;
            objCmd.Parameters.Add("PNC_DESC_OTRO_DECISION_P", OracleDbType.Varchar2).Value = producto_p.PNC_DESC_OTRO_DECISION;
            objCmd.Parameters.Add("PNC_REPRESENTANTE_P", OracleDbType.Varchar2).Value = producto_p.PNC_REPRESENTANTE;
            objCmd.Parameters.Add("PNC_USU_REGISTRA_P", OracleDbType.Varchar2).Value = producto_p.PNC_USU_REGISTRA;
            objCmd.Parameters.Add("PNC_TIPO_PROBLEMA_P", OracleDbType.Varchar2).Value = producto_p.PNC_TIPO_PROBLEMA;
            objCmd.Parameters.Add("PNC_AGENCIA_P", OracleDbType.Varchar2).Value = producto_p.PNC_AGENCIA;
            objCmd.Parameters.Add("PNC_OGU_GUIA_P", OracleDbType.Varchar2).Value = producto_p.PNC_OGU_GUIA;
            objCmd.Parameters.Add("PNC_OBSERVACIONES_P", OracleDbType.Varchar2).Value = producto_p.PNC_OBSERVACIONES;
            objCmd.Parameters.Add("PNC_UBICACION_P", OracleDbType.Varchar2).Value = producto_p.PNC_UBICACION;
            objCmd.Parameters.Add("PNC_CATEGORIA_P", OracleDbType.Varchar2).Value = producto_p.PNC_CATEGORIA;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                ProductoNC _producto = new ProductoNC();
                return Ok(_producto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }            
        }


        [Route("api/TX_PROD_NC_TB/ActualizaInvestigación")]
        [ResponseType(typeof(ProductoNC))]
        public IHttpActionResult PutTX_PROD_NC_TB_INVESTIGACION(string id, ProductoNC producto_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_PNC_INVESTIGACION_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("PNC_IDENTIFICADOR_P", OracleDbType.Int32).Value = producto_p.PNC_IDENTIFICADOR;
            objCmd.Parameters.Add("PNC_ESTADO_P", OracleDbType.Varchar2).Value = producto_p.PNC_ESTADO;
            objCmd.Parameters.Add("PNC_EVID_MANIFIESTO_P", OracleDbType.Varchar2).Value = producto_p.PNC_EVID_MANIFIESTO != null ? "S" : "N";
            objCmd.Parameters.Add("PNC_EVID_FACTURA_P", OracleDbType.Varchar2).Value = producto_p.PNC_EVID_FACTURA != null ? "S" : "N";
            objCmd.Parameters.Add("PNC_EVID_VIDEO_P", OracleDbType.Varchar2).Value = producto_p.PNC_EVID_VIDEO != null ? "S" : "N";
            objCmd.Parameters.Add("PNC_EVID_FOTOS_P", OracleDbType.Varchar2).Value = producto_p.PNC_EVID_FOTOS != null ? "S" : "N";
            objCmd.Parameters.Add("PNC_EVID_EXAMEN_P", OracleDbType.Varchar2).Value = producto_p.PNC_EVID_EXAMEN != null ? "S" : "N";
            objCmd.Parameters.Add("PNC_OTRO_P", OracleDbType.Varchar2).Value = producto_p.PNC_OTRO;
            objCmd.Parameters.Add("PNC_INVESTIGACION_P", OracleDbType.Varchar2).Value = producto_p.PNC_INVESTIGACION;
            objCmd.Parameters.Add("PNC_CAUSA_P", OracleDbType.Varchar2).Value = producto_p.PNC_CAUSA;
            objCmd.Parameters.Add("PNC_DESCRIP_ACCION_P", OracleDbType.Varchar2).Value = producto_p.PNC_DESCRIP_ACCION;
            objCmd.Parameters.Add("PNC_RESPONSABLE_P", OracleDbType.Varchar2).Value = producto_p.PNC_RESPONSABLE;
            objCmd.Parameters.Add("PNC_FCH_LIMITE_P", OracleDbType.Varchar2).Value = producto_p.PNC_FCH_LIMITE;
            objCmd.Parameters.Add("PNC_EMAIL_NOTIF_P", OracleDbType.Varchar2).Value = producto_p.PNC_EMAIL_NOTIF;
            objCmd.Parameters.Add("PNC_USU_CALIDAD_P", OracleDbType.Varchar2).Value = producto_p.PNC_USU_CALIDAD;
            objCmd.Parameters.Add("PNC_USU_INVESTIGA_P", OracleDbType.Varchar2).Value = producto_p.PNC_USU_INVESTIGA;
            objCmd.Parameters.Add("PNC_USU_REVISION_P", OracleDbType.Varchar2).Value = producto_p.PNC_USU_REVISION;
            objCmd.Parameters.Add("PNC_USU_APRUEBA_P", OracleDbType.Varchar2).Value = producto_p.PNC_USU_APRUEBA;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                ProductoNC _producto = new ProductoNC();
                return Ok(_producto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
