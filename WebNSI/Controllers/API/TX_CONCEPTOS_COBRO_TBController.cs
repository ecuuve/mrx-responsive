﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_CONCEPTOS_COBRO_TBController: ApiController
    {
        [Route("api/TX_CONCEPTOS_COBRO_TB/busqueda")]
        public IHttpActionResult query(CONCEPTOS_COBRO_BUSQUEDA post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONCEPTOS_COBRO_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("CCB_DESCRIPCION_P", OracleDbType.Varchar2).Value = post.B_CCB_DESCRIPCION;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();

                DataTable table = new DataTable();
                List<CONCEPTOS_COBRO> data = new List<CONCEPTOS_COBRO>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(table);

                cn.Close();

                foreach (DataRow r in table.Rows)
                {
                    data.Add(new CONCEPTOS_COBRO
                    {
                        CCB_CONCEPTO = r["CCB_CONCEPTO"].ToString(),
                        CCB_DESCRIPCION = r["CCB_DESCRIPCION"].ToString(),
                        CCB_USU_REGISTRO = r["CCB_USU_REGISTRO"].ToString(),
                        CCB_FCH_REGISTRO = r["CCB_FCH_REGISTRO"].ToString(),
                    });
                }

                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}