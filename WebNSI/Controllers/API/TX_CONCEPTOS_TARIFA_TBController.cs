﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_CONCEPTOS_TARIFA_TBController: ApiController
    {
        [Route("api/TX_CONCEPTOS_TARIFA_TB/insertar")]
        public IHttpActionResult Insertar(CONCEPTOS_TARIFA_POST post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_CONCEPTOS_TARIFA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("CTR_CONCEPTO_P", OracleDbType.Varchar2).Value = post.CTR_CCB_CONCEPTO;
            objCmd.Parameters.Add("CTR_MONTO_FIJO_P", OracleDbType.Varchar2).Value = post.CTR_MONTO_FIJO;
            objCmd.Parameters.Add("CTR_MONEDA_MONTO_FIJO_P", OracleDbType.Varchar2).Value = post.CTR_MONEDA_MONTO_FIJO;
            objCmd.Parameters.Add("CTR_PCT_PRORRATEO_P", OracleDbType.Varchar2).Value = post.CTR_PCT_PRORRATEO;
            objCmd.Parameters.Add("CTR_CIF_MINIMO_P", OracleDbType.Varchar2).Value = post.CTR_CIF_MINIMO;
            objCmd.Parameters.Add("CTR_MONTO_MINIMO_P", OracleDbType.Varchar2).Value = post.CTR_MONTO_MINIMO;
            objCmd.Parameters.Add("CTR_TRF_ID_P", OracleDbType.Int32).Value = post.CTR_TRF_ID;
            objCmd.Parameters.Add("CTR_MONEDA_MONTO_MINIMO_P", OracleDbType.Varchar2).Value = post.CTR_MONEDA_MONTO_MINIMO;

            try
            {
                objCmd.ExecuteNonQuery();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        
        [Route("api/TX_CONCEPTOS_TARIFA_TB/actualizar")]
        public IHttpActionResult Actualizar(CONCEPTOS_TARIFA_POST post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_CONCEPTOS_TARIFA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("CTR_CONCEPTO_P", OracleDbType.Varchar2).Value = post.CTR_CCB_CONCEPTO;
            objCmd.Parameters.Add("CTR_MONTO_FIJO_P", OracleDbType.Varchar2).Value = post.CTR_MONTO_FIJO;
            objCmd.Parameters.Add("CTR_MONEDA_MONTO_FIJO_P", OracleDbType.Varchar2).Value = post.CTR_MONEDA_MONTO_FIJO;
            objCmd.Parameters.Add("CTR_PCT_PRORRATEO_P", OracleDbType.Varchar2).Value = post.CTR_PCT_PRORRATEO;
            objCmd.Parameters.Add("CTR_CIF_MINIMO_P", OracleDbType.Varchar2).Value = post.CTR_CIF_MINIMO;
            objCmd.Parameters.Add("CTR_MONTO_MINIMO_P", OracleDbType.Varchar2).Value = post.CTR_MONTO_MINIMO;
            objCmd.Parameters.Add("CTR_MONEDA_MONTO_MINIMO_P", OracleDbType.Varchar2).Value = post.CTR_MONEDA_MONTO_MINIMO;
            objCmd.Parameters.Add("CTR_TRF_ID_P", OracleDbType.Int32).Value = post.CTR_TRF_ID;

            try
            {
                objCmd.ExecuteNonQuery();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/TX_CONCEPTOS_TARIFA_TB/busqueda")]
        public IHttpActionResult Busqueda(CONCEPTOS_TARIFA_BUSQUEDA post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONCEPTOS_TARIFA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("CTR_CCB_CONCEPTO_P", OracleDbType.Varchar2).Value = post.B_CTR_CCB_CONCEPTO;
            objCmd.Parameters.Add("CTR_TRF_ID", OracleDbType.Int32).Value = post.B_CTR_TRF_ID;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();

                DataTable table = new DataTable();
                List<CONCEPTOS_TARIFA> data = new List<CONCEPTOS_TARIFA>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(table);

                cn.Close();

                foreach (DataRow r in table.Rows)
                {
                    data.Add(new CONCEPTOS_TARIFA
                    {
                        CTR_TRF_ID = r["CTR_TRF_ID"].ToString(),
                        CTR_CCB_CONCEPTO = r["CTR_CCB_CONCEPTO"].ToString(),
                        CTR_MONTO_FIJO = decimal.Parse(r["CTR_MONTO_FIJO"].ToString()),
                        CTR_MONEDA_MONTO_FIJO = r["CTR_MONEDA_MONTO_FIJO"].ToString(),
                        CTR_PCT_PRORRATEO = decimal.Parse(r["CTR_PCT_PRORRATEO"].ToString()),
                        CTR_CIF_MINIMO = decimal.Parse(r["CTR_CIF_MINIMO"].ToString()),
                        CTR_MONTO_MINIMO = decimal.Parse(r["CTR_MONTO_MINIMO"].ToString()),
                        CTR_MONEDA_MONTO_MINIMO = r["CTR_MONEDA_MONTO_MINIMO"].ToString(),
                    });
                }

                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/TX_CONCEPTOS_TARIFA_TB/eliminar")]
        public IHttpActionResult Eliminar(CONCEPTOS_TARIFA_POST post)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_DEL_CONCEPTO_TARIFA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("CTR_CCB_CONCEPTO_P", OracleDbType.Varchar2).Value = post.CTR_CCB_CONCEPTO;
            objCmd.Parameters.Add("CTR_TRF_ID", OracleDbType.Varchar2).Value = post.CTR_TRF_ID;

            try
            {
                objCmd.ExecuteNonQuery();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}