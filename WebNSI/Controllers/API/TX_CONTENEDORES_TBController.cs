﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_CONTENEDORES_TBController : ApiController
    {
        /// <summary>
        /// Obtiene contenedores
        /// </summary>
        /// <param name="tco_contenedor_p"></param>
        /// <param name="tco_tma_manifiesto_p"></param>
        /// <param name="tco_con_nconsolidador_p"></param>
        /// <param name="tco_num_contenedor_p"></param>
        /// <returns></returns>
        [Route("api/TX_CONTENEDORES_TB/GetContenedores")]
        [ResponseType(typeof(Contenedor))]
        public IHttpActionResult GetContenedores(long? tco_contenedor_p, string tco_tma_manifiesto_p, long? tco_con_nconsolidador_p, string tco_num_contenedor_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONTENEDORES_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tco_contenedor_p", OracleDbType.Int32).Value = tco_contenedor_p;
            objCmd.Parameters.Add("tco_tma_manifiesto_p", OracleDbType.Varchar2).Value = tco_tma_manifiesto_p;
            objCmd.Parameters.Add("tco_con_nconsolidador_p", OracleDbType.Int32).Value = tco_con_nconsolidador_p;
            objCmd.Parameters.Add("tco_num_contenedor_p", OracleDbType.Varchar2).Value = tco_num_contenedor_p;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Contenedor> _Contenedores = new List<Contenedor>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();



            foreach (DataRow e in datos2.Rows)
            {
                _Contenedores.Add(new Contenedor
                {
                    TCO_CONTENEDOR = long.Parse(e["TCO_CONTENEDOR"].ToString()),
                    TCO_TDO_DOCUMENTO = long.Parse(e["TCO_TDO_DOCUMENTO"].ToString()),
                    TCO_TMA_MANIFIESTO = e["TCO_TMA_MANIFIESTO"].ToString(),
                    TCO_DUA = long.Parse(e["TCO_DUA"].ToString()),
                    TCO_ADU_ADUANA = long.Parse(e["TCO_ADU_ADUANA"].ToString()),
                    TCO_PERIODO = long.Parse(e["TCO_PERIODO"].ToString()),//DateTime.Parse(e["cjr_fch_registro"].ToString()).ToString("dd/MM/yyyy"),
                    TCO_VIAJE = long.Parse(e["TCO_VIAJE"].ToString()),
                    TCO_NUM_CONTENEDOR = e["TCO_NUM_CONTENEDOR"].ToString(),
                    TCO_TAMANO = long.Parse(e["TCO_TAMANO"].ToString()),
                    TCO_CON_NCONSOLIDADOR = long.Parse(e["TCO_CON_NCONSOLIDADOR"].ToString()),
                    TCO_ESTADO = e["TCO_ESTADO"].ToString(),
                    TCO_CHEQUEADOR = e["TCO_CHEQUEADOR"].ToString(),
                    TCO_GRUPO_TRABAJO = e["TCO_GRUPO_TRABAJO"].ToString(),
                    TCO_PUERTA = e["TCO_PUERTA"].ToString(),                  
                    TCO_PERSONAS_DESCARGA = !string.IsNullOrEmpty(e["TCO_PERSONAS_DESCARGA"].ToString()) ? long.Parse(e["TCO_PERSONAS_DESCARGA"].ToString()) : 0,
                    TCO_USU_REGISTRO = e["TCO_USU_REGISTRO"].ToString(),
                    TCO_FCH_REGISTRO = e["TCO_FCH_REGISTRO"].ToString(),
                    TCO_MOT_MOTIVO = !string.IsNullOrEmpty(e["TCO_MOT_MOTIVO"].ToString()) ? long.Parse(e["TCO_MOT_MOTIVO"].ToString()) : 0,
                    TCO_USU_MODIFICO = e["TCO_USU_MODIFICO"].ToString(),
                    TCO_FCH_MODIFICO = e["TCO_FCH_MODIFICO"].ToString(),
                    TCO_DEPARTAMENTO = e["TCO_DEPARTAMENTO"].ToString(),
                    TCO_RESPONSABLE = e["TCO_RESPONSABLE"].ToString(),
                    TCO_SOLICITANTE = e["TCO_SOLICITANTE"].ToString(),
                    CONSOLIDADOR = e["CONSOLIDADOR"].ToString(),
                    ADUANA = e["ADUANA"].ToString(),
                    //TCO_FCH_INGRESO = e["TCO_FCH_INGRESO"].ToString(),
                    //TCO_TIPO_CONTENEDOR = e["TCO_TIPO_CONTENEDOR"].ToString(),
                    //TCO_FUNC_ADUANA = e["TCO_FUNC_ADUANA"].ToString(),
                    //TCO_ACTA_SUP = e["TCO_ACTA_SUP"].ToString()

                });
            }





            return Ok(_Contenedores);
        }

        #region Para llenar LVs
        
        /// <summary>
        /// Obtiene consolidadores
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_CONTENEDORES_TB/Consolidadores")]
        [ResponseType(typeof(Consolidador))]
        public IHttpActionResult GetConsolidadores()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_CONSOLIDADORES_PR ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("consolidador_p", OracleDbType.Int32).Value = null;
            objCmd.Parameters.Add("con_nombre_p", OracleDbType.Varchar2).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Consolidador> _Consolidadores = new List<Consolidador>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Consolidadores.Add(new Consolidador
                {
                    Id = int.Parse(r["ID"].ToString()),
                    Nombre = r["NOMBRE"].ToString(),
                    Cedula = r["CEDULA"].ToString(),
                });
            }

            return Ok(_Consolidadores);
        }

        [Route("api/TX_CONTENEDORES_TB/FuncionariosAduana")]
        //[ResponseType(typeof(Consolidador))]
        public IHttpActionResult GetFuncionariosAduana()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            //objCmd.CommandText = "mrx.TX_QRY_CONSOLIDADORES_PR ";
            //objCmd.CommandType = CommandType.StoredProcedure;
            //objCmd.Parameters.Add("consolidador_p", OracleDbType.Int32).Value = null;
            //objCmd.Parameters.Add("con_nombre_p", OracleDbType.Varchar2).Value = null;
            //objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            //objCmd.ExecuteNonQuery();

            //DataTable datos2 = new DataTable();
            //List<Consolidador> _Consolidadores = new List<Consolidador>();

            //OracleDataAdapter objAdapter = new OracleDataAdapter();
            //objAdapter.SelectCommand = objCmd;
            //objAdapter.Fill(datos2);

            cn.Close();

            //foreach (DataRow r in datos2.Rows)
            //{
            //    _Consolidadores.Add(new Consolidador
            //    {
            //        Id = int.Parse(r["ID"].ToString()),
            //        Nombre = r["NOMBRE"].ToString(),
            //        Cedula = r["CEDULA"].ToString(),
            //    });
            //}

            return BadRequest();
        }

        /// <summary>
        /// Obtiene Documentos
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_CONTENEDORES_TB/Documentos")]
        [ResponseType(typeof(Documento))]
        public IHttpActionResult GetDocumentos()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_tipo_doc_SIA_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tdo_tipo_doc_p", OracleDbType.Long).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Documento> _Documentos = new List<Documento>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Documentos.Add(new Documento
                {
                    Id = r["CODIGO"].ToString(),
                    Nombre = r["DOCUMENTO"].ToString()
                });
            }

            return Ok(_Documentos);
        }


        /// <summary>
        /// Obtiene Estados
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_CONTENEDORES_TB/Estados")]
        [ResponseType(typeof(Estados))]
        public IHttpActionResult GetEstados()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_ESTADOS_CONTENEDORES_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Estados> _Estados = new List<Estados>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Estados.Add(new Estados
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Estados);
        }

        /// <summary>
        /// Obtiene Grupos de trabajo
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_CONTENEDORES_TB/Grupos")]
        [ResponseType(typeof(GruposTrabajo))]
        public IHttpActionResult GetGrupos()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_CONT_GRUPO_TRAB_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<GruposTrabajo> _GruposTrabajo = new List<GruposTrabajo>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _GruposTrabajo.Add(new GruposTrabajo
                {
                    Id = r["ID"].ToString(),
                    Nombre = r["NOMBRE"].ToString()
                });
            }

            return Ok(_GruposTrabajo);
        }


        #endregion



        /// <summary>
        /// Agrega contenedor
        /// </summary>
        /// <param name="contenedor_p"></param>
        /// <returns></returns>
        [ResponseType(typeof(Contenedor))]
        public IHttpActionResult PostTX_CONTENEDORES_TB(Contenedor contenedor_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_CONTENEDORES_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tco_tdo_documento_p", OracleDbType.Int32).Value = contenedor_p.TCO_TDO_DOCUMENTO;
            objCmd.Parameters.Add("tco_tma_manifiesto_p", OracleDbType.Varchar2).Value = contenedor_p.TCO_TMA_MANIFIESTO;
            objCmd.Parameters.Add("tco_dua_p", OracleDbType.Int32).Value = contenedor_p.TCO_DUA;
            objCmd.Parameters.Add("tco_adu_aduana_p", OracleDbType.Int32).Value = 1;// contenedor_p.TCO_ADU_ADUANA;
            objCmd.Parameters.Add("tco_periodo_p", OracleDbType.Int32).Value = contenedor_p.TCO_PERIODO;
            objCmd.Parameters.Add("tco_viaje_p", OracleDbType.Int32).Value = contenedor_p.TCO_VIAJE;
            objCmd.Parameters.Add("tco_num_contenedor_p", OracleDbType.Varchar2).Value = contenedor_p.TCO_NUM_CONTENEDOR;
            objCmd.Parameters.Add("tco_tamano_p", OracleDbType.Int32).Value = contenedor_p.TCO_TAMANO;
            objCmd.Parameters.Add("tco_con_nconsolidador_p", OracleDbType.Int32).Value = contenedor_p.TCO_CON_NCONSOLIDADOR;
            objCmd.Parameters.Add("tco_estado_p", OracleDbType.Varchar2).Value = contenedor_p.TCO_ESTADO;
            objCmd.Parameters.Add("tco_chequeador_p", OracleDbType.Varchar2).Value = contenedor_p.TCO_CHEQUEADOR;
            objCmd.Parameters.Add("tco_grupo_trabajo_p", OracleDbType.Varchar2).Value = contenedor_p.TCO_GRUPO_TRABAJO;
            objCmd.Parameters.Add("tco_puerta_p", OracleDbType.Varchar2).Value = contenedor_p.TCO_PUERTA;
            objCmd.Parameters.Add("tco_personas_descarga_p", OracleDbType.Int32).Value = contenedor_p.TCO_PERSONAS_DESCARGA;
            objCmd.Parameters.Add("tco_usu_registro_p", OracleDbType.Varchar2).Value = "MRX";
        
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Contenedor _ContenedorIn = new Contenedor();

            return Ok(_ContenedorIn);
        }

        // PUT: 
        /// <summary>
        /// Actualiza el contenedor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="contenedor_p"></param>
        /// <returns></returns>
        [ResponseType(typeof(Contenedor))]
        public IHttpActionResult PutTX_CONTENEDORES_TB(string id, Contenedor contenedor_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_UPD_CONTENEDORES_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("tco_contenedor_p", OracleDbType.Int32).Value = contenedor_p.TCO_CONTENEDOR;
            objCmd.Parameters.Add("tco_tdo_documento_p", OracleDbType.Int32).Value = contenedor_p.TCO_TDO_DOCUMENTO;
            objCmd.Parameters.Add("tco_tma_manifiesto_p", OracleDbType.Varchar2).Value = contenedor_p.TCO_TMA_MANIFIESTO;
            objCmd.Parameters.Add("tco_dua_p", OracleDbType.Int32).Value = contenedor_p.TCO_DUA;
            objCmd.Parameters.Add("tco_adu_aduana_p", OracleDbType.Int32).Value = 1; //contenedor_p.TCO_ADU_ADUANA;
            objCmd.Parameters.Add("tco_periodo_p", OracleDbType.Int32).Value = contenedor_p.TCO_PERIODO;
            objCmd.Parameters.Add("tco_viaje_p", OracleDbType.Int32).Value = contenedor_p.TCO_VIAJE;
            objCmd.Parameters.Add("tco_num_contenedor_p", OracleDbType.Varchar2).Value = contenedor_p.TCO_NUM_CONTENEDOR;
            objCmd.Parameters.Add("tco_tamano_p", OracleDbType.Int32).Value = contenedor_p.TCO_TAMANO;
            objCmd.Parameters.Add("tco_con_nconsolidador_p", OracleDbType.Int32).Value = contenedor_p.TCO_CON_NCONSOLIDADOR;
            objCmd.Parameters.Add("tco_estado_p", OracleDbType.Varchar2).Value = contenedor_p.TCO_ESTADO;
            objCmd.Parameters.Add("tco_chequeador_p", OracleDbType.Varchar2).Value = contenedor_p.TCO_CHEQUEADOR;
            objCmd.Parameters.Add("tco_grupo_trabajo_p", OracleDbType.Varchar2).Value = contenedor_p.TCO_GRUPO_TRABAJO;
            objCmd.Parameters.Add("tco_puerta_p", OracleDbType.Varchar2).Value = contenedor_p.TCO_PUERTA;
            objCmd.Parameters.Add("tco_personas_descarga_p", OracleDbType.Int32).Value = contenedor_p.TCO_PERSONAS_DESCARGA;
            objCmd.Parameters.Add("tco_usu_modifico_p", OracleDbType.Varchar2).Value = "MRX";
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Contenedor _ContenedorUp = new Contenedor();

            return Ok(_ContenedorUp);
        }

    }

}
