﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_MOVS_ENTRADA_TBController : ApiController
    {

        [Route("api/TX_MOVS_ENTRADA_TB/GetMovimientos")]
        [ResponseType(typeof(MOVS_ENTRADA))]
        public IHttpActionResult GetMovimientos(long? mov, long? BL, string dua, string consig, long? viaje, string contenedor)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_MOVS_ENTRADA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mve_mov_inventario_p", OracleDbType.Varchar2).Value = mov;
            objCmd.Parameters.Add("mve_ien_entrada_p", OracleDbType.Varchar2).Value = BL;
            objCmd.Parameters.Add("ien_dua_p", OracleDbType.Varchar2).Value = dua;
            objCmd.Parameters.Add("ien_consignatario_p", OracleDbType.Varchar2).Value = consig;
            objCmd.Parameters.Add("ien_numero_viaje", OracleDbType.Varchar2).Value = viaje;
            objCmd.Parameters.Add("tma_num_contenedor", OracleDbType.Varchar2).Value = contenedor;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<MOVS_ENTRADA> _movs = new List<MOVS_ENTRADA>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();


       
                foreach (DataRow e in datos2.Rows)
                {
                    //string CJR_fch_mod;
                    //if (e["cjr_fch_modifico"].ToString() != "")
                    //{
                    //     CJR_fch_mod = DateTime.Parse(e["cjr_fch_modifico"].ToString()).ToString("dd/MM/yyyy");
                    //}
                    //else
                    //{
                    //    CJR_fch_mod = "";
                    //}

                    _movs.Add(new MOVS_ENTRADA
                    {
                        MOV_ANNO = e["MOV_ANNO"].ToString(),
                        MOV_A_PISO = (e["MOV_PALETIZADO"].ToString() == "N")? "S":"N", //manifiesto //!string.IsNullOrEmpty(e["MER_BULTOS_REF"].ToString()) ? decimal.Parse(e["MER_BULTOS_REF"].ToString()) : 0,
                        //MOV_BLTS_DISPONIBLES = e["cjr_activo"].ToString(),
                        //MOV_BLTS_EXISTENTES = e["cjr_doble_impr"].ToString(),
                        MOV_BLTS_INGRESADOS = e["MOV_BLTS_INGRESADOS"].ToString(),
                        MOV_CONOCIMIENTO = e["MOV_CONOCIMIENTO"].ToString(),, //entrada
                        MOV_CONSIGNATARIO = e["MOV_CONSIGNATARIO"].ToString(), // entrada
                        MOV_CONSOLIDADOR = e["MOV_CONSOLIDADOR"].ToString(),
                        MOV_DESCRIPCION = e["MOV_DESCRIPCION"].ToString(), // movs entrada
                        MOV_DUA = e["MOV_DUA"].ToString(),
                        MOV_EMBALAJE = e["MOV_EMBALAJE"].ToString(),
                        MOV_ESTADO = e["MOV_ESTADO"].ToString(),
                        MOV_FCH_INGRESO = e["MOV_FCH_INGRESO"].ToString(),
                        MOV_MOVIMIENTO = e["MOV_MOVIMIENTO"].ToString(),
                        //MOV_OBSERVACIONES = e["cjr_usu_modifico"].ToString(),
                        MOV_PALETIZADO = e["MOV_PALETIZADO"].ToString(),
                        //MOV_PESO = e["cjr_usu_modifico"].ToString(),
                        MOV_TPO_CONTENEDOR = e["MOV_TPO_CONTENEDOR"].ToString(),
                        MOV_VIAJE = e["MOV_VIAJE"].ToString(),
                        //MOV_VOL_DISPONIBLE = e["cjr_usu_modifico"].ToString(),
                        MOV_VOL_INGRESADO = e["MOV_VOL_INGRESADO"].ToString()
                        
                    });
                }
            
           
               
           

            return Ok(_movs);
        }

       
    }
}
