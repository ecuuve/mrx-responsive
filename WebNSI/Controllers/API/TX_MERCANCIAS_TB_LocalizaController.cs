﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_MERCANCIAS_TB_LocalizaController : ApiController
    {
        [Route("api/TX_MERCANCIAS_TB_Localiza/MercanciaDetalle")]
        [ResponseType(typeof(Mercancia))]
        public IHttpActionResult GetMercancia(long id)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_loc_mercancia2_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Long).Value = id;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            //objCmd.ExecuteNonQuery();
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            DataTable datos2 = new DataTable();
            Mercancia _Mercancia = new Mercancia();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Mercancia.MER_ID_MERCANCIA = e["MER_ID_MERCANCIA"].ToString();
                _Mercancia.MER_IEN_ENTRADA = e["MER_IEN_ENTRADA"].ToString();
                _Mercancia.MER_BULTOS_DESPALETIZADOS =e["MER_BULTOS_DESPALETIZADOS"].ToString();
                _Mercancia.MER_TIP_CODIGO_IATA = e["MER_TIP_CODIGO_IATA"].ToString();
                _Mercancia.PEL_Descripcion = e["PELIGROSA"].ToString();
                _Mercancia.MER_REFRIGERADO = e["MER_REFRIGERADO"].ToString();
                _Mercancia.MER_LOC_RACK = e["MER_LOC_RACK"].ToString();
                _Mercancia.MER_LOC_COLUMNA = e["MER_LOC_COLUMNA"].ToString();
                _Mercancia.MER_LOC_LADO = e["MER_LOC_LADO"].ToString();
                _Mercancia.MER_LOC_ALTURA = e["MER_LOC_ALTURA"].ToString();
                _Mercancia.MER_USUARIO_Localiza =e["MER_USUARIO_LOCALIZA"].ToString();


            }

            return Ok(_Mercancia);
        }


        [Route("api/TX_MERCANCIAS_TB_Localiza/GetMercanciasLocalizacion")]
        [ResponseType(typeof(Mercancia))]
        public IHttpActionResult GetMercanciasLocalizacion(long? entrada, long? mercancia)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_loc_mercancia_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mer_ien_entrada_p", OracleDbType.Long).Value = entrada;
            objCmd.Parameters.Add("mer_id_mercancia_p", OracleDbType.Long).Value = mercancia;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Mercancia> _Mercancias = new List<Mercancia>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();
            foreach (DataRow e in datos2.Rows)
            {

                _Mercancias.Add(new Mercancia
                {
                    MER_ID_MERCANCIA = e["MER_ID_MERCANCIA"].ToString(),
                    MER_IEN_ENTRADA =e["MER_IEN_ENTRADA"].ToString(),
                    MER_BULTOS_DESPALETIZADOS = e["MER_BULTOS_DESPALETIZADOS"].ToString(),
                    MER_TIP_CODIGO_IATA = e["MER_TIP_CODIGO_IATA"].ToString(),
                    PEL_Descripcion = e["PELIGROSA"].ToString(),
                    MER_REFRIGERADO = e["MER_REFRIGERADO"].ToString(),
                    MER_LOC_RACK = e["MER_LOC_RACK"].ToString(),
                    MER_LOC_COLUMNA = e["MER_LOC_COLUMNA"].ToString(),
                    MER_LOC_LADO = e["MER_LOC_LADO"].ToString(),
                    MER_LOC_ALTURA = e["MER_LOC_ALTURA"].ToString()
                });
            }

            return Ok(_Mercancias);
        }



        [Route("api/TX_MERCANCIAS_TB_Localiza/Peligrosa")]
        [ResponseType(typeof(Peligrosidad))]
        public IHttpActionResult GetPeligrosidades()
        {
            //string id = "";
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_codigo_iata_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Peligrosidad> _Peligrosidad = new List<Peligrosidad>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.AsEnumerable().Take(300))
            {
                _Peligrosidad.Add(new Peligrosidad
                {
                    Id = r["codigo"].ToString(),
                    Nombre = r["nombre"].ToString()
                });
            }

            return Ok(_Peligrosidad);
        }


        // PUT: api/TX_MERCANCIAS_TB/5
        [ResponseType(typeof(Mercancia))]
        public IHttpActionResult PutTX_MERCANCIAS_TB(long id, Mercancia merca_p)
        {
            if (merca_p.MER_REFRIGERADO == null) {
                merca_p.MER_REFRIGERADO = "N";
            }
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;

                objCmd.CommandText = "mrx.tx_upd_loc_mercancia_pr";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("tarima_p", OracleDbType.Int32).Value = id;
                objCmd.Parameters.Add("rack_p", OracleDbType.Long).Value = merca_p.MER_LOC_RACK;
                objCmd.Parameters.Add("col_p", OracleDbType.Long).Value = merca_p.MER_LOC_COLUMNA;
                objCmd.Parameters.Add("lado_p", OracleDbType.Long).Value = merca_p.MER_LOC_LADO;
                objCmd.Parameters.Add("altura_p", OracleDbType.Long).Value = merca_p.MER_LOC_ALTURA;
                objCmd.Parameters.Add("refr_p", OracleDbType.Varchar2).Value = merca_p.MER_REFRIGERADO;
                objCmd.Parameters.Add("iata_p", OracleDbType.Long).Value = decimal.Parse(merca_p.MER_TIP_CODIGO_IATA);
                objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = "MRX";


            //objCmd.Parameters.Add("error_p", OracleDbType.Varchar2).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            //long paramId = long.Parse(objCmd.Parameters["rcl_id_p"].Value.ToString());
            //string paramErr = objCmd.Parameters["error_p"].Value.ToString();
            Mercancia _MercaIn = new Mercancia();
            //// _ReclamoIn.rcl_id_p = paramId;
            //_MercaIn.error_p = paramErr;

            return Ok(_MercaIn);

        }
    }
}