﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;
namespace WebNSI.Controllers.API
{
    public class TX_MERCANCIAS_PERDIDAS_TB_Controller : ApiController
    {
        /// <summary>
        /// Método que se encarga de mostrar la información de las mercancias perdidas.
        /// </summary>
        /// <param name="Id_Mercancia">Id de la mercancia</param>
        /// <param name="Fecha_Reportada">Fecha de reporte de la mercancia perdida</param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_PERDIDAS_TB/GetMercancias_Perdidas")]
        [ResponseType(typeof(MercanciasPerdidas))]
        public IHttpActionResult GetMercancias_Perdidas(long? Id_Mercancia, string Fecha_Reportada)
        {

            //Obtenemos la conección string desde el app.config          
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_MERC_PERDIDA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("id_mercancia_p", OracleDbType.Int32).Value = Id_Mercancia;
            objCmd.Parameters.Add("fecha_reporte_p", OracleDbType.Varchar2).Value = Fecha_Reportada;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                  
            try
            {
                objCmd.ExecuteNonQuery();               
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            DataTable datos2 = new DataTable();
            List<MercanciasPerdidas> _Mercancias_Perdidas = new List<MercanciasPerdidas>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
              
                _Mercancias_Perdidas.Add(new MercanciasPerdidas
                {
                    MRP_ID = Convert.ToInt32(e["MRP_ID"]),
                    MRP_MER_ID_MERCANCIA = e["MRP_MER_ID_MERCANCIA"].ToString(),
                    MRP_CONSIGNATARIO = e["CONSIGNATARIO"].ToString(),
                    DES_TIPO_RESOL = e["DES_TIPO_RESOL"].ToString(),
                    MRP_USU_REPORTA = e["MRP_USU_REPORTA"].ToString(),                  
                    MRP_FCH_INICIO = DateTime.Parse(e["MRP_FCH_INICIO"].ToString()).ToString("dd/MM/yyyy"),                   
    });
            }
            return Ok(_Mercancias_Perdidas);
        }

        /// <summary>
        /// Método que se encarga de obtener la información del detalle de la mercancia perdida a editar.
        /// </summary>
        /// <param name="Id_Mercancia">Id de la mercancia a editar</param>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_PERDIDAS_TB/GetDetalleMercancias_Perdidas")]
        [ResponseType(typeof(MercanciasPerdidas))]
        public IHttpActionResult GetDetalleMercancias_Perdidas(long? Id_Mercancia)
        {
            //Obtenemos la conección string desde el app.config
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_MERC_PERDIDA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("id_mercancia_p", OracleDbType.Int32).Value = Id_Mercancia;
            objCmd.Parameters.Add("fecha_reporte_p", OracleDbType.Varchar2).Value = null;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
          
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            DataTable datos2 = new DataTable();
            List<MercanciasPerdidas> _Mercancias_Perdidas = new List<MercanciasPerdidas>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Mercancias_Perdidas.Add(new MercanciasPerdidas
                {
                    MRP_ID = Convert.ToInt32(e["MRP_ID"]),
                    MRP_MER_ID_MERCANCIA = e["MRP_MER_ID_MERCANCIA"].ToString(),
                    MRP_CONSIGNATARIO = e["CONSIGNATARIO"].ToString(),
                    MRP_TIPO_RESOL = e["MRP_TIPO_RESOL"].ToString(),
                    DES_TIPO_RESOL = e["DES_TIPO_RESOL"].ToString(),
                    MRP_USU_REPORTA = e["MRP_USU_REPORTA"].ToString(),
                    MRP_FCH_INICIO = DateTime.Parse(e["MRP_FCH_INICIO"].ToString()).ToString("dd/MM/yyyy"),

                });
            }
            return Ok(_Mercancias_Perdidas);
        }

        /// <summary>
        /// Método que se encarga de actualizar el tipo de resolución de las mercancias perdidas
        /// </summary>
        /// <param name="mercancias_perdidas_p">Modelo que trae los datos de la mercancia perdida a editar</param>
        /// <returns></returns>
        [ResponseType(typeof(MercanciasPerdidas))]
        public IHttpActionResult PutTX_MERCANCIAS_PERDIDAS_TB(MercanciasPerdidas mercancias_perdidas_p)
        {
            //Obtenemos la conección string desde el app.config        
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;

            objCmd.CommandText = "mrx.TX_UPD_MERC_PERDIDA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("mrp_id_p", OracleDbType.Int32).Value = mercancias_perdidas_p.MRP_ID;
            objCmd.Parameters.Add("mrp_tipo_resol_p", OracleDbType.Varchar2).Value = mercancias_perdidas_p.MRP_TIPO_RESOL;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value =mercancias_perdidas_p.UserMERX;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            MercanciasPerdidas _Mercancias_PerdidasIn = new MercanciasPerdidas();
            return Ok(_Mercancias_PerdidasIn);

        }

        /// <summary>
        /// Método que se encarga de llenar el combo con los tipos de resolución
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_MERCANCIAS_PERDIDAS_TB/TipoResolucion")]
        [ResponseType(typeof(TipoResolucion))]
        public IHttpActionResult GetTipoResolucion()
        {
            //Obtenemos la conección string desde el app.config           
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_LV_MRP_TIPO_RESOL_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<TipoResolucion> _Tipo_Resolucion = new List<TipoResolucion>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Tipo_Resolucion.Add(new TipoResolucion
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Tipo_Resolucion);
        }



    }
}
