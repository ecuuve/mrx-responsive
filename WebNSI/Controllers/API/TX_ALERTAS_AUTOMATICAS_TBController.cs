﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_ALERTAS_AUTOMATICAS_TBController : ApiController
    {
        /// <summary>
        /// Carga los datos y lista las alertas automaticas de indicaciones a posterior
        /// </summary>
        /// <param name="fch_evento"></param>
        /// <param name="ien_entrada"></param>
        /// <returns></returns>
        [Route("api/TX_ALERTAS_AUTOMATICAS_TB/GetAlertas")]
        public IHttpActionResult GetAlertas(string fch_evento , string ien_entrada )
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_alert_auto_pr ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("aum_fch_evento_p  ", OracleDbType.Varchar2).Value = fch_evento;
            objCmd.Parameters.Add("aum_ien_entrada_p", OracleDbType.Int32).Value = ien_entrada;
            objCmd.Parameters.Add("recordset_p ", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos = new DataTable();
                List<IndicacionesPosterior> _IndicacionesPosterior = new List<IndicacionesPosterior>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos);

                cn.Close();

                foreach (DataRow e in datos.Rows)
                {
                    _IndicacionesPosterior.Add(new IndicacionesPosterior
                    {
                        AUM_ID = e["AUM_ID"].ToString(),
                        AUM_FCH_EVENTO = e["fch_evento"].ToString(),
                        AUM_IEN_ENTRADA = e["AUM_IEN_ENTRADA"].ToString(),
                        AUM_DETALLE = e["AUM_DETALLE"].ToString(),
                        AUM_FCH_INICIO_PREALERTA = e["fch_prealerta"].ToString(),
                        AUM_FCH_REGISTRA = e["fch_registra"].ToString(),
                        AUM_ESTADO = e["AUM_ESTADO"].ToString(),
                        AUM_USU_REGISTRA = e["AUM_USU_REGISTRA"].ToString(),
                        AUM_USU_FINALIZA = e["AUM_USU_FINALIZA"].ToString(),
                        AUM_FCH_FINALIZA = e["fch_finaliza"].ToString()
                    });
                }
                return Ok(_IndicacionesPosterior);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/TX_ALERTAS_AUTOMATICAS_TB/FinalizarAlerta")]
        public IHttpActionResult GetFinalizarAlerta(int id, string user)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_upd_alert_auto_pr  ";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("aum_id_p", OracleDbType.Int32).Value = id;
            objCmd.Parameters.Add("aum_usuario", OracleDbType.Varchar2).Value = user;           
            objCmd.Parameters.Add("error_p ", OracleDbType.Char,50).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                cn.Close();
                return Ok("Finalización realizada correctamente");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        #region PANTALLA CONOCIMENTOS
            [Route("api/TX_ALERTAS_AUTOMATICAS_TB/AlertasAutomaticas")]
            public IHttpActionResult GetAlertasAutomaticas(int id)
            {
                //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.tx_qry_alertas_aut_pr  ";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("cant_alert_p ", OracleDbType.Char, 50).Direction = ParameterDirection.Output;
                try
                {
                    objCmd.ExecuteNonQuery();
                    cn.Close();
                    string alertas = objCmd.Parameters["cant_alert_p"].Value.ToString();
                    return Ok("Finalización realizada correctamente");
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }

            }
        #endregion
    }
}
