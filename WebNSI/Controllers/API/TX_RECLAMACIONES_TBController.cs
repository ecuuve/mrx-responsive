﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_RECLAMACIONES_TBController : ApiController
    {

        /// <summary>
        /// Obtiene datos de los reclamos
        /// </summary>
        /// <param name="id"></param>
        /// <param name="empresa"></param>
        /// <param name="tipo"></param>
        /// <param name="tipoCliente"></param>
        /// <returns></returns>
        [Route("api/TX_RECLAMACIONES_TB/Reclamo")]
        [ResponseType(typeof(Reclamo))]
        public IHttpActionResult GetReclamo(long? id, string empresa, string tipo, string tipoCliente)
        {
            if (tipo == "0")
            {
                tipo = null;
            }
            if (tipoCliente == "0")
            {
                tipoCliente = null;
            }

            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_reclama_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("rcl_id_p", OracleDbType.Int32).Value = id;
            objCmd.Parameters.Add("rcl_tipo_p", OracleDbType.Varchar2).Value = tipo;
            objCmd.Parameters.Add("rcl_tipo_cliente_p", OracleDbType.Varchar2).Value = tipoCliente;
            objCmd.Parameters.Add("rcl_empresa_p", OracleDbType.Varchar2).Value = empresa;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }


            DataTable datos2 = new DataTable();
            Reclamo _Reclamo = new Reclamo();
            List<Reclamo> _Reclamos = new List<Reclamo>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();
            if (datos2.Rows.Count > 1)
            {
                foreach (DataRow r in datos2.Rows)
                {
                    _Reclamos.Add(new Reclamo
                    {
                        RCL_Id = long.Parse(r["RCL_ID"].ToString()),
                        RCL_Tipo = r["DESC_RCL_TIPO"].ToString(),
                        RCL_TIPO_Cliente = r["DES_RCL_TIPO_CLIENTE"].ToString(),
                        RCL_Empresa = r["RCL_EMPRESA"].ToString(),                    
                    });
                }

                return Ok(_Reclamos);

            }
            else
            {
                foreach (DataRow r in datos2.Rows)
                {
                    _Reclamo.RCL_Id = long.Parse(r["RCL_ID"].ToString());
                    _Reclamo.RCL_Fecha = DateTime.Parse(r["RCL_FECHA"].ToString()).ToString("dd/MM/yyyy");
                    _Reclamo.RCL_Tipo = r["RCL_TIPO"].ToString();
                    _Reclamo.RCL_TIPO_Cliente = r["RCL_TIPO_CLIENTE"].ToString();
                    _Reclamo.RCL_Empresa = r["RCL_EMPRESA"].ToString();
                    _Reclamo.RCL_Representante = r["RCL_REPRESENTANTE"].ToString();
                    _Reclamo.RCL_Email = r["RCL_EMAIL"].ToString();
                    _Reclamo.RCL_Telefono = r["RCL_TELEFONO"].ToString();
                    _Reclamo.RCL_Area = r["RCL_AREA"].ToString();
                    _Reclamo.RCL_AREA_Otro = r["RCL_AREA_OTRO"].ToString();
                    _Reclamo.RCL_Servicio = r["RCL_SERVICIO"].ToString();
                    _Reclamo.RCL_SERVICIO_Otro = r["RCL_SERVICIO_OTRO"].ToString();
                    _Reclamo.RCL_Descripcion = r["RCL_DESCRIPCION"].ToString();
                    _Reclamo.RCL_IEN_Entrada = r["RCL_IEN_ENTRADA"].ToString();
                    _Reclamo.RCL_OGU_Guia = r["RCL_OGU_GUIA"].ToString();
                    _Reclamo.RCL_USU_Registra = r["RCL_USU_REGISTRA"].ToString();
                    _Reclamo.RCL_FCH_Registra = DateTime.Parse(r["RCL_FCH_REGISTRA"].ToString()).ToString("dd/MM/yyyy");
                    _Reclamo.RCL_Observacion = r["RCL_OBSERVACION"].ToString();
                    _Reclamo.RCL_USU_Seguimiento = r["RCL_USU_SEGUIMIENTO"].ToString();
                    if (r["RCL_FCH_SEGUIMIENTO"].ToString() != "")
                    {
                        _Reclamo.RCL_FCH_Seguimiento = DateTime.Parse(r["RCL_FCH_SEGUIMIENTO"].ToString()).ToString("dd/MM/yyyy");
                    }
                   
                }

                return Ok(_Reclamo);

            }

        }

        /// <summary>
        /// Obtiene tipo cliente 
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_RECLAMACIONES_TB/TipoCliente")]
        [ResponseType(typeof(RCL_TipoCliente))]
        public IHttpActionResult GetTipoCliente()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_rcl_tipo_cli_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<RCL_TipoCliente> _TipoCliente = new List<RCL_TipoCliente>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TipoCliente.Add(new RCL_TipoCliente
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_TipoCliente);
        }

        /// <summary>
        /// Obtiene el tipo para la lista de valores
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_RECLAMACIONES_TB/Tipo")]
        [ResponseType(typeof(RCL_Tipo))]
        public IHttpActionResult GetTipo()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_rcl_tipo_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<RCL_Tipo> _Tipo = new List<RCL_Tipo>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Tipo.Add(new RCL_Tipo
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Tipo);
        }

        /// <summary>
        /// Obtiene los servicios
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_RECLAMACIONES_TB/Servicio")]
        [ResponseType(typeof(RCL_Servicio))]
        public IHttpActionResult GetServicio()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_rcl_servicio_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<RCL_Servicio> _Servicio = new List<RCL_Servicio>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Servicio.Add(new RCL_Servicio
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Servicio);
        }

        /// <summary>
        /// Obtiene las áreas
        /// </summary>
        /// <returns></returns>
        [Route("api/TX_RECLAMACIONES_TB/Area")]
        [ResponseType(typeof(RCL_Area))]
        public IHttpActionResult GetArea()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_rcl_area_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<RCL_Area> _Area = new List<RCL_Area>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _Area.Add(new RCL_Area
                {
                    Id = r["Id"].ToString(),
                    Nombre = r["Nombre"].ToString()
                });
            }

            return Ok(_Area);
        }

        /// <summary>
        /// Actualiza los datos del reclamo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="reclamo_p"></param>
        /// <returns></returns>
        [Route("api/TX_RECLAMACIONES_TB/ActualizarReclamo")]
        [ResponseType(typeof(Reclamo))]
        public IHttpActionResult PutTX_RECLAMACIONES_TB(long id, Reclamo reclamo_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;

            if (reclamo_p.RCL_IEN_Entrada == "")
            {
                reclamo_p.RCL_IEN_Entrada = null;
            }
                objCmd.CommandText = "mrx.TX_UPD_RECLAMA_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("rcl_id_p", OracleDbType.Int32).Value = id;
                objCmd.Parameters.Add("rcl_representante_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Representante;
                objCmd.Parameters.Add("rcl_email_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Email;
                objCmd.Parameters.Add("rcl_telefono_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Telefono;
                objCmd.Parameters.Add("rcl_area_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Area;
                objCmd.Parameters.Add("rcl_area_otro_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_AREA_Otro;
                objCmd.Parameters.Add("rcl_servicio_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Servicio;
                objCmd.Parameters.Add("rcl_servicio_otro_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_SERVICIO_Otro;
                objCmd.Parameters.Add("rcl_descripcion_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Descripcion;
                objCmd.Parameters.Add("rcl_ien_entrada_p", OracleDbType.Int32).Value = reclamo_p.RCL_IEN_Entrada;
                objCmd.Parameters.Add("rcl_ogu_guia_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_OGU_Guia;
                objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = reclamo_p.UserMERX;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Reclamo _ReclamoIn = new Reclamo();

            return Ok(_ReclamoIn);
            
        }

        /// <summary>
        /// Actualiza los datos del reclamo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="reclamo_p"></param>
        /// <returns></returns>
        [Route("api/TX_RECLAMACIONES_TB/ActualizarReclamoSeguimiento")]
        [ResponseType(typeof(Reclamo))]
        public IHttpActionResult PutTX_RECLAMACIONES_TB_Seguimiento(long id, Reclamo reclamo_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
           
            objCmd.CommandText = "mrx.TX_UPD_RECLAMA_SEG_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("rcl_id_p", OracleDbType.Int32).Value = id;
            objCmd.Parameters.Add("rcl_observacion_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Observacion;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = reclamo_p.UserMERX;
            
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Reclamo _ReclamoIn = new Reclamo();

            return Ok(_ReclamoIn);

        }

        /// <summary>
        /// Guarda la información de reclamos 
        /// </summary>
        /// <param name="reclamo_p"></param>
        /// <returns></returns>
        [ResponseType(typeof(Reclamo))]
        public IHttpActionResult PostTX_RECLAMACIONES_TB(Reclamo reclamo_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_INS_RECLAMA_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("rcl_fecha_p", OracleDbType.Date).Value = DateTime.Parse(reclamo_p.RCL_Fecha);
            objCmd.Parameters.Add("rcl_tipo_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Tipo;
            objCmd.Parameters.Add("rcl_tipo_cliente_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_TIPO_Cliente;
            objCmd.Parameters.Add("rcl_empresa_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Empresa;
            objCmd.Parameters.Add("rcl_representante_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Representante;
            objCmd.Parameters.Add("rcl_email_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Email;
            objCmd.Parameters.Add("rcl_telefono_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Telefono;
            objCmd.Parameters.Add("rcl_area_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Area;
            objCmd.Parameters.Add("rcl_area_otro_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_AREA_Otro;
            objCmd.Parameters.Add("rcl_servicio_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Servicio;
            objCmd.Parameters.Add("rcl_servicio_otro_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_SERVICIO_Otro;
            objCmd.Parameters.Add("rcl_descripcion_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_Descripcion;
            objCmd.Parameters.Add("rcl_ien_entrada_p", OracleDbType.Int32).Value = reclamo_p.RCL_IEN_Entrada;
            objCmd.Parameters.Add("rcl_ogu_guia_p", OracleDbType.Varchar2).Value = reclamo_p.RCL_OGU_Guia;          
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = reclamo_p.UserMERX;
            objCmd.Parameters.Add("rcl_id_p", OracleDbType.Int32).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            long paramId = long.Parse(objCmd.Parameters["rcl_id_p"].Value.ToString());
            Reclamo _ReclamoIn = new Reclamo();
            _ReclamoIn.rcl_id_p = paramId;

            return Ok(_ReclamoIn);
        }

    }
}