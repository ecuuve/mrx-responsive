﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_INGRESO_ANDEN_TBController : ApiController
    {
        /// <summary>
        /// Lista registros anden 
        /// </summary>
        /// <param name="cia_tiquete_p"></param>
        /// <param name="cia_placa_p"></param>
        /// <returns></returns>
        [Route("api/TX_INGRESO_ANDEN_TB/GetAnden")]
        [ResponseType(typeof(MonitoreoAnden))]
        public IHttpActionResult GetAnden(long? cia_tiquete_p, string cia_placa_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_tiempo_anden_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("cia_tiquete_p", OracleDbType.Int32).Value = cia_tiquete_p;
            objCmd.Parameters.Add("cia_placa_p", OracleDbType.Varchar2).Value = cia_placa_p;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
                DataTable datos2 = new DataTable();
                List<MonitoreoAnden> _MonitoreoAnden = new List<MonitoreoAnden>();

                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _MonitoreoAnden.Add(new MonitoreoAnden
                    {
                        CIA_POSICION = e["CIA_POSICION"].ToString(),
                        CIA_TIQUETE = e["CIA_TIQUETE"].ToString(),
                        CIA_PLACA = e["CIA_PLACA"].ToString(),
                        DESC_AGENCIA = e["DESC_AGENCIA"].ToString(),
                        CIA_HORA_LLEGADA = e["CIA_HORA_LLEGADA"].ToString(),
                        TIEMPO_ATENCION = e["TIEMPO_ATENCION"].ToString()
                    });
                }
                return Ok(_MonitoreoAnden);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/TX_INGRESO_ANDEN_TB/GetRegistros")]
        [ResponseType(typeof(MonitoreoAnden))]
        public IHttpActionResult GetRegistros()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_ut_espera_ing_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("cant_ut_p", OracleDbType.Char, 100).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Si ya  tiene una prealerta registrada envía el mensaje
                return BadRequest(e.Message);
            }

            MonitoreoAnden _monitoreo = new MonitoreoAnden();
            _monitoreo.UT_ESPERA = objCmd.Parameters["cant_ut_p"].Value.ToString();

            return Ok(_monitoreo);
        }
    }
}
