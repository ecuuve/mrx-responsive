﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_MANIFIESTO_DUAS_TBController : ApiController
    {
        public class lista_duas
        {
            public List<Duas> duas { set; get; }
            public string id { set; get; }
        }
        /// <summary>
        /// Inserta los viajes en la tabla manifiesto viajes
        /// </summary>
        /// <param name="viaje"></param>
        /// <returns></returns>
        [Route("api/TX_MANIFIESTO_DUAS_TB/GuardarDua")]
        public IHttpActionResult PostTX_MANIFIESTOS_TB(lista_duas duas)
        {
            if (duas.duas != null)
            {
                string error = "";
                foreach (Duas viaje in duas.duas)
                {
                    //Get the connection string from the app.config            
                    var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                    OracleConnection cn = new OracleConnection(connectionString);
                    cn.Open();
                    OracleCommand objCmd = new OracleCommand();
                    objCmd.Connection = cn;
                    objCmd.CommandText = "mrx.TX_INS_MANIFIESTO_DUAS_PR";
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("MDU_TMA_ID_MANIFIESTO_P", OracleDbType.Int32).Value = viaje.Id;
                    objCmd.Parameters.Add("MDU_DUA_P", OracleDbType.Varchar2).Value = viaje.Nombre;
                    objCmd.Parameters.Add("MDU_USU_REGISTRO_P", OracleDbType.Varchar2).Value = viaje.MDU_USU_REGISTRO;
                    try
                    {
                        objCmd.ExecuteNonQuery();
                        cn.Close();
                    }
                    catch (Exception ex)
                    {
                        error += ex.Message;
                    }
                }
                if (error == "")
                    return Ok(duas.id);
                else
                    return BadRequest(error);
            }
            else
            {
                return Ok(duas.id);
            }
        
        }

        [Route("api/TX_MANIFIESTO_DUAS_TB/Dua")]
        [ResponseType(typeof(Duas))]
        public IHttpActionResult GetDuas(long? id_manifiesto, string dua)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_MANIFIESTO_DUAS_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("MDU_TMA_ID_MANIFIESTO_P", OracleDbType.Int32).Value = id_manifiesto;
            objCmd.Parameters.Add("MDU_DUA_P", OracleDbType.Varchar2).Value = dua;
            objCmd.Parameters.Add("recordset_p", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            try
            {
                objCmd.ExecuteNonQuery();

                DataTable datos2 = new DataTable();
                List<Duas> _duas = new List<Duas>();
                OracleDataAdapter objAdapter = new OracleDataAdapter();
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(datos2);
                cn.Close();
                foreach (DataRow e in datos2.Rows)
                {
                    _duas.Add(new Duas
                    {

                        Id = e["MDU_TMA_ID_MANIFIESTO"].ToString(),
                        Nombre = e["MDU_DUA"].ToString()
                    });
                }
                return Ok(_duas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        //Delete
        [Route("api/TX_MANIFIESTO_DUAS_TB/BorrarDua")]
        public IHttpActionResult PostBorrar(lista_duas duas)
        {
            if (duas.id != null)
            {
                //Get the connection string from the app.config            
                var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
                OracleConnection cn = new OracleConnection(connectionString);
                cn.Open();
                OracleCommand objCmd = new OracleCommand();
                objCmd.Connection = cn;
                objCmd.CommandText = "mrx.TX_DEL_MANIFIESTO_DUAS_PR";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("MDU_TMA_ID_MANIFIESTO_P", OracleDbType.Int32).Value = duas.id;
                objCmd.Parameters.Add("MDU_DUA_P", OracleDbType.Varchar2).Value = "";
                try
                {
                    objCmd.ExecuteNonQuery();
                    cn.Close();
                    return Ok(duas.id);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Ok(duas.id);
            }
        }
           
    }
}
