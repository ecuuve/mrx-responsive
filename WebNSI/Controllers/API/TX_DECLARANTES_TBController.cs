﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_DECLARANTES_TBController : ApiController
    {
        [Route("api/TX_DECLARANTES_TB/GetDeclarantes")]
        [ResponseType(typeof(Declarante))]
        public IHttpActionResult GetDeclarantes(string cod_declarante, string nombre)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_declarantes_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dct_declarante_id_p", OracleDbType.Varchar2).Value = cod_declarante;
            objCmd.Parameters.Add("dct_nombre_p", OracleDbType.Varchar2).Value = nombre;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Declarante> _Declarantes = new List<Declarante>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Declarantes.Add(new Declarante
                {
                    DCT_Id = e["DCT_DECLARANTE_ID"].ToString(),
                    DCT_Cedula = long.Parse(e["DCT_CEDULA"].ToString()),
                    DCT_Nombre = e["DCT_NOMBRE"].ToString(),                  
                    DCT_Email = e["DCT_EMAIL"].ToString(),
                    DCT_Estado = e["DES_ESTADO"].ToString(),
                });
            }
            return Ok(_Declarantes);
        }

        [Route("api/TX_DECLARANTES_TB/GetDeclaranteDetalle")]
        [ResponseType(typeof(Declarante))]
        public IHttpActionResult GetDeclarante(string cod_declarante)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_det_declarantes_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dct_declarante_id_p", OracleDbType.Varchar2).Value = cod_declarante;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Declarante> _Declarantes = new List<Declarante>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow e in datos2.Rows)
            {
                _Declarantes.Add(new Declarante
                {
                    DCT_Id = e["DCT_DECLARANTE_ID"].ToString(),
                    DCT_Nombre = e["DCT_NOMBRE"].ToString(),
                    DCT_Telefono = e["DCT_TELEFONO"].ToString(),
                    DCT_Cedula = long.Parse(e["DCT_CEDULA"].ToString()),
                    DCT_Email = e["DCT_EMAIL"].ToString(),
                    DCT_Estado_Tica = e["DCT_ESTADO_TICA"].ToString(),
                    DCT_Estado = e["DES_ESTADO"].ToString(),
                    DCT_Fch_Registro = e["DCT_FCH_REGISTRO"].ToString(),
                    DCT_Usu_Registro = e["DCT_USU_REGISTRO"].ToString(),

                });
            }
            return Ok(_Declarantes);
        }


        [Route("api/TX_DECLARANTES_TB/GetEstadosTica")]
        [ResponseType(typeof(TipoEstadoTica))]
        public IHttpActionResult GetEstadosTica()
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_lv_dct_estado_tica_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<TipoEstadoTica> _TipoEstadoTica = new List<TipoEstadoTica>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();

            foreach (DataRow r in datos2.Rows)
            {
                _TipoEstadoTica.Add(new TipoEstadoTica
                {
                    Id = r["id"].ToString(),
                    Nombre = r["nombre"].ToString()
                });
            }

            return Ok(_TipoEstadoTica);
        }


        // PUT: api/TX_DECLARANTES_TB/5
        [ResponseType(typeof(Declarante))]
        public IHttpActionResult PutTX_DECLARANTES_TB(string id, Declarante decla_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;

            objCmd.CommandText = "mrx.tx_upd_declarantes_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dct_declarante_id_p", OracleDbType.Varchar2).Value = id;
            objCmd.Parameters.Add("dct_nombre_p", OracleDbType.Varchar2).Value = decla_p.DCT_Nombre;
            objCmd.Parameters.Add("dct_estado_tica_p", OracleDbType.Varchar2).Value = decla_p.DCT_Estado_Tica;
            objCmd.Parameters.Add("dct_telefono_p", OracleDbType.Long).Value = decla_p.DCT_Telefono;
            objCmd.Parameters.Add("dct_email_p", OracleDbType.Varchar2).Value = decla_p.DCT_Email;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();

            Declarante _Declarante = new Declarante();


            return Ok(_Declarante);

        }

        /// <summary>
        /// Insert de declarantes
        /// </summary>
        /// <param name="declarante_p"></param>
        /// <returns></returns>
        [ResponseType(typeof(Declarante))]
        public IHttpActionResult PostTX_DECLARANTES_TB(Declarante declarante_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_ins_declarantes_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("dct_declarante_id_p", OracleDbType.Varchar2).Value = declarante_p.DCT_Id;
            objCmd.Parameters.Add("dct_cedula_p", OracleDbType.Long).Value = declarante_p.DCT_Cedula;
            objCmd.Parameters.Add("dct_nombre_p", OracleDbType.Varchar2).Value = declarante_p.DCT_Nombre;
            objCmd.Parameters.Add("dct_estado_tica_p", OracleDbType.Varchar2).Value = declarante_p.DCT_Estado_Tica;
            objCmd.Parameters.Add("dct_telefono_p", OracleDbType.Long).Value = declarante_p.DCT_Telefono;
            objCmd.Parameters.Add("dct_email_p", OracleDbType.Varchar2).Value = declarante_p.DCT_Email;
            objCmd.Parameters.Add("usu_p", OracleDbType.Varchar2).Value = declarante_p.UserMERX;
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            Declarante _DeclaranteIn = new Declarante();

            return Ok(_DeclaranteIn);
        }

    }
}
