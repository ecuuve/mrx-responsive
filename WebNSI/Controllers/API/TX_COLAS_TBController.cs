﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebNSI.Models;

namespace WebNSI.Controllers.API
{
    public class TX_COLAS_TBController : ApiController
    {

        [Route("api/TX_COLAS_TB/GetColas")]
        [ResponseType(typeof(ColaAforo))]
        public IHttpActionResult GetColas(long? consecutivo, string tipoCola)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.TX_QRY_COLA_AR_AC_PR";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("que_ien_entrada_p", OracleDbType.Int32).Value = consecutivo;
            objCmd.Parameters.Add("que_tipo_cola_p", OracleDbType.Varchar2).Value = tipoCola;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;


            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            DataTable datos2 = new DataTable();
            List<ColaAforo> _RegsColaAforo = new List<ColaAforo>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();



            foreach (DataRow e in datos2.Rows)
            {
                _RegsColaAforo.Add(new ColaAforo
                {
                    QUE_LINEA = e["QUE_LINEA"].ToString(),
                    QUE_IEN_ENTRADA = e["QUE_IEN_ENTRADA"].ToString(),
                    QUE_CONSIGNATARIO = e["QUE_CONSIGNATARIO"].ToString(),
                    QUE_AGA_NAGENCIA_ADUANAL = e["QUE_AGA_NAGENCIA_ADUANAL"].ToString(),
                    AGENCIA = e["AGENCIA"].ToString(),
                    QUE_ESTADO = e["QUE_ESTADO"].ToString(),
                    DESC_ESTADO = e["DESC_ESTADO"].ToString(),
                    QUE_EXAMEN_PREVIO = e["QUE_EXAMEN_PREVIO"].ToString(),
                    QUE_HORA_GENERO = e["QUE_HORA_GENERO"].ToString(),
                    QUE_HORA_INICIO_ATENCION = e["QUE_HORA_INICIO_ATENCION"].ToString(),
                    QUE_HORA_FIN_ATENCION = e["QUE_HORA_FIN_ATENCION"].ToString(),
                    QUE_USU_USUARIO_GENERO = e["QUE_USU_USUARIO_GENERO"].ToString(),
                    QUE_TIPO_COLA = e["QUE_TIPO_COLA"].ToString(),
                    QUE_AUTORIZADO = e["QUE_AUTORIZADO"].ToString()

                });
            }
            foreach (ColaAforo cola in _RegsColaAforo)
            {
                switch (cola.QUE_ESTADO)
                {
                    case "P":
                        cola.PENDIENTE = true;
                        cola.ATENDIDO = false;
                        cola.FINALIZADO = false;
                        break;
                    case "A":
                        cola.PENDIENTE = false;
                        cola.ATENDIDO = true;
                        cola.FINALIZADO = false;
                        break;
                    case "F":
                        cola.PENDIENTE = false;
                        cola.ATENDIDO = false;
                        cola.FINALIZADO = true;
                        break;
                    default:
                        //Console.WriteLine("Default case");
                        break;
                }
            }
            return Ok(_RegsColaAforo);
        }

        [Route("api/TX_COLAS_TB/GetColasConsulta")]
        [ResponseType(typeof(ColaAforo))]
        public IHttpActionResult GetColasConsulta(long? consecutivo, string tipoCola)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_cola_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("que_tipo_cola_p", OracleDbType.Varchar2).Value = tipoCola;
            objCmd.Parameters.Add("que_ien_entrada_p", OracleDbType.Int32).Value = consecutivo;           
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            DataTable datos2 = new DataTable();
            List<ColaAforo> _RegsCola = new List<ColaAforo>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();



            foreach (DataRow e in datos2.Rows)
            {
                _RegsCola.Add(new ColaAforo
                {
                    QUE_LINEA = e["QUE_LINEA"].ToString(),
                    QUE_IEN_ENTRADA = e["QUE_IEN_ENTRADA"].ToString(),
                    MANIFIESTO = e["MANIFIESTO"].ToString(),
                    GUIA = e["GUIA"].ToString(),
                    QUE_CONSIGNATARIO = e["QUE_CONSIGNATARIO"].ToString(),
                    QUE_AGA_NAGENCIA_ADUANAL = e["QUE_AGA_NAGENCIA_ADUANAL"].ToString(),
                    AGENCIA = e["AGENCIA"].ToString(),
                    DESC_ESTADO = e["DESC_ESTADO"].ToString(),
                    QUE_EXAMEN_PREVIO = e["QUE_EXAMEN_PREVIO"].ToString(),
                    QUE_HORA_GENERO = e["QUE_HORA_GENERO"].ToString(),
                    QUE_HORA_INICIO_ATENCION = e["QUE_HORA_INICIO_ATENCION"].ToString(),
                    QUE_HORA_FIN_ATENCION = e["QUE_HORA_FIN_ATENCION"].ToString(),
                    QUE_USU_USUARIO_GENERO = e["QUE_USU_USUARIO_GENERO"].ToString(),
                    QUE_TIPO_COLA = e["QUE_TIPO_COLA"].ToString(),
                 
                });
            }
            foreach (ColaAforo cola in _RegsCola)
            {
                switch (cola.QUE_ESTADO)
                {
                    case "P":
                        cola.PENDIENTE = true;
                        cola.ATENDIDO = false;
                        cola.FINALIZADO = false;
                        break;
                    case "A":
                        cola.PENDIENTE = false;
                        cola.ATENDIDO = true;
                        cola.FINALIZADO = false;
                        break;
                    case "F":
                        cola.PENDIENTE = false;
                        cola.ATENDIDO = false;
                        cola.FINALIZADO = true;
                        break;
                    default:
                        //Console.WriteLine("Default case");
                        break;
                }
            }
            return Ok(_RegsCola);
        }

        [Route("api/TX_COLAS_TB/GetColasCarrusel")]
        [ResponseType(typeof(ColaAforo))]
        public IHttpActionResult GetColasCarrusel(long? consecutivo)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_cola_sc_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("que_ien_entrada_p", OracleDbType.Int32).Value = consecutivo;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            DataTable datos2 = new DataTable();
            List<ColaAforo> _RegsCola = new List<ColaAforo>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();



            foreach (DataRow e in datos2.Rows)
            {
                _RegsCola.Add(new ColaAforo
                {
                    QUE_LINEA = e["QUE_LINEA"].ToString(),
                    QUE_IEN_ENTRADA = e["QUE_IEN_ENTRADA"].ToString(),
                    QUE_CONSIGNATARIO = e["QUE_CONSIGNATARIO"].ToString(),
                    QUE_AGA_NAGENCIA_ADUANAL = e["QUE_AGA_NAGENCIA_ADUANAL"].ToString(),
                    AGENCIA = e["AGENCIA"].ToString(),
                    QUE_ESTADO = e["QUE_ESTADO"].ToString(),
                    DESC_ESTADO = e["DESC_ESTADO"].ToString(),
                    QUE_EXAMEN_PREVIO = e["QUE_EXAMEN_PREVIO"].ToString(),
                    QUE_HORA_GENERO = e["QUE_HORA_GENERO"].ToString(),
                    QUE_HORA_INICIO_ATENCION = e["QUE_HORA_INICIO_ATENCION"].ToString(),
                    QUE_HORA_FIN_ATENCION = e["QUE_HORA_FIN_ATENCION"].ToString(),
                    QUE_USU_USUARIO_GENERO = e["QUE_USU_USUARIO_GENERO"].ToString(),
                    QUE_AUTORIZADO = e["QUE_AUTORIZADO"].ToString(),
                    QUE_TIPO_COLA = e["QUE_TIPO_COLA"].ToString()

                });
            }
            foreach (ColaAforo cola in _RegsCola)
            {
                switch (cola.QUE_ESTADO)
                {
                    case "P":
                        cola.PENDIENTE = true;
                        cola.ATENDIDO = false;
                        cola.FINALIZADO = false;
                        break;
                    case "A":
                        cola.PENDIENTE = false;
                        cola.ATENDIDO = true;
                        cola.FINALIZADO = false;
                        break;
                    case "F":
                        cola.PENDIENTE = false;
                        cola.ATENDIDO = false;
                        cola.FINALIZADO = true;
                        break;
                    default:
                        //Console.WriteLine("Default case");
                        break;
                }
            }

            return Ok(_RegsCola);
        }

        [Route("api/TX_COLAS_TB/GetColasHistorico")]
        [ResponseType(typeof(ColaHistorico))]
        public IHttpActionResult GetColasHistorico(long? consecutivo, string tipoCola)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_cola_hist_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("quh_tipo_cola_p", OracleDbType.Varchar2).Value = tipoCola;
            objCmd.Parameters.Add("quh_ien_entrada_p", OracleDbType.Int32).Value = consecutivo;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<ColaHistorico> _RegsColaHis = new List<ColaHistorico>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();



            foreach (DataRow e in datos2.Rows)
            {
                _RegsColaHis.Add(new ColaHistorico
                {
                    QUH_IEN_ENTRADA = e["QUH_IEN_ENTRADA"].ToString(),
                    QUH_HORA_GENERO = e["QUH_HORA_GENERO"].ToString(),
                    USU_GENERO = e["USU_GENERO"].ToString(),
                    QUH_HORA_INICIO_ATENCION = e["QUH_HORA_INICIO_ATENCION"].ToString(),
                    QUH_HORA_FIN_ATENCION = e["QUH_HORA_FIN_ATENCION"].ToString(),
                    USU_ATENDIO = e["USU_ATENDIO"].ToString(),
                    MANIFIESTO = e["MANIFIESTO"].ToString(),
                    GUIA = e["GUIA"].ToString(),
                    QUH_CONSIGNATARIO = e["QUH_CONSIGNATARIO"].ToString(),
                    QUH_AGA_NAGENCIA_ADUANAL = e["QUH_AGA_NAGENCIA_ADUANAL"].ToString(),
                    AGENCIA = e["AGENCIA"].ToString(),
                    DESC_TIP_EXAMEN = e["DESC_TIP_EXAMEN"].ToString(),
                    QUH_TIPO_EXAMEN = e["QUH_TIPO_EXAMEN"].ToString(),
                    AGENTE = e["AGENTE"].ToString(),
                    NOM_IMPORTADOR = e["NOM_IMPORTADOR"].ToString(),              
                });
            }
            
            return Ok(_RegsColaHis);
        }

        [Route("api/TX_COLAS_TB/GetMercCola")]
        [ResponseType(typeof(Tarima_Ing))]
        public IHttpActionResult GetColasMercAR(long? consecutivo)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.tx_qry_merc_cola_pr";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("que_ien_entrada_p", OracleDbType.Int32).Value = consecutivo;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            objCmd.ExecuteNonQuery();

            DataTable datos2 = new DataTable();
            List<Tarima_Ing> _MercColas = new List<Tarima_Ing>();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(datos2);

            cn.Close();



            foreach (DataRow e in datos2.Rows)
            {
                _MercColas.Add(new Tarima_Ing
                {
                    MER_ID_MERCANCIA = long.Parse(e["MER_ID_MERCANCIA"].ToString()),
                    MER_BULTOS_DESPALETIZADOS = decimal.Parse(e["MER_BULTOS_DESPALETIZADOS"].ToString()),
                    MER_REFRIGERADO = e["MER_REFRIGERADO"].ToString(),
                    MER_LOC_RACK = decimal.Parse(e["MER_LOC_RACK"].ToString()),
                    MER_LOC_COLUMNA = decimal.Parse(e["MER_LOC_COLUMNA"].ToString()),
                    MER_LOC_LADO = decimal.Parse(e["MER_LOC_LADO"].ToString()),
                    MER_LOC_ALTURA = decimal.Parse(e["MER_LOC_ALTURA"].ToString())//
                    
                });
            }
            return Ok(_MercColas);
        }

        // PUT: 
        [ResponseType(typeof(ColaAforo))]
        public IHttpActionResult PutTX_COLAS_TB(long linea_p, long consecutivo_p, string tipo_p, string user_p, string accion_p)
        {
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandType = CommandType.StoredProcedure;

            if (accion_p == "AT")
            {
                objCmd.CommandText = "mrx.TX_UPD_ATENDER_COLA_PR";              
                objCmd.Parameters.Add("que_linea_p", OracleDbType.Int32).Value = linea_p;
                objCmd.Parameters.Add("que_ien_entrada_p", OracleDbType.Int32).Value = consecutivo_p;
                objCmd.Parameters.Add("que_tipo_cola_p", OracleDbType.Varchar2).Value = tipo_p;
                objCmd.Parameters.Add("que_user_merx_p", OracleDbType.Varchar2).Value = user_p;

            }
            if (accion_p == "FN")
            {
                objCmd.CommandText = "mrx.TX_UPD_FIN_COLA_PR";
                objCmd.Parameters.Add("que_linea_p", OracleDbType.Int32).Value = linea_p;
                objCmd.Parameters.Add("que_ien_entrada_p", OracleDbType.Int32).Value = consecutivo_p;
                objCmd.Parameters.Add("que_tipo_cola_p", OracleDbType.Varchar2).Value = tipo_p;
            }
            if (accion_p == "TM")
            {
                objCmd.CommandText = "mrx.TX_UPD_TERMINA_COLA_PR";
                objCmd.Parameters.Add("consecutivo_p", OracleDbType.Int32).Value = consecutivo_p;
                objCmd.Parameters.Add("tipo_cola_p", OracleDbType.Varchar2).Value = tipo_p;
            }
            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }

            cn.Close();
            ColaAforo _ColaUp = new ColaAforo();
            return Ok(_ColaUp);

        }

    }
}
