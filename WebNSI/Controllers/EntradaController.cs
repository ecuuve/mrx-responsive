﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebNSI.Controllers
{
    public class EntradaController : Controller
    {
        // GET: Entrada
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ReimpresionEtiqueta()
        {
            return View();
        }

        public ActionResult Etiqueta(string tarima)
        {
            //1966743
            //Get the connection string from the app.config            
            var connectionString = ConfigurationManager.ConnectionStrings["NSI_CONN"].ConnectionString;
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();
            OracleCommand objCmd = new OracleCommand();
            objCmd.Connection = cn;
            objCmd.CommandText = "mrx.etiquetamta";
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("pTarima", OracleDbType.Int32).Value = tarima;
            objCmd.Parameters.Add("Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            try
            {
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }

            DataTable data = new DataTable();

            OracleDataAdapter objAdapter = new OracleDataAdapter();
            objAdapter.SelectCommand = objCmd;
            objAdapter.Fill(data);
            cn.Close();

            EtiquetaModel model = new EtiquetaModel();
            if (data.Rows.Count > 0)
            {
                DataRow row = data.Rows[0];
                model = new EtiquetaModel()
                {
                    Manifiesto = row["MANIFIESTO"].ToString(),
                    Guia = row["GUIA"].ToString(),
                    LineaAerea = row["LINEA_AEREA"].ToString(),
                    Importador = row["IMPORTADOR"].ToString(),
                    Descripcion = row["DESCRIPCION"].ToString(),
                    Observacion = row["OBSERVACION"].ToString(),
                    Ingreso = row["FECHA_INGRESO"].ToString(),
                    Consl = row["CONSOLIDADOR"].ToString(),
                    Movimiento = row["MOVIMIENTO"].ToString(),
                    barcodeValue = row["TARIMA"].ToString(),
                    barcodeImage = "/Entrada/BarCode?d=" + row["TARIMA"].ToString(),
                    Bultos = row["BULTOS"].ToString(),
                    Kilos = row["KILOS"].ToString(),
                    valor = row["guia_original"].ToString()
                };
            }
            return View(model);
        }

        public ActionResult BarCode(string d)
        {
            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
            b.IncludeLabel = false;
            b.LabelPosition = BarcodeLib.LabelPositions.TOPCENTER;

            BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39;
            string strData = d;
            int imageWidth = 200;
            int imageHeight = 150;

            Image barcodeImage = b.Encode(type, strData.Trim(), System.Drawing.ColorTranslator.FromHtml("#000000"), System.Drawing.ColorTranslator.FromHtml("#FFFFFF"), imageWidth, imageHeight);

            barcodeImage = cropImage(barcodeImage,imageWidth, imageHeight / 2);

            MemoryStream ms = new MemoryStream();

            barcodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

            return File(ms.ToArray(), "image/png");
        }

        private static Image cropImage(Image img, int width, int height)
        {
            Rectangle cropArea = new Rectangle(0, 0, width, height);
            Bitmap bmpImage = new Bitmap(img);
            return bmpImage.Clone(cropArea, bmpImage.PixelFormat);
        }
    }

    public class EtiquetaModel
    {
        public string Manifiesto { get; set; }
        public string Guia { get; set; }
        public string LineaAerea { get; set; }
        public string Importador { get; set; }
        public string Descripcion { get; set; }
        public string Observacion { get; set; }
        public string Ingreso { get; set; }
        public string Consl { get; set; }
        public string Movimiento { get; set; }
        public string barcodeValue { get; set; }
        public string barcodeImage { get; set; }
        public string Bultos { get; set; }
        public string Kilos { get; set; }
        public string valor { get; set; }

    }
}