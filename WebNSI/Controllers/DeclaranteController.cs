﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebNSI.Controllers
{
    public class DeclaranteController : Controller
    {
        // GET: Declarante
        public ActionResult Index(string id)
        {
            if (id != null)
            {
                string[] tuplas = id.Split(',');
                foreach (string tuplasdet in tuplas)
                {
                    if (tuplasdet.ToString() != "")
                    {
                        if (ViewBag.flr_iddeclarante == null)
                        {
                            ViewBag.flr_iddeclarante = tuplasdet.ToString();
                        }
                        else
                        {
                            ViewBag.flr_nombredeclarante = tuplasdet.ToString();

                        }

                    }

                }

                return View();
            }
            else
            {
                return View();
            }
            
           
        }

        public ActionResult Funcionarios(string id)
        {
            string[] tuplas = id.Split(',');
            foreach (string tuplasdet in tuplas)
            {
                if (tuplasdet.ToString() != "")
                {
                    if (ViewBag.Declarante == null)
                    {
                        ViewBag.Declarante = tuplasdet.ToString();
                    }
                    else
                    {
                        if (ViewBag.NombreDeclarante == null)
                        {
                            ViewBag.NombreDeclarante = tuplasdet.ToString();
                        }
                        else
                        {
                            if (ViewBag.flr_iddeclarante == null)
                            {
                                ViewBag.flr_iddeclarante = tuplasdet.ToString();
                            }
                            else
                            {
                                ViewBag.flr_nombredeclarante = tuplasdet.ToString();
                            }

                        }
                       
                    }

                }

            }
            return View();
        }
    }
}