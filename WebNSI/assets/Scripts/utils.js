﻿function CargaInicial() {
    $('#TxtPass1').on('input', function (e) {
        validaPass1()
    });
    $('#TxtPass2').on('input', function (e) {
        validaClaves()
    });
    $('#Pass1ok').hide();
    $('#Pass1error').hide();
    $('#Pass2Validate').hide();
    $('#Pass2Error').hide();

    $("#Btn_Aplicar").prop("disabled", true);
}

function openModal(vclave) {
    $('#LblModalId').text = vclave;
    $('#CambioPass').modal('show');
}

function validaClaves()
{
    var txt1 = $('#TxtPass1').val();
    var txt2 = $('#TxtPass2').val();

    if (txt1 != txt2) {
        $('#Pass2Validate').hide();
        $('#Pass2Error').show();
        $("#Btn_Aplicar").prop("disabled", true);
    }
    else {
        $('#Pass2Validate').show();
        $('#Pass2Error').hide();
        $("#Btn_Aplicar").prop("disabled", false);
    };

}

function validaPass1() {
    var txt1 = $('#TxtPass1').val();

    if (txt1.length < 6) {
        $('#Pass1ok').hide();
        $('#Pass1error').show();
        $("#Btn_Aplicar").prop("disabled", true);
    }
    else {
        $('#Pass1ok').show();
        $('#Pass1error').hide();
        $("#Btn_Aplicar").prop("disabled", false);
    };
    validaClaves();
}

