var bloqueoGuia = {

    init: function () {
      TecladoFuncionalidadPersonalizada();
        bloqueoGuia.LimpiarCamposBloqueos();
        bloqueoGuia.BLO_CargarControlesPrevios();
        
        $("#BLO_IEN_ENTRADA").attr('disabled', true);
        $("#BLO_ACTA").attr('disabled', true);
        $("#s_BLO_VIA_SOLICITUD").attr('disabled', true);
        $("#s_BLO_TIPO_BLOQUEO").attr('disabled', true);
        $("#BLO_FACTURACION").attr('disabled', true);
        $("#BLO_PREVIOS").attr('disabled', true);
        $("#BLO_ORDENES_DESP").attr('disabled', true);

        $("#btn_actualizar_bloqueo").attr('style', 'display:none');
        $("#btn_registrar_bloqueo").attr('style', 'display:none');
        $("#btn_registrar_PREbloqueo").attr('style', 'display:none');
        $("#btn_buscar_bloqueos").attr('style', 'display:none');
        $("#btn_nuevo_bloqueo").attr('style', 'display:block');
        
        $("#div_filters").attr('style', 'display:block');
        $("#div_filters_consulta").attr('style', 'display:none');
        $("#div_desbloqueo").attr('style', 'display:none');
        $("#div_bloqueo").attr('style', 'display:none');

        // Cuando hace click en el tab de bloqueo
        $('#bloqueo-tab').on('click', function () { 
             $("#in_flr_guiaoriginal").focus();        
            // si estaba en ingreso
          if ( $("#div_filters_consulta").css('display') == 'block'){
              $("#btn_actualizar_bloqueo").attr('style', 'display:none');
              $("#btn_registrar_bloqueo").attr('style', 'display:block');
              $("#btn_registrar_PREbloqueo").attr('style', 'display:none');
              $("#btn_buscar_bloqueos").attr('style', 'display:block');
              $("#btn_nuevo_bloqueo").attr('style', 'display:none');

          }

            //si estaba en consulta
            if ( $("#div_filters").css('display') == 'block'){
              if($("#in_flr_guiaoriginal").val() != ""){
               $("#btn_actualizar_bloqueo").attr('style', 'display:block');
              }else{
               $("#btn_actualizar_bloqueo").attr('style', 'display:none');
              }
              $("#btn_buscar_bloqueos").attr('style', 'display:none');
              $("#btn_registrar_bloqueo").attr('style', 'display:none');
              $("#btn_registrar_PREbloqueo").attr('style', 'display:none');        
              $("#btn_nuevo_bloqueo").attr('style', 'display:block');
            }
          
        });

        // Cuando hace click en el tab de PREbloqueo
        $('#prebloqueo-tab').on('click', function () { 
            $("#btn_registrar_PREbloqueo").attr('style', 'display:block');
            $("#btn_actualizar_bloqueo").attr('style', 'display:none');
            $("#btn_registrar_bloqueo").attr('style', 'display:none');           
            $("#btn_buscar_bloqueos").attr('style', 'display:none');
            $("#btn_nuevo_bloqueo").attr('style', 'display:none'); 

            $("#IPB_GUIA").focus();                      
        });





        //Disparo evento de consulta de Consecutivo cuando lo digitan
        $('#BLO_IEN_ENTRADA').change(function () {
            var cod_p = $('#BLO_IEN_ENTRADA').val();
            bloqueoGuia.BLO_ConsultarDatosDetalleConse(cod_p);
        });
        // $('#BLO_IEN_ENTRADA').bind("enterKey",function(e){
        //   var cod_p = $('#BLO_IEN_ENTRADA').val();
        //   bloqueoGuia.BLO_ConsultarDatosDetalleConse(cod_p);
        // });
        // $('#BLO_IEN_ENTRADA').keyup(function(e){
        //   if(e.keyCode == 13)
        //   {
        //     $(this).trigger("enterKey");
        //   }
        // });


         $("#in_flr_guiaoriginal").focus();

        $('#in_flr_guiaoriginalIn').bind("enterKey",function(e){
          var p_guia = $("#in_flr_guiaoriginalIn").val();
           bloqueoGuia.BLO_ConsultarDatosConsecutivos(p_guia);
        });
        $('#in_flr_guiaoriginalIn').keyup(function(e){
          if(e.keyCode == 13)
          {
            $(this).trigger("enterKey");
          }
        });


        $('#IPB_CSG_CONSIGNATARIO').bind("enterKey",function(e){
           bloqueoGuia.BLO_ConsultarDatosConsignatarios();
        });
        $('#IPB_CSG_CONSIGNATARIO').keyup(function(e){
          if(e.keyCode == 13)
          {
            $(this).trigger("enterKey");
          }
        });

        $('#BLO_PREVIOS').prop('checked', true);
        $('#BLO_ORDENES_DESP').prop('checked', true);
        $('#BLO_FACTURACION').prop('checked', true);



    },


    onfocus: function () {
      if ($("#lbl_flr_guiaoriginal").hasClass('lblfocus'))
          $("#lbl_flr_guiaoriginal").removeClass('lblfocus');
      if ($("#lbl_flr_guiaoriginalIn").hasClass('lblfocus'))
            $("#lbl_flr_guiaoriginalIn").removeClass('lblfocus');
      if ($("#lbl_IPB_GUIA").hasClass('lblfocus'))
            $("#lbl_IPB_GUIA").removeClass('lblfocus');   
    },

    //Carga las listas de valores en los select
    BLO_CargarControlesPrevios: function () {
        bloqueoGuia.getTipoBloqueo();
        bloqueoGuia.getViaSolicitud(); 
        $('#BLO_PREVIOS').prop('checked', true);
        $('#BLO_ORDENES_DESP').prop('checked', true);
        $('#BLO_FACTURACION').prop('checked', true);      
    },

    //Obtiene la lista de valores para el select
    getViaSolicitud: function (valor) {    
        principal.getData(
            "/api/TX_BLOQUEOS_TB_Guias/ViaSolicitud",
            function (data) {
                $("#s_BLO_VIA_SOLICITUD").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        );
    },

    //Obtiene la lista de valores para el select
    getTipoBloqueo: function (valor) {
        principal.getData(
            "/api/TX_BLOQUEOS_TB_Guias/TipoBloqueo",
            function (data) {
                $("#s_BLO_TIPO_BLOQUEO").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        );
    },

    //Búsqueda de bloqueos
    BLO_BuscarBloqueo: function(){
       var p_guiaoriginal = $("#in_flr_guiaoriginal").val();
       var p_consecutivo = $("#in_flr_consecutivo").val();
      
       bloqueoGuia.BLO_ConsultarDatos(p_consecutivo,p_guiaoriginal);
    },

    // Método que va al api a consultar los bloqueos según filtro
    BLO_ConsultarDatos:function(p_consecutivo, p_guiaoriginal){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_BLOQUEOS_TB_Guias/GetDatosGuiaBloqueo?entrada_p='+p_consecutivo+ '&ien_guia_original_p='+p_guiaoriginal,
            //data: postdata,
            success: function (data) {
                if (data !== null) {
                  
                  if(data.length > 1){

                   bloqueoGuia.BLO_Listar(data);

                  }else{

                    if(data["BLO_IEN_ENTRADA"] != 0){
                       //trae los datos de la queja
                       principal.activeLabels();
                       bloqueoGuia.BLO_MuestraDatos(data);                                        
                    }
                    else{

                      swal({
                        title: "Bloqueos",
                        text: "No existen datos para los valores consultados.",
                        icon: "error",
                        button: "OK!",
                      })

                    }
                  }                   
                }
            },
            failure: function (data) {
                
                //console.log("fail");
                if (data.responseJSON.Message == "false") {
                    prod_nc.infoConsecutivo(consecutivo, sistema);
                } else {
                    swal({
                        title: "Bloqueo",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            },
            error: function (data) {
                if (data.responseJSON.Message == "false") {
                    prod_nc.infoConsecutivo(consecutivo, sistema);
                } else {
                    swal({
                        title: "Bloqueos",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            }
        });
    },

    //Consulta el detalle del bloqueo para mostrar los datos
    BLO_ConsultarDatosDetalle:function(p_consecutivo, p_id){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_BLOQUEOS_TB_Guias/GetDatosBloqueoDetalle?blo_ien_entrada_p='+p_consecutivo+ '&blo_id_bloqueo_p='+p_id,
            success: function (data) {
                if (data !== null) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Bloqueos",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }
                    else{
                      if(data["BLO_IEN_ENTRADA"] != 0){
                         //trae los datos de la queja
                         principal.activeLabels();
                         bloqueoGuia.BLO_MuestraDatos(data);
                                           
                      }
                      else{

                        swal({
                          title: "Bloqueos",
                          text: "No existen datos para los valores consultados.",
                          icon: "error",
                          button: "OK!",
                        })

                      }
                    }                  
                }
            },
            failure: function (data) {
                
                if (data.responseJSON.Message == "false") {
                    prod_nc.infoConsecutivo(consecutivo, sistema);
                } else {
                    swal({
                        title: "Bloqueos",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            },
            error: function (data) {

                if (data.responseJSON.Message == "false") {
                    prod_nc.infoConsecutivo(consecutivo, sistema);
                } else {
                    swal({
                        title: "Bloqueos",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            }
        });
    },

    //Carga el modal de bloqueos en caso de que sean varios
    BLO_Listar: function (data) {
        var templateText = $("#BLO_Bloqueos_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#BLO_Bloqueos_tablebody").html('');
         
          $.each(data, function (index, elem) {
              $("#BLO_Bloqueos_tablebody").append(tableTemplate(elem));
          }) 

         $("#myModalBloqueos").modal('show');
    },

    //Muestra los datos del bloqueo consultado o seleccionado en pantalla
    BLO_MuestraDatos: function (p_bloqueo) {
        for (var campo in p_bloqueo[0]) {    
            
            //si hay info de bloqueo muestra los campos       
              if (campo == "BLO_MOTIVO_BLOQUEO" && p_bloqueo[0][campo] != "" ){
                $("#div_bloqueo").attr('style', 'display:block');

                 $("#btn_actualizar_bloqueo").attr('style', 'display:block');
                 $("#btn_registrar_bloqueo").attr('style', 'display:none'); 
                 $("#btn_registrar_PREbloqueo").attr('style', 'display:none');
                 $("#btn_buscar_bloqueos").attr('style', 'display:none');
                 $("#btn_nuevo_bloqueo").attr('style', 'display:block'); 

                 $("#BLO_MOTIVO_BLOQUEO").attr('disabled', true);
                  $("#USUARIO_BLOQUEA").attr('style', 'display:block');
                  $("#lbl_USUARIO_BLOQUEA").attr('style', 'display:block');
                  $("#BLO_FCH_BLOQUEO").attr('style', 'display:block');
                  $("#lbl_BLO_FCH_BLOQUEO").attr('style', 'display:block');


              }
              //Si hay info de desbloqueo muestra los campos y bloquea botón de update
              if (campo == "BLO_MOTIVO_DESBLOQUEO"){
                 if(p_bloqueo[0][campo] != ""){

                   $("#btn_actualizar_bloqueo").attr('style', 'display:none');
                   $("#btn_registrar_bloqueo").attr('style', 'display:none'); 
                   $("#btn_registrar_PREbloqueo").attr('style', 'display:none');
                   $("#btn_buscar_bloqueos").attr('style', 'display:none');
                   $("#btn_nuevo_bloqueo").attr('style', 'display:block');

                   $("#div_desbloqueo").attr('style', 'display:block');

                   $("#BLO_MOTIVO_DESBLOQUEO").attr('disabled', true);
                   $("#USUARIO_DESBLOQUEA").attr('style', 'display:block');
                   $("#lbl_USUARIO_DESBLOQUEA").attr('style', 'display:block');
                   $("#BLO_FCH_DESBLOQUEO").attr('style', 'display:block');
                   $("#lbl_BLO_FCH_DESBLOQUEO").attr('style', 'display:block');

                 }else{
                   $("#btn_actualizar_bloqueo").attr('style', 'display:block');
                   $("#btn_registrar_bloqueo").attr('style', 'display:none'); 
                   $("#btn_registrar_PREbloqueo").attr('style', 'display:none');
                   $("#btn_buscar_bloqueos").attr('style', 'display:block');
                   $("#btn_nuevo_bloqueo").attr('style', 'display:block');

                   $("#div_desbloqueo").attr('style', 'display:block');

                   $("#BLO_MOTIVO_DESBLOQUEO").attr('disabled', false);
                   $("#USUARIO_DESBLOQUEA").attr('style', 'display:none');
                   $("#lbl_USUARIO_DESBLOQUEA").attr('style', 'display:none');
                   $("#BLO_FCH_DESBLOQUEO").attr('style', 'display:none');
                   $("#lbl_BLO_FCH_DESBLOQUEO").attr('style', 'display:none');
                 }

              }

            if(p_bloqueo[0][campo] != null){
                principal.setValueByName(campo, p_bloqueo[0][campo],'frm_bloqueo');
              }
            }               
    },

    //Llama al método que consulta detalle del bloqueo seleccionado
    BLO_VerDetalle: function(prm_conse, prm_id){
       var p_consecutivo = prm_conse;
       var p_id = prm_id;
       bloqueoGuia.BLO_ConsultarDatosDetalle(p_consecutivo,p_id);
       $("#myModalBloqueos").modal('hide');
    },

    // Limpia campos de los formularios
    LimpiarCamposBloqueos: function () {
        principal.resetAllFields('detInfo_bloqueo');
        principal.resetAllFields('detInfo_prebloqueo');      
        principal.resetAllFields('filtros');
        TecladoFuncionalidadPersonalizada();
        principal.deactivateLabels();
        bloqueoGuia.BLO_CargarControlesPrevios();
        $("#btn_actualizar_bloqueo").attr('style', 'display:none');
        $("#in_flr_guiaoriginal").focus();
        //$("#in_flr_guiaoriginal").focus();
    },

    //Funcionalidad botón nuevo bloqueo
    BLO_Nuevo: function(){
      $("#div_filters").attr('style', 'display:none');
      $("#div_filters_consulta").attr('style', 'display:block');

      $("#div_bloqueo").attr('style', 'display:block');
      $("#div_desbloqueo").attr('style', 'display:none');

      $("#USUARIO_BLOQUEA").attr('style', 'display:none');
      $("#lbl_USUARIO_BLOQUEA").attr('style', 'display:none');
      $("#BLO_FCH_BLOQUEO").attr('style', 'display:none');
      $("#lbl_BLO_FCH_BLOQUEO").attr('style', 'display:none');
      $("#BLO_MOTIVO_BLOQUEO").attr('disabled', false);
      $("#BLO_MOTIVO_DESBLOQUEO").attr('disabled', false);
       
      $("#BLO_IEN_ENTRADA").attr('disabled', false);
      $("#BLO_ACTA").attr('disabled', false);
      $("#s_BLO_VIA_SOLICITUD").attr('disabled', false);
      $("#s_BLO_TIPO_BLOQUEO").attr('disabled', false);
      $("#BLO_FACTURACION").attr('disabled', false);
      $("#BLO_PREVIOS").attr('disabled', false);
      $("#BLO_ORDENES_DESP").attr('disabled', false);

        $("#btn_buscar_bloqueos").attr('style', 'display:block');
        $("#btn_actualizar_bloqueo").attr('style', 'display:none');
        $("#btn_registrar_bloqueo").attr('style', 'display:block');
        $("#btn_registrar_PREbloqueo").attr('style', 'display:none');        
        $("#btn_nuevo_bloqueo").attr('style', 'display:none');

        bloqueoGuia.LimpiarCamposBloqueos();
    },

    //Funcionalidad botón guardar bloqueo
    BLO_GuardarBloqueo: function(){
      $("#USER_MERX").val($("#UsReg").val());        
      $('[name= "BLO_FACTURACION"]').is(":checked") == true ? $('[name= "BLO_FACTURACION"]').val('S') : $('[name= "BLO_FACTURACION"]').val('N');
      $('[name= "BLO_PREVIOS"]').is(":checked") == true ? $('[name= "BLO_PREVIOS"]').val('S') : $('[name= "BLO_PREVIOS"]').val('N');
      $('[name= "BLO_ORDENES_DESP"]').is(":checked") == true ? $('[name= "BLO_ORDENES_DESP"]').val('S') : $('[name= "BLO_ORDENES_DESP"]').val('N');
      $.post(
      "/api/TX_BLOQUEOS_TB_Guias/",
       $('#frm_bloqueo').serialize(),
      function (data) {
          if (typeof data === 'string' || data instanceof String){
              swal({
                title: "Registro de Bloqueos",
                text: data,
                icon: "error",
                button: "OK!",
              })
          }else{
              swal({
              title: "Registro de Bloqueos",
              text: "Bloqueo guardado exitosamente.",
              icon: "success",
              button: "OK!",

              }).then((value) => {
                  bloqueoGuia.LimpiarCamposBloqueos();
              });
          }
      })
      .fail(function (data) {

          if (data.responseJSON.Message == "false") {
              prod_nc.infoConsecutivo(consecutivo, sistema);
          } else {
              swal({
                  title: "Bloqueos",
                  text: data.responseJSON.Message,
                  icon: "error",
                  button: "OK!",

              });
          }
      });
    },

     //Funcionalidad botón guardar Prebloqueo
    BLO_GuardarPREBloqueo: function(){ 
      $("#USER_MERXPRE").val($("#UsReg").val());       
      $.post(
      "/api/TX_PRE_BLOQUEOS_TB/",
       $('#frm_prebloqueo').serialize(),
      function (data) {
          if (typeof data === 'string' || data instanceof String){
              swal({
                title: "Registro de PRE-Bloqueos",
                text: data,
                icon: "error",
                button: "OK!",
              })
          }else{
              swal({
              title: "Registro de PRE-Bloqueos",
              text: "PRE-Bloqueo guardado exitosamente.",
              icon: "success",
              button: "OK!",

              }).then((value) => {
                  bloqueoGuia.LimpiarCamposBloqueos();
              });
          }
      })
      .fail(function (data) {

          if (data.responseJSON.ModelState) {
              principal.processErrorsPopUp(data.responseJSON.ModelState);
          }
      });
    },

    //Funcionalidad botón busqueda de bloqueos
    BLO_Busqueda: function(){
      bloqueoGuia.init();
    },

    //Funcionalidad botón bloqueo
    BLO_ActualizarBloqueo: function(){
      $("#USER_MERX").val($("#UsReg").val());
        $("#BLO_IEN_ENTRADA").attr('disabled', false);
        $("#BLO_ID_BLOQUEO").attr('disabled', false);
        $("#BLO_MOTIVO_DESBLOQUEO").attr('disabled', false);
        var id = $('#BLO_ID_BLOQUEO').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_bloqueo').serializeArray());
            $("#BLO_IEN_ENTRADA").attr('disabled', true);
            $("#BLO_ID_BLOQUEO").attr('disabled', true);
            $("#BLO_MOTIVO_DESBLOQUEO").attr('disabled', true);
            $.ajax({
                url: '/api/TX_BLOQUEOS_TB_Guias/' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                //dataType: 'json',
                contentType: "application/json",
                success: function (data) {
                   // if (typeof data === 'string' || data instanceof String){
                    swal({
                    title: "Bloqueos",
                    text: "Se actualizó correctamente la información",
                    icon: "success",
                    button: "OK!",

                    }).then((value) => {
                       bloqueoGuia.init();
                    });
                },
                error: function (data) {
                  swal({
                          title: "Actualización de Bloqueos",
                          text: data.responseJSON.Message,
                          icon: "error",
                          button: "OK!",
                        })
                  }
            })
        }
        else {
            swal({
                title: "Quejas",
                text: "El ID de la queja no puede ser vacío",
                icon: "error",
                button: "OK!",

            }).then((value) => {
              $("#BLO_IEN_ENTRADA").attr('disabled', true);
              $("#BLO_ID_BLOQUEO").attr('disabled', true);
              $("#BLO_MOTIVO_DESBLOQUEO").attr('disabled', true);           
            });
        }
    },

    //Funcionalidad del botón busqueda de consecutivo según guia original para ingreso de bloqueo
    BLO_BuscarConsecutivo: function(){
      var p_guia = $("#in_flr_guiaoriginalIn").val();
      bloqueoGuia.BLO_ConsultarDatosConsecutivos(p_guia);
    },

    //Método que va al api y consulta los consecutivos de la guía dada
    BLO_ConsultarDatosConsecutivos:function(p_guiaoriginal){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_BLOQUEOS_TB_Guias/GetDatosConsecutivos?ien_guia_original_p='+p_guiaoriginal,
            //data: postdata,
            success: function (data) {
                if (data !== null) {                  
                    if(data.length > 1){
                        bloqueoGuia.BLO_ListarConsecutivos(data);
                    }else{                   
                      if(data[0]["BLO_IEN_ENTRADA"] != 0){
                        //asigna consecutivo al form de bloqueo y dispara busqueda de datos del bloqueo
                         var id = data[0]["BLO_IEN_ENTRADA"];
                         bloqueoGuia.BLO_ConsultarDatosDetalleConse(id);                                            
                      }
                      else{
                          swal({
                            title: "Bloqueos",
                            text: "No existen datos para los valores consultados.",
                            icon: "error",
                            button: "OK!",
                          })
                      }                    
                    }                                     
                }
            },
            failure: function (data) {            
              swal({
                      title: "Bloqueos",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                  });
            },
            error: function (data) {
              swal({
                      title: "Bloqueos",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                  });
            }
        });
    },

    // Lista los consecutivos en el modal cuando son varios 
    BLO_ListarConsecutivos: function (data) {
        var templateText = $("#BLO_Conse_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#BLO_Conse_tablebody").html('');
         
          $.each(data, function (index, elem) {
              $("#BLO_Conse_tablebody").append(tableTemplate(elem));
          }) 

         $("#myModalConse").modal('show');
    },


    BLO_Consignatarios: function(){
        var consignatario_p = "";
        var nomconsigna_p = "";
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_PRE_BLOQUEOS_TB/Consignatario?consignatario_p='+consignatario_p+'&nomconsignatario_p='+nomconsigna_p,
            //data: postdata,
            success: function (data) {
                if (data !== null) {  
                  if(data.length > 1){
                    bloqueoGuia.BLO_ListarConsignatarios(data); 
                   }//else{
                  //   bloqueoGuia.BLO_SelectConsigna(data[0]["Id"],data[0]["Nombre"]); 
                  // }                                                                 
                }
            },
            failure: function (data) {            
              swal({
                      title: "Pre-Bloqueos",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                  });
            },
            error: function (data) {
              swal({
                      title: "Pre-Bloqueos",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                  });
            }
        });
    },


    //Método que va al api y consulta los consignatarios
    BLO_ConsultarDatosConsignatarios:function(){
        var consignatario_p = $("#IPB_CSG_CONSIGNATARIO").val();
        var nomconsigna_p = "";
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_PRE_BLOQUEOS_TB/Consignatario?consignatario_p='+consignatario_p+'&nomconsignatario_p='+nomconsigna_p,
            //data: postdata,
            success: function (data) {
                if (data !== null) {  
                  if(data.length > 1){
                    bloqueoGuia.BLO_ListarConsignatarios(data); 
                  }else{
                    bloqueoGuia.BLO_SelectConsigna(data[0]["Id"],data[0]["Nombre"]); 
                  }                                                                 
                }
            },
            failure: function (data) {            
              swal({
                      title: "Pre-Bloqueos",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                  });
            },
            error: function (data) {
              swal({
                      title: "Pre-Bloqueos",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                  });
            }
        });
    },

    // Lista los consignatarios en el modal  
    BLO_ListarConsignatarios: function (data) {
        var templateText = $("#BLO_Consigna_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#BLO_Consigna_tablebody").html('');
         
          $.each(data, function (index, elem) {
              $("#BLO_Consigna_tablebody").append(tableTemplate(elem));
          }) 

         $("#myModalConsigna").modal('show');
    },

    BLO_SelectConsigna: function(id,nombre){
      $("#IPB_CSG_CONSIGNATARIO").val(id);
      $("#CONSIGNATARIO").val(nombre);
      $("#lbl_IPB_CSG_CONSIGNATARIO").addClass('active');
      $("#myModalConsigna").modal('hide');               
    },

    BLO_BuscarConsigna: function(){
        var p_codconsigna = "";
        var p_nomconsigna = "";
        //var p_popup = 0;
        $('.modal-body #blo_flt_cod_consig,.modal-body textarea').each(function () {
            p_codconsigna = $(this).val();
        });
        $(".modal-body #blo_flt_nom_consig,.modal-body textarea").each(function () {
            p_nomconsigna = $(this).val();
        });

        //if (p_codimportador !== "" || p_nomimportador !== "") {
        //    p_popup = 1;
        //}

        //Obtenemos la plantilla
        var templateText = $("#BLO_Consigna_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $(".modal-body #BLO_Consigna_tablebody,.modal-body textarea").each(function () {
            $(this).html('');
        });
        //$("#Prev_Importador_table1body").html('');
        if (p_codconsigna !== "" || p_nomconsigna !== "") {
            //var p_tipo = "F";
            $.getJSON('/api/TX_PRE_BLOQUEOS_TB/Consignatario?consignatario_p=' + p_codconsigna+'&nomconsignatario_p='+p_nomconsigna).then(function (data) {
                $.each(data, function (index, elem) {
                    $("#BLO_Consigna_tablebody").append(tableTemplate(elem));
                })
            });
        }
        else {
          bloqueoGuia.BLO_Consignatarios();
            // $.getJSON('/api/TX_PRE_BLOQUEOS_TB/Consignatario').then(function (data) {
            //     $.each(data, function (index, elem) {
            //         $("#BLO_Consigna_tablebody").append(tableTemplate(elem));
            //     })
            //     $("#divProcessing").hide();
            // });
        }
    },

    limpiarFiltrosConsigna: function(){
      $("#blo_flt_cod_consig").val("");
      $("#blo_flt_nom_consig").val("");
      bloqueoGuia.BLO_Consignatarios();    
    },

    //Funcionalidad del botón al seleccinar el consecutivo de la lista
    BLO_SelectConsecutivo:function(p_consecutivo){
      bloqueoGuia.BLO_ConsultarDatosDetalleConse(p_consecutivo);
    },

    //Consulta el detalle del consecutivo seleccionado o digitado al insertar
    BLO_ConsultarDatosDetalleConse:function(p_consecutivo){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_BLOQUEOS_TB_Guias/GetDatosConsecutivoDetalle?blo_ien_entrada_p='+p_consecutivo,
            //data: postdata,
            success: function (data) {
                if (data !== null) {
                    
                      if(data["BLO_IEN_ENTRADA"] != 0){
                         principal.activeLabels();
                         bloqueoGuia.BLO_MuestraDatos(data);
                         $("#myModalConse").modal('hide');                                           
                      }
                      else{

                        swal({
                          title: "Bloqueos",
                          text: "No existen datos para los valores consultados.",
                          icon: "error",
                          button: "OK!",
                        })

                      }
                }                 
                
            },
            failure: function (data) {
                swal({
                      title: "Bloqueos",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                    })             
            },
            error: function (data) {
              //console.log(textStatus);
              swal({
                    title: "Bloqueos",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                  })
            }
        });
    }
};

$(document).ready(function () {
    bloqueoGuia.init();
})