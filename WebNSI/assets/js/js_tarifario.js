var tarifario = {
    title: "Tarifario",

    init: function () {
        TecladoFuncionalidadPersonalizada();
        this.IniciarControles();
        this.IniciarControlesTarifas();


        //Al seleccionar el tipo de cliente lista los clientes correspondientes
        $('#TTC_TIPO_CLIENTE').change(function () {
            tarifario.getClient();         
        });

        tarifario.buscar();


        //Al seleccionar el tipo de tarifa muestra los campos correspondientes
        $('#TRF_TIPO_TARIFA').change(function () {
            var tipo_tarifa = $('#TRF_TIPO_TARIFA').val();
            switch(tipo_tarifa) {
              case 'C': //completa

                $("#frm_tarifas_completa").attr('style', 'display:block');
                $("#frm_tarifas_consolidada").attr('style', 'display:block');
                $("#frm_tarifas_general").attr('style', 'display:block');
                $("#frm_tarifas_compensacion").attr('style', 'display:block');
                $("#frm_tarifas_parcial").attr('style', 'display:none');

                // $('#frm_tarifas_completa').show();|
                // $('#frm_tarifas_consolidada').show();
                // $('#frm_tarifas_general').show();
                // $('#frm_tarifas_compensacion').show();
                // $('#frm_tarifas_parcial').hide();
                break;
              case 'E': //Especial

                $("#frm_tarifas_completa").attr('style', 'display:none');
                $("#frm_tarifas_consolidada").attr('style', 'display:block');
                $("#frm_tarifas_general").attr('style', 'display:block');
                $("#frm_tarifas_compensacion").attr('style', 'display:block');
                $("#frm_tarifas_parcial").attr('style', 'display:none');
                
                break;
              case 'K': // Consolidada
                
                $("#frm_tarifas_completa").attr('style', 'display:none');
                $("#frm_tarifas_consolidada").attr('style', 'display:block');
                $("#frm_tarifas_general").attr('style', 'display:block');
                $("#frm_tarifas_compensacion").attr('style', 'display:block');
                $("#frm_tarifas_parcial").attr('style', 'display:block');

                break;
              case 'P': //Plena

                $("#frm_tarifas_completa").attr('style', 'display:none');
                $("#frm_tarifas_consolidada").attr('style', 'display:block');
                $("#frm_tarifas_general").attr('style', 'display:block');
                $("#frm_tarifas_compensacion").attr('style', 'display:block');
                $("#frm_tarifas_parcial").attr('style', 'display:none');

                break;
              default:
                // code block
            }
        });
  
    },

    onfocus: function () {
        if ($("#lbl_B_TTC_ID").hasClass('lblfocus'))
            $("#lbl_B_TTC_ID").removeClass('lblfocus');
    },

    //Carga de controles
    IniciarControles: function () {
        this.getTipoCliente();
        this.getConsolidadores();
        this.getConsignatarios();
        // this.getTamanoContenedor();
        // this.getMonedas();
        // this.getTipoParcial();
    },

    //Carga de controles tab tarifas
    IniciarControlesTarifas: function () {
        this.getTarifas();
        this.getTipoContenedor();
        this.getTamanoContenedor();
        this.getTiposDescarga();
        this.getMonedas();
    },

    //Lista los consolidadores
    getConsolidadores: function (e = null) {
        var cliente = e != null ? e : '';
        var tipo_cliente = "C"
        $("#TTC_CON_NCONSOLIDADOR").kendoComboBox({
            placeholder: "",
            dataTextField: "Nombre",
            dataValueField: "Id",
            filter: "contains",
            autoBind: true,
            minLength: 1,
            dataSource: {
                serverFiltering: true,
                transport: {
                    read: function (options) {
                        if (options.data.filter && options.data.filter.filters.length > 0 ) {
                            cliente = options.data.filter.filters[0].value;
                        }
                        $.ajax({
                            type: "GET",
                            url: "/api/TX_ENTRADAS_TB_Ingreso/Consolidadores?cliente=" + cliente + "&tipo_cliente=" + tipo_cliente,
                            success: function (data) {
                                options.success(data);
                            }
                        });
                    }
                }
            }
        });       
    },

    //Lista los consignatarios o importadores
    getConsignatarios: function (e = null) {
        var cliente = e != null ? e : '';
        var tipo_cliente = "I"
        $("#TTC_IMP_CEDULA").kendoComboBox({
            placeholder: "",
            dataTextField: "Nombre",
            dataValueField: "Id",
            filter: "contains",
            autoBind: true,
            minLength: 1,
            dataSource: {
                serverFiltering: true,
                transport: {
                    read: function (options) {
                        if (options.data.filter && options.data.filter.filters.length > 0 ) {
                            cliente = options.data.filter.filters[0].value;
                        }
                        $.ajax({
                            type: "GET",
                            url: "/api/TX_MANIFIESTOS_TB/Clientes?cliente=" + cliente + "&tipo_cliente=" + tipo_cliente,
                            success: function (data) {
                                options.success(data);
                            }
                        });
                    }
                }
            }
        });       
    },

    //Lista los tipos de Clientes
    getTipoCliente: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/TiposClientes", function (data) { $("#TTC_TIPO_CLIENTE").html(principal.arrayToOptions(data)); });
    },

    //Lista los clientes
    getClient: function (e = null, t = null, id = null) {
        var cliente = e != null ? e : '';
        //var cliente = '';
        var tipo_cliente = t != null ? t : $('#TTC_TIPO_CLIENTE option:selected').val();

        if (tipo_cliente != null) {
            $("#TTC_CLIENTE_ID").kendoComboBox({
                placeholder: "",
                dataTextField: "Nombre",
                dataValueField: "Id",
                filter: "contains",
                autoBind: true,
                minLength: 1,
                dataSource: {
                    serverFiltering: true,
                    transport: {
                        read: function (options) {
                            if (options.data.filter && options.data.filter.filters.length > 0 ) {
                                cliente = options.data.filter.filters[0].value;
                            }
                            $.ajax({
                                type: "GET",
                                url: "/api/TX_MANIFIESTOS_TB/Clientes?cliente=" + cliente + "&tipo_cliente=" + tipo_cliente,
                                success: function (data) {
                                    options.success(data);
                                }
                            });
                        }
                    }
                }
            });

            if (id != null) {
                $("#TTC_CLIENTE_ID").data("kendoComboBox").dataSource.read();
                $("#TTC_CLIENTE_ID").data("kendoComboBox").value(id);
            }


        }
    },

    //Lista las tarifas
    getTarifas: function () {
        principal.getData("/api/TX_TARIFARIO_TB/TiposTarifa",function (data) {$("#TRF_TIPO_TARIFA").html(principal.arrayToOptions(data));});
    },

    //Lista los tipos de contenedor
    getTipoContenedor: function (valor) {
            principal.getData(
                "/api/TX_TARIFARIO_TB/TiposContenedor",
                function (data) {
                    $("#TRF_TIPO_CONTENEDOR").html(principal.arrayToOptions(data));
                     //$("#TRF_TIPO_CONTENEDOR").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
                }
            );
    },

    //Lista los tama�os de contenedor
    getTamanoContenedor: function (valor) {
            principal.getData(
                "/api/TX_TARIFARIO_TB/TamanosContenedor",
                function (data) {
                    $("#TRF_TAMANO_CONT").html(principal.arrayToOptions(data));
                }
            );
    },

    //Lista los tipos descargo
    getTiposDescarga: function (valor) {
            principal.getData(
                "/api/TX_TARIFARIO_TB/TiposDescarga",
                function (data) {
                    $("#TRF_TIPO_DESCARGA").html(principal.arrayToOptions(data));
                }
            );
    },

    //obtiene monedas 
    getMonedas: function () {
        principal.getData(
            "/api/TX_TARIFARIO_TB/monedas",
            function (data) {
                $("#TRF_MONEDA_TARIFA_MIN").html(principal.arrayToOptionsSelectedWithDefault(data, "", ""));
                $("#TRF_MONEDA_TARIF_ADIC").html(principal.arrayToOptionsSelectedWithDefault(data, "", ""));
            }
        );
    },


    //obtiene base calculo 
    getBaseCalculo: function () {
        principal.getData(
            "/api/TX_TARIFARIO_TB/basecalculo",
            function (data) {
                $("#TRF_BASE_CALCULO_SEG_ADIC").html(principal.arrayToOptionsSelectedWithDefault(data, "", ""));
            }
        );
    },



    //Prepara pantalla para agregar nuevo cliente tarifa
    nuevoClienteTarifa: function(){
        $("#div_tarifario_busqueda").hide();
        
        $("#div_consolid_consig").hide();
        $("#div_tabla").hide();

        $("#div_tarifario_inputs").show();

        $('#tarifario_btn_regresar').hide();
        $('#tarifario_btn_listar').show();
        $('#tarifario_btn_guardar').show();
        $('#tarifario_btn_actualizar').hide();
        $('#tarifario_btn_limpiar').show();
        $('#tarifario_btn_nuevo').hide();
        
        $("#TTC_TIPO_CLIENTE").attr('disabled', false);
        $("#TTC_CLIENTE_ID").data("kendoComboBox").enable(true);

        this.limpiar();
    },

    //limpiar
    limpiar: function () {
        principal.resetAllFields('frm_tarifario');
        principal.resetAllFields('frm_tarifario_busqueda');

        $("#tarifas_cliente_tab").hide();
        $("#rubros_tab").hide();
        $("#otros_tab").hide();
        $("#div_consolid_consig").hide();

        $("#tarifario_btn_guardar").show();
        $("#tarifario_btn_actualizar").hide();
        $('#tarifario_btn_nuevo').hide();
        $("#tarifario_busqueda_tablebody").html("");

        principal.deactivateLabels();
    },

    //prepara buscar clientes tarifa
    listar: function () {
        $("#TTC_CON_NCONSOLIDADOR").data("kendoComboBox").value("");
        $("#TTC_IMP_CEDULA").data("kendoComboBox").value("");        
        $("#div_tarifario_inputs").hide();
        $("#div_tarifario_busqueda").show();

        $('#tarifario_btn_regresar').show();
        $('#tarifario_btn_listar').hide();
        $('#tarifario_btn_guardar').hide();
        $('#tarifario_btn_actualizar').hide();
        $('#tarifario_btn_limpiar').hide();
        $('#tarifario_btn_nuevo').show();

        $("#tarifas_cliente_tab").hide();
        $("#rubros_tab").hide();
        $("#otros_tab").hide();

        tarifario.buscar();
    },

    //buscar clientes tarifa
    buscar: function () {
        $.ajax({
            url: '/api/TX_TARIFARIO_TB/busquedaClientesTarifa',
            data: $("#frm_tarifario_busqueda").serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                var templateText = $("#tarifario_busqueda_table_template").html();
                var tableTemplate = Handlebars.compile(templateText);
                $("#tarifario_busqueda_tablebody").html('');
                $.each(data, function (index, elem) {
                    elem.JSON = JSON.stringify(elem);
                    $("#tarifario_busqueda_tablebody").append(tableTemplate(elem));
                });
            },
            error: function (data) {
                principal.alertsSwal.msj_error(tarifario.title, data.responseJSON.Message);
            }
        });
    },

    //buscar relacion consolid consigna
    buscarConsolidConsigna: function (idReg) {
     $("#TCC_TTC_ID").val(idReg);      
        $.ajax({
            url: '/api/TX_TARIFARIO_TB/busquedaConsolidConsigna',
            data: $("#frm_consolid_consigna").serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                var templateText = $("#tarifario_consolidconsigna_table_template").html();
                var tableTemplate = Handlebars.compile(templateText);
                $("#tarifario_consolidconsigna_tablebody").html('');
                $.each(data, function (index, elem) {
                    elem.JSON = JSON.stringify(elem);
                    $("#tarifario_consolidconsigna_tablebody").append(tableTemplate(elem));
                });
            },
            error: function (data) {
                principal.alertsSwal.msj_error(tarifario.title, data.responseJSON.Message);
            }
        });
    },

    regresar: function () {
        $("#div_tarifario_busqueda").hide();
        $("#div_tarifario_inputs").show();

        $('#tarifario_btn_regresar').hide();
        $('#tarifario_btn_listar').show();
        $('#tarifario_btn_guardar').show();
        $('#tarifario_btn_actualizar').hide();
        $('#tarifario_btn_limpiar').show();
        $('#tarifario_btn_nuevo').hide();

        this.limpiar();
    },

    //Guarda el cliente tarifa 
    guardarCliente: function () {
        $("#TTC_USU_REGISTRO").val($("#UsReg").val());
        var formData = $("#frm_tarifario").serialize();
        $.ajax({
            url: "/api/TX_TARIFARIO_TB/insertarClienteTarifa",
            method: "POST",
            data: formData,
            dataType: "text",
            success: function (data) {
                if (data.length === 0) {
                    principal.alertsSwal.msj_success(tarifario.title, "Se Ingresaron los datos correctamente", function () {
                        tarifario.listar();
                    });
                } else {
                    principal.alertsSwal.msj_error(tarifario.title, data);
                }
            },
            error: function (e) {
                principal.alertsSwal.msj_error(tarifario.title, JSON.parse(e.responseText).Message);
            }
        });
    },


    editarCliente: function (idCli, tipoCli, idReg) {
        tarifario.getClient("", tipoCli, idCli);
        tarifario.buscarConsolidConsigna(idReg);

        $("#TTC_TIPO_CLIENTE").val(tipoCli);
        $("#TTC_ID").val(idReg);
       
        $("#div_consolid_consig").show();
        $("#div_tabla").show();
        $("#div_tarifario_busqueda").hide();
        $("#div_tarifario_inputs").show();

        $('#tarifario_btn_regresar').hide();
        $('#tarifario_btn_listar').show();
        //$('#tarifario_btn_guardar').show();
        
        $('#tarifario_btn_actualizar').show();
        $('#tarifario_btn_limpiar').show();
        $('#tarifario_btn_nuevo').show();

        // var data = JSON.parse(strJson);
        // this.regresar();

        

        $("#TTC_TIPO_CLIENTE").attr('disabled', true);
        $("#TTC_CLIENTE_ID").data("kendoComboBox").enable(false);


        tarifario.nuevoConsolidConsig();
       // $("#TTC_CLIENTE_ID").data("kendoComboBox").attr('disabled', true);
        // 
        // $("#TTC_CLIENTE_ID").attr('disabled', true);
        
        // $("#tarifario_btn_actualizar").show();
        // $.each(data, function (field, value) {
        //     principal.setValueByName(field, value, 'frm_tarifario');
        // });
        // //principal.activeLabels();
        // TecladoFuncionalidadPersonalizada();
    },

    //valida  el borrado de registro de cliente tarifa
    borrarCliente: function(id){       
        swal({
          title: "Cliente/Tarifa",
          text: "Eliminar la informacion del registro.?",
          icon: "warning",
          buttons: true,
          dangerMode: true
        })
        .then((willDelete) => {
          if (willDelete) {
            tarifario.borrar_Cliente(id);                     
          } 
        });       
    },

    //Borra el registro del cliente que a�n no tenga informaci�n de tarifas asociadas
    borrar_Cliente: function(id){
        var user_Merx = $("#UsReg").val();
        if (id.length > 0) {         
            $.ajax({
                url: '/api/TX_TARIFARIO_TB/BorrarClienteTarifa?id=' + id + '&user=' + user_Merx,
                type: 'DELETE',
                contentType: "application/json",
                success: function (data) {                   
                    swal({
                        title: "Cliente/Tarifa",
                        text: "Se elimin� correctamente la informaci�n",
                        icon: "success",
                        button: "OK!"

                        }).then((value) => {

                            tarifario.listar();
                            // tarimas_ingreso.RefrescarTarimas();
                            // tarimas_ingreso.limpiarPostBorrado();
                    });                    
                },
                error: function (data) {
                    swal({
                          title: "Eliminar Cliente Tarifa",
                          text: data.responseJSON.Message,
                          icon: "error",
                          button: "OK!"
                        })
                }
            })
        }
        else {
            swal({
                title: "Cliente Tarifa",
                text: "El ID del cliente no puede ser vac�o",
                icon: "error",
                button: "OK!",

            })
        }
    },

    //valida  el borrado de registro de consolid consigna del cliente tarifa
    borrarConsolidConsigna: function(id){       
        swal({
          title: "Consolidador/Consignatario",
          text: "Eliminar la informacion del registro?",
          icon: "warning",
          buttons: true,
          dangerMode: true
        })
        .then((willDelete) => {
          if (willDelete) {
            tarifario.borrar_ConsolidConsigna(id);                     
          } 
        });       
    },
    

    //Borra el registro de consolidador consignatario que a�n no tenga informaci�n de tarifas asociadas
    borrar_ConsolidConsigna: function(id){
        var user_Merx = $("#UsReg").val();
        if (id.length > 0) {         
            $.ajax({
                url: '/api/TX_TARIFARIO_TB/borrarConsolidConsigna?id=' + id + '&user=' + user_Merx,
                type: 'DELETE',
                contentType: "application/json",
                success: function (data) {                   
                    swal({
                        title: "Consolidador/Consignatario",
                        text: "Se elimin� correctamente la informaci�n",
                        icon: "success",
                        button: "OK!"

                        }).then((value) => {

                            tarifario.buscarConsolidConsigna($("#TCC_TTC_ID").val());
                            tarifario.nuevoConsolidConsig();

                            $("#tarifas_cliente_tab").hide();
                            $("#rubros_tab").hide();
                            $("#otros_tab").hide();

                            // tarimas_ingreso.RefrescarTarimas();
                            // tarimas_ingreso.limpiarPostBorrado();
                    });                    
                },
                error: function (data) {
                    swal({
                          title: "Eliminar Consolidador/Consignatario",
                          text: data.responseJSON.Message,
                          icon: "error",
                          button: "OK!"
                        })
                }
            })
        }
        else {
            swal({
                title: "Cliente Tarifa",
                text: "El ID del cliente no puede ser vac�o",
                icon: "error",
                button: "OK!",

            })
        }
    },


    //Agrega registro de consolidador consignatario
    agregarConsolidConsig: function(){
        $("#TTC_USU_REGISTRA").val($("#UsReg").val());
        $("#TCC_TTC_ID").val($("#TTC_ID").val());
        var formData = $("#frm_consolid_consigna").serialize();
        $.ajax({
            url: "/api/TX_TARIFARIO_TB/insertarClntTrfConsolConssig",
            method: "POST",
            data: formData,
            dataType: "text",
            success: function (data) {
                if (data.length === 0) {
                    principal.alertsSwal.msj_success(tarifario.title, "Se ingresaron los datos correctamente", function () {
                       // tarifario.listar();
                        
                        $("#TTC_CON_NCONSOLIDADOR").data("kendoComboBox").value("");
                        $("#TTC_IMP_CEDULA").data("kendoComboBox").value("");
                        tarifario.buscarConsolidConsigna($("#TCC_TTC_ID").val());
                    });


                     


                } else {
                    principal.alertsSwal.msj_error(tarifario.title, data);
                }
            },
            error: function (e) {
                principal.alertsSwal.msj_error(tarifario.title, JSON.parse(e.responseText).Message);
            }
        });
    },


    editarConsolidConsig: function (id,idconsol, idconsig) {
        $("#TTC_CON_NCONSOLIDADOR").data("kendoComboBox").enable(false);
        $("#TTC_IMP_CEDULA").data("kendoComboBox").enable(false);

         
        $("#TTC_CON_NCONSOLIDADOR").data("kendoComboBox").value(idconsol);
        $("#TTC_IMP_CEDULA").data("kendoComboBox").value(idconsig);

        $("#btn_agregar_consolidconsigna").hide();
        $("#btn_nuevo_consolidconsigna").show();
        

        $("#tarifas_cliente_tab").show();
        $("#rubros_tab").show();
        $("#otros_tab").show();


        // tarifario.getClient("", tipoCli, idCli);
        // tarifario.buscarConsolidConsigna(idReg);

        // $("#TTC_TIPO_CLIENTE").val(tipoCli);
        // $("#TTC_ID").val(idReg);
       
        // $("#div_consolid_consig").show();
        // $("#div_tabla").show();
        // $("#div_tarifario_busqueda").hide();
        // $("#div_tarifario_inputs").show();

        // $('#tarifario_btn_regresar').hide();
        // $('#tarifario_btn_listar').show();

        // $('#tarifario_btn_actualizar').show();
        // $('#tarifario_btn_limpiar').show();
        // $('#tarifario_btn_nuevo').show();

        // $("#TTC_TIPO_CLIENTE").attr('disabled', true);
        // $("#TTC_CLIENTE_ID").data("kendoComboBox").enable(false);
    },

    //Prepara pantalla para agregar nuevo consolid consigna
    nuevoConsolidConsig: function(){
        $("#TTC_CON_NCONSOLIDADOR").data("kendoComboBox").enable(true);
        $("#TTC_IMP_CEDULA").data("kendoComboBox").enable(true);

         
         $("#TTC_CON_NCONSOLIDADOR").data("kendoComboBox").value("");
         $("#TTC_IMP_CEDULA").data("kendoComboBox").value("");

        $("#btn_agregar_consolidconsigna").show();
        $("#btn_nuevo_consolidconsigna").hide();

         principal.resetAllFields('frm_consolid_consigna');    
    },

    

};

$(document).ready(function () {
    tarifario.init();
    // tarifario.tarifas_cliente.init();
    // tarifario.conceptos_tarifa.init();
});