var sumatoria = {

	init: function(){		
		
	},

	//Lista las tarimas de la guía sin importar el estado
	buscarSumatoria: function (mercancia,tipomercancia) {
	    var entrada = $('#IEN_ENTRADA').val();
	    if(entrada != undefined || entrada != ""){ 	    	
	    	var url = '/api/TX_MERCANCIAS_TB/MercanciasResumen?guia=' + entrada + '&mercancia=' + mercancia + '&tipomercancia=' + tipomercancia;
	    	var cb = function(data){
	    		var templateText = $("#tarimas_cons_table-template").html();
	              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
	            var tableTemplate = Handlebars.compile(templateText);
	            $("#tarimas_cons_tablebody").html('');
	              //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
	            $.each(data, function (index, elem) {
	                $("#tarimas_cons_tablebody").append(tableTemplate(elem));
	            });

	             if($('#frm_tarima_resumen').hasClass('fullHidden'))
        		 $('#frm_tarima_resumen').removeClass('fullHidden')

	    	};
	    	principal.ajax('GET',url,cb,"Tarimas");
	  }
	},

	//Filtra si se escribe mas de un caracter o si no existe ninguno
	filtrarSumatoria: function(input){
		//var entrada = $('#IEN_ENTRADA').val();
	    var mercancia = input.value;//$('#tarima_flr_ien_entradaZ').val();
	    var tipomercancia = $('#MER_TME_MERCANCIA_FILTROZ').val();
	   	
	   	if (tipomercancia == ""){
	   		tipomercancia = null;
	   	}

	    if( input.value.length > 1){
	    
	    	sumatoria.buscarSumatoria(mercancia,tipomercancia);
	        
	    }else{
	        if(input.value.length = 0)
	        {
	            sumatoria.buscarSumatoria(null,tipomercancia);
	        }
	    }
	},


////////////////////////// Acciones/////////////////////////////////
	
	//Marca todos los radio button de las tarimas 
	SeleccionaTodas: function(){
		var conn = 0;
	 	$("#tarimas_cons_table tbody tr").each(function(i,e){ 		
	 		if($(this).find("input[id=rd_C]").prop('checked') == false){
	 			$(this).find("input[id=rd_C]").prop('checked',true);
	 			conn ++;
	 		}
	 		if($(this).find("input[id=rd_C]").prop('checked') == true){
	 			conn ++;
	 		}
		});

	 	if(conn == 0){
	 		swal({
              title: "Información",
              text: "No existen tarimas abiertas para cerrar.",
              icon: "warning",
              button: "OK!",
            });
	 	}else{
	 		//Botones de accion de cerrar todas las tarimas abiertas
			$('#btn_cerrar').show();
			$('#btn_selec').hide();
			$('#btn_deselec').show();
	 	}				 	     
    },

    //Desmarca todos los radio button de las tarimas
    DesSeleccionaTodas: function(){
	 	$("#tarimas_cons_table tbody tr").each(function(i,e){ 
     		
	 		if($(this).find("input[id=rd_C]").prop('checked') == true){
	 			$(this).find("input[id=rd_C]").prop('checked',false);
	 		}
		});

		//Botones de accion de cerrar todas las tarimas abiertas
		//$('#btn_cerrar').hide();
		$('#btn_selec').show();
		$('#btn_deselec').hide();     
    },

    //Crea arreglo de las tarimas seleccionadas
    Tarimas_Cerrar: function(){   	
    	var id_Merc = "";
    	if($("#USER_MERX").val() != undefined && $("#USER_MERX").val() != ""){
    		var user_Merx = $("#USER_MERX").val();
    	}else{
    		var user_Merx = $("#UsReg").val();
    	}
    	
    	var arrEdit = [];

            $("#tarimas_cons_table tbody tr").each(function(i,e){               
              
          		id_Merc = $(this).find("td").eq(0).html(); //Con que mando a cerrar?         
           		
           		 if($(this).find("input[id=rd_C]").is(':checked')){
	                   	arrEdit.push({
		                'idM': id_Merc,
                    	'user': user_Merx
		           		});
                  }    	                         
            });
          //console.log(arrEdit);
          if(arrEdit.length == 0){
          	swal({
                  title: "Información",
                  text: "No ha seleccionado tarimas para cerrar.",
                  icon: "warning",
                  button: "OK!",
                });
          }else{
          	sumatoria.CargaTarimasCerrar(arrEdit);    
          }
    },

    //Validación para proceder a cerrar todas las tarimas.
    CargaTarimasCerrar: function(listaTarimas){  
    	if($("#IEN_GUIA_ORIGINAL_Z").val() != undefined && $("#IEN_GUIA_ORIGINAL_Z").val() != ""){
    		var conocimiento = $("#IEN_GUIA_ORIGINAL_Z").val();
    	}else{
    		var conocimiento = $("#IEN_GUIA_ORIGINAL").val();
    	}
        
    	swal({
          title: "Confirmación?",
          text: "Desea cerrar las tarimas seleccionadas del Conocimiento:" + conocimiento + "?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
           sumatoria.Cerrar(listaTarimas);                            
          } 
        });     
    },

    //Ejecuta acción de cerrar las tarimas de la lista.
    Cerrar: function(listaTarimas){
	    $.ajax({
	        url: '/api/TX_MERCANCIAS_TB/CerrarTarimas',
	        type: 'POST',
	        data: JSON.stringify(listaTarimas),
	        dataType: 'json',
	        contentType: "application/json",
	        success: function (data) {                   
	             swal({
	                 title: "Tarimas",
	                 text: "Se cerraron las tarimas seleccionadas.",
	                 icon: "success",
	                 button: "OK!",
	             })
	             .then((willDelete) => {
			          if (willDelete) {
			           sumatoria.buscarSumatoria(); 
			           //Botones de accion de cerrar todas las tarimas abiertas
						$('#btn_cerrar').show();
						$('#btn_selec').show();
						$('#btn_deselec').hide();                           
			          } 
			        }); 

	        },
	        error: function (data) {
              swal({
                  title: "Error",
                  text: data.responseJSON.Message,
                  icon: "error",
                  button: "OK!",
                });
	        }
	    })
    }

	
};
$(document).ready(function () {
    sumatoria.init();
});