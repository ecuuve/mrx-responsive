﻿var principal = {
    alertsSwal: {
        mensajes: function (msj) {
            swal({
                title: msj,
                //type: "success"
                confirmButtonText: 'Aceptar',
                confirmButtonColor: '#044677'
            });
        },
        msj_alerta: function (title, msj, cb) {
            swal({
                title: title,
                icon: "warning",
                text: msj,
                confirmButtonText: 'Aceptar',
                confirmButtonColor: '#044677',
            }).then( () => {
                if (cb) {
                    cb();
                }
            });
        },

        msj_success: function (title, msj, cb) {
            if (msj === null) {
                msg = "";
            }
            swal({
                title: title,
                icon: "success",
                text: msj,
                confirmButtonText: 'Aceptar',
                confirmButtonColor: '#044677',
            }).then( () => {
                if (cb) {
                    cb();
                }
            });

        },

        msj_error: function (title,msj, cb) {
            swal({
                title: title,
                text: msj,
                icon: "error",
                button: "OK!",
            }).then( () => {
                if (cb) {
                    cb();
                }
            });

        }
    },
    init: function () {
        $('input[type="number"]').bind("cut copy paste",function(e) {
             //e.preventDefault(); //Se quita a solicitud de Yecson que quería copiar y pegar para buscar 5/dic/2019
         });
    },

    DefaultDateHour: function () {
        $(".dtp_default").kendoDatePicker({
            format: "dd/MM/yyyy",
            value: new Date()
        });
    },

    
    jsonForm: function (formArray) {//serialize data function
        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {
            returnArray[formArray[i]['name']] = formArray[i]['value'];
        }
        return returnArray;
    },
    getData: function (url, callback) {
        $.ajax({
            dataType: "json",
            url: url,
            success: function (data) {
                callback(data);
            },
            error: function (e) {
                console.log(e);
            }
        });
    },
    ajax: function(type, url, callback, tittleError, data){
        $.ajax({
                type: type,
                dataType: 'json',
                contentType: 'application/json',
                url: url,
                data: data,
                success: function (data) {
                    if(typeof(callback) == 'function')
                        callback(data); 
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                    else{
                        swal({
                          title: tittleError,
                          text: data.responseJSON.Message,
                          icon: "error",
                          button: "OK!",
                        });
                    }
                }
            });
    },
    get: function (pathp, datap, callback) {
        $.get(
            pathp,
            datap,
            function (data) {
                callback(data)
            });
    },

    //Funcionalidad de filtro de inputs
    filterTable_Input: function (id_filter, id_table, columnNumber) {
        var input, filter, table, tr, td, i;
        filter = $('#' + id_filter).val().toUpperCase();
        if(filter != ""){
            table = document.getElementById(id_table);
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[columnNumber];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1 && tr[i].style.display != "none") {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }

                }
            }
        }
    },

    //Funcionalidad de filtro selec
    filterTable_select: function (id_filter, id_table, columnNumber) {

        var curr = ($('#' + id_filter + ' option:selected').text().replace(/\d+/g, '').replace('-', '')).trim();
        if (curr != 'Seleccione una opción') {

            var table, tr, td, i;
            table = document.getElementById(id_table);
            tr = table.getElementsByTagName("tr");

            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[columnNumber];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(curr.toUpperCase()) == -1) {
                        tr[i].style.display = "none";
                    } 
                }
            }
        }
    },

    activeLabelsInput: function(){
        $('input').each(function() { 
           $this = $(this);
           if($this.prop('type') == "text" || $this.prop('type') == "number" ){
                if($this.prop('id').search("FCH") == -1){
                     $label = $('label[for="'+ $this.attr('id') +'"]');
                   if ($label.length > 0 ) {
                       $label.addClass('active');//this input has a label associated with it, lets do something!
                   }
                }            
           }          
        });
    },

    deactiveLabelsInput: function(){
        $('input').each(function() { 
           $this = $(this);
           if($this.prop('type') == "text" || $this.prop('type') == "number" ){
                if($this.prop('id').search("FCH") == -1){
                    $label = $('label[for="'+ $this.attr('id') +'"]');
                   if ($label.length > 0 ) {
                       $label.removeClass('active');//this input has a label associated with it, lets do something!
                   }
                }              
           }           
        });
    },

    formatoNumeros: function(){
        $("#IEN_VOLUMEN_MANIF").on({
          "focus": function(event) {
            $(event.target).select();
          },
          "keyup": function(event) {
            $(event.target).val(function(index, value) {
              return value.replace(/\D/g, "")
                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
            });
          }
        });
    },

    //Agrego class Active a todos los labels
    activeLabels: function () {
        $('label').addClass('active');
    },

    //Quito class Active a todos los labels
    deactivateLabels: function () {
        $('label').removeClass('active');
    },

    ActivarTab: function(tab){
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    },

    //Limpia todos los input
    resetAllFields: function (form) {
        $("." + form + " input[type=text], ." + form + " input[type=hidden], ." + form + " input[type=password], ." + form + " textarea, ."+ form + " input[type=number]").val("");
        $("." + form + " select").prop("selectedIndex", 0);
        $("." + form + " input[type=checkbox], ." + form + " input[type=radio]").prop('checked', false);
    },

    //CARGA LOS OBJ CON DATA
    setValueById: function (id, val) {
        var obj = $('#' + id);
        if (obj.length > 0) {
            if ($(obj).is('select')) {
                $(obj).val(val);
            } else if ($(obj).is('input')) {
                switch (obj.attr('type')) {
                    case 'text':
                    case 'hidden':
                    case 'password':
                    case 'number':
                        $(obj).val(val);
                        break;
                    case 'checkbox':
                        $(obj).prop('checked', val);
                        break;
                    case 'radio':
                        $("input[name=" + id + "][value=" + val + "]").prop('checked', true);
                        break;
                    case 'textarea':
                        $(obj).val(val);
                        break;
                    default:
                        console.log('No se encontro el campo: ' + id);
                        break;
                };
            } else {
                console.log('No se encontro el tipo de campo de: ' + id);
            }
        }
    },

    //CARGA LOS OBJ CON DATA
    setValueByName: function (id, val, form) {
        var obj =  $('#' + form).find("[name=" + id + "]"); //$("'[name='"+ id +"']'");//$('input[name="textboxname"]'); //$('#' + id);
        if (obj.length > 0) {
            if ($(obj).is('select')) {
                if(val == '')
                    $(obj).val(0);
                else
                    $(obj).val(val);
                if($(obj).data('role') == 'combobox'){
                    var text = $("#" + id + " option:selected").text();
                    $('#' + id + ' > option').each(function(i,v){
                        if($(this).text() == text){
                            $("#" + id).data('kendoComboBox').select(i);
                        }
                    });
                }
            } else if ($(obj).is('input')) {
                switch (obj.attr('type')) {
                    case 'text':
                    case 'hidden':
                    case 'password':
                    case 'number':
                        $(obj).val(val);
                        break;
                    case 'checkbox':
                        if(val == "S")
                             $(obj).prop('checked', true);
                        else if(val == "N")
                             $(obj).prop('checked', false);
                        else
                             $(obj).prop('checked', val);
                        break;
                    case 'radio':
                        $("input[name=" + id + "][value=" + val + "]").prop('checked', true);
                        break;
                    case 'textarea':
                        $(obj).val(val);
                        break;
                    default:
                        console.log('No se encontro el campo: ' + id);
                        break;
                };
            } else if($(obj).is('textarea')) {
                $(obj).val(val);
            }
            else{
                console.log('No se encontro el tipo de campo de: ' + id);
            }
        }
    },


    processErrors: function (modelState) {
        var keys = Object.getOwnPropertyNames(modelState);
        for (var index = 0; index < keys.length; index++) {
            var key = keys[index];
            var errorMessage = modelState[key];
            //var formGroup = $("[name='" + key + "']").closest(".form-group");
            var formGroup = $("[name='" + key + "']").closest(".md-form");
            formGroup.append('<div class="alert alert-danger">' + errorMessage + '</div>');
        }
    },


    processErrorsPopUp: function (modelState) {
        var keys = Object.getOwnPropertyNames(modelState);
        var errorMessage = "";
        for (var index = 0; index < keys.length; index++) {
            var key = keys[index];
            if (!(key.startsWith("_")) && !(key.startsWith("manifiesto_p")) && !(key.startsWith("c_Documento")) && !(key.startsWith("transportista_p")) && !(key.startsWith("funcionario_p")) && !(key.startsWith("c_Mercancia")) ) {
                errorMessage = errorMessage + modelState[key] + ', ';
            }else{
                console.log(modelState[key]);
            }
            
        }
        swal({
            title: "Error",
            text: errorMessage,
            icon: "error",
            button: "OK!",

        });
    },

    //Crea las opciones de un selec
    arrayToOptions: function (array) {
        var html = "<option value=''>Seleccione una opción</option>";
        for (var index = 0; index < array.length; index++) {
            var item = array[index];
            html += "<option value='" + item.Id + "'>" + item.Nombre + "</option>";
        }
        return html;
    },


    arrayToOptionsSelected: function (array, idOpcion) {
        //var html = "<option>Seleccione una opción</option>";
        var html = "";
        for (var index = 0; index < array.length; index++) {
            var item = array[index];
            if (item.Id == idOpcion) {
                html += "<option value='" + item.Id + "' selected>" + item.Nombre + "</option>";
            } else {
                html += "<option value='" + item.Id + "'>" + item.Nombre + "</option>";
            }
        }
        return html;
    },

    //Crea las opciones de un selec con default 
    arrayToOptionsSelectedWithDefault: function (array, idOpcion, defaultValue) {
        if (defaultValue == undefined) {
            defaultValue = "0";
        }
        var html = "<option value='" + defaultValue + "'>Seleccione una opción</option>";
        //var html = "";
        for (var index = 0; index < array.length; index++) {
            var item = array[index];
            if (item.Id == idOpcion) {
                html += "<option value='" + item.Id + "' selected>" + item.Nombre + "</option>";
            } else {
                html += "<option value='" + item.Id + "'>" + item.Nombre + "</option>";
            }
        }
        return html;
    },

    hidemenu: function () {
        
        if ($("#wrapper").hasClass(''))
        {
            $("#wrapper").toggleClass("toggled");
            if ($("#manifiesto").hasClass('in'))
            {
                $("#manifiesto").removeClass("in");
                $("#manifiesto").addClass("collapse");
            }
            if ($("#mantenimiento").hasClass('in')) {
                $("#mantenimiento").removeClass("in");
                $("#mantenimiento").addClass("collapse");
            }
            if ($("#cargas").hasClass('in')) {
                $("#cargas").removeClass("in");
                $("#cargas").addClass("collapse");
            }
        }//esconde menu
    },

    isArray: function (obj) {
        return !!obj && obj.constructor === Array;
    },
    // Funcion que impide que se digite algo diferente a numeros
    FilterInput: function(event) {
        let numberEntered = false;
        if ((event.which >= 48 && event.which <= 57) || (event.which >= 37 && event.which <= 40)) { //input number entered or one of the 4 directtion up, down, left and right
            //console.log('input number entered :' + event.which + ' ' + event.keyCode + ' ' + event.charCode);
            numberEntered = true;
        }
        else {
            //input command entered of delete, backspace or one of the 4 directtion up, down, left and right
            if ((event.keyCode >= 37 && event.keyCode <= 40) ||  event.which == 8) {
                //console.log('input command entered :' + event.which + ' ' + event.keyCode + ' ' + event.charCode);
            }
            else {
                //console.log('input not number entered :' + event.which + ' ' + event.keyCode + ' ' + event.charCode);
                event.preventDefault();
            }
        }
        // input is not impty
        if (this.validForm) {
            // a number was typed
            if (numberEntered) {
                let newNumber = parseInt(this.numberVoucher + '' + String.fromCharCode(event.which));
                console.log('new number : ' + newNumber);
                // checking the condition of max value
                if ((newNumber <= 30 && newNumber >= 1) || Number.isNaN(newNumber)) {
                    console.log('valid number : ' + newNumber);
                }
                else {
                    console.log('max value will not be valid');
                    event.preventDefault();
                }
            }
            // command of delete or backspace was types
            if (event.keyCode == 46 || event.which == 8) {
                if (this.numberVoucher >= 1 && this.numberVoucher <= 9) {
                    console.log('min value will not be valid');
                    this.numberVoucher = 1;
                    //event.preventDefault();
                    this.validForm = true;
                }
            }
        }
        // input is empty
        else {
            console.log('this.validForm = true');
            this.validForm = false;
        }
    },

    FilterInputNumberDecimal: function(event){
        var $this = $(this);
        if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
           ((event.which < 48 || event.which > 57) &&
           (event.which != 0 && event.which != 8))) {
               event.preventDefault();
        }

        var text = $(this).val();
        if ((event.which == 46) && (text.indexOf('.') == -1)) {
            setTimeout(function() {
                if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                    $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                }
            }, 1);
        }

        if ((text.indexOf('.') != -1) &&
            (text.substring(text.indexOf('.')).length > 2) &&
            (event.which != 0 && event.which != 8) &&
            ($(this)[0].selectionStart >= text.length - 2)) {
                event.preventDefault();
        } 
    },


    //GARANTIZA QUE EL CAMPO CORRESPONDIENTE NO PASE DE LOS DIGITOS MAXLENGTH
    maxLengthCheck: function(object){
        if (object.value.length > object.maxLength)
        object.value = object.value.slice(0, object.maxLength)
    },
    KendoDate: function (control, value) {
        control.kendoDatePicker({
            format: "dd/MM/yyyy",
            value: value,
            dateInput: true,
            // min: new Date(minDate),
            // max: new Date(),
            month: {
                empty: '<span class="k-state-disabled">#=data.value#</span>'
            }
        });
    },
    KendoComboBox: function(data, id, keypressEvent){
        $(id).kendoComboBox({
            minLength: 1,
            filter: "contais",
            dataTextField: 'Nombre',
            dataValueField: 'Id',
            dataSource: data,
            filtering: function(ev){
                var filterValue = ev.filter != undefined ? ev.filter.value : "";
                ev.preventDefault();
                this.dataSource.filter({
                    logic: "or",
                    filters: [
                        {
                          field: "Nombre",
                          operator: "contains",
                          value: filterValue
                        },
                        {
                          field: "Id",
                          operator: "contains",
                          value: filterValue
                        }
                    ]
                });
            },
            suggest: true
        });

        if (keypressEvent !== undefined) {
            $(id).data("kendoComboBox").input.keyup(function () {
                keypressEvent();
            });
        }
    },

    popupCenter: function (url, title, w, h) {
        var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/2);

        myWindow= window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
        //var myWindow=window.open(url,'','width=650,height=500');

        myWindow.focus();
        setTimeout(function(){
            ///ventana.print(); 
            myWindow.close();
        },60000);
        //myWindow.print();
       // myWindow.close();
       return true;
        //return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    },

    imprimirET: function (url, title, w, h){
        //var etiqueta = $("#imp-tarima");
        var ventana = window.open('', 'PRINT', 'height=1000,width=1000');
         ventana.document.write('<html><head>');
         //ventana.document.write('<link rel="stylesheet" href="https://laterminalexpress.com/mod-miami/assets/css/etiqueta.css" />');
         ventana.document.write('</head><body >');
         ventana.document.write("<embed id='imp_tarima' name='imp_tarima' style='width: 500px;height: 500px;' src="+ url + "></embed>");
         //ventana.document.write("<div id='imp_tarima'>hola</div>");         
         ventana.document.write('</body></html>');
        // ventana.document.write("<script>$('#imp_tarima').appendTo('body');</script>")
       ventana.document.close();
        //ventana.document.close();
        
        ventana.focus();
        //ventana.print();
        setTimeout(function(){
            ventana.print(); 
            //ventana.close();
        },2000);
        //ventana.frames["imp_tarima"]print();
        //ventana.document.getElementById("imp_tarima").contentWindow.print();
        return true;
    }

    
    
  

};
//Inicio

$(document).ready(function () {
    principal.init();
})




