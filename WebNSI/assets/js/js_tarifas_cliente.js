var tarifas_cliente = {
    title: "Tarifas por Cliente",

    init: function () {
        TecladoFuncionalidadPersonalizada();
        this.IniciarControles();
        this.eventos();
    },

    //Carga de controles

    IniciarControles: function () {
        this.getTipoCliente();
    },

    getTipoCliente: function (valor) {
        principal.getData(
            "/api/TX_MANIFIESTOS_TB/TiposClientes",
            function (data) {
                $("#TRC_CLIENTE_CONTENEDOR_TIPO").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
                $("#TRC_CLIENTE_GUIA_TIPO").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        );
    },

    // configurar eventos en controles
    eventos: function () {
        $("#TRC_CLIENTE_CONTENEDOR").on("change", function () {
            tarifas_cliente.ev_cliente_contenedor();
        });

        $("#TRC_CLIENTE_CONTENEDOR_TIPO").on("change", function () {
            tarifas_cliente.ev_cliente_contenedor();
        });

        $("#TRC_CLIENTE_GUIA").on("change", function () {
            tarifas_cliente.ev_cliente_guia();
        })

        $("#TRC_TRF_ID").on("change", function () {
            tarifas_cliente.ev_tarifa();
        })
    },

    // queries

    ev_cliente_contenedor: function () {
        var cliente = $("#TRC_CLIENTE_CONTENEDOR").val();
        var tipo = $("#TRC_CLIENTE_CONTENEDOR_TIPO").val();

        if (cliente.length > 0 && tipo.length > 0 && tipo !== "0") {
            $.ajax({
                url: "/api/TX_MANIFIESTOS_TB/Clientes",
                method: "GET",
                data: { cliente: cliente, tipo_cliente: tipo },
                success: function (data) {
                    if (data.length > 0) {
                        $("#TRC_CLIENTE_CONTENEDOR_NOMBRE").val(data[0].Nombre);
                    } else {
                        principal.alertsSwal.msj_error(tarifas_cliente.title, "No hay un cliente con estos valores");
                    }
                }
            });
        }
    },

    ev_cliente_guia: function () {
        var cliente = $("#TRC_CLIENTE_GUIA").val();

        if (cliente.length > 0) {
            $.ajax({
                url: "/api/TX_TARIFAS_CLIENTE_TB/consolidadores",
                method: "POST",
                data: { B_CONSOLIDADOR: cliente, B_NOMBRE: null },
                success: function (data) {
                    if (data.length > 0) {
                        $("#TRC_CLIENTE_GUIA_NOMBRE").val(data[0].NOMBRE);
                    } else {
                        principal.alertsSwal.msj_error(tarifas_cliente.title, "No hay un cliente con estos valores");
                    }
                }
            });
        }
    },    

    ev_tarifa: function () {
        
        var tarifa = $("#TRC_TRF_ID").val();

        if (tarifa.length) {
            $.ajax({
                url: "/api/TX_FACTURAS_TB/GetTarifas",
                method: "GET",
                data: { nombre: null, tarifa: tarifa },
                success: function (data) {
                    if (data.length > 0) {
                        $("#TRC_TRF_NOMBRE").val(data[0].Nombre);
                    } else {
                        principal.alertsSwal.msj_error(tarifas_cliente.title, "No existe la tarifa");
                    }
                }
            });
        }
    },

    // Acciones
    guardar: function () {
        $("#TRC_USU_REGISTRO").val($("#UsReg").val());

        var myform = $('#frm_tarifas_cliente');
        var disabled = myform.find(':input:disabled').removeAttr('disabled');
        var serialized = myform.serialize();
        disabled.attr('disabled', 'disabled');

        $.ajax({
            url: "/api/TX_TARIFAS_CLIENTE_TB/TarifaClienteInsertar",
            method: "POST",
            data: serialized,
            dataType: "text",
            success: function (data) {
                if (data.length === 0) {
                    principal.alertsSwal.msj_success(tarifas_cliente.title, "Se Ingresaron los datos correctamente");
                } else {
                    principal.alertsSwal.msj_error(tarifas_cliente.title, data);
                }
            },
            error: function (e) {
                principal.alertsSwal.msj_error(tarifas_cliente.title, JSON.parse(e.responseText).Message);
            }
        });
    },

    limpiar: function () {
        principal.resetAllFields('frm_tarifas_cliente');
        principal.resetAllFields('frm_tarifas_cliente_busqueda');

        $("#btn_guardar").show();
        $("#btn_actualizar").hide();

        $("#tarifas_cliente_busqueda_tablebody").html("");

        principal.deactivateLabels();
    },

    // buscar
    listar: function () {
        if ($('#div_tarifas_cliente_busqueda').hasClass('fullHidden'))
            $('#div_tarifas_cliente_busqueda').removeClass('fullHidden');
        if ($('#tbl_tarifas_cliente_busqueda').hasClass('fullHidden'))
            $('#tbl_tarifas_cliente_busqueda').removeClass('fullHidden');
        if (!$('#frm_tarifas_cliente').hasClass('fullHidden'))
            $('#frm_tarifas_cliente').addClass('fullHidden');

        $('#btn_regresar').show();
        $('#btn_listar').hide();
        $('#btn_guardar').hide();
        $('#btn_actualizar').hide();
        $('#btn_limpiar').hide();
    },

    regresar: function () {
        if (!$('#div_tarifas_cliente_busqueda').hasClass('fullHidden'))
            $('#div_tarifas_cliente_busqueda').addClass('fullHidden');
        if (!$('#tbl_tarifas_cliente_busqueda').hasClass('fullHidden'))
            $('#tbl_tarifas_cliente_busqueda').addClass('fullHidden');
        if ($('#frm_tarifas_cliente').hasClass('fullHidden'))
            $('#frm_tarifas_cliente').removeClass('fullHidden');

        $('#btn_regresar').hide();
        $('#btn_listar').show();
        $('#btn_guardar').show();
        $('#btn_actualizar').hide();
        $('#btn_limpiar').show();

        this.limpiar();
    },

    buscar: function () {
        $.ajax({
            url: '/api/TX_TARIFAS_CLIENTE_TB/busqueda',
            data: $("#frm_tarifas_cliente_busqueda").serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                var templateText = $("#tarifas_cliente_busqueda_table_template").html();
                var tableTemplate = Handlebars.compile(templateText);
                $("#tarifas_cliente_busqueda_tablebody").html('');
                $.each(data, function (index, elem) {
                    elem.JSON = JSON.stringify(elem);
                    $("#tarifas_cliente_busqueda_tablebody").append(tableTemplate(elem));
                });
            },
            error: function (data) {
                principal.alertsSwal.msj_error(tarifas_cliente.title, data.responseJSON.Message);
            }
        });
    },

    editar: function (strJson) {
        var data = JSON.parse(strJson);
        this.regresar();
        $("#btn_guardar").hide();
        $("#btn_actualizar").show();
        $.each(data, function (field, value) {
            principal.setValueByName(field, value, 'frm_tarifas_cliente');
        });
        principal.activeLabels();
    },

    eliminar: function (strJson) {
        var data = JSON.parse(strJson);
        swal({
            title: tarifas_cliente.title,
            text: "Eliminar&aacute; el registro. Continuar?",
            icon: "warning",
            buttons: true,
            dangerMode: true
        }).then((eliminar) => {
            if (eliminar) {
                $.ajax({
                    url: "/api/TX_TARIFAS_CLIENTE_TB/TarifaClienteEliminar",
                    method: "POST",
                    data: data,
                    dataType: "text",
                    success: function (data) {
                        if (data.length === 0) {
                            principal.alertsSwal.msj_success(tarifas_cliente.title, "Se eliminaron los datos");
                        } else {
                            principal.alertsSwal.msj_error(tarifas_cliente.title, data);
                        }
                        tarifas_cliente.regresar();
                    },
                    error: function (e) {
                        principal.alertsSwal.msj_error(tarifas_cliente.title, JSON.parse(e.responseText).Message);
                    }
                });
            }
        }); 
    }

};

$(document).ready(function () {
    tarifas_cliente.init();
});