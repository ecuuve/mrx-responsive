var cons_indic_posterior = {
	init: function(){
    cons_indic_posterior.LimpiarCamposindicPost();
		cons_indic_posterior.Cons_Indic_Posterior_Listar();

		$("#in_flr_cons_aum_fch_evento").kendoDatePicker({
          format: "dd/MM/yyyy",
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });
    // Dispara busqueda cuando cambian de fecha
     $('#in_flr_cons_aum_fch_evento').change(function () {
        cons_indic_posterior.Cons_Indic_Posterior_Listar();
      });

     //Disparo busqueda cuando dan enter en consecutivo
      $('#in_flr_cons_aum_ien_entrada').bind("enterKey",function(e){
          cons_indic_posterior.Cons_Indic_Posterior_Listar();
        });
        $('#in_flr_cons_aum_ien_entrada').keyup(function(e){
          if(e.keyCode == 13)
          {
            $(this).trigger("enterKey");
          }
        });

      //Controla cuando borran o dan delete
      $('#in_flr_cons_aum_ien_entrada').keyup(function(e){
          if(e.keyCode == 8 || e.keyCode == 46)
          {
             var conse = $("#in_flr_cons_aum_ien_entrada").val();

             if(conse == ""){
              cons_indic_posterior.Cons_Indic_Posterior_Listar();
             }
             
          }
        });

	},
	Cons_Indic_Posterior_Listar: function () {
		var p_fecha = $("#in_flr_cons_aum_fch_evento").val();
        var p_entrada = $("#in_flr_cons_aum_ien_entrada").val();

        var templateText = $("#ConsIndPrev-table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#Cons_Ind_Prev_body").html('');
        $.getJSON('/api/TX_ALERTAS_AUTOMATICAS_TB/GetAlertas?fch_evento=' + p_fecha + '&ien_entrada=' + p_entrada).then(function (data) {
            $.each(data, function (index, elem) {
                $("#Cons_Ind_Prev_body").append(tableTemplate(elem));
            });
        });
    },
    LimpiarCamposindicPost: function () {
     	$('#in_flr_cons_aum_ien_entrada').val('');
     	$('#in_flr_cons_aum_fch_evento').val('');
     	cons_indic_posterior.Cons_Indic_Posterior_Listar();
    },
};

$(document).ready(function () {
    cons_indic_posterior.init();
});