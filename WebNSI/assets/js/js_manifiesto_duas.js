var manifiesto_duas = {
    init: function () {

    },

    // Pinta los duas en la lista de pantalla al darle al + 
    pintaDuas: function () {
        if ($('#mdu_dua').val() != '') {
            var html = '<tr><td>' + $('#mdu_dua').val() + '</td>' +
                '<td><button class="btn btn-elegant waves-effect waves-light" data-placement="Bottom" data-toggle="tooltip" ' +
                'onclick="manifiesto_duas.borrarDuas(this)" title="Eliminar" type="button"><i class="fas fa-trash-alt"></i></button></td>' +
                '</tr>';
            $('#tbody_duas').append(html);
            $('#mdu_dua').val('')
        }
    },

    //Elimina los duas no deseados
    borrarDuas: function (button) {
        button.closest('tr').remove();
    },

    //Guarda los duas del manifiesto
    guardar: function (id, callback) {
        var duas = [];
        var last = $('#tb_duas > tbody > tr:last');

        if(last.length > 0){
            $('#tb_duas > tbody > tr').each(function (i, v) {
            var data = {
                'Id': id,
                'Nombre': v.firstElementChild.textContent,
                'MDU_USU_REGISTRO': $('#TMA_USU_REGISTRO').val()
            };
            duas.push(data);
                if (last[0] == v) {
                    $.post(
                        "/api/TX_MANIFIESTO_DUAS_TB/GuardarDua/",
                        { 'id': id, 'duas': duas },
                        function (data) {
                            // ingreso_mercancia.limpiar();
                            // manifiesto_duas.limpiar();
                            manifiesto_viaje.delete(data, callback);
                            // if(typeof(callback) == 'function')
                            //  callback();
                        })
                        .fail(function (data) {
                            if (data.responseJSON.ModelState) {
                                principal.processErrorsPopUp(data.responseJSON.ModelState);
                            } else {
                                swal({
                                    title: "Manifiesto - Duas",
                                    text: data.responseJSON.Message,
                                    icon: "error",
                                    button: "OK!",
                                });
                            }
                        });
                }
            });
        }else{
             manifiesto_viaje.delete(id, callback); 
        }





        
    },

    //Limpia la lista de duas en pantalla
    limpiar: function () {
        $('#tbody_duas').html('');
    },

    //Busca duas del manifiesto
    buscarDuas: function (p_identificador, consulta = null) {
        var dua = '';
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MANIFIESTO_DUAS_TB/Dua?id_manifiesto=' + p_identificador + '&dua=' + dua,
            success: function (data) {
                manifiesto_duas.limpiar();
                $.each(data, function (i, v) {
                    var html = '<tr>';
                    html += '<td>' + data[i].Nombre + '</td>';
                    if (consulta === null) {
                        html += '<td><button id="btn_borrar_viaje" class="btn btn-elegant waves-effect waves-light" data-placement="Bottom" data-toggle="tooltip" ' +
                            'onclick="manifiesto_duas.borrarDuas(this)" title="Eliminar" type="button"><i class="fas fa-trash-alt"></i></button></td>';
                    }
                    html += '</tr>';
                    $('#tbody_duas').append(html);
                });
            },
            failure: function (data) {
                console.log("fail", data);
            },
            error: function (data) {
                swal({
                    title: "Duas",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                });
            }
        });
    },

    //Borra los registros y los vuelve a crear
    delete: function (id, callback) {
        var duas = [];
        $('#tb_duas > tbody > tr').each(function (i, v) {
            var data = {
                'Id': id,
                'Nombre': v.firstElementChild.textContent,
                'MDU_USU_REGISTRO': $('#TMA_USU_REGISTRO').val()
            };
            duas.push(data);
        });
        $.post(
            "/api/TX_MANIFIESTO_DUAS_TB/BorrarDua/",
            { 'id': id, 'duas': duas },
            function (data) {
                if (data != null)
                    manifiesto_duas.guardar(data, callback);
            })
            .fail(function (data) {
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                } else {
                    swal({
                        title: "Manifiesto - Duas",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",
                    });
                }
            });
    }
};
$(document).ready(function () {
    manifiesto_duas.init();
});