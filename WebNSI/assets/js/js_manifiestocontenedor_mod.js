var manifiestocontenedor_mod = {
    filter: null,
    tipo_cliente: null,
    aerolinea: null,
    codigo_cliente: null,
    codigo_aerolinea: null,
    offset: 10,

     //Acciones iniciales de la pantalla
    init: function () {

        principal.resetAllFields('frm_ingresoMerc');
        TecladoFuncionalidadIngresoMercancia();       
        manifiestocontenedor_mod.CargaInicial();
        $('#TMA_MANIFIESTO').focus();

        //Si se selecciona una modalidad de transporte cambian validaciones de navegación y carga los transportistas correspondientes
        $('#TMA_MOD_TRANSPORTE').change(function () {
            Cookies.set('tipo_transporte', $(this).val());
            // manifiestocontenedor_mod.getTransportistas();
            manifiestocontenedor_mod.navegacion();
            $('#TMA_MANIFIESTO').focus();
            manifiestocontenedor_mod.getTransportistas();
            manifiestocontenedor_mod.tipoTransporteControles();
        });

        //Al seleccionar el tipo de cliente lista los clientes correspondientes
        $('#TMA_CLIENTE_TIPO').change(function () {
            manifiestocontenedor_mod.getClient();
        });

        $("#ingmerc_flr_fecha").on("keypress keyup blur", function (e) {
            var regExp = /[0-9\/]/;
            var value = String.fromCharCode(e.which) || e.key;
            console.log(e);
            // Only numbers, dots and commas
            if (!regExp.test(value)
                // && e.which == 188 // ,
                // && e.which == 190 // .
                && e.which != 8   // backspace
                && e.which != 46  // delete
                && (e.which < 37  // arrow keys
                    || e.which > 40)) {
                e.preventDefault();
                return false;
            }
        });

        $("#mdu_dua").keypress(function(e) {
            //valida si dan enter
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code==13){
                manifiesto_duas.pintaDuas();
            }
        });

        $("#mvi_viaje").keypress(function(e) {
            //valida si dan enter
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code==13){
                manifiesto_viaje.pintaViajes();
            }
        });
    },

    onfocus: function () {
        if ($("#lbl_TMA_MANIFIESTO").hasClass('lblfocus'))
            $("#lbl_TMA_MANIFIESTO").removeClass('lblfocus');
    },

    //Carga inicial de controles de la pantalla
    CargaInicial: function () {
        //Inicializacion de componentes de fecha
        principal.KendoDate($("#TMA_FCH_MANIFIESTO"), new Date());
        principal.KendoDate($("#TMA_FCH_INGRESO"), new Date());
        principal.KendoDate($("#TMA_FCH_REGIMEN"), new Date());
        $("#ingmerc_flr_fecha").kendoDatePicker({
            format: "dd/MM/yyyy"
        });
        $('#btn_regresar_ingresoMerc').hide();
        $('#btn_conocimientos').hide();
        //Inicializacion de los comboboxKendo
        manifiestocontenedor_mod.getFuncionarios();
        manifiestocontenedor_mod.getPuertos();
        manifiestocontenedor_mod.getChekeadores();
        manifiestocontenedor_mod.getPortones();
        manifiestocontenedor_mod.getUbicacionOrigen();
        manifiestocontenedor_mod.getAduanas();
        manifiestocontenedor_mod.getTransportistasFiltro();
        //Inicializacion select normales
        manifiestocontenedor_mod.getTiposContenedor();
        manifiestocontenedor_mod.getModalidadesTransporte();
        manifiestocontenedor_mod.getTamanos();
        manifiestocontenedor_mod.getDocumentos();
        manifiestocontenedor_mod.getTipoCliente();
        manifiestocontenedor_mod.getEstados();

        manifiestocontenedor_mod.getMotivos();
        manifiestocontenedor_mod.getDepartamentos();
    },


    getMotivos: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Motivos", function (data) { $("#TMA_MOT_MOTIVO").html(principal.arrayToOptions(data)); });
    },

    getDepartamentos: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Departamentos", function (data) { $("#TMA_DEPARTAMENTO").html(principal.arrayToOptions(data)); });
    },


    //Obtiene las ubicaciones
    getUbicacionOrigen: function (e = null) {
        var id = '';
        var callback = function (e) {
            conocimientos.getUbicacionOrigen(e.filter.value);
        };
        var cb = function (data) {
            $("#TMA_UBICACION_INGRESO").kendoComboBox({
                minLength: 1,
                filter: "contais",
                dataTextField: 'Nombre',
                dataValueField: 'Id',
                dataSource: data,
                filtering: function (ev) {
                    var filterValue = ev.filter != undefined ? ev.filter.value : "";
                    ev.preventDefault();
                    this.dataSource.filter({
                        logic: "or",
                        filters: [
                            {
                                field: "Nombre",
                                operator: "contains",
                                value: filterValue
                            },
                            {
                                field: "Id",
                                operator: "contains",
                                value: filterValue
                            }
                        ]
                    });
                },
                suggest: true
            });
        };
        principal.get("/api/TX_UBICACIONES_TB/Ubicaciones?nombre=" + id, '', cb);
    },

    //Lista los tipos de contenedor
    getTiposContenedor: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Tipo_Contenedor", function (data) { $("#TMA_TIPO_CONTENEDOR").html(principal.arrayToOptions(data)); });
    },

    //Lista las modalidades de transporte
    getModalidadesTransporte: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/ModalidadesTransporte", function (data) { $("#TMA_MOD_TRANSPORTE").html(principal.arrayToOptionsSelected(data, 1)); });
    },

    //Lista los tamaños
    getTamanos: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Tamano", function (data) { $("#TMA_TAMANO").html(principal.arrayToOptionsSelected(data, 40)); });
    },

    //Lista los documentos
    getDocumentos: function () {
        var tipo_documento = null;
        var cb = function (data) {
            $("#TMA_TDO_DOCUMENTO").html(principal.arrayToOptions(data));
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Documentos?tipo_documento=" + tipo_documento, '', cb);
    },

    //Lista los estados
    getEstados: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Estados", function (data) { $("#TMA_ESTADO").html(principal.arrayToOptionsSelected(data, 'A')); });
        // principal.getData("/api/TX_MANIFIESTOS_TB/Estados",function (data) {$("#TMA_ESTADO").html(principal.arrayToOptions(data));});
    },

    //Lista los tipos de Clientes
    getTipoCliente: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/TiposClientes", function (data) { $("#TMA_CLIENTE_TIPO").html(principal.arrayToOptions(data)); });
    },

    //Lista los Tranportistas
    getTransportistas: function (e = null, m = null, c = null) {
        var modalidad = e != null ? e : $('#TMA_MOD_TRANSPORTE option:selected').val();
        //var aerolinea = m != null ? m : '';
        var aerolinea = '';
        var cod_aerolinea = c != null ? c : '';

        var cb = function (data) {
            principal.KendoComboBox(data, '#TMA_AER_NAEROLINEA');

            //Funcionalidad de carga de datos para la edición
            if (cod_aerolinea != null && manifiestocontenedor_mod.aerolinea != null) {

                $('#TMA_AER_NAEROLINEA').val(cod_aerolinea);
                $('#TMA_AER_NAEROLINEA > option').each(function (i, v) {
                    if ($(this).text() == manifiestocontenedor_mod.aerolinea) {
                        $("#TMA_AER_NAEROLINEA").data('kendoComboBox').select(i);
                    }
                });

                manifiestocontenedor_mod.aerolinea = null;
                manifiestocontenedor_mod.cod_aerolinea = null;
            }
            // //Funcionalidad de carga de datos para la edición
            // if(data.length == 1 && manifiestocontenedor_mod.aerolinea != null){
            //   manifiestocontenedor_mod.aerolinea = null;
            //   $('#TMA_AER_NAEROLINEA').val(data[0].Id);
            //   $('#TMA_AER_NAEROLINEA > option').each(function(i,v){
            //       if($(this).text() == data[0].Nombre){
            //           $("#TMA_AER_NAEROLINEA").data('kendoComboBox').select(i);
            //       }
            //   });
            // }
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Transportistas?mod_transporte=" + modalidad + "&&aerolinea=" + aerolinea, '', cb);
    },

    //Lista los transportistas para la seccion de filtros
    getTransportistasFiltro: function () {
        var aerolinea = '';
        principal.getData("/api/TX_MANIFIESTOS_TB/TransportistasFiltro?aerolinea=" + aerolinea, function (data) { $("#ingmerc_flr_transportista").html(principal.arrayToOptions(data)); });
    },

    //Si es aéreo activa codigo vuelo
    tipoTransporteControles: function () {
        if ($("#TMA_MOD_TRANSPORTE").val() == 1) {
            $("#TMA_CODIGO_VUELO").attr("disabled", "disabled");
        } else {
            $("#TMA_CODIGO_VUELO").removeAttr("disabled");
        }
    },

    //Lista los clientes
    getClient: function (e = null, t = null, id = null) {
        var cliente = e != null ? e : '';
        //var cliente = '';
        var tipo_cliente = t != null ? t : $('#TMA_CLIENTE_TIPO option:selected').val();

        if (tipo_cliente != null) {
            $("#TMA_CLIENTE_ID").kendoComboBox({
                placeholder: "",
                dataTextField: "Nombre",
                dataValueField: "Id",
                filter: "contains",
                autoBind: true,
                minLength: 1,
                dataSource: {
                    serverFiltering: true,
                    transport: {
                        read: function (options) {
                            if (options.data.filter && options.data.filter.filters.length > 0 ) {
                                cliente = options.data.filter.filters[0].value;
                            }
                            $.ajax({
                                type: "GET",
                                url: "/api/TX_MANIFIESTOS_TB/Clientes?cliente=" + cliente + "&tipo_cliente=" + tipo_cliente,
                                success: function (data) {
                                    options.success(data);
                                }
                            });
                        }
                    }
                }
            });

            if (id != null) {
                $("#TMA_CLIENTE_ID").data("kendoComboBox").dataSource.read();
                $("#TMA_CLIENTE_ID").data("kendoComboBox").value(id);
            }
        }
    },

    //Lista los funcionarios y ejecuta el filtro
    getFuncionarios: function () {
        var cb = function (data) {
            principal.KendoComboBox(data, '#TMA_FUN_FUNCIONARIO');
            principal.KendoComboBox(data, '#TMA_ID_FUN_SUPERVISION');
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Funcionarios?id=" + '', '', cb);
    },

    //Lista los puertos
    getPuertos: function () {
        var cb = function (data) {
            principal.KendoComboBox(data, '#TMA_PTO_PUERTO');
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Puertos?id=" + '', '', cb);
    },

    //Lista los chekeadores
    getChekeadores: function () {
        var user = $('#UsReg').val(); //Revisar con métodos de cliente/jafet para cargar el logueado
        var cb = function (data) {
            principal.KendoComboBox(data, '#TMA_CHEQUEADOR');
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Chekeadores?nombre_p=" + ''+ "&usuario_p=" +"" , '', cb);
    },

     //Lista los portones
    getPortones: function () {
        var cb = function (data) {
           // principal.KendoComboBox(data, '#TMA_PORTON');
            $("#frm_ingresoMerc #TMA_PORTON").html(principal.arrayToOptions(data)); 
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Portones?tipo_p=" + "M", '', cb);
    },

    //Lista las aduanas con los filtros kendo respectivos
    getAduanas: function () {
        //principal.getData("/api/TX_MANIFIESTOS_TB/Aduanas?id=" + '', function (data) { $("#TMA_ADU_ENVIA").html(principal.arrayToOptions(data)); $("#TMA_ADU_RECIBE").html(principal.arrayToOptionsSelected(data, 5));});
        var cb = function (data) {
            principal.KendoComboBox(data, '#TMA_ADU_ENVIA');
            principal.KendoComboBox(data, '#TMA_ADU_RECIBE');
            $("#TMA_ADU_RECIBE").data('kendoComboBox').select(4);
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Aduanas?id=" + '', '', cb);
    },

    //Validaciones de navegación de la página
    navegacion: function () {
        if ($('#TMA_MOD_TRANSPORTE').val() == 4) {
            $('[name="TMA_AER_NAEROLINEA_input"]').prop('tabIndex', 9);
            $('#TMA_AER_NAEROLINEA').prop('tabIndex', 9);
            $('#TMA_CODIGO_VUELO').prop('tabIndex', 15);
            $("input[name='tma_aer_naerolinea_input']").prop('tabIndex', 9);

            $("#TMA_NUM_CONTENEDOR").attr('disabled', true);
            $("#TMA_TAMANO").attr('disabled', true);

            if ($('#codVuelo').hasClass('fullHidden'))
            $('#codVuelo').removeClass('fullHidden')
            
            if ($('#tranportistaInt').hasClass('fullHidden'))
            $('#tranportistaInt').removeClass('fullHidden')

           $("#TMA_ADU_ENVIA").data('kendoComboBox').select(4);
            

        } else {
            $('#TMA_AER_NAEROLINEA').prop('tabIndex', '');
            $('[name="TMA_AER_NAEROLINEA_input"]').prop('tabIndex', '');
            $('#TMA_CODIGO_VUELO').prop('tabIndex', '');
            $("input[name='tma_aer_naerolinea_input']").prop('tabIndex', '');

            $("#TMA_NUM_CONTENEDOR").attr('disabled', false);
            $("#TMA_TAMANO").attr('disabled', false);

            if (!$('#codVuelo').hasClass('fullHidden'))
            $('#codVuelo').addClass('fullHidden')
            
            if (!$('#tranportistaInt').hasClass('fullHidden'))
            $('#tranportistaInt').addClass('fullHidden')

             $("#TMA_ADU_ENVIA").data("kendoComboBox").value("");

        }
        if ($('#TMA_MOD_TRANSPORTE').val() == 1) {
            $('#TMA_CHEQUEADOR').prop('tabIndex', 17);
        } else {
            $('#TMA_CHEQUEADOR').prop('tabIndex', '');
        }
    },

    //Manejo de botones entre el listar y el formulario
    Refrescar: function () {
        manifiestocontenedor_mod.limpiaFiltros();
        if ($('#tbl_ingresoMerc').hasClass('fullHidden'))
            $('#tbl_ingresoMerc').removeClass('fullHidden')
        if ($('#div_ingresoMerc_filtros').hasClass('fullHidden'))
            $('#div_ingresoMerc_filtros').removeClass('fullHidden')
        if (!$('#frm_ingresoMerc').hasClass('fullHidden'))
            $('#frm_ingresoMerc').addClass('fullHidden')
        $('#btn_listar_ingresoMerc').hide();
        $('#btn_guardar_ingresoMerc').hide();
        $('#btn_editar_ingresoMerc').hide();
        $('#btn_conocimientos').hide();
        $('#btn_regresar_ingresoMerc').show();
        
        $('#ingmerc_flr_manifiesto').focus();

          if (!$('#codVuelo').hasClass('fullHidden'))
            $('#codVuelo').addClass('fullHidden')
            
            if (!$('#tranportistaInt').hasClass('fullHidden'))
            $('#tranportistaInt').addClass('fullHidden')
        //manifiestocontenedor_mod.buscar();
    },

    //Ejecuta la busqueda de un manifiestos mediante filtros especificos
    buscar: function () {
        var manifiesto_id = null;
        var pagina = $("#pagina").val();
        if (pagina === undefined) pagina = 1;
        var tma_manifiesto_p = $('#ingmerc_flr_manifiesto').val();
        var tma_fch_manifiesto_p = $('#ingmerc_flr_fecha').val();
        var tma_aer_naerolinea_p = $('#ingmerc_flr_transportista').val();
        var cliente = $('#ingmerc_flr_cliente').val();
        var num_contenedor = $('#ingmerc_flr_num_contenedor').val();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MANIFIESTOS_TB/ManifiestoModificar?manifiesto_id=' + manifiesto_id + '&tma_manifiesto_p=' + tma_manifiesto_p + '&tma_fch_manifiesto_p=' + tma_fch_manifiesto_p + '&tma_aer_naerolinea_p=' + tma_aer_naerolinea_p + '&cliente=' + cliente + '&num_contenedor=' + num_contenedor, // + "&page=" + pagina + "&offset=" + manifiestocontenedor_mod.offset,
            success: function (data) {
                //manifiestocontenedor_mod.setPaginas(Math.ceil(data.N_REGISTROS / manifiestocontenedor_mod.offset), pagina);
                //Obtenemos la plantilla
                var templateText = $("#ingresoMerc_table-template").html();
                //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                var tableTemplate = Handlebars.compile(templateText);
                $("#ingresoMerc_tablebody").html('');
                //$.each(data.REGISTROS, function (index, elem) {
                $.each(data, function (index, elem) {
                    $("#ingresoMerc_tablebody").append(tableTemplate(elem));
                });
            },
            error: function (data) {
                swal({
                    title: "Manifiesto",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
            }
        });
    },

    setPaginas: function (paginas, pagina) {
        if (paginas === null) { paginas = 1; }
        $("#pagina").empty();
        for (i = 1; i <= paginas; i++) {
            selected = "";
            if (i === parseInt(pagina)) selected = " selected";
            $('#pagina').append('<option value="' + i + '"' + selected + '>' + i + '</option>');
        }
    },

    //Guarda el manifiesto
    guardar: function () {
        if($("#TMA_CLIENTE_ID").data("kendoComboBox") != undefined){
             $('#TMA_CLIENTE_NOMBRE').val($("#TMA_CLIENTE_ID").data("kendoComboBox").text());
        }
       
        $('#TMA_USU_REGISTRO').val($('#UsReg').val());
        $.post(
            "/api/TX_MANIFIESTOS_TB/GuardarManifiesto/",
            $('#frm_ingresoMerc').serialize(),
            function (data) {
                if (data != null) {
                    var cb = function () {
                        manifiesto_duas.limpiar();
                        manifiestocontenedor_mod.limpiar();
                        manifiesto_viaje.limpiar();
                        swal({
                            title: "Contenedor/Manifiesto",
                            text: "Se guardó correctamente el Contenedor/Manifiesto",
                            icon: "success",
                            button: "OK!",
                        });
                    };

                    var id = data;
                    Cookies.set('manifiesto', id);
                    manifiesto_duas.delete(data, cb);
                }
            })
            .fail(function (data) {
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                } else {
                    swal({
                        title: "Contenedor/Manifiesto",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",
                    });
                }
            });
    },

    //Limpia todos los campos
    limpiar: function () {
        principal.resetAllFields('frm_ingresoMerc');
        var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd/MM/yyyy');
        $("#TMA_TAMANO").val(40);
        $("#TMA_FCH_MANIFIESTO").val(todayDate);
        $("#TMA_FCH_INGRESO").val(todayDate);
        $("#TMA_FCH_REGIMEN").val(todayDate);
        $("#TMA_FUN_FUNCIONARIO").data("kendoComboBox").value("");
        $("#TMA_ID_FUN_SUPERVISION").data("kendoComboBox").value("");
        $("#TMA_PTO_PUERTO").data("kendoComboBox").value("");
        $("#TMA_ADU_ENVIA").data("kendoComboBox").value("");

        $("#TMA_UBICACION_INGRESO").data("kendoComboBox").value("");


        $("#TMA_ADU_RECIBE").data("kendoComboBox").value("");
        $("#TMA_ADU_RECIBE").data('kendoComboBox').select(4);

        $("#TMA_CHEQUEADOR").data("kendoComboBox").value("");
        //$("#TMA_PORTON").data("kendoComboBox").value("");

        $('#btn_conocimientos').hide();
        $("#TMA_ESTADO").attr('disabled', true);

        if (!$('#frm_ingresoMerc').hasClass('fullHidden'))
            $('#btn_guardar_ingresoMerc').show();

        $('#btn_editar_ingresoMerc').hide();
        var ddl = $('#TMA_CLIENTE_ID').data("kendoComboBox");
        if (ddl != undefined)
            ddl.setDataSource([]);
        var ddl1 = $('#TMA_AER_NAEROLINEA').data("kendoComboBox");
        if (ddl1 != undefined)
            ddl1.setDataSource([]);
        //manifiestocontenedor_mod.limpiaFiltros();
         $('#ingmerc_flr_manifiesto').val('');
        $('#ingmerc_flr_fecha').val('');
        $('#ingmerc_flr_transportista').val('');
        $('#ingmerc_flr_cliente').val('');
        $('#ingmerc_flr_num_contenedor').val('');

        principal.deactiveLabelsInput();

        manifiesto_duas.limpiar();
        manifiesto_viaje.limpiar();
    },

    //Filtra si se escribe mas de un caracter o si no existe ninguno
    filtrar: function (input) {
        if (input.value.length > 1) {
            manifiestocontenedor_mod.buscar();
        } else {
            if (input.value.length === 0) {
                manifiestocontenedor_mod.buscar();
            }
        }
    },

    //Limpia los filtros
    limpiaFiltros: function () {
        $('#ingmerc_flr_manifiesto').val('');
        $('#ingmerc_flr_fecha').val('');
        $('#ingmerc_flr_transportista').val('');
        $('#ingmerc_flr_cliente').val('');
        $('#ingmerc_flr_num_contenedor').val('');
        manifiestocontenedor_mod.buscar();
    },

    //Si está en el listar regresa al form
    regresar: function () {
        if (!$('#tbl_ingresoMerc').hasClass('fullHidden'))
            $('#tbl_ingresoMerc').addClass('fullHidden')
        if (!$('#div_ingresoMerc_filtros').hasClass('fullHidden'))
            $('#div_ingresoMerc_filtros').addClass('fullHidden')
        if ($('#frm_ingresoMerc').hasClass('fullHidden'))
            $('#frm_ingresoMerc').removeClass('fullHidden')
        $('#btn_listar_ingresoMerc').show();


        $('#TMA_MANIFIESTO').focus();

         if($("#TMA_ID_MANIFIESTO").val() != ""){
            $('#btn_guardar_ingresoMerc').hide();
            $('#btn_editar_ingresoMerc').show();
        }else{
            $('#btn_guardar_ingresoMerc').show();
            $('#btn_editar_ingresoMerc').hide();
        }
        $('#btn_regresar_ingresoMerc').hide();
        if($("#TMA_ID_MANIFIESTO").val() != ""){
             $('#btn_conocimientos').show();
        }
    },

    //Busca el manifiesto y lo carga, esto para la edicion
    buscarMercancia: function (p_identificador) {
        principal.resetAllFields('frm_ingresoMerc');
        $("#TMA_ADU_RECIBE").data("kendoComboBox").value("");
        var tma_manifiesto_p = '';
        var tma_fch_manifiesto_p = '';
        var tma_aer_naerolinea_p = null;
        var cliente = '';
        var num_contenedor = '';
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MANIFIESTOS_TB/ManifiestoModificar?manifiesto_id=' + p_identificador + '&tma_manifiesto_p=' + tma_manifiesto_p + '&tma_fch_manifiesto_p=' + tma_fch_manifiesto_p + '&tma_aer_naerolinea_p=' + tma_aer_naerolinea_p + '&cliente=' + cliente + '&num_contenedor=' + num_contenedor, // + "&page=1&offset=" + manifiestocontenedor_mod.offset,
            success: function (data) {
                //$.each(data.REGISTROS[0], function (i, v) {
                $.each(data[0], function (i, v) {
                    principal.setValueByName(i, v, 'frm_ingresoMerc');
                    if (i === 'TMA_AER_NAEROLINEA') {
                        manifiestocontenedor_mod.codigo_aerolinea = v;
                    } else if (i === 'TMA_CLIENTE_ID') {
                        manifiestocontenedor_mod.codigo_cliente = v;
                    } else if (i === 'TMA_CLIENTE_TIPO') {
                        manifiestocontenedor_mod.getClient(manifiestocontenedor_mod.tipo_cliente, v, manifiestocontenedor_mod.codigo_cliente);
                    } else if (i == 'TMA_CLIENTE_NOMBRE') {
                        manifiestocontenedor_mod.tipo_cliente = v;
                    } else if (i == 'AEROLINEA') {
                        manifiestocontenedor_mod.aerolinea = v;
                    } else if (i == 'TMA_MOD_TRANSPORTE') {
                        manifiestocontenedor_mod.getTransportistas(v, manifiestocontenedor_mod.aerolinea, manifiestocontenedor_mod.codigo_aerolinea);
                    }
                });
                //$('#TMA_MOD_TRANSPORTE').change(function(){
                principal.activeLabelsInput();
                Cookies.set('tipo_transporte', $('#TMA_MOD_TRANSPORTE').val());
            },
            error: function (data) {
                swal({
                    title: "Contenedor/Manifiesto",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                });
            }
        });
    },

    //Hace el cambio de navegacion de botones y la busqueda del manifiesto o 
    editar: function (id) {
        manifiestocontenedor_mod.limpiar();
        //$("#TMA_ADU_RECIBE").data("kendoComboBox").value("");
        Cookies.set('manifiesto', id);
        manifiestocontenedor_mod.buscarMercancia(id);
        manifiesto_viaje.buscarViajes(id);
        manifiesto_duas.buscarDuas(id);
        if (!$('#tbl_ingresoMerc').hasClass('fullHidden'))
            $('#tbl_ingresoMerc').addClass('fullHidden')
        if (!$('#div_ingresoMerc_filtros').hasClass('fullHidden'))
            $('#div_ingresoMerc_filtros').addClass('fullHidden')
        if ($('#frm_ingresoMerc').hasClass('fullHidden'))
            $('#frm_ingresoMerc').removeClass('fullHidden')
        $('#btn_listar_ingresoMerc').show();
        $('#btn_guardar_ingresoMerc').hide();
        $('#btn_editar_ingresoMerc').show();
        $('#btn_conocimientos').show();
        $('#btn_regresar_ingresoMerc').hide();
        if ($('#btn_editar_ingresoMerc').hasClass('fullHidden'))
            $('#btn_editar_ingresoMerc').removeClass('fullHidden')
        $("#TMA_ESTADO").attr('disabled', false);
        principal.resetAllFields('div_ingresoMerc_filtros');      
    },

    redireccionar: function () {
         window.location.href = "Conocimientos_Modificar?id=" + $("#TMA_MANIFIESTO").val();
        //window.location.href = "Conocimientos";
    },

    //Actualiza un manifiesto especifico seleccionado desde el listado
    Actualizar: function () {
        $('#TMA_CLIENTE_NOMBRE').val($("#TMA_CLIENTE_ID").data("kendoComboBox").text());
        $('#TMA_USU_REGISTRO').val($('#UsReg').val());
        $('#TMA_USU_MODIFICO').val($('#UsReg').val());
        var id = $('#TMA_ID_MANIFIESTO').val();
        if (id.length > 0) {
            $("#TMA_ID_MANIFIESTO").attr('disabled', false);
            var data = principal.jsonForm($('#frm_ingresoMerc').serializeArray());
            $("#TMA_ID_MANIFIESTO").attr('disabled', true);
            $.ajax({
                // url: '/api/TX_MANIFIESTOS_TB/ActualizarManifiesto?id=' + id,
                url: '/api/TX_MANIFIESTOS_TB/ManifiestoModificar_Post?id=' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    var cb = function () {
                        swal({
                            title: "Contenedor/Manifiesto",
                            text: "Se actualizó correctamente el Contenedor/Manifiesto",
                            icon: "success",
                            button: "OK!",
                        }).then((value) => {
                            manifiesto_duas.limpiar();
                            manifiestocontenedor_mod.limpiar();
                            manifiesto_viaje.limpiar();
                        });
                    };
                    if (data != null) {
                        manifiesto_duas.delete(data, cb);
                        // manifiesto_viaje.delete(data);
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                    else {
                        swal({
                            title: "Contenedor/Manifiesto",
                            text: data.responseJSON.Message,
                            icon: "error",
                            button: "OK!",
                        });
                    }
                }
            });
        }
        else {
            swal({
                title: "Contenedor/Manifiesto",
                text: "El ID del Contenedor/Manifiesto no puede ser vacío",
                icon: "error",
                button: "OK!",

            });
        }
    }

};
$(document).ready(function () {
    manifiestocontenedor_mod.init();
});