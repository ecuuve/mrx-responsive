var productoNC_verificacion = {
	init: function(){
        TecladoFuncionalidadPersonalizada();
		//Al darle click a la cejilla se carga la tabla con las verificaciones ya insertadas en algun moemnto si existiesen
		$('#li_verificacionPNC').on('click', function(){
			productoNC_verificacion.buscar($('#frm_productoNC #pnc_identificador').val());
			$('#VNC_PNC_IDENTIFICADOR').val($('#frm_productoNC #pnc_identificador').val());
		});
		//Inicializa el componente de fecha
		$("#frm_productoNC_verificacion #VNC_FECHA").kendoDatePicker({
          format: "dd/MM/yyyy",
          value: new Date(),
          dateInput: true,
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });
	},
	//Trae la informacion relevante a la verificacion de un identificador particular o pnc, y la carga en la tabla.
	buscar: function(identificador){
		if(identificador != ''){
			$.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_VERIF_NC_TB/Descripciones?identificador='+identificador,
            success: function (data) {
                //Obtenemos la plantilla
                var templateText = $("#verificacion_producto_table-template").html();
                //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                var tableTemplate = Handlebars.compile(templateText);
                $("#verificacion_producto_tablebody").html('');
                //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                    $.each(data, function (index, elem) {
                        $("#verificacion_producto_tablebody").append(tableTemplate(elem));
                    });
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Verificación Productos No Conforme",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
                console.log("error");
            }
        });
		}
	},
	//Guarda en base de datos una verificación nueva perteneciente al pnc con el que se está trabajando
	guardar: function(){
		$.post(
            "/api/TX_VERIF_NC_TB/GuardarVerificacion/",
             $('#frm_productoNC_verificacion').serialize(),
            function (data) {
                if(data != null){
                	productoNC_verificacion.buscar($('#VNC_PNC_IDENTIFICADOR').val());
                    swal({
	                    title: "Verificación Producto No Conforme",
	                    text: "Verificación perteneciente al Producto No Conforme guardado exitosamente",
	                    icon: "success",
	                    button: "OK!",
	                    }).then((value) => {
                       		principal.resetAllFields('frm_productoNC_verificacion');
                    });
                }
            })
            .fail(function (data) {
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                }else{
                	swal({
                      title: "Verificación Producto No Conforme",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                    });
                }
            });
	},
	//Limpia los campos del form 
	limpiar: function(){
		$('#VNC_DESCRIPCION').val('');
		var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd/MM/yyyy');
		$("#VNC_FECHA").val(todayDate);
	}
};
$(document).ready(function () {
    productoNC_verificacion.init();
});