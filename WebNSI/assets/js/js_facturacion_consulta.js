var facturacion_consulta = {
     FACTURA: null,

    init: function () {
        TecladoFuncionalidadPersonalizada();

        $('#div_tarifa').addClass('fullHidden');
       
        $("#li_movimientos").attr('style', 'display:none');
        $("#li_adicionales").attr('style', 'display:none');
        $("#li_detalle").attr('style', 'display:none');
        $("#li_totales").attr('style', 'display:none');
        $("#li_totales_tarifa").attr('style', 'display:none');

        //Al dar click en el tab de totales trae los datos
        $('#totales-tab').on('click', function () { 
            facturacion_consulta.totales(facturacion_consulta.FACTURA);
        }); 

        $("#frm_registro_prefactura_cons :input").prop("disabled", true);
        $("#frm_detallefac_cons :input").prop("disabled", true);
        $("#frm_Valores :input").prop("disabled", true);
        
        $("#FAC_FACTURA").prop("disabled", false);
        $("#FAC_DUA_SALIDA").prop("disabled", false);
        $("#FAC_VIAJE_TRANSITO").prop("disabled", false);     

        $("#FAC_FACTURA").focus();
        $('#btn_reversion').hide();
        $('#btn_clonacion').hide();
        $('#btn_reimpresion').hide();


        //Al digitar la factura consulta 
        $('#FAC_FACTURA').change(function(){ 
            var factura_p = $("#FAC_FACTURA").val();          
            facturacion_consulta.getDatosFactura(factura_p);
        });

        //Al digitar el dua consulta 
        $('#FAC_DUA_SALIDA').change(function(){            
            facturacion_consulta.getDatosFactura();
        });

        //Al digitar el dua consulta 
        $('#FAC_VIAJE_TRANSITO').change(function(){            
            facturacion_consulta.getDatosFactura();
        });

         //Al seleccionar el declarante lista los agentes correspondientes
        $('#FAC_DCT_DECLARANTE_ID_CLO').change(function(){
          facturacion_consulta.getAgentesDecl($("#FAC_DCT_DECLARANTE_ID_CLO option:selected").val());
        });

         //Al seleccionar nombre de cuenta carga el número de cuenta
        $('#FAC_NOMBRE_CUENTA_CLO').change(function(){
            $("#FAC_CUENTA_CLO").val($("#FAC_NOMBRE_CUENTA_CLO option:selected").val());
            $('#lbl_FAC_CUENTA_CLO').addClass('active');
        });

        //Al seleccionar CONTRIBUYENTE carga el número de cuenta
        $('#FAC_NBR_CONTRIBUYENTE_CLO').change(function(){
            $("#FAC_CONTRIBUYENTE_CLO").val($("#FAC_NBR_CONTRIBUYENTE_CLO option:selected").val());
            $('#lbl_FAC_CONTRIBUYENTE_CLO').addClass('active');
        });
    },

    //Obtiene datos de la factura
    getDatosFactura: function (factura_p) { 
        //var factura_p = $("#FAC_FACTURA").val();
        var dua_p = $("#FAC_DUA_SALIDA").val();
        var viaje_p = $("#FAC_VIAJE_TRANSITO").val();
        facturacion_consulta.FACTURA = factura_p; 

        var cb = function(data){

            if(data.length > 1){

                $("#myModalFacturas").modal('show');
                //Obtenemos la plantilla
                var templateText = $("#tb_facturas_table-template").html();
                  //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                var tableTemplate = Handlebars.compile(templateText);
                $("#tb_facturas_tablebody").html('');
                $.each(data, function (index, elem) {
                    $("#tb_facturas_tablebody").append(tableTemplate(elem));
                });

            }else{
                $.each(data[0], function(i,v){             
                  principal.setValueByName(i, v,'frm_registro_prefactura_cons');
                  principal.setValueByName(i, v,'frm_detallefac_cons');                
                }); 

                principal.activeLabels();
              
                  //Carga en movimientos, los valores heredados 
                  $("#FAC_FACTURA_ENC").val($("#FAC_FACTURA").val());
                  $("#FAC_DUA_SALIDA_ENC").val($("#FAC_DUA_SALIDA").val());
                  $("#FAC_VIAJE_TRANSITO_ENC").val($("#FAC_VIAJE_TRANSITO").val());
              

                $("#li_movimientos").attr('style', 'display:block');
                $("#li_adicionales").attr('style', 'display:block');
                $("#li_detalle").attr('style', 'display:block');
                $("#li_totales").attr('style', 'display:block');
                $("#li_totales_tarifa").attr('style', 'display:block');


                if(data[0].FAC_ESTADO != "Borrada"){
                    $('#btn_reversion').show();
                }

                 if(data[0].FAC_ESTADO == "Borrada"){
                    $('#btn_clonacion').show();
                }
               
               
               $('#btn_reimpresion').show();

                $("#FAC_FACTURA").prop("disabled", true);
                $("#FAC_DUA_SALIDA").prop("disabled", true);
                $("#FAC_VIAJE_TRANSITO").prop("disabled", true); 

                facturacion_consulta.getMovimientosFactura(factura_p);
                facturacion_consulta.getAdicionalesFactura(factura_p);
                facturacion_consulta.totalesTarifa();

            }
            
        };
        principal.get("/api/TX_FACTURAS_CONSULTA_TB/GetObtieneFactura?factura=" + factura_p + "&viaje=" + viaje_p + "&dua=" + dua_p,'',cb);
    },

    //Limpia todos los campos de pantalla
    limpiarCamposFactura: function () {
        principal.resetAllFields('detInfo_registro_prefactura_cons');
                
        $("#FAC_FACTURA").focus();
        $('#btn_reversion').hide();
        $('#btn_clonacion').hide();
        $('#btn_reimpresion').hide();

        $("#li_movimientos").attr('style', 'display:none');
        $("#li_adicionales").attr('style', 'display:none');
        $("#li_detalle").attr('style', 'display:none');
        $("#li_totales").attr('style', 'display:none');
        $("#li_totales_tarifa").attr('style', 'display:none');
        $("#li_registro_prefactura").attr('style', 'display:block');
        
        if (!$('#registro_prefactura').hasClass('active'))
             $('#registro_prefactura').addClass('show');
             $('#registro_prefactura').addClass('active');
            $('#movimientos').removeClass('active');
            $('#adicionales').removeClass('active');
            $('#detalle').removeClass('active');
            $('#totales').removeClass('active');
            $('#totales_tarifa').removeClass('active');
        
        if (!$('#registro_prefactura-tab').hasClass('active'))
             $('#registro_prefactura-tab').addClass('active');
            $('#movimientos-tab').removeClass('active');
            $('#adicionales-tab').removeClass('active');
            $('#detalle-tab').removeClass('active');
            $('#totales-tab').removeClass('active');
            $('#totales_tarifa-tab').removeClass('active');

        $("#FAC_FACTURA").prop("disabled", false);
        $("#FAC_DUA_SALIDA").prop("disabled", false);
        $("#FAC_VIAJE_TRANSITO").prop("disabled", false);  

        $("#FAC_FACTURA").focus();   
    },

    //Busca y lista los movimientos de la factura
    getMovimientosFactura: function (factura_p) {
        var cb = function(data){
            //Obtenemos la plantilla
            var templateText = $("#movimientosfac_table-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#movimientosfac_tablebody").html('');
            $.each(data, function (index, elem) {
                $("#movimientosfac_tablebody").append(tableTemplate(elem));
            });
        };
        principal.get("/api/TX_FACTURAS_CONSULTA_TB/GetMovimientosFact?factura=" + factura_p,'',cb); 
    },

    // Obtiene los totales de la factura
    totalesTarifa: function(){
        var fac_p = facturacion_consulta.FACTURA;
        var cb = function(data){

            //Obtenemos la plantilla
            var templateText = $("#totalesfac_tarifa_table-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#totalesfac_tarifa_tablebody").html('');
            $.each(data, function (index, elem) {
                $("#totalesfac_tarifa_tablebody").append(tableTemplate(elem));
            });          
        };
        principal.ajax("GET","/api/TX_FACTURAS_TB/GetTotalesTarifa?factura=" + fac_p,cb, "Facturación", ""); 
    },

    //Carga la factura seleccionada en la lista
    selectFactura: function(factura_p){
        $("#myModalFacturas").modal('hide');
        $("#myModalFacturasporMov").modal('hide');
       facturacion_consulta.limpiarCamposFactura();
       facturacion_consulta.getDatosFactura(factura_p);
    },

    //Muestra el modal de valores  
    modal_ValoresMovimiento: function (id_p) {              
        $("#myModalValores").modal('show');
        $("#FMV_PCT_DESCUENTO").val("0");
        $('#frm_Valores label').addClass('active');
        

        facturacion_consulta.getCedulaEndoso(id_p);
        

        var cb = function(data){
            $.each(data[0], function(i,v){
              principal.setValueByName(i, v,'frm_Valores');
            }); 
        };
        principal.get("/api/TX_FACTURAS_TB/GetObtMovimientosFact?factura=" + facturacion_consulta.FACTURA + '&mov='+id_p,'',cb); 
    },

    //Muestra el modal de tarimas  
    modal_tarimasMovimiento: function (id_p, mov_p, tarifa_p) {              
        $("#myModalTarimas").modal('show');
        $("#num_movimiento").html(mov_p);
        $("#id_movimiento").val(id_p);
        $("#nom_tarifa").html(tarifa_p);
        //$('#div_edit_tarima').addClass('fullHidden');


        facturacion_consulta.obtieneTarimas(id_p);
    },

    //Obtiene la cédula endoso
    getCedulaEndoso: function (mov_p) {
        principal.getData(
            "/api/TX_FACTURAS_TB/GetCedEndoso?mov_p=" + mov_p,
            function (data) {
                if(data != ""){
                   $("#CED_ENDOSO").val(data);
                }
            }
        )
    },

    //Lista las tarimas del movimiento dado
    obtieneTarimas: function(id_p){
        var cb = function(data){
            //Obtenemos la plantilla
            var templateText = $("#tarimasMovimiento_table-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#tarimasMovimiento_tablebody").html('');
            $.each(data, function (index, elem) {
                $("#tarimasMovimiento_tablebody").append(tableTemplate(elem));
            });
        };
        principal.get("/api/TX_FACTURAS_CONSULTA_TB/GetTarimas?factura_p=" + facturacion_consulta.FACTURA + '&id_mov_p='+id_p,'',cb); 
    },

    //Busca y lista los adicionales de la factura
    getAdicionalesFactura: function (factura_p) {
        var id = null;
        var cb = function(data){
            //Obtenemos la plantilla
            var templateText = $("#adicionalesfac_table-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#adicionalesfac_tablebody").html('');
            $.each(data, function (index, elem) {
                $("#adicionalesfac_tablebody").append(tableTemplate(elem));
            });
        };
        principal.get("/api/TX_FACTURAS_CONSULTA_TB/GetAdicionales?factura=" + factura_p,'',cb); 
    },

    //Muestra el modal de valores  
    modal_ConsultaporMovimiento: function () { 
        $("#myModalFacturasporMov").modal('show');
        $("#in_movimiento").focus();   
    },

    //Ejecuta acción al digitar el movimiento
    consultaMov: function () {
        var mov_p = $("#in_movimiento").val();              
        facturacion_consulta.consultaporMovimiento(mov_p);
    }, 

    //Limpia modal de consulta por movimiento
    consultaMovLimpiar: function (){
        $("#tb_facturas_mov_tablebody").html('');
        $("#in_movimiento").val('');
        $("#in_guia").val('');
        $("#in_movimiento").focus();
    },

    //Consulta facturas por movimiento 
    consultaporMovimiento: function (mov_p) {                       
        var cb = function(data){
            //Obtenemos la plantilla
            var templateText = $("#tb_facturas_mov_table-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#tb_facturas_mov_tablebody").html('');
            $.each(data, function (index, elem) {
                $("#tb_facturas_mov_tablebody").append(tableTemplate(elem));
            }); 
        };
        principal.get("/api/TX_FACTURAS_CONSULTA_TB/GetObtFactxMov?movimiento=" + mov_p,'',cb); 
    },

    // Obtiene los totales de la factura
    totales: function(){
        var fac_p = facturacion_consulta.FACTURA;
        var cb = function(data){

            //Obtenemos la plantilla
            var templateText = $("#totalesfac_table-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#totalesfac_tablebody").html('');
            $.each(data, function (index, elem) {
                $("#totalesfac_tablebody").append(tableTemplate(elem));
            });
          
        };
        principal.ajax("GET","/api/TX_FACTURAS_TB/GetTotales?factura=" + fac_p,cb, "Facturación", ""); 
    },

    //Valida que se realice la reversión
    reversion: function(){
        //var fac =  $('#FAC_FACTURA').val();
        swal({
          title: "Está seguro?",
          text: "Se realizará la reversión de la factura: " + facturacion_consulta.FACTURA,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            facturacion_consulta.reversarFactura();                     
          } 
        });    
    },

    //Realiza la reversión de factura
    reversarFactura: function(){
        var fac_p =  facturacion_consulta.FACTURA;//$("#FAC_TIQUETE").val();
        var estado_p = "B"
        var user_p = $("#UsReg").val();
        var cb = function(data){
            swal({
                title: "Reversión",
                text: "Se reversó la factura: " + fac_p,
                icon: "success",
                button: "OK!",
            }).then((willDelete) => {
              if (willDelete) {
                facturacion_consulta.limpiarCamposFactura();                  
              } 
            });   
        };
        principal.ajax("POST","/api/TX_FACTURAS_CONSULTA_TB/REVERSAR?fac_p=" + fac_p +'&estado_p=' + estado_p +'&user_p=' + user_p,cb, "Consulta Facturación", ""); 
    },

    //Valida la reimpresión de factura
    reimpresion: function(){
        swal({
          title: "Está seguro?",
          text: "Se realizará una reimpresión de la factura: " + facturacion_consulta.FACTURA,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            facturacion_consulta.reimprimeFactura(facturacion_consulta.FACTURA);                     
          } 
        });    
    },

    //Realiza la reimpresión de factura
    reimprimeFactura: function(){
     swal({
            title: "Reimpresión",
            text: "Se reimprime la factura: " + facturacion_consulta.FACTURA,
            icon: "success",
            button: "OK!",
        })
        .then((willDelete) => {
          if (willDelete) {
            //Llama el servicio de impresión 
           window.open("http://merxalmacenadora/WebEtiquetaNCI/RepComprobanteSalHR.aspx?Factura=" + facturacion_consulta.FACTURA);                  
          } 
        });
    },

    //Abre el modal de clonación
    clonacion: function(){
        $("#myModalClonacion").modal('show'); 
        facturacion_consulta.cargaInicialClonar();
    },

    //Carga los controles de clonación
    cargaInicialClonar: function(){
        facturacion_consulta.getTipoDocumento();
        facturacion_consulta.getDeclarantes();
        facturacion_consulta.getCuentas("L",null,null,null,null,null);
        facturacion_consulta.getCuentas("N",null,null,null,null,null);
    },

    //Carga tipo doc
    getTipoDocumento: function () {
        var cb = function(data){
            //$("#FAC_DIN_TIPO_DOCUMENTO_CLO").html(principal.arrayToOptions(data));
            principal.KendoComboBox(data, '#FAC_DIN_TIPO_DOCUMENTO_CLO');
        };
        principal.get("/api/TX_FACTURAS_TB/TipoDocumento",'',cb);
    },

    //Lista los declarantes
    getDeclarantes: function (e = null, id = null) {
         var cliente = e != null ? e : '';
         // $("#FAC_DCT_DECLARANTE_ID_CLO").kendoComboBox({
         //        placeholder: "",
         //        dataTextField: "Nombre",
         //        dataValueField: "Id",
         //        filter: "contains",
         //        autoBind: true,
         //        minLength: 1,
         //        dataSource: {
         //            serverFiltering: true,
         //            transport: {
         //                read: function (options) {
         //                    if (options.data.filter && options.data.filter.filters.length > 0 ) {
         //                        cliente = options.data.filter.filters[0].value;
         //                    }
         //                    $.ajax({
         //                        type: "GET",
         //                        url: "/api/TX_FACTURAS_TB/GetDeclarantes?id=" + cliente + "&nombre=" + cliente,
         //                        success: function (data) {
         //                            options.success(data);
         //                        }
         //                    });
         //                }
         //            }
         //        }
         //    });

            // if (id != null) {
            //     $("#FAC_DCT_DECLARANTE_ID_CLO").data("kendoComboBox").dataSource.read();
            //     $("#FAC_DCT_DECLARANTE_ID_CLO").data("kendoComboBox").value(id);
            // }


        var cb = function(data){
        principal.KendoComboBox(data, '#FAC_DCT_DECLARANTE_ID_CLO');
        };
        principal.get("/api/TX_FACTURAS_TB/GetDeclarantes?id=" + "" + "&nombre=" + "",'',cb);
    },

    //Lista los agentes del declarante seleccionado
    getAgentesDecl: function (id) {
        var cb = function(data){
        principal.KendoComboBox(data, '#FAC_ADC_AGENTE_ID_CLO');
        };
        principal.get("/api/TX_FACTURAS_TB/GetAgentesDecl?declarante=" + id,'',cb);
    },

    //Obtiene las cuentas de la bd según filtros
    getCuentas: function (tipo_p,nombre_p,cuenta_p,forma_pago_p,tipo_cta_p,importador_p) {
        $.ajax({
            method: 'POST',
            dataType: 'JSON',
            url: '/api/TX_FACTURAS_TB/BuscarCuentas', 
            data: { tipo_p: tipo_p, nombre_p: nombre_p, cuenta_p: cuenta_p, forma_pago_p: forma_pago_p, tipo_cta_p: tipo_cta_p, importador_p: importador_p },
            success: function (data) {
                if (data !== null && tipo_p == "L") {                
                    principal.KendoComboBox(data, '#FAC_NOMBRE_CUENTA_CLO');
                }
                 if (data !== null && tipo_p == "N") {                
                    principal.KendoComboBox(data, '#FAC_NBR_CONTRIBUYENTE_CLO');
                }

            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(textStatus);
                //console.log("error consulta");
            }
        });
    },

    //Limpia el formulario de clonación
    limpiaClon: function(){
        principal.resetAllFields('info_clona');

        var ddl = $('#FAC_NOMBRE_CUENTA_CLO').data("kendoComboBox");
        if(ddl != undefined)
        ddl.setDataSource([]);

        var ddl2 = $('#FAC_DIN_TIPO_DOCUMENTO_CLO').data("kendoComboBox");
        if(ddl2 != undefined)
        ddl2.setDataSource([]);

        var ddl3 = $('#FAC_DCT_DECLARANTE_ID_CLO').data("kendoComboBox");
        if(ddl3 != undefined)
        ddl3.setDataSource([]);

        var ddl4 = $('#FAC_ADC_AGENTE_ID_CLO').data("kendoComboBox");
        if(ddl4 != undefined)
        ddl4.setDataSource([]);

        var ddl5 = $('#FAC_NBR_CONTRIBUYENTE_CLO').data("kendoComboBox");
        if(ddl5 != undefined)
        ddl5.setDataSource([]);

        facturacion_consulta.cargaInicialClonar();
    },

    //Clona la factura
    clonarFactura: function(){
        $("#FAC_CUENTA_CLO").attr('disabled', false);
        $("#FAC_CONTRIBUYENTE_CLO").attr('disabled', false);

        $("#FAC_USU_DIGITO_CLO").val($("#UsReg").val());
        $("#FAC_FACTURA_CLO").val(facturacion_consulta.FACTURA);

        $.post(
          "/api/TX_FACTURAS_CONSULTA_TB/ClonarFactura/",
           $('#frm_clonar').serialize(),
          function (data) {
              if(data != null && data != ""){
                 $("#NUEVA_FAC").val(data);
                swal({
                    title: "Clonación",
                    text: "Se clona la factura: " + facturacion_consulta.FACTURA,
                    icon: "success",
                    button: "OK!",
                });

              }
          })
          .fail(function (data) {
              if (data.responseJSON.ModelState) {
                  principal.processErrorsPopUp(data.responseJSON.ModelState);
              }else{
                swal({
                    title: "Clonación de Facturas",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                  });
              }
          });


       $("#FAC_CUENTA_CLO").attr('disabled', true);
       $("#FAC_CONTRIBUYENTE_CLO").attr('disabled', true);
    },

    //Ciera el modal de clonación y regresa a pantalla de búsqueda
    regresar: function(){
        $("#myModalClonacion").modal('hide');
        //facturacion_consulta.limpiarCamposFactura();
    }

};

$(document).ready(function () {
    facturacion_consulta.init();
})