var productoNC_detalle = {
	init: function(){
		TecladoFuncionalidadPersonalizada();
		//Al dar click a la cejilla se carga la tabla de detalles existentes para ese identificador
		$('#li_detallepnc').on('click', function(){
			productoNC_detalle.buscar($('#pnc_identificador').val(),'');
			$('#UsReg_pncIdentificador').val($('#pnc_identificador').val());
		});
		$('#TNC_TARIMA').bind("cut copy paste",function(e) {
             e.preventDefault();
         });
		$('#TNC_BULTOS').bind("cut copy paste",function(e) {
             e.preventDefault();
         });
		$('#TNC_BULTOS_NC').bind("cut copy paste",function(e) {
             e.preventDefault();
         });
		
	},
	//Guarda en base de datos un nuevo detalle para esa pnc
	guardar: function(){
		var bultos = parseInt($('#TNC_BULTOS').val());
		var bultosNC = parseInt($('#TNC_BULTOS_NC').val());
		if(bultosNC <= bultos){
			$.post(
	            "/api/TX_TARIMAS_PROD_NC_TB/GuardarDetallePNC/",
	             $('#frm_productoNC_detalle').serialize(),
	            function (data) {
	                if(data != null){
	                	productoNC_detalle.buscar($('#UsReg_pncIdentificador').val(),'');
	                    swal({
		                    title: "Detalle Producto No Conforme",
		                    text: "Tarimas pertenecientes al Producto No Conforme guardado exitosamente",
		                    icon: "success",
		                    button: "OK!",
		                    }).then((value) => {
	                       		productoNC_detalle.limpiar();
	                    });
	                }
	            })
	            .fail(function (data) {
	                if (data.responseJSON.ModelState) {
	                    principal.processErrorsPopUp(data.responseJSON.ModelState);
	                }else{
	                	swal({
	                      title: "Producto No Conforme",
	                      text: data.responseJSON.Message,
	                      icon: "error",
	                      button: "OK!",
	                    });
	                }
	            });
	        }else{
	        	swal({
	              title: "Bultos",
	              text: 'Los Bultos NC NO pueden ser mayores a Bultos',
	              icon: "error",
	              button: "OK!",
	            });
	            $('#TNC_BULTOS_NC').val('');
	        }
	},
	// en la tabla se selecciona un detalle y mediante esta funcion se edita la informacion corespondiente al mismo
	actualizar: function(){
		var bultos = parseInt($('#TNC_BULTOS').val());
		var bultosNC = parseInt($('#TNC_BULTOS_NC').val());
		if(bultosNC <= bultos){
	        var data = principal.jsonForm($('#frm_productoNC_detalle').serializeArray());
	        $.ajax({
	            url: '/api/TX_TARIMAS_PROD_NC_TB/',
	            type: 'PUT',
	            data: JSON.stringify(data),
	            contentType: "application/json",
	            success: function (data) {
	               if(data != null){
	                   swal({
	                        title: "Detalle Producto No Conforme",
	                        text: "Se actualizó correctamente el Detalle Producto No Conforme",
	                        icon: "success",
	                        button: "OK!",
	                    });
	                   productoNC_detalle.limpiar();
	                   productoNC_detalle.buscar($('#pnc_identificador').val(),'');
	               }
	            },
	            error: function (data) {
	                if (data.responseJSON.ModelState) {
	                    principal.processErrorsPopUp(data.responseJSON.ModelState);
	                }
	                else{
	                	swal({
	                      title: "Detalle Producto No Conforme",
	                      text: data.responseJSON.Message,
	                      icon: "error",
	                      button: "OK!",
	                    });
	                }
	            }
	        });
	        }else{
	        	swal({
	              title: "Bultos",
	              text: 'Los Bultos NC NO pueden ser mayores a Bultos',
	              icon: "error",
	              button: "OK!",
	            });
	            $('#TNC_BULTOS_NC').val('');
	        }
	},
	//Limpia los campos correspondientes al formulario
	limpiar: function(){
		$('#TNC_TARIMA').val('');
		$('#TNC_BULTOS').val('');
		$('#TNC_BULTOS_NC').val('');
		if(!$('#btn_actualiza_detalle').hasClass('fullHidden'))
    		$('#btn_actualiza_detalle').addClass('fullHidden')
    	if($('#btn_crea_detalle').hasClass('fullHidden'))
    		$('#btn_crea_detalle').removeClass('fullHidden')
	},
	//Valida que el campo Bultos NC no sea mayor al campo Bultos
	validarBultos: function(bultosNc){
		var bultos = $('#TNC_BULTOS').val();
		if(bultosNc > bultos){
			return true;
		}else{
			return false;
		}
	},
	//Funcion que permite listar en la tabla todos los detalles pertenecientes al indicador dado
	buscar: function(identificador, secuencia){
		if(identificador != ''){
			$.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_TARIMAS_PROD_NC_TB/Tarimas?identificador='+identificador + '&&secuencia=' + secuencia,
            success: function (data) {
                //Obtenemos la plantilla
                var templateText = $("#detalle_producto_table-template").html();
                //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                var tableTemplate = Handlebars.compile(templateText);
                $("#detalle_producto_tablebody").html('');
                //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                    $.each(data, function (index, elem) {
                        $("#detalle_producto_tablebody").append(tableTemplate(elem));
                    });
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Detalle Productos",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
                console.log("error");
            }
        });
		}
	},
	//Trae la informacion de una linea especifica y pinta  la información en los campos correspondientes
	editar: function(identificador, secuencia){
		$.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_TARIMAS_PROD_NC_TB/Tarimas?identificador='+identificador + '&&secuencia=' + secuencia,
            success: function (data) {
            	$.each(data[0], function(i,v){
            		principal.setValueByName(i, v,'frm_productoNC_detalle');
            	});
            	if($('#btn_actualiza_detalle').hasClass('fullHidden'))
            		$('#btn_actualiza_detalle').removeClass('fullHidden')
            	if(!$('#btn_crea_detalle').hasClass('fullHidden'))
            		$('#btn_crea_detalle').addClass('fullHidden')
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Detalle Productos",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
                console.log("error");
            }
        });
	}
};
$(document).ready(function () {
    productoNC_detalle.init();
});