var tarima = {
    
    init: function () {
        //principal.hidemenu();
        $('#in_mta_bultos_dezp').focus();
        $('#th_tarima').hide();
        $('#div_filters').hide();
        TecladoFuncionalidadPersonalizada();
        tarima.cargarcontrolesTarima();
        tarima.listenerFiltersTarimas('txt_filtro_embalaje','s_mta_embalaje');
        tarima.listenerFiltersTarimas('txt_filtro_tipo_Mercancia','s_mta_tipo_mercancia');
        tarima.listenerFiltersTarimas('txt_filtro_Peligrosidad', 's_mta_Peligrosidad');
        tarima.BuscarTarimaEnter();
        tarima.fsumakilos(); 
        tarima.cambiarEstadoBotones(false, false, false, true, false, true, false);      
        tarima.ValidacionesEnPantalla();

    },


// Validaciones en pantalla
    ValidacionesEnPantalla: function(){

        $('#s_mta_Peligrosidad').change(function () {
            tarima.ValidacionesPeligrosidad($('#s_mta_Peligrosidad').val());
        });

        $('#in_mta_indice_transp').change(function () {
            tarima.ValidacionesIndiceTrans($('#s_mta_Peligrosidad').val(),$('#in_mta_indice_transp').val());
        });

        $('#in_mta_codigo_un').change(function () {
            tarima.ValidacionesCodigoUN($('#s_mta_Peligrosidad').val(),$('#in_mta_codigo_un').val());
        });

        $('#s_mta_embalaje').change(function () {
            tarima.ValidacionesEmbalaje($('#s_mta_operacion_tica').val(),$('#s_mta_embalaje').val());
        });

        $('#s_mta_operacion_tica').change(function () {
            tarima.ValidacionesOperTica($('#s_mta_operacion_tica').val(),$('#s_mta_embalaje').val());
        });

        $('#in_mta_tarima_ref').change(function () {
            tarima.ValidacionesTarimaRef($('#s_mta_operacion_tica').val(),$('#in_mta_tarima_ref').val());
        });

        $('#in_mta_Rack').change(function () {
            tarima.ValidacionesRack($('#s_mta_estado').val(),$('#in_mta_Rack').val());
        });

        $('#s_mta_estado').change(function () {
            tarima.ValidacionesEstado($('#in_mta_Rack').val(),$('#s_mta_estado').val());
        });

        $("#MER_ID_MERCANCIA_REF").change(function () {
            tarima.ValidacionesTarimaReferencia();
        })

        $('#in_mta_bultos_dezp').change(function () {
            var bultos = $('#in_mta_bultos_dezp').val();
            if(bultos < 1){
                swal({
                title: "Tarimas",
                text: "Los bultos tarima deben ser mayores a cero.",
                icon: "info",
                button: "OK!",
                timer: 5000,
                }) 
            }
        });

        $('#in_mta_kg_bruto').change(function () {
            var kilos = $('#in_mta_kg_bruto').val();
            if(kilos < 0){
                swal({
                title: "Tarimas",
                text: "Los kilos bruto no pueden ser menores a cero.",
                icon: "info",
                button: "OK!",
                timer: 5000,
                }) 
            }
        });

        $('#chb_mta_refrigerada').change(function () {
             var tipo = $('#s_mta_tipo_mercancia').val();
             var valor = $('[name= "MER_REFRIGERADO"]').is(":checked");
             if (valor) {
                //Checkbox has been checked
              if(tipo != 8 || tipo != 15){
                swal({
                title: "Tarimas",
                text: "Combinación entre check de refrigerado y el tipo de mercancia no corresponde.",
                icon: "error",
                button: "OK!",
                timer: 5000,
                }) 
              }                
            }else{
                //Checkbox has been unchecked
                if(tipo == 8 || tipo == 15){
                    swal({
                    title: "Tarimas",
                    text: "Combinación entre check de refrigerado y el tipo de mercancia no corresponde.",
                    icon: "error",
                    button: "OK!",
                    timer: 5000,
                    }) 
                }
            }
        });
    },

    ValidacionesPeligrosidad: function(valor){
        if(valor != 7){
            $('#in_mta_indice_transp').val("");
            $('#in_mta_codigo_un').focus();   
        }
        if(valor == 9){
            $('#in_mta_indice_transp').val("");
            $('#in_mta_codigo_un').val("");
            $('#in_mer_linea_guia').focus();
        }
        if(valor == 7){
            $('#in_mta_indice_transp').focus();
        }
    },

    ValidacionesIndiceTrans: function(pelig,valor){     
        if(pelig == 7 && (valor == "" || valor == "0")){
            swal({
                title: "Tarimas",
                text: "DEBE INGRESAR INDICE DE TRANSPORTE PARA RADIOACTIVOS",
                icon: "info",
                button: "OK!",
                timer: 5000,
            }) 
            $('#in_mta_indice_transp').focus();
        }
        
        if(pelig == 0 && valor != ""){
            swal({
                title: "Tarimas",
                text: "NO REQUIERE INDICE DE TRANSPORTE",
                icon: "info",
                button: "OK!",
                timer: 5000,
            })

        }
    },

    ValidacionesCodigoUN: function(pelig,valor){     
        if(pelig != 0 && (valor == "" || valor == "0")){
            swal({
                title: "Tarimas",
                text: "VALOR DE CÓDIGO U.N. INCORRECTO",
                icon: "info",
                button: "OK!",
                timer: 5000,
            }) 
            $('#in_mta_codigo_un').focus();
        }
        
        if(pelig == 0 && valor != ""){
            swal({
                title: "Tarimas",
                text: "NO REQUIERE CÓDIGO U.N.",
                icon: "info",
                button: "OK!",
                timer: 5000,
            })

        }
    },

    ValidacionesEmbalaje: function(operT,valor){     
        if(operT == "F" && (valor == "PAE" || valor == "BDL" || valor == "BSK" || valor == "CNT" || valor == "GUA")){
            swal({
                title: "Tarimas",
                text: "Tipo de embalaje no corresponde a operación Fraccionamiento.",
                icon: "info",
                button: "OK!",
                timer: 5000,
            })      
        }
    },

    ValidacionesOperTica: function(operT,valor){     
        var mercRef = $('#in_mta_tarima_ref').val();
        if(operT == "F" && (valor == "PAE" || valor == "BDL" || valor == "BSK" || valor == "CNT" || valor == "GUA")){
            swal({
                title: "Tarimas",
                text: "Tipo de embalaje no corresponde a operación Fraccionamiento.",
                icon: "info",
                button: "OK!",
                timer: 5000,
            }) 
            
        }
        if(operT != "F" && mercRef != ""){
            swal({
                title: "Tarimas",
                text: "Operación no permite digitar referencia.",
                icon: "info",
                button: "OK!",
                timer: 5000,
            })

        }       
    },

    ValidacionesTarimaRef: function(operT,valor){     
        if(operT != "F" && valor != "" ){
            swal({
                title: "Tarimas",
                text: "Operación no permite digitar referencia.",
                icon: "info",
                button: "OK!",
                timer: 5000,
            }) 
            
        }
        if(operT == "F" && valor == ""){
            swal({
                title: "Tarimas",
                text: "Si operación tica es Fraccionamiento debe digitar referencia.",
                icon: "info",
                button: "OK!",
                timer: 5000,
            })
            $('#in_mta_tarima_ref').focus();
        }       
    },
    
    ValidacionesRack:function(estado, valor){
        if(valor != 66 && estado != "V"){
            swal({
                title: "Tarimas",
                text: "Solo se permite localizar en piso (66).",
                icon: "info",
                button: "OK!",
                timer: 5000,
                }) 
            $('#in_mta_Rack').focus();
        }
        if(valor != 0 && estado == "V"){
            swal({
                title: "Tarimas",
                text: "Las virtuales deben localizarse en posición cero.",
                icon: "info",
                button: "OK!",
                timer: 5000,
                }) 
            $('#in_mta_Rack').focus();
        }
    },

    ValidacionesEstado:function(rack, valor){
        if(rack != 66 && valor != "V"){
            swal({
                title: "Tarimas",
                text: "Solo se permite localizar en piso (66).",
                icon: "info",
                button: "OK!",
                timer: 5000,
                }) 
            $('#in_mta_Rack').focus();
        }
        if(rack != 0 && valor == "V"){
            swal({
                title: "Tarimas",
                text: "Las virtuales deben localizarse en posición cero.",
                icon: "info",
                button: "OK!",
                timer: 5000,
                }) 
            $('#in_mta_Rack').focus();
        }
    },

    ValidacionesTarimaReferencia: function () {
        var tarimaReferencia = $("#MER_ID_MERCANCIA_REF").val();
        $.ajax({
            url: '/api/TX_MERCANCIAS_TB_Tarimas/GetMercanciasEntrada?entrada=&mercancia=' + tarimaReferencia,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (count(data) === 0) {
                    swal({
                        title: "Tarimas",
                        text: "La tarima de referencia no existe.",
                        icon: "error",
                        button: "OK!",
                        timer: 5000,
                    });
                }
            }
        });
    },

// Clase para manejar el focus en el camp
    onfocus: function () {
        if ($("#in_mta_bultos_dezp").hasClass('lblfocus'))
            $("#in_mta_bultos_dezp").removeClass('lblfocus');
    },

//Manejo de la citan de  botones 
    cambiarEstadoBotones: function (regresar, listar, guardar, editar, limpiar, filtro, guia) {
        $('#btn_listar_tarima').prop('disabled', listar);
        $('#btn_guardar_tarima').prop('disabled', guardar);
        $('#btn_editar_tarima').prop('disabled', editar);
        $('#btn_limpiar_tarima').prop('disabled', limpiar);        
        $('#btn_filters_tarima').prop('disabled', filtro);
        $('#btn_guia').prop('disabled', guia);

        if (regresar == true) {
            $("#btn_regresar_tarima").show();
        }
        else {
            $("#btn_regresar_tarima").hide();
        }
    },

//Carga de controles 
    cargarcontrolesTarima: function () {        
        tarima.getTipoMercancia(); 
        tarima.getTipoPeligrosidadIATA();
        tarima.getCondiciones(); 
        tarima.getEmbalajes();
        tarima.getEstadosTarima();                  
        tarima.getOperacionTica();
        
        tarima.limpiarCamposTarimas();
        //tarima.getRefrigerado();
        $("#in_mer_linea_guia").val("1");
        $("#in_mta_Rack").val("66");
        $("#in_mta_columna").val("1");
        $("#in_mta_lado").val("0");
        $("#in_mta_Rack").attr('disabled', true);
        $("#in_mta_columna").attr('disabled', true);
        $("#in_mta_lado").attr('disabled', true);
        $("#s_mta_estado").attr('disabled', true);
    },

// Carga listas de valores

    getTipoMercancia: function (valor) {
        if (valor === "") {
            valor = "0";
        }
        principal.getData(
            "/api/TX_MERCANCIAS_TB_Tarimas/TipoMercancia",
            function (data) {
                $("#s_mta_tipo_mercancia").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        )
    },

    getTipoPeligrosidadIATA: function (valor) {
        if (valor === "") {
            valor = "0";
        }
        principal.getData(
            "/api/TX_MERCANCIAS_TB_Tarimas/TipoPeligrosidadIATA",
            function (data) {
                $("#s_mta_Peligrosidad").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        )
    },

    getCondiciones: function (valor) {
        if (valor === "") {
            valor = "0";
        }
        principal.getData(
            "/api/TX_MERCANCIAS_TB_Tarimas/Condicion",
            function (data) {
                $("#s_mta_condicion").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        )
    },

    getEmbalajes: function (valor) {
        if (valor === "") {
            valor = "0";
        }
        principal.getData(
            "/api/TX_MERCANCIAS_TB_Tarimas/Embalajes",
            function (data) {
                $("#s_mta_embalaje").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        )
    },

    getEstadosTarima: function (valor) {
        if (valor === "") {
            valor = "0";
        }
        principal.getData(
            "/api/TX_MERCANCIAS_TB_Tarimas/Estados",
            function (data) {
                $("#s_mta_estado").html(principal.arrayToOptionsSelected(data, valor));
            }
        )
    },

    getOperacionTica: function (valor) {
        if (valor === "") {
            valor = "0";
        }
        principal.getData(
            "/api/TX_MERCANCIAS_TB_Tarimas/OperacionTica",
            function (data) {
                $("#s_mta_operacion_tica").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        )
    },

//Asigna valores a los checkbox
    getPerecedera: function (valor) {
        if(valor == "S"){
                $('#chb_mta_perecedera').prop('checked', true);
        } else{
               $('#chb_mta_perecedera').prop('checked', false);
        }
    },

    getArmaMunicion: function (valor) {
        if(valor == "S"){
                $('#chb_mta_armamunicion').prop('checked', true);
        } else{
               $('#chb_mta_armamunicion').prop('checked', false);
        }
    },

    getRefrigerado: function (valor) {
        if(valor == "S"){
                $('#chb_mta_refrigerada').prop('checked', true);
        } else{
               $('#chb_mta_refrigerada').prop('checked', false);
        }
    },


// Listener de filtros en listas de valores

    listenerFiltersTarimas: function (id_txtbox, id_cmbox) {
        $('#' + id_txtbox).keyup(function () {
            var filter = $(this).val();
            $('select#' + id_cmbox + '>option').each(function () {
                if(/^[a-zA-Z0-9- ]*$/.test(filter) === false) {
                    var text = $(this).text();
                    if (text.indexOf(filter) !== -1) {
                        $(this).show(); $(this).prop('selected', true);
                    }
                    else {
                        $(this).hide();
                    }
                    //  swal({
                    //     title: "Guías",
                    //     text: "El filtro " + filter + " tiene caracteres especiales",
                    //     icon: "error",
                    //     button: "OK!",

                    // });
                }else{
                    var text = $(this).text().toLowerCase();
                    if (text.indexOf(filter) !== -1) {
                        $(this).show(); $(this).prop('selected', true);
                    }
                    else {
                        $(this).hide();
                    }
                }

            });
        });



        /*$('#' + id_txtbox).keyup(function () {
            var filter = $(this).val();
            $('select#' + id_cmbox + '>option').each(function () {
                var text = $(this).text().toLowerCase();
                if (text.indexOf(filter) !== -1) {
                    $(this).show(); $(this).prop('selected', true);
                }
                else {
                    $(this).hide();
                }
            });
        });*/
    },

// Calculo kilos netos
    fsumakilos: function (){
        $("#in_mta_kg_Tarima").change(function () {
            var tar = $('#in_mta_kg_Tarima').val();
            var br = $('#in_mta_kg_bruto').val();
            console.log(br - tar);
            //alert('entro');
            $("#in_mta_kg_neto").val(br - tar);
            $("#KN").addClass('active');
        });
    },

//Lista las tarimas de la guía dada
    Refrescar: function () {
        //tarima.Limpiar();
        tarima.cambiarEstadoBotones(true, false, true, true, true, false, false);
        $("#in_mta_manifiesto").attr('disabled', false);
        $("#in_mta_guia").attr('disabled', false);
        $("#in_mta_guia_original").attr('disabled', false);
        var p_mercancia = $("#in_mta_tarima").val();  //Franciny: lo cambie porque daba error con la variable global tarima,no sé donde se usa
        var p_guia = $("#in_mta_guia").val();
        var manifiesto = $("#in_mta_manifiesto").val();

        if(p_guia == ""){
            p_guia = null;
        }
        //if(p_mercancia == ""){
            p_mercancia = null;
        //}

        $('#th_tarima').show();
        $('#div_filters').show();
        $("#th_tarima").attr('style', 'display:block');
        $("#detalles-tarima").attr('style', 'display:none');
        var templateText = $("#tb-tarima-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#tb_tarimas").html('');
        $.getJSON('/api/TX_MERCANCIAS_TB_Tarimas/GetMercanciasEntrada?entrada=' + p_guia + '&mercancia=' + p_mercancia).then(function (data) {
            $.each(data, function (index, elem) {
                $("#tb_tarimas").append(tableTemplate(elem));
            })
        });
        $("#in_mta_manifiesto").attr('disabled', true);
        $("#in_mta_guia").attr('disabled', true);
        $("#in_mta_guia_original").attr('disabled', true);
    },

    BusquedaFiltros: function () {
        principal.filterTable_Input('in_flr_tarima', 'tb_tarima', 0);
        principal.filterTable_Input('in_flr_desc', 'tb_tarima', 3);
    },

    Regresar: function () {
        $('#div_filters').hide();
        $("#th_tarima").attr('style', 'display:none');
        $("#detalles-tarima").attr('style', 'display:block');
        tarima.Limpiar();
        tarima.cambiarEstadoBotones(false, false, false, true, false, true, false);
    },

//Cargar la tarima seleccionada en el form
    Editar: function (id) {
        principal.hidemenu();
        tarima.Limpiar();
        var p_guia = null;
            $("#detalles-tarima").attr('style', 'display:block');
            $.ajax({
                url: '/api/TX_MERCANCIAS_TB_Tarimas/GetMercanciasEntrada?entrada=' + p_guia + '&mercancia=' + id,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $("#th_tarima").attr('style', 'display:none');
                    $('#div_filters').hide();
                    tarima.MuestraTarimas(data);
                    //$("#detalles").html(template(data));
                    $("#in_mta_guia").attr('disabled', true);
                    $("#in_mta_manifiesto").attr('disabled', true);
                    $("#in_mta_guia_original").attr('disabled', true);
                    $("#s_mta_estado").attr('disabled', false);
                    $("#in_mta_Rack").attr('disabled', false);
                    $("#in_mta_tarima").attr('disabled', true);
                },
                error: function (request, message, error) {
                    console.log(data);
                }
            });
            tarima.cambiarEstadoBotones(false, false, true, false, false, true, false);
    },

// Asigna valores en el form tarima
    MuestraTarimas: function (p_tarima) {
        for (var campo in p_tarima[0]) {
            if(campo == "MER_REFRIGERADO"){
                if(p_tarima[0][campo] == "S"){
                    p_tarima[0][campo] = true;
                } else{
                    p_tarima[0][campo] = false;
                }
            }
            if(campo == "MER_PERECEDERO"){
                if(p_tarima[0][campo] == "S"){
                    p_tarima[0][campo] = true;
                } else{
                    p_tarima[0][campo] = false;
                }
            }
            if(campo == "MER_ARMAS"){
                if(p_tarima[0][campo] == "S"){
                    p_tarima[0][campo] = true;
                } else{
                    p_tarima[0][campo] = false;
                }
            }
            if(campo == "MTA_CONDICION"){
                tarima.getCondiciones(p_tarima[0][campo]);
            }
             if(campo == "MER_TIP_CODIGO_IATA"){
                tarima.getTipoPeligrosidadIATA(p_tarima[0][campo]);
            }
            principal.setValueByName(campo, p_tarima[0][campo],'frm_tarima');
        }

        principal.activeLabels();
    },

//muestra filtros en el grid de tarimas
    FiltrosTarima: function () {
        $('#div_filters').show();
        $('#in_flr_tarima').val("");
        $('#in_flr_desc').val("");
    },

// oculta filtros en el grid de tarimas
    OcultarFiltrosTarima: function () {
        $('#div_filters').hide();
    },

// Limpiar campos de pantalla
    Limpiar: function () {
        tarima.limpiarCamposTarimas();
        tarima.cargarcontrolesTarima();
        tarima.cambiarEstadoBotones(false, false, false, true, false, true, false);
    },

    limpiarCamposTarimas: function () {
        if($('#MTA_GUIA_ID').val() != ""){
             $('#MTA_GUIA_ID').val($('#in_mta_guia').val());
             $('#MTA_MANIFIESTO_ID').val($('#in_mta_manifiesto').val());
             $('#MTA_GUIA_ORIGINAL').val($('#in_mta_guia_original').val());

            principal.resetAllFields('detalles_tarima');
            principal.deactivateLabels();

            $('#lbl_in_mta_lado').addClass('active'); 
            $('#lbl_in_mta_columna').addClass('active'); 
            $('#lbl_in_mta_Rack').addClass('active');  
            $('#lbl_in_mta_manifiesto').addClass('active'); 
            $('#lbl_in_mta_guia').addClass('active'); 
            $('#lbl_in_mta_guia_original').addClass('active');

             $('#in_mta_manifiesto').val($('#MTA_MANIFIESTO_ID').val());
             //$('#MTA_MANIFIESTO_ID').val("");
             $('#in_mta_guia').val($('#MTA_GUIA_ID').val());
             //$('#MTA_GUIA_ID').val("");
             $('#in_mta_guia_original').val($('#MTA_GUIA_ORIGINAL').val());
             //$('#MTA_GUIA_ORIGINAL').val("");
        }else{

            principal.resetAllFields('detalles_tarima');
            principal.deactivateLabels();
            $('#lbl_in_mta_lado').addClass('active'); 
            $('#lbl_in_mta_columna').addClass('active'); 
            $('#lbl_in_mta_Rack').addClass('active');  
            $('#lbl_in_mta_manifiesto').addClass('active'); 
            $('#lbl_in_mta_guia').addClass('active'); 
            $('#lbl_in_mta_guia_original').addClass('active'); 
            
        }

        $("#in_mta_manifiesto").attr('disabled', true);
        $("#in_mta_guia").attr('disabled', true);
        $('#in_mta_guia_original').attr('disabled', true);
        $("#in_mta_Rack").attr('disabled', true);
        $("#in_mta_columna").attr('disabled', true);
        $("#in_mta_lado").attr('disabled', true);
        $("#in_mta_tarima").attr('disabled', false);
        $('#in_mta_bultos_dezp').focus();
    },

//Busqueda de tarima

    BuscarTarimaEnter: function () {
        $('#in_mta_tarima').change(function () {
            tarima.buscarTarima(null,$('#in_mta_tarima').val());
            tarima.cambiarEstadoBotones(false, false, true, false, false, true, false);
        });
    },

    buscarTarima: function (p_guia, p_mercancia) {
        //if (p_guia != null || p_manifiesto != null) {
            var postdata = { mercancia: p_mercancia };
            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_MERCANCIAS_TB_Tarimas/GetMercanciasEntrada?entrada=' + p_guia + '&mercancia=' + p_mercancia,
                data: postdata,
                success: function (data) {
                    //console.log(data);
                    if (data != null) {
                        //trae los datos de la tarima
                        $("#th_tarima").attr('style', 'display:none');
                        tarima.MuestraTarimas(data);
                        $("#in_mta_guia").attr('disabled', true);
                        $("#s_mta_estado").attr('disabled', false);
                        $("#in_mta_Rack").attr('disabled', false);

                    }
                },
                failure: function (data) {
                    console.log("fail");
                    //alert(response.d);
                },
                error: function (data) {
                    swal({
                        title: "Tarimas",
                        text: "Error Buscando la Tarima: " +p_mercancia +"  con Guía: " + p_guia,
                        icon: "error",
                        button: "OK!",

                    });
                    console.log("error");
                }
            });
       // }
    },

    Actualizar: function(){
        var id = $('#in_mta_tarima').val();
        if (id.length > 0) {
            $("#in_mta_tarima").attr('disabled', false);
            $("#in_mta_manifiesto").attr('disabled', false);
            $("#in_mta_guia").attr('disabled', false);
            $("#in_mta_guia_original").attr('disabled', false);
            $("#in_mta_Rack").attr('disabled', false);
            $("#in_mta_columna").attr('disabled', false);
            $("#in_mta_lado").attr('disabled', false);

            $('[name= "MER_REFRIGERADO"]').is(":checked") == true ? $('[name= "MER_REFRIGERADO"]').val('S') : $('[name= "MER_REFRIGERADO"]').val('N');
            $('[name= "MER_PERECEDERA"]').is(":checked") == true ? $('[name= "MER_PERECEDERA"]').val('S') : $('[name= "MER_PERECEDERA"]').val('N');
            $('[name= "MER_ARMAS"]').is(":checked") == true ? $('[name= "MER_ARMAS"]').val('S') : $('[name= "MER_ARMAS"]').val('N');

            $("#UserMERX").val($("#UsReg").val());

            var data = principal.jsonForm($('#frm_tarima').serializeArray());
            $.ajax({
                url: '/api/TX_MERCANCIAS_TB_Tarimas/' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                     if (typeof data === 'string' || data instanceof String){
                        $("#in_mta_tarima").attr('disabled', true);
                        $("#in_mta_manifiesto").attr('disabled', true);
                        $("#in_mta_guia").attr('disabled', true);
                        $("#in_mta_guia_original").attr('disabled', true);
                        swal({
                        title: "Tarimas",
                        text: data,
                        icon: "error",
                        button: "OK!",                       
                        })

                     }else{

                        $("#in_mta_tarima").attr('disabled', true);
                        $("#in_mta_manifiesto").attr('disabled', true);
                        $("#in_mta_guia").attr('disabled', true);
                        $("#in_mta_guia_original").attr('disabled', true);
                        //$("#in_mta_Rack").attr('disabled', true);
                        //$("#s_mta_estado").attr('disabled', true);
                        swal({
                            title: "Tarimas",
                            text: "Tarima actualizada exitosamente",
                            icon: "success",
                            button: "OK!",
                            timer: 5000,
                        })
                     }
                },
                error: function (data) {
                    swal({
                        title: "Tarimas",
                        text: "Error Tarima no se actualizó",
                        icon: "error",
                        button: "OK!",
                        timer: 5000,
                    })
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Tarimas",
                text: "La Tarima no puede ser vacío",
                icon: "error",
                button: "OK!",
                timer: 5000,
            })            
        }
    },

    Guardar: function () {
        $('[name= "MER_REFRIGERADO"]').is(":checked") == true ? $('[name= "MER_REFRIGERADO"]').val('S') : $('[name= "MER_REFRIGERADO"]').val('N');
        $('[name= "MER_PERECEDERA"]').is(":checked") == true ? $('[name= "MER_PERECEDERA"]').val('S') : $('[name= "MER_PERECEDERA"]').val('N');
        $('[name= "MER_ARMAS"]').is(":checked") == true ? $('[name= "MER_ARMAS"]').val('S') : $('[name= "MER_ARMAS"]').val('N');

        $("#UserMERX").val($("#UsReg").val());

        $("#in_mta_tarima").attr('disabled', false);
        $("#in_mta_manifiesto").attr('disabled', false);
        $("#in_mta_guia").attr('disabled', false);
        $("#in_mta_guia_original").attr('disabled', false);
        $("#in_mta_Rack").attr('disabled', false);
        $("#in_mta_columna").attr('disabled', false);
        $("#in_mta_lado").attr('disabled', false);
        $("#s_mta_estado").attr('disabled', false);
        

       var data = principal.jsonForm($('#frm_tarima').serializeArray());
        
        $("#in_mta_tarima").attr('disabled', true);
        $("#in_mta_manifiesto").attr('disabled', true);
        $("#in_mta_guia").attr('disabled', true);
        $("#in_mta_guia_original").attr('disabled', true);
        $("#in_mta_Rack").attr('disabled', true);
        $("#in_mta_columna").attr('disabled', true);
        $("#in_mta_lado").attr('disabled', true);
        $("#s_mta_estado").attr('disabled', true);

        $.ajax({
            url: '/api/TX_MERCANCIAS_TB_Tarimas',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: "application/json",
            success: function (data) {
                 if (typeof data === 'string' || data instanceof String){
                     swal({
                          title: "Tarimas",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                 }else{
                    tarima.cargarcontrolesTarima();
                    swal({
                        title: "Tarimas",
                        text: "Tarima guardada exitosamente",
                        icon: "success",
                        button: "OK!",
                        timer: 5000,
                    }).then((value) => {
                            tarima.limpiarCamposTarimas();
                    });
                 }
                
            },
            error: function (data) {
                swal({
                    title: "Tarimas",
                    text: "Error Tarima no se guardó",
                    icon: "error",
                    button: "OK!",
                    timer: 5000,
                })
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                }
            }
        })
    },

//Navegación 

    pasarGuiaManifiesto: function () {
        var Dato = new Array( $('#in_mta_manifiesto').val() , $('#in_mta_guia').val());

        window.location.href = "/Guias/Index/" + Dato;
    },

    




// Métodos que aún no se utilizan




    buscarTarimaRef: function (p_tarima) {
        if (p_tarima != null) {
            var postdata = { tarima: p_tarima };
            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/MX_TARIMAS_TB/TarimaRef',
                data: postdata,
                success: function (data) {
                    if (data != null) {
                        var man = $("#in_mta_manifiesto").val();
                        //Valido que el estado de la tarima sea virtual y que pertenezca al manifiesto
                        if(data.MTA_MMA_MANIFIESTO != man){
                            swal({
                                title: "Tarima",
                                text: "La tarima referencia: " + p_tarima + " no pertenece al manifiesto: "+ man +"! ",
                                icon: "warning",
                                button: "OK!",

                            });
                        }
                        else
                        {
                            if (data.MTA_ESTADO != 'V' ) {
                                swal({
                                    title: "Tarima",
                                    text: "Estado de la tarima referencia " + data.MTA_TARIMA + " no es virtual! ",
                                    icon: "warning",
                                    button: "OK!",

                                });                            
                            }
                            else
                            { 
                                var bultosRef = $("#in_mta_blts_ref").val();
                                if(bultosRef == ""){
                                    swal({
                                        title: "Tarima",
                                        text: "Debe digitar los Bultos Referencia",                                        icon: "warning",
                                        button: "OK!",
                                    }); 
                                }
                                else
                                {
                                    var disponibles = (data.MTA_BLTS_DESPALETIZADOS - data.MTA_BLTS_FACTURADOS - data.MTA_BLTS_DESPACHADOS);
                                    if(bultosRef > disponibles){
                                        swal({
                                            title: "Tarima",
                                            text: "Los Bultos Referencia no pueden ser mayores a los disponibles para la tarima " + data.MTA_TARIMA,
                                            icon: "warning",
                                            button: "OK!",
                                        }); 
                                    }
                                    else
                                    {
                                        if($("#in_mta_tarima").val() != ""){
                                            tarima.Actualizar();
                                        }else{
                                            tarima.Guardar();
                                        }
                                        
                                    }
                                }
                                
                            }
                        }                        
                    }
                    else 
                    {
                        swal({
                            title: "Tarima",
                            text: "La tarima referencia " + p_tarima + " no existe! ",
                            icon: "warning",
                            button: "OK!",

                        });
                    }
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    swal({
                        title: "Tarima",
                        text: "Error Buscando la tarima " + data.MMA_TARIMA,
                        icon: "error",
                        button: "OK!",

                    });
                    console.log("error");
                }
            });
        }
    },

   

    ImprimeTarima: function(id){
        var url = "http://merxtest/WebEtiquetaMerx/FrmEtiqueta.aspx?etiqueta="+id;
        window.open(url);
    },

    //Carga pantalla de búsqueda de tarimas para reimpresión
    ReimpresionEtiquetas: function(){
        var Dato = new Array($('#in_mta_manifiesto').val(), $('#in_mta_guia').val(), $('#in_mta_guia_original').val());

        window.location.href = "/Tarimas/ReimpresionEtiquetas/" + Dato;   
    }
    
}
$(document).ready(function () {
    tarima.init();
})