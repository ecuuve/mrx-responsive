var conocimientos_consulta = {
    id_manifiesto: null,
    modalidad_transporte: null, //maritimo, aereo, terrestre (se obtiene de la guia pasada por el cookie)
    guia: null,
    nombre_consolidador: null,
    codigo_cliente: null,
    consolidador: null,

    //Acciones iniciales de la pantalla
    init: function () {

        $('form *').prop('disabled', true);

        //Botones de accion de cerrar todas las tarimas abiertas
        $('#btn_cerrar').hide();
        $('#btn_selec').hide();
        $('#btn_deselec').hide();

        $('#frm_conocimientos #TOT_BULTOS').prop('disabled', true);
        $('#frm_conocimientos #TOT_KILOS').prop('disabled', true);
        $('#frm_conocimientos #TOT_VOLUMEN').prop('disabled', true);

         $("#IEN_ANTECESOR").attr("disabled", "true");

         // $('#IEN_KILOS_MANIFESTADOS').maskNumber();
         // $('#IEN_BULTOS_MANIFESTADOS').maskNumber({integer: true});

        principal.resetAllFields("frm_manif_fielset");
        TecladoFuncionalidadPersonalizada();
        conocimientos_consulta.id_manifiesto = Cookies.get('manifiesto');
        conocimientos_consulta.informacionInicial(conocimientos_consulta.id_manifiesto);
         $('#frm_manif label').addClass('active');
        conocimientos_consulta.getChekeoPortones();
        conocimientos_consulta.getDocumentos();
        conocimientos_consulta.getDuas();
        conocimientos_consulta.getViajes();
        conocimientos_consulta.getUbicacionOrigen();
        conocimientos_consulta.getUbicacionDestino();
        conocimientos_consulta.getIndicacionRefrigeracion();
        conocimientos_consulta.getDudaRefrigeracion();
        conocimientos_consulta.getClasificacion();
        conocimientos_consulta.getEstados();
        conocimientos_consulta.getEstadosFiltros();
        conocimientos_consulta.getConsolidadores();
        $('#btn_regresar_conoc').hide();
        $('#btn_tarimas').hide();
        
        //Reglas de navegación según tipo de documento
        $('#frm_conocimientos #IEN_DIN_TIPO_DOCUMENTO').change(function () {
            var div = $('#frm_conocimientos #IEN_ANTECESOR').closest("div.md-form");
            if ($(this).val() === '09') {
                if (div.hasClass('disabled'))
                    div.removeClass('disabled');
                $('#frm_conocimientos #IEN_ANTECESOR').prop('disabled', false);
            } else {
                if (!div.hasClass('disabled'))
                    div.addClass('disabled');
                $('#frm_conocimientos #IEN_ANTECESOR').prop('disabled', true);
            }
        });

        //Si el tipo de transporte es maritimo o terrestre activo el campo volumen
        var div = null;
        // if (Cookies.get('tipo_transporte') != 4 && Cookies.get('tipo_transporte') != undefined) {
        //     div = $('#frm_conocimientos #IEN_VOLUMEN_MANIF').closest("div.md-form");
        //     if (div.hasClass('disabled'))
        //         div.removeClass('disabled');
        //     $('#frm_conocimientos #IEN_VOLUMEN_MANIF').prop('disabled', false);
        //     div = $('#frm_conocimientos #IEN_VOLUMEN_INGRESADO').closest("div.md-form");
        //     if (div.hasClass('disabled'))
        //         div.removeClass('disabled');
        //     $('#frm_conocimientos #IEN_VOLUMEN_INGRESADO').prop('disabled', false);
        // } else {
        //     div = $('#frm_conocimientos #IEN_VOLUMEN_MANIF').closest("div.md-form");
        //     if (!div.hasClass('disabled'))
        //         div.addClass('disabled');
        //     $('#frm_conocimientos #IEN_VOLUMEN_MANIF').prop('disabled', true);
        //     div = $('#frm_conocimientos #IEN_VOLUMEN_INGRESADO').closest("div.md-form");
        //     if (!div.hasClass('disabled'))
        //         div.addClass('disabled');
        //     $('#frm_conocimientos #IEN_VOLUMEN_INGRESADO').prop('disabled', true);
        // }
        
        $("#conoc_flr_ien_fch_digitacion").kendoDatePicker({
            format: "dd/MM/yyyy"
        });

        $('#IEN_KILOS_MANIFESTADOS, #IEN_VOLUMEN_MANIF, #IEN_KILOS_INGRESADOS, #IEN_VOLUMEN_INGRESADO').blur(function () {
            if(this.value != ""){
                this.value = parseFloat(this.value).toFixed(2);
            }
        });


        $('#frm_conocimientos #IEN_PUESTO_CHEQUEO').change(function (){
            if($('#IEN_PUESTO_CHEQUEO').val() == ""){
                swal({
                    title: "Conocimientos",
                    text: 'Debe seleccionar un Chequeo / Portón.',
                    icon: "warning",
                    button: "OK!",
                });
            }else{
                $('#IEN_GUIA_ORIGINAL').focus();
            }           
        });


        $('#btn_limpiar_sum_filtros').hide();
        $('#btn_sumatoria').hide();

        $('#MER_TME_MERCANCIA_FILTROZ').change(function(){
            var mercancia = $('#tarima_flr_ien_entradaZ').val();
            var tipomercancia = $(this).val();
            if (mercancia == ""){
                mercancia = null;
            }
            sumatoria.buscarSumatoria(mercancia,tipomercancia);         
        });

        // $('#IEN_VOLUMEN_MANIF, #IEN_VOLUMEN_INGRESADO').blur(function () {
        //     this.value = parseFloat(this.value).toFixed(2);
        // });

        // $("#IEN_BULTOS_INGRESADOS").keypress(function (e) {
        //     if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
        //         return false;
        //     }
        // });
        // $("#IEN_BULTOS_MANIFESTADOS").keypress(function (e) {
        //     if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
        //         return false;
        //     }
        // });
    },

    //Método para focus de los inputs iniciales
    onfocus: function () {
        if ($("#lbl_guiaBL").hasClass('lblfocus'))
            $("#lbl_guiaBL").removeClass('lblfocus');
    },

    //método para los decimales y miles(aún pendiente)
    parseNumber: function (strg) {
      var strg = strg || "";
      var decimal = ',';
      strg = strg.replace(/[^0-9$,.]/g, '');
      if(strg.indexOf(',') > strg.indexOf('.')) decimal = ',';
      if((strg.match(new RegExp("\\" + decimal,"g")) || []).length > 1) decimal="";
      if (decimal !== "" && (strg.length - strg.indexOf(decimal) - 1 == 3) && strg.indexOf("0" + decimal)!==0) decimal = "";
      strg = strg.replace(new RegExp("[^0-9$" + decimal + "]","g"), "");
      strg = strg.replace(',', '.');
      return parseFloat(strg);
    },  

    ////////////////////// Cerrar Tarimas del conocimiento/////////////////////////////

    // Carga el cuadro de encabezado
    CargaCuadroResumenTarimas: function (entrada, manifiesto) {
        principal.resetAllFields('frm_tarima_info');
        if(entrada != undefined){ 
            let guiaOrig = '', consolidador = '', fecha = '', estado = '';
            let url = '/api/TX_ENTRADAS_TB_Ingreso/Guias?manifiesto=' + manifiesto + '&guiaOrig='+ guiaOrig + '&entrada='+ entrada + '&consolidador='+ consolidador + '&fecha=' + fecha + '&estado=' + estado;
            var cb = function(data){
                $.each(data[0], function(i,v){
                    principal.setValueByName(i, v,'frm_tarima_resumen');
                    $('#frm_tarima_resumen label').addClass('active');
                }); 
            };
            principal.ajax('GET',url,cb,"Tarimas");
        }      
    },

    //Carga información del manifiesto
    informacionInicial: function (id) {
        if (id !== undefined) {
            var tma_manifiesto_p = '';
            var tma_fch_manifiesto_p = '';
            var tma_aer_naerolinea_p = '';
            var cliente = '';
            var num_contenedor = '';
            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_MANIFIESTOS_TB/ManifiestoConsulta?manifiesto_id=' + id + '&tma_manifiesto_p=' + tma_manifiesto_p + '&tma_fch_manifiesto_p=' + tma_fch_manifiesto_p + '&tma_aer_naerolinea_p=' + tma_aer_naerolinea_p + '&cliente=' + cliente + '&num_contenedor=' + num_contenedor,
                success: function (data) {
                    $.each(data[0], function (i, v) {
                        principal.setValueByName(i, v, 'frm_manif');
                        principal.setValueByName(i, v, 'frm_manif_sum');
                        if (i === "TMA_MOD_TRANSPORTE") {
                            conocimientos_consulta.modalidad_transporte = v;
                            conocimientos_consulta.modalidad_transporteControls();
                        }
                        $('#frm_manif label').addClass('active');
                        $('#frm_manif_sum label').addClass('active');
                    });
                    //No permite que se active el label para no perder el color 
                    $.each(data[0], function (i, v) {
                        $('[name="' + id + '"]').closest('label').removeClass('active');
                    });
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    swal({
                        title: "Manifiesto",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",
                    });
                }
            });
        }
    },

    //Reglas de navegación según modalidad de transporte
    modalidad_transporteControls: function () {
        if (conocimientos_consulta.modalidad_transporte === '1' || conocimientos_consulta.modalidad_transporte === '7') { //1 - Maritimo 4 - Aereo 7 - Terrestre
            $("#IEN_GUIA").attr("disabled", "true");
            $("#IEN_GUIA_MASTER").attr("disabled", "true");
            $("#IEN_DUDA_REFRI").attr("disabled", "true");
            $("#IEN_TIPO_DUDA_REF").attr("disabled", "true");
            $("#IEN_BOLETA_CLASE").attr("disabled", "true");            
        } else {
            $("#IEN_GUIA").removeAttr("disabled");
            $("#IEN_GUIA_MASTER").removeAttr("disabled");
            $("#IEN_DUDA_REFRI").removeAttr("disabled");
            $("#IEN_TIPO_DUDA_REF").removeAttr("disabled");
            $("#IEN_BOLETA_CLASE").removeAttr("disabled");
        }
    },

    //Valida la existencia de la prealerta de guía
    valida_existe_prealerta: function (guia) {
        conocimientos_consulta.onfocus();
        if (guia != null && guia != "") {
            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_PREALERTAS_TB/GetPrealerta?guia=' + guia.value,
                success: function (data) {
                    if (data != null && data != 'N') {
                        swal({
                            title: "Conocimientos",
                            text: "Existe un archivo de prealerta para la guía: " + guia.value,
                            icon: "warning",
                            button: "OK!",
                        })
                        .then((willDelete) => {
                          if (willDelete) {
                            //Llama el servicio de impresión 
                            window.open("http://merxalmacenadora/WebEtiquetaNCI/RepPrealerta.aspx?Documento=" + data);
                            console.log("Se abrió el archivo: "+ pdf);
                            //window.open(data, '_blank');
                           // conocimientos.abrirArchivoPrealerta(data);
                          } 
                        });
                    }
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    swal({
                        title: "Prealerta",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",
                    });
                }
            });
            conocimientos_consulta.valida_existe_bloqueo(guia);
        }
    },

    //Valida la existencia de bloqueo para la guía
    valida_existe_bloqueo: function (guia) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_PRE_BLOQUEOS_TB/PrealertaBloqueo?guia=' + guia.value,
            success: function (data) {
                if (data == 'S') {
                    swal({
                        title: "Bloqueo",
                        text: "Carga con retención, tomar medidas cautelares",
                        icon: "warning",
                        button: "OK!",
                    }).then((value) => {
                        $("#IEN_GUIA").focus();
                    });
                }
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Prealerta",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                });
            }
        });
    },

    //Valida que el consecutivo antecesor sea numérico
    valida_antecesorNumerico: function () { 
        var antecesor = $("#IEN_ANTECESOR").val();     
        if(antecesor != "" && antecesor != null){
            if (!$.isNumeric(antecesor)) {
                swal({
                    title: "Conocimientos",
                    text: "El consecutivo antecesor debe ser numérico.",
                    icon: "warning",
                    button: "OK!",
                }).then((value) => {
                    $("#IEN_ANTECESOR").focus();
                });
            }   
        }        
    },

    //Valida existencia de la guía
    valida_guia: function (guia) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_ENTRADAS_TB/GuiaExiste?guia=' + guia.value + '&manifiesto=' + Cookies.get('manifiesto'),
            success: function (data) {
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                if (data.responseJSON) {
                    swal({
                        title: "Existe Guía",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",
                    });
                    $('#frm_conocimientos #IEN_GUIA').val('');
                }
            }
        });
    },

    //Lista los chekeo portones
    getChekeoPortones: function () {
        var cb = function (data) {
            $("#frm_conocimientos #IEN_PUESTO_CHEQUEO").html(principal.arrayToOptions(data)); 
            //principal.KendoComboBox(data, '#IEN_PUESTO_CHEQUEO');
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Portones?tipo_p=" + "C", '', cb);
    },

    //Obtiene los tipos de documento
    getDocumentos: function (e = null) {
        var id = '';
        var callback = function (e) {
            conocimientos_consulta.getDocumentos(e.filter.value);
        };
        var cb = function (data) {
            $("#frm_conocimientos #IEN_DIN_TIPO_DOCUMENTO").kendoComboBox({
                minLength: 1,
                filter: "contais",
                dataTextField: 'Nombre',
                dataValueField: 'Id',
                dataSource: data,
                filtering: function (ev) {
                    var filterValue = ev.filter != undefined ? ev.filter.value : "";
                    ev.preventDefault();
                    this.dataSource.filter({
                        logic: "or",
                        filters: [
                            {
                                field: "Nombre",
                                operator: "contains",
                                value: filterValue
                            },
                            {
                                field: "Id",
                                operator: "contains",
                                value: filterValue
                            }
                        ]
                    });
                },
                suggest: true
            });
             $("#IEN_DIN_TIPO_DOCUMENTO").data('kendoComboBox').select(16); // 19 DUA
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Documentos?tipo_documento=" + id, '', cb);
    },

    //Obtiene las duas
    getDuas: function () {
        if (conocimientos_consulta.id_manifiesto != undefined) {
            var dua = '';
            principal.getData("/api/TX_MANIFIESTO_DUAS_TB/Dua?id_manifiesto=" + conocimientos_consulta.id_manifiesto + '&dua=' + dua, 
                function (data){ 
                    if(data.length == 1){
                        $("#frm_conocimientos #IEN_DUA").html(principal.arrayToOptionsSelected(data,0));
                        $('#IEN_DUA').prop('tabIndex', '');
                    }else{
                        $("#frm_conocimientos #IEN_DUA").html(principal.arrayToOptions(data)); 
                    }
                }
            );
        }
    },

    // Obtiene la lista de viajes
    getViajes: function () {
        if (conocimientos_consulta.id_manifiesto != undefined) {
            var viaje = '';
            principal.getData('/api/TX_MANIFIESTO_VIAJES_TB/Viajes?id_manifiesto=' + conocimientos_consulta.id_manifiesto + '&viaje=' + viaje, 
                function (data) { 
                    if(data.length == 1){
                        $("#frm_conocimientos #IEN_NUMERO_VIAJE").html(principal.arrayToOptionsSelected(data,0));
                        $('#IEN_NUMERO_VIAJE').prop('tabIndex', '');
                    }else{
                        $("#frm_conocimientos #IEN_NUMERO_VIAJE").html(principal.arrayToOptions(data));
                    }
                }
            );
        }
    },

    //Lista los importadores
    importadores: function (id) {
        $("#blo_flt_cod_consig").val("");
        $("#blo_flt_nom_consig").val("");
        var id_p;
        //if(id.value.length > 0){
        if (id != null) {
            if (id.value.length > 0) {
                id_p = id.value
            }
        } else {
            id_p = "";
        }

        var tipo_p = '';
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_IMPORTADORES_TB/Importador?id=' + id_p + '&tipo=' + tipo_p,
            success: function (data) {
                if (data != null) {
                    if (data.length == 1) {
                        $('#frm_conocimientos #IEN_IMPORTADOR').val(data[0].Nombre);
                    }
                    if (data.length > 1) {
                        conocimientos_consulta.Modal_ListarConsignatarios(data);

                    }
                    if (data.length == 0) {
                        $('#frm_conocimientos #IEN_IMP_CEDULA').val('');
                        $('#frm_conocimientos #IEN_IMPORTADOR').val('');

                        if ($("#myModalConsigna").css('display') == 'none') {
                            //$('#frm_conocimientos #IEN_IMP_CEDULA').focus();
                        }
                        //
                    }
                }
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Importador",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                });
                $('#IEN_IMP_CEDULA').val('');
            }
        });
    },

    // Lista los consignatarios en el modal  
    Modal_ListarConsignatarios: function (data) {
        var templateText = $("#BLO_Consigna_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#BLO_Consigna_tablebody").html('');

        $.each(data, function (index, elem) {
            $("#BLO_Consigna_tablebody").append(tableTemplate(elem));
        })

        $("#myModalConsigna").modal('show');
    },

    //Carga datos del consignatario seleccionado en el modal
    Modal_SelectConsigna: function (id, nombre) {
        $("#IEN_IMP_CEDULA").val($.trim(id));
        $('#lbl_IEN_IMP_CEDULA').addClass('active')
        $("#IEN_IMPORTADOR").val($.trim(nombre));
        $("#IEN_IMPORTADOR").focus();
        $("#myModalConsigna").modal('hide');
    },

    //Limpia campos de filtro del modal
    Modal_limpiarFiltrosConsigna: function () {
        $("#blo_flt_cod_consig").val("");
        $("#blo_flt_nom_consig").val("");
        conocimientos_consulta.importadores(null);
    },

    //Busca consignatario para carga el modal segun el filtro
    Modal_BuscarConsigna: function () {
        var p_codconsigna = "";
        var p_nomconsigna = "";
        //var p_popup = 0;
        $('.modal-body #blo_flt_cod_consig,.modal-body textarea').each(function () {
            p_codconsigna = $(this).val();
        });
        $(".modal-body #blo_flt_nom_consig,.modal-body textarea").each(function () {
            p_nomconsigna = $(this).val();
        });

        //if (p_codimportador !== "" || p_nomimportador !== "") {
        //    p_popup = 1;
        //}

        //Obtenemos la plantilla
        var templateText = $("#BLO_Consigna_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $(".modal-body #BLO_Consigna_tablebody,.modal-body textarea").each(function () {
            $(this).html('');
        });
        //$("#Prev_Importador_table1body").html('');
        if (p_codconsigna !== "" || p_nomconsigna !== "") {
            //var p_tipo = "F";
            $.getJSON('/api/TX_IMPORTADORES_TB/Importador?id=' + p_codconsigna + '&tipo=' + p_nomconsigna).then(function (data) {
                $.each(data, function (index, elem) {
                    $("#BLO_Consigna_tablebody").append(tableTemplate(elem));
                })
            });
        }
        else {
            conocimientos_consulta.importadores(null);
        }
    },

    //Levanta el modal que lista los consolidadores
    buscarCons: function (id) {
        var nombre = '';
        //conocimientos.limpiarFiltrosModal();
        $('#conoc_flt_Id').val('');
        $('#conoc_flt_Nombre').val('');
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_CONSOLIDADORES_TB/Consolidadores?consolidador=' + id.value + "&nombre=" + nombre,
            success: function (data) {
                if (data != null) {
                    $('#Conoc_ConsolidadorModal').modal('show');
                    var templateText = $("#Conoc_Consolidadore_table-template").html();
                    //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                    var tableTemplate = Handlebars.compile(templateText);
                    $("#Conoc_Consolidador_tablebody").html('');
                    $.each(data, function (index, elem) {
                        $("#Conoc_Consolidador_tablebody").append(tableTemplate(elem));
                    });
                }
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Consolidadores",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                });
            }
        });
    },

    //Pinta el nombre del consolidador en el campo correspondiente del formulario
    pintarConsolidadores: function (id, nombre) {
        $('#Conoc_ConsolidadorModal').modal('hide');
        //Validaciones de consolidador y guia master
        if (id === 999 && $('#IEN_GUIA_MASTER').val() !== '') {
            swal({
                title: "Consolidadores",
                text: 'Si digita guía máster el consolidador NO puede ser 999',
                icon: "error",
                button: "OK!",

            }).then((value) => {
                $("#IEN_CON_NCONSOLIDADOR").focus();
            });

            $('#frm_conocimientos #IEN_CON_NCONSOLIDADOR').val('');
            $('#frm_conocimientos #CONSOLIDADOR').val('');
            $('#frm_conocimientos #IEN_CON_NCONSOLIDADOR').attr("data-id", '');
        } else if ($('#IEN_GUIA_MASTER').val() == '' && id != 999) {
            swal({
                title: "Consolidadores",
                text: 'Si la carga es consolidada debe indicar guía master.',
                icon: "error",
                button: "OK!",

            }).then((value) => {
                $("#IEN_GUIA_MASTER").focus();
            });
            $('#frm_conocimientos #IEN_CON_NCONSOLIDADOR').val('');
            $('#frm_conocimientos #CONSOLIDADOR').val('');
            $('#frm_conocimientos #IEN_CON_NCONSOLIDADOR').attr("data-id", '');
        } else {
            $('#frm_conocimientos #IEN_CON_NCONSOLIDADOR').val($.trim(id));
            $('#frm_conocimientos #CONSOLIDADOR').val($.trim(nombre));
            $('#frm_conocimientos #IEN_CON_NCONSOLIDADOR').attr("data-id", $.trim(id));
            $("#IEN_BULTOS_MANIFESTADOS").focus();
        }
    },

    //Limpia filtros de modal consolidador
    limpiarFiltrosModal: function () {
        $('#conoc_flt_Id').val('');
        $('#conoc_flt_Nombre').val('');
        conocimientos_consulta.BuscarConsolidadorFiltro();
    },

    //Busca consolidador según el filtro
    BuscarConsolidadorFiltro: function () {
        var id = $('#Conoc_ConsolidadorModal #conoc_flt_Id').val();
        var nombre = $('#Conoc_ConsolidadorModal #conoc_flt_Nombre').val();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_CONSOLIDADORES_TB/Consolidadores?consolidador=' + id + "&nombre=" + nombre,
            success: function (data) {
                if (data != null) {
                    var templateText = $("#Conoc_Consolidadore_table-template").html();
                    //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                    var tableTemplate = Handlebars.compile(templateText);
                    $("#Conoc_Consolidador_tablebody").html('');
                    $.each(data, function (index, elem) {
                        $("#Conoc_Consolidador_tablebody").append(tableTemplate(elem));
                    });
                }
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Consolidadores",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
            }
        });
    },

    //Obtiene las ubicaciones
    getUbicacionOrigen: function (e = null) {
        var id = '';
        var callback = function (e) {
            conocimientos_consulta.getUbicacionOrigen(e.filter.value);
        };
        var cb = function (data) {
            $("#frm_conocimientos #IEN_UBICACION_INGRESO").kendoComboBox({
                minLength: 1,
                filter: "contais",
                dataTextField: 'Nombre',
                dataValueField: 'Id',
                dataSource: data,
                filtering: function (ev) {
                    var filterValue = ev.filter != undefined ? ev.filter.value : "";
                    ev.preventDefault();
                    this.dataSource.filter({
                        logic: "or",
                        filters: [
                            {
                                field: "Nombre",
                                operator: "contains",
                                value: filterValue
                            },
                            {
                                field: "Id",
                                operator: "contains",
                                value: filterValue
                            }
                        ]
                    });
                },
                suggest: true
            });
        };
        principal.get("/api/TX_UBICACIONES_TB/Ubicaciones?nombre=" + id, '', cb);
    },

    //Obtiene las ubicaciones
    getUbicacionDestino: function (e = null) {
        var id = '';
        var callback = function (e) {
            conocimientos_consulta.getUbicacionDestino(e.filter.value);
        };
        var cb = function (data) {
            $("#frm_conocimientos #IEN_UBICACION_TRANSITO").kendoComboBox({
                minLength: 1,
                filter: "contais",
                dataTextField: 'Nombre',
                dataValueField: 'Id',
                dataSource: data,
                filtering: function (ev) {
                    var filterValue = ev.filter != undefined ? ev.filter.value : "";
                    ev.preventDefault();
                    this.dataSource.filter({
                        logic: "or",
                        filters: [
                            {
                                field: "Nombre",
                                operator: "contains",
                                value: filterValue
                            },
                            {
                                field: "Id",
                                operator: "contains",
                                value: filterValue
                            }
                        ]
                    });
                },
                suggest: true
            });
            $("#IEN_UBICACION_TRANSITO").data('kendoComboBox').select(26);

        };
        principal.get("/api/TX_UBICACIONES_TB/Ubicaciones?nombre=" + id, '', cb);
    },

    //Obtiene indicadores de refrigeración
    getIndicacionRefrigeracion: function (e = null) {
        var callback = function (e) {
            conocimientos_consulta.getIndicacionRefrigeracion(e.filter.value);
        };
        var cb = function (data) {
            $("#frm_conocimientos #IEN_DUDA_REFRI").kendoComboBox({
                minLength: 1,
                filter: "contais",
                dataTextField: 'Nombre',
                dataValueField: 'Id',
                dataSource: data,
                filtering: function (ev) {
                    var filterValue = ev.filter != undefined ? ev.filter.value : "";
                    ev.preventDefault();
                    this.dataSource.filter({
                        logic: "or",
                        filters: [
                            {
                                field: "Nombre",
                                operator: "contains",
                                value: filterValue
                            },
                            {
                                field: "Id",
                                operator: "contains",
                                value: filterValue
                            }
                        ]
                    });
                },
                suggest: true
            });
        };
        principal.get("/api/TX_MANIFIESTOS_TB/IndicacionRefrigeracion", '', cb);
    },

    //Obtiene dudas de refrigeración
    getDudaRefrigeracion: function (e = null) {
        var tipo = '';
        var callback = function (e) {
            conocimientos_consulta.getDudaRefrigeracion(e.filter.value);
        };
        var cb = function (data) {
            $("#frm_conocimientos #IEN_TIPO_DUDA_REF").kendoComboBox({
                minLength: 1,
                filter: "contais",
                dataTextField: 'Nombre',
                dataValueField: 'Id',
                dataSource: data,
                filtering: function (ev) {
                    var filterValue = ev.filter != undefined ? ev.filter.value : "";
                    ev.preventDefault();
                    this.dataSource.filter({
                        logic: "or",
                        filters: [
                            {
                                field: "Nombre",
                                operator: "contains",
                                value: filterValue
                            },
                            {
                                field: "Id",
                                operator: "contains",
                                value: filterValue
                            }
                        ]
                    });
                },
                suggest: true
            });
        };
        principal.get("/api/TX_DUDAS_REFR_TB/DudaRefrigeracion?tipo=" + tipo, '', cb);
    },

    //Obtiene clasificaciones
    getClasificacion: function (e = null) {
        var tipo = '';
        var callback = function (e) {
            conocimientos_consulta.getClasificacion(e.filter.value);
        };
        var cb = function (data) {
            $("#frm_conocimientos #IEN_CLASE").kendoComboBox({
                minLength: 1,
                filter: "contais",
                dataTextField: 'Nombre',
                dataValueField: 'Id',
                dataSource: data,
                filtering: function (ev) {
                    var filterValue = ev.filter != undefined ? ev.filter.value : "";
                    ev.preventDefault();
                    this.dataSource.filter({
                        logic: "or",
                        filters: [
                            {
                                field: "Nombre",
                                operator: "contains",
                                value: filterValue
                            },
                            {
                                field: "Id",
                                operator: "contains",
                                value: filterValue
                            }
                        ]
                    });
                },
                suggest: true
            });
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Clasificacion", '', cb);
    },

    //Validación de boleta
    validacionBoleta: function (boleta) {
        if ($("#frm_conocimientos #IEN_CLASE").data("kendoComboBox").value() == 'TRANSITO' && boleta.value.length == 0) {
            swal({
                title: "Boleta",
                text: 'SI LA CLASE ES TRÁNSITO DEBE INDICAR NÚMERO DE BOLETA',
                icon: "error",
                button: "OK!",
            });
        }
    },

    //Obtiene los estados
    getEstados: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/EstadosGuia", function (data) { $("#frm_conocimientos #IEN_ESTADO").html(principal.arrayToOptionsSelected(data, 'A')); });
    },

    //Obtiene los estados segín filtro
    getEstadosFiltros: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/EstadosGuia", function (data) { $("#div_conocimientos_filtros #conoc_flr_ien_estado").html(principal.arrayToOptions(data)); });
    },

    //Guardar guía
    guardar: function () {
        $('#IEN_USU_DIGITO').val($('#UsReg').val());
        conocimientos_consulta.valida_existe_bloqueo($('#IEN_GUIA_ORIGINAL').val());
        conocimientos_consulta.valida_guia($('#IEN_GUIA_ORIGINAL').val());
        $('#IEN_TMA_ID_MANIFIESTO').val(conocimientos_consulta.id_manifiesto);
        //conocimientos.nombre_consolidador = $('#IEN_CON_NCONSOLIDADOR').val();
        //$('#IEN_CON_NCONSOLIDADOR').val($('#IEN_CON_NCONSOLIDADOR').data('id'));
        Cookies.set('descripcion', $('#IEN_DESCRIPCION_MANIF').val());
        $.post(
            "/api/TX_ENTRADAS_TB_Ingreso/Guardar/",
            $('#frm_conocimientos').serialize(),
            function (data) {
                if (data != null) {
                    Cookies.set('entrada', data);
                    conocimientos_consulta.limpiar();
                    swal({
                        title: "Conocimientos",
                        text: "Conocimiento guardado exitosamente",
                        icon: "success",
                        button: "OK!",
                    });
                }
            })
            .fail(function (data) {
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                } else {
                    swal({
                        title: "Conocimientos",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",
                    });
                }
                //$('#IEN_CON_NCONSOLIDADOR').val(conocimientos.nombre_consolidador);
            });
    },

    //Limpair campos de conocimientos
    limpiar: function () {
        $("#IEN_ESTADO").attr('disabled', true);
        principal.resetAllFields('frm_conocimientos');
        $('#conseTitulo').text("");
        $("#IEN_DIN_TIPO_DOCUMENTO").data("kendoComboBox").value("");
        $("#IEN_UBICACION_INGRESO").data("kendoComboBox").value("");
        $("#IEN_UBICACION_TRANSITO").data("kendoComboBox").value("");
        $("#IEN_UBICACION_TRANSITO").data('kendoComboBox').select(26); //A164

        $("#IEN_DIN_TIPO_DOCUMENTO").data("kendoComboBox").value("");
        $("#IEN_DIN_TIPO_DOCUMENTO").data('kendoComboBox').select(16); //19 DUA

        $("#IEN_DUDA_REFRI").data("kendoComboBox").value("");
        $("#IEN_TIPO_DUDA_REF").data("kendoComboBox").value("");
        $("#IEN_CLASE").data("kendoComboBox").value("");
        $("#IEN_CON_NCONSOLIDADOR").data("kendoComboBox").value("");
        conocimientos_consulta.informacionInicial(conocimientos_consulta.id_manifiesto);
        conocimientos_consulta.limpiaFiltros();
        $('#btn_tarimas').hide();
        
        if ($('#foo').is(':visible')) {
            $('#btn_guardar_conoc').show();
            $('#btn_editar_conoc').hide();
        }

         $('#btn_guardar_conoc').show();
         $('#btn_editar_conoc').hide();
          $('#btn_limpiar_sum_filtros').hide();
          $("#IEN_ANTECESOR").attr("disabled", "true");
         principal.deactiveLabelsInput();
         // $("#IEN_PUESTO_CHEQUEO").focus();
         // if (!$('#lbl_IEN_PUESTO_CHEQUEO').hasClass('lblfocus'))
         //    $('#lbl_IEN_PUESTO_CHEQUEO').addClass('lblfocus')
    },

    //Limpiar campos de conocimientos luego de borrarlo por si se ha consultado
    limpiarPostBorrado: function () {
        $("#IEN_ESTADO").attr('disabled', true);
        principal.resetAllFields('frm_conocimientos');
        $('#conseTitulo').text("");
        $("#IEN_DIN_TIPO_DOCUMENTO").data("kendoComboBox").value("");
        $("#IEN_UBICACION_INGRESO").data("kendoComboBox").value("");
        $("#IEN_UBICACION_TRANSITO").data("kendoComboBox").value("");
        $("#IEN_UBICACION_TRANSITO").data('kendoComboBox').select(26); //A164

        $("#IEN_DIN_TIPO_DOCUMENTO").data("kendoComboBox").value("");
        $("#IEN_DIN_TIPO_DOCUMENTO").data('kendoComboBox').select(16); //19 DUA

        $("#IEN_DUDA_REFRI").data("kendoComboBox").value("");
        $("#IEN_TIPO_DUDA_REF").data("kendoComboBox").value("");
        $("#IEN_CLASE").data("kendoComboBox").value("");
        $("#IEN_CON_NCONSOLIDADOR").data("kendoComboBox").value("");
        conocimientos_consulta.informacionInicial(conocimientos_consulta.id_manifiesto);
        conocimientos_consulta.limpiaFiltros();
       
        $("#IEN_ANTECESOR").attr("disabled", "true");
        principal.deactiveLabelsInput();
    },

    //Muestra la lista de guía/conocimiento
    Refrescar: function () {
        if ($('#tbl_conocimientos').hasClass('fullHidden'))
            $('#tbl_conocimientos').removeClass('fullHidden')
        if ($('#div_conocimientos_filtros').hasClass('fullHidden'))
            $('#div_conocimientos_filtros').removeClass('fullHidden')
        if (!$('#frm_conocimientos').hasClass('fullHidden'))
            $('#frm_conocimientos').addClass('fullHidden')
        $('#btn_listar_conoc').hide();
        $('#btn_guardar_conoc').hide();
        $('#btn_editar_conoc').hide();
        $('#btn_regresar_conoc').show();
        $('#btn_regresar_manifiesto').hide();
        $('#btn_tarimas').hide();
        $('#conoc_flr_ien_entrada').focus();
        conocimientos_consulta.buscar();
    },

    //Lista guía/conocimiento
    buscar: function () {
        var manifiesto = conocimientos_consulta.id_manifiesto;
        if (manifiesto !== undefined) {
            var guiaOrig = $('#conoc_flr_ien_guia_original').val();
            var entrada = $('#conoc_flr_ien_entrada').val();
            var consolidador = $('#conoc_flr_ien_con_nconsolidador').val();
            var fecha = $('#conoc_flr_ien_fch_digitacion').val();
            var estado = '';
            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_ENTRADAS_TB_Ingreso/Guias_Consulta?manifiesto=' + manifiesto + '&guiaOrig=' + guiaOrig + '&entrada=' + entrada + '&consolidador=' + consolidador + '&fecha=' + fecha + '&estado=' + estado,
                success: function (data) {
                    //Obtenemos la plantilla
                    var templateText = $("#conocimientos_table-template").html();
                    //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                    var tableTemplate = Handlebars.compile(templateText);
                    $("#conocimientos_tablebody").html('');
                    //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                    $.each(data, function (index, elem) {
                        $("#conocimientos_tablebody").append(tableTemplate(elem));
                    });
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    swal({
                        title: "Manifiesto",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                    console.log("error");
                }
            });
        }
    },

    //Filtra cuándo escriben en el input
    filtrar: function (input) {
        if (input.value.length > 2) {
            conocimientos_consulta.buscar();
        }
    },

    //Limpia filtros de lista de guías
    limpiaFiltros: function () {
        $('#conoc_flr_ien_guia_original').val('');
        $('#conoc_flr_ien_entrada').val('');
        $('#conoc_flr_ien_con_nconsolidador').val('');
        $('#conoc_flr_ien_fch_digitacion').val('');
        conocimientos_consulta.buscar();
    },

    //Limpia los filtros en la pantalla de listar tarimas
    limpiaFiltrosSum: function(){
        $('#tarima_flr_ien_entradaZ').val('');
        $("#MER_TME_MERCANCIA_FILTROZ").data("kendoComboBox").value("");
        sumatoria.buscarSumatoria(null,null);
    },

    //Regresar de la lista al form de ingreso 
    regresar: function () {
        if (!$('#tbl_conocimientos').hasClass('fullHidden'))
            $('#tbl_conocimientos').addClass('fullHidden')

        if (!$('#div_conocimientos_filtros').hasClass('fullHidden'))
            $('#div_conocimientos_filtros').addClass('fullHidden')

        if ($('#frm_conocimientos').hasClass('fullHidden'))
            $('#frm_conocimientos').removeClass('fullHidden')


         if (!$('#tbl_tarimas_cons').hasClass('fullHidden'))
            $('#tbl_tarimas_cons').addClass('fullHidden')
        
        if (!$('#div_tarimas_filtrosZ').hasClass('fullHidden'))
            $('#div_tarimas_filtrosZ').addClass('fullHidden')




        $('#btn_listar_conoc').show();
        $('#btn_limpiar_conoc').show();
        $('#btn_limpiar_sum_filtros').hide();

         if($("#conseTitulo").text() != ""){
            $('#btn_guardar_conoc').hide();
            $('#btn_editar_conoc').show();
            $('#btn_tarimas').show();
            $('#btn_sumatoria').show();

        }else{
            $('#btn_guardar_conoc').show();
            $('#btn_editar_conoc').hide();
            $('#btn_sumatoria').hide();
        }

         //Botones de accion de cerrar todas las tarimas abiertas
        $('#btn_cerrar').hide();
        $('#btn_selec').hide();
        $('#btn_deselec').hide();
        
        $('#btn_regresar_conoc').hide();
        $('#btn_regresar_manifiesto').show();
        $('#IEN_PUESTO_CHEQUEO').focus();
    },

    //Carga la guía seleccionada en pantalla
    editar: function (id) {
        Cookies.set('entrada', id);
        conocimientos_consulta.buscarConocimientos(id);
        if (!$('#tbl_conocimientos').hasClass('fullHidden'))
            $('#tbl_conocimientos').addClass('fullHidden')
        if (!$('#div_conocimientos_filtros').hasClass('fullHidden'))
            $('#div_conocimientos_filtros').addClass('fullHidden')
        if ($('#frm_conocimientos').hasClass('fullHidden'))
            $('#frm_conocimientos').removeClass('fullHidden')
        $('#btn_listar_conoc').show();
        $('#btn_guardar_conoc').hide();
        $('#btn_editar_conoc').show();
        $('#btn_regresar_conoc').hide();
        $('#btn_tarimas').show();
        $('#btn_regresar_manifiesto').show();
        if ($('#btn_editar_conoc').hasClass('fullHidden'))
            $('#btn_editar_conoc').removeClass('fullHidden')
        principal.resetAllFields('div_conocimientos_filtros');
        //$("#IEN_ESTADO").attr('disabled', false);
    },

    //Buscar la guía/conocimiento
    buscarConocimientos: function (p_identificador) {
        principal.resetAllFields('frm_conocimientos');
        $("#IEN_UBICACION_TRANSITO").data("kendoComboBox").value("");
        $("#IEN_DIN_TIPO_DOCUMENTO").data("kendoComboBox").value("");
        $("#IEN_CON_NCONSOLIDADOR").data("kendoComboBox").value("");
        var manifiesto = conocimientos_consulta.id_manifiesto;
        if (manifiesto != undefined) {
            var guiaOrig = '';
            var consolidador = '';
            var fecha = '';
            var estado = '';
            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_ENTRADAS_TB_Ingreso/Guias_Consulta?manifiesto=' + manifiesto + '&guiaOrig=' + guiaOrig + '&entrada=' + p_identificador + '&consolidador=' + consolidador + '&fecha=' + fecha + '&estado=' + estado,
                success: function (data) {
                    conocimientos_consulta.informacionInicial(conocimientos_consulta.id_manifiesto);
                    $.each(data[0], function (i, v) {
                        principal.setValueByName(i, v, 'frm_conocimientos');
                        principal.setValueByName(i, v, 'frm_manif_sum'); // Carga el DUA del seleccionado en la sumatoria
                        principal.setValueByName(i, v, 'frm_manif'); // Carga el DUA del seleccionado en el resumen
                    });



                    Cookies.set('descripcion', $('#IEN_DESCRIPCION_MANIF').val());
                    $('#conseTitulo').text($('#IEN_ENTRADA').val());
                    principal.activeLabelsInput();
                    $('#btn_sumatoria').show();
                     //$('form *').prop('disabled', true);
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    swal({
                        title: "Conocimientos",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            });
        }
    },

    //Actualizar la guía/conocimiento
    Actualizar: function () {
        $('#IEN_USU_DIGITO').val($('#UsReg').val());
        $('#IEN_ENTRADA').prop('disabled', false);
        $('#IEN_GUIA').prop('disabled', false);

        // $('#IEN_BULTOS_MANIFESTADOS').val(conocimientos.parseNumber($('#IEN_BULTOS_MANIFESTADOS').val()));
        // $('#IEN_KILOS_MANIFESTADOS').val(conocimientos.parseNumber($('#IEN_KILOS_MANIFESTADOS').val()));

        var id = $('#IEN_ENTRADA').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_conocimientos').serializeArray());
            $('#IEN_ENTRADA').prop('disabled', true);
            $('#IEN_GUIA').prop('disabled', true);
            $.ajax({
                url: '/api/TX_ENTRADAS_TB_Ingreso/Actualizar?id=' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (data != null) {
                        swal({
                            title: "Conocimientos",
                            text: "Se actualizó correctamente la Guía",
                            icon: "success",
                            button: "OK!",
                        });
                        conocimientos_consulta.limpiar();
                        conocimientos_consulta.regresar();
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                    else {
                        swal({
                            title: "Contenedor/Manifiesto",
                            text: data.responseJSON.Message,
                            icon: "error",
                            button: "OK!",
                        });
                    }
                }
            });
        }
        else {
            swal({
                title: "Conocimientos",
                text: "El id de manifiesto no puede ser vacío",
                icon: "error",
                button: "OK!",

            });
        }
    },

    //Borra la guia abierta 
    Guia_Borrar: function(id){       
        swal({
          title: "Está seguro?",
          text: "Eliminará la información de la guía: "+ id,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            conocimientos_consulta.Eliminar(id);                     
          } 
        });       
    },

    //Ejecuta la eliminación confirmada
    Eliminar: function(id){
        var user_Merx = $('#UsReg').val();
        if (id.length > 0) {         
            $.ajax({
                url: '/api/TX_ENTRADAS_TB_Ingreso/Borrar?id=' + id + '&user=' + user_Merx,
                type: 'DELETE',
                contentType: "application/json",
                success: function (data) {                   
                    swal({
                        title: "Conocimientos",
                        text: "Se eliminó correctamente la información",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {

                            conocimientos_consulta.Refrescar();
                            conocimientos_consulta.limpiarPostBorrado();
                    });                    
                },
                error: function (data) {
                    swal({
                          title: "Eliminar Conocimientos",
                          text: data.responseJSON.Message,
                          icon: "error",
                          button: "OK!",
                        })
                }
            })
        }
        else {
            swal({
                title: "Conocimientos",
                text: "El ID de la guía no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    },

    //Lista los consolidadores
    getConsolidadores: function (e = null) {
        var cliente = e != null ? e : '';
        var tipo_cliente = "C"
        $("#IEN_CON_NCONSOLIDADOR").kendoComboBox({
            placeholder: "",
            dataTextField: "Nombre",
            dataValueField: "Id",
            filter: "contains",
            autoBind: true,
            minLength: 1,
            dataSource: {
                serverFiltering: true,
                transport: {
                    read: function (options) {
                        if (options.data.filter && options.data.filter.filters.length > 0 ) {
                            cliente = options.data.filter.filters[0].value;
                        }
                        $.ajax({
                            type: "GET",
                            url: "/api/TX_ENTRADAS_TB_Ingreso/Consolidadores?cliente=" + cliente + "&tipo_cliente=" + tipo_cliente,
                            success: function (data) {
                                options.success(data);
                            }
                        });
                    }
                }
            }
        });
        // if (id != null) {
        //     $("#IEN_CON_NCONSOLIDADOR").data("kendoComboBox").dataSource.read();
        //     $("#IEN_CON_NCONSOLIDADOR").data("kendoComboBox").value(id);
        // }        
    },


    ListarSumatoria: function(){
        if ($('#tbl_tarimas_cons').hasClass('fullHidden'))
            $('#tbl_tarimas_cons').removeClass('fullHidden')
        
        if ($('#div_tarimas_filtrosZ').hasClass('fullHidden'))
            $('#div_tarimas_filtrosZ').removeClass('fullHidden')

        if (!$('#tbl_conocimientos').hasClass('fullHidden'))
            $('#tbl_conocimientos').addClass('fullHidden')

        if (!$('#div_conocimientos_filtros').hasClass('fullHidden'))
            $('#div_conocimientos_filtros').addClass('fullHidden')

        if (!$('#frm_conocimientos').hasClass('fullHidden'))
            $('#frm_conocimientos').addClass('fullHidden')

        $('#btn_listar_conoc').hide();
        $('#btn_guardar_conoc').hide();
        $('#btn_editar_conoc').hide();
        $('#btn_regresar_conoc').show();
        $('#btn_regresar_manifiesto').hide();
        $('#btn_tarimas').hide();
        $('#btn_sumatoria').hide();
        $('#btn_limpiar_conoc').hide();
        $('#btn_limpiar_sum_filtros').show();
     
        conocimientos_consulta.getTiposMercancia();
        sumatoria.buscarSumatoria(null,null);
        
        //Botones de accion de cerrar todas las tarimas abiertas
        $('#btn_cerrar').show();
        $('#btn_selec').show();
        $('#btn_deselec').hide();
        //$('#conoc_flr_ien_entrada').focus();
    },

    //Lista los tipos de mercancía
    getTiposMercancia: function(){
        var cb = function(data){
          principal.KendoComboBox(data, '#MER_TME_MERCANCIA_FILTROZ');
        };
        principal.get("/api/TX_MERCANCIAS_TB/tiposMercancias?tipo=" + '','',cb);
    },

    //Redirecciona a la pantalla Tarimas
    redireccionar: function () {
        window.location.href = "Tarimas_Consulta";
    },

    //Redirecciona a la pantalla de Manifiesto/Contenedor
    redirectContenedor: function () {
        window.location.href = "ManifiestoContenedor_Consulta";
    }
};
$(document).ready(function () {
    conocimientos_consulta.init();
});