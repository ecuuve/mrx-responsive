﻿var localiza = {

    init: function () {
        TecladoFuncionalidadPersonalizada();
         // $('#li_mercancias').on('click', function () { 
         //        //funcionalidad al tocar el tab mercancias          
               
         //    });
        
         $("#detalles-localiza").attr('style', 'display:none');
         $("#btn_regresar").attr('style', 'display:none');
         $("#btn_actualizar").attr('style', 'display:none');

    },

    onfocus: function () {
        if ($("#lbl_flr_mer_ien_entrada").hasClass('lblfocus'))
            $("#lbl_flr_mer_ien_entrada").removeClass('lblfocus');
    },

    MERLOC_Regresar: function(){
        $("#detalles-localiza").attr('style', 'display:none');
        $("#btn_regresar").attr('style', 'display:none');
        $("#btn_actualizar").attr('style', 'display:none');
        $("#btn_filtrar_mercaLocaliza").attr('style', 'display:block');
        $("#btn_limpiar_filtros").attr('style', 'display:block');
        $("#th_mercLocalizaEspecial").attr('style', 'display:block');
        $("#div_filters").attr('style', 'display:block'); 
        localiza.MERLOC_ListarLocaliza();      
    },

    MERLOC_ListarLocaliza: function(){
        //Buscar lista de mercancías según filtros
         var consecutivo = $("#in_flr_mer_ien_entrada").val();
         var mercancia = $("#in_flr_mer_id_mercancia").val();

         if (consecutivo == "" && mercancia == ""){
            swal({
                    title: "Localización",
                    text: "Debe indicar al menos el valor de Consecutivo ó Mercancia para consultar datos.",
                    icon: "error",
                    button: "OK!",
                    timer: 5000,
                })
         }else{
            //Obtenemos la plantilla
        $('#th_mercLocalizaEspecial').show();
        $("#th_mercLocalizaEspecial").attr('style', 'display:block');
        var templateText = $("#Loc_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#MercLocalizaEspecial_table1body").html('');
        $.getJSON('/api/TX_MERCANCIAS_TB_Localiza/GetMercanciasLocalizacion?entrada=' + consecutivo + '&mercancia='+mercancia).then(function (data) {
            $.each(data, function (index, elem) {
                $("#MercLocalizaEspecial_table1body").append(tableTemplate(elem));
            })
        });
         }
        
    },

    MERLOC_Actualizar: function(){
        $("#MER_ID_MERCANCIA").attr('disabled', false);
        $("#MER_IEN_ENTRADA").attr('disabled', false);
        $("#MER_BULTOS_DESPALETIZADOS").attr('disabled', false);
        $("#PEL_Descripcion").attr('disabled', false);
        $('[name= "MER_Refrigerado"]').is(":checked") == true ? $('[name= "MER_Refrigerado"]').val('S') : $('[name= "MER_Refrigerado"]').val('N');
        var id = $('#MER_ID_MERCANCIA').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_localiza').serializeArray());
            $("#MER_ID_MERCANCIA").attr('disabled', true);
            $("#MER_IEN_ENTRADA").attr('disabled', true);
            $("#MER_BULTOS_DESPALETIZADOS").attr('disabled', true);
            $("#PEL_Descripcion").attr('disabled', true);
            $.ajax({
                url: '/api/TX_MERCANCIAS_TB_Localiza/' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Actualización de localización",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{
                        swal({
                        title: "Localización",
                        text: "Se actualizó correctamente la información",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            if(cambioR == 'false'){
                                localiza.limpiarFiltrosLocaliza();
                            }else{
                                //muestra el tab de seg
                                //principal.ActivarTab('seguimiento');
                            }
                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Localización",
                text: "El ID de la localización no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    },

    MERLOC_BuscarMercancia: function (p_mercancia){
        localiza.MERLOC_ConsultarDatos(p_mercancia);
    },

    MERLOC_ConsultarDatos: function (p_mercancia) {      
        var postdata = { id: p_mercancia };
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MERCANCIAS_TB_Localiza/MercanciaDetalle?id='+ p_mercancia,
            //data: postdata,
            success: function (data) {
                if (data !== null) {
                    //trae los datos de la mercancia
                     localiza.ActiveCamposLocaliza();
                     localiza.MERLOC_MuestraDatos(data);
                    $("#divwithscroll2").attr('style', 'display:none');
                    $("#th_mercLocalizaEspecial").attr('style', 'display:none');                   
                    $("#div_filters").attr('style', 'display:none');     
                    $("#detalles-localiza").attr('style', 'display:block');
                    $("#btn_regresar").attr('style', 'display:block');
                    $("#btn_actualizar").attr('style', 'display:block');
                    $("#btn_filtrar_mercaLocaliza").attr('style', 'display:none');
                    $("#btn_limpiar_filtros").attr('style', 'display:none');
                   
                }
            },
            failure: function (data) {
                
                if (data.responseJSON.Message == "false") {
                    prod_nc.infoConsecutivo(consecutivo, sistema);
                } else {
                    swal({
                        title: "Localización Especial",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            },
            error: function (data) {

                if (data.responseJSON.Message == "false") {
                    prod_nc.infoConsecutivo(consecutivo, sistema);
                } else {
                    swal({
                        title: "Localización Especial",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            }
        });
    },

    MERLOC_MuestraDatos: function (p_mercancia) {       
        for (var campo in p_mercancia) {
            if(campo === "MER_Refrigerado"){
                if(p_mercancia[campo] === "S"){
                    p_mercancia[campo] = true;
                } else{
                    p_mercancia[campo] = false;
                }
            }
            if(p_mercancia[campo] != null){
                principal.setValueByName(campo, p_mercancia[campo],'frm_localiza');
            }  
        }      
    },

    MERLOC_MostrarListaPeligrosa: function () {
        $("#myModalPeligrosa").modal('show');
       // $("#divProcessing").show();
        localiza.BuscarPeligrosa();
        
    },

    BuscarPeligrosaFiltro: function () {
        principal.filterTable_Input('loc_flt_cod_peligrosa', 'LOC_Peligrosa_table1', 0);
        principal.filterTable_Input('loc_flt_nom_peligrosa', 'LOC_Peligrosa_table1', 1);
    },

    BuscarPeligrosa: function () {       
        var templateText = $("#LOC_Peligrosa_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $(".modal-body #LOC_Peligrosa_table1body,.modal-body textarea").each(function () {
            $(this).html('');
        });

      
        $.getJSON('/api/TX_MERCANCIAS_TB_Localiza/Peligrosa').then(function (data) {
            $.each(data, function (index, elem) {
                $("#LOC_Peligrosa_table1body").append(tableTemplate(elem));
            })
            
        });     
    },

    MERLOC_MuestraDatosPeligrosa: function (p_codpeligrosa, p_nompeligrosa) {
        $("#myModalPeligrosa").modal('hide');
       $('#MER_TIP_CODIGO_IATA').val(p_codpeligrosa);
       $('#PEL_Descripcion').val(p_nompeligrosa);
        // $.getJSON('/api/TX_MERCANCIAS_TB_Localiza/PeligrosaId?id=' + p_codpeligrosa).then(function (data) {
        //     $.each(data, function (index, elem) {
        //         $('#MER_TIP_CODIGO_IATA').val(elem.Id);
        //         $('#PEL_Descripcion').val(elem.Nombre);
        //         localiza.ActiveCamposLocaliza();
        //     })
        // });
        
    },

    limpiarFiltrosLocalizaPeligrosa: function () {
        $('.modal-body input,.modal-body textarea').each(function () {
            $(this).val('');
        });
        $('.modal-body label,.modal-body textarea').each(function () {
            $(this).removeClass('active');
        });
       localiza.BuscarPeligrosa();
    },


    limpiarFiltrosLocaliza: function(){
        $("#in_flr_mer_ien_entrada").val("");
        $("#in_flr_mer_id_mercancia").val("");
        localiza.DeactiveCamposLocaliza();     
         $("#MercLocalizaEspecial_table1body").html('');
         TecladoFuncionalidadPersonalizada(); 
         $("#in_flr_mer_ien_entrada").focus();
    },

    ActiveCamposLocaliza: function () {
        $('label').addClass('active');
    },

    DeactiveCamposLocaliza: function () {
        $('label').removeClass('active');
    }
    
};

$(document).ready(function () {
    localiza.init();
})