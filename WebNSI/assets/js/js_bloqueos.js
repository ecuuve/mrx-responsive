var bloqueo = {

    init: function () {
        TecladoFuncionalidadPersonalizada();
         bloqueo.BLO_limpiarFiltros();
        bloqueo.BLO_CargarControles();   
        $('#btn_guardar_bloqueoOPS').prop('disabled', true); 
        $('#in_blo_ien_entrada').prop('disabled', true);    



        $('#in_flr_blo_ien_entrada').change(function () {
          //var cod_p = $('#AGA_NAGENCIA_ADUANAL').val();
          bloqueo.BLO_ConsultaGuiaBloqueo();
        });

        //Disparo evento de consulta de Consecutivo cuando lo digitan
        // $('#in_flr_blo_ien_entrada').bind("enterKey",function(e){
        //   bloqueo.BLO_ConsultaGuiaBloqueo();
        // });
        // $('#in_flr_blo_ien_entrada').keyup(function(e){
        //   if(e.keyCode == 13)
        //   {
        //     $(this).trigger("enterKey");
        //   }
        // });           
    },

    onfocus: function () {
      if ($("#lbl_consec").hasClass('lblfocus'))
          $("#lbl_consec").removeClass('lblfocus');     
    },

    BLO_CargarControles: function () {
        bloqueo.getTipoBloqueo();
        bloqueo.getViaSolicitud();
    },

    BLO_ConsultaGuiaBloqueo: function () {      
        var p_entrada = $("#in_flr_blo_ien_entrada").val();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_BLOQUEOS_TB/GetDatosGuiaBloqueo?id='+p_entrada,
            //data: postdata,
            success: function (data) {
                    if (data !== null) { 
                    if(principal.isArray(data)){
                        if(data.length > 0 ){
                            if(data[0]["IEN_Entrada"] > 0){
                       
                           if(data[0]["Existencias_p"] > 0){
                            bloqueo.BLO_ActiveCampos();
                            bloqueo.BLO_MuestraDatos(data);
                            $('#btn_guardar_bloqueoOPS').prop('disabled', false);
                             bloqueo.BLO_CargarControles(); 
                           }else{

                            swal({
                            title: "Bloqueo de consecutivos",
                            text: "No es posible bloquear el consecutivo consultado.",
                            icon: "error",
                            button: "OK!",
                            })

                           }
                       
                       
                        }
                        }else{

                            swal({
                            title: "Consulta bloqueo",
                            text: "No existen datos para el valor consultado.",
                            icon: "error",
                            button: "OK!",
                            })
                        }
                    

                    }else{
                            
                        if (typeof data === 'string' || data instanceof String){
                          swal({
                            title: "Consulta bloqueo",
                            text: data,
                            icon: "error",
                            button: "OK!",
                          })
                        }

                    }                       
                }
            },
            failure: function (data) {
                
                console.log("fail");
            },
            error: function (xhr, textStatus, errorThrown) {
              console.log(textStatus);
                //console.log("error consulta guía");
            }
        });
    },

    BLO_MuestraDatos: function (p_guia) {
        for (var campo in p_guia[0]) {
            if (campo != null) {
                principal.setValueByName(campo, p_guia[0][campo], 'frm_guiabloqueo');
            }
        }      
    },

    //Metodo para crear un nuevo bloqueo
    BLO_GuardarBloqueo: function () {
        $("#in_blo_ien_entrada").attr("disabled", false);        
        $.post(
                "/api/TX_BLOQUEOS_TB/",
                 $('#frm_guiabloqueo').serialize(),
                function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Bloqueo de consecutivos",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        }).then((value) => {
                           
                            $("#in_blo_ien_entrada").attr("disabled", true);
                              
                        });

                    }else{
                        swal({
                        title: "Bloqueo de consecutivos",
                        text: "Bloqueo guardado exitosamente",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            bloqueo.BLO_limpiarFiltros();
                            $("#in_blo_ien_entrada").attr("disabled", true);
                             $('#btn_guardar_bloqueoOPS').prop('disabled', true);  
                        });
                    }
                })
                .fail(function (data) {

                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                })
        ;
    },

    getViaSolicitud: function (valor) {    
        principal.getData(
            "/api/TX_BLOQUEOS_TB/ViaSolicitud",
            function (data) {
                $("#blo_viasolicitud").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        );
    },
    
    getTipoBloqueo: function (valor) {
        principal.getData(
            "/api/TX_BLOQUEOS_TB/TipoBloqueo",
            function (data) {
                $("#blo_tipobloqueo").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        );
    },

    BLO_LimpiarCampos: function(){
        principal.resetAllFields('detInfo_guiaBloqueo');
        bloqueo.BLO_DeactiveCampos();
         $("#in_flr_ien_entrada").focus();       
         TecladoFuncionalidadPersonalizada(); 
        $('#btn_guardar_bloqueoOPS').prop('disabled', true);
    },

    BLO_limpiarFiltros: function(){
       $("#in_flr_blo_ien_entrada").val("");
       $('#btn_guardar_bloqueoOPS').prop('disabled', true);
       bloqueo.BLO_LimpiarCampos();
    },
    
    BLO_ActiveCampos: function () {
        $('label').addClass('active');
    },

    BLO_DeactiveCampos: function () {
        $('label').removeClass('active');
    }
    
};



$(document).ready(function () {
    bloqueo.init();
})