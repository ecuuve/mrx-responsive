﻿var declarante = {

    init: function () {      
         $("#detalles-declarante").attr('style', 'display:none');
         $("#btn_regresar_declarante").attr('style', 'display:none');
         $("#btn_regresar_declaranteupt").attr('style', 'display:none');  
         $("#btn_actualizar_declarante").attr('style', 'display:none');
         $("#btn_guardar_declarante").attr('style', 'display:none');
         $("#btn_limpiar_declarante").attr('style', 'display:none');

         var p_cod_declarante = $("#flr_iddeclarante").val();
         var p_nombre_declarante = $("#flr_nombredeclarante").val();

        //if (p_cod_declarante != '' ||  p_nombre_declarante != ''){
            
            if(p_cod_declarante != 'NA'){
                $("#in_flr_dct_declarante_id").val(p_cod_declarante);
            }
            if(p_nombre_declarante != 'NA'){
                $("#in_flr_dct_nombre_declarante").val(p_nombre_declarante);
            }          
            principal.activeLabels();
            declarante.DCT_Listar();
            $("#flr_iddeclarante").val("");
            $("#flr_nombredeclarante").val("");
         
         declarante.getEstadosTica("");
    },

    DCT_Listar: function(){
        //Buscar lista según filtros
          var p_cod_declarante = $("#in_flr_dct_declarante_id").val();
          var p_nombre_declarante = $("#in_flr_dct_nombre_declarante").val();
            //Obtenemos la plantilla
            $('#th_Declarantes').show();
            $("#th_Declarantes").attr('style', 'display:block');
            var templateText = $("#Declarantes_table-template").html();
            //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#Declarantes_table1body").html('');
            $.getJSON('/api/TX_DECLARANTES_TB/GetDeclarantes?cod_declarante='+ p_cod_declarante + '&nombre=' + p_nombre_declarante).then(function (data) {
                $.each(data, function (index, elem) {
                    $("#Declarantes_table1body").append(tableTemplate(elem));
                })
            });
        //}
    },

    DCT_Regresar:function(){
        var p_cod_declarante = $("#in_flr_dct_declarante_id").val();
        var p_nombre_declarante = $("#in_flr_dct_nombre_declarante").val();

        if (p_cod_declarante != '' ||  p_nombre_declarante != ''){
            principal.activeLabels();
        }

        $("#detalles-declarante").attr('style', 'display:none');
        $("#btn_regresar_declarante").attr('style', 'display:none');
        $("#btn_regresar_declaranteupt").attr('style', 'display:none');       
        $("#btn_actualizar_declarante").attr('style', 'display:none');
        $("#btn_guardar_declarante").attr('style', 'display:none');
        $("#btn_limpiar_declarante").attr('style', 'display:none');
        $("#th_Declarantes").attr('style', 'display:block');
        $("#div_filters").attr('style', 'display:block');   
        $("#btn_nuevo_declarante").attr('style', 'display:block');  

        $("#DCT_Cedula").attr('disabled', true);
        $("#DCT_Id").attr('disabled', true);
        $("#DCT_Usu_Registro").attr('style', 'display:block');  
        $("#DCT_Fch_Registro").attr('style', 'display:block');  
        $("#lbl_usu").attr('style', 'display:block');  
        $("#lbl_fch").attr('style', 'display:block'); 
    },

    DCT_Regresar_Update: function(){
        declarante.DCT_Regresar();
        declarante.DCT_Listar();
    },

    DCT_limpiarFiltros: function () {
        $("#in_flr_dct_declarante_id").val("");
        $("#in_flr_dct_nombre_declarante").val("");
        $("#Declarantes_table1body").html('');
        principal.deactivateLabels();       
        declarante.DCT_Listar();
    },

    DCT_limpiar: function () {
        principal.resetAllFields("detInfo_declarante");
        principal.deactivateLabels();    
        $("#DCT_Id").focus();  
    },

    DCT_ConsultarDatos: function (p_codigo) {      
        var postdata = { id: p_codigo };
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_DECLARANTES_TB/GetDeclaranteDetalle?cod_declarante='+ p_codigo,
            data: postdata,
            success: function (data) {
                if (data !== null) {

                    //trae los datos 
                     principal.activeLabels();
                     declarante.DCT_MuestraDatos(data[0]);
                    //$("#divwithscroll2").attr('style', 'display:none');
                    $("#th_Declarantes").attr('style', 'display:none');                   
                    $("#div_filters").attr('style', 'display:none');     
                    $("#detalles-declarante").attr('style', 'display:block');
                    $("#btn_regresar_declaranteupt").attr('style', 'display:block');
                    $("#btn_actualizar_declarante").attr('style', 'display:block');
                    $("#btn_limpiar_declarante").attr('style', 'display:none'); 
                    $("#btn_nuevo_declarante").attr('style', 'display:none'); 
                   
                }
            },
            failure: function (data) {
                
                console.log("fail");
            },
            error: function (data) {

                console.log("error consulta declarante");
            }
        });
    },

    DCT_MuestraDatos: function (p_declarante) {       
        for (var campo in p_declarante) {
            if(campo == "DCT_Estado_Tica"){
                declarante.getEstadosTica(p_declarante[campo]);              
            }
            if(campo == "DCT_Telefono" && p_declarante[campo] == 0){
                p_declarante[campo] = "";             
            }
            if(p_declarante[campo] != null){
                principal.setValueByName(campo, p_declarante[campo],'frm_declarante');
            }  
        }      
    },

    DCT_Actualizar: function(){
        $("#DCT_Id").attr('disabled', false);
       

        var id = $('#DCT_Id').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_declarante').serializeArray());
            $("#DCT_Id").attr('disabled', true);   
            $.ajax({
                url: '/api/TX_DECLARANTES_TB/' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Actualización de declarante",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{
                        swal({
                        title: "Declarantes",
                        text: "Se actualizó correctamente la información",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            
                            //declarante.DCT_limpiarFiltros();
                            //declarante.DCT_Listar();
                            
                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Declarantes",
                text: "El ID de declarante no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    },

    getEstadosTica: function (valor) {
        if (valor==="")
        {
            valor = "A";
        }
        principal.getData(
            "/api/TX_DECLARANTES_TB/GetEstadosTica",
            function (data) {
                $("#S_DCT_Estado_Tica").html(principal.arrayToOptionsSelected(data, valor));
            }
        )
    },


    DCT_Nuevo: function(){
        $("#detalles-declarante").attr('style', 'display:block');
       $("#btn_regresar_declarante").attr('style', 'display:block');
        $("#btn_regresar_declaranteupt").attr('style', 'display:none');  
        $("#btn_guardar_declarante").attr('style', 'display:block');
        $("#btn_limpiar_declarante").attr('style', 'display:block');
        $("#th_Declarantes").attr('style', 'display:none');
        $("#div_filters").attr('style', 'display:none');   
        $("#btn_nuevo_declarante").attr('style', 'display:none');  

        $("#DCT_Id").attr('disabled', false);
        $("#DCT_Cedula").attr('disabled', false);
        $("#DCT_Usu_Registro").attr('style', 'display:none');  
        $("#DCT_Fch_Registro").attr('style', 'display:none');  
        $("#lbl_usu").attr('style', 'display:none');  
        $("#lbl_fch").attr('style', 'display:none');  

        declarante.DCT_limpiar();      
    },

    DCT_Funcionarios: function(id,nombre){
        var p_cod_declarante = $("#in_flr_dct_declarante_id").val();
        var p_nombre_declarante = $("#in_flr_dct_nombre_declarante").val();
        if(p_cod_declarante == ""){
            p_cod_declarante = "NA";
        }
        if(p_nombre_declarante == ""){
            p_nombre_declarante = "NA";
        }
        if(nombre.includes("/")){
            nombre = nombre.replace("/", "-");
        }
        var Dato = new Array( id, nombre,p_cod_declarante, p_nombre_declarante);
        window.location.href = "/Declarante/Funcionarios/" + Dato;  
    },

    DCT_Guardar: function(){   
        $("#USER_MERX").val($("#UsReg").val());  
        var id = $('#DCT_Id').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_declarante').serializeArray()); 
            $.ajax({
                url: '/api/TX_DECLARANTES_TB/',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Ingreso de declarante",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{
                        swal({
                        title: "Declarantes",
                        text: "Se ingresó correctamente la información",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            declarante.DCT_limpiar();                           
                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Declarantes",
                text: "El ID del declarante no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    }
};

$(document).ready(function () {
    declarante.init();
})