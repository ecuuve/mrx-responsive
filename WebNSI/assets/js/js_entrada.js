﻿var entrada = {

    init: function () {

            TecladoFuncionalidadPersonalizada();
            entrada.CargaFechas();            
    },

    BuscarEntrada: function (){
        var p_entrada = $("#in_flr_ien_entrada").val();
        var p_consolidador = $("#in_flr_ien_consolidador").val();
        var p_guiaOr = $("#in_flr_guia_original").val();
        var p_aerolinea = $("#in_flr_aerolinea").val();
        entrada.ENT_ConsultarDatos(p_entrada,p_guiaOr,p_consolidador,p_aerolinea);
    },

    ENT_ConsultarDatos: function (p_entrada,p_guiaOr,p_consolidador,p_aerolinea) {      
        //var postdata = { id: p_entrada, guia: p_guiaOr, consolidador: p_consolidador, aerolinea: p_aerolinea };
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_ENTRADAS_TB/Entrada?id='+p_entrada+'&guia='+p_guiaOr+'&consolidador='+p_consolidador+'&aerolinea='+p_aerolinea,
            //data: postdata,
            success: function (data) {
                if (data !== null) {
                  
                  if(principal.isArray(data)){

                    entrada.ENT_Listar(data);

                  }else{
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Consulta guías",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }
                    else{
                      if(data["IEN_Entrada"] != 0){
                         //trae los datos de la guia
                         entrada.ActiveCamposEntrada();
                         entrada.ENT_MuestraDatos(data);
                         mercancia.MER_Listar();
                      }
                      else{

                        swal({
                          title: "Consulta guías",
                          text: "No existen datos para los valores consultados.",
                          icon: "error",
                          button: "OK!",
                        })

                      }
                    }
                    
                  }                   
                }
            },
            failure: function (data) {
                
                console.log("fail");
            },
            error: function (xhr, textStatus, errorThrown) {
              console.log(textStatus);
                //console.log("error consulta guía");
            }
        });
    },

    ENT_MuestraDatos: function (p_guia) {
        for (var campo in p_guia) {           
            if(campo == "IEN_Bloqueada"){
                if(p_guia[campo] == "S"){
                    p_guia[campo] = true;
                } else{
                    p_guia[campo] = false;
                }
            }
            if(campo == "IEN_ES_Courier"){
                if(p_guia[campo] == "S"){
                    p_guia[campo] = true;
                } else{
                    p_guia[campo] = false;
                }
            }
            if(campo == "IEN_Muestras"){
                if(p_guia[campo] == "S"){
                    p_guia[campo] = true;
                } else{
                    p_guia[campo] = false;
                }
            }
            if(p_guia[campo] != null){
                principal.setValueByName(campo, p_guia[campo],'frm_guiaconsulta');
            }  
        }      
    },
    
    ENT_Listar: function (data) {

        var p_entrada = $("#in_flr_ien_entrada").val();
        var p_consolidador = $("#in_flr_ien_consolidador").val();
        var p_guiaOr = $("#in_flr_guia_original").val();
        var p_aerolinea = $("#in_flr_aerolinea").val();


        //Obtenemos la plantilla
        
        var templateText = $("#ENT_guias_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#ENT_guias_tablebody").html('');
         
          $.getJSON('/api/TX_ENTRADAS_TB/Entrada?id='+p_entrada+'&guia='+p_guiaOr+'&consolidador='+p_consolidador+'&aerolinea='+p_aerolinea).then(function (data) {
            $.each(data, function (index, elem) {
                $("#ENT_guias_tablebody").append(tableTemplate(elem));
            })
          });


          // for (var elem in data) {      
          //   $("#ENT_guias_tablebody").append(tableTemplate(elem));
          // }   

           $("#myModal").modal('show');
    },


    ENT_Escoger: function(prm_entrada){
        var p_entrada = prm_entrada;//$("#in_flr_ien_entrada").val();
        var p_consolidador = 0;//$("#in_flr_ien_consolidador").val();
        var p_guiaOr = "";//$("#in_flr_guia_original").val();
        var p_aerolinea = 0;//$("#in_flr_aerolinea").val();

         entrada.ENT_ConsultarDatos(p_entrada,p_guiaOr,p_consolidador,p_aerolinea);
         $("#myModal").modal('hide');
    },


    LimpiarCamposEntrada: function(){
        principal.resetAllFields('detInfo_guia');
        entrada.DeactiveCamposEntrada();
         $("#in_flr_ien_entrada").focus();
         entrada.CargaFechas();
         $("#Mercancias_Tablebody").html('');
         TecladoFuncionalidadPersonalizada(); 
    },

    limpiarFiltrosEntrada: function(){
       $("#in_flr_ien_entrada").val("");
       $("#in_flr_ien_consolidador").val("");
       $("#in_flr_guia_original").val("");
       $("#in_flr_aerolinea").val("");
       entrada.LimpiarCamposEntrada();
    },
    

    CargaFechas: function(){
        var today = new Date();
        var minDate = today.setDate(today.getDate()-1);
        $("#dtp_ien_fch_manifiesto").kendoDatePicker({
          format: "MM/dd/yyyy",
          value: new Date(),
          min: new Date(minDate),
          max: new Date(),
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });
        
        $("#dtp_ien_fch_digitacion").kendoDatePicker({
          format: "MM/dd/yyyy",
          value: new Date(),
          min: new Date(minDate),
          max: new Date(),
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });  

         $("#dtp_ien_fch_ingreso").kendoDatePicker({
          format: "MM/dd/yyyy",
          value: new Date(),
          min: new Date(minDate),
          max: new Date(),
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });               
    },

    ActiveCamposEntrada: function () {
        $('label').addClass('active');
    },

    DeactiveCamposEntrada: function () {
        $('label').removeClass('active');
    }
    
};



$(document).ready(function () {
    entrada.init();
})