﻿var funcionario = {

    init: function () {
         IdDeclarante = $("#DCT_Id").val();
         funcionario.FUNC_Listar();
         funcionario.getTipos("");
         funcionario.getEstadosTica("");
        $("#detalles-DeclaranteFunc").attr('style', 'display:none');
        $('#th_funcionarios').show();
        $('#div_filters').show();


         $("#btn_listar_funcionarios").attr('style', 'display:none');
         $("#btn_nuevo_funcionario").attr('style', 'display:block');
         $("#btn_guardar_funcionario").attr('style', 'display:none');            
         $("#btn_actualizar_funcionario").attr('style', 'display:none');
         $("#btn_limpiar_funcionario").attr('style', 'display:none');
         $("#btn_regresar_func_declarante").attr('style', 'display:block');

         funcionario.FUNC_limpiarFiltros();
               
    },


    //Consulta los datos de funcionarios del declarante
    FUNC_Listar: function(){
        funcionario.AsignaIdDeclarante();
        var templateText = $("#tableDeclaranteFunc-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#DeclaranteFuncTablebody").html('');
        $.getJSON('/api/TX_AGENTES_DECL_TB/GetFuncionarios?cod_declarante=' + IdDeclarante).then(function (data) {
            $.each(data, function (index, elem) {
                $("#DeclaranteFuncTablebody").append(tableTemplate(elem));
            })
        });
    },

    //Carga la tabla de funcionarios del declarante seleccionado
    FUNC_Cargar: function () {
        //Oculta campos de agregar nuevo y editar
        $('#detalles-DeclaranteFunc').hide();
        $('#th_funcionarios').show();
        $('#div_filters').show();
        $("#btn_listar_funcionarios").attr('style', 'display:none');
         $("#btn_nuevo_funcionario").attr('style', 'display:block');
         $("#btn_guardar_funcionario").attr('style', 'display:none');            
         $("#btn_actualizar_funcionario").attr('style', 'display:none');
         $("#btn_limpiar_funcionario").attr('style', 'display:none');
         $("#btn_regresar_func_declarante").attr('style', 'display:block');

            funcionario.FUNC_limpiarFiltros();

            funcionario.FUNC_Listar();      
    },

    //Consulta los datos del funcionario
    FUNC_ConsultarDatosEditar: function (p_idDCL, p_idFNC) {      
        var postdata = { cod_declarante: p_idDCL, cod_funcionario: p_idFNC };
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_AGENTES_DECL_TB/GetFuncionarioDetalle?cod_declarante='+ p_idDCL + '&cod_funcionario=' + p_idFNC,
            data: postdata,
            success: function (data) {
                if (data !== null) {

                    //trae los datos 
                     principal.activeLabels();
                     funcionario.FUNC_MuestraDatos(data[0]);
                    //$("#divwithscroll2").attr('style', 'display:none');
                    $("#th_funcionarios").attr('style', 'display:none');                   
                    $("#div_filters").attr('style', 'display:none');     
                    $("#detalles-DeclaranteFunc").attr('style', 'display:block');

                     $("#ADC_Usu_Registro").attr('style', 'display:block');  
                     $("#ADC_Fch_Registro").attr('style', 'display:block');
                     $("#lbl_usu").attr('style', 'display:block');  
                     $("#lbl_fch").attr('style', 'display:block');    
                    
                     $("#btn_listar_funcionarios").attr('style', 'display:block');
                     $("#btn_nuevo_funcionario").attr('style', 'display:none');
                     $("#btn_guardar_funcionario").attr('style', 'display:none');     
                     $("#btn_actualizar_funcionario").attr('style', 'display:block');
                     $("#btn_limpiar_funcionario").attr('style', 'display:none');
                     $("#btn_regresar_func_declarante").attr('style', 'display:block');
                   
                }
            },
            failure: function (data) {
                
                console.log("fail");
            },
            error: function (data) {

                console.log("error consulta funcionario");
            }
        });
    },

    //Muestra los datos en el form de funcionario
    FUNC_MuestraDatos: function (p_funcionario) {       
        for (var campo in p_funcionario) {
            if(campo === "ADC_Estado_Tsm"){
                if(p_funcionario[campo] === "A"){
                    p_funcionario[campo] = true;
                } else{
                    p_funcionario[campo] = false;
                }
            }

            if(campo == "ADC_TIPO"){
                funcionario.getTipos(p_funcionario[campo]);              
            }
            if(p_funcionario[campo] != null){
                principal.setValueByName(campo, p_funcionario[campo],'frm_funcionario');
            }  
        }      
    },

    FUNC_Guardar: function(){   
        $('[name= "ADC_Estado_Tsm"]').is(":checked") == true ? $('[name= "ADC_Estado_Tsm"]').val('A') : $('[name= "ADC_Estado_Tsm"]').val('I');
        $("#USER_MERX").val($("#UsReg").val());  
        var id = $('#DCT_Id').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_funcionario').serializeArray()); 
            $.ajax({
                url: '/api/TX_AGENTES_DECL_TB/',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Ingreso de funcionario",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{
                        swal({
                        title: "Funcionarios",
                        text: "Se ingresó correctamente la información",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            funcionario.FUNC_limpiar();                           
                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Funcionarios",
                text: "El ID del declarante no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    },

    FUNC_Actualizar: function(){ 
        $('[name= "ADC_Estado_Tsm"]').is(":checked") == true ? $('[name= "ADC_Estado_Tsm"]').val('A') : $('[name= "ADC_Estado_Tsm"]').val('I');

        var id = $('#DCT_Id').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_funcionario').serializeArray()); 
            $.ajax({
                url: '/api/TX_AGENTES_DECL_TB/' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Actualización de declarante",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{
                        swal({
                        title: "Declarantes",
                        text: "Se actualizó correctamente la información",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            
                            //declarante.DCT_limpiarFiltros();
                            //declarante.DCT_Listar();
                            
                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Declarantes",
                text: "El ID de declarante no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    },

    //Asigna el id de declarante para los funcionarios 
    AsignaIdDeclarante: function(){
        if (IdDeclarante == ""){
            IdDeclarante = $("#DCT_Id").val();
        }else{
            $("#DCT_Id").val(IdDeclarante);
        }
    },

    FUNC_Nuevo: function(){
        $("#detalles-DeclaranteFunc").attr('style', 'display:block');
        $("#th_funcionarios").attr('style', 'display:none');
        $("#div_filters").attr('style', 'display:none');   

        $("#btn_listar_funcionarios").attr('style', 'display:block');
        $("#btn_nuevo_funcionario").attr('style', 'display:none');
        $("#btn_guardar_funcionario").attr('style', 'display:block');            
        $("#btn_actualizar_funcionario").attr('style', 'display:none');
        $("#btn_limpiar_funcionario").attr('style', 'display:block');
        $("#btn_regresar_func_declarante").attr('style', 'display:block');
          

        $("#ADC_Usu_Registro").attr('style', 'display:none');  
        $("#ADC_Fch_Registro").attr('style', 'display:none');  
        $("#lbl_usu").attr('style', 'display:none');  
        $("#lbl_fch").attr('style', 'display:none');  

        funcionario.FUNC_limpiar();      
    },

    FUNC_limpiar: function () {
        principal.resetAllFields("detalles-DeclaranteFunc");
        principal.deactivateLabels(); 
        $("#ADC_Nombre").focus();     
    },

     //Ejecuta los filtros que se hayan seleccionado en la pantalla de funcionario
    BusquedaFiltros: function () {
        principal.filterTable_Input('in_flr_funcionario', 'tb_funcionarios', 2);
    },

    //Obtiene los tipos de funcionario para el select
    getTipos: function (valor) {
        if (valor==="")
        {
            valor = "AG";
        }
        principal.getData(
            "/api/TX_AGENTES_DECL_TB/GetTiposFuncionario",
            function (data) {
                $("#S_ADC_Tipo").html(principal.arrayToOptionsSelected(data, valor));
            }
        )
    },

    //Obtiene los estados tica para el select
    getEstadosTica: function (valor) {
        if (valor==="")
        {
            valor = "A";
        }
        principal.getData(
            "/api/TX_AGENTES_DECL_TB/GetEstadoTica",
            function (data) {
                $("#S_ADC_Estado_Tica").html(principal.arrayToOptionsSelected(data, valor));
            }
        )
    },

     //Regresa a declarante
    RegresarDeclarante: function () {
        var flr_iddeclarante = $("#flr_iddeclarante").val();
        var flr_nombredeclarante = $("#flr_nombredeclarante").val();
        if(flr_iddeclarante == ""){
            flr_iddeclarante = "NA";
        }
        if(flr_nombredeclarante == ""){
            flr_nombredeclarante = "NA";
        }

        var Dato = new Array(flr_iddeclarante, flr_nombredeclarante);
        window.location.href = "/Declarante/Index/" + Dato;  
    },

    FUNC_limpiarFiltros: function () {
        $("#in_flr_funcionario").val("");
        $("#DeclaranteFuncTablebody").html('');
        principal.deactivateLabels();       
    }
};

$(document).ready(function () {
    funcionario.init();
})