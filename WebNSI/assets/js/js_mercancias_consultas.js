var mercancias_consultas = {
    filter: null,
    tipo_cliente: null,
    aerolinea: null,
    codigo_cliente: null,
    codigo_aerolinea: null,

    init: function () {
        principal.resetAllFields('frm_ingresoMerc');
        mercancias_consultas.CargaInicial();
        
    },

    //Carga inicial de controles de la pantalla
    CargaInicial: function () {
        //Inicializacion de componentes de fecha
        principal.KendoDate($("#TMA_FCH_MANIFIESTO"), new Date());
        principal.KendoDate($("#TMA_FCH_INGRESO"), new Date());
        $("#ingmerc_flr_fecha").kendoDatePicker({
            format: "dd/MM/yyyy"
        });
        $('#btn_regresar_ingresoMerc').hide();
        $('#btn_conocimientos').hide();
        //Inicializacion de los comboboxKendo
        mercancias_consultas.getFuncionarios();
        mercancias_consultas.getPuertos();
        mercancias_consultas.getAduanas();
        mercancias_consultas.getTransportistasFiltro();
        //Inicializacion select normales
        mercancias_consultas.getTiposContenedor();
        mercancias_consultas.getModalidadesTransporte();
        mercancias_consultas.getTamanos();
        mercancias_consultas.getDocumentos();
        mercancias_consultas.getTipoCliente();
        mercancias_consultas.getEstados();
    },

    //Lista los tipos de contenedor
    getTiposContenedor: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Tipo_Contenedor", function (data) { $("#TMA_TIPO_CONTENEDOR").html(principal.arrayToOptions(data)); });
    },

    //Lista las modalidades de transporte
    getModalidadesTransporte: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/ModalidadesTransporte", function (data) { $("#TMA_MOD_TRANSPORTE").html(principal.arrayToOptions(data)); });
    },

    //Lista los tamaños
    getTamanos: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Tamano", function (data) { $("#TMA_TAMANO").html(principal.arrayToOptions(data)); });
    },

    //Lista los documentos
    getDocumentos: function () {
        var tipo_documento = null;
        var cb = function (data) {
            $("#TMA_TDO_DOCUMENTO").html(principal.arrayToOptions(data));
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Documentos?tipo_documento=" + tipo_documento, '', cb);
    },

    //Lista los estados
    getEstados: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Estados", function (data) { $("#TMA_ESTADO").html(principal.arrayToOptionsSelected(data, 'A')); });
        // principal.getData("/api/TX_MANIFIESTOS_TB/Estados",function (data) {$("#TMA_ESTADO").html(principal.arrayToOptions(data));});
    },

    //Lista los tipos de Clientes
    getTipoCliente: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/TiposClientes", function (data) { $("#TMA_CLIENTE_TIPO").html(principal.arrayToOptions(data)); });
    },

    //Lista los Tranportistas
    getTransportistas: function (e = null, m = null, c = null) {
        var modalidad = e != null ? e : $('#TMA_MOD_TRANSPORTE option:selected').val();
        //var aerolinea = m != null ? m : '';
        var aerolinea = '';
        var cod_aerolinea = c != null ? c : '';

        var cb = function (data) {
            principal.KendoComboBox(data, '#TMA_AER_NAEROLINEA');

            //Funcionalidad de carga de datos para la edición
            if (cod_aerolinea != null && mercancias_consultas.aerolinea != null) {

                $('#TMA_AER_NAEROLINEA').val(cod_aerolinea);
                $('#TMA_AER_NAEROLINEA > option').each(function (i, v) {
                    if ($(this).text() == mercancias_consultas.aerolinea) {
                        $("#TMA_AER_NAEROLINEA").data('kendoComboBox').select(i);
                    }
                });

                mercancias_consultas.aerolinea = null;
                mercancias_consultas.cod_aerolinea = null;
            }
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Transportistas?mod_transporte=" + modalidad + "&&aerolinea=" + aerolinea, '', cb);
    },

    //Lista los transportistas para la seccion de filtros
    getTransportistasFiltro: function () {
        var aerolinea = '';
        principal.getData("/api/TX_MANIFIESTOS_TB/TransportistasFiltro?aerolinea=" + aerolinea, function (data) { $("#ingmerc_flr_transportista").html(principal.arrayToOptions(data)); });
    },

    //Lista los clientes
    getClient: function (e = null, t = null, c = null) {
        //var cliente = e != null ? e : '';
        var cliente = '';
        var tipo_cliente = t != null ? t : $('#TMA_CLIENTE_TIPO option:selected').val();
        var cod_cliente = c != null ? c : '';

        var cb2 = function (data) {
            principal.KendoComboBox(data, '#TMA_CLIENTE_ID');
            if (cod_cliente != null && mercancias_consultas.tipo_cliente != null) {
                $('#TMA_CLIENTE_ID').val(cod_cliente);
                $('#TMA_CLIENTE_ID > option').each(function (i, v) {
                    if ($(this).text() == mercancias_consultas.tipo_cliente) {
                        $("#TMA_CLIENTE_ID").data('kendoComboBox').select(i);
                    }
                });
                mercancias_consultas.tipo_cliente = null;
                mercancias_consultas.codigo_cliente = null;
            }
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Clientes?cliente=" + cliente + "&tipo_cliente=" + tipo_cliente, '', cb2);
    },

    //Lista los funcionarios y ejecuta el filtro
    getFuncionarios: function () {
        var cb = function (data) {
            principal.KendoComboBox(data, '#TMA_FUN_FUNCIONARIO');
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Funcionarios?id=" + '', '', cb);
    },

    //Lista los puertos
    getPuertos: function () {
        var cb = function (data) {
            principal.KendoComboBox(data, '#TMA_PTO_PUERTO');
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Puertos?id=" + '', '', cb);
    },

    //Lista las aduanas con los filtros kendo respectivos
    getAduanas: function () {
        var cb = function (data) {
            principal.KendoComboBox(data, '#TMA_ADU_ENVIA');
            principal.KendoComboBox(data, '#TMA_ADU_RECIBE');
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Aduanas?id=" + '', '', cb);
    },
    
    //Manejo de botones entre el listar y el formulario
    Refrescar: function () {
        mercancias_consultas.limpiaFiltros();
        if ($('#tbl_ingresoMerc').hasClass('fullHidden'))
            $('#tbl_ingresoMerc').removeClass('fullHidden')
        if ($('#div_ingresoMerc_filtros').hasClass('fullHidden'))
            $('#div_ingresoMerc_filtros').removeClass('fullHidden')
        if (!$('#frm_ingresoMerc').hasClass('fullHidden'))
            $('#frm_ingresoMerc').addClass('fullHidden')
        $('#btn_listar_ingresoMerc').hide();
        $('#btn_guardar_ingresoMerc').hide();
        $('#btn_editar_ingresoMerc').hide();
        $('#btn_regresar_ingresoMerc').show();
        mercancias_consultas.buscar();
    },

    //Ejecuta la busqueda de un manifiestos mediante filtros especificos
    buscar: function () {
        var manifiesto_id = null;
        var tma_manifiesto_p = $('#ingmerc_flr_manifiesto').val();
        var tma_fch_manifiesto_p = $('#ingmerc_flr_fecha').val();
        var tma_aer_naerolinea_p = $('#ingmerc_flr_transportista').val();
        var cliente = $('#ingmerc_flr_cliente').val();
        var num_contenedor = $('#ingmerc_flr_num_contenedor').val();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MANIFIESTOS_TB/ManifiestoConsulta?manifiesto_id=' + manifiesto_id + '&tma_manifiesto_p=' + tma_manifiesto_p + '&tma_fch_manifiesto_p=' + tma_fch_manifiesto_p + '&tma_aer_naerolinea_p=' + tma_aer_naerolinea_p + '&cliente=' + cliente + '&num_contenedor=' + num_contenedor,
            success: function (data) {
                //Obtenemos la plantilla
                var templateText = $("#ingresoMerc_table-template").html();
                //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                var tableTemplate = Handlebars.compile(templateText);
                $("#ingresoMerc_tablebody").html('');
                $.each(data, function (index, elem) {
                    $("#ingresoMerc_tablebody").append(tableTemplate(elem));
                });
            },
            error: function (data) {
                swal({
                    title: "Manifiesto",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
            }
        });
    },

    //Limpia todos los campos
    limpiar: function () {
        principal.resetAllFields('frm_ingresoMerc');
        var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd/MM/yyyy');
        $("#TMA_FCH_MANIFIESTO").val(todayDate);
        $("#TMA_FCH_INGRESO").val(todayDate);
        $("#TMA_FUN_FUNCIONARIO").data("kendoComboBox").value("");
        $("#TMA_PTO_PUERTO").data("kendoComboBox").value("");
        $("#TMA_ADU_ENVIA").data("kendoComboBox").value("");
        $("#TMA_ADU_RECIBE").data("kendoComboBox").value("");
        $('#btn_conocimientos').hide();

        if (!$('#frm_ingresoMerc').hasClass('fullHidden'))
            $('#btn_guardar_ingresoMerc').show();

        $('#btn_editar_ingresoMerc').hide();
        var ddl = $('#TMA_CLIENTE_ID').data("kendoComboBox");
        if (ddl != undefined)
            ddl.setDataSource([]);
        var ddl1 = $('#TMA_AER_NAEROLINEA').data("kendoComboBox");
        if (ddl1 != undefined)
            ddl1.setDataSource([]);
        mercancias_consultas.limpiaFiltros();
        manifiesto_duas.limpiar();
        manifiesto_viaje.limpiar();
    },

    //Filtra si se escribe mas de un caracter o si no existe ninguno
    filtrar: function (input) {
        if (input.value.length > 1) {
            mercancias_consultas.buscar();
        } else {
            if (input.value.length === 0) {
                mercancias_consultas.buscar();
            }
        }
    },

    //Limpia los filtros
    limpiaFiltros: function () {
        $('#ingmerc_flr_manifiesto').val('');
        $('#ingmerc_flr_fecha').val('');
        $('#ingmerc_flr_transportista').val('');
        $('#ingmerc_flr_cliente').val('');
        $('#ingmerc_flr_num_contenedor').val('');
        mercancias_consultas.buscar();
    },

    //Si está en el listar regresa al form
    regresar: function () {
        if (!$('#tbl_ingresoMerc').hasClass('fullHidden'))
            $('#tbl_ingresoMerc').addClass('fullHidden')
        if (!$('#div_ingresoMerc_filtros').hasClass('fullHidden'))
            $('#div_ingresoMerc_filtros').addClass('fullHidden')
        if ($('#frm_ingresoMerc').hasClass('fullHidden'))
            $('#frm_ingresoMerc').removeClass('fullHidden')
        $('#btn_listar_ingresoMerc').show();
        $('#btn_guardar_ingresoMerc').show();
        $('#btn_editar_ingresoMerc').hide();
        $('#btn_regresar_ingresoMerc').hide();
    },

    //Busca el manifiesto y lo carga, esto para la edicion
    buscarMercancia: function (p_identificador) {
        principal.resetAllFields('frm_ingresoMerc');
        var tma_manifiesto_p = '';
        var tma_fch_manifiesto_p = '';
        var tma_aer_naerolinea_p = null;
        var cliente = '';
        var num_contenedor = '';
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MANIFIESTOS_TB/ManifiestoConsulta?manifiesto_id=' + p_identificador + '&tma_manifiesto_p=' + tma_manifiesto_p + '&tma_fch_manifiesto_p=' + tma_fch_manifiesto_p + '&tma_aer_naerolinea_p=' + tma_aer_naerolinea_p + '&cliente=' + cliente + '&num_contenedor=' + num_contenedor,
            success: function (data) {
                $.each(data[0], function (i, v) {
                    if (i == 'TMA_AER_NAEROLINEA') {
                        mercancias_consultas.codigo_aerolinea = v;
                    }
                    if (i == 'TMA_CLIENTE_ID') {
                        mercancias_consultas.codigo_cliente = v;
                    }
                    if (i == 'TMA_CLIENTE_TIPO') {
                        mercancias_consultas.getClient(mercancias_consultas.tipo_cliente, v, mercancias_consultas.codigo_cliente);
                    }
                    if (i == 'TMA_CLIENTE_NOMBRE') {
                        mercancias_consultas.tipo_cliente = v;
                    }
                    if (i == 'AEROLINEA') {
                        mercancias_consultas.aerolinea = v;
                    }
                    if (i == 'TMA_MOD_TRANSPORTE') {
                        mercancias_consultas.getTransportistas(v, mercancias_consultas.aerolinea, mercancias_consultas.codigo_aerolinea)
                    }
                    principal.setValueByName(i, v, 'frm_ingresoMerc');
                });
                //$('#TMA_MOD_TRANSPORTE').change(function(){
                Cookies.set('tipo_transporte', $('#TMA_MOD_TRANSPORTE').val());
            },
            error: function (data) {
                swal({
                    title: "Contenedor/Manifiesto",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                });
            }
        });
    },

    //Hace el cambio de navegacion de botones y la busqueda del manifiesto o 
    editar: function (id) {
        mercancias_consultas.limpiar();
        Cookies.set('manifiesto', id);
        mercancias_consultas.buscarMercancia(id);
        manifiesto_viaje.buscarViajes(id, 'consulta');
        manifiesto_duas.buscarDuas(id, 'consulta');
        if (!$('#tbl_ingresoMerc').hasClass('fullHidden'))
            $('#tbl_ingresoMerc').addClass('fullHidden')
        if (!$('#div_ingresoMerc_filtros').hasClass('fullHidden'))
            $('#div_ingresoMerc_filtros').addClass('fullHidden')
        if ($('#frm_ingresoMerc').hasClass('fullHidden'))
            $('#frm_ingresoMerc').removeClass('fullHidden')
        $('#btn_listar_ingresoMerc').show();
        $('#btn_guardar_ingresoMerc').hide();
        $('#btn_editar_ingresoMerc').show();
        $('#btn_conocimientos').show();
        $('#btn_regresar_ingresoMerc').hide();
        if ($('#btn_editar_ingresoMerc').hasClass('fullHidden'))
            $('#btn_editar_ingresoMerc').removeClass('fullHidden')
        principal.resetAllFields('div_ingresoMerc_filtros');
    },

    redireccionar: function () {
        window.location.href = "Conocimientos_Consulta_Copy?id=" + $("#TMA_MANIFIESTO").val();
    }

};
$(document).ready(function () {
    mercancias_consultas.init();
});