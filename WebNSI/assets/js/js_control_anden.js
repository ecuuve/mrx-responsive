﻿var control_anden = {

    init: function () {
     control_anden.getAnden();
     control_anden.getAnden2();
     control_anden.getTiposCamion();
     control_anden.getMotivosAnula();
     control_anden.CAD_ListarFacturasPendientes();
     control_anden.getColas();
     //control_anden.getTiposCamion();
     // control_anden.CIA_Registros();


      $('#facspend-tab').on('click', function () { 
            $('#btn_agregarfacturas').show();
            control_anden.CAD_ListarFacturasPendientes();
        });  


      $('#controlAnden-tab').on('click', function () { 
            $('#btn_agregarfacturas').hide();
        });  


      $('#CONTROL_ANDEN2').change(function(){
            var anden = $('#CONTROL_ANDEN2').val();
            var cola = $('#COLAS').val();
            control_anden.ListarAnden(anden,cola);
        });

      $('#COLAS').change(function(){
            var anden = $('#CONTROL_ANDEN2').val();
            var cola = $('#COLAS').val();
            control_anden.ListarAnden(anden,cola);
        });

    },

    //Lista los andén facturas pendientes
    getAnden: function () {
        principal.getData("/api/TX_CONTROL_ANDEN_TB/GetControlAndenLV",function (data) {$("#CONTROL_ANDEN").html(principal.arrayToOptions(data));});
    },

    //Lista los andén
    getAnden2: function () {
        principal.getData("/api/TX_CONTROL_ANDEN_TB/GetControlAndenLV",function (data) {$("#CONTROL_ANDEN2").html(principal.arrayToOptions(data));});
    },

     //Lista las facturas pendientes 
    CAD_ListarFacturasPendientes: function(){
        //Obtenemos la plantilla
        $('#th_AndenFacturas').show();
        $("#th_AndenFacturas").attr('style', 'display:block');
        var templateText = $("#AndenFacturas_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#AndenFacturas_table1body").html('');
        $.getJSON('/api/TX_CONTROL_ANDEN_TB/GetFactPendientes').then(function (data) {
            $.each(data, function (index, elem) {
                $("#AndenFacturas_table1body").append(tableTemplate(elem));
            })
        });
    },

    //Lista las colas de control andén
    getColas: function () {
        principal.getData("/api/TX_CONTROL_ANDEN_TB/GetColas",function (data) {$("#COLAS").html(principal.arrayToOptionsSelected(data,'2'));});
    },

    //Método que recorre la tabla para verificar los check y agregar facturas
    CAD_AgregarFacturas: function(){
        var fac = '';
        var row = '';
        $("#AndenFacturas_Table tbody tr").each(function(i,e){ 
            fac = $(this).find("td").eq(0).html();
            row = $(this).find("input[id=chk_" + fac + "]");


            if (row.prop("checked") == true){
                //Agrego la factura al andén seleccionado
                var anden = $("#CONTROL_ANDEN").val();
                var user = $("#UsReg").val();
                control_anden.CAD_AgregaFacturaAnden(fac,anden,user);

                // swal({
                //      title: "Control Andén",
                //      text: "Factura: " + fac + ", está checkeada",
                //      icon: "success",
                //      button: "OK!",
                //      });

            }           
        });       
    },

    //Agrega facturas al andén seleccionado
    CAD_AgregaFacturaAnden: function (fac_p, anden_p, user_p){
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_CONTROL_ANDEN_TB/InsertaControlAnden?factura_p=' + fac_p + '&anden_p=' + anden_p + '&user_MERX=' + user_p,
            success: function (data) {
                if(data != null){
                    swal({
                         title: "Control Andén",
                         text: "Factura(s) agregadas al andén: " +anden_p,
                         icon: "success",
                         button: "OK!",
                         }).then((value) => {
                           //Actualizo la lista de fact pendientes.
                            control_anden.CAD_ListarFacturasPendientes(); 
                           
                            });
                }
            },
            failure: function (data) {
              console.log("fail");
            },
            error: function (data) {
                swal({
                  title: "Control Andén",
                  text: data.responseJSON.Message,
                  icon: "error",
                  button: "OK!",

                });
            }
        });
    },

    //Lista según en anden y la cola
    ListarAnden: function (anden_p, cola_p) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_CONTROL_ANDEN_TB/GetControlAnden?anden_p='+ anden_p + "&cola_p=" + cola_p,
            success: function (data) {
                if(data != null){
                    var templateText = $("#EnColaAsig_table-template").html();
                    //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                    var tableTemplate = Handlebars.compile(templateText);
                    $("#EnColaAsig_tablebody").html('');
                    $.each(data, function (index, elem) {
                        $("#EnColaAsig_tablebody").append(tableTemplate(elem));
                    });
                }
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Control Andén",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
            }
        });
    },


    //Lista los tipos camión
    getTiposCamion: function () {
        principal.getData("/api/TX_CONTROL_ANDEN_TB/GetTiposCamion",function (data) {$("#S_TIPOCAMION").html(principal.arrayToOptions(data));});
    },

    //Llama al método que muestra modal para editar tipo camión
    EditarTipoCamion: function (placa_p, tipo_p) {
        $("#PLACA").val("");
        $("#S_TIPOCAMION").val("");

        $("#myModal_TipoCam").modal('show');
        $("#PLACA").val(placa_p);
        if(tipo_p != ""){
            $("#S_TIPOCAMION").val(tipo_p);
        }       
        principal.activeLabels();  
        $('#lbl_control_anden').removeClass('active');
        $('#lbl_CONTROL_ANDEN2').removeClass('active');
        $('#lbl_colas').removeClass('active');

    },

    //Actualiza el tipo camión 
    ActualizarTipoCamion: function () {
        var placa_p = $("#PLACA").val();
        var tipo_p =  $("#S_TIPOCAMION").val();
        var user_p = $("#UsReg").val();
          $.ajax({
              url: '/api/TX_CONTROL_ANDEN_TB/ActualizaTipoCamion?placa_p=' + placa_p + '&tipo_camion_p=' + tipo_p + '&userMERX_p=' + user_p,
              type: 'POST',
              dataType: 'json',
              //data: JSON.stringify(data),
              contentType: "application/json",
              success: function (data) {
                  swal({
                  title: "Tipo Camión",
                  text: "Se actualizó correctamente la información",
                  icon: "success",
                  button: "OK!",

                  }).then((value) => {
                      $("#myModal_TipoCam").modal('hide');
                        var anden = $('#CONTROL_ANDEN2').val();
                        var cola = $('#COLAS').val();
                        control_anden.ListarAnden(anden,cola);
                  });
              },
              error: function (data) {
                  swal({
                  title: "Tipo Camión",
                  text: data.responseJSON.Message,
                  icon: "error",
                  button: "OK!",
                })
              }
          })
    },

    //Llama al método que muestra modal para editar teléfono
    EditarTelefono: function (placa_p, tel_p) {
        $("#PLACATEL").val("");
        $("#TELEFONO").val("");

        $("#myModal_Telefono").modal('show');
        $("#PLACATEL").val(placa_p);
        if(tel_p != "Agregar Teléfono"){
            $("#TELEFONO").val(tel_p);
        }
                  
        principal.activeLabels(); 
        $('#lbl_control_anden').removeClass('active');
        $('#lbl_CONTROL_ANDEN2').removeClass('active');
        $('#lbl_colas').removeClass('active');   
    },

    //Actualiza Telefono
    ActualizarTelefono: function () {
        var placa_p = $("#PLACATEL").val();
        var tipo_p =  $("#TELEFONO").val();
        var user_p = $("#UsReg").val();
      $.ajax({
          url: '/api/TX_CONTROL_ANDEN_TB/ActualizaTelefonoControlAnden?placa_p=' + placa_p + '&telefono_p=' + tipo_p + '&userMERX_p=' + user_p,
          type: 'POST',
          dataType: 'json',
          //data: JSON.stringify(data),
          contentType: "application/json",
          success: function (data) {
              swal({
              title: "Teléfono",
              text: "Se actualizó correctamente la información",
              icon: "success",
              button: "OK!",

              }).then((value) => {
                  $("#myModal_Telefono").modal('hide');
                    var anden = $('#CONTROL_ANDEN2').val();
                    var cola = $('#COLAS').val();
                    control_anden.ListarAnden(anden,cola);
              });
          },
          error: function (data) {
              swal({
              title: "Teléfono",
              text: data.responseJSON.Message,
              icon: "error",
              button: "OK!",
            })
          }
      })
    },

     //Llama al método que muestra modal para editar posicion anden
    EditarPosicionAnden: function (placa_p, pos_p) {
        $("#PLACANDEN").val("");
        $("#POSICION").val("");

        $("#myModal_Anden").modal('show');
        $("#PLACANDEN").val(placa_p);
        $("#POSICION").val(pos_p);

        principal.activeLabels(); 
        $('#lbl_control_anden').removeClass('active');
        $('#lbl_CONTROL_ANDEN2').removeClass('active');
        $('#lbl_colas').removeClass('active');   
    },

    //Actualiza Posicion Anden
    ActualizarPosicionAnden: function () {
        var placa_p = $("#PLACANDEN").val();
        var pos_p =  $("#POSICION").val();
        var user_p = $("#UsReg").val();
      $.ajax({
          url: '/api/TX_CONTROL_ANDEN_TB/ActualizaPosicCamionAnden?placa_p=' + placa_p + '&posicion_p=' + pos_p + '&userMERX_p=' + user_p,
          type: 'POST',
          dataType: 'json',
          //data: JSON.stringify(data),
          contentType: "application/json",
          success: function (data) {
              swal({
              title: "Posición Camión",
              text: "Se actualizó correctamente la información",
              icon: "success",
              button: "OK!",

              }).then((value) => {
                  $("#myModal_Anden").modal('hide');
                    var anden = $('#CONTROL_ANDEN2').val();
                    var cola = $('#COLAS').val();
                    control_anden.ListarAnden(anden,cola);
              });
          },
          error: function (data) {
              swal({
              title: "Posición Camión",
              text: data.responseJSON.Message,
              icon: "error",
              button: "OK!",
            })
          }
      })
    },

     //Ejecuta la accion correspondiente de la cola andén
    ActualizarEstado: function (placa_p, accion_p,tipo_camion_p) {
       var user_p = $("#UsReg").val();
      $.ajax({
          url: '/api/TX_CONTROL_ANDEN_TB/ActualizaControlAnden?placa_p=' + placa_p + '&accion_p=' + accion_p + '&tipo_camion_p=' + tipo_camion_p +'&userMERX_p=' + user_p,
          type: 'POST',
          dataType: 'json',
          //data: JSON.stringify(data),
          contentType: "application/json",
          success: function (data) {
              swal({
              title: "Control Andén",
              text: "Acción ejecutada correctamente",
              icon: "success",
              button: "OK!",

              }).then((value) => {
                    var anden = $('#CONTROL_ANDEN2').val();
                    var cola = $('#COLAS').val();
                    control_anden.ListarAnden(anden,cola);
              });
          },
          error: function (data) {
              swal({
              title: "Control Andén",
              text: data.responseJSON.Message,
              icon: "error",
              button: "OK!",
            })
          }
      })
    },


     //Lista los motivos de anulación 
    getMotivosAnula: function () {
        principal.getData("/api/TX_CONTROL_ANDEN_TB/GetMotivosAnula",function (data) {$("#S_MOTIVO").html(principal.arrayToOptions(data));});
    },

     //Llama al método que muestra modal para anular
    Anular: function (placa_p) {
        $("#PLACANULAR").val("");
        $("#S_MOTIVO").val("");

        $("#myModal_Anular").modal('show');
        $("#PLACANULAR").val(placa_p);
        

        principal.activeLabels();  
        $('#lbl_control_anden').removeClass('active');
        $('#lbl_CONTROL_ANDEN2').removeClass('active');
        $('#lbl_colas').removeClass('active');  
    },

    //Ejecuta la acción de anular 
    EjecutaAnular: function(){
        var placa_p = $("#PLACANULAR").val();
        var mot_anulacion_p =  $("#S_MOTIVO").val();
        var user_p = $("#UsReg").val();
      $.ajax({
          url: '/api/TX_CONTROL_ANDEN_TB/Anular?placa_p=' + placa_p + '&mot_anulacion_p=' + mot_anulacion_p  +'&userMERX_p=' + user_p,
          type: 'POST',
          dataType: 'json',
          //data: JSON.stringify(data),
          contentType: "application/json",
          success: function (data) {
              swal({
              title: "Control Andén",
              text: "Anulación Exitosa",
              icon: "success",
              button: "OK!",

              }).then((value) => {
                  $("#myModal_Anular").modal('hide');
                    var anden = $('#CONTROL_ANDEN2').val();
                    var cola = $('#COLAS').val();
                    control_anden.ListarAnden(anden,cola);
              });
          },
          error: function (data) {
              swal({
              title: "Control Andén - Anulación",
              text: data.responseJSON.Message,
              icon: "error",
              button: "OK!",
            })
          }
      })
    },

    //Muestra modal de facturas 
    Facturas: function(placa_p, cliente_p, clienteid_p){
        $("#PLACAFAC").val("");
        $("#CLIENTEFAC").val("");
        $("#myModal_Facturas").modal('show');

        $("#PLACAFAC").val(placa_p);
        $("#CLIENTEFAC").val(cliente_p);

        principal.activeLabels();  
        $('#lbl_control_anden').removeClass('active');
        $('#lbl_CONTROL_ANDEN2').removeClass('active');
        $('#lbl_colas').removeClass('active');

        control_anden.ListarFacturas(placa_p,clienteid_p);
    },

    //Lista facturas por placa y cliente
    ListarFacturas: function (placa_p,clienteid_p) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_CONTROL_ANDEN_TB/GetFactPorPlaca?placa_p='+ placa_p + '&cliente_p=' + clienteid_p,// + "&cola_p=" + cola_p,
            success: function (data) {
                if(data != null){
                    var templateText = $("#Facs_table-template").html();
                    //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                    var tableTemplate = Handlebars.compile(templateText);
                    $("#Facs_tablebody").html('');
                    $.each(data, function (index, elem) {
                        $("#Facs_tablebody").append(tableTemplate(elem));
                    });
                }
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Control Andén - Facturas",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
            }
        });
    }

};

$(document).ready(function () {
    control_anden.init();
})