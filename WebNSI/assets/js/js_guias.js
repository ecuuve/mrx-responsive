﻿var guia = {

    init: function () {
         $('#MGU_TablaGuias').hide();
         $('#div_filters').hide();
         TecladoFuncionalidadPersonalizada();
        guia.BuscarGuiaEnter();
        guia.listenerFiltersGuias('txt_filtro_indic_rfr', 's_guia_indic_refri');
        guia.listenerFiltersGuias('txt_filtro_ubc_ing', 's_guia_ubiOrigen');
        guia.listenerFiltersGuias('txt_filtro_ubc_des', 's_guia_ubidestino');
        guia.listenerFiltersGuias('txt_filtro_duda_rfr', 's_guia_duda_refr');
        guia.listenerFiltersGuias('txt_filtro_docsia', 's_guia_docsia');
        guia.listenerFiltersGuias('txt_filtro_doctica', 's_mgu_doctica');
        if($('#in_mgu_guia').val() == ""){
           guia.cambiarEstadoBotones(false, false, false, true, false, true, false, true);
        }else{
            guia.cambiarEstadoBotones(false, false, true, false, false, true, false, false)
        }
        principal.DefaultDateHour();
        guia.cargarcontrolesGuia();
        guia.limpiarCamposGuia();
         guia.ValidacionesEnPantalla(); 

        if ($('#in_mgu_manifiesto').val() !== "") {
            $("#in_mgu_manifiesto").attr('disabled', true);
             $('#lbl_mgu_guia').addClass('active');
             $('#in_mgu_guiaoriginal').focus();
             $("#s_mgu_estado").attr('disabled', true);
        }
        if ($('#in_mgu_guia').val() !== "" && $('#in_mgu_manifiesto').val() !== "") {
            guia.BuscarGuia($("#in_mgu_guia").val(), $("#in_mgu_manifiesto").val());
            $("#in_mgu_guia").attr('disabled', true);
            $("#in_mgu_manifiesto").attr('disabled', true);
             $('#lbl_mgu_guiatica').addClass('active');
            $("#s_mgu_estado").attr('disabled', false);
            $('#in_mgu_guiaoriginal').focus();
        }

       

    },

// Validaciones en pantalla

    ValidacionesEnPantalla: function(){
        $('#in_mgu_boleta').change(function () {
            guia.ValidacionesBoleta($('#in_mgu_boleta').val(),$('#s_guia_clasificacion').val());
        });
        $('#s_guia_clasificacion').change(function () {
            guia.ValidacionesBoleta($('#in_mgu_boleta').val(),$('#s_guia_clasificacion').val());
        });
        $('#in_mgu_guiamaster').change(function () {
            guia.ValidacionesGuiaMaster($('#in_tco_con_consolidadorguia').val(),$('#in_mgu_guiamaster').val());
            guia.Prealerta($('#in_mgu_guiamaster').val());
            guia.Prebloqueo($('#in_mgu_guiamaster').val());
        });
        $('#in_mgu_guiaoriginal').change(function () {
            guia.Prealerta($('#in_mgu_guiaoriginal').val());
            guia.Prebloqueo($('#in_mgu_guiaoriginal').val());
        });

        $('#in_mgu_guiatica').change(function () {
            guia.ValidaGuia($('#in_mgu_guiatica').val());
            guia.AlertasAuto($('#in_mgu_guiatica').val());
            
        });
    },


    ValidacionesGuiaMaster: function(consolidador,guiaMaster){     
        if(guiaMaster.length != 11 ){
            swal({
                title: "Guías",
                text: "La guía master debe ser de 11 caracteres.",
                icon: "info",
                button: "OK!",
                timer: 5000,
            }) 
            $('#in_mgu_guiamaster').focus();
        }

        if(guiaMaster != "" && consolidador == 999){
            swal({
                title: "Guías",
                text: "Si digita guía máster el consolidador NO puede ser 999.",
                icon: "info",
                button: "OK!",
                timer: 5000,
            }) 
            $('#in_mgu_guiamaster').focus();
        }

        if(guiaMaster == "" && consolidador != "" && consolidador != 999){
            swal({
                title: "Guías",
                text: "Si la carga es consolidada debe indicar guía master.",
                icon: "info",
                button: "OK!",
                timer: 5000,
            }) 
            $('#in_mgu_guiamaster').focus();
        }
    },

    ValidacionesBoleta: function(boleta,clase){     
        if(clase == "TRANSITO" && boleta == ""){
            swal({
                title: "Guías",
                text: "Si la clase es Tránsito debe indicar número de Boleta.",
                icon: "info",
                button: "OK!",               
            }) 
            $('#in_mgu_boleta').focus();
        }
    },


// Clase para manejar el focus en el camp
    onfocus: function () {
        if ($("#lbl_mgu_guiaoriginal").hasClass('lblfocus'))
            $("#lbl_mgu_guiaoriginal").removeClass('lblfocus');
    },

//**** CARGA DE CONTROLES****/

    //Llama todos los métodos de carga
    cargarcontrolesGuia: function () {
        guia.getDocumentosSia();
        guia.CargaFechasValidas();
        guia.getTransportistas();
        guia.getPuertosTotal();
        guia.getEstados();
        guia.getDocumentosTica();
        guia.getClases();
        guia.getDudaRefri();
        guia.getIndicacionRefri();
        guia.getUbicacionOrigen();
        guia.getUbicacionTransito();
    },

    // Carga el rango de fechas validas para una guía
    CargaFechasValidas: function(){
        var today = new Date();
        var minDate = today.setDate(today.getDate()-31);
        $("#dtp_guia_fechaIngreso").kendoDatePicker({
          format: "dd/MM/yyyy",
          value: new Date(),
          min: new Date(minDate),
          max: new Date(),
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });
        
        $("#dtp_guia_fechaDigitacion").kendoDatePicker({
          format: "dd/MM/yyyy",
          value: new Date(),
          min: new Date(),
          max: new Date(),
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });

        $("#dtp_guia_fechaIngreso").data("kendoDatePicker").enable();
        $("#dtp_guia_fechaDigitacion").data("kendoDatePicker").enable();
    },

    //Obtiene datos de transportista para llenar select
    getTransportistas: function () {
        principal.getData(
            "/api/TX_MANIFIESTOS_TB/Transportista",
            function (data) {
                $("#s_guia_aerolinea").html(principal.arrayToOptions(data));
            }
        )
    },

    //Obtiene datos de puertos para llenar select
    getPuertosTotal: function () {
        principal.getData(
            "/api/TX_MANIFIESTOS_TB/PuertosTotal",
            function (data) {
                $("#s_guia_puerto").html(principal.arrayToOptions(data));
            }
        )
    },

    //Obtiene datos de documentos SIA para llenar select
    getDocumentosSia: function (valor) {
        if (valor === "") {
            valor = "0";
        }
        principal.getData(
            "/api/TX_CONTENEDORES_TB/Documentos",
            function (data) {
                $("#s_guia_docsia").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        )
    },

    //Obtiene datos de documentos TICA para llenar select
    getDocumentosTica: function (valor) {
        if (valor === "") {
            valor = "0";
        }
        principal.getData(
            "/api/TX_ENTRADAS_TB_Ingreso/DocumentosTICA",
            function (data) {
                $("#s_mgu_doctica").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        )
    },

    //Obtiene datos de Estados de guía para llenar select
    getEstados: function () {
        principal.getData(
            "/api/TX_ENTRADAS_TB_Ingreso/Estados",
            function (data) {
                $("#s_guia_estado").html(principal.arrayToOptionsSelected(data, 'A'));
            }
        )
    },

    //Obtiene datos de Clasificacion para llenar select
    getClases: function () {
        principal.getData(
            "/api/TX_ENTRADAS_TB_Ingreso/Clasificacion",
            function (data) {
                $("#s_guia_clasificacion").html(principal.arrayToOptions(data));
            }
        )
    },

    //Obtiene datos de dudas refri para llenar select
    getDudaRefri: function (valor) {
        if (valor==="")
        {
            valor = "0";
        }
        principal.getData(
            "/api/TX_ENTRADAS_TB_Ingreso/DudasRefri",
            function (data) {
                $("#s_guia_duda_refr").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        )
    },

    //Obtiene datos de indicador refrigeración para llenar select
    getIndicacionRefri: function (valor) {
        if (valor === "") {
            valor = "0";
        }
        principal.getData(
            "/api/TX_ENTRADAS_TB_Ingreso/IndicRefri",
            function (data) {
                $("#s_guia_indic_refri").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        )
    },

    //Obtiene datos de ubicación origen para llenar select
    getUbicacionOrigen: function (valor) {
        if (valor==="")
        {
            valor = "0";
        }
        principal.getData(
            "/api/TX_ENTRADAS_TB_Ingreso/Localizaciones",
            function (data) {
                $("#s_guia_ubiOrigen").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        )
    },

    //Obtiene datos de ubicación tránsito para llenar select
    getUbicacionTransito: function (valor) {
        if (valor === "") {
            valor = "0";
        }
        principal.getData(
            "/api/TX_ENTRADAS_TB_Ingreso/Localizaciones",
            function (data) {
                $("#s_guia_ubidestino").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        )
    },



//**** NAVEGACIÓN***//

    //Pasa a la pantalla de Manifiesto 
    pasarManifiesto: function () {
        var manifiesto = $('#in_mgu_manifiesto').val();
        window.location.href = "/Manifiesto/Index/" + manifiesto;
    },

    //Pasa a la pantalla de tarimas 
    pasarTarima: function () {
        var Dato = new Array($('#in_mgu_manifiesto').val(), $('#in_mgu_guia').val(), $('#in_mgu_guiaoriginal').val());

        window.location.href = "/Tarimas/Index/" + Dato;
    },


//**** GRID DE CONSULTA LISTA ****//
    
    //Hace la consulta y carga el grid de guías del manifiesto dado
    MGU_Refrescar: function () {
        principal.hidemenu();
        var p_guiaOrig = "";
        var p_guia = null;
        var p_aerolinea = null;
        var p_manifiesto = $("#in_mgu_manifiesto").val();
        //Obtenemos la plantilla
        $('#MGU_TablaGuias').show();
        $('#div_filters').show();
        $("#MGU_TablaGuias").attr('style', 'display:block');
        $("#detalles-guia").attr('style', 'display:none');
        var templateText = $("#MGU_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#MGU_table1body").html('');
        $.getJSON('/api/TX_ENTRADAS_TB_Ingreso/Guias_Modificar?manifiesto='+p_manifiesto+ '&guiaOrig='+p_guiaOrig+ '&id='+p_guia+ '&aerolinea='+p_aerolinea).then(function (data) {
            $.each(data, function (index, elem) {
                $("#MGU_table1body").append(tableTemplate(elem));
            })
        });

        guia.cambiarEstadoBotones(true, false, true, true, true, false, false, true);

    },

    //Filtra dentro del grid de guías
    BusquedaFiltros: function () {
        principal.filterTable_Input('in_flr_guia', 'MGU_table1', 0);
        principal.filterTable_select('s_flr_consolidador', 'MGU_table1', 3);
    },

    // Vuelve a la pantalla principal form de guías
    Regresar: function () {
        principal.hidemenu();
        $('#div_filters').hide();
        $("#MGU_TablaGuias").attr('style', 'display:none');
        $("#detalles-guia").attr('style', 'display:block');
        if($('#in_mgu_guia').val() == ""){
           guia.cambiarEstadoBotones(false, false, false, true, false, true, false, true);
           $("#in_mgu_guiaoriginal").focus();
        }else{
            guia.cambiarEstadoBotones(false, false, true, false, false, true, false, false)
            guia.BuscarGuia($('#in_mgu_guia').val(), $('#in_mgu_manifiesto').val());
        }   
    },

    //muestra filtros
    FiltrosGuia: function () {
            $('#div_filters').show();
    },

    //Oculta filtros
    OcultarFiltrosGuia: function () {
            $('#div_filters').hide();
    },


//**** OTRAS FUNCIONALIDADES EN PANTALLA ****//


    //Cambia estado de botones según la acción 
    cambiarEstadoBotones: function (regresar, listar, guardar, actualizar, limpiar, filtro, manifiesto, tarima) {
        $('#btn_listar_guia').prop('disabled', listar);
        $('#btn_guardar_guia').prop('disabled', guardar);
        $('#btn_editar_guia').prop('disabled', actualizar);
        $('#btn_limpiar_guia').prop('disabled', limpiar);        
        $('#btn_filters_guia').prop('disabled', filtro);
        $('#btn_manifiesto').prop('disabled', manifiesto);
        $('#btn_tarimas_guia').prop('disabled', tarima);

         if (regresar === true) {
            $("#btn_regresar_guia").show();
        }
        else {
            $("#btn_regresar_guia").hide();
        }
    },

    //Hace el filtro dentro de los selects en la pantalla
    listenerFiltersGuias: function (id_txtbox, id_cmbox) {
        $('#' + id_txtbox).keyup(function () {
            var filter = $(this).val();
            $('select#' + id_cmbox + '>option').each(function () {
                if(/^[a-zA-Z0-9- ]*$/.test(filter) === false) {
                    var text = $(this).text();
                    if (text.indexOf(filter) !== -1) {
                        $(this).show(); $(this).prop('selected', true);
                    }
                    else {
                        $(this).hide();
                    }
                    //  swal({
                    //     title: "Guías",
                    //     text: "El filtro " + filter + " tiene caracteres especiales",
                    //     icon: "error",
                    //     button: "OK!",

                    // });
                }else{
                    var text = $(this).text().toLowerCase();
                    if (text.indexOf(filter) !== -1) {
                        $(this).show(); $(this).prop('selected', true);
                    }
                    else {
                        $(this).hide();
                    }
                }

            });
        });
    },

    //Limpia los campos del formulario de guías
    limpiarCamposGuia: function () {        
        if($('#MGU_GUIA_ID').val() !== "" && $('#in_mgu_guiatica').val() == "" ){
             $('#MGU_GUIA_ID').val($('#in_mgu_guia').val());
             $('#MGU_MANIFIESTO_ID').val($('#in_mgu_manifiesto').val());

             principal.resetAllFields('detInfo_guia');
              principal.deactivateLabels();

             $('#in_mgu_manifiesto').val($('#MGU_MANIFIESTO_ID').val());
             //$('lbl_mgu_manifiesto').addClass('active');
             $('#MGU_MANIFIESTO_ID').val("");
             $('#in_mgu_guia').val($('#MGU_GUIA_ID').val());
             //$('lbl_mgu_guia').addClass('active');
             $('#MGU_GUIA_ID').val("");

             $('#pasoTarima').val("");
        }else{

            $('#MGU_MANIFIESTO_ID').val($('#in_mgu_manifiesto').val());
             principal.resetAllFields('detInfo_guia');
              principal.deactivateLabels();
            $('#in_mgu_manifiesto').val($('#MGU_MANIFIESTO_ID').val());
             $('#lbl_mgu_manifiesto').addClass('active');
            $("#in_mgu_guia").attr('disabled', true);
            //$('#lbl_mgu_guiaoriginal').addClass('active');
            $('#in_mgu_guiaoriginal').focus();

        }
        if($('#in_mgu_guia').val() == ""){
           guia.cambiarEstadoBotones(false, false, false, true, false, true, false, true);
        }

        
        $("#in_mgu_guia").attr('disabled', false);
        $("#in_mgu_guiaoriginal").attr('disabled', false);
        $("#s_guia_estado").attr('disabled', true); 
        var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd/MM/yyyy');
        $("#dtp_guia_fechaIngreso").val(todayDate);
        $("#dtp_guia_fechaDigitacion").val(todayDate);
        $("#dtp_guia_fechaIngreso").data("kendoDatePicker").enable();
        $("#dtp_guia_fechaDigitacion").data("kendoDatePicker").enable();
        $('#in_mgu_guiaoriginal').focus();
    },

    //Muestra la lista de consolidadores en un modal.
    MostrarListaConsolidadores: function(){
        $("#myModalConsolidadoresGuia").modal('show');
        //Obtenemos la plantilla
        var templateText = $("#TCO_Consolida_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#TCO_Consolida_tablebody").html('');
        $.getJSON('/api/TX_CONTENEDORES_TB/Consolidadores').then(function (data) {
            $.each(data, function (index, elem) {
                $("#TCO_Consolida_tablebody").append(tableTemplate(elem));
            })
        });
    },


    Escoger:function(id,nombre){
       $("#in_tco_con_consolidadorguia").val(id);
       $("#in_consolidadorguia").val(nombre);
       $("#myModalConsolidadoresGuia").modal('hide');
       $('#lbl_in_consolidadorguia').addClass('active');
    },


//**** CONSULTAS ****//

//Funcionalidad del enter para buscar 
    BuscarGuiaEnter: function () {
        $('#in_mgu_guia').change(function () {
             guia.BuscarGuia($('#in_mgu_guia').val(), $('#in_mgu_manifiesto').val());
             guia.cambiarEstadoBotones(false, false, true, false, false, true, false, false)

            // guia.MGU_PreAlerta();
        });
    },


    BuscarGuia: function (p_guia,p_manifiesto) {
        var p_guiaOrig = null;
        var p_aerolinea = null;
        if (p_guia !==null||p_manifiesto !== null) {
            var postdata = { manifiesto: p_manifiesto };
            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_ENTRADAS_TB_Ingreso/Guias?manifiesto='+p_manifiesto+ '&guiaOrig='+p_guiaOrig+ '&id='+p_guia+ '&aerolinea='+p_aerolinea,
                data: postdata,
                success: function (data) {
                    if (data !== null) {
                        //trae los datos de la guia
                        $("#MGU_TablaGuias").attr('style', 'display:none');
                        guia.MUG_MuestraGuias(data);
                        $("#in_mgu_guia").attr('disabled', true);
                        $("#in_mgu_guiaoriginal").attr('disabled', true);
                        $("#s_guia_estado").attr('disabled', false);                       
                        $("#dtp_guia_fechaIngreso").data("kendoDatePicker").enable(false);
                        $("#dtp_guia_fechaDigitacion").data("kendoDatePicker").enable(false);
                    }
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    // swal({
                    //     title: "Guías",
                    //     text: "Error Buscando el la Guía " + p_guia + " Manifiesto" + p_manifiesto,
                    //     icon: "error",
                    //     button: "OK!",

                    // });
                    console.log("error consulta guía-manifiesto");
                }
            });
        }
    },


    MUG_MuestraGuias: function (p_guia) {
        for (var campo in p_guia[0]) {
            if(campo === "MGU_PEND_DESCONSOLIDAR"){
                if(p_guia[campo] === "S"){
                    p_guia[0][campo] = true;
                } else{
                    p_guia[0][campo] = false;
                }
            }
            if(campo === "IEN_ES_Courier"){
                if(p_guia[0][campo] === "S"){
                    p_guia[0][campo] = true;
                } else{
                    p_guia[0][campo] = false;
                }
            }
            if(campo === "IEN_Antecesor"){
                if(p_guia[0][campo] === 0){
                    p_guia[0][campo] = "";
                } 
            }
            if(p_guia[0][campo] != null){
                principal.setValueByName(campo, p_guia[0][campo],'frm_guia');
            }
            
        }

         principal.activeLabels();
    },


//**** Edición ****//

    SeleccionaEditar: function (id) {
        var p_guiaOrig = "";
        var p_aerolinea = null;
        var p_manifiesto = "";
        principal.hidemenu();
        guia.limpiarCamposGuia();
        var cb = function () {
            $("#detalles-guia").attr('style', 'display:block');
            $.ajax({
                url: '/api/TX_ENTRADAS_TB_Ingreso/Guias?manifiesto='+p_manifiesto+ '&guiaOrig='+p_guiaOrig+ '&id='+id+ '&aerolinea='+p_aerolinea,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $("#MGU_TablaGuias").attr('style', 'display:none');
                    $('#div_filters').hide();
                    guia.MUG_MuestraGuias(data);
                    $("#in_mgu_guia").attr('disabled', true);
                    $("#in_mgu_guiaoriginal").attr('disabled', true);
                    $("#in_mgu_manifiesto").attr('disabled', true);
                    $("#s_guia_estado").attr('disabled', false);
                    $("#dtp_guia_fechaIngreso").data("kendoDatePicker").enable(false);
                    $("#dtp_guia_fechaDigitacion").data("kendoDatePicker").enable(false);
                },
                error: function (request, message, error) {
                    console.log(data);
                }
            });
            guia.cambiarEstadoBotones(false, false, true, false, false, true, false, false)
        }
        cb();
    },

    Actualizar: function () {
        var id = $('#in_mgu_guia').val();

        if (id.length > 0) {
            $("#in_mgu_guia").attr('disabled', false);
            $("#in_mgu_manifiesto").attr('disabled', false);
            $("#in_mgu_guiaoriginal").attr('disabled', false);
            // $("#dtp_guia_fechaIngreso").data("kendoDatePicker").enable();
            // $("#dtp_guia_fechaDigitacion").data("kendoDatePicker").enable();


            $('[name= "IEN_ES_Courier"]').is(":checked") === true ? $('[name= "IEN_ES_Courier"]').val('S') : $('[name= "IEN_ES_Courier"]').val('N');
            //$('[name= "MGU_PEND_DESCONSOLIDAR"]').is(":checked") === true ? $('[name= "MGU_PEND_DESCONSOLIDAR"]').val('S') : $('[name= "MGU_PEND_DESCONSOLIDAR"]').val('N');
             $("#UserMERX").val($("#UsReg").val());

            var data = principal.jsonForm($('#frm_guia').serializeArray());
            $.ajax({
                url: '/api/TX_ENTRADAS_TB_Ingreso/' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        $("#in_mgu_guia").attr('disabled', true);
                        $("#in_mgu_manifiesto").attr('disabled', true);
                        $("#in_mgu_guiaoriginal").attr('disabled', true);
                        // $("#dtp_guia_fechaIngreso").data("kendoDatePicker").enable(false);
                        // $("#dtp_guia_fechaDigitacion").data("kendoDatePicker").enable(false);
                        swal({
                        title: "Guías",
                        text: data,
                        icon: "error",
                        button: "OK!",                       
                        })
                    }else{
                        $("#in_mgu_guia").attr('disabled', true);
                        $("#in_mgu_manifiesto").attr('disabled', true);
                        $("#in_mgu_guiaoriginal").attr('disabled', true);
                        // $("#dtp_guia_fechaIngreso").data("kendoDatePicker").enable(false);
                        // $("#dtp_guia_fechaDigitacion").data("kendoDatePicker").enable(false);
                        swal({
                            title: "Guías",
                            text: "Guía actualizada exitosamente",
                            icon: "success",
                            button: "OK!",
                            timer: 5000,
                        })
                        
                    }                  
                },
                error: function (data) {
                    swal({
                        title: "Guías",
                        text: "Error Guía no se actualizó",
                        icon: "error",
                        button: "OK!",
                        timer: 5000,
                    })
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                        $("#in_mgu_guia").attr('disabled', true);
                        $("#in_mgu_manifiesto").attr('disabled', true);
                    }
                }
            })
        }
        else {
            swal({
                title: "Guías",
                text: "El Consecutivo no puede ser vacío",
                icon: "error",
                button: "OK!",
                timer: 5000,
            })
        }
    },


//**** Guardar ****//

   MGU_Guardar: function () {
        $('[name= "IEN_ES_Courier"]').is(":checked") === true ? $('[name= "IEN_ES_Courier"]').val('S') : $('[name= "IEN_ES_Courier"]').val('N');

       $("#UserMERX").val($("#UsReg").val());
        $("#in_mgu_manifiesto").attr('disabled', false);
        var data = principal.jsonForm($('#frm_guia').serializeArray());
        $.ajax({
            url: '/api/TX_ENTRADAS_TB_Ingreso',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: "application/json",
            success: function (data) {
                 if (typeof data === 'string' || data instanceof String){
                     swal({
                          title: "Guías",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                 }else{
                    swal({
                    title: "Guías",
                    text: "Guía guardada exitosamente",
                    icon: "success",
                    button: "OK!",
                    timer: 5000,
                    }).then((value) => {
                        guia.limpiarCamposGuia();
                    });
                 }
                
                //guia.cargarcontrolesGuia();
                
            },
            error: function (data) {
                $("#in_mgu_guia").attr('disabled', true);
                $("#in_mgu_manifiesto").attr('disabled', true);
                swal({
                    title: "Guías",
                    text: "Error Guía no se guardó",
                    icon: "error",
                    button: "OK!",
                    timer: 5000,
                })
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                }
            }
        })
    },

// ***** Prealertas ****//
    Prealerta: function (numguia) {
        $.ajax({
            url: '/api/TX_ENTRADAS_TB_Ingreso/Prealerta?guia_p=' + numguia,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (typeof data === 'string' || data instanceof String){
                     swal({
                          title: "Guías",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                }else{
                    if(data.Existe == "S"){
                        swal({
                            title: "Guías",
                            text: "La Guía " + numguia + " tiene una Pre-Alerta",
                            icon: "info",
                            button: "OK!",
                        });
                        /*var url = "http://merxtest/WebPrealerta/WebPrealerta.aspx?guia=" + id;
                        window.open(url);*/
                    }
                    
                   
                }
            },
            error: function (request, message, error) {
                console.log(data);
            }
        });
    },

//***** Prebloqueo ****//

    Prebloqueo: function (numguia) {
        $.ajax({
            url: '/api/TX_ENTRADAS_TB_Ingreso/Prebloqueo?guia_original_p=' + numguia,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (typeof data === 'string' || data instanceof String){
                     if(data != "" && data == "S"){
                        swal({
                        title: "Guías",
                        text: "La Guía " + numguia + " tiene un Pre-Bloqueo",
                        icon: "info",
                        button: "OK!",
                        });
                        /*var url = "http://merxtest/WebPrealerta/WebPrealerta.aspx?guia=" + id;
                        window.open(url);*/
                     }
                     if(data != "" && data != "S" && data != "N"){
                        swal({
                          title: "Guías",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                     }
                     
                }
            },
            error: function (request, message, error) {
                console.log(data);
            }
        });
    },

// ***** Alertas Automáticas ****//
    AlertasAuto: function () {
        $.ajax({
            url: '/api/TX_ENTRADAS_TB_Ingreso/AlertasAut',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (typeof data === 'string' || data instanceof String){
                     swal({
                          title: "Guías",
                          text: data,
                          icon: "info",
                          button: "OK!",
                        })
                }
            },
            error: function (request, message, error) {
                console.log(data);
            }
        });
    },

// **********Existe guía ***********//

    ValidaGuia: function (guiatica) {
        var manifiesto_p = $("#in_mgu_manifiesto").val();
        $.ajax({
            url: '/api/TX_ENTRADAS_TB_Ingreso/ValidaGuia?guia_p=' + guiatica + '&manifiesto_p=' + manifiesto_p,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (typeof data === 'string' || data instanceof String){
                 swal({
                      title: "Guías",
                      text: data,
                      icon: "info",
                      button: "OK!",
                    })
                 $('#in_mgu_guiatica').focus();
                }
            },
            error: function (request, message, error) {
                console.log(data);
            }
        });
    },

//********************************métodos sin revisar*******************************************//

 

    MGU_Limpiar: function () {
        principal.hidemenu();
        guia.limpiarCamposGuia();
        guia.cargarcontrolesGuia();
    },

    MGU_Nuevo: function () {
        guia.limpiarCamposGuia();
        guia.cargarcontrolesGuia();
    },

    MGU_Eliminar: function () {
        var id = $('#in_mgu_guia').val();

        if (id.length > 0) {
            $("#in_mgu_guia").attr('disabled', false);
        }

        $.ajax({
            url: '/api/MX_GUIAS_TB/' + id,
            type: 'DELETE',
            dataType: 'json',
            success: function (data) {
                $("#MGU_TablaGuias").attr('style', 'display:block');
                $("#detalles-guia").attr('style', 'display:none');
                guia.MGU_Refrescar();
                alert("Inactivo");
            },
            error: function (request, message, error) {
                console.log(data);
            }
        });
    }

   

};

$(document).ready(function () {
    guia.init();
})