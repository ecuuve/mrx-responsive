﻿var reclamos = {

    init: function () {
        reclamos.LimpiarCamposReclamaciones();
       reclamos.limpiarFiltrosReclamaciones();
       principal.resetAllFields('detInfo_revision');
       $("#rcl_in_observacion").val("")
        reclamos.RCL_CargaFechas();
        reclamos.RCL_CargarControlesPrevios();
        $("#btn_registrar_queja").attr('style', 'display:block');
        $("#btn_actualizar_queja").attr('style', 'display:none');
        $("#btn_actualizar_queja_seg").attr('style', 'display:none');
        $('#li_seguimiento').hide();
        $('#TabActive').val('Reg');

         
        //Control de cambios en forms
        $("input, select").change(function () {
          var modified = $('#TabActive').val();
          switch(modified) {
            case 'Reg':
            
                $("#Registro_CAMBIO").val('true');
                $("#Seguimiento_CAMBIO").val('false');
                break;
            case 'Seg':
                $("#Registro_CAMBIO").val('false');
                $("#Seguimiento_CAMBIO").val('true');
                break;
            default:
                $("#Registro_CAMBIO").val('false');
                $("#Seguimiento_CAMBIO").val('false');
          }
        });

        $('#seguimiento-tab').on('click', function () { 
             $('#TabActive').val('Seg');
             $("#btn_actualizar_queja").attr('style', 'display:none');
             $("#btn_actualizar_queja_seg").attr('style', 'display:block');
             $("#btn_limpiar_filtros_reclamo").attr('style', 'display:none');             
             reclamos.VerificarModificacion();
        });

        $('#registro-tab').on('click', function () { 
             $('#TabActive').val('Reg');
             if($('#rcl_in_id').val() != ""){
               $("#btn_actualizar_queja").attr('style', 'display:block');
               $("#btn_registrar_queja").attr('style', 'display:none');
             }else{
               $("#btn_actualizar_queja").attr('style', 'display:none');
               $("#btn_registrar_queja").attr('style', 'display:block');
               
             }
            
            $("#btn_actualizar_queja_seg").attr('style', 'display:none');
             $("#btn_limpiar_filtros_reclamo").attr('style', 'display:block');  
             reclamos.VerificarModificacion();
            //alert("cambia");
                        
                // guiaCerradaEdicion.VerificarModificacion();
        });


    },


    onfocus: function () {
        if ($("#lbl_flr_rcl_id").hasClass('lblfocus'))
            $("#lbl_flr_rcl_id").removeClass('lblfocus');
    },

    RCL_CargarControlesPrevios: function () {
        reclamos.RCL_getTipo();
        reclamos.RCL_getTipoCliente();
        reclamos.RCL_getServicio();
        reclamos.RCL_getArea();
    },

    getDatos: function (filtro, cb, api) {
        principal.get(
            api,
            { 'filtro': filtro },
            cb
        )
    },

    //Controles iniciales
    RCL_getTipo: function () {
        principal.getData(
            "/api/TX_RECLAMACIONES_TB/Tipo",
            function (data) {
                $("#s_rcl_tipo").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
                $("#s_flr_rcl_tipo").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        )
    },

    RCL_getTipoCliente: function () {
        principal.getData(
            "/api/TX_RECLAMACIONES_TB/TipoCliente",
            function (data) {
                $("#s_rcl_tipocliente").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
                $("#s_flr_rcl_tipocliente").html(principal.arrayToOptionsSelectedWithDefault(data, 0));

            }
        )
    },

    RCL_getServicio: function () {
        principal.getData(
            "/api/TX_RECLAMACIONES_TB/Servicio",
            function (data) {
                $("#s_rcl_servicio").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        )
    },

    RCL_getArea: function () {
        principal.getData(
            "/api/TX_RECLAMACIONES_TB/Area",
            function (data) {
                $("#s_rcl_area").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        )
    },

    RCL_CargaFechas: function () {
        var today = new Date();
        var minDate = today.setDate(today.getDate() - 1);
        $("#dtp_rcl_fch_queja").kendoDatePicker({
            format: "dd/MM/yyyy",
            value: today,
            dateInput: true,
            // min: new Date(minDate),
            // max: new Date(),
            month: {
                empty: '<span class="k-state-disabled">#=data.value#</span>'
            }
        });
    },

// Procesos de consulta e inserción
    RCL_BuscarReclamo: function(){
       var p_idRegistro = $("#in_flr_rcl_id").val();
       var p_tipo = $("#s_flr_rcl_tipo").val();
       var p_tipo_cliente = $("#s_flr_rcl_tipocliente").val();
       var p_empresa = $("#in_flr_rcl_empresa").val();
       reclamos.RCL_ConsultarDatos(p_idRegistro,p_empresa,p_tipo,p_tipo_cliente);
    },

    RCL_ConsultarDatos:function(p_idRegistro, p_empresa, p_tipo, p_tipo_cliente){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_RECLAMACIONES_TB/Reclamo?id='+p_idRegistro+ '&empresa='+p_empresa+ '&tipo='+p_tipo+'&tipoCliente='+p_tipo_cliente,
            //data: postdata,
            success: function (data) {
                if (data !== null) {
                  
                  if(principal.isArray(data)){

                   reclamos.RCL_Listar(data);

                  }else{
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Consulta quejas",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }
                    else{
                      if(data["RCL_Id"] != 0){
                         //trae los datos de la queja
                         principal.activeLabels();
                         reclamos.RCL_MuestraDatos(data);
                         $("#btn_actualizar_queja").attr('style', 'display:block');
                         $("#btn_registrar_queja").attr('style', 'display:none');                        
                         $('#li_seguimiento').show();

                          $("#Registro_CAMBIO").val('false');
                          $("#Seguimiento_CAMBIO").val('false');

                        reclamos.RCL_CambiarEstadoCampos(true, true, true, true, true);
                         
                      }
                      else{

                        swal({
                          title: "Consulta guías",
                          text: "No existen datos para los valores consultados.",
                          icon: "error",
                          button: "OK!",
                        })

                      }
                    }
                    
                  }                   
                }
            },
            failure: function (data) {
                
                console.log("fail");
            },
            error: function (xhr, textStatus, errorThrown) {
              console.log(textStatus);
                //console.log("error consulta guía");
            }
        });
    },

    RCL_Listar: function (data) {

        // var p_entrada = $("#in_flr_ien_entrada").val();
        // var p_consolidador = $("#in_flr_ien_consolidador").val();
        // var p_guiaOr = $("#in_flr_guia_original").val();
        // var p_aerolinea = $("#in_flr_aerolinea").val();


        //Obtenemos la plantilla
        
        var templateText = $("#RCL_quejas_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#RCL_quejas_tablebody").html('');
         
          //$.getJSON('/api/TX_ENTRADAS_TB/Entrada?id='+p_entrada+'&guia='+p_guiaOr+'&consolidador='+p_consolidador+'&aerolinea='+p_aerolinea).then(function (data) {
            $.each(data, function (index, elem) {
                $("#RCL_quejas_tablebody").append(tableTemplate(elem));
            })
         // });


          // for (var elem in data) {      
          //   $("#ENT_guias_tablebody").append(tableTemplate(elem));
          // }   

           $("#myModalQuejas").modal('show');
    },

    RCL_Escoger:function(prm_id){
       var p_idRegistro = prm_id;
       var p_tipo = $("#s_flr_rcl_tipo").val();
       var p_tipo_cliente = $("#s_flr_rcl_tipocliente").val();
       reclamos.RCL_ConsultarDatos(p_idRegistro,p_tipo,p_tipo_cliente);
       $("#myModalQuejas").modal('hide');
    },

    RCL_GuardarReclamo: function(){        
        $("#USER_MERX").val($("#UsReg").val());
        $.post(
                "/api/TX_RECLAMACIONES_TB/",
                 $('#frm_informacioncliente').serialize(),
                function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Registro de Quejas",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{
                        swal({
                        title: "Registro de Quejas",
                        text: "Queja guardada exitosamente. Id de registro: #" + data.rcl_id_p,
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            reclamos.LimpiarCamposReclamaciones();
                            //$("#in_blo_ien_entrada").attr("disabled", true); 
                        });
                    }
                })
                .fail(function (data) {

                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                });
    },

    RCL_ActualizarReclamo: function(){
      $("#USER_MERX").val($("#UsReg").val());
      $("#rcl_in_id").attr('disabled', false);
      var cambioR = $("#Registro_CAMBIO").val();
      var id = $('#rcl_in_id').val();
      if (id.length > 0) {
          var data = principal.jsonForm($('#frm_informacioncliente').serializeArray());
          $("#rcl_in_id").attr('disabled', true);
          $.ajax({
              url: '/api/TX_RECLAMACIONES_TB/ActualizarReclamo?id=' + id,
              type: 'PUT',
              data: JSON.stringify(data),
              contentType: "application/json",
              success: function (data) {
                  if (typeof data === 'string' || data instanceof String){
                      swal({
                        title: "Actualización de Quejas",
                        text: data,
                        icon: "error",
                        button: "OK!",
                      })
                  }else{
                      swal({
                      title: "Quejas",
                      text: "Se actualizó correctamente la información",
                      icon: "success",
                      button: "OK!",

                      }).then((value) => {
                          if(cambioR == 'false'){
                              reclamos.limpiarFiltrosReclamaciones();
                          }else{
                              //muestra el tab de seg
                              //principal.ActivarTab('seguimiento');
                          }
                      });
                  }
              },
              error: function (data) {
                  if (data.responseJSON.ModelState) {
                      principal.processErrorsPopUp(data.responseJSON.ModelState);
                  }
              }
          })
      }
      else {
          swal({
              title: "Quejas",
              text: "El ID de la queja no puede ser vacío",
              icon: "error",
              button: "OK!",

          })
      }
    },

    RCL_ActualizarReclamoSeg: function(){
      $("#USER_MERXSeg").val($("#UsReg").val());
        var id = $('#rcl_in_id').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_seguimiento').serializeArray());
            $.ajax({
                url: '/api/TX_RECLAMACIONES_TB/ActualizarReclamoSeguimiento?id=' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Actualización de Seguimiento de quejas",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{
                        swal({
                        title: "Seguimiento de quejas",
                        text: "Se actualizó correctamente la información de seguimiento",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            reclamos.limpiarSeguimientoReclamaciones();
                            //$("#in_blo_ien_entrada").attr("disabled", true); 
                        });
                    }

                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Quejas",
                text: "El ID de la queja no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    },

// Activación de campos y funciones especiales
    
    VerificarModificacion: function(){
      var cambioR = $("#Registro_CAMBIO").val();
      var cambioS = $("#Seguimiento_CAMBIO").val();
     

       if (cambioR == 'true')
          {                  
              swal({
                 title: "Quejas",
                 text: "Ha realizado modificaciones en la queja, desea guardar los cambios antes de continuar con el seguimiento?",
                 icon: "warning",
                 buttons: true,
                 dangerMode: false,
              
              }).then((willDelete) => {
                  if (willDelete) {
                      //Click OK
                      //Guardar los cambios de queja y activa tab de seg
                      reclamos.RCL_ActualizarReclamo();
                      $("#Registro_CAMBIO").val('false');
                      $("#Seguimiento_CAMBIO").val('false');

                  } else {
                      //Click Cancelar
                       $("#Registro_CAMBIO").val('false');
                      // $("#Seguimiento_CAMBIO").val('false');
                      principal.ActivarTab('seguimiento');
                  }
              });
          }

          if (cambioS == 'true')
          {
              swal({
                 title: "Quejas",
                 text: "Ha realizado modificaciones en el seguimiento, desea guardar el cambio?",
                 icon: "warning",
                 buttons: true,
                 dangerMode: false,
              
              }).then((willDelete) => {
                  if (willDelete) {
                      //Click OK
                      reclamos.RCL_ActualizarReclamoSeg();
                      $("#Seguimiento_CAMBIO").val('false');

                      

                  } else {
                      //Click Cancelar
                      $("#Seguimiento_CAMBIO").val('false');
                      
                  }
              });
          }
                   
    },

    RCL_MuestraDatos: function (p_queja) {
        for (var campo in p_queja) {           
            
            if(p_queja[campo] != null){
                principal.setValueByName(campo, p_queja[campo],'frm_informacioncliente');
                principal.setValueByName(campo, p_queja[campo],'frm_seguimiento');
            }  
        }      
    },
    
    RCL_CambiarEstadoCampos: function (id, fecha, tipo, tipoCliente, empresa) {
         //$("#rcl_in_id").attr('disabled', id);
         $("#dtp_rcl_fch_queja").attr('disabled', fecha);
         $("#s_rcl_tipo").attr('disabled', tipo);
         $("#s_rcl_tipocliente").attr('disabled', tipoCliente);
         $("#rcl_in_empresa").attr('disabled', empresa);
    },

    LimpiarCamposReclamaciones: function () {
        $('#li_seguimiento').hide();
        principal.resetAllFields('detInfo_Cliente');
       // principal.resetAllFields('container-fluid');
        TecladoFuncionalidadPersonalizada();
        principal.deactivateLabels();
        reclamos.RCL_CargaFechas();
        $("#btn_registrar_queja").attr('style', 'display:block');
        $("#btn_actualizar_queja").attr('style', 'display:none'); 
        $("#Registro_CAMBIO").val('false');
        $("#Seguimiento_CAMBIO").val('false');      
        reclamos.RCL_CambiarEstadoCampos(false, false, false, false, false);
    },

    limpiarFiltrosReclamaciones: function () {
        $("#in_flr_rcl_id").val("");
        $("#in_flr_fch_queja").val("");
        reclamos.LimpiarCamposReclamaciones();
        principal.resetAllFields('detfiltros');
        $("#in_flr_rcl_id").focus();
    },

    limpiarSeguimientoReclamaciones:function(){
       // principal.resetAllFields('detInfo_revision');
       
        $("#Seguimiento_CAMBIO").val('false');
        //reclamos.LimpiarCamposReclamaciones();
        principal.ActivarTab('registro');
    }

};

$(document).ready(function () {
    reclamos.init();
})