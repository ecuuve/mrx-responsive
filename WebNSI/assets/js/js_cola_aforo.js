﻿
var colaAforo = {
    pickedup : null,
    init: function () {
      colaAforo.QUE_limpiar_AR();
      colaAforo.QUE_limpiar_AC();
      

        $('#rack-tab').on('click', function () { 
             $("#MERC_AR_table1body").html("");
             $('#th_mercAR').hide();
            $("#in_flr_que_ien_entradaAR").val("");
        }); 

        $('#carrusel-tab').on('click', function () { 
             $("#MERC_AC_table1body").html("");
             $('#th_mercAC').hide();
            $("#in_flr_que_ien_entradaAC").val("");
        }); 

        $('#in_flr_que_ien_entradaAC').bind("cut copy paste",function(e) {
             e.preventDefault();
         });
        $('#in_flr_que_ien_entradaAR').bind("cut copy paste",function(e) {
             e.preventDefault();
         });      
    },

    QUE_CargaInicialAR:function(){      
        var cb = function(){
            colaAforo.QUE_CheckearColas();
        };
        colaAforo.QUE_Listar_AR(cb);
    },

    QUE_CargaInicialAC:function(){      
        var cb = function(){
            colaAforo.QUE_CheckearColas();
        };
        colaAforo.QUE_Listar_AC(cb);
    },

    QUE_Funcionalidad_Click: function(tipo,consecutivo, linea){  
        if(tipo == 'ColaAforoAC'){
            //Limpia tabla de mercancias y la oculta
             $("#MERC_AC_table1body").html("");
             $('#th_mercAC').hide();
        }else{
            //Limpia tabla de mercancias y la oculta
             $("#MERC_AR_table1body").html("");
             $('#th_mercAR').hide();
        }     

         //regresa al color negro antes de haberlo seleccionado
         if (colaAforo.pickedup != null) {
             colaAforo.pickedup.css( "background-color", "#3C3C3C" );
         }
         //cambia el color de la fila a gris
         $( linea ).css( "background-color", "grey" );
 
        colaAforo.pickedup = $( linea );

          var cb = function(){
              colaAforo.QUE_CheckearTarimas(tipo);
          };

         colaAforo.QUE_Listar_Mercancias(consecutivo,tipo,cb);

         //$('#th_mercAR').show();
    },
  
    QUE_Listar_AC: function(callback){        
          var p_consecutivo = $("#in_flr_que_ien_entradaAC").val();
          var p_tipoCola = "AC";
          if (p_consecutivo == ""){
            p_consecutivo = null;
          }
          
            $.getJSON('/api/TX_COLAS_TB/GetColas?consecutivo='+ p_consecutivo + '&tipoCola=' + p_tipoCola).then(function (data) {             
                if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Colas Aforo Carrusel",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                }else{

                  if(data.length == 0 && p_consecutivo != null){
                    swal({
                          title: "Colas Aforo Carrusel",
                          text: "No existe información en Aforo Carrusel para el consecutivo dado.",
                          icon: "error",
                          button: "OK!",
                        })
                  }else{
                    //Obtenemos la plantilla
                    $('#th_ColaAforoAC').show();
                    $("#th_ColaAforoAC").attr('style', 'display:block');
                    var templateText = $("#QUE_AC_table-template").html();
                    //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                    var tableTemplate = Handlebars.compile(templateText);
                    $("#ColaAforoAC_table1body").html('');
                    $.each(data, function (index, elem) {
                      $("#ColaAforoAC_table1body").append(tableTemplate(elem));
                    })
                    $('#th_mercAC').hide();

                    $("#ColaAforoAC_Table > tbody > tr").on("click", function(e) {
                      if($("#carrusel-tab").hasClass("active")){
                        var consecutivo = $(this).find("td").eq(1).html();
                        colaAforo.QUE_Funcionalidad_Click("ColaAforoAC", consecutivo, $( this ));
                      }
                    });      

                    if(typeof(callback) == "function"){
                        callback();
                    }
                  }

                }

            });         
    },

    QUE_Listar_AR:function(callback){
        var p_consecutivo = $("#in_flr_que_ien_entradaAR").val();
          var p_tipoCola = "AR";
          if (p_consecutivo == ""){
            p_consecutivo = null;
          }
           
            $.getJSON('/api/TX_COLAS_TB/GetColas?consecutivo='+ p_consecutivo + '&tipoCola=' + p_tipoCola).then(function (data) {
                if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Colas Aforo Rack",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                }else{

                  if(data.length == 0 && p_consecutivo != null){
                    swal({
                          title: "Colas Aforo Rack",
                          text: "No existe información en Aforo Rack para el consecutivo dado.",
                          icon: "error",
                          button: "OK!",
                        })
                  }else{
                      //Obtenemos la plantilla
                      $('#th_ColaAforoAR').show();
                      $("#th_ColaAforoAR").attr('style', 'display:block');
                      var templateText = $("#QUE_AR_table-template").html();
                      //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                      var tableTemplate = Handlebars.compile(templateText);
                      $("#ColaAforoAR_table1body").html('');
                      $.each(data, function (index, elem) {
                      $("#ColaAforoAR_table1body").append(tableTemplate(elem));
                      })
                      $('#th_mercAR').hide();

                      $("#ColaAforoAR_Table > tbody > tr").on("click", function(e) {
                        if($("#rack-tab").hasClass("active")){
                          var consecutivo = $(this).find("td").eq(1).html();
                          colaAforo.QUE_Funcionalidad_Click("ColaAforoAR", consecutivo, $( this ));
                        }
                      });  


                      if(typeof(callback) == "function"){
                          callback();
                      }
                  }

                }
            });
    },

    QUE_CheckearColas: function(){
        $('#ColaAforoAR_Table tbody tr').each(function(){
            var accion1 = $(this).find('td span input[id^="rdPrev"]');
            var accion2 = $(this).find('td span input[id^="rdAut"]');

            colaAforo.QUE_RevisaCheck(accion1);
            colaAforo.QUE_RevisaCheck(accion2);
            
        });

        $('#ColaAforoAC_Table tbody tr').each(function(){
            var accion1 = $(this).find('td span input[id^="rdPrev"]');
            var accion2 = $(this).find('td span input[id^="rdAut"]');

            colaAforo.QUE_RevisaCheck(accion1);
            colaAforo.QUE_RevisaCheck(accion2);
            
        });
    },

    QUE_RevisaCheck:function(accion){
        var checkbox = accion.attr('id');

            //console.log(checkbox, $('#' + checkbox).val());
             if(!accion.prop('checked') && $('#' + checkbox).val() == "S"){
                
                 $('#' + checkbox).prop('checked', true);
             }

             if(accion.prop('checked') && $('#' + checkbox).val() == "N"){
                
                 $('#' + checkbox).prop('checked', false);
             }
    },

    QUE_Listar_Mercancias: function(consecutivo_p,tipo, callback){
        if(tipo=="ColaAforoAC"){
            //Obtenemos la plantilla
            $('#th_mercAC').show();
            $("#th_mercAC").attr('style', 'display:block');
            var templateText = $("#MERC_AR_table-template").html();
            //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#MERC_AC_table1body").html('');
            $.getJSON('/api/TX_COLAS_TB/GetMercCola?consecutivo='+ consecutivo_p).then(function (data) {
                $.each(data, function (index, elem) {
                    $("#MERC_AC_table1body").append(tableTemplate(elem));
                })
                if(typeof(callback) == "function"){
                    callback();
                }
            });
        }else{
            //Obtenemos la plantilla
            $('#th_mercAR').show();
            $("#th_mercAR").attr('style', 'display:block');
            var templateText = $("#MERC_AR_table-template").html();
            //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#MERC_AR_table1body").html('');
            $.getJSON('/api/TX_COLAS_TB/GetMercCola?consecutivo='+ consecutivo_p).then(function (data) {
                $.each(data, function (index, elem) {
                    $("#MERC_AR_table1body").append(tableTemplate(elem));
                })
                if(typeof(callback) == "function"){
                    callback();
                }
            });
        }   
    },

    QUE_CheckearTarimas: function(tipo){
        if(tipo=="ColaAforoAC"){
            tipo="AC";
        }else{
            tipo="AR";
        }
        $('#MERC_'+tipo+'_Table tbody tr').each(function(){
            var accion = $(this).find('td span input[type="checkbox"]');
            var checkbox = accion.attr('id');

            if(!accion.prop('checked') && $('#' + checkbox).val() == "S"){
                
                $('#' + checkbox).prop('checked', true);
            }

            if(accion.prop('checked') && $('#' + checkbox).val() == "N"){
                
                $('#' + checkbox).prop('checked', false);
            }
        });
    },

    QUE_Atender: function(linea,consecutivo,tipo){
        var accion = "AT";
        colaAforo.QUE_UP_Atender(linea,consecutivo,tipo,accion);
    },

    QUE_UP_Atender: function(linea,consecutivo,tipo,accion){
        var UserMERX = $('#UsReg').val();
        
            $.ajax({
                url: '/api/TX_COLAS_TB/?linea_p=' + linea + '&consecutivo_p='+ consecutivo + '&tipo_p='+ tipo + '&user_p='+ UserMERX + '&accion_p='+ accion,
                type: 'PUT',
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Atención de Colas Aforo",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{

                        swal({
                        title: "Colas Aforo",
                        text: "Se atendió correctamente la línea.",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            if(tipo=="AR"){
                                colaAforo.QUE_CargaInicialAR();
                                 $('#th_mercAR').hide();
                            }else{
                                colaAforo.QUE_CargaInicialAC();
                                 $('#th_mercAC').hide();
                            }
                            
                        });
                    }  
                },
            })               
    },

    QUE_Finalizar: function(linea,consecutivo,tipo){
       var accion = "FN";
       colaAforo.QUE_UP_Finalizar(linea,consecutivo,tipo,accion);      
    },

    QUE_UP_Finalizar: function(linea,consecutivo,tipo,accion){
        var UserMERX = $('#UsReg').val();
        
            $.ajax({
                url: '/api/TX_COLAS_TB/?linea_p=' + linea + '&consecutivo_p='+ consecutivo + '&tipo_p='+ tipo + '&user_p='+ UserMERX + '&accion_p='+ accion,
                type: 'PUT',
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Finalizar Colas Aforo",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{

                        swal({
                        title: "Colas Aforo",
                        text: "Se finalizó correctamente la línea.",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            if(tipo=="AR"){
                                colaAforo.QUE_CargaInicialAR();
                                $('#th_mercAR').hide();
                            }else{
                                colaAforo.QUE_CargaInicialAC();
                                $('#th_mercAC').hide();
                            }
                            
                        });
                    }  
                },
            })               
    },

    QUE_Terminar: function(linea,consecutivo,tipo){
       var accion = "TM";
       colaAforo.QUE_UP_Terminar(linea,consecutivo,tipo,accion);      
    },

    QUE_UP_Terminar: function(linea,consecutivo,tipo,accion){
        var UserMERX = $('#UsReg').val();
        
            $.ajax({
                url: '/api/TX_COLAS_TB/?linea_p=' + linea + '&consecutivo_p='+ consecutivo + '&tipo_p='+ tipo + '&user_p='+ UserMERX + '&accion_p='+ accion,
                type: 'PUT',
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Terminar Colas Aforo",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{

                        swal({
                        title: "Colas Aforo",
                        text: "Se terminó correctamente la línea.",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            if(tipo=="AR"){
                                $("#in_flr_que_ien_entradaAR").val("");
                                colaAforo.QUE_CargaInicialAR();
                                $('#th_mercAR').hide();
                            }else{
                                $("#in_flr_que_ien_entradaAC").val("");
                                colaAforo.QUE_CargaInicialAC();
                                $('#th_mercAC').hide();
                            }
                            
                        });
                    }  
                },
            })               
    },

    QUE_limpiar_AR: function () {
         $("#in_flr_que_ien_entradaAR").val("");
         $("#ColaAforoAR_table1body").html("");
         $("#MERC_AR_table1body").html("");
         $('#th_mercAR').hide();

         colaAforo.QUE_CargaInicialAR(); 
    },

    QUE_limpiar_AC: function () {
         $("#in_flr_que_ien_entradaAC").val("");
         $("#ColaAforoAC_table1body").html("");
         $("#MERC_AC_table1body").html("");
         $('#th_mercAC').hide();

         colaAforo.QUE_CargaInicialAC();        
    }
};

$(document).ready(function () {
    colaAforo.init();
})