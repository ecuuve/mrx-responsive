var indic_Posterior = {
    
	init: function(){
        indic_Posterior.LimpiarCamposindicPost();
        TecladoFuncionalidadPersonalizada();
		indic_Posterior.Indic_Posterior_Listar();

		$("#in_flr_aum_fch_evento").kendoDatePicker({
          format: "dd/MM/yyyy",
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });

        // Dispara busqueda cuando cambian de fecha
     $('#in_flr_aum_fch_evento').change(function () {
        indic_Posterior.Indic_Posterior_Listar();
      });

     //Disparo busqueda cuando dan enter en consecutivo
      // $('#in_flr_aum_ien_entrada').bind("enterKey",function(e){
      //     indic_Posterior.Indic_Posterior_Listar();
      //   });
      //   $('#in_flr_aum_ien_entrada').keyup(function(e){
      //     if(e.keyCode == 13)
      //     {
      //       $(this).trigger("enterKey");
      //     }
      //   });

         $('#in_flr_aum_ien_entrada').change(function () {
          indic_Posterior.Indic_Posterior_Listar();
        });

        
      //Controla cuando borran o dan delete
      $('#in_flr_aum_ien_entrada').keyup(function(e){
          if(e.keyCode == 8 || e.keyCode == 46)
          {
             var conse = $("#in_flr_aum_ien_entrada").val();

             if(conse == ""){
              indic_Posterior.Indic_Posterior_Listar();
             }
             
          }
        });
	},

    //Lista las alertas
	Indic_Posterior_Listar: function () {
		var p_fecha = $("#in_flr_aum_fch_evento").val();
        var p_entrada = $("#in_flr_aum_ien_entrada").val();

        var templateText = $("#IndPrev-table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#Ind_Prev_body").html('');
        $.getJSON('/api/TX_ALERTAS_AUTOMATICAS_TB/GetAlertas?fch_evento=' + p_fecha + '&ien_entrada=' + p_entrada).then(function (data) {
            $.each(data, function (index, elem) {
                $("#Ind_Prev_body").append(tableTemplate(elem));
            });
        });
    },

    //Limpia pantalla 
    LimpiarCamposindicPost: function () {
     	$('#in_flr_aum_ien_entrada').val('');
     	$('#in_flr_aum_fch_evento').val('');
     	indic_Posterior.Indic_Posterior_Listar();
    },

    //Finaliza alerta automática con el update 
    Finaliza_Alerta_Automatica: function(id){
    	var p_user = $('#UsReg').val();
        $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_ALERTAS_AUTOMATICAS_TB/FinalizarAlerta?id='+id+ '&user=' + p_user,
                success: function (data) {
                    if(data != null){                    	
                    	swal({
	                        title: "Indicación a Posterior",
	                        text: "Alerta finalizada correctamente",
	                        icon: "success",
	                        button: "OK!",

	                    }).then((value) => {
                            indic_Posterior.LimpiarCamposindicPost();
                        });
                    }
                },
                failure: function (data) {
                    swal({
                        title: "Indicación a Posterior",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",
                    });
                },
                error: function (data) {
                    swal({
                        title: "Indicación a Posterior",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",
                    });
                }
            });
    }
};

$(document).ready(function () {
    indic_Posterior.init();
});