var prealerta = {

    CONSECUTIVO_P: null,
    TOT_GUIAS_P : null,
    CONT_GUIAS_P: null,
    USUARIO_P : null,
    P_FILES: "N",
    
    init: function () {
        prealerta.PLR_limpiar();
        TecladoFuncionalidadPersonalizada();
        $("#prealerta_vuelo").attr('disabled', true);
        prealerta.PLR_Listar();
        $('#prealerta_vuelo').focus(function(){
            prealerta.PLR_ConsultarDatos();
        });

         //Disparo evento de consulta de consolidador cuando dan enter
       //  $('#PLR_CON_NCONSOLIDADOR').bind("enterKey",function(e){
       //     prealerta.PLR_NomConsolidador();
       //  });
       //  $('#PLR_CON_NCONSOLIDADOR').keyup(function(e){
       //    if(e.keyCode == 13)
       //    {
       //      $(this).trigger("enterKey");
       //    }
       // });

        $('#PLR_CON_NCONSOLIDADOR').blur(function(){
            prealerta.PLR_NomConsolidador();
        });

    },
    
    //Obtiene datos de prealetas
    PLR_ConsultarDatos: function(){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_PREALERTAS_TB/GetConsultaDatoss?guia='+  $('#PLR_GUIA').val() + '&consolidador=' + $('#PLR_CON_NCONSOLIDADOR').val() ,
            success: function (data) {
                if (data !== null) {
                  
                }
            },
            failure: function (data) {
                
                console.log("fail");
            },
            error: function (data) {

                console.log("error consulta prealertas");
            }
        });
    },

    //Buscar lista de prealertas según filtros
    PLR_Listar: function(){   
        //Obtenemos la plantilla
        var templateText = $("#PLR_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#Prealertas_tablebody").html('');
        $.getJSON('/api/TX_PREALERTAS_TB/GetPrealertas').then(function (data) {
            $.each(data, function (index, elem) {
                $("#Prealertas_tablebody").append(tableTemplate(elem));
            });
        });    
    },

    //Limpia los campos del form que poseen datos
    PLR_limpiar: function () {
        $("#PLR_GUIA").val("");  
        $("#PLR_CON_NCONSOLIDADOR").val(""); 
        $("#NOMBRE_CONSOLIDADOR").val(""); 
        $("#prealerta_vuelo").val(""); 
        $("#prealerta_vuelo").attr('disabled', true);
        $("#NOMBRE_CONSOLIDADOR").attr('disabled', false); 
        $("#PLR_GUIA").focus(); 
        $("#filename").val(""); 
        $("#fileUpload").val(""); 
        $('#lblarchivo').removeClass('active');
    },

    //Obtiene nombre de consolidador
    PLR_NomConsolidador: function(){
        var idcon = $('#PLR_CON_NCONSOLIDADOR').val();
        if(idcon == ""){
            swal({
              title: "Consulta Consolidador",
              text: "Debe digitar el código del consolidador.",
              icon: "warning",
              button: "OK!",
            }).then((willDelete)=> {
                 $('#PLR_CON_NCONSOLIDADOR').focus();
            });
        }else{
           $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_PREALERTAS_TB/GetConsolidador?consolidador=' + idcon,
            success: function (data) {
                if (data !== null) {
                    $('#NOMBRE_CONSOLIDADOR').val(data);
                    $("#NOMBRE_CONSOLIDADOR").attr('disabled', true);
                }
            },
            failure: function (data) {
                
                console.log("fail");
            },
            error: function (data) {

                swal({
                      title: "Error",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                    });
            }
        }); 
        }
        
    },

    //Guarda prearleta
    PLR_Guardar: function(info ){
        var data = new FormData();
        var files = $("#fileUpload").get(0).files;

        if (files.length > 0){
            prealerta.P_FILES = "S";
            //data.append("pdf", files[0]);
            data.append('pdf', files[0]);          
        }

        var info = {
            'VUELO': $('#prealerta_vuelo').val(),
            'CONSECUTIVO': prealerta.CONSECUTIVO_P,
            'GUIA_ORIGINAL': $('#frm_prealerta #PLR_GUIA').val(),
            'TOT_GUIAS': prealerta.TOT_GUIAS_P,
            'CONT_GUIAS': prealerta.CONT_GUIAS_P,
            'USUARIO': prealerta.USUARIO_P,
            'CONSOLIDADOR': $('#PLR_CON_NCONSOLIDADOR').val(),
            'ARCHIVO': prealerta.P_FILES,
            'UserMERX': $('#UsReg').val(), 
        };

        data.append('json', JSON.stringify(info));

        //Se evalua el codigo de guia
         $.ajax({
            type: 'POST',
            //dataType: 'json',
            //contentType: 'application/json',
            contentType: false,
            processData: false,
            url: '/api/TX_PREALERTAS_TB/PostEvaluaGuia',
            data: data,//JSON.stringify(info),
            success: function (data) {
                prealerta.CONSECUTIVO_P = null;
                if (data !== null) {
                    // var files = $("#fileUpload").get(0).files;
                    //  if (files.length > 0){

                        if(data.CONSECUTIVO != '' || data.CONSECUTIVO != null){
                        if(data.TOT_GUIAS == 1){
                            swal({
                              title: "Desea registrar una desconsolidación?",
                              icon: "warning",
                              buttons: true,
                              dangerMode: true,
                            })
                            .then((willDelete) => {
                              if (willDelete) {
                                $("#prealerta_vuelo").attr('disabled', false);
                                $("#prealerta_vuelo").focus();
                                prealerta.CONSECUTIVO_P = data.CONSECUTIVO;
                                prealerta.TOT_GUIAS_P = data.TOT_GUIAS;
                                prealerta.CONT_GUIAS_P = data.CONT_GUIAS;
                                prealerta.USUARIO_P = data.USUARIO;                                
                              } else {
                                prealerta.PLR_limpiar();
                              }
                            });
                        }
                        else{
                            prealerta.PLR_limpiar();
                            $("#prealerta_vuelo").attr('disabled', true);
                            prealerta.CONSECUTIVO_P = null;
                            prealerta.TOT_GUIAS_P = null;
                            prealerta.CONT_GUIAS_P = null;
                            prealerta.USUARIO_P = null;
                             swal({
                                title: "Transacción satisfactoria",
                                text: data,
                                icon: "success",
                                button: "OK!",
                                })
                                .then((willDelete)=> {
                                     prealerta.PLR_Listar();
                                     $("#PLR_GUIA").focus(); 
                                });
                           }
                        }

                     // }
                     // else{
                     //    swal({
                     //    title: "Registro Prealerta",
                     //    text: "Debe seleccionar un archivo .pdf",
                     //    icon: "warning",
                     //    button: "OK!",
                     //  }); 
                     // }
                }
            },
            failure: function (data) {
                prealerta.CONSECUTIVO_P = null;
                console.log("fail");
            },
            error: function (data) {               
                prealerta.CONSECUTIVO_P = null;
                $("#prealerta_vuelo").attr('disabled', true);
                if(data.responseJSON.Message){
                    swal({
                      title: "Error",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                    });
                }

                if(prealerta.TOT_GUIAS_P != null){
                    $("#prealerta_vuelo").attr('disabled', false);
                    $("#prealerta_vuelo").focus(); 
                }
                //prealerta.PLR_limpiar();
            }
        });
    },

    //Agrega desconsolidación 
    PLT_InsertarDesconsolidacion: function(desconsolidador){
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_PREALERTAS_TB/GetEvaluaGuia?guia=' + $('#PLR_GUIA').val() + '&consolidador=' + $('#PLR_CON_NCONSOLIDADOR').val() ,
            success: function (data) {
                if (data !== null) {
                }
            },
            failure: function (data) {
                
                console.log("fail");
            },
            error: function (data) {
                if(data.responseJSON.Message){
                    swal({
                      title: "Error",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                    });
                }
                prealerta.PLR_limpiar();
            }
        });
    },

    //Sube y guarda el archivo y registro de prealerta o desconsolidación
    Upload: function() {
      
        var files = $("#fileUpload").get(0).files;
        // Add the uploaded image content to the form data collection
        if (files.length > 0) {

            swal({
                title: "Registro Prealerta",
                text: "Si cargó archivo",
                icon: "success",
                button: "OK!",
              }).then((value) => {
                  // prealertaweb.PLR_Listar(); 
                  // prealertaweb.PLR_limpiar();               
            });
           //  data.append("pdf", files[0]);
           //  // Make Ajax request with the contentType = false, and procesDate = false
           // $.ajax({
           //      type: "POST",
           //      url: "/api/TX_PREALERTAS_TB/Upload/" + guia,
           //      contentType: false,
           //      processData: false,
           //      data: data,
           //      success: function () {
           //        swal({
           //              title: "Prealerta Web",
           //              text: "Se subió correctamente el archivo",
           //              icon: "success",
           //              button: "OK!",
           //            }).then((value) => {
           //                prealertaweb.PLR_Listar(); 
           //                prealertaweb.PLR_limpiar();               
           //            });
           //      },
           //      error: function (data) {
           //        swal({
           //              title: "Error",
           //              text: data.responseJSON.Message,
           //              icon: "error",
           //              button: "OK!",
           //            });
           //      }
           //  });
        }else{
          swal({
            title: "Registro Prealerta",
            text: "Debe seleccionar un archivo .pdf",
            icon: "warning",
            button: "OK!",
          }); 
        }



      // var data = new FormData();
      // var guia = $("#PLR_GUIA").val();
      // $("#UserWEB").val($("#UsReg").val());
      // var userweb = $("#UserWEB").val();
        
      // if(guia == ""){
      //     swal({
      //           title: "Prealertas Web",
      //           text: "Debe digitar un número de guía.",
      //           icon: "warning",
      //           button: "OK!",
      //         });        
      // }else{
      //   guia = guia+','+ userweb;
      //   var files = $("#fileUpload").get(0).files;

      //   // Add the uploaded image content to the form data collection
      //   if (files.length > 0) {
      //       data.append("pdf", files[0]);
      //       // Make Ajax request with the contentType = false, and procesDate = false
      //      $.ajax({
      //           type: "POST",
      //           url: "/api/TX_PREALERTAS_TB/Upload/" + guia,
      //           contentType: false,
      //           processData: false,
      //           data: data,
      //           success: function () {
      //             swal({
      //                   title: "Prealerta Web",
      //                   text: "Se subió correctamente el archivo",
      //                   icon: "success",
      //                   button: "OK!",
      //                 }).then((value) => {
      //                     prealertaweb.PLR_Listar(); 
      //                     prealertaweb.PLR_limpiar();               
      //                 });
      //           },
      //           error: function (data) {
      //             swal({
      //                   title: "Error",
      //                   text: data.responseJSON.Message,
      //                   icon: "error",
      //                   button: "OK!",
      //                 });
      //           }
      //       });
      //   }else{
      //     swal({
      //       title: "Prealertas Web",
      //       text: "Debe seleccionar un archivo .pdf",
      //       icon: "warning",
      //       button: "OK!",
      //     }); 
      //   }
      // }  
    },

     // Actualiza el nombre del archivo en el campo
    changeInputText:function(){
      var str = $("#fileUpload").val();
      var i;
      if (str.lastIndexOf('\\')) {
        i = str.lastIndexOf('\\') + 1;
      } else if (str.lastIndexOf('/')) {
        i = str.lastIndexOf('/') + 1;
      }
      
      var strtext = str.slice(i, str.length);
      $("#filename").val(strtext);
       $('#lblarchivo').addClass('active');
    }
    
};

$(document).ready(function () {
    prealerta.init();
})