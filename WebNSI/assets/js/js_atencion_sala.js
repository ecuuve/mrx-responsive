﻿var atencionsala = {

    init: function () {

      atencionsala.ATS_Listar_F();
      atencionsala.ATS_Listar_C();
      atencionsala.ATS_Listar_T();
      atencionsala.ATS_Listar_G();
      atencionsala.ATS_Listar_I();
      atencionsala.ATS_Listar_M();
      atencionsala.ATS_Listar_B();
      atencionsala.ATS_Listar_R();
      
       $('#s_permisos').change(function () {
           var usuario = $('#in_flr_nombre').val();
           //if (usuario == ""){
              var opcion = $("#s_permisos").val(); 
              atencionsala.ATS_Listar_Usuarios(usuario,opcion);
           //}
           
        });
      $('#usuarios-tab').on('click', function () { 
          atencionsala.getPermisos();
          var usuario = "";
          var opcion = ""; 
          atencionsala.ATS_Listar_Usuarios(usuario,opcion);
      }); 

      //Controla cuando borran o dan delete
      $('#in_flr_nombre').keyup(function(e){
          if(e.keyCode == 8 || e.keyCode == 46)
          {
             var nombre = $("#in_flr_nombre").val();
             var opcion = $("#s_permisos").val(); 

             if(nombre == "" && opcion == ""){
              atencionsala.ATS_Listar_Usuarios(nombre,opcion);

             }
             if(nombre == "" && opcion != ""){
              atencionsala.ATS_Listar_Usuarios(nombre,opcion);

             }
          }
        });
    },


     //Obtiene datos de permisos para llenar select
    getPermisos: function () {
        principal.getData(
            "/api/TX_ATENCION_SALA_TB/Permisos",
            function (data) {
                $("#s_permisos").html(principal.arrayToOptions(data));
            }
        )
    },

    ATS_Listar_Usuarios: function(usuario_p, opcion_p){        
      $.getJSON('/api/TX_ATENCION_SALA_TB/GetUsuarios?usuario_p='+ usuario_p + '&opcion_p=' + opcion_p).then(function (data) {
           if (typeof data === 'string' || data instanceof String){
                swal({
                  title: "Usuarios Atención Sala",
                  text: data,
                  icon: "error",
                  button: "OK!",
                })
            }else{
              if(data.length == 0){
                if(usuario_p != "" && opcion_p != ""){
                   swal({
                    title: "Usuarios Atención Sala",
                    text: "No existe información del permiso seleccionado para el usuario: " + usuario_p+ ". Puede proceder a agregarlo.",
                    icon: "error",
                    button: "OK!",
                  })
                }else{
                  if(usuario_p != ""){
                    swal({
                      title: "Usuarios Atención Sala",
                      text: "No existe información de permisos para el usuario: " + usuario_p+ ".",
                      icon: "error",
                      button: "OK!",
                    })
                  }

                  if(opcion_p != ""){
                    swal({
                      title: "Usuarios Atención Sala",
                      text: "No existe información de usuarios para el permiso seleccionado.",
                      icon: "error",
                      button: "OK!",
                    })
                  }
                }
              }else{
                var cb = function(){
                  atencionsala.ATS_CheckearUsuarios();
                };
                atencionsala.ATS_CargarTablaUsuarios(data,cb);                 
              }      
            }   
      });            
    },

    ATS_CargarTablaUsuarios: function(data,callback){
      //Obtenemos la plantilla
      var templateText = $("#Usuarios_table-template").html();
      //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
      var tableTemplate = Handlebars.compile(templateText);
      $("#Usuarios_table1body").html('');
      $.each(data, function (index, elem) {
        $("#Usuarios_table1body").append(tableTemplate(elem));
      });
       
       if(typeof(callback) == "function"){
          callback();
        }   
    },

    ATS_CheckearUsuarios: function(){
        $('#Usuarios_Table tbody tr').each(function(){
            var accion = $(this).find('td span input[id^="rdPrior"]');
            atencionsala.ATS_RevisaCheck(accion);            
        });
    },

    UAS_Agregar: function(){
      var nombre = $('#in_flr_nombre').val();
      var permiso = $('#s_permisos').val();
      var prior = "N";
      $.ajax({
          url: '/api/TX_ATENCION_SALA_TB/?usuario_p=' + nombre + '&opcion_p='+ permiso + '&prioridad_p='+ prior,
          type: 'POST',
          contentType: "application/json",
          success: function (data) {
              if (typeof data === 'string' || data instanceof String){
                  swal({
                    title: "Permisos Usuario",
                    text: data,
                    icon: "error",
                    button: "OK!",
                  })
              }else{
                  swal({
                  title: "Permisos Usuario",
                  text: "Se ingresó correctamente la información",
                  icon: "success",
                  button: "OK!",

                  }).then((value) => {
                      var usuario = $("#in_flr_nombre").val();
                      var opcion = ""; 
                      atencionsala.ATS_Listar_Usuarios(usuario,opcion);                         
                  });
              }
          }
      })             
    },

    UAS_Limpiar: function(){
      $("#in_flr_nombre").val("");
      atencionsala.getPermisos(); 
      var usuario = "";
      var opcion = ""; 
      atencionsala.ATS_Listar_Usuarios(usuario,opcion);
    },

    UAS_Filtrar: function(){
      var nombre = $("#in_flr_nombre").val();
      if(nombre.length >= 2){
        principal.filterTable_Input('in_flr_nombre', 'Usuarios_Table', 3);
      }
      // if(nombre.length == 1){
      //   var usuario = "";
      //     var opcion = ""; 
      //     atencionsala.ATS_Listar_Usuarios(usuario,opcion);
      // }
    },

    UAS_Borrar: function(p_usuario, p_opcion, p_desc_opcion){
        swal({
          title: "Está seguro?",
          text: "Eliminará el permiso de: "+p_desc_opcion+" asignado al usuario: "+p_usuario+".",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            atencionsala.UAS_Eliminar(p_usuario,p_opcion);                     
          } 
        });        
    },

    UAS_Eliminar: function(p_usuario,p_opcion){       
      $.ajax({
          url: '/api/TX_ATENCION_SALA_TB/?usuario_p=' + p_usuario + '&opcion_p='+ p_opcion,
          type: 'DELETE',
          contentType: "application/json",
          success: function (data) {
              if (typeof data === 'string' || data instanceof String){
                  swal({
                    title: "Eliminar permisos",
                    text: data,
                    icon: "error",
                    button: "OK!",
                  })
              }else{
                  swal({
                      title: "Permisos Usuario",
                      text: "Se eliminó correctamente la información",
                      icon: "success",
                      button: "OK!",

                      }).then((value) => {
                        $("#in_flr_nombre").val("");
                        atencionsala.getPermisos(); 
                        var usuario = p_usuario;
                        var opcion = ""; 
                        atencionsala.ATS_Listar_Usuarios(usuario,opcion);
                          
                  });
              }
          }
      })       
    },










    ATS_Listar_F: function(){        
        var p_tipoCola = "F";
        $.getJSON('/api/TX_ATENCION_SALA_TB/GetColasAtencion?tipoCola='+ p_tipoCola).then(function (data) {
            if(data.length == 0){
                swal({
                      title: "Atención Sala Facturación",
                      text: "No existe información para Facturación.",
                      icon: "error",
                      button: "OK!",
                    })
            }else{
              var cb = function(){
                atencionsala.ATS_CheckearColas(p_tipoCola);
              };
              atencionsala.ATS_CargarTabla(data,p_tipoCola,cb);                 
            }           
        });            
    },

    ATS_Listar_C: function(){        
      var p_tipoCola = "C";
        $.getJSON('/api/TX_ATENCION_SALA_TB/GetColasAtencion?tipoCola='+ p_tipoCola).then(function (data) {
            if(data.length == 0){
                swal({
                      title: "Atención Sala Contado",
                      text: "No existe información para Contado.",
                      icon: "error",
                      button: "OK!",
                    })
            }else{
              var cb = function(){
               atencionsala.ATS_CheckearColas(p_tipoCola);
              };
              atencionsala.ATS_CargarTabla(data,p_tipoCola,cb);            
            }           
        });             
    },

    ATS_Listar_T: function(){        
      var p_tipoCola = "T";
        $.getJSON('/api/TX_ATENCION_SALA_TB/GetColasAtencion?tipoCola='+ p_tipoCola).then(function (data) {
            if(data.length == 0){
                swal({
                      title: "Atención Trámite Personal",
                      text: "No existe información para Trámite Personal.",
                      icon: "error",
                      button: "OK!",
                    })
            }else{
              var cb = function(){
               atencionsala.ATS_CheckearColas(p_tipoCola);
              };
              atencionsala.ATS_CargarTabla(data,p_tipoCola,cb);            
            }           
        });             
    },

    ATS_Listar_G: function(){        
      var p_tipoCola = "G";
        $.getJSON('/api/TX_ATENCION_SALA_TB/GetColasAtencion?tipoCola='+ p_tipoCola).then(function (data) {
            if(data.length == 0){
                swal({
                      title: "Atención Consultas",
                      text: "No existe información para Consultas.",
                      icon: "error",
                      button: "OK!",
                    })
            }else{
              var cb = function(){
               atencionsala.ATS_CheckearColas(p_tipoCola);
              };
              atencionsala.ATS_CargarTabla(data,p_tipoCola,cb);            
            }           
        });             
    },

    ATS_Listar_I: function(){        
      var p_tipoCola = "I";
        $.getJSON('/api/TX_ATENCION_SALA_TB/GetColasAtencion?tipoCola='+ p_tipoCola).then(function (data) {
            if(data.length == 0){
                swal({
                      title: "Atención Aforo Importador",
                      text: "No existe información para Aforo Importador.",
                      icon: "error",
                      button: "OK!",
                    })
            }else{
              var cb = function(){
               atencionsala.ATS_CheckearColas(p_tipoCola);
              };
              atencionsala.ATS_CargarTabla(data,p_tipoCola,cb);            
            }           
        });             
    },

    ATS_Listar_M: function(){        
      var p_tipoCola = "M";
        $.getJSON('/api/TX_ATENCION_SALA_TB/GetColasAtencion?tipoCola='+ p_tipoCola).then(function (data) {
            if(data.length == 0){
                swal({
                      title: "Atención Muestras",
                      text: "No existe información para Muestras.",
                      icon: "error",
                      button: "OK!",
                    })
            }else{
              var cb = function(){
               atencionsala.ATS_CheckearColas(p_tipoCola);
              };
              atencionsala.ATS_CargarTabla(data,p_tipoCola,cb);            
            }           
        });             
    },

    ATS_Listar_B: function(){        
      var p_tipoCola = "B";
        $.getJSON('/api/TX_ATENCION_SALA_TB/GetColasAtencion?tipoCola='+ p_tipoCola).then(function (data) {
            if(data.length == 0){
                swal({
                      title: "Atención Rev.Externa",
                      text: "No existe información para Rev. Externa.",
                      icon: "error",
                      button: "OK!",
                    })
            }else{
              var cb = function(){
               atencionsala.ATS_CheckearColas(p_tipoCola);
              };
              atencionsala.ATS_CargarTabla(data,p_tipoCola,cb);            
            }           
        });             
    },

    ATS_Listar_R: function(){        
      var p_tipoCola = "R";
        $.getJSON('/api/TX_ATENCION_SALA_TB/GetColasAtencion?tipoCola='+ p_tipoCola).then(function (data) {
            if(data.length == 0){
                swal({
                      title: "R",
                      text: "No existe información para Registro Agente-Asistente.",
                      icon: "error",
                      button: "OK!",
                    })
            }else{
              var cb = function(){
               atencionsala.ATS_CheckearColas(p_tipoCola);
              };
              atencionsala.ATS_CargarTabla(data,p_tipoCola,cb);            
            }           
        });             
    },

    ATS_CargarTabla: function(data,tipoCola,callback){
      //Obtenemos la plantilla
      var templateText = $("#"+tipoCola+"_table-template").html();
      //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
      var tableTemplate = Handlebars.compile(templateText);
      $("#"+tipoCola+"_table1body").html('');
      $.each(data, function (index, elem) {
        $("#"+tipoCola+"_table1body").append(tableTemplate(elem));
      });
       
       if(typeof(callback) == "function"){
          callback();
        }   
    },

    ATS_CheckearColas: function(tipoCola){
        $('#'+tipoCola+'_Table tbody tr').each(function(){
            var accion = $(this).find('td span input[id^="rdPrior"]');
            atencionsala.ATS_RevisaCheck(accion);            
        });
    },

    ATS_RevisaCheck:function(accion){
      var checkbox = accion.attr('id');
         if(!accion.prop('checked') && $('#' + checkbox).val() == "S"){
            
             $('#' + checkbox).prop('checked', true);
         }

         if(accion.prop('checked') && $('#' + checkbox).val() == "N"){
            
             $('#' + checkbox).prop('checked', false);
         }
    },

    ATS_UP_Atender: function(tipoCola, id, prior){
        var UserMERX = $('#UsReg').val();
        var estado = "A";
            $.ajax({
                url: '/api/TX_ATENCION_SALA_TB/?tipo_p=' + tipoCola + '&id_p='+ id + '&estado_p='+ estado + '&prior_p='+ prior + '&usu_p='+ UserMERX,
                type: 'PUT',
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Atención Sala",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{

                        swal({
                        title: "Atención Sala",
                        text: "Se atendió correctamente el tiquete.",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            switch(tipoCola) {
                              case "F":
                                atencionsala.ATS_Listar_F();
                                break;
                              case "C":
                                atencionsala.ATS_Listar_C();
                                break;
                              case "T":
                                atencionsala.ATS_Listar_T();
                                break;
                              case "G":
                                atencionsala.ATS_Listar_G();
                                break;
                              case "I":
                                atencionsala.ATS_Listar_I();
                                break;
                              case "M":
                                atencionsala.ATS_Listar_M();
                                break;
                              case "B":
                                atencionsala.ATS_Listar_B();    
                                break;
                              case "R":
                                atencionsala.ATS_Listar_R();
                                break;
                              default:
                                // code block
                            }
                            
                        });
                    }  
                },
            })               
    },

    ATS_UP_NoPresente: function(tipoCola, id, prior){
        var UserMERX = $('#UsReg').val();
        var estado = "N";
            $.ajax({
                url: '/api/TX_ATENCION_SALA_TB/?tipo_p=' + tipoCola + '&id_p='+ id + '&estado_p='+ estado + '&prior_p='+ prior + '&usu_p='+ UserMERX,
                type: 'PUT',
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Atención Sala No Presente",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{

                        swal({
                        title: "Atención Sala No Presente",
                        text: "Se finalizó correctamente el tiquete.",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            switch(tipoCola) {
                              case "F":
                                atencionsala.ATS_Listar_F();
                                break;
                              case "C":
                                atencionsala.ATS_Listar_C();
                                break;
                              case "T":
                                atencionsala.ATS_Listar_T();
                                break;
                              case "G":
                                atencionsala.ATS_Listar_G();
                                break;
                              case "I":
                                atencionsala.ATS_Listar_I();
                                break;
                              case "M":
                                atencionsala.ATS_Listar_M();
                                break;
                              case "B":
                                atencionsala.ATS_Listar_B();    
                                break;
                              case "R":
                                atencionsala.ATS_Listar_R();
                                break;
                              default:
                                // code block
                            }
                            
                        });
                    }  
                },
            })               
    },

    ATS_UP_Finalizar: function(tipoCola, id, prior){
        var UserMERX = $('#UsReg').val();
        var estado = "F";
            $.ajax({
                url: '/api/TX_ATENCION_SALA_TB/?tipo_p=' + tipoCola + '&id_p='+ id + '&estado_p='+ estado + '&prior_p='+ prior + '&usu_p='+ UserMERX,
                type: 'PUT',
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Atención Sala Finalizar",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{

                        swal({
                        title: "Atención Sala Finalizar",
                        text: "Se finalizó correctamente el tiquete.",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            switch(tipoCola) {
                              case "F":
                                atencionsala.ATS_Listar_F();
                                break;
                              case "C":
                                atencionsala.ATS_Listar_C();
                                break;
                              case "T":
                                atencionsala.ATS_Listar_T();
                                break;
                              case "G":
                                atencionsala.ATS_Listar_G();
                                break;
                              case "I":
                                atencionsala.ATS_Listar_I();
                                break;
                              case "M":
                                atencionsala.ATS_Listar_M();
                                break;
                              case "B":
                                atencionsala.ATS_Listar_B();    
                                break;
                              case "R":
                                atencionsala.ATS_Listar_R();
                                break;
                              default:
                                // code block
                            }
                            
                        });
                    }  
                },
            })               
    }
};

$(document).ready(function () {
    atencionsala.init();
})