﻿var merc_perdidas = {

    //Función que contiene todas las funciones que deberán estar cargadas el ingresar a la pantalla.
    //tambien aca podemos esconder y mostrar ciertos controles, dependiendo de lo que necesitemos.
    init: function () {
        merc_perdidas.CargaFechas();
        merc_perdidas.CargarControlesPrevios();
        merc_perdidas.MERC_PERD_CargaInicial()
        $("#dtp_in_fch_reportada").val("");
        $("#detalles-mercancias_perdidas").attr('style', 'display:none');
        $("#btn_regresar").attr('style', 'display:none');
        $("#btn_actualizar").attr('style', 'display:none');
        $("#btn_limpiar").attr('style', 'display:none');

         $('#in_Mercancia_id').bind("cut copy paste",function(e) {
             e.preventDefault();
         });
    },

    //Llama el listar con param en null y vació para que liste la totalidad
    MERC_PERD_CargaInicial:function(){
        merc_perdidas.MERC_PERD_Obtener(null,"");
    },

    // Función al dar click en el botón de buscar en los filtros
    MERC_PERD_Buscar:function(){
        var p_Id_Mercancia = $("#in_Mercancia_id").val();
        var p_Fecha_Reportada = $("#dtp_in_fch_reportada").val();
        if(p_Id_Mercancia == "" && p_Fecha_Reportada == ""){
            swal({
                title: "Mercancías Perdidas",
                text: "Debe digitar un id o seleccionar una fecha para realizar la consulta.",
                icon: "warning",
                button: "OK!",
            })
        }else{
            
            merc_perdidas.MERC_PERD_Obtener(p_Id_Mercancia,p_Fecha_Reportada);
        }
    },

    //Función que se encarga de listar las mercancias perdidas
    MERC_PERD_Obtener: function (p_Id_Mercancia,p_Fecha_Reportada) {
        //Obtenemos la plantilla
        $('#th_Mercancias_Perdidas').show();
        $("#th_Mercancias_Perdidas").attr('style', 'display:block');
        var templateText = $("#MERC_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        
        $.getJSON('/api/TX_MERCANCIAS_PERDIDAS_TB/GetMercancias_Perdidas?Id_Mercancia=' + p_Id_Mercancia + '&Fecha_Reportada=' + p_Fecha_Reportada).then(function (data) {
            if (typeof data === 'string' || data instanceof String){
                swal({
                    title: "Mercancías Perdidas",
                    text: data,
                    icon: "warning",
                    button: "OK!",
                })
            }else{
                if(data.length != 0){
                     $("#Mercancias_Perdidas_table1body").html('');
                    $.each(data, function (index, elem) {
                    $("#Mercancias_Perdidas_table1body").append(tableTemplate(elem));
                    })
                }else{
                   swal({
                    title: "Mercancías Perdidas",
                    text: "No existen datos para la consulta.",
                    icon: "warning",
                    button: "OK!",
                }) 
                }
               
            }
        });
    },

    //Función que se encarga de dar el formato a los datepicker,
    CargaFechas: function () {
        $("#dtp_in_fch_reportada").kendoDatePicker({
            format: "dd/MM/yyyy",
            value: new Date(),
            dateInput: true,
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
           
        });      
    },

    //Función que se encarga de llenar el combo de los tipos de resolución
    TipoResolucion: function (valor) {
        if (valor == '') {
            valor = 0;
        }
        principal.getData(
            "/api/TX_MERCANCIAS_PERDIDAS_TB/TipoResolucion",
            function (data) {
                $("#MRP_TIPO_RESOL").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        );
    },

    //Función que se encarga de limpiar los filtros y la tabla con los datos de mercancias perdidas
    MERC_PERD_limpiarFiltros: function () {
        $("#in_Mercancia_id").val("");        
        principal.deactivateLabels();
        $("#Mercancias_Perdidas_table1body").html('');
        merc_perdidas.CargaFechas();
        $("#dtp_in_fch_reportada").val("");
        merc_perdidas.MERC_PERD_CargaInicial();
    },

    //Función que se encarga de cargar ciertas funciones necesarias para el funcionamiento de la pantalla, la llamamos en el init 
    CargarControlesPrevios: function () {
        merc_perdidas.TipoResolucion('');
    },

    //Función que se encarga de obtener los datos de las mercancias perdidas para ser editadas
    MERC_PERD_ConsultarDatosMercanciasPerdidas: function (p_id_mercancia) {
       var postdata = { id: p_id_mercancia };
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MERCANCIAS_PERDIDAS_TB/GetDetalleMercancias_Perdidas?id_mercancia=' + p_id_mercancia,
            data: postdata,
            success: function (data) {
                if (data !== null) {
                    //trae los datos de la mercancia
                    principal.activeLabels();
                    //Seteamos el valor del id de la mrcancia que vamos a editar.
                    merc_perdidas.MERC_PERD_MuestraDatos(data[0]);
                    $("#th_Mercancias_Perdidas").attr('style', 'display:none');
                    $("#div_filters").attr('style', 'display:none');
                    $("#detalles-mercancias_perdidas").attr('style', 'display:block');
                    $("#btn_regresar").attr('style', 'display:block');
                    $("#btn_actualizar").attr('style', 'display:block');
                    $("#btn_limpiar").attr('style', 'display:none');
                    $("#MRP_TIPO_RESOL").val(0);
                    merc_perdidas.TipoResolucion(data[0].MRP_TIPO_RESOL);
                }
            },
            failure: function (data) {

                console.log("fail");
            },
            error: function (data) {

                console.log("error consulta mercancías perdidas");
            }
        });
    },

    //Función que se encarga de realizar la actualización del tipo de resolución de las mercancias perdidas
    MERC_PERD_Actualizar: function () {
        $("#UserMERX").val($("#UsReg").val());
        var data = principal.jsonForm($('#frm_mercancias_perdidas').serializeArray());
        debugger;
        if (data.MRP_TIPO_RESOL != "0") {

            $.ajax({
                url: '/api/TX_MERCANCIAS_PERDIDAS_TB_',
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String) {
                        swal({
                            title: "Actualización de Mercancías Perdidas",
                            text: data,
                            icon: "error",
                            button: "OK!",
                        })
                    } else {
                        swal({
                            title: "Mercancías Perdidas",
                            text: "Se actualizó correctamente la información",
                            icon: "success",
                            button: "OK!",

                        }).then((value) => {
                            //if (cambioR == 'false') {
                            //    cajera.CJR_limpiarFiltros();
                            //} else {
                            //    //muestra el tab de seg
                            //    //principal.ActivarTab('seguimiento');
                            //}
                            merc_perdidas.MERC_PERD_Regresar();
                            $("#MRP_TIPO_RESOL").val(0);
                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })



        } else {

            swal({
                title: "Mercancías Perdidas",
                text: "Debe seleccionar el tipo de resolución",
                icon: "error",
                button: "OK!",

            })


        }

              
             
    },


    //Función que ejecuta todas las acciones que realiza el botón de regresar
    MERC_PERD_Regresar: function () {
        $("#detalles-mercancias_perdidas").attr('style', 'display:none');
        $("#btn_regresar").attr('style', 'display:none');
        $("#btn_actualizar").attr('style', 'display:none');
        $("#btn_limpiar").attr('style', 'display:none');
        $("#th_Mercancias_Perdidas").attr('style', 'display:block');
        $("#div_filters").attr('style', 'display:block');
        merc_perdidas.MERC_PERD_limpiarFiltros();
    },


    //Función que se encarga de setear los valores de la mercancia a editar.
    MERC_PERD_MuestraDatos: function (p_mercancias) {
        for (var campo in p_mercancias) {
            if (p_mercancias[campo] != null) {
                principal.setValueByName(campo, p_mercancias[campo], 'frm_mercancias_perdidas');
            }
        }
    },

};
//Función que se ejecuta cada vez que la página se carga. 
$(document).ready(function () {
    merc_perdidas.init();
})