var cons_prealerta = {

    init: function () {
        TecladoFuncionalidadPersonalizada();
        cons_prealerta.PLR_Listar($("#in_flr_plr_guia").val());


        //Disparo evento de consulta de guía cuando dan enter
        $('#in_flr_plr_guia').bind("enterKey",function(e){
          cons_prealerta.PLR_Listar($("#in_flr_plr_guia").val());
        });
        $('#in_flr_plr_guia').keyup(function(e){
          if(e.keyCode == 13)
          {
            $(this).trigger("enterKey");
          }
        });

        //Controla cuando borran o dan delete
      $('#in_flr_plr_guia').keyup(function(e){
          if(e.keyCode == 8 || e.keyCode == 46)
          {
             var guia = $("#in_flr_plr_guia").val();
             

             if(guia == ""){
              cons_prealerta.PLR_Listar(guia);
             }           
          }
        });
    },
    
    onfocus: function () {
        if ($("#lbl_flr_guia").hasClass('lblfocus'))
            $("#lbl_flr_guia").removeClass('lblfocus');
    },

    PLR_Listar: function(guia){
        //Buscar lista de prealertas según filtros
        //Obtenemos la plantilla
        var templateText = $("#PLR_Cons_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#Cons_Prealertas_tablebody").html('');
        $.getJSON('/api/TX_PREALERTAS_TB/GetConsPrealertas?p_guia=' + guia).then(function (data) {
            $.each(data, function (index, elem) {
                $("#Cons_Prealertas_tablebody").append(tableTemplate(elem));
            })
        });
    },
    

    PLR_BuscarFiltro: function () {
        //if($("#in_flr_plr_guia").val() == ""){
        cons_prealerta.PLR_Listar($("#in_flr_plr_guia").val());
        //} 
        //principal.filterTable_Input('in_flr_plr_guia', 'Cons_Prealertas_Table', 0);
    },
    

    PLR_limpiarFiltros: function () {
        $("#in_flr_plr_guia").val("");
        $("#in_flr_plr_guia").focus();
        cons_prealerta.PLR_Listar($("#in_flr_plr_guia").val());
    },
    
    PLR_abrirarchivo: function (p_pdf) {      
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_PREALERTAS_TB/AbrirPDF?pdf='+ p_pdf,
            success: function (data) {
                 console.log(data);
            },
            failure: function (data) {

                swal({
                    title: "Consulta Prealertas",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                });
                
            },
            error: function (data) {
                swal({
                    title: "Consulta Prealertas",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                });               
            }
        });
    },


    PLR_Imprimir: function(pdf){

        window.open("http://merxalmacenadora/WebEtiquetaNCI/RepPrealerta.aspx?Documento=" + pdf);
        // var serverFolder = "\\\\Dpsavfss\\COMPARTIDOS\\PUBLICO\\PREALERTASALSATEST\\";
        // var destinationFilename = serverFolder + pdf;
        // // $("#frame").attr("src", pdf);
        // //Lo abre en una pestaña nueva
        
        // cons_prealerta.PLR_abrirarchivo(pdf);


        //window.open(destinationFilename, '_blank');
         // window.location = "/assets/Archivos/License.pdf";

         //Lo abre en un popup pero tarda mucho en cargar y levantar por lo que no es factible en este momento
        // var testFrame = document.createElement("IFRAME");
        // $("#testFrame").remove();   
        // testFrame.id = "testFrame";
        // testFrame.width="100%";
        // testFrame.height="600";
        // testFrame.scrolling="no";
        // testFrame.frameborder="0px";
        // testFrame.src = pdf+"?wmode=transparent"; //Sacar el nombre del fichero pdf desde el parametro
        // var control = document.getElementById("testFrame")
        // if (control==null) { 
        //     $('#container').append(document.body.appendChild(testFrame));
        // }
        // $('#myModal').modal('show');    
    }
};

$(document).ready(function () {
    cons_prealerta.init();
})