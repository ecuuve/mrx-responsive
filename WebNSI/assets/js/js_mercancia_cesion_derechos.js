var mercancia_cesion_derechos = {
	init: function(){
        TecladoFuncionalidadPersonalizada();
	},
    onfocus: function () {
        if ($("#lbl_consecutivo_ces").hasClass('lblfocus'))
            $("#lbl_consecutivo_ces").removeClass('lblfocus');
    }, 

    // Busca las cesiones
	buscarCes: function (id) {      
        var entrada = $('#div_cesion_derechos_filtros #FLT_IEN_ENTRADA').val();
        var manifiesto = $('#div_cesion_derechos_filtros #FLT_IEN_MANIFIESTO').val();
        var guia = $('#div_cesion_derechos_filtros #FLT_IEN_GUIA_ORIGINAL').val();
        var consignatario = $('#ManifiestoModal #ce_flt_consignatario').val();
		$.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MERCANCIAS_TB/CesionDerechos?entrada='+entrada + "&manifiesto=" + manifiesto + "&guia=" + guia + "&consignatario=" + consignatario,
            success: function (data) {
            	if(data != null){
                    if(data.length == 1){
                		if(data[0].IEN_ENTRADA != null)
                			$('#div_cesion_derechos_filtros #FLT_IEN_ENTRADA').val(data[0].IEN_ENTRADA);
                		if(data[0].IEN_MANIFIESTO != null)
                			$('#div_cesion_derechos_filtros #FLT_IEN_MANIFIESTO').val(data[0].IEN_MANIFIESTO);
                		if(data[0].IEN_GUIA_ORIGINAL != null)
                			$('#div_cesion_derechos_filtros #FLT_IEN_GUIA_ORIGINAL').val(data[0].IEN_GUIA_ORIGINAL);
                		$.each(data[0], function(i,v){
    	            		principal.setValueByName(i, v,'div_cesion_derechos_filtros');
    	            	});
                        principal.activeLabels(); 
    	            	if ($('#div_cesion_derechos_filtros #FLT_IEN_ENTRADA').val() != "") {
    	            		mercancia_cesion_derechos.buscar();
    	            	}
                    }else{
                        if(manifiesto.length > 3 || guia.length > 3){
                            $('#ManifiestoModal').modal('show');
                            var templateText = $("#manfiestosTable_table-template").html();
                            //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                            var tableTemplate = Handlebars.compile(templateText);
                            $("#manfiestosTable_tablebody").html('');
                            $.each(data, function (index, elem) {
                                $("#manfiestosTable_tablebody").append(tableTemplate(elem));
                            });
                        }
                    }
            	}
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Mercancias con Cesion de Derechos",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
            }
        });
    },

    // Accion del botón buscar principal
    filtrar: function(){       
        mercancia_cesion_derechos.buscarCes("");      
    },

    // filtrar: function(input){
    //     if(input.value.length > 3){
    //         mercancia_cesion_derechos.buscarCes();
    //     }
    // },


    buscar: function () {
       var entrada = $('#div_cesion_derechos_filtros #FLT_IEN_ENTRADA').val();
       var mercancia = null;
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MERCANCIAS_TB/CESMercancias?&entrada=' + entrada+ "&mercancia=" + mercancia,
            success: function (data) {
                //Obtenemos la plantilla
                var templateText = $("#ces_table-template").html();
                //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                var tableTemplate = Handlebars.compile(templateText);
                $("#ces_tablebody").html('');
                //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                    $.each(data, function (index, elem) {
                        $("#ces_tablebody").append(tableTemplate(elem));
                    });
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Mercancias con Cesion de Derechos",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
                console.log("error");
            }
        });
    },
    editar: function(merc){
    	if($('#editForm').hasClass('fullHidden'))
    		$('#editForm').removeClass('fullHidden');
    	$('#id_mercancia').val(merc);
    	if($('#btn_editar_ces').hasClass('fullHidden'))
    		$('#btn_editar_ces').removeClass('fullHidden');
    	$("#div_cesion_derechos_filtros #FLT_IEN_ENTRADA").prop('disabled', true);
		$("#div_cesion_derechos_filtros #FLT_IEN_MANIFIESTO").prop('disabled', true);
		$("#div_cesion_derechos_filtros #FLT_IEN_GUIA_ORIGINAL").prop('disabled', true);
		mercancia_cesion_derechos.buscarCES(merc);
    },
    Actualizar: function () {
        var id = $('#id_mercancia').val();
        var user = $('#UsReg').val();
        if (id.length > 0) {
        	var bultos = $('#ces_bultos_salidos').val();
        	var kilos = $('#ces_kilos_salidos').val();
            $.ajax({
                url: '/api/TX_MERCANCIAS_TB/ActualizaCES?id=' + id + '&bultos_salida=' + bultos + '&kilos_salidos=' + kilos+ '&user=' + user,
                type: 'PUT',
                contentType: "application/json",
                success: function (data) {
                   if(data != null){
	                   swal({
	                        title: "Mercancias con Cesion de Derechos",
	                        text: "Se actualizó correctamente el Registor de Mercancias con Cesion de Derechos",
	                        icon: "success",
	                        button: "OK!",
	                    });
	                   mercancia_cesion_derechos.limpiar();
	               }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                    else{
	                	swal({
	                      title: "Mercancias con Cesion de Derechos",
	                      text: data.responseJSON.Message,
	                      icon: "error",
	                      button: "OK!",
	                    });
	                }
                }
            });
        }
        else {
            swal({
                title: "Mercancias con Cesion de Derechos",
                text: "Debe seleccionar una Solicitud para ser editada",
                icon: "error",
                button: "OK!",

            });
        }
    },
    buscarCES: function (merc) {
        var id = $('#div_cesion_derechos_filtros #FLT_IEN_ENTRADA').val();
		$.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MERCANCIAS_TB/CESMercancias?&entrada=' + id+ "&mercancia=" + merc,
            success: function (data) {
            	if(data != null){
            		if(data[0].MER_BULTOS_SALIDOS != null)
            			$('#ces_bultos_salidos').val(data[0].MER_BULTOS_SALIDOS);
            		if(data[0].MER_KILOS_SALIDOS != null)
            			$('#ces_kilos_salidos').val(data[0].MER_KILOS_SALIDOS);
            	}
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Mercancias con Cesion de Derechos",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
            }
        });
    },

    //Limpia la pantalla principal
    limpiar: function(){
        $('#ce_flt_consecutivo').val('');
        $('#ce_flt_guia_original').val('');
        $('#ce_flt_consignatario').val('');
        
    	principal.resetAllFields('div_cesion_derechos_filtros');
       $("#ces_tablebody").html('');
       	if(!$('#editForm').hasClass('fullHidden'))
    		$('#editForm').addClass('fullHidden');
    	$('#id_mercancia').val('');
    	if(!$('#btn_editar_ces').hasClass('fullHidden'))
    		$('#btn_editar_ces').addClass('fullHidden');
    	$("#div_cesion_derechos_filtros #FLT_IEN_ENTRADA").prop('disabled', false);
		$("#div_cesion_derechos_filtros #FLT_IEN_MANIFIESTO").prop('disabled', false);
		$("#div_cesion_derechos_filtros #FLT_IEN_GUIA_ORIGINAL").prop('disabled', false);        
        principal.deactivateLabels();
        $("#FLT_IEN_ENTRADA").focus();
    },


    BuscarCesFiltro: function () {
        var entrada = $('#ManifiestoModal #ce_flt_consecutivo').val();
        var manifiesto = $('#div_cesion_derechos_filtros #FLT_IEN_MANIFIESTO').val();
        var guia = $('#ManifiestoModal #ce_flt_guia_original').val();
        var consignatario = $('#ManifiestoModal #ce_flt_consignatario').val();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MERCANCIAS_TB/CesionDerechos?entrada='+entrada + "&manifiesto=" + manifiesto + "&guia=" + guia + "&consignatario=" + consignatario,
            success: function (data) {
                if(data != null){
                    $('#ManifiestoModal').modal('show');
                    var templateText = $("#manfiestosTable_table-template").html();
                    //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                    var tableTemplate = Handlebars.compile(templateText);
                    $("#manfiestosTable_tablebody").html('');
                    $.each(data, function (index, elem) {
                        $("#manfiestosTable_tablebody").append(tableTemplate(elem));
                    });
                }
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Mercancias con Cesion de Derechos",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
            }
        });
    },

    //Limpia los filtros del modal
    limpiarFiltrosCes: function(){
        $('#ce_flt_consecutivo').val('');
        $('#ce_flt_guia_original').val('');
        $('#ce_flt_consignatario').val('');
        mercancia_cesion_derechos.BuscarCesFiltro();
    },

    //Pinta los datos de la fila seleccionada.
    pintarCesInputs: function(id){
        var manifiesto = $('#div_cesion_derechos_filtros #FLT_IEN_MANIFIESTO').val();
        var guia = $('#ManifiestoModal #ce_flt_guia_original').val();
        var consignatario = $('#ManifiestoModal #ce_flt_consignatario').val();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MERCANCIAS_TB/CesionDerechos?entrada='+id + "&manifiesto=" + manifiesto + "&guia=" + guia + "&consignatario=" + consignatario,
            success: function (data) {
                if(data != null){
                    if(data.length == 1){
                        if(data[0].IEN_ENTRADA != null)
                            $('#div_cesion_derechos_filtros #FLT_IEN_ENTRADA').val(data[0].IEN_ENTRADA);
                        if(data[0].IEN_MANIFIESTO != null)
                            $('#div_cesion_derechos_filtros #FLT_IEN_MANIFIESTO').val(data[0].IEN_MANIFIESTO);
                        if(data[0].IEN_GUIA_ORIGINAL != null)
                            $('#div_cesion_derechos_filtros #FLT_IEN_GUIA_ORIGINAL').val(data[0].IEN_GUIA_ORIGINAL);
                        $.each(data[0], function(i,v){
                            principal.setValueByName(i, v,'div_cesion_derechos_filtros');
                        }); 
                         principal.activeLabels(); 
                        if ($('#div_cesion_derechos_filtros #FLT_IEN_ENTRADA').val() != "") {
                            mercancia_cesion_derechos.buscar();
                        }
                        $('#ManifiestoModal').modal('hide');
                    }
                }
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Mercancias con Cesion de Derechos",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                });
            }
        });
    }
};
$(document).ready(function () {
    mercancia_cesion_derechos.init();
});