var solicitud_video = {
	init: function(){
        principal.resetAllFields('frm_solicitudVideo');
        // solicitud_video.limpiaFiltros();
		solicitud_video.cargaInicial();
        TecladoFuncionalidadPersonalizada();
         $("#btn_editar_solicitudes").attr('style', 'display:none');
           //$('#tbl_solicitudesVideo').addClass('fullHidden');

        $('#SIV_IEN_ENTRADA').change(function () {
            solicitud_video.buscarConse();
        });

        $('#SIV_TIPO').change(function () {
            if($('#SIV_TIPO').val() == 'G'){
                $("#SIV_SISTEMA").val("N");
            }else if($('#SIV_TIPO').val() == 'O'){
                 $("#SIV_SISTEMA").val("M");
            }else{
                $("#SIV_SISTEMA").val("");
            }           
        });

         $('#SIV_SISTEMA').change(function () {
            if($('#SIV_SISTEMA').val() == 'M'){
                $("#SIV_TIPO").val("O");
            }else if($('#SIV_SISTEMA').val() == 'N'){
                 $("#SIV_TIPO").val("G");
            }else{
                $("#SIV_TIPO").val("");
            }           
        });
	},
	cargaInicial: function(){
		solicitud_video.getTipoSolicitud();
		solicitud_video.getSistemas();
		solicitud_video.getSolicitantes();
        solicitud_video.getSistemasFiltro();
        solicitud_video.getEstados();

		$("#SIV_FCH_REGISTRA").kendoDatePicker({
          format: "dd/MM/yyyy",
          value: new Date(),
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });
		$('#btn_regresar_solicitudes').hide();
	},

	//Lista los tipos de solicitud
	getTipoSolicitud: function () {
        principal.getData("/api/TX_SOLICITUDES_VIDEO_TB/TipoSolicitud",function (data) {$("#SIV_TIPO").html(principal.arrayToOptions(data));});
    },

    //Lista Sistemas
    getSistemas: function () {
        principal.getData("/api/TX_SOLICITUDES_VIDEO_TB/Sistemas",function (data) {$("#SIV_SISTEMA").html(principal.arrayToOptions(data));});
    },

    getSistemasFiltro: function () {
        principal.getData("/api/TX_SOLICITUDES_VIDEO_TB/Sistemas",function (data) {$("#siv_flr_sistema").html(principal.arrayToOptions(data));});
    },

    //Lista los solicitantes
    getSolicitantes: function () {
        principal.getData("/api/TX_SOLICITUDES_VIDEO_TB/Solicitantes",function (data) {$("#SIV_SOLICITA").html(principal.arrayToOptions(data));});
    },

    getEstados: function () {
        principal.getData("/api/TX_SOLICITUDES_VIDEO_TB/Estados",function (data) {$("#siv_flr_estado").html(principal.arrayToOptions(data));});
    },

    guardar: function(){
    	$("#SIV_USU_REGISTRA").val($("#SIV_USU").val());
        $.post(
            "/api/TX_SOLICITUDES_VIDEO_TB/GuardarSolicitudVideo/",
             $('#frm_solicitudVideo').serialize(),
            function (data) {
                if(data != null){
                    swal({
	                    title: "Solicitud de Video",
	                    text: "Registro de Video guardado exitosamente",
	                    icon: "success",
	                    button: "OK!",
	                    }).then((value) => {
	                       	principal.resetAllFields('frm_solicitudVideo');
	                        var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd/MM/yyyy');
	    					$("#SIV_FCH_REGISTRA").val(todayDate);
	                    });
                }
            })
            .fail(function (data) {
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                }else{
                	swal({
                      title: "Solicitud de Video",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                    });
                }
            });
    },

    limpiaFiltros: function(){
    	principal.resetAllFields('frm_solicitudVideo');
        var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd/MM/yyyy');
		$("#SIV_FCH_REGISTRA").val(todayDate);
		$('#siv_flr_solicitud').val('');
        $('#siv_flr_solicitud').val();
        $('#siv_flr_estado').val('0');
        $('#siv_flr_entrada').val('');
        $('#siv_flr_sistema').val('0');
        $('#btn_guardar_solicitudes').show();
        $("#btn_editar_solicitudes").attr('style', 'display:none');       
		solicitud_video.buscar();

        principal.deactiveLabelsInput();
        $('#SIV_IEN_ENTRADA').focus();
    },

    buscarConse: function () {
       var consecutivo = $('#SIV_IEN_ENTRADA').val();
       var sistema = $('#SIV_SISTEMA').val();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_SOLICITUDES_VIDEO_TB/InfoConse?consecutivo='+consecutivo + "&sistema=" + sistema,
            success: function (data) {
                if(data.length > 0){
                    $('#IEN_GUIA_MASTER').val(data[0].IEN_GUIA_MASTER);
                    $('#IEN_GUIA_ORIGINAL').val(data[0].IEN_GUIA_ORIGINAL);
                    $('#CONSIGNATARIO').val(data[0].IEN_IMPORTADOR);
                    $('#AER_NOMBRE').val(data[0].AER_NOMBRE);                  
                }                
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Solicitud de Video",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
                console.log("error");
            }
        });
    },

    buscar: function () {
       var solicitud = $('#siv_flr_solicitud').val();
       var estado = $('#siv_flr_estado').val();
       var entrada = $('#siv_flr_entrada').val();
       var sistema = $('#siv_flr_sistema').val();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_SOLICITUDES_VIDEO_TB/Solicitudes?solicitud='+solicitud + "&estado=" + estado +"&entrada=" + entrada + "&sistema=" + sistema,
            success: function (data) {
                //if(data.length > 1){
                    
                //}
                 
                
                //Obtenemos la plantilla
                var templateText = $("#solicitudesVideo_table-template").html();
                //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                var tableTemplate = Handlebars.compile(templateText);
                $("#solicitudesVideo_tablebody").html('');
                //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                    $.each(data, function (index, elem) {
                        $("#solicitudesVideo_tablebody").append(tableTemplate(elem));
                    });
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Solicitud de Video",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
                console.log("error");
            }
        });
    },

    Refrescar: function () {
        $('#frm_solicitudVideo').hide();
        $('#tbl_solicitudesVideo').show();
        $('#div_solicitavideo_filtros').show();
        $('#btn_listar_solicitudes').hide();
        $('#btn_guardar_solicitudes').hide();
        $('#btn_editar_solicitudes').hide();
        $('#btn_regresar_solicitudes').show();
        $('#btn_limpiar_solicitudes').hide();
        if($('#tbl_solicitudesVideo').hasClass('fullHidden'))
            $('#tbl_solicitudesVideo').removeClass('fullHidden');
            $('#div_solicitavideo_filtros').removeClass('fullHidden');
        solicitud_video.buscar();
    },

    regresar: function(){
    	$('#siv_flr_solicitud').val('');
        $('#frm_solicitudVideo').show();
        $('#tbl_solicitudesVideo').hide();
        $('#div_solicitavideo_filtros').hide();
        $('#btn_listar_solicitudes').show();
        $('#btn_guardar_solicitudes').show();
        $('#btn_editar_solicitudes').show();
        $('#btn_regresar_solicitudes').hide();
        $('#btn_limpiar_solicitudes').show();
        $('#tbl_solicitudesVideo').addClass('fullHidden');
        $('#div_solicitavideo_filtros').addClass('fullHidden');
        principal.resetAllFields('frm_solicitudVideo');
        $('#IEN_GUIA_MASTER').focus();
        //solicitud_video.limpiaFiltros();
    },

    editar: function(id){

    	solicitud_video.buscarSolicitud(id);
    	$('#btn_regresar_solicitudes').hide();
    	$('#btn_listar_solicitudes').show();
        $('#btn_limpiar_solicitudes').show();
    	//$('#btn_guardar_solicitudes').show();
    	$('#btn_editar_solicitudes').show();
        $("#tbl_solicitudesVideo").hide();
        $('#div_solicitavideo_filtros').hide();
        $('#frm_solicitudVideo').show();
        principal.resetAllFields('div_solicitavideo_filtros');
    },

    buscarSolicitud: function (id) {
        var estado = $('#siv_flr_estado').val();
        var entrada = $('#siv_flr_entrada').val();
        var sistema = $('#siv_flr_sistema').val();
    	principal.resetAllFields('frm_solicitudVideo');
    	var sistema = '';
    	var consecutivo = '';
		$.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_SOLICITUDES_VIDEO_TB/Solicitudes?solicitud='+id + "&estado=" + estado +"&entrada=" + entrada + "&sistema=" + sistema,
            success: function (data) {
            	$('#SIV_SOLICITUD').val(id);
            	$.each(data[0], function(i,v){
            		principal.setValueByName(i, v,'frm_solicitudVideo');
            	});
                principal.activeLabelsInput(); 
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Solicitud de Video",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
            }
        });
    },

    Actualizar: function () {
        var id = $('#SIV_SOLICITUD').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_solicitudVideo').serializeArray());
            $.ajax({
                url: '/api/TX_SOLICITUDES_VIDEO_TB/ActualizaSolicitud?id=' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                   if(data != null){
	                   swal({
	                        title: "Solicitud de Video",
	                        text: "Se actualizó correctamente la Solicitud de Video",
	                        icon: "success",
	                        button: "OK!",
	                    });
                      // solicitud_video.limpiaFiltros();
	                   principal.resetAllFields('frm_solicitudVideo');
	               }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                    else{
	                	swal({
	                      title: "Solicitud de Video",
	                      text: data.responseJSON.Message,
	                      icon: "error",
	                      button: "OK!",
	                    });
	                }
                }
            });
        }
        else {
            swal({
                title: "Solicitud de Video",
                text: "Debe seleccionar una Solicitud para ser editada",
                icon: "error",
                button: "OK!",

            });
        }
    },

    filtrar: function(input){
        if(input.value.length > 3){
            solicitud_video.buscar();
        }
    }
};
$(document).ready(function () {
    solicitud_video.init();
});