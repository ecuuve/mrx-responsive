var consolidador = {
    pickedup : null,
    init: function () {  

       $("#CON_NOMBRE").attr('disabled', true);
       $("#CON_CEDULA").attr('disabled', true); 
       $("#CON_ESTADO_ENTRADA").attr('disabled', true);

       $("#CON_EST_ESTADO").attr('disabled', true); 
       $("#CON_USU_MODIFICA").attr('disabled', true); 
       
      



        TecladoFuncionalidadPersonalizada();
        consolidador.CON_LimpiarCampos();
        $('#CON_NCONSOLIDADOR').change(function () {
          var cod_p = $('#CON_NCONSOLIDADOR').val();
          var nom_p = $('#CON_NOMBRE').val();
          consolidador.CON_ConsultarDatosDetConsolidador(cod_p, nom_p);
        });

        $("#btn_actualizar_consolidador").attr('style', 'display:none');
        $("#div_agrega_consig").attr('style', 'display:none');
        

        // Cuando hace click en el tab de consolidador
        $('#consolidador-tab').on('click', function () {          
           $("#botones").show();                       
        });

        // Cuando hace click en el tab de destinos
        $('#destinos-tab').on('click', function () { 
          $("#CON_NCONSOLIDADOR_DEST").val($('#CON_NCONSOLIDADOR').val());
          $("#CON_NOMBRE_DEST").val($('#CON_NOMBRE').val());

          $("#UBI_CONSIG").val("");
          $('#CONSOLID_CNS').val($('#CON_NCONSOLIDADOR').val());
          $("#botones").hide();
          
          if( $("#Consig_table1body").html() == ''){
          }          
        });   
    },

    //Funcionalidad al dar click en el destino 
    CON_Funcionalidad_Click: function(consolid,ubic, linea){  
     //regresa al color gris antes de haberlo seleccionado
     if (consolidador.pickedup != null) {
         consolidador.pickedup.css( "background-color", "grey" );
     }
     //cambia el color de la fila a negro
     $( linea ).css( "background-color", "#3C3C3C" );

      consolidador.pickedup = $( linea );

      consolidador.CON_Listar_Consignatarios(consolid,ubic);
    },

    //Focus del label principal
    onfocus: function () {
        if ($("#lbl_CON_NCONSOLIDADOR").hasClass('lblfocus'))
            $("#lbl_CON_NCONSOLIDADOR").removeClass('lblfocus');
    },

    //Carga las listas de valores en los select
    CON_CargarControlesPrevios: function () {
        consolidador.CargaFechas();
        consolidador.getEstados();
        consolidador.getRecibeEstados();
        consolidador.getEstadosEntrada();       
    },

    //Carga las fechas del form
    CargaFechas: function () {
        principal.KendoDate($("#CON_FECHA_INGRESO"), new Date());
        principal.KendoDate($("#CON_FCH_RIGE"), new Date());
        principal.KendoDate($("#CON_FCH_MODIFICA"), new Date());
    },

    //Obtiene la lista de valores para el select
    getEstados: function (valor) {    
        principal.getData(
            "/api/TX_CONSOLIDADORES_TB/Estados",
            function (data) {
                $("#s_EstadoConsolidador").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        );
    },

    //Obtiene la lista de valores para el select
    getRecibeEstados: function (valor) {
        principal.getData(
            "/api/TX_CONSOLIDADORES_TB/RecibeEstados",
            function (data) {
                $("#s_RecibeEstados").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        );
    },

    //Obtiene la lista de valores para el select
    getEstadosEntrada: function (valor) {    
        principal.getData(
            "/api/TX_CONSOLIDADORES_TB/EstadosEnt",
            function (data) {
                $("#s_EstadoEntrada").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        );
    },

    //Consulta el detalle del consolidador seleccionado o digitado al insertar
    CON_ConsultarDatosDetConsolidador:function(p_id, p_nombre){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_CONSOLIDADORES_TB/GetConsolidadores?consolidador='+ p_id + "&nombre=" + p_nombre,
            //data: postdata,
            success: function (data) { 
                if (data !== null) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Consolidadores",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }
                    else{
                      if(data.length != 0){
                         principal.activeLabels();
                         consolidador.CON_MuestraDatos(data); 
                         $("#btn_actualizar_consolidador").attr('style', 'display:block');                         
                          if($('#li_destinos').hasClass('fullHidden'))
                              $('#li_destinos').removeClass('fullHidden');
                          consolidador.CON_Listar_Destinos(data[0]["CON_NCONSOLIDADOR"]);

                           $("#CON_NCONSOLIDADOR").attr('disabled', true); 
                           $("#CON_NOMBRE").attr('disabled', true);
                           $("#CON_CEDULA").attr('disabled', true); 
                           $("#CON_ESTADO_ENTRADA").attr('disabled', true);

                           $("#CON_EST_ESTADO").attr('disabled', true); 
                           $("#CON_USU_MODIFICA").attr('disabled', true); 
                           
                           $("#CON_FECHA_INGRESO").data("kendoDatePicker").enable(false); 
                           $("#CON_FCH_MODIFICA").data("kendoDatePicker").enable(false); 
                           

                      }
                       else{

                        swal({
                          title: "Consolidador",
                          text: "No existen datos para el valor consultado.",
                          icon: "error",
                          button: "OK!",
                        })

                       }
                    }                 
                }
            },           
            error: function (data) {
              swal({
                    title: "Consolidadores",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                  })
            }
        });
    },

     //Muestra los datos del consolidador consultado o seleccionado en pantalla
    CON_MuestraDatos: function (p_con) {
      for (var campo in p_con[0]) {    
        if(p_con[0][campo] != null){
            principal.setValueByName(campo, p_con[0][campo],'frm_consolidador');
        }
      }
        $("#btn_listar_agencia").attr('style', 'display:block');
        $("#btn_actualizar_agencia").attr('style', 'display:block');
        $("#btn_registrar_agencia").attr('style', 'display:none');
        $("#btn_limpiar_agencia").attr('style', 'display:block');
        $("#btn_regresar_agencia").attr('style', 'display:none');              
    },   

    //Método que va al api a listar los destinos 
    CON_Listar_Destinos: function(consolidador_p){
        //Obtenemos la plantilla       
        var templateText = $("#Destinos_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#Destinos_table1body").html('');
        $.getJSON('/api/TX_CONSOLIDADORES_TB/UbicacionesConsolidador?consolidador_p='+ consolidador_p).then(function (data) {
            $.each(data, function (index, elem) {
                $("#Destinos_table1body").append(tableTemplate(elem));
            })

            $("#Destinos_Table > tbody > tr").on("click", function(e) {
              if($("#destinos-tab").hasClass("active")){
                var consolid = $(this).find("td").eq(0).html();
                var ubicacion = $(this).find("td").eq(1).html();
                 $('#UBIC_CNS').val(ubicacion);
                consolidador.CON_Funcionalidad_Click(consolid, ubicacion, $( this ));
              }
            });

            $("#Consig_table1body").html('');
            $("#div_agrega_consig").attr('style', 'display:none'); 
        });  
    },

    //Método para borrar destinos
    CON_Borrar_Destino: function(consolid, ubicacion, numconsigna){
      if(numconsigna > 0){
         swal({
          title: "Está seguro?",
          text: "Eliminará la información de los "+ numconsigna + " consignatarios correspondientes al destino: "+ ubicacion +".",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            consolidador.DEST_Eliminar(consolid, ubicacion);                     
          } 
        });        
      }else{
        consolidador.DEST_Eliminar(consolid, ubicacion);   
      }
    },

    //Método para borrar destinos
    DEST_Eliminar: function(consolid, ubicacion){
      $.ajax({
              type:'Delete',
              //dataType: 'json',
              contentType: 'application/json',
              url: '/api/TX_CONSOLIDADORES_TB/DeleteDestino?id='+ consolid + '&ubi=' + ubicacion,
              //data: JSON.stringify(data),
              success: function (data) {
                swal({
                    title: "Destinos",
                    text: "Se eliminó correctamente el destino",
                    icon: "success",
                    button: "OK!",

                    }).then((value) => {

                        consolidador.DEST_LimpiarUbi();
                        consolidador.CON_Listar_Destinos(consolid);
                });
              },
              failure: function (data) {
                  swal({
                      title: "Error",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                  });
              },
              error: function (data) {
                  if(data.responseJSON.Message){
                      swal({
                          title: "Error",
                          text: data.responseJSON.Message,
                          icon:  "error",
                          button: "OK!",
                      });
                  }
              }
          });
      // var cb = function () {
      //   swal({
      //         title: "Destinos",
      //         text: "Se eliminó correctamente la información",
      //         icon: "success",
      //         button: "OK!",

      //         }).then((value) => {

      //             consolidador.CON_Listar_Destinos(consolid);
      //     });
      //  }
      // var url = '/api/TX_CONSOLIDADORES_TB/DeleteDestino?id='+ consolid + '&ubi=' + ubicacion;
      // principal.ajax("Delete", url,cb,"Destinos consolidador");
    },

    //Llama al método que Muestra screen con lista de ubicaciones
    DEST_MostrarListaUbicaciones: function () {
        $("#myModal").modal('show');
        $("#divProcessing").show();
        consolidador.DEST_BuscarUbicacion();     
    },

    //Muestra screen con lista de ubicaciones
    DEST_BuscarUbicacion: function () {       
        //Obtenemos la plantilla
        var templateText = $("#Ubicaciones_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $(".modal-body #Ubicaciones_table1body,.modal-body textarea").each(function () {
            $(this).html('');
        });

         $.getJSON('/api/TX_CONSOLIDADORES_TB/Ubicaciones').then(function (data) {
                $.each(data, function (index, elem) {
                    $("#Ubicaciones_table1body").append(tableTemplate(elem));
                })
                $("#divProcessing").hide();
            });
    },

    //Carga datos de la ubicación seleccionada en el screen
    DEST_MuestraDatosUbicacion: function (id, nombre){
      $("#CON_UBI_ID").val(id);
      $("#CON_UBI").val(nombre);
      $("#myModal").modal('hide');
    },

    //Agrega el destino al consolidador
    DEST_AgregaDestino: function () {
      var consolid = $("#CON_NCONSOLIDADOR").val();
      var ubicacion = $("#CON_UBI_ID").val();

      $.ajax({
              type:'POST',
              //dataType: 'json',
              contentType: 'application/json',
              url: '/api/TX_CONSOLIDADORES_TB/PostDestino?id='+ consolid + '&ubi=' + ubicacion,
              //data: JSON.stringify(data),
              success: function (data) {
                swal({
                    title: "Destinos",
                    text: "Se agregó correctamente el destino",
                    icon: "success",
                    button: "OK!",

                    }).then((value) => {

                        consolidador.DEST_LimpiarUbi();
                        consolidador.CON_Listar_Destinos(consolid);
                });
              },
              failure: function (data) {
                  swal({
                      title: "Error",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                  });
              },
              error: function (data) {
                  if(data.responseJSON.Message){
                      swal({
                          title: "Error",
                          text: data.responseJSON.Message,
                          icon:  "error",
                          button: "OK!",
                      });
                  }
              }
          });
      //   var cb2 = function () {
      //   swal({
      //         title: "Destinos",
      //         text: "Se agregó correctamente el destino",
      //         icon: "success",
      //         button: "OK!",

      //         }).then((value) => {

      //             consolidador.DEST_LimpiarUbi();
      //             consolidador.CON_Listar_Destinos(consolid);
      //     });
      //   }
       
      // var url = '/api/TX_CONSOLIDADORES_TB/PostDestino?id='+ consolid + '&ubi=' + ubicacion;
      // principal.ajax("POST", url,cb2,"Destinos consolidador");
    },

    //Limpia campos de ubicación a agregar 
    DEST_LimpiarUbi: function() {
      $("#CON_UBI_ID").val("");
      $("#CON_UBI").val("");
      $('#lbl_con_ubi').removeClass('active');
    },

    //Método que va al api a listar los consignatarioa 
    CON_Listar_Consignatarios: function(consolidador_p, ubicacion_p){       
        //Obtenemos la plantilla        
        var templateText = $("#Consig_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#Consig_table1body").html('');
        $.getJSON('/api/TX_CONSOLIDADORES_TB/ConsignatariosUbicacion?consolidador_p='+ consolidador_p + "&ubicacion_p=" + ubicacion_p).then(function (data) {
            $.each(data, function (index, elem) {
                $("#Consig_table1body").append(tableTemplate(elem));
            })
            $("#div_agrega_consig").attr('style', 'display:block');
            // if(typeof(callback) == "function"){
            //     callback();
            // }
        });
    },

    //Agrega el consignatario al destino correspondiente
    CNS_AgregaConsigDestino:function(){
      var consig = $("#UBI_CONSIG").val();
      var consolid = $("#CONSOLID_CNS").val();
      var ubicacion = $("#UBIC_CNS").val();
      $.ajax({
              type:'POST',
              //dataType: 'json',
              contentType: 'application/json',
              url: '/api/TX_CONSOLIDADORES_TB/PostConsig?id='+ consolid + '&ubi=' + ubicacion+ '&consig=' + consig,
              //data: JSON.stringify(data),
              success: function (data) {
                swal({
                    title: "Consignatarios",
                    text: "Se agregó correctamente el consignatario",
                    icon: "success",
                    button: "OK!",

                    }).then((value) => {
                        consolidador.CON_Listar_Consignatarios(consolid,ubicacion);
                        consolidador.CNS_LimpiarCNS();
                });
              },
              failure: function (data) {
                  swal({
                      title: "Error",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                  });
              },
              error: function (data) {
                  if(data.responseJSON.Message){
                      swal({
                          title: "Error",
                          text: data.responseJSON.Message,
                          icon:  "error",
                          button: "OK!",
                      });
                  }
              }
          });
    },

    //Limpia campos de consignatario
    CNS_LimpiarCNS:function(){
      $("#UBI_CONSIG").val("");
      $("#UBI_CONSIG").focus();
    },

    //Método para borrar destinos
    CNS_EliminarConsig: function(consig_p){
      $.ajax({
              type:'Delete',
              //dataType: 'json',
              contentType: 'application/json',
              url: '/api/TX_CONSOLIDADORES_TB/DeleteConsignatario?id='+ consig_p,
              //data: JSON.stringify(data),
              success: function (data) {
                swal({
                    title: "Consignatarios",
                    text: "Se eliminó correctamente el consignatario",
                    icon: "success",
                    button: "OK!",

                    }).then((value) => {
                        var consolid = $("#CONSOLID_CNS").val();
                        var ubicacion = $("#UBIC_CNS").val();
                        consolidador.CON_Listar_Consignatarios(consolid,ubicacion);
                        consolidador.CNS_LimpiarCNS();
                });
              },
              failure: function (data) {
                  swal({
                      title: "Error",
                      text: data,
                      icon: "error",
                      button: "OK!",
                  });
              },
              error: function (data) {
                  if(data.responseJSON.Message){
                      swal({
                          title: "Error",
                          text: data.responseJSON.Message,
                          icon:  "error",
                          button: "OK!",
                      });
                  }
              }
          });
      // var cb = function () {
      //   swal({
      //         title: "Destinos",
      //         text: "Se eliminó correctamente la información",
      //         icon: "success",
      //         button: "OK!",

      //         }).then((value) => {

      //             consolidador.CON_Listar_Destinos(consolid);
      //     });
      //  }
      // var url = '/api/TX_CONSOLIDADORES_TB/DeleteDestino?id='+ consolid + '&ubi=' + ubicacion;
      // principal.ajax("Delete", url,cb,"Destinos consolidador");
    },

    //Funcionalidad botón actualizar Consolidador
    CON_Actualizar: function(){
      $("#USER_MERX").val($("#UsReg").val());
      var id = $('#CON_NCONSOLIDADOR').val();
      if (id.length > 0) {
          $("#CON_NCONSOLIDADOR").attr('disabled', false); 
          var data = principal.jsonForm($('#frm_consolidador').serializeArray());
          $("#CON_NCONSOLIDADOR").attr('disabled', true); 

          $.ajax({
              url: '/api/TX_CONSOLIDADORES_TB/' + id,
              type: 'PUT',
              data: JSON.stringify(data),
              contentType: "application/json",
              success: function (data) {
                  swal({
                  title: "Consolidadores",
                  text: "Se actualizó correctamente la información",
                  icon: "success",
                  button: "OK!",

                  }).then((value) => {
                     consolidador.CON_LimpiarCampos();
                     $("#CON_NCONSOLIDADOR").attr('disabled', false); 
                  });
              },
              error: function (data) {
                  swal({
                  title: "Consolidadores",
                  text: data.responseJSON.Message,
                  icon: "error",
                  button: "OK!",
                })
              }
          })
        }
        else {
            swal({
                title: "Consolidador",
                text: "El ID de no puede ser vacío",
                icon: "error",
                button: "OK!",

            }).then((value) => {
              // $("#BLO_IEN_ENTRADA").attr('disabled', true);
              // $("#BLO_ID_BLOQUEO").attr('disabled', true);
              // $("#BLO_MOTIVO_DESBLOQUEO").attr('disabled', true);           
            });
        }
    },

     // Limpia campos de los formularios
    CON_LimpiarCampos: function () {
      consolidador.DEST_LimpiarUbi();
      principal.resetAllFields('detInfo_consolidador');
  
      TecladoFuncionalidadPersonalizada();
      principal.deactivateLabels();

      $("#CON_NCONSOLIDADOR").focus();    
      consolidador.CON_CargarControlesPrevios(); 
      $("#Consig_table1body").html('');
      $("#Destinos_table1body").html('');

      if(!$('#li_destinos').hasClass('fullHidden'))
          $('#li_destinos').addClass('fullHidden');  

      if(!$("#consolidador-tab").hasClass("active"))
        $("#consolidador-tab").addClass("active")

        $("#CON_NCONSOLIDADOR_DEST").val("");
        $("#CON_NOMBRE_DEST").val("");

      $("#btn_actualizar_consolidador").attr('style', 'display:none');   
      $("#div_agrega_consig").attr('style', 'display:none'); 
      $("#CON_NCONSOLIDADOR").attr('disabled', false); 
      $("#CON_NCONSOLIDADOR").focus();
      $("#CON_FECHA_INGRESO").data("kendoDatePicker").enable(false); 
      $("#CON_FCH_MODIFICA").data("kendoDatePicker").enable(false); 
    }

};

$(document).ready(function () {
    consolidador.init();
})