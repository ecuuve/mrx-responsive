var conceptos_tarifa = {
    title: "Conceptos por Tarifa",

    init: function () {
        TecladoFuncionalidadPersonalizada();
        this.IniciarControles();
    },

    //Carga de controles

    IniciarControles: function () {
        this.getMonedas();
        this.getConceptos();
    },

    getMonedas: function (valor) {
        principal.getData(
            "/api/TX_TARIFARIO_TB/monedas",
            function (data) {
                $("#CTR_MONEDA_MONTO_FIJO").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
                $("#CTR_MONEDA_MONTO_MINIMO").html(principal.arrayToOptionsSelectedWithDefault(data, valor));
            }
        );
    },
    
    getConceptos: function (valor) {
        $.ajax({
            url: "/api/TX_CONCEPTOS_COBRO_TB/busqueda",
            method: "POST",
            data: { B_CCB_DESCRIPCION: null },
            dataType: "json",
            success: function (data) {
                var html = "<option value=''>Seleccione una opci&oacute;n</option>";
                for (var index = 0; index < data.length; index++) {
                    var item = data[index];
                    html += "<option value='" + item.CCB_CONCEPTO + "'>" + item.CCB_DESCRIPCION + "</option>";
                }
                $("#CTR_CCB_CONCEPTO").html(html);
            },
            error: function (e) {
                console.log(e);
            }
        });
    },

    // Acciones
    guardar: function () {
        $("#TRF_USU_REGISTRO").val($("#UsReg").val());

        $.ajax({
            url: "/api/TX_CONCEPTOS_TARIFA_TB/insertar",
            method: "POST",
            data: $("#frm_conceptos_tarifa").serialize(),
            dataType: "text",
            success: function (data) {
                if (data.length === 0) {
                    principal.alertsSwal.msj_success(conceptos_tarifa.title, "Se Ingresaron los datos correctamente");
                } else {
                    principal.alertsSwal.msj_error(conceptos_tarifa.title, data);
                }
            },
            error: function (e) {
                principal.alertsSwal.msj_error(conceptos_tarifa.title, JSON.parse(e.responseText).Message);
            }
        });
    },

    actualizar: function () {
        $("#TRF_USU_REGISTRO").val($("#UsReg").val());

        var myform = $('#frm_conceptos_tarifa');
        var disabled = myform.find(':input:disabled').removeAttr('disabled');
        var serialized = myform.serialize();
        disabled.attr('disabled', 'disabled');

        $.ajax({
            url: "/api/TX_CONCEPTOS_TARIFA_TB/actualizar",
            method: "POST",
            data: serialized,
            dataType: "text",
            success: function (data) {
                if (data.length === 0) {
                    principal.alertsSwal.msj_success(conceptos_tarifa.title, "Se actualizaron los datos");
                } else {
                    principal.alertsSwal.msj_error(conceptos_tarifa.title, data);
                }
            },
            error: function (e) {
                principal.alertsSwal.msj_error(conceptos_tarifa.title, JSON.parse(e.responseText).Message);
            }
        });
    },

    limpiar: function () {
        principal.resetAllFields('frm_conceptos_tarifa');
        principal.resetAllFields('frm_conceptos_tarifa_busqueda');

        $("#btn_guardar").show();
        $("#btn_actualizar").hide();

        $("#conceptos_tarifa_busqueda_tablebody").html("");

        principal.deactivateLabels();
    },

    // buscar
    listar: function () {
        if ($('#div_conceptos_tarifa_busqueda').hasClass('fullHidden'))
            $('#div_conceptos_tarifa_busqueda').removeClass('fullHidden');
        if ($('#tbl_conceptos_tarifa_busqueda').hasClass('fullHidden'))
            $('#tbl_conceptos_tarifa_busqueda').removeClass('fullHidden');
        if (!$('#frm_conceptos_tarifa').hasClass('fullHidden'))
            $('#frm_conceptos_tarifa').addClass('fullHidden');

        $('#btn_regresar').show();
        $('#btn_listar').hide();
        $('#btn_guardar').hide();
        $('#btn_actualizar').hide();
        $('#btn_limpiar').hide();
    },

    regresar: function () {
        if (!$('#div_conceptos_tarifa_busqueda').hasClass('fullHidden'))
            $('#div_conceptos_tarifa_busqueda').addClass('fullHidden');
        if (!$('#tbl_conceptos_tarifa_busqueda').hasClass('fullHidden'))
            $('#tbl_conceptos_tarifa_busqueda').addClass('fullHidden');
        if ($('#frm_conceptos_tarifa').hasClass('fullHidden'))
            $('#frm_conceptos_tarifa').removeClass('fullHidden');

        $('#btn_regresar').hide();
        $('#btn_listar').show();
        $('#btn_guardar').show();
        $('#btn_actualizar').hide();
        $('#btn_limpiar').show();

        this.limpiar();
    },

    buscar: function () {
        $.ajax({
            url: '/api/TX_CONCEPTOS_TARIFA_TB/busqueda',
            data: $("#frm_conceptos_tarifa_busqueda").serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                var templateText = $("#conceptos_tarifa_busqueda_table_template").html();
                var tableTemplate = Handlebars.compile(templateText);
                $("#conceptos_tarifa_busqueda_tablebody").html('');
                $.each(data, function (index, elem) {
                    elem.JSON = JSON.stringify(elem);
                    $("#conceptos_tarifa_busqueda_tablebody").append(tableTemplate(elem));
                });
            },
            error: function (data) {
                principal.alertsSwal.msj_error(conceptos_tarifa.title, data.responseJSON.Message);
            }
        });
    },

    editar: function (strJson) {
        var data = JSON.parse(strJson);
        this.regresar();
        $("#btn_guardar").hide();
        $("#btn_actualizar").show();
        $.each(data, function (field, value) {
            principal.setValueByName(field, value, 'frm_conceptos_tarifa');
        });
        principal.activeLabels();
    }

};

$(document).ready(function () {
    conceptos_tarifa.init();
});