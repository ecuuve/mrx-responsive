﻿var consulta_guias = {
    me: null,
    mercanciasData: [],
    init: function () {
        me = this;
        this.LimpiarCampos();
        this.CargaFechas();
        this.CargarControlesPrevios();
        this.Eventos();
    },

    LimpiarCampos: function () {
        principal.resetAllFields('container-fluid');
        TecladoFuncionalidadPersonalizada();
    },

    CargaFechas: function () {
        //me.KendoDate($("#MUE_FECHA"));
    },

    KendoDate: function (control) {
        control.kendoDatePicker({
            format: "dd/MM/yyyy",
            value: new Date(),
            // min: new Date(minDate),
            // max: new Date(),
            month: {
                empty: '<span class="k-state-disabled">#=data.value#</span>'
            }
        });
    },

    CargarControlesPrevios: function () {
    },

    Eventos: function () {
        $("#btn_search").on("click", function (e) {
            e.preventDefault();
            me.BuscarGuia();
        })
    },

    getDatos: function (filtro, cb, api) {
        principal.get(
            api,
            { 'filtro': filtro },
            cb
        )
    },

    //Controles iniciales
    getMotivo: function () {
        principal.getData(
            "/api/TX_REGISTRO_MUESTRAS_TB/Motivo",
            function (data) {
                $("#MUE_CODIGO_MOTIVO").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        )
    },

// Procesos de consulta e inserción
    BuscarGuia: function () {
        var s_mercancia = $("#s_mercancia").val();
        var s_consecutivo = $("#s_consecutivo").val();

        me.BuscarGuias(s_mercancia, s_consecutivo);

    },

    BuscarGuias: function (s_mercancia, s_consecutivo) {
        $.ajax({
            method: 'POST',
            dataType: 'JSON',
            url: '/api/TX_CONSULTA_GUIAS_TB/BuscarGuia',
            data: { ien_entrada_p: s_consecutivo, ien_guia_original_p: s_mercancia },
            success: function (data) {
                if (data !== null) {
                    if (principal.isArray(data) && data.length > 1) {
                        me.MostrarListaGuias(data);
                    } else {
                        if (typeof data === 'string' || data instanceof String) {
                            swal({
                                title: "Consulta Guias",
                                text: data,
                                icon: "error",
                                button: "OK!",
                            })
                        } else {
                            if (data["ien_entrada"] != 0) {
                                //trae los datos de la queja
                                me.ActiveCampos();
                                me.MuestraDatos(data);
                                me.MuestraDatosMercancia(data["ien_entrada"]);
                            } else {
                                swal({
                                    title: "Consulta Guias",
                                    text: "No existen datos para los valores consultados.",
                                    icon: "error",
                                    button: "OK!",
                                })

                            }
                        }

                    }
                }
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(textStatus);
                //console.log("error consulta guía");
            }
        });
    },
    MuestraDatosMercancia: function (mercancia) {
        $.ajax({
            url: "/api/TX_CONSULTA_GUIAS_TB/BuscarMercancias",
            data: { mer_ien_entrada_p: mercancia },
            method: "POST",
            dataType: "json",
            success: function (data) {
                me.mercanciasData = data;
                var templateText = $("#mercancias-template").html();
                var tableTemplate = Handlebars.compile(templateText);
                $("#mercancias_tablebody").html('');
                $.each(data, function (index, elem) {
                    $("#mercancias_tablebody").append(tableTemplate(elem));
                })
            },
            error: function (err) {

            }
        });
    },

    MuestraDatosMercanciaMore: function (index) {
        if (me.mercanciasData[index]) {
            var data = me.mercanciasData[index];
            for (var campo in data) {
                if (data[campo] != null) {
                    principal.setValueByName(campo, data[campo], "frm_mercancias_detalle");
                }
            }
        }
    },

    MostrarListaGuias: function (data) {
        var templateText = $("#guias-template").html();
        var tableTemplate = Handlebars.compile(templateText);
        $("#guias_tablebody").html('');
        $.each(data, function (index, elem) {
            $("#guias_tablebody").append(tableTemplate(elem));
        })
        $("#myModalGuias").modal('show');
    },

    MostrarGuiaSeleccionado: function (guia) {
        me.BuscarGuias(s_mue_muestra);
        $("#myModalGuias").modal('hide');
    },
    MuestraDatos: function (data) {
        for (var campo in data) {           
            if(data[campo] != null){
                principal.setValueByName(campo, data[campo],'frm_consulta_guias');
            }  
        }
    },

    limpiarFiltros: function () {
        $("#s_mue_muestra").val("");
        principal.resetAllFields('detInfo_registro_muestras');
        principal.resetAllFields('detInfo_detalle');
        principal.ActivarTab('registro_muestras');
    },

    ActiveCampos: function () {
        $('label').addClass('active');
    },

    DeactiveCampos: function () {
        $('label').removeClass('active');
    },
    LimpiarCamposDetalle: function () {
        principal.resetAllFields('detInfo_detalle');
        $("#btn_add_item").attr('style', 'display:block');
        $("#btn_edit_item").attr('style', 'display:none');
        $("#btn_canceledit_item").attr('style', 'display:none');
    }
};

$(document).ready(function () {
    consulta_guias.init();
})