//Metodos para el funcionamiento con WebAppi
var contenedor = {

    init: function () {
        $('#th_contenedor').hide();
        $('#div_filters').hide();
        TecladoFuncionalidadPersonalizada();
        contenedor.BuscarContenedorEnter();
        contenedor.limpiarCamposContenedor();
        contenedor.CargaControlesContenedor();
        if($('#in_tco_num_contenedor').val()!="")
        {
            contenedor.BuscarContenedor($('#in_tco_tma_manifiesto').val(),$('#in_tco_contenedor').val());
            $("#in_tco_num_contenedor").attr('disabled', true);
        }
        
        contenedor.cambiarEstadoBotones(false, false, true, false, true, false, true, false);
        $("#lbl_tco_num_contenedor").focus();
    },

    onfocus: function () {
        if ($("#lbl_tco_num_contenedor").hasClass('lblfocus'))
            $("#lbl_tco_num_contenedor").removeClass('lblfocus');
    },



    //Regresa a la pantalla principal del formulario contenedor
    Regresar: function () {
        principal.hidemenu();
        $("#th_contenedor").hide();
        $('#div_filters').hide();
        contenedor.cambiarEstadoBotones(false, false, true, false, true, false, true, false);
        $('#detalles-contenedor').show();
        principal.activeLabels();
    },

    //Metodo para carga la lista de contenedores
    Refrescar: function () {
        $('#detalles-contenedor').hide();
        //contenedor.limpiarCamposContenedor();
        $('#th_contenedor').show();
        $('#div_filters').show();
        contenedor.limpiarCamposFiltrosContenedor();
        contenedor.ListarContenedor($('#in_tco_tma_manifiesto').val(),$('#in_tco_contenedor').val());
        contenedor.cambiarEstadoBotones(false, true, true, true, false, true, true,true);
    },

    //Carga los select del form
    CargaControlesContenedor: function () {
        contenedor.getGrupos();
        contenedor.getDocumentos();
        contenedor.getAduanas();
        contenedor.getEstados();
        $("#dtp_tco_fechaManIngreso").kendoDatePicker({
          format: "dd/MM/yyyy",
          value: new Date(),
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });
        $("#dtp_tco_fechaManIngreso").data("kendoDatePicker").enable();
    },

    //Llena select de Aduanas
    getAduanas: function () {
        principal.getData(
            "/api/TX_MANIFIESTOS_TB/Aduanas",
            function (data) {
                $("#s_tco_adu_aduana").html(principal.arrayToOptions(data));
            }
        )
    },

    //Llena select de Documentos
    getDocumentos: function () {
        principal.getData(
            "/api/TX_CONTENEDORES_TB/Documentos",
            function (data) {
                $("#s_tco_tdo_documento").html(principal.arrayToOptions(data));
            }
        )
    },

    //Llena select de Estados
    getEstados: function () {
        principal.getData(
            "/api/TX_CONTENEDORES_TB/Estados",
            function (data) {
                $("#s_tco_estado").html(principal.arrayToOptionsSelected(data, 'R'));
            }
        )
    },

    //Llena select de Grupos de trabajo
    getGrupos: function () {
        principal.getData(
            "/api/TX_CONTENEDORES_TB/Grupos",
            function (data) {
                $("#s_tco_grupo_trabajo").html(principal.arrayToOptions(data));
            }
        )
    },

    //Listener del cambio en el textbox de contenedor que haria la llamada al metodo ajax para buscar el contenedor
    BuscarContenedorEnter: function () {
        $('#in_tco_num_contenedor').change(function () {
            contenedor.BuscarContenedor($('#in_tco_tma_manifiesto').val(),$('#in_tco_contenedor').val());
        });
    },

    //Cambia el estado de los botones de la cinta principal 
    cambiarEstadoBotones: function (listar, guardar, editar, limpiar, filtro, manifiesto, guias, regresar) {
        //contenedor.cambiarEstadoBotones(false, true, false, false, true, false, false, false);
        $('#btn_listar_contenedor').prop('disabled', listar);
        $('#btn_guardar_contenedor').prop('disabled', guardar);
        $('#btn_editar_contenedor').prop('disabled', editar);
        $('#btn_limpiar_contenedor').prop('disabled', limpiar);
        $('#btn_filters_contenedor').prop('disabled', filtro);
        $('#btn_manifiesto_contenedor').prop('disabled', manifiesto);
        $('#btn_guias_contenedor').prop('disabled', guias);       
        if (regresar == true) {
            $("#btn_regresar_contenedor").show();
        }
        else {
            $("#btn_regresar_contenedor").hide();
        }
    },

    //Metodo para guardar el nuevo contenedor
    GuardarContenedor: function () {
        $("#TCO_USU_REGISTRO").val($("#UsReg").val());
        $("#in_tco_tma_manifiesto").attr('disabled', false);
        $("#s_tco_estado").attr('disabled', false);
                        
        $.post(
                "/api/TX_CONTENEDORES_TB/",
                 $('#frm_contenedor').serialize(),
                function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Contenedores",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                        $("#in_tco_tma_manifiesto").attr('disabled', true);
                        $("#s_tco_estado").attr('disabled', true);
                    }else{

                        swal({
                        title: "Contenedores",
                        text: "Contenedores guardado exitosamente",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            contenedor.limpiarCamposContenedor();
                        });
                    }

                })
                .fail(function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                });
    },

    //Ejecuta la actualizacion del contenedor
    Actualizar: function () {
        var id = $('#in_tco_contenedor').val();
        if (id.length > 0) {

            $("#in_tco_tma_manifiesto").attr('disabled', false);
            $("#in_tco_num_contenedor").attr('disabled', false);
            
            $("#TCO_USU_MODIFICO").val($("#UsReg").val());

            var data = principal.jsonForm($('#frm_contenedor').serializeArray());
            $.ajax({
                url: '/api/TX_CONTENEDORES_TB/' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Contenedores",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                        $("#in_tco_tma_manifiesto").attr('disabled', true);
                        $("#in_tco_num_contenedor").attr('disabled', true);
                        $("#s_tco_estado").attr('disabled', true);
                        
                    }else{

                        swal({
                        title: "Contenedores",
                        text: "Se actualizó correctamente el contenedor",
                        icon: "success",
                        button: "OK!",

                        })
                       contenedor.limpiarCamposContenedor();
                    }  
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                    $("#in_tco_tma_manifiesto").attr('disabled', true);
                    $("#in_tco_num_contenedor").attr('disabled', true);
                    $("#s_tco_estado").attr('disabled', true);
                }
            })
        }
        else {
            swal({
                title: "Contenedores",
                text: "El ID del contenedor no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    },

    //Accion del boton que me carga la informacion en el formulario de contenedor
    Editar: function (id) {
        //principal.hidemenu();
        contenedor.limpiarCamposContenedor();
        contenedor.BuscarContenedor($('#in_tco_tma_manifiesto').val(),id);
        $("#th_contenedor").hide();
        $('#div_filters').hide();
        $('#detalles-contenedor').show();
    },

    //Funcion Ajax para la busqueda de datos de contenedor
    BuscarContenedor: function (p_manifiesto,p_contenedor) {
        if (p_manifiesto != null) {

           var p_consolidador = null;
           var p_num_contenedor = $("#in_tco_num_contenedor").val();

            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_CONTENEDORES_TB/GetContenedores?tco_contenedor_p='+p_contenedor+ '&tco_tma_manifiesto_p='+p_manifiesto+ '&tco_con_nconsolidador_p='+p_consolidador+ '&tco_num_contenedor_p='+p_num_contenedor,
                success: function (data) {
                    if (data.length == 1) {
                        //Valido que el estado del manifiesto no sea cerrado
                        if (data[0].TCO_ESTADO != 'C') {
                            contenedor.MuestraContenedor(data[0]);                         
                            contenedor.cambiarEstadoBotones(false, true, false, false, true, false, false, false);
                            $("#in_tco_num_contenedor").attr('disabled', true);
                            $("#s_tco_estado").attr('disabled', false);
                        }
                        else {
                            contenedor.limpiarCamposContenedor();
                            swal({
                                title: "Contenedor",
                                text: "Estado de contenedor " + data[0].TCO_CONTENEDOR + " es cerrado! ",
                                icon: "warning",
                                button: "OK!",

                            });
                        }

                    }else{

                         //Obtenemos la plantilla
                        var templateText = $("#tb-contenedor-template").html();
                        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                        var tableTemplate = Handlebars.compile(templateText);
                        $("#tb_contenedores").html('');
                        //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                            $.each(data, function (index, elem) {
                                $("#tb_contenedores").append(tableTemplate(elem));
                            })
                        //});
                    }
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    swal({
                        title: "Contenedor",
                        text: "Error Buscando el contenedor " + data.TCO_CONTENEDOR,
                        icon: "error",
                        button: "OK!",

                    });
                    console.log("error");
                }
            });
        }
    },

    //Funcion Ajax para la busqueda de datos de contenedor
    ListarContenedor: function (p_manifiesto,p_contenedor) {
        if (p_manifiesto != null) {

           var p_consolidador = null;
           var p_num_contenedor = $("#in_tco_num_contenedor").val();

            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_CONTENEDORES_TB/GetContenedores?tco_contenedor_p='+p_contenedor+ '&tco_tma_manifiesto_p='+p_manifiesto+ '&tco_con_nconsolidador_p='+p_consolidador+ '&tco_num_contenedor_p='+p_num_contenedor,
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                        title: "Contenedores",
                        text: data,
                        icon: "error",
                        button: "OK!",                       
                        })

                    }else{
                         //Obtenemos la plantilla
                        var templateText = $("#tb-contenedor-template").html();
                        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                        var tableTemplate = Handlebars.compile(templateText);
                        $("#tb_contenedores").html('');
                        //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                            $.each(data, function (index, elem) {
                                $("#tb_contenedores").append(tableTemplate(elem));
                            })
                        //});
                    }
                        
                    
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    swal({
                        title: "Contenedor",
                        text: "Error Buscando el contenedor " + data.TCO_CONTENEDOR,
                        icon: "error",
                        button: "OK!",

                    });
                    console.log("error");
                }
            });
        }
    },

    //Muestra campos de filtro en la lista
    FiltrosContenedor: function () {
        $('#div_filters').show();
    },

    //Oculta campos de filtro en la lista
    OcultarFiltrosContenedor: function () {
        $('#div_filters').hide();
    },

    //Ejecuta los filtros que se hayan seleccionado en la pantalla de Contenedor
    BusquedaFiltros: function () {
        principal.filterTable_Input('in_flr_contenedor', 'tb_contenedor', 0);
        principal.filterTable_Input('in_flr_viaje', 'tb_contenedor', 2);
    },

    //Limpia los campos del formulario de contenedores
    limpiarCamposContenedor: function () {
        if($('#TCO_CONTENEDOR_ID').val() !== "" && $('#in_tco_num_contenedor').val() == ""  ){
             $('#TCO_CONTENEDOR_ID').val($('#in_tco_contenedor').val());
             $('#TCO_MANIFIESTO_ID').val($('#in_tco_tma_manifiesto').val());

             principal.resetAllFields('detalles_contenedor');
             principal.deactivateLabels();

             $('#in_tco_tma_manifiesto').val($('#TCO_MANIFIESTO_ID').val());
             //$('lbl_mgu_manifiesto').addClass('active');
             $('#TCO_MANIFIESTO_ID').val("");
             $('#in_tco_contenedor').val($('#TCO_CONTENEDOR_ID').val());
             //$('lbl_mgu_guia').addClass('active');
             $('#TCO_CONTENEDOR_ID').val("");

             //$('#pasoTarima').val("");
        }else{

            $('#TCO_MANIFIESTO_ID').val($('#in_tco_tma_manifiesto').val());
             principal.resetAllFields('detalles_contenedor');
             principal.deactivateLabels();
            $('#in_tco_tma_manifiesto').val($('#TCO_MANIFIESTO_ID').val());
             $('#lbl_tco_tma_manifiesto').addClass('active');
            /*$("#in_mgu_guia").attr('disabled', true);
            $('#lbl_mgu_guia').addClass('active');
            $('#in_mgu_guiatica').focus();*/

        }
        if($('#in_tco_contenedor').val() == ""){
        contenedor.cambiarEstadoBotones(false, false, true, false, true, false, true, false);
        }

        $("#in_tco_num_contenedor").attr('disabled', false);
        $("#s_tco_estado").attr('disabled', true);
        $("#in_tco_tma_manifiesto").attr('disabled', true);

        contenedor.CargaControlesContenedor();
    },

    //Limpia los filtros de contenedor
    limpiarCamposFiltrosContenedor: function () {
        $('#in_flr_contenedor').val("");
        $('#in_flr_viaje').val("");
    },

    //Asignacion de la informacion devuelta en los campos de la pantalla Contenedor
    MuestraContenedor: function (p_contenedor) {
        for (var campo in p_contenedor) {
            if(p_contenedor[campo] != null){
                principal.setValueByName(campo, p_contenedor[campo],'frm_contenedor');
            }
        }
        //Agrego class Active a todos los labels
        principal.activeLabels();
    },

    //Muestra la lista de consolidadores en un modal.
    MostrarListaConsolidadores: function(){
        $("#myModalConsolidadores").modal('show');
        //Obtenemos la plantilla
        var templateText = $("#TCO_Consolida_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#TCO_Consolida_tablebody").html('');
        $.getJSON('/api/TX_CONTENEDORES_TB/Consolidadores').then(function (data) {
            $.each(data, function (index, elem) {
                $("#TCO_Consolida_tablebody").append(tableTemplate(elem));
            })
        });
    },
    //Muestra la lista de funcionarios aduana en un modal.
    MostrarFuncionariosAduana: function(){
        $("#myModalFuncionariosAduana").modal('show');
        //Obtenemos la plantilla
        var templateText = $("#TCO_FuncionariosAduana_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#TCO_FuncionariosAduana_tablebody").html('');
        $.getJSON('/api/TX_CONTENEDORES_TB/FuncionariosAduana').then(function (data) {
            $.each(data, function (index, elem) {
                $("#TCO_FuncionariosAduana_tablebody").append(tableTemplate(elem));
            })
        });
    },


    Escoger:function(id,nombre){
       /*var p_idRegistro = prm_id;
       var p_tipo = $("#s_flr_rcl_tipo").val();
       var p_tipo_cliente = $("#s_flr_rcl_tipocliente").val();
       reclamos.RCL_ConsultarDatos(p_idRegistro,p_tipo,p_tipo_cliente);*/
       $("#in_tco_con_consolidador").val(id);
       $("#in_consolidador").val(nombre);
       $("#myModalConsolidadores").modal('hide');
       $("#myModalFuncionariosAduana").modal('hide');
    },

    EscogerFuncionario:function(id,nombre){
       $("#in_tco_func_aduana_ced").val(id);
       $("#in_tco_func_aduana_nom").val(nombre);
       $("#myModalFuncionariosAduana").modal('hide');
    },
    
    //Pasa a la pantalla de guías
    pasarGuia: function () {
        var manifiesto = $('#in_tco_tma_manifiesto').val();
        window.location.href = "/Guias/Index/" + manifiesto;
    },

    //Regresa a la pantalla de manifiesto
    pasarManifiesto: function () {
        var manifiesto = $('#in_tco_tma_manifiesto').val();
        window.location.href = "/Manifiesto/Index/" + manifiesto;
    },
};

$(document).ready(function () {
    contenedor.init();
})


