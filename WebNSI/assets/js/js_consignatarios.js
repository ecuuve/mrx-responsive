var consignatario = {
     pickedup : null,
    init: function () {
      TecladoFuncionalidadPersonalizada();
      consignatario.CNS_limpiar();
      $("#btn_actualizar").attr('style', 'display:none');
      $("#btn_imp").attr('style', 'display:none');
      
    },

    onfocus: function () {
        if ($("#lbl_mov").hasClass('lblfocus'))
            $("#lbl_mov").removeClass('lblfocus');
    },

    CNS_Listar:function(){      
        var cb = function(){
            consignatario.CNS_ClickTable();
        };
        consignatario.CNS_Listar_Consig(cb);
    },

    CNS_ClickTable:function(){
        $("#Consignatarios_Table > tbody > tr").on("click", function(e) {
            var conse = $(this).find("td").eq(0).html();
            //var ubicacion = $(this).find("td").eq(1).html();

            consignatario.CNS_Funcionalidad_Click(conse, $( this ));             
        });
    },

    CNS_Listar_Consig: function(callback){
        //Buscar lista de mercancías según filtros
        var p_MOV = $("#in_MOV").val();
        var p_GUIA = $("#in_GUIA").val();
        var p_VUELO = $("#in_VUELO").val();
        var p_CONSOLID = $("#in_CONSOLID").val();
        //Obtenemos la plantilla
        var templateText = $("#Consignatarios_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#Consignatarios_table1body").html('');
        $.getJSON('/api/TX_CONSIGNATARIOS_TB/GetConsignatarios?movi='+ p_MOV + '&guia=' + p_GUIA + '&vuelo=' + p_VUELO + '&consolid=' + p_CONSOLID).then(function (data) {
            $.each(data, function (index, elem) {
                $("#Consignatarios_table1body").append(tableTemplate(elem));
            })

            if(typeof(callback) == "function"){
                callback();
            }
        });   
    },

    CNS_Funcionalidad_Click: function(conse_p, linea){  
         //regresa al color negro antes de haberlo seleccionado
         if (consignatario.pickedup != null) {
             consignatario.pickedup.css( "background-color", "#3C3C3C" );
         }
         //cambia el color de la fila a gris
         $( linea ).css( "background-color", "grey" );
 
         consignatario.pickedup = $( linea );

         consignatario.CNS_CargaInputsUpdate(conse_p);
    },

    CNS_CargaInputsUpdate: function(consecutivo_p) {
        $("#in_up_consecutivo").val(consecutivo_p);
        $('#lbl_consecutivo').addClass('active');
        $("#btn_actualizar").attr('style', 'display:block');
        $("#btn_imp").attr('style', 'display:block');
        
    },

    CNS_MostrarListaImportador: function () {
        $("#myModal").modal('show');
        $("#divProcessing").show();
        consignatario.CNS_BuscarImportadores();      
    },

    CNS_BuscarImportadores: function () {
       
        var p_codimportador = "";
        var p_nomimportador = "";
        //var p_popup = 0;
        $('.modal-body #cns_flt_cod_importador,.modal-body textarea').each(function () {
            p_codimportador = $(this).val();
        });
        $(".modal-body #cns_flt_nom_importador,.modal-body textarea").each(function () {
            p_nomimportador = $(this).val();
        });

        //Obtenemos la plantilla
        var templateText = $("#CNS_Importador_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $(".modal-body #CNS_Importador_table1body,.modal-body textarea").each(function () {
            $(this).html('');
        });
        if (p_codimportador !== "" || p_nomimportador !== "") {
            $.getJSON('/api/TX_CONSIGNATARIOS_TB/GetImportadores?id=' + p_codimportador+'&nombre='+p_nomimportador).then(function (data) {
                $.each(data, function (index, elem) {
                    $("#CNS_Importador_table1body").append(tableTemplate(elem));
                })
                $("#divProcessing").hide();
            });
        }
        else {
            $.getJSON('/api/TX_CONSIGNATARIOS_TB/GetImportadores?id=' + p_codimportador+'&nombre='+p_nomimportador).then(function (data) {
                $.each(data, function (index, elem) {
                    $("#CNS_Importador_table1body").append(tableTemplate(elem));
                })
                $("#divProcessing").hide();
            });
        }
    },

    CNS_MuestraCedulaImp:function(id){
        $('#lbl_cedulaimp').addClass('active');
        $("#in_up_imp_ced").val(id);
        $("#myModal").modal('hide');
    },


    CNS_LimpiarFiltrosImportador: function(){
        $("#cns_flt_cod_importador").val("");
        $("#cns_flt_nom_importador").val("");
    },

    CNS_limpiar: function () {
        $("#in_MOV").val("");
        $("#in_GUIA").val("");
        $("#in_VUELO").val("");
        $("#in_CONSOLID").val("");
        $("#in_up_consecutivo").val("");
        $("#in_up_imp_ced").val("");
        $("#Consignatarios_table1body").html('');
        principal.deactivateLabels();
        $("#in_MOV").focus();
        consignatario.CNS_Listar();  
        $("#btn_actualizar").attr('style', 'display:none');
        $("#btn_imp").attr('style', 'display:none');    
    },


    CNS_Actualizar: function(){      
        var ced = $('#in_up_imp_ced').val();
        var id = $('#in_up_consecutivo').val();
        if (ced.length > 0) {
           // var data = principal.jsonForm($('#frm_cajera').serializeArray());
            //$("#CJR_CAJERA").attr('disabled', true);   
            $.ajax({
                url: '/api/TX_CONSIGNATARIOS_TB/PutConsignatario?id=' + id+'&cedula='+ced,
                type: 'PUT',
                //data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    // if (typeof data === 'string' || data instanceof String){
                        
                    // }else{
                        swal({
                        title: "Consignatarios",
                        text: "Se actualizó correctamente la información",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                           consignatario.CNS_limpiar();
                        });
                   // }
                },
                error: function (data) {
                    swal({
                      title: "Actualización de consignatario",
                      text: data,
                      icon: "error",
                      button: "OK!",
                    })
                }
            })
        }
        else {
            swal({
                title: "Consignatarios",
                text: "Debe seleccionar un importador para realizar la actualización.",
                icon: "error",
                button: "OK!",

            })
        }
    }



};

$(document).ready(function () {
    consignatario.init();
})