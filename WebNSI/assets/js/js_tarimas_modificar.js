var tarimas_modificar = {
    ien_entrada: null,
    manifiesto: null,
    tipo_transporte: null,

    init: function () {
        principal.resetAllFields('frm_tarimas');
        tarimas_modificar.tipo_transporte = Cookies.get('tipo_transporte');
        TecladoFuncionalidadTarimas();
        tarimas_modificar.ien_entrada = Cookies.get('entrada');
        tarimas_modificar.id_manifiesto = Cookies.get('manifiesto');
        tarimas_modificar.getPuestosChekeo();
        tarimas_modificar.InfoInicial(tarimas_modificar.ien_entrada, tarimas_modificar.id_manifiesto);
        tarimas_modificar.getTarimasReferencia();
        tarimas_modificar.getTiposMercancia();
        tarimas_modificar.getTiposEmbalaje();
        tarimas_modificar.getTiposAverias();
        tarimas_modificar.getPeligrosidad();
        tarimas_modificar.getEstados();
        tarimas_modificar.getOperacionTica();
        tarimas_modificar.getMotivos();
        tarimas_modificar.getDepartamentos();
        $("#MER_TIP_CODIGO_IATA").change(function () {
            tarimas_modificar.validacionPeligrosidad();
        });
        tarimas_modificar.validacionNavegacion();
        $('#MER_DESCRIPCION').val(Cookies.get('descripcion'));
        $('#btn_regresar_tarimas').hide();
        $('#btn_limpiar_tarimas_filtros').hide();

        //Botones de accion de cerrar todas las tarimas abiertas
        $('#btn_cerrar').hide();
        $('#btn_selec').hide();
        $('#btn_deselec').hide();


        //Nuevo código FSUAREZ

        $("#MER_KILOS_TARIMA").change(function () {
            $('#MER_KILOS_NETO').val("");
            tarimas_modificar.calculoKilosNeto();
        });

        $("#MER_KILOS_BRUTO").change(function () {
            $('#MER_KILOS_NETO').val("");
            tarimas_modificar.calculoKilosNeto();
        });

        $('.number').on("cut copy paste", function (e) {
            e.preventDefault();
        });

        $('.number').keypress(function (event) {
            var $this = $(this);
            //Solo permite números
            if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
                ((event.which < 48 || event.which > 57) &&
                    (event.which != 0 && event.which != 8))) {
                event.preventDefault();
            }
            //Borra digitos extra si mueve el punto a un lugar que deja más de 3 decimales
            var text = $(this).val();
            if ((event.which == 46) && (text.indexOf('.') == -1)) {
                setTimeout(function () {
                    if ($this.val().substring($this.val().indexOf('.')).length > 4) {
                        $this.val($this.val().substring(0, $this.val().indexOf('.') + 4));
                    }
                }, 1);
            }

            if ((event.which == 46) && (text.indexOf('.') != -1)) {
                setTimeout(function () {
                    if ($this.val().substring($this.val().indexOf('.')).length > 4) {
                        $this.val($this.val().substring(0, $this.val().indexOf('.') + 4));
                    }
                }, 1);
            }

            //Permite solo 3 decimales y permite agregar más valores a la izq del punto
            //y no a la derecha si mueve cursor
            if ((text.indexOf('.') != -1) &&
                (text.substring(text.indexOf('.')).length > 3) &&
                (event.which != 0 && event.which != 8) &&
                ($(this)[0].selectionStart >= text.length - 3)) {
                event.preventDefault();
            }

            //No permite más de 17 dígitos antes del punto decimal. 
            if ((text.indexOf('.') != -1)) {
                var substr = text.split('.');

                if ((substr[0].length == 17) && ($(this)[0].selectionStart <= 17)) {
                    event.preventDefault();
                }
            }

            if ((text.indexOf('.') == -1) && (event.which != 46) && (text.length == 17)) {
                event.preventDefault();
            }
        });

        $('#frm_tarimas #MER_PUESTO_CHEQUEO').change(function () {
            if ($('#MER_PUESTO_CHEQUEO').val() == "") {
                swal({
                    title: "Tarimas",
                    text: 'Debe seleccionar un Puesto Chequeo.',
                    icon: "warning",
                    button: "OK!",
                });
            } else {
                $('#MER_BULTOS_DESPALETIZADOS').focus();
            }

        });


        // $('#MER_BULTOS_REF').keypress(function(event) {
        // 	tarimas_modificar.validacionBultosRef();
        // });

        $('#MER_TME_MERCANCIA_FILTROZ').change(function () {
            //var entrada = $('#IEN_ENTRADA').val();
            var mercancia = $('#tarima_flr_ien_entradaZ').val();
            var tipomercancia = $(this).val();
            if (mercancia == "") {
                mercancia = null;
            }
            sumatoria.buscarSumatoria(mercancia, tipomercancia);
        });


        $('#MER_TME_MERCANCIA_FILTRO').change(function () {
            var entrada = $('#IEN_ENTRADA').val();
            var mercancia = $('#tarima_flr_ien_entrada').val();
            var tipomercancia = $(this).val();
            if (mercancia == "") {
                mercancia = null;
            }
            tarimas_modificar.buscarTarimas(entrada, mercancia, tipomercancia);
        });

        $('#MER_TEM_EMBALAJE,#MER_OPERACION_TICA').change(function () {
            tarimas_modificar.validacionEmbalajeOperacionTica();
            //tarimas_modificar.getTarimasReferencia();
        });

        $('#MER_TME_MERCANCIA, #MER_REFRIGERADO').change(function () {
            tarimas_modificar.validacionRefrigerado();
        });

        $("#FRAC_REFERENCIA").change(function () {
            $("#tarimas_ref_tablebody").html('');
            var ref = $('#FRAC_REFERENCIA').val();
            if (ref != undefined && ref != "") {
                tarimas_modificar.buscarMovsFracciona();
                // $('#btn_actualiza_fracc').show();
            } else {
                $('#btn_actualiza_fracc').hide();
                $("#tarimas_ref_tablebody").html('');
            }

        });

        $('#MER_BULTOS_DESPALETIZADOS').focus();
    },

    onfocus: function () {
        if ($("#lbl_MER_BULTOS_DESPALETIZADOS").hasClass('lblfocus'))
            $("#lbl_MER_BULTOS_DESPALETIZADOS").removeClass('lblfocus');
    },

    //Filtra si se escribe mas de un caracter o si no existe ninguno
    filtrar: function (input) {
        var entrada = $('#IEN_ENTRADA').val();
        var mercancia = $('#tarima_flr_ien_entrada').val();
        var tipomercancia = $('#MER_TME_MERCANCIA_FILTRO').val();

        if (tipomercancia == "") {
            tipomercancia = null;
        }

        if (input.value.length > 1) {

            tarimas_modificar.buscarTarimas(entrada, mercancia, tipomercancia);

        } else {
            if (input.value.length = 0) {
                tarimas_modificar.buscarTarimas(entrada, null, tipomercancia);
            }
        }
    },

    // Carga el cuadro de encabezado
    InfoInicial: function (entrada, manifiesto) {
        principal.resetAllFields('frm_tarima_info');
        if (entrada != undefined) {
            let guiaOrig = '', consolidador = '', fecha = '', estado = '';
            let url = '/api/TX_ENTRADAS_TB_Ingreso/Guias_Consulta?manifiesto=' + manifiesto + '&guiaOrig=' + guiaOrig + '&entrada=' + entrada + '&consolidador=' + consolidador + '&fecha=' + fecha + '&estado=' + estado;
            var cb = function (data) {
                $.each(data[0], function (i, v) {
                    if (i == 'IEN_PUESTO_CHEQUEO') {
                        $('#MER_PUESTO_CHEQUEO').val(v);
                    }
                    principal.setValueByName(i, v, 'frm_tarima_info');
                    principal.setValueByName(i, v, 'frm_tarima_resumen');
                    $('#frm_tarima_resumen label').addClass('active');
                    $('#frm_tarima_info label').addClass('active');
                });
            };
            principal.ajax('GET', url, cb, "Tarimas");
        }
        $('#MER_DESCRIPCION').val(Cookies.get('descripcion'));
        $('#lbl_MER_DESCRIPCION').addClass('active');

        principal.activeLabelsInput();
    },

    //Calcula el valor del campo kilos neto
    calculoKilosNeto: function () {
        var ktarima = $('#MER_KILOS_TARIMA').val();
        var kbruto = $('#MER_KILOS_BRUTO').val();
        if (ktarima == "") {
            ktarima = 0;
        }
        if (kbruto == "") {
            kbruto = 0;
        }
        var netos = (parseFloat(kbruto) - parseInt(ktarima));
        if (netos <= 0) {
            swal({
                title: "Tarimas",
                text: "Los Kilos Netos deben ser mayores a cero. Revisar Kilos Bruto y Kilos Tarima.",
                icon: "warning",
                button: "OK!",
            });
            $('#MER_KILOS_BRUTO').focus();
        } else {
            $('#MER_KILOS_NETO').val(netos);
            $('#lbl_MER_KILOS_NETO').addClass('active');
        }
    },

    ////////////////////////// Carga de LV /////////////////////////////////

    //Lista los puestos chekeo 
    getPuestosChekeo: function () {
        var cb = function (data) {
            $("#MER_PUESTO_CHEQUEO").html(principal.arrayToOptions(data));
            //principal.KendoComboBox(data, '#IEN_PUESTO_CHEQUEO');
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Portones?tipo_p=" + "T", '', cb);
    },

    //Lista los tipos de mercancía
    getTiposMercancia: function () {
        var cb = function (data) {
            principal.KendoComboBox(data, '#MER_TME_MERCANCIA');
            principal.KendoComboBox(data, '#MER_TME_MERCANCIA_FILTRO');
            principal.KendoComboBox(data, '#MER_TME_MERCANCIA_FILTROZ');
        };
        principal.get("/api/TX_MERCANCIAS_TB/tiposMercancias?tipo=" + '', '', cb);
    },

    //Lista los tipos de embalaje
    getTiposEmbalaje: function () {
        var cb = function (data) {
            principal.KendoComboBox(data, '#MER_TEM_EMBALAJE');
        };
        principal.get("/api/TX_MERCANCIAS_TB/tiposEmbalaje?embalaje=" + '' + '&entrada=' + tarimas_modificar.ien_entrada, '', cb);
    },

    // Lista los tipos de avería
    getTiposAverias: function () {
        principal.getData("/api/TX_MERCANCIAS_TB/tiposAveria?tipo=" + '', function (data) { $("#MER_AVE_AVERIA").html(principal.arrayToOptions(data)); });
    },

    //Lista las peligrosidades
    getPeligrosidad: function () {
        var cb = function (data) {
            principal.KendoComboBox(data, '#MER_TIP_CODIGO_IATA');
        };
        principal.get("/api/TX_MERCANCIAS_TB/peligrocidadIata?tipo=" + '', '', cb);
    },

    //Lista los estados
    getEstados: function () {
        principal.getData("/api/TX_MERCANCIAS_TB/Estados", function (data) { $("#MER_ESTADO").html(principal.arrayToOptionsSelected(data, 'A')); });
    },

    // Lista las operaciones tica
    getOperacionTica: function () {
        principal.getData("/api/TX_MERCANCIAS_TB/OperacionTica", function (data) { $("#MER_OPERACION_TICA").html(principal.arrayToOptions(data)); });
    },

    //Obtiene las tarimas que pueden ser referencia 
    getTarimasReferencia: function () {
        //if ($('#MER_OPERACION_TICA').val() == 'F') {
        var dua = '';
        principal.getData("/api/TX_MERCANCIAS_TB/TarimasReferencia?conse=" + tarimas_modificar.ien_entrada,
            function (data) {
                if (data.length == 1) {
                    if ($('#frm_tarimas #MER_ID_MERCANCIA_REF').prop('disabled') == false) {
                        $("#frm_tarimas #MER_ID_MERCANCIA_REF").html(principal.arrayToOptionsSelected(data, 0));
                    } else {
                        $("#frm_tarimas #MER_ID_MERCANCIA_REF").html(principal.arrayToOptions(data));
                    }
                } else {
                    $("#frm_tarimas #MER_ID_MERCANCIA_REF").html(principal.arrayToOptions(data));
                }
            }
        );
        //}
    },

    //Obtiene las tarimas que pueden ser referencia en pantalla de Fraccionamiento
    getTarimasRefFracc: function () {
        principal.getData("/api/TX_MERCANCIAS_TB/TarimasReferencia?conse=" + tarimas_modificar.ien_entrada,
            function (data) {
                $("#FRAC_REFERENCIA").html(principal.arrayToOptions(data));
            }
        );
    },


    getMotivos: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Motivos", function (data) { $("#MER_MOT_ULTIMA_MODIFICACION").html(principal.arrayToOptions(data)); });
    },

    getDepartamentos: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Departamentos", function (data) { $("#MER_DEPARTAMENTO").html(principal.arrayToOptions(data)); });
    },

    ////////////////////////// Validaciones /////////////////////////////////

    //Bultos y kilos mayores a cero
    validaMayorCero: function () {
        var bultos = parseInt($('#MER_BULTOS_DESPALETIZADOS').val());
        var kilos = parseInt($('#MER_KILOS_BRUTO').val());
        if (bultos <= 0 || kilos <= 0) {
            swal({
                title: "Tarimas",
                text: 'Bultos tarima y Kilos (Bruto) no pueden ser menores o iguales a 0',
                icon: "error",
                button: "OK!",
            });
        }
    },

    //valida si el embalaje seleccionado es 'PAE','BDL','BSK','CNT' o 'GUA' y la operación tica es F debe mostrar un mensaje
    validacionEmbalajeOperacionTica: function () {
        let array = ['PAE', 'BDL', 'BSK', 'CNT', 'GUA', ''], value = $("#MER_TEM_EMBALAJE").data('kendoComboBox').value();
        if ($.inArray(value, array) != -1) {
            if ($('#MER_OPERACION_TICA').val() == 'F') {
                principal.alertsSwal.msj_alerta('Embalaje', 'Tipo de embalaje no corresponde a operación Fraccionamiento.');
                return 1;
            }
        }
    },

    //Validacion de Refrigerado: 	si el check está marcado, pero el tipo de mercancía no es 8 ni 15 o si el check no está marcado y el tipo de mercancía es 8 o 15 debe mostrar un error
    validacionRefrigerado: function () {
        let mercancia = $("#MER_TME_MERCANCIA").data('kendoComboBox').value();
        switch (mercancia) {
            case '8':
                if (!$('#MER_REFRIGERADO').is(":checked")) {
                    principal.alertsSwal.msj_error('Refrigerado', 'Combinación entre check de refrigerado y el tipo de mercancía no corresponde.');
                    return 1;
                }
                break;
            case '15':
                if (!$('#MER_REFRIGERADO').is(":checked")) {
                    principal.alertsSwal.msj_error('Refrigerado', 'Combinación entre check de refrigerado y el tipo de mercancía no corresponde.');
                    return 1;
                }
                break;
            default:
                if ($('#MER_REFRIGERADO').is(":checked")) {
                    principal.alertsSwal.msj_error('Refrigerado', 'Combinación entre check de refrigerado y el tipo de mercancía no corresponde.');
                    return 1;
                }
        }
    },

    //Validación de peligrosidad
    validacionPeligrosidad: function () {
        let peligrosidad = $("#MER_TIP_CODIGO_IATA").data('kendoComboBox').value();

        if (peligrosidad != "7") {
            $('#MER_INDICE_TRANS').val('');
        }
        if (peligrosidad == "9") {
            $('#MER_CODIGO_UN').val('');
            $('#MER_INDICE_TRANS').val('');
        }
        if (peligrosidad == "0") {
            $('#MER_CODIGO_UN').val('');
            $('#frm_tarimas #MER_CODIGO_UN').prop('disabled', true);
            $('#frm_tarimas #MER_INDICE_TRANS').prop('disabled', true);
        } else {
            $('#frm_tarimas #MER_CODIGO_UN').prop('disabled', false);
            $('#frm_tarimas #MER_INDICE_TRANS').prop('disabled', false);
        }
    },

    //se valida que si la peligrosidad IATA es diferente de Cero y el código un es nulo o Cero muestra un mensaje de error
    //. Si la peligrosidad IATA es Cero y el código un no es nulo debe mostrar un mensaje de error
    validacionCodigo: function () {
        let peligrosidad = $("#MER_TIP_CODIGO_IATA").data('kendoComboBox').value();
        if (peligrosidad != "0" && peligrosidad != "" && ($("#MER_CODIGO_UN").val() == '' || $("#MER_CODIGO_UN").val() == "0")) {

            swal({
                title: "Código UN",
                text: "Valor de código U.N incorrecto.",
                icon: "warning",
                button: "OK!",

            }).then((value) => {
                $("#MER_CODIGO_UN").focus();
                return 1;
            });
            // principal.alertsSwal.msj_error('Código','Valor de código U.N incorrecto.');
            // return 1;
        }
        if (peligrosidad == "0" && $("#MER_CODIGO_UN").val() != '') {

            swal({
                title: "Código UN",
                text: "No requiere código U.N",
                icon: "warning",
                button: "OK!",

            }).then((value) => {
                $("#MER_CODIGO_UN").focus();
                //$("#MER_CODIGO_UN").val("");
                return 1;
            });
            // principal.alertsSwal.msj_error('Código','No requiere código U.N');
            // return 1;
        }
    },

    //valida si la peligrosidad IATA es 7 y índice de transporte es nulo o Cero muestra el mensaje de error
    //Si la peligrosidad IATA es 0 y el índice transporte no es nulo debe indicar
    validacionIndice: function () {
        let peligrosidad = $("#MER_TIP_CODIGO_IATA").data('kendoComboBox').value();
        if (peligrosidad == "7" && ($("#MER_INDICE_TRANS").val() == '' || $("#MER_INDICE_TRANS").val() == "0")) {

            swal({
                title: "Índice",
                text: "Debe ingresar índice de transporte para radioactivos.",
                icon: "warning",
                button: "OK!",

            }).then((value) => {
                $("#MER_INDICE_TRANS").focus();
                return 1;
            });
            // principal.alertsSwal.msj_error('Índice','Debe ingresar índice de transporte para radioactivos.');
            // $("#MER_INDICE_TRANS").focus();
            //return 1;
        }
        if (peligrosidad == "0" && $("#MER_INDICE_TRANS").val() != '') {
            swal({
                title: "Índice",
                text: "No requiere índice de transporte.",
                icon: "warning",
                button: "OK!",

            }).then((value) => {
                $("#MER_INDICE_TRANS").focus();
                return 1;
            });
            // principal.alertsSwal.msj_error('Índice','No requiere índice de transporte.');
            // $("#MER_INDICE_TRANS").focus();
            //return 1;
        }
    },

    validacionOperacion: function () {
        if ($('#MER_OPERACION_TICA').val() == 'F') {
            tarimas_modificar.getTarimasReferencia();
            $('#frm_tarimas #MER_ID_MERCANCIA_REF').prop('disabled', false);
        } else {
            $('#frm_tarimas #MER_ID_MERCANCIA_REF').prop('disabled', true);
            tarimas_modificar.getTarimasReferencia();
        }

        //tarimas_modificar.validacionTarimaReferencia();
    },

    //Validacion de referencia y operación
    validacionTarimaReferencia: function () {
        let mercancia = $("#MER_ID_MERCANCIA_REF").val();
        let operacionTica = $('#MER_OPERACION_TICA').val();
        if (mercancia != '') {
            if (operacionTica != 'F') {
                principal.alertsSwal.msj_error('Referencia', 'Operación no permite digitar referencia.');
                return 1;
            } else {
                $.ajax({
                    url: '/api/TX_MERCANCIAS_TB/GetTarimaRef?mercancia=' + mercancia + '&operaTica=' + operacionTica,
                    //url: '/api/TX_MERCANCIAS_TB/GetMercanciasIngreso?guia=&mercancia=' + mercancia + '&tipomercancia=',
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        $("#bultosRef").val(data);
                    },
                    error: function (data) {
                        swal({
                            title: "Tarima Referencia",
                            text: data.responseJSON.Message,
                            icon: "error",
                            button: "OK!",
                        }).then(() => {
                            $('#MER_ID_MERCANCIA_REF').focus();
                        });
                        return 1;
                    }
                });
            }
        } else {
            if ($('#MER_OPERACION_TICA').val() == 'F') {
                principal.alertsSwal.msj_alerta('Tarima Referencia', 'Si operación es fraccionamiento debe digitar referencia.');
                return 1;
            }
        }
    },

    //Valida cantidad de bultos ref
    validacionBultosRef: function () {
        if (parseInt($('#MER_BULTOS_REF').val()) > parseInt($('#bultosRef').val())) {
            swal({
                title: "Bultos Referencia",
                text: 'Bultos referencia no puede ser mayor a bultos disponibles en tarima. Total disponible:' + $('#bultosRef').val(),
                icon: "error",
                button: "OK!",
            }).then(() => {
                $('#MER_BULTOS_REF').val('');
                $('#MER_BULTOS_REF').focus();
            });
        }
    },

    //Valida si el rack es diferente de 66 y el estado es diferente de V debe mostrar un mensaje,Si el estado es V y el Rack es diferente de Cero, muestro el mensaje
    validacionRack: function () { //ya no se usa se quitan los campos de localización
        if ($('#MER_LOC_RACK').val() != "66" && $('#MER_ESTADO').val() != "V")

            swal({
                title: "Localización / Estado",
                text: "Solo se permite localizar en piso (66).",
                icon: "warning",
                button: "OK!",

            }).then((value) => {
                $("#MER_LOC_RACK").focus();
                $("#MER_LOC_RACK").val("66");
                //return 1;
            });
        //principal.alertsSwal.msj_error('Rack','Solo se permite localizar en piso (66).');

        if ($('#MER_LOC_RACK').val() != 0 && $('#MER_ESTADO').val() == "V")

            swal({
                title: "Localización / Estado",
                text: "Las virtuales deben localizarse en posición cero.",
                icon: "warning",
                button: "OK!",

            }).then((value) => {
                $("#MER_LOC_RACK").focus();
                $("#MER_LOC_RACK").val("0");
                //return 1;
            });
        //principal.alertsSwal.msj_error('Rack','Las virtuales deben localizarse en posición cero.');
    },

    //Si la modalidad es marítimo y terrestre, habilitar espacio de dimensiones: Largo, Alto, Ancho
    validacionNavegacion: function () {
        if (tarimas_modificar.tipo_transporte == 1 || tarimas_modificar.tipo_transporte == 7) {
            var div = $('#frm_tarimas #MER_LARGO').closest("div.form-group");
            if (div.hasClass('disabled'))
                div.removeClass('disabled');
            $('#frm_tarimas #MER_LARGO').prop('disabled', false);
            var div = $('#frm_tarimas #MER_ALTO').closest("div.form-group");
            if (div.hasClass('disabled'))
                div.removeClass('disabled');
            $('#frm_tarimas #MER_ALTO').prop('disabled', false);
            var div = $('#frm_tarimas #MER_ANCHO').closest("div.form-group");
            if (div.hasClass('disabled'))
                div.removeClass('disabled');
            $('#frm_tarimas #MER_ANCHO').prop('disabled', false);
        }
    },

    ////////////////////////// Acciones/////////////////////////////////

    //Marca todos los radio button de las tarimas 
    SeleccionaTodas: function () {
        var conn = 0;
        $("#tarimas_cons_table tbody tr").each(function (i, e) {
            if ($(this).find("input[id=rd_C]").prop('checked') == false) {
                $(this).find("input[id=rd_C]").prop('checked', true);
                conn++;
            } else {
                conn++;
            }
        });

        if (conn == 0) {
            swal({
                title: "Información",
                text: "No existen tarimas abiertas para cerrar.",
                icon: "warning",
                button: "OK!",
            });
        } else {
            //Botones de accion de cerrar todas las tarimas abiertas
            $('#btn_cerrar').show();
            $('#btn_selec').hide();
            $('#btn_deselec').show();
        }
    },

    //Desmarca todos los radio button de las tarimas
    DesSeleccionaTodas: function () {
        $("#tarimas_cons_table tbody tr").each(function (i, e) {

            if ($(this).find("input[id=rd_C]").prop('checked') == true) {
                $(this).find("input[id=rd_C]").prop('checked', false);
            }
        });

        //Botones de accion de cerrar todas las tarimas abiertas
        //$('#btn_cerrar').hide();
        $('#btn_selec').show();
        $('#btn_deselec').hide();
    },

    //Crea arreglo de las tarimas seleccionadas
    Tarimas_Cerrar: function () {
        var id_Merc = "";
        var user_Merx = $("#USER_MERX").val();
        var arrEdit = [];

        $("#tarimas_cons_table tbody tr").each(function (i, e) {

            id_Merc = $(this).find("td").eq(0).html(); //Con que mando a cerrar?         

            if ($(this).find("input[id=rd_C]").is(':checked')) {
                arrEdit.push({
                    'idM': id_Merc,
                    'user': user_Merx
                });
            }
        });
        //console.log(arrEdit);
        if (arrEdit.length == 0) {
            swal({
                title: "Información",
                text: "No ha seleccionado tarimas para cerrar.",
                icon: "warning",
                button: "OK!",
            });
        } else {
            tarimas_modificar.CargaTarimasCerrar(arrEdit);
        }
    },

    //Validación para proceder a cerrar todas las tarimas.
    CargaTarimasCerrar: function (listaTarimas) {
        var conocimiento = $("#IEN_GUIA_ORIGINAL_Z").val();
        swal({
            title: "Confirmación?",
            text: "Desea cerrar las tarimas seleccionadas del Conocimiento:" + conocimiento + "?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    tarimas_modificar.Cerrar(listaTarimas);
                }
            });
    },

    //Ejecuta acción de cerrar las tarimas de la lista.
    Cerrar: function (listaTarimas) {
        $.ajax({
            url: '/api/TX_MERCANCIAS_TB/CerrarTarimas',
            type: 'POST',
            data: JSON.stringify(listaTarimas),
            dataType: 'json',
            contentType: "application/json",
            success: function (data) {
                swal({
                    title: "Tarimas",
                    text: "Se cerraron las tarimas seleccionadas.",
                    icon: "success",
                    button: "OK!",
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            tarimas_modificar.buscar();
                            //Botones de accion de cerrar todas las tarimas abiertas
                            $('#btn_cerrar').show();
                            $('#btn_selec').show();
                            $('#btn_deselec').hide();
                        }
                    });

            },
            error: function (data) {
                swal({
                    title: "Error",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                });
            }
        })
    },

    //Borra la guia abierta 
    Tarima_Borrar: function (id) {
        swal({
            title: "Está seguro?",
            text: "Eliminará la información de la tarima: " + id,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    tarimas_modificar.Eliminar(id);
                }
            });
    },

    //Borra la tarima abierta 
    Eliminar: function (id) {
        var user_Merx = $("#USER_MERX").val();
        if (id.length > 0) {
            $.ajax({
                url: '/api/TX_MERCANCIAS_TB/Borrar?id=' + id + '&user=' + user_Merx,
                type: 'DELETE',
                contentType: "application/json",
                success: function (data) {
                    swal({
                        title: "Tarimas",
                        text: "Se eliminó correctamente la información",
                        icon: "success",
                        button: "OK!",

                    }).then((value) => {

                        tarimas_modificar.RefrescarTarimas();
                        tarimas_modificar.limpiarPostBorrado();
                    });
                },
                error: function (data) {
                    swal({
                        title: "Eliminar Tarima",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",
                    })
                }
            })
        }
        else {
            swal({
                title: "Tarimas",
                text: "El ID de la tarima no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    },

    //Muestra lista de fraccionamientos
    Fraccionar: function () {

        if (!$('#div_tarimas_filtrosZ').hasClass('fullHidden'))
            $('#div_tarimas_filtrosZ').addClass('fullHidden')

        if(!$('#div_tarimas_filtros').hasClass('fullHidden'))
            $('#div_tarimas_filtros').addClass('fullHidden')


        if (!$('#tbl_tarimas').hasClass('fullHidden'))
            $('#tbl_tarimas').addClass('fullHidden')
        if (!$('#tbl_tarimas_cons').hasClass('fullHidden'))
            $('#tbl_tarimas_cons').addClass('fullHidden')

        if ($('#tbl_tarimas_ref').hasClass('fullHidden'))
            $('#tbl_tarimas_ref').removeClass('fullHidden')

        if (!$('#frm_tarimas').hasClass('fullHidden'))
            $('#frm_tarimas').addClass('fullHidden')
        if (!$('#frm_tarima_resumen').hasClass('fullHidden'))
            $('#frm_tarima_resumen').addClass('fullHidden')

        if ($('#fracci').hasClass('fullHidden'))
            $('#fracci').removeClass('fullHidden')

        $('#btn_listar_tarimas_consec').hide();
        $('#btn_listar_tarimas').hide();
        $('#btn_guardar_tarimas').hide();
        $('#btn_editar_tarimas').hide();
        $('#btn_limpiar_tarimas').hide();
        $('#btn_regresar_tarimas').show();
        $('#btn_regresar_conocimientos').hide();
        $('#btn_fracciona_tarimas').hide();

        $('#btn_actualiza_fracc').hide();
        //Botones de accion de cerrar todas las tarimas abiertas
        $('#btn_cerrar').hide();
        $('#btn_selec').hide();
        $('#btn_deselec').hide();

        $("#tarimas_ref_tablebody").html('');
        tarimas_modificar.getTarimasRefFracc();
    },

    //Lista los movimientos
    buscarMovsFracciona: function () {
        var ref = $('#FRAC_REFERENCIA').val();
        if (ref != undefined && ref != "") {
            var url = '/api/TX_MERCANCIAS_TB/MovsFracciona?conse=' + tarimas_modificar.ien_entrada + '&tarimaref=' + ref;
            var cb = function (data) {

                if (data.length != 0) {
                    $('#btn_actualiza_fracc').show();
                    var templateText = $("#tarimas_ref_table-template").html();
                    //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                    var tableTemplate = Handlebars.compile(templateText);
                    $("#tarimas_ref_tablebody").html('');
                    //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                    $.each(data, function (index, elem) {
                        $("#tarimas_ref_tablebody").append(tableTemplate(elem));
                    });
                } else {
                    $('#btn_actualiza_fracc').hide();
                }


                //    tarimas_modificar.InfoInicial();
                //    if($('#frm_tarima_resumen').hasClass('fullHidden'))
                // $('#frm_tarima_resumen').removeClass('fullHidden')

            };
            principal.ajax('GET', url, cb, "Fraccionamiento");
        }
    },

    //Calcula los bultos Ref en fraccionamiento
    CalcularBultosRef: function () {
        var tarimaref = $("#FRAC_REFERENCIA").val();
        var bultosref = $("#FRAC_BULTOS").val();
        var user_Merx = $("#USER_MERX").val();

        if (bultosref != "" && bultosref != undefined) {
            $.ajax({
                url: '/api/TX_MERCANCIAS_TB/CalculaBultosRef?conse=' + tarimas_modificar.ien_entrada + '&tarimaref=' + tarimaref + '&bultosref=' + bultosref + '&user=' + user_Merx,
                type: 'POST',
                contentType: "application/json",
                success: function (data) {
                    swal({
                        title: "Tarimas - Fraccionamiento",
                        text: "Se actualizó correctamente la información",
                        icon: "success",
                        button: "OK!",
                    }).then((value) => {
                        $("#FRAC_BULTOS").val("");
                        tarimas_modificar.buscarMovsFracciona();
                    });
                },
                error: function (data) {
                    swal({
                        title: "Tarimas - Fraccionamiento",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",
                    });
                }
            });
        } else {
            swal({
                title: "Tarimas - Fraccionamiento",
                text: "Debe digitar los bultos de la tarima seleccionada",
                icon: "warning",
                button: "OK!",
            });

        }
    },

    //Muestra total de tarimas de la guía 
    Refrescar: function () {
        if (!$('#tbl_tarimas').hasClass('fullHidden'))
            $('#tbl_tarimas').addClass('fullHidden')

        if ($('#tbl_tarimas_cons').hasClass('fullHidden'))
            $('#tbl_tarimas_cons').removeClass('fullHidden')

        if (!$('#frm_tarimas').hasClass('fullHidden'))
            $('#frm_tarimas').addClass('fullHidden')

        if ($('#frm_tarima_resumen').hasClass('fullHidden'))
            $('#frm_tarima_resumen').removeClass('fullHidden')

        if ($('#div_tarimas_filtrosZ').hasClass('fullHidden'))
            $('#div_tarimas_filtrosZ').removeClass('fullHidden')


        $('#btn_listar_tarimas_consec').hide();
        $('#btn_listar_tarimas').hide();
        $('#btn_guardar_tarimas').hide();
        $('#btn_editar_tarimas').hide();
        $('#btn_limpiar_tarimas').hide();
        $('#btn_regresar_tarimas').show();
        $('#btn_regresar_conocimientos').hide();

        $('#btn_limpiar_tarimas_filtros').show();

        //Botones de accion de cerrar todas las tarimas abiertas
        $('#btn_cerrar').show();
        $('#btn_selec').show();
        $('#btn_deselec').hide();



        sumatoria.buscarSumatoria(null, null);
    },



    // Lista las tarima abiertas de la guía
    RefrescarTarimas: function () {
        //tarimas_modificar.limpiaFiltros();

        $('#tarima_flr_ien_entrada').val('');
        $("#MER_TME_MERCANCIA_FILTRO").data("kendoComboBox").value("");

        if ($('#div_tarimas_filtros').hasClass('fullHidden'))
            $('#div_tarimas_filtros').removeClass('fullHidden')
        if ($('#tbl_tarimas').hasClass('fullHidden'))
            $('#tbl_tarimas').removeClass('fullHidden')
        if (!$('#tbl_tarimas_cons').hasClass('fullHidden'))
            $('#tbl_tarimas_cons').addClass('fullHidden')
        if (!$('#frm_tarimas').hasClass('fullHidden'))
            $('#frm_tarimas').addClass('fullHidden')

        if ($('#frm_tarima_resumen').hasClass('fullHidden'))
            $('#frm_tarima_resumen').removeClass('fullHidden')

        $('#btn_listar_tarimas_consec').hide();
        $('#btn_listar_tarimas').hide();
        $('#btn_guardar_tarimas').hide();
        $('#btn_editar_tarimas').hide();
        $('#btn_limpiar_tarimas').hide();
        $('#btn_regresar_tarimas').show();
        $('#btn_limpiar_tarimas_filtros').show();
        $('#btn_regresar_conocimientos').hide();

        $('#tarima_flr_ien_entrada').focus();


        var entrada = $('#IEN_ENTRADA').val();
        var mercancia = null;
        var tipomercancia = null;
        tarimas_modificar.buscarTarimas(entrada, mercancia, tipomercancia);
    },

    //Busca la tarima según el filtro sean 1 o n tarimas
    buscarTarimas: function (entrada_p, mercancia_p, tipomercancia_p) {
        //var entrada = $('#IEN_ENTRADA').val();
        if (entrada_p != undefined || entrada_p != "") {
            //var mercancia = null;
            var url = '/api/TX_MERCANCIAS_TB/GetMercanciasModificar?guia=' + entrada_p + '&mercancia=' + mercancia_p + '&tipomercancia=' + tipomercancia_p;
            var cb = function (data) {
                var templateText = $("#tarimas_table-template").html();
                //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                var tableTemplate = Handlebars.compile(templateText);
                $("#tarimas_tablebody").html('');
                //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                $.each(data, function (index, elem) {
                    $("#tarimas_tablebody").append(tableTemplate(elem));
                });
            };
            principal.ajax('GET', url, cb, "Tarimas");
        }
    },

    //Guarda la  nueva tarima
    guardar: function () {
        // var error = 0;
        // error = tarimas_modificar.validacionRefrigerado();
        // error = tarimas_modificar.validacionEmbalajeOperacionTica();
        // tarimas_modificar.validacionPeligrosidad();
        // error = tarimas_modificar.validacionCodigo();
        // error = tarimas_modificar.validacionIndice();
        // error = tarimas_modificar.validacionTarimaReferencia();

        var error = 0;
        error = tarimas_modificar.validacionRefrigerado();
        if (error != 1) {
            error = tarimas_modificar.validacionEmbalajeOperacionTica();
            if (error != 1) {
                error = tarimas_modificar.validacionEmbalajeOperacionTica();
                if (error != 1) {
                    error = tarimas_modificar.validacionCodigo();
                    if (error != 1) {
                        error = tarimas_modificar.validacionIndice();
                        if (error != 1) {
                            //error = tarimas_modificar.validacionTarimaReferencia();
                        }

                        if (error != 1) {

                            tarimas_modificar.validacionPeligrosidad();

                            if (error == 0 || error == undefined) {
                                $("#MER_ID_MERCANCIA").attr('disabled', false);
                                $("#MER_LARGO").attr('disabled', false);
                                $("#MER_ALTO").attr('disabled', false);
                                $("#MER_ANCHO").attr('disabled', false);
                                $("#IEN_MANIFIESTO").attr('disabled', false);
                                $("#IEN_ENTRADA").attr('disabled', false);
                                $("#IEN_GUIA_ORIGINAL").attr('disabled', false);
                                $("#TOT_KILOS").attr('disabled', false);
                                $("#TOT_BULTOS").attr('disabled', false);
                                //$("#MER_ESTADO").attr('disabled', false); 
                                $.post(
                                    '/api/TX_MERCANCIAS_TB/GuardarMercancia',
                                    $('#frm_tarimas').serialize(),
                                    function (data) {
                                        if (data != null) {

                                            swal({
                                                title: "Tarimas",
                                                text: "Tarima guardada exitosamente",
                                                icon: "success",
                                                button: "OK!",
                                            }).then((value) => {
                                                var estado = $("#MER_ESTADO").val();
                                                if (estado == "A") {
                                                    tarimas_modificar.ImprimirEtiqueta(parseInt(data));
                                                }
                                                if (estado == "V") {
                                                    tarimas_modificar.getTarimasReferencia();
                                                }
                                                tarimas_modificar.limpiar();
                                            });
                                        }
                                    })
                                    .fail(function (data) {
                                        if (data.responseJSON.ModelState) {
                                            principal.processErrorsPopUp(data.responseJSON.ModelState);
                                        } else {
                                            swal({
                                                title: "Tarimas",
                                                text: data.responseJSON.Message,
                                                icon: "error",
                                                button: "OK!",
                                            });
                                        }
                                    });
                                $("#MER_ID_MERCANCIA").attr('disabled', true);
                                // $("#MER_LARGO").attr('disabled', true); 
                                // $("#MER_ALTO").attr('disabled', true); 
                                // $("#MER_ANCHO").attr('disabled', true); 
                                $("#IEN_MANIFIESTO").attr('disabled', true);
                                $("#IEN_ENTRADA").attr('disabled', true);
                                $("#IEN_GUIA_ORIGINAL").attr('disabled', true);
                                $("#TOT_KILOS").attr('disabled', true);
                                $("#TOT_BULTOS").attr('disabled', true);
                                //$("#MER_ESTADO").attr('disabled', true); 
                            }
                        }
                    }
                }
            }
        }
    },

    //Actualiza la tarima
    Actualizar: function () {
        $("#MER_USER_MERX").val($("#USER_MERX").val());
        var error = 0;
        error = tarimas_modificar.validacionRefrigerado();
        if (error != 1) {
            error = tarimas_modificar.validacionEmbalajeOperacionTica();
            if (error != 1) {
                error = tarimas_modificar.validacionEmbalajeOperacionTica();
                if (error != 1) {
                    error = tarimas_modificar.validacionCodigo();
                    if (error != 1) {
                        error = tarimas_modificar.validacionIndice();
                        if (error != 1) {
                            //error = tarimas_modificar.validacionTarimaReferencia();
                        }
                    }
                }
            }
        }

        tarimas_modificar.validacionPeligrosidad();

        if (error != 1) {
            $("#MER_ID_MERCANCIA").attr('disabled', false);
            $("#MER_LARGO").attr('disabled', false);
            $("#MER_ALTO").attr('disabled', false);
            $("#MER_ANCHO").attr('disabled', false);
            $("#IEN_MANIFIESTO").attr('disabled', false);
            $("#IEN_GUIA_ORIGINAL").attr('disabled', false);
            $("#TOT_KILOS").attr('disabled', false);
            $("#TOT_BULTOS").attr('disabled', false);
            $("#IEN_ENTRADA").attr('disabled', false);
            var id = $("#USER_MERX").val();
            var data = principal.jsonForm($('#frm_tarimas').serializeArray());
            $.ajax({
                //url: '/api/TX_MERCANCIAS_TB/Actualizar?usuario=' + id,
                url: '/api/TX_MERCANCIAS_TB/Mercancias_Modificar_Post',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (data != null) {
                        swal({
                            title: "Tarimas",
                            text: "Se actualizó correctamente la tarima",
                            icon: "success",
                            button: "OK!",
                        }).then((value) => {
                            var estado = $("#MER_ESTADO").val();
                            if (estado == "V") {
                                tarimas_modificar.getTarimasReferencia();
                            }
                            tarimas_modificar.limpiar();
                            tarimas_modificar.regresar();
                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                    else {
                        swal({
                            title: "Tarimas",
                            text: data.responseJSON.Message,
                            icon: "error",
                            button: "OK!",
                        });
                    }
                }
            });
            $("#MER_ID_MERCANCIA").attr('disabled', true);
            // $("#MER_LARGO").attr('disabled', true); 
            // $("#MER_ALTO").attr('disabled', true); 
            // $("#MER_ANCHO").attr('disabled', true); 
            $("#IEN_MANIFIESTO").attr('disabled', true);
            $("#IEN_ENTRADA").attr('disabled', true);
            $("#IEN_GUIA_ORIGINAL").attr('disabled', true);
            $("#TOT_KILOS").attr('disabled', true);
            $("#TOT_BULTOS").attr('disabled', true);
        }
    },

    //Carga la info de la tarima seleccionada
    editar: function (id) {
        //$("#MER_ESTADO").attr('disabled', false);
        tarimas_modificar.buscarTarimasEdit(id);
        if (!$('#tbl_tarimas').hasClass('fullHidden'))
            $('#tbl_tarimas').addClass('fullHidden')
        if (!$('#tbl_tarimas_cons').hasClass('fullHidden'))
            $('#tbl_tarimas_cons').addClass('fullHidden')
        if ($('#frm_tarimas').hasClass('fullHidden'))
            $('#frm_tarimas').removeClass('fullHidden')
        $('#btn_listar_tarimas').show();
        $('#btn_guardar_tarimas').hide();
        $('#btn_editar_tarimas').show();
        $('#btn_regresar_tarimas').hide();
        $('#btn_limpiar_tarimas').show();
        $('#btn_limpiar_tarimas_filtros').hide();
        if ($('#btn_editar_tarimas').hasClass('fullHidden'))
            $('#btn_editar_tarimas').removeClass('fullHidden');
        if (!$('#div_tarimas_filtros').hasClass('fullHidden'))
            $('#div_tarimas_filtros').addClass('fullHidden')
        if (!$('#div_tarimas_filtrosZ').hasClass('fullHidden'))
            $('#div_tarimas_filtrosZ').addClass('fullHidden')
        $('#btn_listar_tarimas_consec').show();
        $('#btn_regresar_conocimientos').show();

        if (!$('#frm_tarima_resumen').hasClass('fullHidden'))
            $('#frm_tarima_resumen').addClass('fullHidden')
    },

    //Busca la tarima que se va editar
    buscarTarimasEdit: function (p_identificador) {
        // principal.resetAllFields('frm_tarimas');
        var entrada = $('#IEN_ENTRADA').val();
        if (entrada != undefined || entrada != "") {
            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_MERCANCIAS_TB/GetMercanciasModificar?guia=' + entrada + '&mercancia=' + p_identificador + '&tipomercancia=' + null,
                success: function (data) {
                    tarimas_modificar.InfoInicial(tarimas_modificar.ien_entrada, tarimas_modificar.id_manifiesto);
                    //tarimas_modificar.InfoInicial(tarimas_modificar.id_manifiesto);
                    $.each(data[0], function (i, v) {
                        if (i === 'MER_OPERACION_TICA') {
                            if (v == 'F') {
                                $('#frm_tarimas #MER_ID_MERCANCIA_REF').prop('disabled', false);
                            }
                        }
                        principal.setValueByName(i, v, 'frm_tarimas');
                        if (i === 'MER_TIP_CODIGO_IATA') {
                            tarimas_modificar.validacionPeligrosidad();
                        }
                    });

                    tarimas_modificar.calculoKilosNeto();
                    $('#TarimaTitulo').text($('#MER_ID_MERCANCIA').val());
                    principal.activeLabelsInput();
                   
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    swal({
                        title: "Tarimas",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            });
        }
    },

    //Vuelve de la pantalla de lista a la de ingreso de tarima
    regresar: function () {
        if (!$('#div_tarimas_filtros').hasClass('fullHidden'))
            $('#div_tarimas_filtros').addClass('fullHidden')

        if (!$('#div_tarimas_filtrosZ').hasClass('fullHidden'))
            $('#div_tarimas_filtrosZ').addClass('fullHidden')

        if (!$('#tbl_tarimas_cons').hasClass('fullHidden'))
            $('#tbl_tarimas_cons').addClass('fullHidden')

        if (!$('#tbl_tarimas').hasClass('fullHidden'))
            $('#tbl_tarimas').addClass('fullHidden')

        if ($('#frm_tarimas').hasClass('fullHidden'))
            $('#frm_tarimas').removeClass('fullHidden')

        if (!$('#tbl_tarimas_ref').hasClass('fullHidden'))
            $('#tbl_tarimas_ref').addClass('fullHidden')

        if (!$('#fracci').hasClass('fullHidden'))
            $('#fracci').addClass('fullHidden')

        $('#btn_listar_tarimas').show();
        if ($("#TarimaTitulo").text() != "") {
            $('#btn_editar_tarimas').show();
            $('#btn_guardar_tarimas').hide();
        } else {
            $('#btn_editar_tarimas').hide();
            $('#btn_guardar_tarimas').show();
        }

        $('#btn_limpiar_tarimas').show();

        //Botones de accion de cerrar todas las tarimas abiertas
        $('#btn_cerrar').hide();
        $('#btn_selec').hide();
        $('#btn_deselec').hide();

        $('#btn_regresar_tarimas').hide();
        $('#btn_listar_tarimas_consec').show();
        $('#btn_limpiar_tarimas_filtros').hide();
        $('#btn_regresar_conocimientos').show();
        $('#btn_fracciona_tarimas').show();

        if (!$('#frm_tarima_resumen').hasClass('fullHidden'))
            $('#frm_tarima_resumen').addClass('fullHidden')
    },

    //Limpia los campos de la pantalla de ingresar tarima
    limpiar: function () {
        principal.resetAllFields('frm_tarimas');
        $('#TarimaTitulo').text("");
        $("#MER_TME_MERCANCIA").data("kendoComboBox").value("");
        $("#MER_TEM_EMBALAJE").data("kendoComboBox").value("");
        $("#MER_TIP_CODIGO_IATA").data("kendoComboBox").value("");
        $('#frm_tarimas #MER_ID_MERCANCIA_REF').val("");
        $('#frm_tarimas #MER_ID_MERCANCIA_REF').prop('disabled', true);
        tarimas_modificar.getTarimasReferencia();
        tarimas_modificar.InfoInicial(tarimas_modificar.ien_entrada, tarimas_modificar.id_manifiesto);
        $('#btn_listar_tarimas').show();
        $('#btn_guardar_tarimas').show();
        $('#btn_limpiar_tarimas').show();
        $('#btn_editar_tarimas').hide();
        $('#btn_regresar_tarimas').hide();
        //$("#MER_ESTADO").attr('disabled', true);

        principal.deactiveLabelsInput();
        $('#lbl_MER_DESCRIPCION').addClass('active');
    },

    //Limpia los campos de la pantalla de borrar la tarima por si los datos estan cargados
    limpiarPostBorrado: function () {
        principal.resetAllFields('frm_tarimas');
        $('#TarimaTitulo').text("");
        $("#MER_TME_MERCANCIA").data("kendoComboBox").value("");
        $("#MER_TEM_EMBALAJE").data("kendoComboBox").value("");
        $("#MER_TIP_CODIGO_IATA").data("kendoComboBox").value("");
        $('#frm_tarimas #MER_ID_MERCANCIA_REF').val("");
        $('#frm_tarimas #MER_ID_MERCANCIA_REF').prop('disabled', true);
        tarimas_modificar.getTarimasReferencia();
        tarimas_modificar.InfoInicial(tarimas_modificar.ien_entrada, tarimas_modificar.id_manifiesto);
        principal.deactiveLabelsInput();
    },

    //Limpia los filtros en la pantalla de listar tarimas
    limpiaFiltros: function () {
        $('#tarima_flr_ien_entrada').val('');
        $("#MER_TME_MERCANCIA_FILTRO").data("kendoComboBox").value("");
        $('#tarima_flr_ien_entradaZ').val('');
        $("#MER_TME_MERCANCIA_FILTROZ").data("kendoComboBox").value("");
        var entrada = $('#IEN_ENTRADA').val();
        tarimas_modificar.buscarTarimas(entrada, null, null);
        sumatoria.buscarSumatoria(null, null);
    },

    ImprimirEtiqueta: function (tarima) {
        //window.open("http://webherediatest/WebEtiquetaMerx/FrmEtiquetaHR.aspx?etiqueta=" + tarima);
        //window.open("http://webherediatest/WebEtiquetaNCI/FrmEtiquetaHR.aspx?etiqueta="	 + tarima);	
        var tarima = "http://merxalmacenadora/WebEtiquetaNCI/FrmEtiquetaHR.aspx?etiqueta=" + tarima;
       // var tarima = "http://webherediatest/WebEtiquetaNCI/FrmEtiquetaHR.aspx?etiqueta=" + tarima;
        principal.popupCenter(tarima, 'Etiqueta', 750, 500);
    },

    //Redirecciona de regreso a conocimientos / guías
    redirectConocimientos: function () {
        window.location.href = "Conocimientos_Modificar";
    }
};
$(document).ready(function () {
    tarimas_modificar.init();
});