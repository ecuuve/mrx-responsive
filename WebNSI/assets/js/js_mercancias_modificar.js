var mercancias_modificar = {
    filter: null,
    tipo_cliente: null,
    aerolinea: null,
    codigo_cliente: null,
    codigo_aerolinea: null,

    init: function () {
        principal.resetAllFields('frm_manifiesto_modificar');
        TecladoFuncionalidadIngresoMercancia();
        mercancias_modificar.CargaInicial();
        //Si se selecciona una modalidad de transporte cambian validaciones de navegación y carga los transportistas correspondientes
        $('#TMA_MOD_TRANSPORTE').change(function () {
            Cookies.set('tipo_transporte', $(this).val());
            // ingreso_mercancia.getTransportistas();
            mercancias_modificar.navegacion();
            $('#TMA_MANIFIESTO').focus();
            mercancias_modificar.getTransportistas();
        });

        //Al seleccionar el tipo de cliente lista los clientes correspondientes
        $('#TMA_CLIENTE_TIPO').change(function () {
            mercancias_modificar.getClient();
        });

        $("#ingmerc_flr_fecha").on("keypress keyup blur", function (e) {
            var regExp = /[0-9\/]/;
            var value = String.fromCharCode(e.which) || e.key;
            console.log(e);
            // Only numbers, dots and commas
            if (!regExp.test(value)
                // && e.which == 188 // ,
                // && e.which == 190 // .
                && e.which != 8   // backspace
                && e.which != 46  // delete
                && (e.which < 37  // arrow keys
                    || e.which > 40)) {
                e.preventDefault();
                return false;
            }
        });
    },

    //Carga inicial de controles de la pantalla
    CargaInicial: function () {
        //Inicializacion de componentes de fecha
        principal.KendoDate($("#TMA_FCH_MANIFIESTO"), new Date());
        principal.KendoDate($("#TMA_FCH_INGRESO"), new Date());
        $("#ingmerc_flr_fecha").kendoDatePicker({
            format: "dd/MM/yyyy"
        });
        $('#btn_regresar_ingresoMerc').hide();
        $('#btn_conocimientos').hide();
        //Inicializacion de los comboboxKendo
        mercancias_modificar.getFuncionarios();
        mercancias_modificar.getPuertos();
        mercancias_modificar.getAduanas();
        mercancias_modificar.getTransportistasFiltro();
        //Inicializacion select normales
        mercancias_modificar.getTiposContenedor();
        mercancias_modificar.getModalidadesTransporte();
        mercancias_modificar.getTamanos();
        mercancias_modificar.getDocumentos();
        mercancias_modificar.getTipoCliente();
        mercancias_modificar.getEstados();
        mercancias_modificar.getMotivos();
        mercancias_modificar.getDepartamentos();
    },

    //Lista los tipos de contenedor
    getTiposContenedor: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Tipo_Contenedor", function (data) { $("#TMA_TIPO_CONTENEDOR").html(principal.arrayToOptions(data)); });
    },

    //Lista las modalidades de transporte
    getModalidadesTransporte: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/ModalidadesTransporte", function (data) { $("#TMA_MOD_TRANSPORTE").html(principal.arrayToOptions(data)); });
    },

    //Lista los tamaños
    getTamanos: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Tamano", function (data) { $("#TMA_TAMANO").html(principal.arrayToOptions(data)); });
    },

    //Lista los documentos
    getDocumentos: function () {
        var tipo_documento = null;
        var cb = function (data) {
            $("#TMA_TDO_DOCUMENTO").html(principal.arrayToOptions(data));
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Documentos?tipo_documento=" + tipo_documento, '', cb);
    },

    //Lista los estados
    getEstados: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Estados", function (data) { $("#TMA_ESTADO").html(principal.arrayToOptionsSelected(data, 'A')); });
    },

    getMotivos: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Motivos", function (data) { $("#TMA_MOT_MOTIVO").html(principal.arrayToOptions(data)); });
    },

    getDepartamentos: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/Departamentos", function (data) { $("#TMA_DEPARTAMENTO").html(principal.arrayToOptions(data)); });
    },
    //Lista los tipos de Clientes
    getTipoCliente: function () {
        principal.getData("/api/TX_MANIFIESTOS_TB/TiposClientes", function (data) { $("#TMA_CLIENTE_TIPO").html(principal.arrayToOptions(data)); });
    },

    //Lista los Tranportistas
    getTransportistas: function (e = null, m = null, c = null) {
        var modalidad = e != null ? e : $('#TMA_MOD_TRANSPORTE option:selected').val();
        //var aerolinea = m != null ? m : '';
        var aerolinea = '';
        var cod_aerolinea = c != null ? c : '';

        var cb = function (data) {
            principal.KendoComboBox(data, '#TMA_AER_NAEROLINEA');

            //Funcionalidad de carga de datos para la edición
            if (cod_aerolinea != null && mercancias_modificar.aerolinea != null) {

                $('#TMA_AER_NAEROLINEA').val(cod_aerolinea);
                $('#TMA_AER_NAEROLINEA > option').each(function (i, v) {
                    if ($(this).text() == mercancias_modificar.aerolinea) {
                        $("#TMA_AER_NAEROLINEA").data('kendoComboBox').select(i);
                    }
                });

                mercancias_modificar.aerolinea = null;
                mercancias_modificar.cod_aerolinea = null;
            }
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Transportistas?mod_transporte=" + modalidad + "&&aerolinea=" + aerolinea, '', cb);
    },

    //Lista los transportistas para la seccion de filtros
    getTransportistasFiltro: function () {
        var aerolinea = '';
        principal.getData("/api/TX_MANIFIESTOS_TB/TransportistasFiltro?aerolinea=" + aerolinea, function (data) { $("#ingmerc_flr_transportista").html(principal.arrayToOptions(data)); });
    },

    //Lista los clientes
    //Lista los clientes
    getClient: function (e = null, t = null, id = null) {
        var cliente = e != null ? e : '';
        //var cliente = '';
        var tipo_cliente = t != null ? t : $('#TMA_CLIENTE_TIPO option:selected').val();

        if (tipo_cliente != null) {
            $("#TMA_CLIENTE_ID").kendoComboBox({
                placeholder: "",
                dataTextField: "Nombre",
                dataValueField: "Id",
                filter: "contains",
                autoBind: true,
                minLength: 1,
                dataSource: {
                    serverFiltering: true,
                    transport: {
                        read: function (options) {
                            if (options.data.filter && options.data.filter.filters.length > 0) {
                                cliente = options.data.filter.filters[0].value;
                            }
                            $.ajax({
                                type: "GET",
                                url: "/api/TX_MANIFIESTOS_TB/Clientes?cliente=" + cliente + "&tipo_cliente=" + tipo_cliente,
                                success: function (data) {
                                    options.success(data);
                                }
                            });
                        }
                    }
                }
            });

            if (id != null) {
                $("#TMA_CLIENTE_ID").data("kendoComboBox").dataSource.read();
                $("#TMA_CLIENTE_ID").data("kendoComboBox").value(id);
            }
        }
    },

    //Lista los funcionarios y ejecuta el filtro
    getFuncionarios: function () {
        var cb = function (data) {
            principal.KendoComboBox(data, '#TMA_FUN_FUNCIONARIO');
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Funcionarios?id=" + '', '', cb);
    },

    //Lista los puertos
    getPuertos: function () {
        var cb = function (data) {
            principal.KendoComboBox(data, '#TMA_PTO_PUERTO');
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Puertos?id=" + '', '', cb);
    },

    //Lista las aduanas con los filtros kendo respectivos
    getAduanas: function () {
        var cb = function (data) {
            principal.KendoComboBox(data, '#TMA_ADU_ENVIA');
            principal.KendoComboBox(data, '#TMA_ADU_RECIBE');
        };
        principal.get("/api/TX_MANIFIESTOS_TB/Aduanas?id=" + '', '', cb);
    },

    //Validaciones de navegación de la página
    navegacion: function () {
        if ($('#TMA_MOD_TRANSPORTE').val() == 4) {
            $('[name="TMA_AER_NAEROLINEA_input"]').prop('tabIndex', 9);
            $('#TMA_AER_NAEROLINEA').prop('tabIndex', 9);
            $('#TMA_CODIGO_VUELO').prop('tabIndex', 15);
            $("input[name='tma_aer_naerolinea_input']").prop('tabIndex', 9);
        } else {
            $('#TMA_AER_NAEROLINEA').prop('tabIndex', '');
            $('[name="TMA_AER_NAEROLINEA_input"]').prop('tabIndex', '');
            $('#TMA_CODIGO_VUELO').prop('tabIndex', '');
            $("input[name='tma_aer_naerolinea_input']").prop('tabIndex', '');
        }
        if ($('#TMA_MOD_TRANSPORTE').val() == 1) {
            $('#TMA_CHEQUEADOR').prop('tabIndex', 17);
        } else {
            $('#TMA_CHEQUEADOR').prop('tabIndex', '');
        }
    },

    //Manejo de botones entre el listar y el formulario
    Refrescar: function () {
        mercancias_modificar.limpiaFiltros();
        if ($('#tbl_ingresoMerc').hasClass('fullHidden'))
            $('#tbl_ingresoMerc').removeClass('fullHidden')
        if ($('#div_ingresoMerc_filtros').hasClass('fullHidden'))
            $('#div_ingresoMerc_filtros').removeClass('fullHidden')
        if (!$('#frm_manifiesto_modificar').hasClass('fullHidden'))
            $('#frm_manifiesto_modificar').addClass('fullHidden')
        $('#btn_listar_ingresoMerc').hide();
        $('#btn_guardar_mercancia').hide();
        $('#btn_regresar_ingresoMerc').show();
        mercancias_modificar.buscar();
    },

    //Ejecuta la busqueda de un manifiestos mediante filtros especificos
    buscar: function () {
        var manifiesto_id = null;
        var tma_manifiesto_p = $('#ingmerc_flr_manifiesto').val();
        var tma_fch_manifiesto_p = $('#ingmerc_flr_fecha').val();
        var tma_aer_naerolinea_p = $('#ingmerc_flr_transportista').val();
        var cliente = $('#ingmerc_flr_cliente').val();
        var num_contenedor = $('#ingmerc_flr_num_contenedor').val();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MANIFIESTOS_TB/ManifiestoModificar?manifiesto_id=' + manifiesto_id + '&tma_manifiesto_p=' + tma_manifiesto_p + '&tma_fch_manifiesto_p=' + tma_fch_manifiesto_p + '&tma_aer_naerolinea_p=' + tma_aer_naerolinea_p + '&cliente=' + cliente + '&num_contenedor=' + num_contenedor,
            success: function (data) {
                //Obtenemos la plantilla
                var templateText = $("#ingresoMerc_table-template").html();
                //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                var tableTemplate = Handlebars.compile(templateText);
                $("#ingresoMerc_tablebody").html('');
                $.each(data, function (index, elem) {
                    $("#ingresoMerc_tablebody").append(tableTemplate(elem));
                });
            },
            error: function (data) {
                swal({
                    title: "Manifiesto",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
            }
        });
    },

    //Limpia todos los campos
    limpiar: function () {
        principal.resetAllFields('frm_manifiesto_modificar');
        var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd/MM/yyyy');
        $("#TMA_FCH_MANIFIESTO").val(todayDate);
        $("#TMA_FCH_INGRESO").val(todayDate);
        $("#TMA_FUN_FUNCIONARIO").data("kendoComboBox").value("");
        $("#TMA_PTO_PUERTO").data("kendoComboBox").value("");
        $("#TMA_ADU_ENVIA").data("kendoComboBox").value("");
        $("#TMA_ADU_RECIBE").data("kendoComboBox").value("");
        $('#btn_conocimientos').hide();
        $("#TMA_ESTADO").attr('disabled', true);

        if (!$('#frm_manifiesto_modificar').hasClass('fullHidden'))
            $('#btn_guardar_ingresoMerc').show();

        $('#btn_guardar_mercancia').hide();
        var ddl = $('#TMA_CLIENTE_ID').data("kendoComboBox");
        if (ddl != undefined)
            ddl.setDataSource([]);
        var ddl1 = $('#TMA_AER_NAEROLINEA').data("kendoComboBox");
        if (ddl1 != undefined)
            ddl1.setDataSource([]);
        mercancias_modificar.limpiaFiltros();
        manifiesto_duas.limpiar();
        manifiesto_viaje.limpiar();
    },

    //Filtra si se escribe mas de un caracter o si no existe ninguno
    filtrar: function (input) {
        if (input.value.length > 1) {
            mercancias_modificar.buscar();
        } else {
            if (input.value.length === 0) {
                mercancias_modificar.buscar();
            }
        }
    },

    //Limpia los filtros
    limpiaFiltros: function () {
        $('#ingmerc_flr_manifiesto').val('');
        $('#ingmerc_flr_fecha').val('');
        $('#ingmerc_flr_transportista').val('');
        $('#ingmerc_flr_cliente').val('');
        $('#ingmerc_flr_num_contenedor').val('');
        mercancias_modificar.buscar();
    },

    //Si está en el listar regresa al form
    regresar: function () {
        if (!$('#tbl_ingresoMerc').hasClass('fullHidden'))
            $('#tbl_ingresoMerc').addClass('fullHidden')
        if (!$('#div_ingresoMerc_filtros').hasClass('fullHidden'))
            $('#div_ingresoMerc_filtros').addClass('fullHidden')
        if ($('#frm_manifiesto_modificar').hasClass('fullHidden'))
            $('#frm_manifiesto_modificar').removeClass('fullHidden')
        $('#btn_listar_ingresoMerc').show();
        $('#btn_guardar_mercancia').hide();
        $('#btn_regresar_ingresoMerc').hide();
    },

    //Busca el manifiesto y lo carga, esto para la edicion
    buscarMercancia: function (p_identificador) {
        principal.resetAllFields('frm_manifiesto_modificar');
        var tma_manifiesto_p = '';
        var tma_fch_manifiesto_p = '';
        var tma_aer_naerolinea_p = null;
        var cliente = '';
        var num_contenedor = '';
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MANIFIESTOS_TB/ManifiestoModificar?manifiesto_id=' + p_identificador + '&tma_manifiesto_p=' + tma_manifiesto_p + '&tma_fch_manifiesto_p=' + tma_fch_manifiesto_p + '&tma_aer_naerolinea_p=' + tma_aer_naerolinea_p + '&cliente=' + cliente + '&num_contenedor=' + num_contenedor,
            success: function (data) {
                $.each(data[0], function (i, v) {
                    if (i == 'TMA_AER_NAEROLINEA') {
                        mercancias_modificar.codigo_aerolinea = v;
                    }
                    if (i == 'TMA_CLIENTE_ID') {
                        mercancias_modificar.codigo_cliente = v;
                    }
                    if (i == 'TMA_CLIENTE_TIPO') {
                        mercancias_modificar.getClient(mercancias_modificar.tipo_cliente, v, mercancias_modificar.codigo_cliente);
                    }
                    if (i == 'TMA_CLIENTE_NOMBRE') {
                        mercancias_modificar.tipo_cliente = v;
                    }
                    if (i == 'AEROLINEA') {
                        mercancias_modificar.aerolinea = v;
                    }
                    if (i == 'TMA_MOD_TRANSPORTE') {
                        mercancias_modificar.getTransportistas(v, mercancias_modificar.aerolinea, mercancias_modificar.codigo_aerolinea)
                    }
                    principal.setValueByName(i, v, 'frm_manifiesto_modificar');
                });
                //$('#TMA_MOD_TRANSPORTE').change(function(){
                Cookies.set('tipo_transporte', $('#TMA_MOD_TRANSPORTE').val());
            },
            error: function (data) {
                swal({
                    title: "Contenedor/Manifiesto",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                });
            }
        });
    },

    //Hace el cambio de navegacion de botones y la busqueda del manifiesto o 
    editar: function (id) {
        mercancias_modificar.limpiar();
        Cookies.set('manifiesto', id);
        mercancias_modificar.buscarMercancia(id);
        manifiesto_viaje.buscarViajes(id);
        manifiesto_duas.buscarDuas(id);
        if (!$('#tbl_ingresoMerc').hasClass('fullHidden'))
            $('#tbl_ingresoMerc').addClass('fullHidden')
        if (!$('#div_ingresoMerc_filtros').hasClass('fullHidden'))
            $('#div_ingresoMerc_filtros').addClass('fullHidden')
        if ($('#frm_manifiesto_modificar').hasClass('fullHidden'))
            $('#frm_manifiesto_modificar').removeClass('fullHidden')
        $('#btn_listar_ingresoMerc').show();
        $('#btn_guardar_mercancia').show();
        $('#btn_conocimientos').show();
        $('#btn_regresar_ingresoMerc').hide();
        if ($('#btn_guardar_mercancia').hasClass('fullHidden'))
            $('#btn_guardar_mercancia').removeClass('fullHidden')
        $("#TMA_ESTADO").attr('disabled', false);
        principal.resetAllFields('div_ingresoMerc_filtros');
    },

    redireccionar: function () {
        window.location.href = "Conocimientos_Modificar_Copy?id=" + $("#TMA_MANIFIESTO").val();
    },

    guardar: function () {
        var myform = $('#frm_manifiesto_modificar');
        var disabled = myform.find(':input:disabled').removeAttr('disabled');
        var serialized = myform.serialize();
        disabled.attr('disabled', 'disabled');

        $.ajax({
            url: "/api/TX_MANIFIESTOS_TB/ManifiestoModificar_Post",
            type: 'PUT',
            data: serialized,
            dataType: "text",
            success: function (data) {
                if (data.length == 0) {
                    /*manifiesto_duas.limpiar();
                    mercancias_modificar.limpiar();
                    manifiesto_viaje.limpiar();*/
                    swal({
                        title: "Contenedor/Manifiesto",
                        text: "Se actualizó correctamente",
                        icon: "success",
                        button: "OK!",
                    });
                } else {
                    swal({
                        title: "Contenedor/Manifiesto",
                        text: data,
                        icon: "error",
                        button: "OK!",
                    });
                }

                //manifiesto_duas.delete(data, cb);
            },
            error: function (e) {
                swal({
                    title: "Contenedor/Manifiesto",
                    text: JSON.parse(e.responseText).Message,
                    icon: "error",
                    button: "OK!",
                });
            }
        });
    }

};
$(document).ready(function () {
    mercancias_modificar.init();
});