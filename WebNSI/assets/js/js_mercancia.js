﻿var mercancia = {

    init: function () {

         $('#li_mercancias').on('click', function () { 
                //funcionalidad al tocar el tab mercancias          
               
            });

         $("#detalles-mercancia").attr('style', 'display:none');
        $("#btn_regresar_mercancia").attr('style', 'display:none');
        mercancia.MER_ListarReimpresion();

        $('#in_flr_mer_ien_entrada').change(function () {
          mercancia.MER_ListarReimpresion();
        });
        $('#in_flr_mer_id_mercancia').change(function () {
          mercancia.MER_ListarReimpresion();
        });
    },

    MER_Regresar: function(){
        $("#detalles-mercancia").attr('style', 'display:none');
        $("#btn_regresar_mercancia").attr('style', 'display:none');
        $("#th_mercancias").attr('style', 'display:block');
    },

    MER_BuscarMercancia: function (p_mercancia){
        mercancia.MER_ConsultarDatos(p_mercancia);
    },

    MER_ConsultarDatos: function (p_mercancia) {      
        var postdata = { id: p_mercancia };
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MERCANCIAS_TB/Mercancia?id='+ p_mercancia,
            data: postdata,
            success: function (data) {
                if (data !== null) {
                    //trae los datos de la mercancia
                     mercancia.ActiveCamposMercancia();
                     mercancia.MER_MuestraDatos(data);
                    $("#th_mercancias").attr('style', 'display:none');
                    $("#detalles-mercancia").attr('style', 'display:block');
                    $("#btn_regresar_mercancia").attr('style', 'display:block');
                   
                }
            },
            failure: function (data) {
                
                if (data.responseJSON.Message == "false") {
                    prod_nc.infoConsecutivo(consecutivo, sistema);
                } else {
                    swal({
                        title: "Reimpresión",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            },
            error: function (data) {

                if (data.responseJSON.Message == "false") {
                    prod_nc.infoConsecutivo(consecutivo, sistema);
                } else {
                    swal({
                        title: "Reimpresión",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            }
        });
    },

    MER_MuestraDatos: function (p_mercancia) {
        for (var campo in p_mercancia) {
            if(p_mercancia[campo] == 0){
              p_mercancia[campo] == " ";
            }
            if(campo === "IEN_Bloqueada"){
                if(p_mercancia[campo] === "S"){
                    p_mercancia[campo] = true;
                } else{
                    p_mercancia[campo] = false;
                }
            }
            if(campo === "IEN_ES_Courier"){
                if(p_mercancia[campo] === "S"){
                    p_mercancia[campo] = true;
                } else{
                    p_mercancia[campo] = false;
                }
            }
            if(campo === "IEN_Muestras"){
                if(p_mercancia[campo] === "S"){
                    p_mercancia[campo] = true;
                } else{
                    p_mercancia[campo] = false;
                }
            }
            if(p_mercancia[campo] != null){
                principal.setValueByName(campo, p_mercancia[campo],'frm_mercancia');
            }  
        }      
    },
    
    MER_Listar: function () {
        var entrada = $("#in_ien_entrada").val();
        //Obtenemos la plantilla
        $('#th_mercancias').show();
        $("#th_mercancias").attr('style', 'display:block');
        $("#detalles-mercancia").attr('style', 'display:none');
        var templateText = $("#Mercancias_Table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#Mercancias_Tablebody").html('');
        $.getJSON('/api/TX_MERCANCIAS_TB/GetMercanciasEntrada?entrada=' + entrada).then(function (data) {
            $.each(data, function (index, elem) {
                $("#Mercancias_Tablebody").append(tableTemplate(elem));
            })
        });
    },
   
    MER_ListarReimpresion: function(){
        //Buscar lista de mercancías según filtros
         var entrada = $("#in_flr_mer_ien_entrada").val();
         var tarima = $("#in_flr_mer_id_mercancia").val();
        //Obtenemos la plantilla
        $('#th_mercanciasReimp').show();
        $("#th_mercanciasReimp").attr('style', 'display:block');
        var templateText = $("#MercanciasReimp_Table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#MercanciasReimp_Tablebody").html('');
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MERCANCIAS_TB/GetMercanciasReimp?entrada=' + entrada + '&tarima=' + tarima,
            //data: postdata,
            success: function (data) {
                if (data !== null) {
                    $.each(data, function (index, elem) {
                        $("#MercanciasReimp_Tablebody").append(tableTemplate(elem));
                    });

                }
            },
            failure: function (data) {

                if (data.responseJSON.Message == "false") {
                    prod_nc.infoConsecutivo(consecutivo, sistema);
                } else {
                    swal({
                        title: "Reimpresión",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            },
            error: function (data) {

                if (data.responseJSON.Message == "false") {
                    prod_nc.infoConsecutivo(consecutivo, sistema);
                } else {
                    swal({
                        title: "Reimpresión",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            }
        });
    },

    Imprimir: function (tarima) {       
        //var tarima = "http://webherediatest/WebEtiquetaNCI/FrmEtiquetaHR.aspx?etiqueta="  + tarima;
        var tarima = "http://merxalmacenadora/WebEtiquetaNCI/FrmEtiquetaHR.aspx?etiqueta="  + tarima;
        principal.popupCenter(tarima, 'Etiqueta',750,500);
        
        //window.open("http://webherediatest/WebEtiquetaMerx/FrmEtiquetaHR.aspx?etiqueta=" + tarima); 
        // window.open("http://webherediatest/Entrada/Etiqueta?Tarima=" + tarima);
         //window.open("http://webherediatest/WebEtiquetaNCI/FrmEtiquetaHR.aspx?etiqueta="  + tarima); 
    },

   




    limpiarFiltrosMerca: function(){
        $("#in_flr_mer_ien_entrada").val("");
        $("#in_flr_mer_id_mercancia").val("");
        mercancia.DeactiveCamposMercancia();     
         $("#MercanciasReimp_Tablebody").html('');
         TecladoFuncionalidadPersonalizada(); 
        $("#in_flr_mer_ien_entrada").focus();
        mercancia.MER_ListarReimpresion();
    },


    ActiveCamposMercancia: function () {
        $('label').addClass('active');
    },

    DeactiveCamposMercancia: function () {
        $('label').removeClass('active');
    }
    
};

$(document).ready(function () {
    mercancia.init();
})