﻿var correcTrans = {
    init: function () {
      TecladoFuncionalidadPersonalizada();
       correcTrans.MERTRANS_Limpiar();
    },

    onfocus: function () {
        if ($("#lbl_consecutivo").hasClass('lblfocus'))
            $("#lbl_consecutivo").removeClass('lblfocus');
    },


    MERTRANS_BusquedaFiltros: function () {
        var entradanum, manifiestonum, guianum;

        entradanum = $('#in_flr_mer_ien_entrada').val().toUpperCase();
        manifiestonum = $('#in_flr_mer_manifiesto').val().toUpperCase();
        guianum = $('#in_flr_ien_guia').val().toUpperCase();
       
        if (guianum == '' &&  manifiestonum == '' && entradanum == '')
        {
          swal({
              title: "Corrección de transmisiones",
              text: "Debe digitar al menos uno de los valores para consultar los datos.",
              icon: "error",
              button: "OK!",
            })  
        }
        else
        {
            correcTrans.MERTRANS_Buscar(entradanum, manifiestonum, guianum);
        }
    },

    MERTRANS_Buscar: function (p_entrada, p_manifiesto, p_guia) {
        var cb=function(){
           correcTrans.RadioCheck();
            $("#btn_actualizar").attr('style', 'display:block');
            $("#btn_cerrar").attr('style', 'display:block');
            $("#btn_finalizar").attr('style', 'display:block');
       
        };
        correcTrans.CargarTabla(p_entrada, p_manifiesto, p_guia,cb);
    },

    CargarTabla: function(p_entrada, p_manifiesto, p_guia, callback){
        var templateText = $("#MercanciasCorrec_Table-template").html();
            //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#MercanciasCorrec_Tablebody").html('');
            $.getJSON('/api/TX_MERCANCIAS_TBCorrec/GetMercanciasCorrec?entrada_p=' + p_entrada + '&manifiesto_p=' + p_manifiesto + '&guia_p=' + p_guia).then(function (data) {

                $.each(data, function (index, elem) {
                     elem.consecutivo = index;
                    $("#MercanciasCorrec_Tablebody").append(tableTemplate(elem));

                });
                if(typeof(callback) == "function"){
                    callback();
                }
            });     
    },

    RadioCheck: function(){
     $("#MercanciasCorrec_Table tbody tr").each(function(i,e){ 
             est = $(this).find("td").eq(0).html();
             $(this).find("input[id=rd_" + est + "]").attr('checked',true);
     });
    },

    MERTRANS_Cerrar: function(){
        $("#MercanciasCorrec_Table tbody tr").each(function(i,e){ 
             $(this).find("input[id=rd_C]").prop('checked',false);
             $(this).find("input[id=rd_E]").prop('checked',false);
             $(this).find("input[id=rd_T]").prop('checked',false);
             $(this).find("input[id=rd_F]").prop('checked',false);
             $(this).find("input[id=rd_C]").prop('checked',true);
        });
    },

    MERTRANS_Finalizar: function(){
        $("#MercanciasCorrec_Table tbody tr").each(function(i,e){ 
             $(this).find("input[id=rd_C]").prop('checked',false);
             $(this).find("input[id=rd_E]").prop('checked',false);
             $(this).find("input[id=rd_T]").prop('checked',false);
             $(this).find("input[id=rd_F]").prop('checked',false);
             $(this).find("input[id=rd_F]").prop('checked',true);
        });
    },

    MERTRANS_Actualizar: function(){
           var est_Merc = "";  
           var id_Merc = "";
           var arrEdit = [];
            $("#MercanciasCorrec_Table tbody tr").each(function(i,e){ 
               
                //var cb = function(){
                  id_Merc = $(this).find("td").eq(2).html();
             
                  if($(this).find("input[id=rd_F]").is(':checked')){
                    est_Merc = "F";
                  }
                  if($(this).find("input[id=rd_C]").is(':checked')){
                    est_Merc = "C";
                  }
                  if($(this).find("input[id=rd_T]").is(':checked')){
                    est_Merc = "T";
                  }
                  if($(this).find("input[id=rd_E]").is(':checked')){
                    est_Merc = "E";
                  }
               // };
               arrEdit.push({
                    'idM': id_Merc,
                    'estadoM': est_Merc
               });
               
            });
         
            // arrEdit.forEach(function(merc,index) {
            //     correcTrans.Actualiza(merc.idM,merc.estadoM);
            // });
             // arrEdit.forEach(function(merc,index) {
                correcTrans.Actualiza(arrEdit);
            // });
         
    },

    Actualiza: function(dataM,callback){
            $.ajax({
                url: '/api/TX_MERCANCIAS_TBCorrec/ActualizaCorrec',
                type: 'POST',
                data: JSON.stringify(dataM),
                dataType: 'json',
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Actualización de correciones",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        });
                    }else{
                         swal({
                         title: "Correciones",
                         text: "Se actualizó correctamente la información",
                         icon: "success",
                         button: "OK!",

                         });
                    }
                },
                error: function (data) {
                  if(data.status != 200){
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }else{
                      swal({
                          title: "Error",
                          text: data.responseJSON.Message,
                          icon: "error",
                          button: "OK!",
                        });
                    }
                  }else{
                     swal({
                         title: "Correciones",
                         text: "Se actualizó correctamente la información",
                         icon: "success",
                         button: "OK!",

                         });
                  }
                }
            })

    },

    MERTRANS_VerDetalle: function(p_mercancia){
        var postdata = { id: p_mercancia };
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MERCANCIAS_TBCorrec/MercanciaDetalle?id='+ p_mercancia,
            data: postdata,
            success: function (data) {
                if (data !== null) {
                    //trae los datos de la mercancia
                     //correcTrans.ActiveCamposFiltro();
                     correcTrans.MERTRANS_MuestraDatos(data);
                   
                    

                    
                   
                }
            },
            failure: function (data) {
                
                console.log("fail");
            },
            error: function (data) {

                console.log("error consulta mercancia correciones");
            }
        });
    },

    MERTRANS_MuestraDatos: function (p_mercancia) {       
        for (var campo in p_mercancia) {
            if(p_mercancia[campo] != null){
                principal.setValueByName(campo, p_mercancia[campo],'frm_detalleMerc');
            }  
        }

        $("#th_mercanciasCorrec").attr('style', 'display:none');                   
        $("#div_filters").attr('style', 'display:none');     
        $("#frm_detalleMerc").attr('style', 'display:block');
        $("#btn_regresar").attr('style', 'display:block');
        $("#btn_actualizar").attr('style', 'display:none');
        $("#btn_cerrar").attr('style', 'display:none');
        $("#btn_finalizar").attr('style', 'display:none');  

        principal.activeLabels();    
    },

    MERTRANS_Regresar:function(){
        $("#th_mercanciasCorrec").attr('style', 'display:block');                   
        $("#div_filters").attr('style', 'display:block');     
        $("#frm_detalleMerc").attr('style', 'display:none');
        $("#btn_regresar").attr('style', 'display:none');
        $("#btn_actualizar").attr('style', 'display:block');
        $("#btn_cerrar").attr('style', 'display:block');
        $("#btn_finalizar").attr('style', 'display:block');
        principal.resetAllFields('frm_detalleMerc');
    },

    MERTRANS_Limpiar: function (){
        principal.deactivateLabels();
        $('#in_flr_mer_ien_entrada').val("");
        $('#in_flr_mer_manifiesto').val("");
        $('#in_flr_ien_guia').val("");
        $('#in_flr_mer_ien_entrada').focus();
        //$('#in_flr_mer_id_mercancia').val("");
        $("#MercanciasCorrec_Tablebody").html('');
        $("#btn_actualizar").attr('style', 'display:none');
        $("#btn_cerrar").attr('style', 'display:none');
        $("#btn_finalizar").attr('style', 'display:none');
        $("#btn_regresar").attr('style', 'display:none');
        $("#frm_detalleMerc").attr('style', 'display:none');
    }

}
$(document).ready(function () {
    correcTrans.init();
})