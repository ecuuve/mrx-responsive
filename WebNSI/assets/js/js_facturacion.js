var facturacion = {
    numTique : null,
    tipTique: null,
    fecTique: null,
    FACTURA: null,

    init: function () {
        TecladoFuncionalidadPersonalizada();
        //***********INICIO Llamados de movs*************//
        facturacion.getTarifas();
        facturacion.getBases();
        facturacion.getConceptos();


        //facturacion.FACTURA = 31;

        if(facturacion.FACTURA != null){
             facturacion.getMovimientosFactura(facturacion.FACTURA);
        }
       
        $('#div_tarifa').addClass('fullHidden');
        $('#btn_actualiza_tarifa').hide();
         $('#btn_cancela_actualiza_tarifa').hide();
        
        
        $("#li_movimientos").attr('style', 'display:none');
        $("#li_adicionales").attr('style', 'display:none');
        $("#li_detalle").attr('style', 'display:none');
        $("#li_totales").attr('style', 'display:none');
        $("#li_totales_tarifa").attr('style', 'display:none');

        //***********FIN Llamados de movs*************//

        facturacion.numTique = Cookies.get('tiquete');
        facturacion.tipTique = Cookies.get('tipotiquete');
        facturacion.fecTique = Cookies.get('fchtiquete');

        if(facturacion.numTique != null && facturacion.tipTique != null &&  facturacion.fecTique != null){
           facturacion.FuncionalidadTiqueteAtencion();    
        }

       
        facturacion.CargaFechas();
        facturacion.CargarControlesPrevios();
        facturacion.FuncionalidadCuentasContribuyente();

        //Oculta los botones de la cinta
         $('#btn_calcular_fac').hide();
         $('#btn_facturar').hide();
         $('#btn_anular_fac').hide();
         $('#btn_guardar_encabFac').hide();
         $('#btn_limpiar_fac').hide();
         $('#btn_limpiar_detalle').hide();

        
        //Valida el tipo de documento seleccionado
        $('#FAC_DIN_TIPO_DOCUMENTO').change(function(){
            var tipodoc = $("#FAC_DIN_TIPO_DOCUMENTO option:selected").val();
            if(tipodoc == 42){
                $("#FAC_VALOR_CIF").attr('disabled', true);
                $("#FAC_IMPUESTOS").attr('disabled', true);
                $("#FAC_IMP_EXENTOS").attr('disabled', true); 
                 
                $("#FAC_MARCH_ELECTRONICO").attr('disabled', true);
                $("#FAC_TRAMITE").attr('disabled', true);
                $("#FAC_CLIENTE_DUA").attr('disabled', true);
                 
                $("#FAC_NOTA_EXENCION").attr('disabled', true);
                $("#FAC_DESDE_EXENCION").attr('disabled', true);
                $("#FAC_HASTA_EXENCION").attr('disabled', true);

                $("#FAC_VALOR_CIF").val(0);
                $("#FAC_IMPUESTOS").val(0);
                $("#FAC_IMP_EXENTOS").val(0);

                principal.activeLabels();
            }else{
                $("#FAC_VALOR_CIF").attr('disabled', false);
                $("#FAC_IMPUESTOS").attr('disabled', false);
                $("#FAC_IMP_EXENTOS").attr('disabled', false); 

                $("#FAC_MARCH_ELECTRONICO").attr('disabled', false);
                $("#FAC_TRAMITE").attr('disabled', false);
                $("#FAC_CLIENTE_DUA").attr('disabled', false);
                 
                $("#FAC_NOTA_EXENCION").attr('disabled', false);
                $("#FAC_DESDE_EXENCION").attr('disabled', false);
                $("#FAC_HASTA_EXENCION").attr('disabled', false);

                $("#FAC_VALOR_CIF").val("");
                $("#FAC_IMPUESTOS").val("");
                $("#FAC_IMP_EXENTOS").val("");
            }
        });

        //Al seleccionar el declarante lista los agentes correspondientes
        $('#FAC_DCT_DECLARANTE_ID').change(function(){
          facturacion.getAgentesDecl($("#FAC_DCT_DECLARANTE_ID option:selected").val());
        });

        //Al dar click en el tab de adicionales trae los datos
        $('#adicionales-tab').on('click', function () { 
            facturacion.getAdicionalesFactura(facturacion.FACTURA);
        });


        $('#frm_detallefac #CFR_CODIGO').change(function(){
            if($(this).val() != ''){
                 facturacion.getPersonas("","",$(this).val());
            }

        });

        $('#frm_detallefac #TRA_TRANSPORTISTA').change(function(){
            if($(this).val() != ''){
                 facturacion.getConductores("","",$(this).val());
            }
        });

         //Al dar click en el tab de detalle activa el botón de facturar
        $('#detalle-tab').on('click', function () { 
            $('#btn_facturar').show();
            $('#btn_limpiar_fac').hide();
            $('#btn_limpiar_detalle').show();

        }); 
        

        //Al dar click en el tab de totales trae los datos
        $('#totales-tab').on('click', function () { 
            facturacion.totales(facturacion.FACTURA);
        }); 

         //Al dar click en el tab de totales trae los datos
        $('#totales_tarifa-tab').on('click', function () { 
            facturacion.totalesTarifa();
        }); 
    },

    //Controla el tiquete que se esté atendiendo
    FuncionalidadTiqueteAtencion: function(){
        facturacion.numTique = Cookies.get('tiquete');
        facturacion.tipTique = Cookies.get('tipotiquete');
        facturacion.fecTique = Cookies.get('fchtiquete');

        swal({
              title: "Continuar atendiendo el tiquete asignado?",
              text: "# Tiquete: "+ facturacion.numTique + " / Tipo: " + facturacion.tipTique + " / Fecha: " + facturacion.fecTique,
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willUse) => {
              if (willUse) {
                principal.resetAllFields('detInfo_registro_prefactura');

                $("#FAC_TIQUETE").val(facturacion.numTique);
                $("#lbl_FAC_TIQUETE").addClass('active');
                $("#FAC_TIPO_TIQUETE").val(facturacion.tipTique);
                $("#FAC_FCH_TIQUETE").val(facturacion.fecTique);
                $("#lbl_FAC_FCH_TIQUETE").addClass('active');

                $("#FAC_DUA_SALIDA").focus();

                //Oculta los botones de la cinta
                $('#btn_guardar_encabFac').show();
                $('#btn_limpiar_fac').show();
                $('#btn_obt_tiquete').hide();  
                $('#btn_calcular_fac').hide();
                $('#btn_facturar').hide();
                $('#btn_anular_fac').hide(); 
                $('#btn_limpiar_detalle').hide();



                $("#frm_registro_prefactura :input").prop("disabled", false);
                $("#FAC_TIQUETE").prop("disabled", true);
                $("#FAC_TIPO_TIQUETE").prop("disabled", true);
                $("#FAC_FCH_TIQUETE").prop("disabled", true);
                $("#FAC_TIPO_CAMBIO").prop("disabled", true);
                $("#FAC_CUENTA").prop("disabled", true);
                $("#FAC_CONTRIBUYENTE").prop("disabled", true);
                $("#FAC_FACTURA").prop("disabled", true);

                facturacion.getTipoCambio();

              }else{

               facturacion.cancelarTiquete();
              }
            }); 
    },

    //Maneja la navegación de los campos de cnt y contr
    FuncionalidadCuentasContribuyente: function(){
        //Al seleccionar el IMPORTADOR lista las cuentas correspondientes
        $('#FAC_IMP_CEDULA').change(function(){
          if($("#FAC_IMP_CEDULA option:selected").val() != ""){
            facturacion.BuscarCuentas();
          }
            
        });
        //Al seleccionar forma de pago lista las cuentas correspondientes
        $('#FAC_CONTADO').change(function(){
            facturacion.BuscarCuentas();
          //facturacion.getCuentas($("#FAC_CONTADO option:selected").val());
        });

        //Al seleccionar nombre de cuenta carga el número de cuenta
        $('#FAC_NOMBRE_CUENTA').change(function(){
            $("#FAC_CUENTA").val($("#FAC_NOMBRE_CUENTA option:selected").val());
            $('#lbl_FAC_CUENTA').addClass('active');
        });

        //Al seleccionar tipo lista los contribuyentes correspondientes
        $('#A_QUIEN_FACTURA').change(function(){ 
            $("#FAC_CONTRIBUYENTE").val("");           
            var seleccionado = $("#A_QUIEN_FACTURA option:selected").val();
                facturacion.BuscarContribuyentes(seleccionado);                   
        });

        //Al seleccionar contribuyente carga el número de contribuyente
        $('#FAC_NBR_CONTRIBUYENTE').change(function(){
            $("#FAC_CONTRIBUYENTE").val($("#FAC_NBR_CONTRIBUYENTE option:selected").val());
            $('#lbl_FAC_CONTRIBUYENTE').addClass('active');         
        });
    },

    //Carga el tiquete de atención
    CargarTiquete: function(){
        $.ajax({
            method: 'GET',
            dataType: 'JSON',
            url: '/api/TX_FACTURAS_TB/Tiquete', 
            success: function (data) {
                $("#FAC_TIQUETE").val(data[0].Id);
                $("#lbl_FAC_TIQUETE").addClass('active');
                $("#FAC_TIPO_TIQUETE").val(data[0].Nombre);

                $("#FAC_DUA_SALIDA").focus();

                $('#btn_guardar_encabFac').show();
                $('#btn_limpiar_fac').show();
               
                $('#btn_obt_tiquete').hide();

                Cookies.set('tiquete',$("#FAC_TIQUETE").val());
                Cookies.set('tipotiquete',$("#FAC_TIPO_TIQUETE").val()); 

                 facturacion.CargaFechaActual();
            },
            failure: function (data) {
                console.log("fail TIQUETE");
                principal.alertsSwal.msj_error("Facturación/Tiquete", data.responseJSON.Message);
            },
            error: function (data) {
               console.log("error consulta TIQUETE");
               principal.alertsSwal.msj_error("Facturación/Tiquete", data.responseJSON.Message);
            }
        });
    },

    //Evento que activa el label del campo focus
    onfocus: function () {
        if ($("#lbl_DUA").hasClass('lblfocus'))
            $("#lbl_DUA").removeClass('lblfocus');
    },

    //Limpia todos los campos de pantalla
    LimpiarInicio: function () {
        principal.resetAllFields('detInfo_registro_prefactura');
        TecladoFuncionalidadPersonalizada();

        var ddl = $('#FAC_ADC_AGENTE_ID').data("kendoComboBox");
        if(ddl != undefined)
        ddl.setDataSource([]);

        var ddl1 = $('#FAC_IMP_CEDULA').data("kendoComboBox");
        if(ddl1 != undefined)
        ddl1.setDataSource([]);
    },

    //Limpia todos los campos de pantalla
    LimpiarCamposEncabezado: function () {
        var tiq = $("#FAC_TIQUETE").val();
        var tipTiqe = $("#FAC_TIPO_TIQUETE").val();
        var fch =  $("#FAC_FCH_TIQUETE").val();


        principal.resetAllFields('detInfo_registro_prefactura');

        $("#FAC_TIQUETE").val(tiq);
        $("#FAC_TIPO_TIQUETE").val(tipTiqe);
        $("#FAC_FCH_TIQUETE").val(fch);

        TecladoFuncionalidadPersonalizada();
        var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd/MM/yyyy');
        $("#FAC_FCH_DUA").val(todayDate);
        $("#FAC_DESDE_EXENCION").val(todayDate);
        $("#FAC_HASTA_EXENCION").val(todayDate);

        var ddl = $('#FAC_ADC_AGENTE_ID').data("kendoComboBox");
        if(ddl != undefined)
        ddl.setDataSource([]);

        facturacion.getImportadores("","");
        //$("#FAC_IMP_CEDULA").data("kendoComboBox").value("");

        //facturacion.CargaFechaActual(); 

        //Oculta los botones de la cinta
        // $('#btn_calcular_fac').hide();
        // $('#btn_facturar').hide();
        // $('#btn_anular_fac').hide();
        // $('#btn_guardar_encabFac').hide();
        // $('#btn_limpiar_fac').hide();
        // $('#btn_obt_tiquete').show();   

        facturacion.getTipoCambio();   
    },

    //Cargo los datepicker y la fecha actual a mostrar
    CargaFechas: function () {
        principal.KendoDate($("#FAC_FCH_DUA"), new Date());
        principal.KendoDate($("#FAC_DESDE_EXENCION"), new Date());   
        principal.KendoDate($("#FAC_HASTA_EXENCION"), new Date());         
    },

    //Carga la fecha actual cuándo ingresa a la pantalla
    CargaFechaActual: function(){
        var d = new Date();
        var minutes = (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();
        var seconds = (d.getSeconds() < 10 ? '0' : '') + d.getSeconds();

        //var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
        var strDate = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
        var time_date = strDate + " " + d.getHours() + ":" + minutes + ":" + seconds;
    
        $("#FAC_FCH_TIQUETE").val(time_date);
        $("#lbl_FAC_FCH_TIQUETE").addClass('active');
    
        Cookies.set('fchtiquete',$("#FAC_FCH_TIQUETE").val());
    },

    //Carga de controles al inicio de la pantalla
    CargarControlesPrevios: function () {
        facturacion.getTipoDocumento();
        facturacion.getTipoCambio();
        facturacion.getImportadores("","");
        facturacion.getDeclarantes();
        // facturacion.CargarTiquete();
    },

    //Controles iniciales
    getTipoDocumento: function () {
        var cb = function(data){
        principal.KendoComboBox(data, '#FAC_DIN_TIPO_DOCUMENTO');
        };
        principal.get("/api/TX_FACTURAS_TB/TipoDocumento",'',cb);
    },

    //Obtiene la lista de importadores
    getImportadores: function (id, nombre) {
        var cb = function(data){
            if(data.length > 0){

                 //Limpio el combo de importadores
                var ddl = $('#FAC_IMP_CEDULA').data("kendoComboBox");
                if(ddl != undefined)
                ddl.setDataSource([]);



                principal.KendoComboBox(data, '#FAC_IMP_CEDULA');
                if(id != null && id != ""){
                    $("#FAC_IMP_CEDULA").val(id);
                    var text = $("#FAC_IMP_CEDULA option:selected").text();
                    $('#FAC_IMP_CEDULA > option').each(function(i,v){
                        if($(this).text() == text){
                            $("#FAC_IMP_CEDULA").data('kendoComboBox').select(i);
                            if($("#FAC_IMP_CEDULA option:selected").val() != ""){
                                facturacion.BuscarCuentas();
                            }

                        }
                    });             
                }
            }
       
        };
        principal.get("/api/TX_IMPORTADORES_TB/Importadores?id=" + id + "&nombre=" + nombre,'',cb);
    },

    //Lista los declarantes
    getDeclarantes: function () {
        var cb = function(data){
        principal.KendoComboBox(data, '#FAC_DCT_DECLARANTE_ID');
        };
        principal.get("/api/TX_FACTURAS_TB/GetDeclarantes?id=" + "" + "&nombre=" + "",'',cb);
    },

    //Lista los agentes del declarante seleccionado
    getAgentesDecl: function (id) {
        var cb = function(data){
        principal.KendoComboBox(data, '#FAC_ADC_AGENTE_ID');
        };
        principal.get("/api/TX_FACTURAS_TB/GetAgentesDecl?declarante=" + id,'',cb);
    },

    //Obtiene el tipo de cambio que se muestra en pantalla
    getTipoCambio: function () {
        principal.getData(
            "/api/TX_FACTURAS_TB/TipoCambio",
            function (data) {
                $("#FAC_TIPO_CAMBIO").val(data);
                $("#lbl_tipocambio").addClass('active');
            }
        )
    },

    // Obtiene la cédula del importador según la ubicación digitada
    // y lo selecciona en el campo de importador
    getUbicacion: function (cod_ubi) {

        if (cod_ubi == null){
            cod_ubi = $("#FAC_UBI_UBICACION").val();
        }


        principal.getData(
            "/api/TX_FACTURAS_TB/GetUbicacion?codigo=" + cod_ubi,
            function (data) {
                if(data != null){
                   facturacion.getImportadores(data,"");
                }else{
                    facturacion.getImportadores("","");
                }
            }
        )
    },

    //Busca cuentas para encabezado
    BuscarCuentas: function () {
        //variables
        //var opcion_p = $("#FAC_CONTADO option:selected").val();
        var tipo_p = "K";
        var nombre_p = "";
        var cuenta_p = "";
        var forma_pago_p = $("#FAC_CONTADO option:selected").val(); //"R";
        var tipo_cta_p = "";
        var importador_p = $("#FAC_IMP_CEDULA option:selected").val();// "400004213902";  //hacer trim 
        if(importador_p != "" && importador_p != undefined){
            importador_p = importador_p.trim();
        }
        

        facturacion.getCuentas(tipo_p,nombre_p,cuenta_p,forma_pago_p,tipo_cta_p,importador_p,"N");
    },

    //Busca contribuyentes para encabezado
    BuscarContribuyentes: function (opcion_p) {        
        var cnta_p = "";
        if(opcion_p == 'C'){
            cnta_p = $("#FAC_CUENTA").val();

            var tipo_p = "K";
            var nombre_p = "";
            var cuenta_p = cnta_p;
            var forma_pago_p = $("#FAC_CONTADO option:selected").val(); //"R";
            var tipo_cta_p = "";
            var importador_p = $("#FAC_IMP_CEDULA option:selected").val();// "400004213902";  //hacer trim 
            importador_p = importador_p.trim();

            facturacion.getCuentas(tipo_p,nombre_p,cuenta_p,forma_pago_p,tipo_cta_p,importador_p,"S");

        }else{
            //variables
            var tipo_p = "C";
            var nombre_p = "";
            var cuenta_p = "";
            var forma_pago_p = $("#FAC_CONTADO option:selected").val();
            var tipo_cta_p = $("#A_QUIEN_FACTURA option:selected").val();
            var importador_p = $("#FAC_IMP_CEDULA option:selected").val(); 
             
             importador_p = importador_p.trim();

            facturacion.getCuentas(tipo_p,nombre_p,cuenta_p,forma_pago_p,tipo_cta_p,importador_p, "N");
        }
    },

    //Obtiene las cuentas de la bd según filtros
    getCuentas: function (tipo_p,nombre_p,cuenta_p,forma_pago_p,tipo_cta_p,importador_p, escopia_p) {
        $.ajax({
            method: 'POST',
            dataType: 'JSON',
            url: '/api/TX_FACTURAS_TB/BuscarCuentas', 
            data: { tipo_p: tipo_p, nombre_p: nombre_p, cuenta_p: cuenta_p, forma_pago_p: forma_pago_p, tipo_cta_p: tipo_cta_p, importador_p: importador_p },
            success: function (data) {
                if (data !== null && tipo_p == "K") {
                    if(escopia_p == "S"){
                        principal.KendoComboBox(data, '#FAC_NBR_CONTRIBUYENTE');
                        ///Cargar la cuenta y seleccionar en el combo
                        $("#FAC_NBR_CONTRIBUYENTE").data('kendoComboBox').select(0);
                        $("#FAC_CONTRIBUYENTE").val($("#FAC_NBR_CONTRIBUYENTE option:selected").val());
                        $('#lbl_FAC_CONTRIBUYENTE').addClass('active');
                        

                    }else{
                        console.log(data);
                        principal.KendoComboBox(data, '#FAC_NOMBRE_CUENTA'); 
                        $("#FAC_NOMBRE_CUENTA").data("kendoComboBox").value("");

                    }
                     
                }
                 if (data !== null && tipo_p == "C") {
                     console.log(data);                     
                     principal.KendoComboBox(data, '#FAC_NBR_CONTRIBUYENTE');
                     $("#FAC_NBR_CONTRIBUYENTE").data("kendoComboBox").value("");
                }

            },
            failure: function (data) {

                console.log("fail");
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(textStatus);
                //console.log("error consulta guía");
            }
        });
    },

    //Guarda el encabezado de la factura
    GuardarEncabezado: function(){
       $("#FAC_USU_DIGITO").val($("#UsReg").val());

       $("#FAC_TIQUETE").attr('disabled', false);
       $("#FAC_TIPO_TIQUETE").attr('disabled', false);
       $("#FAC_FCH_TIQUETE").attr('disabled', false);
       $("#FAC_TIPO_CAMBIO").attr('disabled', false);
       $("#FAC_CUENTA").attr('disabled', false);
       $("#FAC_CONTRIBUYENTE").attr('disabled', false);

        $.post(
          "/api/TX_FACTURAS_TB/GuardarEncabezado/",
           $('#frm_registro_prefactura').serialize(),
          function (data) {
              if(data != null && data != ""){
                
                //Con el # de factura carga los datos del encabezado solo para consulta.
                facturacion.getDatosEncabezado(data);

                facturacion.FACTURA = data;

                //Activa el tab de movimientos
                
                $('#registro_prefactura-tab').removeClass('active');
                $('#movimientos-tab').addClass('active');
                $('#registro_prefactura').removeClass('active');
                $('#movimientos').addClass('active'); 

                $("#li_movimientos").attr('style', 'display:block');
                $("#li_adicionales").attr('style', 'display:block');
                $("#li_detalle").attr('style', 'display:block');
                $("#li_totales").attr('style', 'display:block');
                $("#li_totales_tarifa").attr('style', 'display:block');

                $('#btn_calcular_fac').show();
                $('#btn_limpiar_fac').hide();
                $('#btn_guardar_encabFac').hide();
                // $('#btn_facturar').show();
                $('#btn_anular_fac').show();
              }
          })
          .fail(function (data) {
              if (data.responseJSON.ModelState) {
                  principal.processErrorsPopUp(data.responseJSON.ModelState);
              }else{
                swal({
                    title: "Facturación/Prefactura",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                  });
              }
          });


       $("#FAC_TIQUETE").attr('disabled', true);
       $("#FAC_TIPO_TIQUETE").attr('disabled', true);
       $("#FAC_FCH_TIQUETE").attr('disabled', true);
       $("#FAC_TIPO_CAMBIO").attr('disabled', true);
       $("#FAC_CUENTA").attr('disabled', true);
       $("#FAC_CONTRIBUYENTE").attr('disabled', true);
    },

//*********************************** Movimientos*********************************************//
    
    //Obtiene tarifas de movimientos
    getTarifas: function () {
        var tarifa = null;
        var nombre = "";
        var cb = function(data){
            $("#FMV_ITT_TARIFA").html(principal.arrayToOptions(data));
        };
        principal.get("/api/TX_FACTURAS_TB/getTarifas?nombre=" + nombre+ "&tarifa=" + tarifa,'',cb);
    },

    //Obtiene base de porcentaje
    getBases: function () {
        var cb = function(data){
            $("#FMV_BASE_DESCUENTO").html(principal.arrayToOptionsSelected(data,'B'));
        };
        principal.get("/api/TX_FACTURAS_TB/getBases",'',cb);
    },

    //Obtiene datos del encabezado insertado
    getDatosEncabezado: function (factura_p) {      
        var cb = function(data){
            $.each(data[0], function(i,v){
              
              //principal.setValueByName(i, v,'frm_movimientos');
              principal.setValueByName(i, v,'frm_registro_prefactura');
             //$('#frm_registro_prefactura :label').addClass('active');
              
              //Deshabilita el form de encabezado
              $("#frm_registro_prefactura :input").prop("disabled", true);
              
              //Carga en movimientos, los valores heredados 
              $("#FAC_FACTURA_ENC").val($("#FAC_FACTURA").val());
              $("#FAC_DUA_SALIDA_ENC").val($("#FAC_DUA_SALIDA").val());
              $("#FAC_VIAJE_TRANSITO_ENC").val($("#FAC_VIAJE_TRANSITO").val());
              $("#lbl_FAC_FACTURA_ENC").addClass('active');
              $("#lbl_FAC_DUA_SALIDA_ENC").addClass('active');
              $("#lbl_FAC_VIAJE_TRANSITO_ENC").addClass('active');

               $('#btn_guardar_encabFac').hide();
               $('#btn_limpiar_fac').hide();

            }); 
        };
        principal.get("/api/TX_FACTURAS_TB/GetObtieneEncabezado?factura=" + factura_p,'',cb);
    },

    //Busca y lista los movimientos de la factura
    getMovimientosFactura: function (factura_p) {
        var id = null;
        var cb = function(data){
            //Obtenemos la plantilla
            var templateText = $("#movimientosfac_table-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#movimientosfac_tablebody").html('');
            $.each(data, function (index, elem) {
                $("#movimientosfac_tablebody").append(tableTemplate(elem));
            });
        };
        principal.get("/api/TX_FACTURAS_TB/GetObtMovimientosFact?factura=" + factura_p + '&mov='+id,'',cb); 
    },

    //Agrega movimiento a factura
    AgregarMovimiento: function(){
        var movimiento_p = $("#FMV_MOVIMIENTO").val();
        var user_p = $("#UsReg").val();
        var cb = function(data){

            facturacion.getMovimientosFactura(facturacion.FACTURA);
            $("#FMV_MOVIMIENTO").val("");
            $("#FMV_MOVIMIENTO").focus();
          
        };
        principal.ajax("GET","/api/TX_FACTURAS_TB/GetObtMovimiento?factura=" + facturacion.FACTURA + '&movimiento='+movimiento_p + '&user='+user_p,cb, "Facturación/Movimientos", ""); 
    },

    //Muestra el modal de valores  
    modal_ValoresMovimiento: function (id_p) {              
        $("#myModalValores").modal('show');
        $("#FMV_PCT_DESCUENTO").val("0");
        $('#frm_Valores label').addClass('active');
        principal.resetAllFields('valores_formulario');
        
        facturacion.getCedulaEndoso(id_p);
        

        var cb = function(data){
            $.each(data[0], function(i,v){
              principal.setValueByName(i, v,'frm_Valores');
            }); 
        };
        principal.get("/api/TX_FACTURAS_TB/GetObtMovimientosFact?factura=" + facturacion.FACTURA + '&mov='+id_p,'',cb); 
    },

    //Muestra el modal de tarimas  
    modal_tarimasMovimiento: function (id_p, mov_p, tarifa_p) {              
        $("#myModalTarimas").modal('show');
        $("#num_movimiento").html(mov_p);
        $("#id_movimiento").val(id_p);
        $("#nom_tarifa").html(tarifa_p);
        $('#div_edit_tarima').addClass('fullHidden');


        facturacion.obtieneTarimas(id_p);
    },

    //Lista las tarimas del movimiento dado
    obtieneTarimas: function(id_p){
        var cb = function(data){
            //Obtenemos la plantilla
            var templateText = $("#tarimasMovimiento_table-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#tarimasMovimiento_tablebody").html('');
            $.each(data, function (index, elem) {
                $("#tarimasMovimiento_tablebody").append(tableTemplate(elem));
            });
        };
        principal.get("/api/TX_FACTURAS_TB/GetTarimas?factura_p=" + facturacion.FACTURA + '&id_mov_p='+id_p,'',cb); 
    },

    //Carga la información de la tarima a editar
    editarTarima: function(tarima_p,bultos_p,kilos_p,volumen_p){
        if($('#div_edit_tarima').hasClass('fullHidden'))
            $('#div_edit_tarima').removeClass('fullHidden')

       $('#BULTOS').val(bultos_p);
       $('#KILOS').val(kilos_p);
       $('#VOLUMEN').val(volumen_p);
       $('#TARIMA').val(tarima_p);

        $('#lbl_BULTOS').addClass('active');
        $('#lbl_KILOS').addClass('active');
        $('#lbl_VOLUMEN').addClass('active');
    },
  
    //Actualiza los datos de la tarima
    actualizarTarimas: function(){
        var bultos_p = $('#BULTOS').val();
        var kilos_p = $('#KILOS').val();
        var volumen_p = $('#VOLUMEN').val();
        var tarima_p = $('#TARIMA').val();

         $.ajax({
            method: 'POST',
            dataType: 'JSON',
            url: '/api/TX_FACTURAS_TB/ActualizarTarima', 
            data: { ID_TARIMA: tarima_p, BULTOS: bultos_p, KILOS: kilos_p, VOLUMEN: volumen_p },
            success: function (data) {

                facturacion.obtieneTarimas($("#id_movimiento").val());
                if(!$('#div_edit_tarima').hasClass('fullHidden'))
                    $('#div_edit_tarima').addClass('fullHidden')
            },
            failure: function (data) {

                console.log("fail");
            },
            error: function (xhr, textStatus, errorThrown,data) {
                console.log(textStatus);
                swal({
                      title: "Facturación",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                    });
                //console.log("error consulta guía");
            }
        });
    },

    //Validación de eliminar tarima
    borrarTarima: function(id){
        swal({
          title: "Está seguro?",
          text: "Eliminará la información de la tarima.",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            facturacion.eliminaTarima(id);                     
          } 
        });    
    },

    //Elimina tarima de movimiento
    eliminaTarima: function(id){
        var id = $("#id_movimiento").val();
        var cb = function(data){

            facturacion.obtieneTarimas(id);
          
        };
        principal.ajax("DELETE","/api/TX_FACTURAS_TB/DeleteTarimaMovimiento?id=" + id,cb, "Facturación", ""); 
    },

    //Obtiene la cédula endoso
    getCedulaEndoso: function (mov_p) {
        principal.getData(
            "/api/TX_FACTURAS_TB/GetCedEndoso?mov_p=" + mov_p,
            function (data) {
                if(data != ""){
                   $("#CED_ENDOSO").val(data);
                }
            }
        )
    },

    //Validación de eliminar movimiento
    eliminarMovimiento: function(id){
        swal({
          title: "Está seguro?",
          text: "Eliminará la información del movimiento.",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            facturacion.eliminarMov(id);                     
          } 
        });    

    },

    //Elimina el movimiento de factura
    eliminarMov: function(id){
        var user_p = $("#UsReg").val();
        var cb = function(data){

            facturacion.getMovimientosFactura(facturacion.FACTURA);
          
        };
        principal.ajax("DELETE","/api/TX_FACTURAS_TB/DeleteMovimiento?id=" + id,cb, "Facturación/Movimientos", ""); 
    },

    //calcula datos y actualiza valores en pantalla
    calculaValores: function (){
        var mov = $("#FMV_ID").val();
        var desc = $("#FMV_PCT_DESCUENTO").val();
        var base = $("#FMV_BASE_DESCUENTO").val();

        var cb = function(data){
           $("#FMV_ALMACENAJE").val(data.ALMACENAJE);
           $("#FMV_MANEJO").val(data.MANEJO);
           $("#FMV_SEGURO").val(data.SEGURO);
           $("#DESCUENTO").val(data.DESCUENTO);
           $("#IVA").val(data.IMP_IVA);
           $("#FMV_TOTAL_IVA").val(data.TOTAL);
          
        };
        principal.ajax("GET","/api/TX_FACTURAS_TB/GetCalculaPct?mov_p=" + mov + '&pct_desc_p='+ desc + '&base_p='+ base,cb, "Facturación", "");
    },

    //Vuelve a cargar la tabla de movimientos
    regresaValores: function (){
        facturacion.getMovimientosFactura(facturacion.FACTURA);
    },

    //carga la información de la tarifa a editar
    editarTarifaMovimiento: function (id_p,mov_p, tarifa_p){
        if($('#div_tarifa').hasClass('fullHidden'))
            $('#div_tarifa').removeClass('fullHidden')

        
        $('#btn_agregar_movimiento').hide();
        $('#btn_actualiza_tarifa').show();
        $('#btn_cancela_actualiza_tarifa').show();


        $("#id_mov_tarifa").val(id_p);
        $('#FMV_ITT_TARIFA').val(tarifa_p);
        $('#FMV_MOVIMIENTO').val(mov_p);
        $("#FMV_MOVIMIENTO").attr('disabled', true);
        $('#frm_movimientos label').addClass('active');
    },

    //Actualiza la tarifa del movimiento
    actualizaTarifa: function(){

        var tarifa_p = $("#FMV_ITT_TARIFA").val();
        var id_p = $("#id_mov_tarifa").val();
        //var user_p = $("#UsReg").val();
        var cb = function(data){
            $("#FMV_MOVIMIENTO").attr('disabled', false);
            $('#FMV_MOVIMIENTO').val("");
            $('#btn_agregar_movimiento').show();
            $('#btn_actualiza_tarifa').hide();
             $('#btn_cancela_actualiza_tarifa').hide();

            if(!$('#div_tarifa').hasClass('fullHidden'))
            $('#div_tarifa').addClass('fullHidden')

            facturacion.getMovimientosFactura(facturacion.FACTURA);
           
        };
        principal.ajax("POST","/api/TX_FACTURAS_TB/ACTUALIZA_TARIFA?tarifa_p=" + tarifa_p +'&id_p=' + id_p,cb, "Facturación", ""); 
    },

    cancelaActualizaTarifa: function(){
        if(!$('#div_tarifa').hasClass('fullHidden'))
            $('#div_tarifa').addClass('fullHidden')

        
        $('#btn_agregar_movimiento').show();
        $('#btn_actualiza_tarifa').hide();
        $('#btn_cancela_actualiza_tarifa').hide();

        $("#id_mov_tarifa").val("");
        $('#FMV_ITT_TARIFA').val("");
        $('#FMV_MOVIMIENTO').val("");
        $("#FMV_MOVIMIENTO").attr('disabled', false);
        $('#FMV_MOVIMIENTO').focus();
       // $('#frm_movimientos label').removeClass('active');
    },

//*********************************** Adicionales*********************************************//

    //Obtiene conceptos de movimiento
    getConceptos: function () {
        var tarifa = null;
        var nombre = "";
        var cb = function(data){
            $("#FCO_RUB_RUBRO").html(principal.arrayToOptions(data));
        };
        principal.get("/api/TX_FACTURAS_TB/Conceptos",'',cb);
    },

    //Agrega adicional de la factura
    agregarAdicional: function (){
        var concepto_p = $("#FCO_RUB_RUBRO").val();
        var monto_p = $("#FCO_MONTO").val();
        var user_p = $("#UsReg").val();

        if(monto_p != "" && monto_p != null){
             var cb = function(data){

            facturacion.getAdicionalesFactura(facturacion.FACTURA);
            facturacion.limpiarAdicional();
          
            };
            principal.ajax("POST","/api/TX_FACTURAS_TB/AgregarAdicional?fac=" +facturacion.FACTURA +'&rubro=' + concepto_p + '&monto='+monto_p + '&user='+user_p,cb, "Facturación/Adicionales", ""); 
        }else{
            swal({
                title: "Facturación/Adicionales",
                text: "El monto no puede ser vacío",
                icon: "error",
                button: "OK!",
              });
        }
       
    },

    //Busca y lista los adicionales de la factura
    getAdicionalesFactura: function (factura_p) {
        var id = null;
        var cb = function(data){
            //Obtenemos la plantilla
            var templateText = $("#adicionalesfac_table-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#adicionalesfac_tablebody").html('');
            $.each(data, function (index, elem) {
                $("#adicionalesfac_tablebody").append(tableTemplate(elem));
            });
        };
        principal.get("/api/TX_FACTURAS_TB/GetAdicionales?factura=" + factura_p,'',cb); 
    },

    //Limpia campos de adicional
    limpiarAdicional: function(){
         principal.resetAllFields('detInfo_adicionales');
    },

    //Validación de eliminar adicional
    eliminarAdicional: function(id){
        swal({
          title: "Está seguro?",
          text: "Eliminará la información del adicional.",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            facturacion.eliminarAdic(id);                     
          } 
        });    

    },

    //Elimina el adicional
    eliminarAdic: function(id){
        var user_p = $("#UsReg").val();
        var cb = function(data){

            facturacion.getAdicionalesFactura(facturacion.FACTURA);
          
        };
        principal.ajax("DELETE","/api/TX_FACTURAS_TB/DeleteAdicional?fac=" + facturacion.FACTURA+ '&rub='+ id,cb, "Facturación/Adicionales", ""); 
    },


//*********************************** Detalles *********************************************//
    
    //Busca por código al dar enter 
    buscarConductorCodigo: function(id){
        var cod = $('#TRA_TRANSPORTISTA').val();
            if(id != ""){
                 facturacion.getConductores("","",id)
            }
    },

    //muestra el modal de personas para buscar
    personas: function(){
        facturacion.limpiarFiltroPersonas();
        $("#myModalPersonas").modal('show');
    },

    //muestra el modal de conductores para buscar
    conductores: function(){
        facturacion.limpiarFiltroConductores();
        $("#myModalConductores").modal('show');
    },

    //Lista las personas
    getPersonas: function (id,nombre,cod) {
        var cb = function(data){
            if(cod != ""){

                $("#CFR_CEDULA").val($.trim(data[0].CFR_CEDULA));
                $("#CFR_NOMBRE").val($.trim(data[0].CFR_NOMBRE));
                //$("#CFR_CODIGO").val($.trim(data[0].CFR_CODIGO));
                $("#CFR_TELEFONO").val($.trim(data[0].CFR_TELEFONO));
                $('#frm_detallefac label').addClass('active');//principal.activeLabels();
                $("#FAC_AUTORIZADO_POR").focus();

            }else{
                //Obtenemos la plantilla
                var templateText = $("#tb_personas_table-template").html();
                  //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                var tableTemplate = Handlebars.compile(templateText);
                $("#tb_personas_tablebody").html('');
                $.each(data, function (index, elem) {
                    $("#tb_personas_tablebody").append(tableTemplate(elem));
                });
            }
            
        };
        principal.get("/api/TX_FACTURAS_TB/GetPersonas?id=" + id + "&nombre=" + nombre + "&cod=" + cod,'',cb); 
    },

    //Lista las conductores
    getConductores: function (id,nombre,cod) {       
        var cb = function(data){
             if(cod != ""){
                $("#TRA_CEDULA").val($.trim(data[0].TRA_CEDULA));
                $("#TRA_NOMBRE").val($.trim(data[0].TRA_NOMBRE));
                //$("#TRA_TRANSPORTISTA").val($.trim(data[0].CFR_CEDULA));

                $("#FAC_PLACA_VEHICULO").focus();

             }else{
                //Obtenemos la plantilla
                var templateText = $("#tb_conductores_table-template").html();
                  //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                var tableTemplate = Handlebars.compile(templateText);
                $("#tb_conductores_tablebody").html('');
                $.each(data, function (index, elem) {
                    $("#tb_conductores_tablebody").append(tableTemplate(elem));
                });
             }
            
        };
        principal.get("/api/TX_FACTURAS_TB/GetConductores?id=" + id + "&nombre=" + nombre+ "&cod=" + cod,'',cb); 
    },

    //Carga la información de la persona seleccionada
    selectPersona: function(id,ced,nombre,tel){
        $("#CFR_CEDULA").val($.trim(ced));
        $("#CFR_NOMBRE").val($.trim(nombre));
        $("#CFR_CODIGO").val($.trim(id));
        $("#CFR_TELEFONO").val($.trim(tel));

        $("#FAC_AUTORIZADO_POR").focus();

        $("#myModalPersonas").modal('hide'); 

        $('#frm_detallefac label').addClass('active');   
    },

    //Carga la información del conductor seleccionado
    selectConductor: function(id,ced,nombre){
        $("#TRA_CEDULA").val($.trim(ced));
        $("#TRA_NOMBRE").val($.trim(nombre));
        $("#TRA_TRANSPORTISTA").val($.trim(id));

         $("#FAC_PLACA_VEHICULO").focus();

        $("#myModalConductores").modal('hide');

        $('#frm_detallefac label').addClass('active');
    },

    //limpia campos de detalle
    LimpiarCamposDetalle: function(){
        principal.resetAllFields('detInfo_detallefac');
        $("#CFR_CODIGO").focus();
    },

    //Filtra en la lista de personas 
    getPersonasFlt: function(){
        var id = $("#PER_CEDULA").val();
        var nombre = $("#PER_NOMBRE").val();

        facturacion.getPersonas(id,nombre,"");
    },

    //Filtra en la lista de conductores 
    getConductoresFlt: function(){
        var id = $("#CONDUC_CEDULA").val();
        var nombre = $("#CONDUC_NOMBRE").val();

        facturacion.getConductores(id,nombre,"");        
    },

    //Limpia filtros de la lista de personas
    limpiarFiltroPersonas: function(){
        $("#PER_CEDULA").val("");
        $("#PER_NOMBRE").val("");
        facturacion.getPersonas("","","");
    },

    //Limpia filtros de la lista de conductores
    limpiarFiltroConductores: function(){
        $("#CONDUC_CEDULA").val("");
        $("#CONDUC_NOMBRE").val("");
        facturacion.getConductores("","","");
    },

    //Guarda detalle, cambia estado de factura, es el proceso final
    facturar: function (){

        $("#factura").val(facturacion.FACTURA); 
        $("#usuario").val($("#UsReg").val());

        $.post(
          "/api/TX_FACTURAS_TB/ActualizarEncabezado/",
           $('#frm_detallefac').serialize(),  //poner name igual que el modelo
          function (data) {
              if(data != null && data != ""){
                
                //Se imprime la tiquete factura !!
                window.open("http://merxalmacenadora/WebEtiquetaNCI/RepComprobanteSalHR.aspx?Factura=" + facturacion.FACTURA);

                facturacion.FuncionalidadTiqueteAtencion();
                $("#FAC_DUA_SALIDA").focus();

                $('#btn_guardar_encabFac').show();
                $('#btn_limpiar_fac').show();

                $('#btn_calcular_fac').hide();
                 $('#btn_facturar').hide();
                 $('#btn_anular_fac').hide();
                 $('#btn_limpiar_detalle').hide();
               
                $('#btn_obt_tiquete').hide();

                 $("#li_movimientos").attr('style', 'display:none');
                $("#li_adicionales").attr('style', 'display:none');
                $("#li_detalle").attr('style', 'display:none');
                $("#li_totales").attr('style', 'display:none');
                $("#li_totales_tarifa").attr('style', 'display:none');


                if (!$('#registro_prefactura').hasClass('active'))
                    $('#registro_prefactura').addClass('active');
                    $('#movimientos').removeClass('active');
                    $('#adicionales').removeClass('active');
                    $('#detalle').removeClass('active');
                    $('#totales').removeClass('active');
                
                if (!$('#registro_prefactura-tab').hasClass('active'))
                     $('#registro_prefactura-tab').addClass('active');
                    $('#movimientos-tab').removeClass('active');
                    $('#adicionales-tab').removeClass('active');
                    $('#detalle-tab').removeClass('active');
                    $('#totales-tab').removeClass('active');


                facturacion.FACTURA = null;
              }
          })
          .fail(function (data) {
              if (data.responseJSON.ModelState) {
                  principal.processErrorsPopUp(data.responseJSON.ModelState);
              }else{
                swal({
                    title: "Facturación",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                  });
              }
          });        
    },


//*********************************** Generales*********************************************//
    
    // Obtiene los totales de la factura
    totales: function(){
        var fac_p = facturacion.FACTURA;
        var cb = function(data){

            //Obtenemos la plantilla
            var templateText = $("#totalesfac_table-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#totalesfac_tablebody").html('');
            $.each(data, function (index, elem) {
                $("#totalesfac_tablebody").append(tableTemplate(elem));
            });
          
        };
        principal.ajax("GET","/api/TX_FACTURAS_TB/GetTotales?factura=" + fac_p,cb, "Facturación", ""); 
    },

    // Obtiene los totales de la factura
    totalesTarifa: function(){
        var fac_p = facturacion.FACTURA;
        var cb = function(data){

            //Obtenemos la plantilla
            var templateText = $("#totalesfac_tarifa_table-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#totalesfac_tarifa_tablebody").html('');
            $.each(data, function (index, elem) {
                $("#totalesfac_tarifa_tablebody").append(tableTemplate(elem));
            });
          
        };
        principal.ajax("GET","/api/TX_FACTURAS_TB/GetTotalesTarifa?factura=" + fac_p,cb, "Facturación", ""); 
    },

    //Obtiene los cálculos de la factura 
    calcular: function (){
        $("#myModalCalculos").modal('show');

        var fac_p = facturacion.FACTURA;

        var cb = function(data){
            
            // Grab the template script
          var theTemplateScript = $("#expressions-template").html();

          // Compile the template
          var theTemplate = Handlebars.compile(theTemplateScript);

          // Define our data object
          var context={
            "description": {
              "ADICIONALES": data[0].ADICIONALES,
              "DESCUENTO": data[0].DESCUENTO,
              "SUBTOTAL": data[0].SUBTOTAL,
              "IVA": data[0].IVA,
              "TOTAL": data[0].TOTAL
            }
          };

          // Pass our data to the template
          var theCompiledHtml = theTemplate(context);

          // Add the compiled html to the page
          $('.content-placeholder').html(theCompiledHtml);
          
        };
        principal.ajax("GET","/api/TX_FACTURAS_TB/GetCalculos?factura=" + fac_p,cb, "Facturación", ""); 
    },

    //Anula la prefactura
    anularEncabezado: function (){
        var id_p = facturacion.FACTURA;
        
        swal({
          title: "Está seguro?",
          text: "Anulará el encabezado de factura #: " + id_p,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            facturacion.anula();
          } 
        });       
    },

    anula: function(){
        var id_p = facturacion.FACTURA;
        var user_p = $("#UsReg").val();
        var cb = function(data){
            
        swal({
            title: "Facturación",
            text: "Se anuló el encabezado: " + facturacion.FACTURA,
            icon: "success",
            button: "OK!",
            }).then((value) => {
                $("#li_movimientos").attr('style', 'display:none');
                $("#li_adicionales").attr('style', 'display:none');
                $("#li_detalle").attr('style', 'display:none');
                $("#li_totales").attr('style', 'display:none');
                $("#li_totales_tarifa").attr('style', 'display:none');
                $("#li_registro_prefactura").attr('style', 'display:block');
                
                if (!$('#registro_prefactura').hasClass('active'))
                     $('#registro_prefactura').addClass('active');
                    $('#movimientos').removeClass('active');
                    $('#adicionales').removeClass('active');
                    $('#detalle').removeClass('active');
                    $('#totales').removeClass('active');
                
                if (!$('#registro_prefactura-tab').hasClass('active'))
                     $('#registro_prefactura-tab').addClass('active');
                    $('#movimientos-tab').removeClass('active');
                    $('#adicionales-tab').removeClass('active');
                    $('#detalle-tab').removeClass('active');
                    $('#totales-tab').removeClass('active');

                 facturacion.LimpiarCamposEncabezado();   

                 $("#FAC_IMP_CEDULA").prop("disabled", false);
                 facturacion.getImportadores("","");

                 facturacion.FuncionalidadTiqueteAtencion();                            
            });
        };
        principal.ajax("POST","/api/TX_FACTURAS_TB/ANULAR_ENCAB?factura_p=" + id_p +'&user_p=' + user_p,cb, "Facturación", "");
    },

    //Cancela el tiquete  
    cancelarTiquete: function(){
        var id_p =  facturacion.numTique;//$("#FAC_TIQUETE").val();
        var tipo_p =  facturacion.tipTique;
        var estado_p = "N"
        //var user_p = $("#UsReg").val();
        var cb = function(data){
            
        swal({
            title: "Facturación/Tiquete",
            text: "Se canceló el tiquete: " + id_p,
            icon: "success",
            button: "OK!",
            }).then((value) => {
                principal.resetAllFields('detInfo_registro_prefactura');
               // facturacion.LimpiarInicio();
                facturacion.numTique = null;
                facturacion.tipTique = null;
                facturacion.fecTique = null;
                facturacion.FACTURA = null; 


                var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd/MM/yyyy');
                $("#FAC_FCH_DUA").val(todayDate);
                $("#FAC_DESDE_EXENCION").val(todayDate);
                $("#FAC_HASTA_EXENCION").val(todayDate);

                var ddl = $('#FAC_ADC_AGENTE_ID').data("kendoComboBox");
                if(ddl != undefined)
                ddl.setDataSource([]);

                facturacion.getImportadores("","");

                facturacion.getTipoCambio();



                //Oculta los botones de la cinta
                 $('#btn_calcular_fac').hide();
                 $('#btn_facturar').hide();
                 $('#btn_anular_fac').hide();
                 $('#btn_guardar_encabFac').hide();
                 $('#btn_limpiar_fac').hide();
                 $('#btn_limpiar_detalle').hide();
                 $('#btn_obt_tiquete').show();
                 
                //Habilita el form y deshabilita campos de inicio disable 
                $("#frm_registro_prefactura :input").prop("disabled", false);
                $("#FAC_TIQUETE").prop("disabled", true);
                $("#FAC_TIPO_TIQUETE").prop("disabled", true);
                $("#FAC_FCH_TIQUETE").prop("disabled", true);
                $("#FAC_TIPO_CAMBIO").prop("disabled", true);
                $("#FAC_CUENTA").prop("disabled", true);
                $("#FAC_CONTRIBUYENTE").prop("disabled", true);
                $("#FAC_FACTURA").prop("disabled", true);

                $("#li_movimientos").attr('style', 'display:none');
                $("#li_adicionales").attr('style', 'display:none');
                $("#li_detalle").attr('style', 'display:none');
                $("#li_totales").attr('style', 'display:none');
                $("#li_totales_tarifa").attr('style', 'display:none');
                $("#li_registro_prefactura").attr('style', 'display:block');
                
                $("#registro_prefactura").show();

                if (!$('#registro_prefactura').hasClass('active'))
                     $('#registro_prefactura').addClass('active');
                
                if (!$('#registro_prefactura-tab').hasClass('active'))
                     $('#registro_prefactura-tab').addClass('active');

                 //facturacion.LimpiarCamposEncabezado();               

            });
        };
        principal.ajax("POST","/api/TX_FACTURAS_TB/CANCELA_TIQUETE?id_p=" + id_p +'&estado_p=' + estado_p + '&tipo_p=' + tipo_p,cb, "Facturación/Tiquete", ""); 
    }

};

$(document).ready(function () {
    facturacion.init();
})