﻿var registro_muestras = {

    init: function () {
        registro_muestras.LimpiarCamposRegistroMuestras();
        registro_muestras.CargaFechas();
        registro_muestras.CargarControlesPrevios();
        registro_muestras.Eventos();

        // Cuando hace click en el tab de agencia
        $('#registro-muestras-tab').on('click', function () {                  
          $("#s_boleta").focus();          
        });
    },

    onfocus: function () {
        if ($("#lbl_boleta_filtro").hasClass('lblfocus'))
            $("#lbl_boleta_filtro").removeClass('lblfocus');
    },

    LimpiarCamposRegistroMuestras: function () {
        principal.resetAllFields('detInfo_registro_muestras');
        principal.resetAllFields('filtro_detInfo_registro_muestras');
        TecladoFuncionalidadPersonalizada();
        $("#btn_actualizar_muestra").attr('style', 'display:none');
        $("#btn_registrar_muestra").attr('style', 'display:block');
        $("#li_detalle").attr('style', 'display:none');
    },

    CargarControlesPrevios: function () {
        registro_muestras.getTipoMuestra();
        registro_muestras.getMotivo();
    },

    Eventos: function () {
        $("#MUE_IEN_ENTRADA").focusout(function (e) {
            registro_muestras.getConsecutivoData();
        });
        $("#btn_add_item").on("click", function (e) {
            e.preventDefault();
            registro_muestras.InsertarDetalleMuestras()
        })
        $("#btn_edit_item").on("click", function (e) {
            e.preventDefault();
            registro_muestras.ActualizarDetalleMuestras()
        })
        $("#btn_canceledit_item").on("click", function (e) {
            e.preventDefault();
            registro_muestras.CancelarActualizarDetalleMuestras()
        })
    },

    getConsecutivoData: function () {
        var consecutivo = $("#MUE_IEN_ENTRADA").val();

        if (consecutivo.length > 0) {
            $.ajax({
                "url": "/api/TX_REGISTRO_MUESTRAS_TB/Consecutivo",
                method: 'POST',
                dataType: 'json',
                data: { consecutivo: consecutivo },
                success: function (data) {
                    if (jQuery.type(data) == "object") {
                        registro_muestras.ActiveCampos();
                        registro_muestras.MuestraDatosConsecutivo(data);
                    } else {
                        swal({
                            title: "Registro de Muestras",
                            text: "No existen datos para el consecutivo: #" + consecutivo ,
                            icon: "error",
                            button: "OK!"
                        });
                    }
                },
                failure: function (data) {

                    //console.log("fail");
                    if (data.responseJSON.Message == "false") {
                        prod_nc.infoConsecutivo(consecutivo, sistema);
                    } else {
                        swal({
                            title: "Registro Muestras",
                            text: data.responseJSON.Message,
                            icon: "error",
                            button: "OK!"

                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.Message == "false") {
                        prod_nc.infoConsecutivo(consecutivo, sistema);
                    } else {
                        swal({
                            title: "Registro Muestras",
                            text: data.responseJSON.Message,
                            icon: "error",
                            button: "OK!",

                        });
                    }
                }
            });
        }
    },

    getDatos: function (filtro, cb, api) {
        principal.get(
            api,
            { 'filtro': filtro },
            cb
        )
    },

    //Controles iniciales
    getTipoMuestra: function () {
        principal.getData(
            "/api/TX_REGISTRO_MUESTRAS_TB/TipoMuestra",
            function (data) {
                $("#MUE_TIPO_MUESTRA").html(principal.arrayToOptionsSelectedWithDefault(data, "", ""));
            }
        )
    },

    getMotivo: function () {
        principal.getData(
            "/api/TX_REGISTRO_MUESTRAS_TB/Motivo",
            function (data) {
                $("#MUE_CODIGO_MOTIVO").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        )
    },


    CargaFechas: function () {
        //registro_muestras.KendoDate($("#MUE_FECHA"));
        registro_muestras.KendoDate($("#MUE_FCH_MUESTRA"));
        registro_muestras.KendoDate($("#MUE_FCH_DEVOLUCION"));
    },

    KendoDate: function (control) {
        control.kendoDatePicker({
            format: "dd/MM/yyyy",
            value: new Date(),
            dateInput: true,
            month: {
                empty: '<span class="k-state-disabled">#=data.value#</span>'
            }
        });
    },

// Procesos de consulta e inserción
    BuscarRegistroMuestras: function () {
        //variables
        var s_boleta = $("#s_boleta").val();
        var s_consecutivo = $("#s_consecutivo").val();
        var s_manifiesto = $("#s_manifiesto").val();
        var s_guia = $("#s_guia").val();

        registro_muestras.MostrarRegistroMuestras(s_boleta, s_consecutivo, s_manifiesto, s_guia);
    },

    MostrarRegistroMuestras: function (s_boleta, s_consecutivo, s_manifiesto, s_guia) {
        $.ajax({
            method: 'POST',
            dataType: 'JSON',
            url: '/api/TX_REGISTRO_MUESTRAS_TB/BuscarRegistroMuestras',
            data: { s_boleta: s_boleta, s_consecutivo: s_consecutivo, s_manifiesto: s_manifiesto, s_guia: s_guia},
            success: function (data) {
                if (data !== null) {
                    if (principal.isArray(data) && data.length > 1) {
                        registro_muestras.MostrarListaRegistroMuestras(data);
                    } else {
                        if (typeof data === 'string' || data instanceof String) {
                            swal({
                                title: "Registro de Muestras",
                                text: data,
                                icon: "error",
                                button: "OK!",
                            })
                        } else {
                            if (data.length > 0) {
                                //trae los datos de la queja
                                registro_muestras.ActiveCampos();
                                registro_muestras.MuestraDatosRegistroMuestras(data);
                                //registro_muestras.MuestraDatos //tx_qry_reclamos_sc_pr
                                $("#btn_actualizar_muestra").attr('style', 'display:block');
                                $("#btn_registrar_muestra").attr('style', 'display:none');
                                $("#li_detalle").attr('style', 'display:block');
                                var boleta = data[0].MUE_BOLETA;
                                registro_muestras.MuestraDatosDetalleMuestras(boleta);
                            } else {
                                swal({
                                    title: "Registro de Muestras",
                                    text: "No existen datos para los valores consultados.",
                                    icon: "error",
                                    button: "OK!",
                                });

                            }
                        }

                    }
                }
            },
            failure: function (data) {

                console.log("fail");
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(textStatus);
                //console.log("error consulta guía");
            }
        });
    },

    MuestraDatosDetalleMuestras: function (boleta) {
        $.ajax({
            url: '/api/TX_REGISTRO_MUESTRAS_TB/DetalleRegistroMuestras?boleta=' + boleta,
            // data: { boleta: boleta },
            method: "GET",
            dataType: "json",
            success: function (data) {
                var templateText = $("#registro_muestras_detalle_table-template").html();
                var tableTemplate = Handlebars.compile(templateText);
                $("#registro_muestras_detalle_tablebody").html('');
                $.each(data, function (index, elem) {
                    console.log("detalle", elem);
                    $("#registro_muestras_detalle_tablebody").append(tableTemplate(elem));
                });
            },
            error: function (err) {

            }
        });
    },

    MostrarListaRegistroMuestras: function (data) {
        var templateText = $("#registro_muestras_table-template").html();
        var tableTemplate = Handlebars.compile(templateText);
        $("#registro_muestras_tablebody").html('');
        $.each(data, function (index, elem) {
            $("#registro_muestras_tablebody").append(tableTemplate(elem));
        });
        $("#myModalRegistroMuestras").modal('show');
    },

    MostrarRegistroMuestrasSeleccionado: function (s_mue_muestra) {
        registro_muestras.MostrarRegistroMuestras(s_mue_muestra);
        $("#myModalRegistroMuestras").modal('hide');
    },

    MuestraDatosRegistroMuestras: function (data) {
        $.each(data[0], function (i, v) {
            principal.setValueByName(i, v, 'frm_registro_muestras');
            principal.setValueByName(i, v, 'frm_registro_devolucion');           
        }); 
        
    },

    MuestraDatosConsecutivo: function (data) {
       for (var campo in data) {           
            
           if(data[campo] != null){
               principal.setValueByName(campo, data[campo],'frm_registro_muestras');
           }  
        }
    },

    GuardarMuestra: function () {
        $("#USER_MERX").val($("#UsReg").val());

        $("#MUE_MANIFIESTO").attr('disabled', false);
        $("#MUE_GUIA").attr('disabled', false);
        $("#TOTBULTOS").attr('disabled', false);
        $("#TOTKILOS").attr('disabled', false);
        $("#MUE_CONSIGNATARIO").attr('disabled', false);
        //console.log($('#frm_registro_muestras').serialize());
        //return;
        $.ajax({
            url: "/api/TX_REGISTRO_MUESTRAS_TB/Guardar",
            method: "POST",
            data: $('#frm_registro_muestras').serialize(),
            dataType: "JSON",
            success: function (data) {
                if (typeof data === 'string' || data instanceof String) {
                    swal({
                        title: "Registro de Muestras",
                        text: data,
                        icon: "error",
                        button: "OK!",
                    })
                } else {
                    swal({
                        title: "Registro de Muestras",
                        text: "Registro guardado exitosamente.",
                        icon: "success",
                        button: "OK!",
                    }).then((value) => {
                        registro_muestras.LimpiarCamposRegistroMuestras();
                    });
                }
            },
            fail: function (data) {
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                }
            }
        });

        $("#MUE_MANIFIESTO").attr('disabled', true);
        $("#MUE_GUIA").attr('disabled', true);
        $("#TOTBULTOS").attr('disabled', true);
        $("#TOTKILOS").attr('disabled', true);
    },

    ActualizarMuestra: function () {
        var s_boleta = $("#MUE_BOLETA").val();
        $.ajax({
            url: "/api/TX_REGISTRO_MUESTRAS_TB/Actualizar",
            method: "POST",
            data: $('#frm_registro_muestras, #frm_registro_devolucion').serialize(),
            dataType: "JSON",
            success: function (data) {
                //if (typeof data === 'string' || data instanceof String) {
                //    swal({
                //        title: "Registro de Muestras",
                //        text: data,
                //        icon: "error",
                //        button: "OK!",
                //    })
                //} else {
                    swal({
                        title: "Registro de Muestras",
                        text: "Registro actualizado exitosamente. Id de registro: #" + s_boleta,
                        icon: "success",
                        button: "OK!",
                    }).then((value) => {
                        registro_muestras.LimpiarCamposRegistroMuestras();
                    });
                //}
            },
            failure: function (data) {

                //console.log("fail");
                if (data.responseJSON.Message == "false") {
                    prod_nc.infoConsecutivo(consecutivo, sistema);
                } else {
                    swal({
                        title: "Muestras",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            },
            error: function (data) {
                if (data.responseJSON.Message == "false") {
                    prod_nc.infoConsecutivo(consecutivo, sistema);
                } else {
                    swal({
                        title: "Muestras",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            }
        });
    },

    InsertarDetalleMuestras: function () {
        var boleta = $("#MUE_BOLETA").val();
        $.ajax({
            url: "/api/TX_REGISTRO_MUESTRAS_TB/InsertarDetalleMuestra",
            method: "POST",
            data: $('#frm_detalle_muestras').serialize() + "&boleta=" + boleta,
            dataType: "JSON",
            success: function (data) {
                var error = false;
                if (typeof data === 'string' || data instanceof String) {
                    if (data.length > 0)
                        error = true;
                }
                if (error) {
                    swal({
                        title: "Registro de Detalle de Muestras",
                        text: data,
                        icon: "error",
                        button: "OK!",
                    })
                } else {
                    swal({
                        title: "Registro Detalles Muestras",
                        text: "Item agregado exitosamente.",
                        icon: "success",
                        button: "OK!",
                    }).then((value) => {
                        registro_muestras.MuestraDatosDetalleMuestras(boleta);
                        registro_muestras.LimpiarCamposDetalle();
                    });
                }
            },
            fail: function (data) {
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                }
            }
        });
    },

    ActualizarDetalleMuestras: function () {
        var boleta = $("#MUE_BOLETA").val();
        $.ajax({
            url: "/api/TX_REGISTRO_MUESTRAS_TB/ActualizarDetalleMuestra",
            method: "POST",
            data: $('#frm_detalle_muestras').serialize() + "&boleta=" + boleta,
            dataType: "JSON",
            success: function (data) {
                var error = false;
                if (typeof data === 'string' || data instanceof String) {
                    if (data.length > 0)
                        error = true;
                }
                if (error) {
                    swal({
                        title: "Registro de Detalle de Muestras",
                        text: data,
                        icon: "error",
                        button: "OK!",
                    })
                } else {
                    swal({
                        title: "Registro Detalles Muestras",
                        text: "Item actualizado exitosamente.",
                        icon: "success",
                        button: "OK!",
                    }).then((value) => {
                        registro_muestras.MuestraDatosDetalleMuestras(boleta);
                        registro_muestras.LimpiarCamposDetalle();
                    });
                }
            },
            fail: function (data) {
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                }
            }
        });
    },

    updateDetalle: function (boleta, item, cantidad, kilos, descripcion, cantidad_dev, kilos_dev) {
        $("#DMU_ITEM").val(item);
        $("#DMU_CANTIDAD").val(cantidad);
        $("#DMU_KILOS").val(kilos);
        $("#DMU_DESCRIPCION").val(descripcion);
        $("#DMU_CANTIDAD_DEV").val(cantidad_dev);
        $("#DMU_KILOS_DEV").val(kilos_dev);

        $("#btn_add_item").attr('style', 'display:none');
        $("#btn_edit_item").attr('style', 'display:block');
        $("#btn_canceledit_item").attr('style', 'display:block');
    },

    CancelarActualizarDetalleMuestras: function () {
        $("#DMU_ITEM").val("");
        $("#DMU_CANTIDAD").val("");
        $("#DMU_KILOS").val("");
        $("#DMU_DESCRIPCION").val("");
        $("#DMU_CANTIDAD_DEV").val("");
        $("#DMU_KILOS_DEV").val("");

        $("#btn_add_item").attr('style', 'display:block');
        $("#btn_edit_item").attr('style', 'display:none');
        $("#btn_canceledit_item").attr('style', 'display:none');
    },
    
    limpiarFiltros: function () {
        $("#s_mue_muestra").val("");
        principal.resetAllFields('detInfo_registro_muestras');
        principal.resetAllFields('detInfo_detalle');
        principal.ActivarTab('registro_muestras');
        principal.resetAllFields('filtro_detInfo_registro_muestras');
        $("#btn_actualizar_muestra").attr('style', 'display:none');
        $("#btn_registrar_muestra").attr('style', 'display:block');
        principal.deactivateLabels();
        $("#s_boleta").focus();
        $("#registro_muestras_detalle_tablebody").html('');
        registro_muestras.CargaFechas();
    },

    ActiveCampos: function () {
        $('label').addClass('active');
    },

    DeactiveCampos: function () {
        $('label').removeClass('active');
    },

    LimpiarCamposDetalle: function () {
        principal.resetAllFields('detInfo_detalle');
        $("#btn_add_item").attr('style', 'display:block');
        $("#btn_edit_item").attr('style', 'display:none');
        $("#btn_canceledit_item").attr('style', 'display:none');
    }
};

$(document).ready(function () {
    registro_muestras.init();
})