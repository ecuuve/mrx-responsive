﻿var colacarrusel = {
    pickedup : null,
    init: function () {
      colacarrusel.QUE_limpiar_SC();
      colacarrusel.QUE_limpiar_AC();
      

        $('#aforocarrusel-tab').on('click', function () { 
           $("#MERC_AC_table1body").html("");
           $('#th_mercAC').hide();
           $("#in_flr_que_ien_entradaAC").val("");
        }); 

        $('#salidacarrusel-tab').on('click', function () { 
            $("#MERC_SC_table1body").html("");
            $('#th_mercSC').hide();
            $("#in_flr_que_ien_entradaSC").val("");
        });   

        $('#in_flr_que_ien_entradaAC').bind("cut copy paste",function(e) {
             e.preventDefault();
         });
        $('#in_flr_que_ien_entradaSC').bind("cut copy paste",function(e) {
             e.preventDefault();
         });    
    },

    QUE_CargaInicialSC:function(){      
        var cb = function(){
            colacarrusel.QUE_CheckearColas();
        };
        colacarrusel.QUE_Listar_SC(cb);
    },

    QUE_CargaInicialAC:function(){      
        var cb = function(){
            colacarrusel.QUE_CheckearColas();
        };
        colacarrusel.QUE_Listar_AC(cb);
    },

    QUE_Funcionalidad_Click: function(tipo,consecutivo, linea){  
        if(tipo == 'ColaAC'){
            //Limpia tabla de mercancias y la oculta
             $("#MERC_AC_table1body").html("");
             $('#th_mercAC').hide();
        }else{
            //Limpia tabla de mercancias y la oculta
             $("#MERC_SC_table1body").html("");
             $('#th_mercSC').hide();
        }     
        
        //regresa al color negro antes de haberlo seleccionado
         if (colacarrusel.pickedup != null) {
             colacarrusel.pickedup.css( "background-color", "#3C3C3C" );
         }
         //cambia el color de la fila a gris
         $( linea ).css( "background-color", "grey" );
 
        colacarrusel.pickedup = $( linea );

          var cb = function(){
              colacarrusel.QUE_CheckearTarimas(tipo);
          };

         colacarrusel.QUE_Listar_Mercancias(consecutivo,tipo,cb);
    },
  
    QUE_Listar_AC: function(callback){        
          var p_consecutivo = $("#in_flr_que_ien_entradaAC").val();
          var p_tipoCola = "AC";
          if (p_consecutivo == ""){
            p_consecutivo = null;
          }
          
            $.getJSON('/api/TX_COLAS_TB/GetColas?consecutivo='+ p_consecutivo + '&tipoCola=' + p_tipoCola).then(function (data) {
                if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Colas Carrusel",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                }else{
                  if(data.length == 0 && p_consecutivo != null){
                    swal({
                          title: "Colas Carrusel",
                          text: "No existe información para el consecutivo dado.",
                          icon: "error",
                          button: "OK!",
                        })
                  }else{
                      //Obtenemos la plantilla
                      $('#th_ColaAC').show();
                      $("#th_ColaAC").attr('style', 'display:block');
                      var templateText = $("#QUE_AC_table-template").html();
                      //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                      var tableTemplate = Handlebars.compile(templateText);
                      $("#ColaAC_table1body").html('');
                      $.each(data, function (index, elem) {
                        $("#ColaAC_table1body").append(tableTemplate(elem));
                      })
                      $('#th_mercAC').hide();

                       $("#ColaAC_Table > tbody > tr").on("click", function(e) {
                        if($("#aforocarrusel-tab").hasClass("active")){
                          var consecutivo = $(this).find("td").eq(1).html();
                          colacarrusel.QUE_Funcionalidad_Click("ColaAC", consecutivo, $( this ));
                        }
                      });    

                      if(typeof(callback) == "function"){
                          callback();
                      }
                  }

                }
                
            });
             
    },

    QUE_Listar_SC:function(callback){
        var p_consecutivo = $("#in_flr_que_ien_entradaSC").val();
          
          if (p_consecutivo == ""){
            p_consecutivo = null;
          }
           
            $.getJSON('/api/TX_COLAS_TB/GetColasCarrusel?consecutivo='+ p_consecutivo).then(function (data) {
                if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Colas Aforo Carrusel",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                }else{
                  if(data.length == 0 && p_consecutivo != null){
                    swal({
                          title: "Colas Salida Carrusel",
                          text: "No existe información para el consecutivo dado.",
                          icon: "error",
                          button: "OK!",
                        })
                  }else{
                      //Obtenemos la plantilla
                      $('#th_ColaSC').show();
                      $("#th_ColaSC").attr('style', 'display:block');
                      var templateText = $("#QUE_SC_table-template").html();
                      //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                      var tableTemplate = Handlebars.compile(templateText);
                      $("#ColaSC_table1body").html('');
                      $.each(data, function (index, elem) {
                      $("#ColaSC_table1body").append(tableTemplate(elem));
                      })
                      $('#th_mercSC').hide();

                      $("#ColaSC_Table > tbody > tr").on("click", function(e) {
                        if($("#salidacarrusel-tab").hasClass("active")){
                          var consecutivo = $(this).find("td").eq(1).html();
                          colacarrusel.QUE_Funcionalidad_Click("ColaSC", consecutivo, $( this ));
                        }
                      });

                      if(typeof(callback) == "function"){
                          callback();
                      }
                  }

                }
                
            });
    },

    QUE_CheckearColas: function(){
        $('#ColaSC_Table tbody tr').each(function(){
            var accion1 = $(this).find('td span input[id^="rdPrev"]');
            var accion2 = $(this).find('td span input[id^="rdAut"]');

            colacarrusel.QUE_RevisaCheck(accion1);
            colacarrusel.QUE_RevisaCheck(accion2);
            
        });

        $('#ColaAC_Table tbody tr').each(function(){
            var accion1 = $(this).find('td span input[id^="rdPrev"]');
            var accion2 = $(this).find('td span input[id^="rdAut"]');

            colacarrusel.QUE_RevisaCheck(accion1);
            colacarrusel.QUE_RevisaCheck(accion2);
            
        });
    },

    QUE_RevisaCheck:function(accion){
        var checkbox = accion.attr('id');

            //console.log(checkbox, $('#' + checkbox).val());
             if(!accion.prop('checked') && $('#' + checkbox).val() == "S"){
                
                 $('#' + checkbox).prop('checked', true);
             }

             if(accion.prop('checked') && $('#' + checkbox).val() == "N"){
                
                 $('#' + checkbox).prop('checked', false);
             }
    },

    QUE_Listar_Mercancias: function(consecutivo_p,tipo, callback){
        if(tipo=="ColaAC"){
            //Obtenemos la plantilla
            $('#th_mercAC').show();
            $("#th_mercAC").attr('style', 'display:block');
            var templateText = $("#MERC_AC_table-template").html();
            //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#MERC_AC_table1body").html('');
            $.getJSON('/api/TX_COLAS_TB/GetMercCola?consecutivo='+ consecutivo_p).then(function (data) {
                $.each(data, function (index, elem) {
                    $("#MERC_AC_table1body").append(tableTemplate(elem));
                })
                if(typeof(callback) == "function"){
                    callback();
                }
            });
        }else{
            //Obtenemos la plantilla
            $('#th_mercSC').show();
            $("#th_mercSC").attr('style', 'display:block');
            var templateText = $("#MERC_SC_table-template").html();
            //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#MERC_SC_table1body").html('');
            $.getJSON('/api/TX_COLAS_TB/GetMercCola?consecutivo='+ consecutivo_p).then(function (data) {
                $.each(data, function (index, elem) {
                    $("#MERC_SC_table1body").append(tableTemplate(elem));
                })
                if(typeof(callback) == "function"){
                    callback();
                }
            });
        }
  
    },

    QUE_CheckearTarimas: function(tipo){
        if(tipo=="ColaAC"){
            tipo="AC";
        }else{
            tipo="SC";
        }
        $('#MERC_'+tipo+'_Table tbody tr').each(function(){
            var accion = $(this).find('td span input[type="checkbox"]');
            var checkbox = accion.attr('id');

            if(!accion.prop('checked') && $('#' + checkbox).val() == "S"){
                
                $('#' + checkbox).prop('checked', true);
            }

            if(accion.prop('checked') && $('#' + checkbox).val() == "N"){
                
                $('#' + checkbox).prop('checked', false);
            }
        });
    },

    QUE_Atender: function(linea,consecutivo,tipo){
        var accion = "AT";
        colacarrusel.QUE_UP_Atender(linea,consecutivo,tipo,accion);
    },


    QUE_UP_Atender: function(linea,consecutivo,tipo,accion){
        var UserMERX = $('#UsReg').val();
        
            $.ajax({
                url: '/api/TX_COLAS_TB/?linea_p=' + linea + '&consecutivo_p='+ consecutivo + '&tipo_p='+ tipo + '&user_p='+ UserMERX + '&accion_p='+ accion,
                type: 'PUT',
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Atención de Colas Carrusel",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{

                        swal({
                        title: "Colas Carrusel",
                        text: "Se atendió correctamente la línea.",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            if(tipo=="SC"){
                                colacarrusel.QUE_CargaInicialSC();
                                 $('#th_mercSC').hide();
                            }else{
                                colacarrusel.QUE_CargaInicialAC();
                                 $('#th_mercAC').hide();
                            }
                            
                        });
                    }  
                },
            })               
    },

    QUE_Finalizar: function(linea,consecutivo,tipo){
       var accion = "FN";
       colacarrusel.QUE_UP_Finalizar(linea,consecutivo,tipo,accion);      
    },

    QUE_UP_Finalizar: function(linea,consecutivo,tipo,accion){
        var UserMERX = $('#UsReg').val();
        
            $.ajax({
                url: '/api/TX_COLAS_TB/?linea_p=' + linea + '&consecutivo_p='+ consecutivo + '&tipo_p='+ tipo + '&user_p='+ UserMERX + '&accion_p='+ accion,
                type: 'PUT',
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Finalizar Colas Carrusel",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{

                        swal({
                        title: "Colas Carrusel",
                        text: "Se finalizó correctamente la línea.",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            if(tipo=="SC"){
                                colacarrusel.QUE_CargaInicialSC();
                                $('#th_mercSC').hide();
                            }else{
                                colacarrusel.QUE_CargaInicialAC();
                                $('#th_mercAC').hide();
                            }
                            
                        });
                    }  
                },
            })               
    },

    QUE_Terminar: function(linea,consecutivo,tipo){
       var accion = "TM";
       colacarrusel.QUE_UP_Terminar(linea,consecutivo,tipo,accion);      
    },

    QUE_UP_Terminar: function(linea,consecutivo,tipo,accion){
        var UserMERX = $('#UsReg').val();
        
            $.ajax({
                url: '/api/TX_COLAS_TB/?linea_p=' + linea + '&consecutivo_p='+ consecutivo + '&tipo_p='+ tipo + '&user_p='+ UserMERX + '&accion_p='+ accion,
                type: 'PUT',
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Terminar Colas Carrusel",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{

                        swal({
                        title: "Colas Carrusel",
                        text: "Se terminó correctamente la línea.",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            if(tipo=="SC"){
                               $("#in_flr_que_ien_entradaSC").val("");
                                colacarrusel.QUE_CargaInicialSC();
                                $('#th_mercSC').hide();
                            }else{
                               $("#in_flr_que_ien_entradaAC").val("");
                                colacarrusel.QUE_CargaInicialAC();
                                $('#th_mercAC').hide();
                            }
                            
                        });
                    }  
                },
            })               
    },

    QUE_limpiar_SC: function () {
         $("#in_flr_que_ien_entradaSC").val("");
         $("#ColaAforoSC_table1body").html("");
         $("#MERC_SC_table1body").html("");
         $('#th_mercSC').hide();

         colacarrusel.QUE_CargaInicialSC(); 
    },

    QUE_limpiar_AC: function () {
         $("#in_flr_que_ien_entradaAC").val("");
         $("#ColaAforoAC_table1body").html("");
         $("#MERC_AC_table1body").html("");
         $('#th_mercAC').hide();

         colacarrusel.QUE_CargaInicialAC();        
    }


};

$(document).ready(function () {
    colacarrusel.init();
})