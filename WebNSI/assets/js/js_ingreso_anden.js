﻿var ingreso_anden = {

    init: function () {
        
         ingreso_anden.CIA_Listar();
         ingreso_anden.CIA_Registros();

    },

    CIA_Listar: function(){
        var p_tiquete = null;
        var p_placa = "";
        //Obtenemos la plantilla
        $('#th_Anden').show();
        $("#th_Anden").attr('style', 'display:block');
        var templateText = $("#Anden_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#Anden_table1body").html('');
        $.getJSON('/api/TX_INGRESO_ANDEN_TB/GetAnden?cia_tiquete_p='+ p_tiquete + '&cia_placa_p=' + p_placa).then(function (data) {
            $.each(data, function (index, elem) {
                $("#Anden_table1body").append(tableTemplate(elem));
            })
        });
    },

    CIA_Registros:function(callback){        
        $.getJSON('/api/TX_INGRESO_ANDEN_TB/GetRegistros').then(function (data) {
            if(data != ""){
               $('#in_ut').val(data.UT_ESPERA); 
                principal.activeLabels();
            }
            
        });
    },

};

$(document).ready(function () {
    ingreso_anden.init();
})