﻿var consultacolas = {
  pickedup : null,
    init: function () {
      consultacolas.QUE_limpiar();
        $('#rack-tab').on('click', function () { 
            $('#in_flr_que_ien_entradaAR').val('');
             $("#MERC_AR_table1body").html("");
              if($("#ColaAR_table1body").html() == ""){
               consultacolas.QUE_CargaInicialAR(); 
              }                       
        }); 

        $('#carrusel-tab').on('click', function () { 
             $('#in_flr_que_ien_entradaAC').val('');
             $("#MERC_AC_table1body").html("");
              if($("#ColaAC_table1body").html() == ""){
               consultacolas.QUE_CargaInicialAC(); 
              }
        });  

        $('#salidacarrusel-tab').on('click', function () { 
            $('#in_flr_que_ien_entradaSC').val('');
              $("#MERC_SC_table1body").html("");
              if($("#ColaSC_table1body").html() == ""){
               consultacolas.QUE_CargaInicialSC();
              }              
        });  
        $('#salidarack-tab').on('click', function () { 
            $('#in_flr_que_ien_entradaSR').val('');
              $("#MERC_SR_table1body").html("");
              if($("#ColaSR_table1body").html() == ""){
                consultacolas.QUE_CargaInicialSR();
              }             
        });  

        $('#in_flr_que_ien_entradaAC').bind("cut copy paste",function(e) {
             e.preventDefault();
         });
        $('#in_flr_que_ien_entradaAR').bind("cut copy paste",function(e) {
             e.preventDefault();
         }); 
        $('#in_flr_que_ien_entradaSC').bind("cut copy paste",function(e) {
             e.preventDefault();
         });
        $('#in_flr_que_ien_entradaSR').bind("cut copy paste",function(e) {
             e.preventDefault();
         }); 


    },
  
  
//Carga inicial de consultas
    QUE_CargaInicialAR:function(){      
        var cb = function(){
            consultacolas.QUE_CheckearColas();
        };
        consultacolas.QUE_Listar("AR",cb);
    },

    QUE_CargaInicialAC:function(){      
        var cb = function(){
            consultacolas.QUE_CheckearColas();
        };
        consultacolas.QUE_Listar("AC",cb);
    },

    QUE_CargaInicialSR:function(){      
        var cb = function(){
            consultacolas.QUE_CheckearColas();
        };
        consultacolas.QUE_Listar("SR",cb);
    },

    QUE_CargaInicialSC:function(){      
        var cb = function(){
            consultacolas.QUE_CheckearColas();
        };
        consultacolas.QUE_Listar("SC",cb);
    },

// Accion al hacer click en la línea
    QUE_Funcionalidad_Click: function(tipo,consecutivo, linea, activeTab){  
      
      //Limpia tabla de mercancias y la oculta
       $("#MERC_"+activeTab+"_table1body").html("");
       $('#th_merc'+ activeTab).hide();

      //regresa al color negro antes de haberlo seleccionado
         if (consultacolas.pickedup != null) {
             consultacolas.pickedup.css( "background-color", "#3C3C3C" );
         }
         //cambia el color de la fila a gris
         $( linea ).css( "background-color", "grey" );
 
        consultacolas.pickedup = $( linea );

          var cb = function(){
              consultacolas.QUE_CheckearTarimas(activeTab);
          };

         consultacolas.QUE_Listar_Mercancias(consecutivo,activeTab,cb);
    },
  
//Métodos que van al api a listar 
    QUE_Listar: function(activeTab,callback){ 
      var p_consecutivo = $("#in_flr_que_ien_entrada" +activeTab).val();
      var p_tipoCola = activeTab;
      if (p_consecutivo == ""){
        p_consecutivo = null;
      }
      
        $.getJSON('/api/TX_COLAS_TB/GetColasConsulta?consecutivo='+ p_consecutivo + '&tipoCola=' + p_tipoCola).then(function (data) {
            if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Consulta Colas",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                }else{
                    if(data.length == 0 && p_consecutivo != null){
                      var titlemsj = "Consulta Colas";  
                        switch(activeTab) {
                          case "AC":
                            titlemsj = "Colas Aforo Carrusel";
                            break;
                          case "AR":
                            titlemsj = "Colas Aforo Rack";
                            break;
                          case "SR":
                            titlemsj = "Colas Salida Rack";
                            break;
                          case "SC":
                            titlemsj = "Colas Salida Carrusel";
                            break;                      
                          default:
                            titlemsj = "Consulta Colas";
                        }

                        swal({
                              title: titlemsj,
                              text: "No existe información para el consecutivo dado.",
                              icon: "error",
                              button: "OK!",
                            })
                    }else{
                        //Obtenemos la plantilla
                        $('#th_ColaAforo'+activeTab).show();
                        $("#th_ColaAforo"+activeTab).attr('style', 'display:block');
                        var templateText = $("#QUE_"+activeTab+"_table-template").html();
                        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                        var tableTemplate = Handlebars.compile(templateText);
                        $("#Cola"+activeTab+"_table1body").html('');
                        $.each(data, function (index, elem) {
                        $("#Cola"+activeTab+"_table1body").append(tableTemplate(elem));
                        })
                        $('#th_merc'+activeTab).hide();

                        $("#Cola"+activeTab+"_Table > tbody > tr").on("click", function(e) {
                          //if($("#aforocarrusel-tab").hasClass("active")){
                            var consecutivo = $(this).find("td").eq(1).html();
                            consultacolas.QUE_Funcionalidad_Click("Cola"+activeTab, consecutivo, $( this ),activeTab);
                          //}
                        });  

                        if(typeof(callback) == "function"){
                            callback();
                        }
                    }  
                }

                         
        });            
    },

//Funcionalidad de checkear colas luego de la consulta
    QUE_CheckearColas: function(){
        $('#ColaAR_Table tbody tr').each(function(){
            var accion1 = $(this).find('td span input[id^="rdPrev"]');
            consultacolas.QUE_RevisaCheck(accion1);           
        });

        $('#ColaAC_Table tbody tr').each(function(){
            var accion1 = $(this).find('td span input[id^="rdPrev"]');
            consultacolas.QUE_RevisaCheck(accion1);           
        });

        $('#ColaSC_Table tbody tr').each(function(){
            var accion1 = $(this).find('td span input[id^="rdPrev"]');
            consultacolas.QUE_RevisaCheck(accion1);           
        });
        $('#ColaSR_Table tbody tr').each(function(){
            var accion1 = $(this).find('td span input[id^="rdPrev"]');
            consultacolas.QUE_RevisaCheck(accion1); 
        });
    },

    QUE_RevisaCheck:function(accion){
        var checkbox = accion.attr('id');

            //console.log(checkbox, $('#' + checkbox).val());
             if(!accion.prop('checked') && $('#' + checkbox).val() == "S"){
                
                 $('#' + checkbox).prop('checked', true);
             }

             if(accion.prop('checked') && $('#' + checkbox).val() == "N"){
                
                 $('#' + checkbox).prop('checked', false);
             }
    },

//Método que va al api a listar las mercancias 
    QUE_Listar_Mercancias: function(consecutivo_p, activeTab, callback){
       //Obtenemos la plantilla
            $('#th_merc'+activeTab).show();
            $("#th_merc"+activeTab).attr('style', 'display:block');
            var templateText = $("#MERC_"+activeTab+"_table-template").html();
            //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#MERC_"+activeTab+"_table1body").html('');
            $.getJSON('/api/TX_COLAS_TB/GetMercCola?consecutivo='+ consecutivo_p).then(function (data) {
                $.each(data, function (index, elem) {
                    $("#MERC_"+activeTab+"_table1body").append(tableTemplate(elem));
                })
                if(typeof(callback) == "function"){
                    callback();
                }
            });
    },

//Funcionalidad de checkear las mercancías
    QUE_CheckearTarimas: function(activeTab){
        $('#MERC_'+activeTab+'_Table tbody tr').each(function(){
            var accion = $(this).find('td span input[type="checkbox"]');
            var checkbox = accion.attr('id');

            if(!accion.prop('checked') && $('#' + checkbox).val() == "S"){
                
                $('#' + checkbox).prop('checked', true);
            }

            if(accion.prop('checked') && $('#' + checkbox).val() == "N"){
                
                $('#' + checkbox).prop('checked', false);
            }
        });
    },

//Métodos para limpiar 
    QUE_limpiar: function () {
        $("#in_flr_que_ien_entradaSC").val("");
         $("#ColaSC_table1body").html("");
         $("#MERC_SC_table1body").html("");
         $('#th_mercSC').hide();

         $("#in_flr_que_ien_entradaSR").val("");
         $("#ColaSR_table1body").html("");
         $("#MERC_SR_table1body").html("");
         $('#th_mercSR').hide();

         $("#in_flr_que_ien_entradaAC").val("");
         $("#ColaAC_table1body").html("");
         $("#MERC_AC_table1body").html("");
         $('#th_mercAC').hide();

         $("#in_flr_que_ien_entradaAR").val("");
         $("#ColaAR_table1body").html("");
         $("#MERC_AR_table1body").html("");
         $('#th_mercAR').hide();

          consultacolas.QUE_CargaInicialAR(); 
          consultacolas.QUE_CargaInicialAC();
          consultacolas.QUE_CargaInicialSR();
          consultacolas.QUE_CargaInicialSC();
    }
};

$(document).ready(function () {
    consultacolas.init();
})