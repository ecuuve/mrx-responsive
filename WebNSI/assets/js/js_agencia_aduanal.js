var agenciaadu = {

    init: function () {
        TecladoFuncionalidadPersonalizada();
        agenciaadu.LimpiarCamposAgencia();
        agenciaadu.LimpiarCamposAgente();
        agenciaadu.AGA_CargarControlesPrevios();
        agenciaadu.TAA_CargarControlesPrevios();

          $('#th_agencias').hide();
          $('#div_filters').hide();

        $("#btn_listar_agencia").attr('style', 'display:block');
        $("#btn_actualizar_agencia").attr('style', 'display:none');
        $("#btn_registrar_agencia").attr('style', 'display:block');
        $("#btn_limpiar_agencia").attr('style', 'display:block');
        $("#btn_regresar_agencia").attr('style', 'display:none');
        
        $("#btn_listar_agente").attr('style', 'display:none');
        $("#btn_nuevo_agente").attr('style', 'display:none');
        $("#btn_actualizar_agente").attr('style', 'display:none');
        $("#btn_registrar_agente").attr('style', 'display:none');
        $("#btn_limpiar_agente").attr('style', 'display:none');
        $("#btn_regresar_agente").attr('style', 'display:none');

        // Cuando hace click en el tab de agencia
        $('#agencia-tab').on('click', function () {                  
          if($('#botones_agencia').hasClass('fullHidden'))
              $('#botones_agencia').removeClass('fullHidden');
              $('#botones_agente').addClass('fullHidden');
               principal.activeLabels();
          
        });

        // Cuando hace click en el tab de agentes
        $('#agentes-tab').on('click', function () { 
          lbl_agenom.innerText = "Agentes - " + $("#AGA_NOMBRE").val();
              //$('#lbl_agenom2').val("hola");
            if($('#botones_agente').hasClass('fullHidden'))
              $('#botones_agente').removeClass('fullHidden');
              $('#botones_agencia').addClass('fullHidden');
               //principal.deactivateLabels();               
        });

        // $('#in_flr_cedula').change(function () {
        //     var cod_p = $('#in_flr_cedula').val();
        //     var nom_p = $('#in_flr_nombre').val();
        //     agenciaadu.BusquedaFiltrosAgentes();
        //   });  
        //  $('#in_flr_nombre').change(function () {
        //     var cod_p = $('#in_flr_cedula').val();
        //     var nom_p = $('#in_flr_nombre').val();
            
        //     agenciaadu.BusquedaFiltrosAgentes();
        // }); 


        $('#AGA_NAGENCIA_ADUANAL').change(function () {
          var cod_p = $('#AGA_NAGENCIA_ADUANAL').val();
          agenciaadu.AGA_ConsultarDatosDetAgencia(cod_p);
        });
    },

    onfocus: function () {
        if ($("#lbl_AGA_NAGENCIA_ADUANAL").hasClass('lblfocus'))
            $("#lbl_AGA_NAGENCIA_ADUANAL").removeClass('lblfocus');

          if ($("#lbl_cedula").hasClass('lblfocus'))
            $("#lbl_cedula").removeClass('lblfocus');
    },

    CargaFecha: function(){
       $("#AGA_FECHA_INGRESO").kendoDatePicker({
          format: "dd/MM/yyyy",
          value: new Date(),
          dateInput: true,
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });

         $("#AGA_FECHA_INGRESO").data("kendoDatePicker").enable(false);        
    },

     CargaFechaAgente: function(){
       $("#TAA_FCH_ACTUALIZA").kendoDatePicker({
          format: "dd/MM/yyyy",
          value: new Date(),
          dateInput: true,
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });

         $("#TAA_FCH_ACTUALIZA").data("kendoDatePicker").enable(false);
     },
   
    //Carga las listas de valores en los select
    AGA_CargarControlesPrevios: function () {
        agenciaadu.getEstados();
        agenciaadu.getRecibeEstados();
        $("#btn_actualizar_agencia").attr('style', 'display:none');
        $("#btn_registrar_agencia").attr('style', 'display:block');        
    },

    //Carga las listas de valores en los select
    TAA_CargarControlesPrevios: function () {
        agenciaadu.getEstadosAgente();
        agenciaadu.getMotivoEstados();
        agenciaadu.getTipoAgente();
        $("#btn_actualizar_agente").attr('style', 'display:none');
        $("#btn_nuevo_agente").attr('style', 'display:none');        
    },

    //Carga pantalla para el ingreso de nuevo agente
    TAA_Nuevo: function(){
      agenciaadu.LimpiarCamposAgente();
        $("#btn_listar_agente").attr('style', 'display:block');
        $("#btn_nuevo_agente").attr('style', 'display:none');
        $("#btn_actualizar_agente").attr('style', 'display:none');
        $("#btn_registrar_agente").attr('style', 'display:block');
        $("#btn_limpiar_agente").attr('style', 'display:block');
       // $("#btn_regresar_agente").attr('style', 'display:block');

        $('#th_agentes').hide();
        $('#div_filters_agentes').hide();
        $('#detalles-agente').show();
        $('#TAA_CEDULA').focus();
    },

    //Obtiene la lista de valores para el select
    getEstados: function (valor) {    
        principal.getData(
            "/api/TX_AGENCIA_ADUANAL_TB/GetEstados",
            function (data) {
                $("#s_Estado").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        );
    },

    //Obtiene la lista de valores para el select
    getRecibeEstados: function (valor) {
        principal.getData(
            "/api/TX_AGENCIA_ADUANAL_TB/GetRecibeEstados",
            function (data) {
                $("#s_Recibe_Estado").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        );
    },

    //Obtiene la lista de valores para el select
    getEstadosAgente: function (valor) {    
        principal.getData(
            "/api/TX_AGENTES_ADUANA_TB/GetEstados",
            function (data) {
                $("#s_EstadoAgente").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        );
    },

    //Obtiene la lista de valores para el select
    getMotivoEstados: function (valor) {    
        principal.getData(
            "/api/TX_AGENTES_ADUANA_TB/GetMotivoEstados",
            function (data) {
                $("#s_MotivoEstado").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        );
    },

    //Obtiene la lista de valores para el select
    getTipoAgente: function (valor) {    
        principal.getData(
            "/api/TX_AGENTES_ADUANA_TB/GetTipoAgente",
            function (data) {
                $("#s_tipoAgente").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        );
    },


    // Limpia campos de los formularios
    LimpiarCamposAgencia: function () {
        principal.resetAllFields('detInfo_agencia');
        //principal.resetAllFields('detInfo_agente');      
        TecladoFuncionalidadPersonalizada();
        principal.deactivateLabels();
        agenciaadu.AGA_CargarControlesPrevios();
        $("#AGA_NAGENCIA_ADUANAL").attr('disabled', false);
        $("#AGA_NAGENCIA_ADUANAL").focus();    
        agenciaadu.CargaFecha(); 

        if(!$('#li_agente').hasClass('fullHidden'))
            $('#li_agente').addClass('fullHidden');      
    },

    // Limpia campos de los formularios
    LimpiarCamposAgente: function () {
        principal.resetAllFields('detInfo_agente');      
        TecladoFuncionalidadPersonalizada();
        principal.deactivateLabels();
        agenciaadu.TAA_CargarControlesPrevios();
        $('#TAA_CEDULA').focus();  
        agenciaadu.CargaFechaAgente();       
    },

    
    //Funcion Ajax para listar las agencias
    AGA_ListarAgencias: function () {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_AGENCIA_ADUANAL_TB/GetAgenciaAduanal',
            success: function (data) {
              //Obtenemos la plantilla
              var templateText = $("#tb-agencias-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
              var tableTemplate = Handlebars.compile(templateText);
              $("#tb_agencias_body").html('');
              //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                  $.each(data, function (index, elem) {
                      $("#tb_agencias_body").append(tableTemplate(elem));
                  })

            }
        });

        $("#btn_listar_agencia").attr('style', 'display:none');
        $("#btn_actualizar_agencia").attr('style', 'display:none');
        $("#btn_registrar_agencia").attr('style', 'display:none');
        $("#btn_limpiar_agencia").attr('style', 'display:none');
        $("#btn_regresar_agencia").attr('style', 'display:block');

        $("#detalles-agencia").attr('style', 'display:none');
        $('#th_agencias').show();
        $('#div_filters').show();

         $('#in_flr_id_agencia').val("");
         $('#in_flr_agencia').val("");

         if(!$('#li_agente').hasClass('fullHidden'))
            $('#li_agente').addClass('fullHidden');
    },

    //Funcion Ajax para listar los agentes de la agencia consultada
    TAA_ListarAgentes: function(){
      var p_agencia = $("#AGA_NAGENCIA_ADUANAL").val();
      $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_AGENTES_ADUANA_TB/GetAgentesAduana?taa_aga_nagencia_p='+p_agencia,
            success: function (data) {
              //Obtenemos la plantilla
              var templateText = $("#tb-agentes-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
              var tableTemplate = Handlebars.compile(templateText);
              $("#tb_agentes_body").html('');
              //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                  $.each(data, function (index, elem) {
                      $("#tb_agentes_body").append(tableTemplate(elem));
                  })

            }
        });

        $("#btn_listar_agente").attr('style', 'display:none');
        $("#btn_nuevo_agente").attr('style', 'display:block');
        $("#btn_actualizar_agente").attr('style', 'display:none');
        $("#btn_registrar_agente").attr('style', 'display:none');
        $("#btn_limpiar_agente").attr('style', 'display:none');
        $("#btn_regresar_agente").attr('style', 'display:none');

        $("#detalles-agente").attr('style', 'display:none');
        $('#th_agentes').show();
        $('#div_filters_agentes').show();

         $('#in_flr_cedula').val("");
         $('#in_flr_nombre').val("");
    },


    //Ejecuta los filtros que se hayan seleccionado en la pantalla de agencia
    BusquedaFiltros: function () {
       var cod_p = $('#in_flr_id_agencia').val();
        var nom_p = $('#in_flr_agencia').val();
        if(cod_p== "" && nom_p == ""){
         agenciaadu.AGA_ListarAgencias();
        }else{
          principal.filterTable_Input('in_flr_id_agencia', 'tb_agencias', 0);
          principal.filterTable_Input('in_flr_agencia', 'tb_agencias', 1);
        }    
    },

    //Ejecuta los filtros que se hayan seleccionado en la pantalla de agente
    BusquedaFiltrosAgentes: function () {
        var cod_p = $('#in_flr_cedula').val();
        var nom_p = $('#in_flr_nombre').val();
        if(cod_p== "" && nom_p == ""){
         agenciaadu.TAA_ListarAgentes();
        }else{
          principal.filterTable_Input('in_flr_cedula', 'tb_agentes', 2);
          principal.filterTable_Input('in_flr_nombre', 'tb_agentes', 3);
        }        
    },

    //Regresa a la pantalla principal del formulario agencia
    Regresar: function () { 
     var agen = $("#AGA_NAGENCIA_ADUANAL").val();
     if(agen == ""){
       agenciaadu.AGA_CargarControlesPrevios();
       agenciaadu.CargaFecha();

        $("#btn_listar_agencia").attr('style', 'display:block');
        $("#btn_actualizar_agencia").attr('style', 'display:none');
        $("#btn_registrar_agencia").attr('style', 'display:block');
        $("#btn_limpiar_agencia").attr('style', 'display:block');
        $("#btn_regresar_agencia").attr('style', 'display:none');

        $("#AGA_NAGENCIA_ADUANAL").focus(); 

     }else{
       if($('#li_agente').hasClass('fullHidden'))
        $('#li_agente').removeClass('fullHidden');

        $("#btn_listar_agencia").attr('style', 'display:block');
        $("#btn_actualizar_agencia").attr('style', 'display:block');
        $("#btn_registrar_agencia").attr('style', 'display:none');
        $("#btn_limpiar_agencia").attr('style', 'display:block');
        $("#btn_regresar_agencia").attr('style', 'display:none');
     }

        $('#th_agencias').hide();
        $('#div_filters').hide();    
        $('#detalles-agencia').show();       
    },

    //Regresa a la pantalla principal lista de agentes
    RegresarAgente: function () {       
        $('#th_agentes').show();
        $('#div_filters_agentes').show();
        //agenciaadu.AGA_CargarControlesPrevios();
        $('#detalles-agentes').hide();
        //agenciaadu.CargaFecha();

        $("#btn_listar_agencia").attr('style', 'display:none');
         $("#btn_nuevo_agente").attr('style', 'display:block');
        $("#btn_actualizar_agencia").attr('style', 'display:none');
        $("#btn_registrar_agencia").attr('style', 'display:none');
        $("#btn_limpiar_agencia").attr('style', 'display:none');
        $("#btn_regresar_agencia").attr('style', 'display:none');
    },

    //Carga la información de la agencia selccionada
    Editar: function(p_id){
      agenciaadu.AGA_ConsultarDatosDetAgencia(p_id);
      $('#th_agencias').hide();
      $('#div_filters').hide();
      $('#detalles-agencia').show();
    },

    //Carga la información del agente selccionado
    EditarAgente: function(p_agencia, p_codigo){
      agenciaadu.TAA_ConsultarDatosDetAgente(p_agencia, p_codigo);
      $('#th_agentes').hide();
      $('#div_filters_agentes').hide();
      $('#detalles-agente').show();
    },

     //Consulta el detalle de la agencia seleccionado o digitado al insertar
    AGA_ConsultarDatosDetAgencia:function(p_id){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_AGENCIA_ADUANAL_TB/GetAgenciaAduanalDetalle?aga_nagencia_aduanal_p='+p_id,
            //data: postdata,
            success: function (data) {
                if (data !== null) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Agencia Aduanal",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }
                    else{
                      if(data.length != 0){
                         principal.activeLabels();
                         agenciaadu.AGA_MuestraDatos(data); 
                          $("#AGA_NAGENCIA_ADUANAL").attr('disabled', true); 
                          if($('#li_agente').hasClass('fullHidden'))
                              $('#li_agente').removeClass('fullHidden');
                          agenciaadu.TAA_ListarAgentes();  

                      }
                       else{

                        swal({
                          title: "Agencia Aduanal",
                          text: "No existen datos para el valor consultado.",
                          icon: "error",
                          button: "OK!",
                        })

                       }
                    }                 
                }
            },           
            error: function (xhr, textStatus, errorThrown) {
              console.log(textStatus);
            }
        });
    },

    //Consulta el detalle de la agencia seleccionado o digitado al insertar
    TAA_ConsultarDatosDetAgente:function(p_agencia, p_codigo){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_AGENTES_ADUANA_TB/GetAgentesAduanalDetalle?taa_aga_nagencia_p='+p_agencia+ '&taa_codigo_p='+p_codigo,
            success: function (data) {
                if (data !== null) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Agentes",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }
                    else{
                      if(data.length != 0){
                         principal.activeLabels();
                         agenciaadu.TAA_MuestraDatos(data); 
                          $("#TAA_CODIGO").attr('disabled', true); 
                          $("#TAA_USU_ACTUALIZA").attr('disabled', true); 
                          $("#TAA_FCH_ACTUALIZA").attr('disabled', true); 
                      }
                       else{

                        swal({
                          title: "Agentes",
                          text: "No existen datos para el valor consultado.",
                          icon: "error",
                          button: "OK!",
                        })

                       }
                    }                 
                }
            },           
            error: function (xhr, textStatus, errorThrown) {
              console.log(textStatus);
            }
        });
    },

    //Muestra los datos del bloqueo consultado o seleccionado en pantalla
    AGA_MuestraDatos: function (p_agencia) {
      for (var campo in p_agencia[0]) {    
          if(p_agencia[0][campo] != null){
              principal.setValueByName(campo, p_agencia[0][campo],'frm_agencia');
            }

          }
        $("#btn_listar_agencia").attr('style', 'display:block');
        $("#btn_actualizar_agencia").attr('style', 'display:block');
        $("#btn_registrar_agencia").attr('style', 'display:none');
        $("#btn_limpiar_agencia").attr('style', 'display:block');
        $("#btn_regresar_agencia").attr('style', 'display:none');              
    },   

    //Muestra los datos del bloqueo consultado o seleccionado en pantalla
    TAA_MuestraDatos: function (p_agente) {
      for (var campo in p_agente[0]) {    
          if(p_agente[0][campo] != null){
              principal.setValueByName(campo, p_agente[0][campo],'frm_agente');
            }

          }
        $("#btn_listar_agente").attr('style', 'display:block');
        $("#btn_nuevo_agente").attr('style', 'display:block');
        $("#btn_actualizar_agente").attr('style', 'display:block');
        $("#btn_registrar_agente").attr('style', 'display:none');
        $("#btn_limpiar_agente").attr('style', 'display:none');
        $("#btn_regresar_agente").attr('style', 'display:none');              
    },               
    
    //Funcionalidad botón guardar Agencia
    AGA_GuardarAgencia: function(){ 
     $("#USER_MERX").val($("#UsReg").val());
     var nombre = $("#AGA_NOMBRE").val();
     if(nombre != ""){
      $.post(
      "/api/TX_AGENCIA_ADUANAL_TB/",
       $('#frm_agencia').serialize(),
      function (data) {
          if (typeof data === 'string' || data instanceof String){
              swal({
                title: "Registro de Agencia",
                text: data,
                icon: "error",
                button: "OK!",
              })
          }else{
              swal({
              title: "Registro de Agencia",
              text: "Agencia guardada exitosamente.",
              icon: "success",
              button: "OK!",

              }).then((value) => {
                   agenciaadu.LimpiarCamposAgencia();
              });
          }
      });
     } else{
        swal({
                title: "Registro de Agencia",
                text: "Digite la información de la Agencia",
                icon: "warning",
                button: "OK!",
              })
     }       
      
    },

    //Funcionalidad botón guardar Agencia
    TAA_GuardarAgente: function(){ 
     $("#USER_MERXagente").val($("#UsReg").val());
     var cedula = $("#TAA_CEDULA").val();
     $("#TAA_Agencia").val($("#AGA_NAGENCIA_ADUANAL").val());
     if(cedula != ""){
      $.post(
      "/api/TX_AGENTES_ADUANA_TB/",
       $('#frm_agente').serialize(),
      function (data) {
          if (typeof data === 'string' || data instanceof String){
              swal({
                title: "Registro de Agente",
                text: data,
                icon: "error",
                button: "OK!",
              })
          }else{
              swal({
              title: "Registro de Agente",
              text: "Agente guardado exitosamente.",
              icon: "success",
              button: "OK!",

              }).then((value) => {
                   agenciaadu.TAA_Nuevo();
              });
          }
      });
     } else{
        swal({
                title: "Registro de Agente",
                text: "Digite la información del Agente",
                icon: "warning",
                button: "OK!",
              })
     }             
    },

    //Funcionalidad botón actualizar Agencia
    AGA_ActualizarAgencia: function(){
        $("#AGA_NAGENCIA_ADUANAL").attr('disabled', false);
        $("#USER_MERX").val($("#UsReg").val());
        var id = $('#AGA_NAGENCIA_ADUANAL').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_agencia').serializeArray());
            $("#AGA_NAGENCIA_ADUANAL").attr('disabled', true);
            $.ajax({
                url: '/api/TX_AGENCIA_ADUANAL_TB/' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Actualización de Agencia",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{
                        swal({
                        title: "Agencia Aduanal",
                        text: "Se actualizó correctamente la información",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                           agenciaadu.LimpiarCamposAgencia();
                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Agencia Aduanal",
                text: "El ID de la agencia no puede ser vacío",
                icon: "error",
                button: "OK!",

            }).then((value) => {
              // $("#BLO_IEN_ENTRADA").attr('disabled', true);
              // $("#BLO_ID_BLOQUEO").attr('disabled', true);
              // $("#BLO_MOTIVO_DESBLOQUEO").attr('disabled', true);           
            });
        }
    },

    //Funcionalidad botón actualizar Agencia
    TAA_ActualizarAgente: function(){
        $("#TAA_CODIGO").attr('disabled', false);
        $("#USER_MERXagente").val($("#UsReg").val());
        $("#TAA_Agencia").val($("#AGA_NAGENCIA_ADUANAL").val());
        
        var id = $('#TAA_CODIGO').val();

        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_agente').serializeArray());
              $("#TAA_CODIGO").attr('disabled', true);
            $.ajax({
                url: '/api/TX_AGENTES_ADUANA_TB/' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Actualización de Agente",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{
                        swal({
                        title: "Agencia Aduanal - Agentes",
                        text: "Se actualizó correctamente la información",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                          agenciaadu.TAA_Nuevo();
                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Agencia Aduanal",
                text: "El ID del agente no puede ser vacío",
                icon: "error",
                button: "OK!",

            }).then((value) => {
              // $("#BLO_IEN_ENTRADA").attr('disabled', true);
              // $("#BLO_ID_BLOQUEO").attr('disabled', true);
              // $("#BLO_MOTIVO_DESBLOQUEO").attr('disabled', true);           
            });
        }
    }

};

$(document).ready(function () {
    agenciaadu.init();
})