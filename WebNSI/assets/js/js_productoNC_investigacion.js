var productoNC_investigacion = {
	init: function(){
        TecladoFuncionalidadPersonalizada();
		productoNC_investigacion.cargaInicial();
		//Al dar click a la cejilla carga el combo box de estados, extrae el valor del identificador si ya existiese y ademas si existiese trae y carga la informacion relevante.
		$('#li_investigacionpnc').on('click', function(){
			productoNC_investigacion.getEstado('I');
			$('#frm_producto_investigacion #PNC_IDENTIFICADOR').val($('#frm_productoNC #pnc_identificador').val());
			if($('#frm_productoNC #pnc_identificador').val() != "")
				productoNC_investigacion.editar($('#frm_productoNC #pnc_identificador').val());
      // $('#btn_limpiar_producto').hide();
		});
	},
	//Lista los estdos
	getEstado: function (pantalla) {
        principal.getData("/api/TX_PROD_NC_TB/GetEstado?pantalla=" + pantalla,
        	function (data) {
        		$("#frm_producto_investigacion #pnc_estado").html(principal.arrayToOptions(data));});
    },
    // Inicializa los componentes de fechas
    cargaInicial: function(){
    	if($('#investigacion-tab').hasClass('active'))
  			productoNC_investigacion.getEstado('I');
  		else
  			productoNC_investigacion.getEstado('E');
      	$("#PNC_FCH_LIMITE").kendoDatePicker({
            format: "dd/MM/yyyy",
            value: new Date(),
            dateInput: true,
            month: {
              empty: '<span class="k-state-disabled">#=data.value#</span>'
            }
          });
          $("#PNC_FCH_INVESTIGA").kendoDatePicker({
            format: "dd/MM/yyyy",
            value: new Date(),
            dateInput: true,
            month: {
              empty: '<span class="k-state-disabled">#=data.value#</span>'
            }
          });
          $("#PNC_FCH_REVISION").kendoDatePicker({
            format: "dd/MM/yyyy",
            value: new Date(),
            dateInput: true,
            month: {
              empty: '<span class="k-state-disabled">#=data.value#</span>'
            }
          });
          $("#PNC_FCH_APRUEBA").kendoDatePicker({
            format: "dd/MM/yyyy",
            value: new Date(),
            dateInput: true,
            month: {
              empty: '<span class="k-state-disabled">#=data.value#</span>'
            }
          });
          $("#PNC_FCH_CALIDAD").kendoDatePicker({
            format: "dd/MM/yyyy",
            value: new Date(),
            dateInput: true,
            month: {
              empty: '<span class="k-state-disabled">#=data.value#</span>'
            }
          });
    },
    //Actualiza la información referente a la investigación 
    Actualizar: function () {
        var id = $('#frm_producto_investigacion #PNC_IDENTIFICADOR').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_producto_investigacion').serializeArray());
            $.ajax({
                url: '/api/TX_PROD_NC_TB/ActualizaInvestigación?id=' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                   if(data != null){
	                   swal({
	                        title: "Investigación Producto No Conforme",
	                        text: "Se actualizó correctamente la Investigación del Producto No Conforme",
	                        icon: "success",
	                        button: "OK!",
	                    });
	                   // principal.resetAllFields('frm_producto_investigacion');
	               }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                    else{
	                	swal({
	                      title: "Investigación Producto No Conforme",
	                      text: data.responseJSON.Message,
	                      icon: "error",
	                      button: "OK!",
	                    });
	                }
                }
            });
        }
        else {
            swal({
                title: "Investigación Producto No Conforme",
                text: "No existe un producto asociado",
                icon: "error",
                button: "OK!",

            });
        }
    },
    //tra de base de datos la informacion relevante a esa investigacion, y lo coloca en los campos del form correspondiente
    editar: function(identificador){
    	principal.resetAllFields('frm_producto_investigacion');
    	var sistema = '';
    	var consecutivo = '';
		$.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_PROD_NC_TB/Productos?identificador='+identificador+ '&sistema='+sistema+ '&consecutivo='+consecutivo,
            success: function (data) {
            	$.each(data[0], function(i,v){
            		principal.setValueByName(i, v,'frm_producto_investigacion');
            	});
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Investigación Producto No Conforme",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
                console.log("error");
            }
        });
	}
};
$(document).ready(function () {
    productoNC_investigacion.init();
});