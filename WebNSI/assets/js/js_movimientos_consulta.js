var movimientos_consulta = {
    mov_p : null,
    bl_p: null,
    dua_p: null,
    consig_p : null,
    viaje_p: null,
    contenedor_p: null,


    init: function(){    
        $('form *').prop('disabled', true);   
        principal.resetAllFields('frm_movimientos');
        TecladoFuncionalidadPersonalizada();
        $('#MVE_MOV_INVENTARIO_flt').focus();
       
        movimientos_consulta.BuscarMovEnter(); 
    },

    onfocus: function () {
        if ($("#lbl_MVE_MOV_INVENTARIO_flt").hasClass('lblfocus'))
            $("#lbl_MVE_MOV_INVENTARIO_flt").removeClass('lblfocus');
    },


    BuscarMovEnter: function () {
        $("#MVE_MOV_INVENTARIO_flt,#MVE_IEN_ENTRADA_flt,#IEN_DUA_flt,#IEN_CONSIGNATARIO_flt,#IEN_NUMERO_VIAJE_flt,#TMA_NUM_CONTENEDOR_flt").change(function(){
            var mov_p = $("#MVE_MOV_INVENTARIO_flt").val();
            var bl_p = $("#MVE_IEN_ENTRADA_flt").val();
            var dua_p = $("#IEN_DUA_flt").val();
            var consig_p = $("#IEN_CONSIGNATARIO_flt").val();
            var viaje_p = $("#IEN_NUMERO_VIAJE_flt").val();
            var contenedor_p = $("#TMA_NUM_CONTENEDOR_flt").val();
          

            movimientos_consulta.buscarMovimientos(mov_p,bl_p,dua_p,consig_p,viaje_p,contenedor_p);

            // swal({
            //     title: "Movimientos",
            //     text: 'Yummy Yummy',
            //     icon: "warning",
            //     button: "OK!",
            // });
        });



        // $('#in_mta_tarima').change(function () {
        //     tarima.buscarTarima(null,$('#in_mta_tarima').val());
        //     tarima.cambiarEstadoBotones(false, false, true, false, false, true, false);
        // });
    },


    //Busca los movimientos según el filtro sean 1 o n 
    buscarMovimientos: function (mov_p,bl_p,dua_p,consig_p,viaje_p,contenedor_p) {
        //var entrada = $('#IEN_ENTRADA').val();
        //if(entrada_p != undefined || entrada_p != ""){ 
        //var mercancia = null;
        var url = '/api/TX_MOVS_ENTRADA_TB/GetMovimientos?mov=' + mov_p + '&BL=' + bl_p + '&dua=' + dua_p + '&consig=' + consig_p + '&viaje=' + viaje_p + '&contenedor=' + contenedor_p;
        var cb = function(data){
            var templateText = $("#movimientos_table-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#movimientos_tablebody").html('');
              //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
            $.each(data, function (index, elem) {
                $("#movimientos_tablebody").append(tableTemplate(elem));
            });
        };
        principal.ajax('GET',url,cb,"Movimientos");
      //}
    },

    //Carga la info del mov seleccionado
    editar: function(id){
      //$("#MER_ESTADO").attr('disabled', false);
        tarimas_consulta.buscarTarimasEdit(id);
        if(!$('#tbl_tarimas').hasClass('fullHidden'))
          $('#tbl_tarimas').addClass('fullHidden')
        if(!$('#tbl_tarimas_cons').hasClass('fullHidden'))
          $('#tbl_tarimas_cons').addClass('fullHidden')
        if($('#frm_tarimas').hasClass('fullHidden'))
            $('#frm_tarimas').removeClass('fullHidden')
            $('#btn_listar_tarimas').show();
        $('#btn_guardar_tarimas').hide();
        $('#btn_editar_tarimas').show();
        $('#btn_regresar_tarimas').hide();
        $('#btn_limpiar_tarimas').show();
        $('#btn_limpiar_tarimas_filtros').hide();
        if($('#btn_editar_tarimas').hasClass('fullHidden'))
        $('#btn_editar_tarimas').removeClass('fullHidden');
        if(!$('#div_tarimas_filtros').hasClass('fullHidden'))
          $('#div_tarimas_filtros').addClass('fullHidden')
      if(!$('#div_tarimas_filtrosZ').hasClass('fullHidden'))
          $('#div_tarimas_filtrosZ').addClass('fullHidden')
        $('#btn_listar_tarimas_consec').show();
         $('#btn_regresar_conocimientos').show();

         if(!$('#frm_tarima_resumen').hasClass('fullHidden'))
                 $('#frm_tarima_resumen').addClass('fullHidden')
    },

    //Busca el movimeinto que se va consultar
    buscarMovConsulta: function (p_movimiento) {
        // principal.resetAllFields('frm_tarimas');
        var entrada = $('#IEN_ENTRADA').val();
        if(entrada != undefined || entrada != ""){ 
            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_MERCANCIAS_TB/GetMercanciasConsulta?guia=' + entrada + '&mercancia=' + p_identificador+ '&tipomercancia=' + null,
                success: function (data) {
                    tarimas_consulta.InfoInicial(tarimas_consulta.ien_entrada,tarimas_consulta.id_manifiesto);
                    //tarimas_consulta.InfoInicial(tarimas_consulta.id_manifiesto);
                    $.each(data[0], function(i,v){
                        if (i === 'MER_OPERACION_TICA'){
                            if(v== 'F'){
                                $('#frm_tarimas #MER_ID_MERCANCIA_REF').prop('disabled', false);
                            }                     
                         }
                      principal.setValueByName(i, v,'frm_tarimas');
                      if (i === 'MER_TIP_CODIGO_IATA'){
                        tarimas_consulta.validacionPeligrosidad();
                      }
                    }); 

                    tarimas_consulta.calculoKilosNeto();
                    $('#TarimaTitulo').text($('#MER_ID_MERCANCIA').val());
                     principal.activeLabelsInput();
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    swal({
                        title: "Tarimas",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",

                    });
                }
            });
        }
    },

     //Limpia los campos de la pantalla 
    limpiar: function(){
        principal.resetAllFields('frm_movimientos');
        $('#MVE_MOV_INVENTARIO_flt').val("");
        $('#MVE_IEN_ENTRADA_flt').val("");
        $('#IEN_DUA_flt').val("");
        $('#IEN_CONSIGNATARIO_flt').val("");
        $('#IEN_NUMERO_VIAJE_flt').val("");
        $('#TMA_NUM_CONTENEDOR_flt').val("");
        $("#movimientos_tablebody").html('');
        principal.deactiveLabelsInput();
        $('#MVE_MOV_INVENTARIO_flt').focus();
        //$('#lbl_MVE_MOV_INVENTARIO_flt').addClass('active');
    },

////////////////////////// Acciones/////////////////////////////////
    

    //Validación para proceder a cerrar todas las tarimas.
    CargaTarimasCerrar: function(listaTarimas){  
        var conocimiento = $("#IEN_GUIA_ORIGINAL_Z").val();
        swal({
          title: "Confirmación?",
          text: "Desea cerrar las tarimas seleccionadas del Conocimiento:" + conocimiento + "?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
           tarimas_consulta.Cerrar(listaTarimas);                            
          } 
        });     
    },

    //Lista los movimientos
    buscarMovsFracciona: function () {
        var ref = $('#FRAC_REFERENCIA').val();
        if(ref != undefined && ref != ""){ 
            var url = '/api/TX_MERCANCIAS_TB/MovsFracciona?conse=' + tarimas_consulta.ien_entrada + '&tarimaref=' + ref;
            var cb = function(data){
                
                if(data.length != 0){
                    $('#btn_actualiza_fracc').show();
                    var templateText = $("#tarimas_ref_table-template").html();
                    //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                    var tableTemplate = Handlebars.compile(templateText);
                    $("#tarimas_ref_tablebody").html('');
                      //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                    $.each(data, function (index, elem) {
                        $("#tarimas_ref_tablebody").append(tableTemplate(elem));
                    });
                }else{
                    $('#btn_actualiza_fracc').hide();
                }
                

             //    tarimas_consulta.InfoInicial();
              //    if($('#frm_tarima_resumen').hasClass('fullHidden'))
                 // $('#frm_tarima_resumen').removeClass('fullHidden')

            };
            principal.ajax('GET',url,cb,"Fraccionamiento");
      }
    },

    //Muestra total de tarimas de la guía 
    Refrescar: function () {
        if(!$('#tbl_tarimas').hasClass('fullHidden'))
            $('#tbl_tarimas').addClass('fullHidden')

        if($('#tbl_tarimas_cons').hasClass('fullHidden'))
            $('#tbl_tarimas_cons').removeClass('fullHidden')

        if(!$('#frm_tarimas').hasClass('fullHidden'))
          $('#frm_tarimas').addClass('fullHidden')
      
        if($('#frm_tarima_resumen').hasClass('fullHidden'))
          $('#frm_tarima_resumen').removeClass('fullHidden')
        
        if($('#div_tarimas_filtrosZ').hasClass('fullHidden'))
        $('#div_tarimas_filtrosZ').removeClass('fullHidden')


        $('#btn_listar_tarimas_consec').hide();
        $('#btn_listar_tarimas').hide();
        $('#btn_guardar_tarimas').hide();
        $('#btn_editar_tarimas').hide();
        $('#btn_limpiar_tarimas').hide();
        $('#btn_regresar_tarimas').show();
        $('#btn_regresar_conocimientos').hide();

        $('#btn_limpiar_tarimas_filtros').show();

        //Botones de accion de cerrar todas las tarimas abiertas
        $('#btn_cerrar').show();
        $('#btn_selec').show();
        $('#btn_deselec').hide();
       

        
        sumatoria.buscarSumatoria(null,null);
    },

    // Lista las tarima abiertas de la guía
    RefrescarTarimas: function () {
        //tarimas_consulta.limpiaFiltros();

        $('#tarima_flr_ien_entrada').val('');
        $("#MER_TME_MERCANCIA_FILTRO").data("kendoComboBox").value("");

        if($('#div_tarimas_filtros').hasClass('fullHidden'))
        $('#div_tarimas_filtros').removeClass('fullHidden')
        if($('#tbl_tarimas').hasClass('fullHidden'))
            $('#tbl_tarimas').removeClass('fullHidden')
        if(!$('#tbl_tarimas_cons').hasClass('fullHidden'))
            $('#tbl_tarimas_cons').addClass('fullHidden')
        if(!$('#frm_tarimas').hasClass('fullHidden'))
          $('#frm_tarimas').addClass('fullHidden')

        if($('#frm_tarima_resumen').hasClass('fullHidden'))
          $('#frm_tarima_resumen').removeClass('fullHidden')

        $('#btn_listar_tarimas_consec').hide();
         $('#btn_listar_tarimas').hide();
        $('#btn_guardar_tarimas').hide();
        $('#btn_editar_tarimas').hide();
        $('#btn_limpiar_tarimas').hide();
        $('#btn_regresar_tarimas').show();
        $('#btn_limpiar_tarimas_filtros').show();
         $('#btn_regresar_conocimientos').hide();

           $('#tarima_flr_ien_entrada').focus();


        var entrada = $('#IEN_ENTRADA').val();
        var mercancia = null;
        var tipomercancia = null;
        tarimas_consulta.buscarTarimas(entrada,mercancia,tipomercancia);
    },

  //Vuelve de la pantalla de lista a la de ingreso de tarima
    regresar: function(){
        if(!$('#div_tarimas_filtros').hasClass('fullHidden'))
            $('#div_tarimas_filtros').addClass('fullHidden')

        if(!$('#div_tarimas_filtrosZ').hasClass('fullHidden'))
            $('#div_tarimas_filtrosZ').addClass('fullHidden')

        if(!$('#tbl_tarimas_cons').hasClass('fullHidden'))
            $('#tbl_tarimas_cons').addClass('fullHidden')

        if(!$('#tbl_tarimas').hasClass('fullHidden'))
            $('#tbl_tarimas').addClass('fullHidden')

        if($('#frm_tarimas').hasClass('fullHidden'))
          $('#frm_tarimas').removeClass('fullHidden')

         if(!$('#tbl_tarimas_ref').hasClass('fullHidden'))
            $('#tbl_tarimas_ref').addClass('fullHidden')

        if(!$('#fracci').hasClass('fullHidden'))
          $('#fracci').addClass('fullHidden')

        $('#btn_listar_tarimas').show();
        if($("#TarimaTitulo").text() != ""){
            $('#btn_editar_tarimas').show();
            $('#btn_guardar_tarimas').hide();
        }else{
            $('#btn_editar_tarimas').hide();
            $('#btn_guardar_tarimas').show();
        }
       
        $('#btn_limpiar_tarimas').show();

         //Botones de accion de cerrar todas las tarimas abiertas
        $('#btn_cerrar').hide();
        $('#btn_selec').hide();
        $('#btn_deselec').hide();
        
        $('#btn_regresar_tarimas').hide();
        $('#btn_listar_tarimas_consec').show();
        $('#btn_limpiar_tarimas_filtros').hide();
        $('#btn_regresar_conocimientos').show();
        $('#btn_fracciona_tarimas').show();

         if(!$('#frm_tarima_resumen').hasClass('fullHidden'))
                 $('#frm_tarima_resumen').addClass('fullHidden')
    }

   
};
$(document).ready(function () {
    movimientos_consulta.init();
});