﻿var bitacora = {

    init: function () {
        bitacora.BIT_limpiarFiltros();

        var pickedup;
        $(document).on("click", "#Bitacora_Table tbody tr", function(e) {
            //regresa al color negro antes de haberlo seleccionado
          if (pickedup != null) {
              pickedup.css( "background-color", "#3C3C3C" );
          }
            // carga la info de la primer columna en el detalle
          $("#text_desc").val($(this).find("td").eq(0).html());
          //cambia el color de la fila a gris
          $( this ).css( "background-color", "grey" );
 
            pickedup = $( this );
        });
    },

    BIT_Listar: function(){
        //Buscar lista de mercancías según filtros
          var p_consecutivo = $("#in_flr_bit_consecutivo").val();
          var p_movimiento = $("#in_flr_bit_movimiento").val();
          var p_tipoEvento = $("#s_flr_bit_tipoEvento").val();
          var p_guia = $("#in_flr_bit_guia").val();


        if (p_consecutivo == "" && p_movimiento == "" && p_guia == "" && p_tipoEvento==""){
            swal({
                    title: "Bitácora",
                    text: "Debe indicar al menos el valor de Consecutivo, Movimiento o Guía para consultar datos.",
                    icon: "error",
                    button: "OK!",
                    timer: 5000,
                })
         }else{
             //Obtenemos la plantilla
            $('#th_Bitacora').show();
            $("#th_Bitacora").attr('style', 'display:block');
            var templateText = $("#BIT_table-template").html();
            //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#Bitacora_table1body").html('');
            //$.getJSON('/api/TX_BIT_GUIAS_TB/GetBitacora?consecutivo='+ p_consecutivo + '&movimiento=' + p_movimiento + '&tipoEvento=' + p_tipoEvento + '&guia=' + p_guia).then(function (data) {
            //    $.each(data, function (index, elem) {
            //        $("#Bitacora_table1body").append(tableTemplate(elem));
            //    })
            //  });
            $.ajax({
                  type: 'GET',
                  dataType: 'json',
                  contentType: 'application/json',
                  url: '/api/TX_BIT_GUIAS_TB/GetBitacora?consecutivo=' + p_consecutivo + '&movimiento=' + p_movimiento + '&tipoEvento=' + p_tipoEvento + '&guia=' + p_guia,
                  //data: postdata,
                success: function (data) {
                    if (data.length > 0) {
                        $.each(data, function (index, elem) {
                            $("#Bitacora_table1body").append(tableTemplate(elem));
                        });
                    }
                    else {
                        swal({
                            title: "Bitácora",
                            text: "No hay datos para los parámetros sugeridos",
                            icon: "error",
                            button: "OK!",

                        });
                    }
                  },
                  failure: function (data) {

                      //console.log("fail");
                      if (data.responseJSON.Message == "false") {
                          prod_nc.infoConsecutivo(consecutivo, sistema);
                      } else {
                          swal({
                              title: "Bitácora",
                              text: data.responseJSON.Message,
                              icon: "error",
                              button: "OK!",

                          });
                      }
                  },
                  error: function (data) {
                      if (data.responseJSON.Message == "false") {
                          prod_nc.infoConsecutivo(consecutivo, sistema);
                      } else {
                          swal({
                              title: "Bitácora",
                              text: data.responseJSON.Message,
                              icon: "error",
                              button: "OK!",

                          });
                      }
                  }
              });

         }
    },

    getTipoEvento: function () {
        principal.getData(
            "/api/TX_BIT_GUIAS_TB/TipoEvento",
            function (data) {
                $("#s_flr_bit_tipoEvento").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        );
    },

    BIT_limpiarFiltros: function () {
        $("#in_flr_bit_consecutivo").val("");
        $("#in_flr_bit_movimiento").val("");
        $("#in_flr_bit_guia").val("");
        $("#in_flr_cjr_cajera").val("");
        $("#text_desc").val("");
        $("#s_flr_bit_tipoEvento").val("");
        $("#Bitacora_table1body").html("");
         bitacora.getTipoEvento();
    }


};

$(document).ready(function () {
    bitacora.init();
})