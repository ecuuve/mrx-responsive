var manifiesto_viaje = {
    init: function () {

    },

    //Pinta viajes en pantalla al darle al +
    pintaViajes: function () {
        var viaje_p = $('#mvi_viaje').val();
        if (viaje_p != '') {
            if (viaje_p.length < 6) {
                swal({
                    title: "Viaje",
                    text: "El tamaño de viaje debe ser de entre 6 y 10 caracteres.",
                    icon: "warning",
                    button: "OK!",

                });
            } else {

                var html = '<tr><td>' + $('#mvi_viaje').val() + '</td>' +
                    '<td><button id="btn_borrar_viaje" class="btn btn-elegant waves-effect waves-light" data-placement="Bottom" data-toggle="tooltip" ' +
                    'onclick="manifiesto_viaje.borrarViajes(this)" title="Eliminar" type="button"><i class="fas fa-trash-alt"></i></button></td>' +
                    '</tr>';
                $('#tbody_viajes').append(html);
                $('#mvi_viaje').val('');

            }

        }
    },

    //Elimina los viajes no deseados
    borrarViajes: function (button) {
        button.closest('tr').remove();
    },

    //Guarda los viajes del manifiesto
    guardar: function (id, callback) {
        var viajes = [];
        var last = $('#tb_viajes > tbody > tr:last');
        if(last.length > 0){
            $('#tb_viajes > tbody > tr').each(function (i, v) {
            var data = {
                'Id': id,
                'Nombre': v.firstElementChild.textContent,
                'MVI_USU_REGISTRO': $('#TMA_USU_REGISTRO').val()
            };
            viajes.push(data);
                if (last[0] == v) {
                    $.post(
                        "/api/TX_MANIFIESTO_VIAJES_TB/GuardarViaje/",
                        { 'id': id, 'viajes': viajes },
                        function (data) {
                            if (typeof (callback) == 'function')
                                callback();
                        })
                        .fail(function (data) {
                            if (data.responseJSON.ModelState) {
                                principal.processErrorsPopUp(data.responseJSON.ModelState);
                            } else {
                                swal({
                                    title: "Manifiesto - Viaje",
                                    text: data.responseJSON.Message,
                                    icon: "error",
                                    button: "OK!",
                                });
                            }
                            manifiesto_duas.limpiar();
                            ingreso_mercancia.limpiar();
                            manifiesto_viaje.limpiar();
                        });
                }
            });
        }else{
           if (typeof (callback) == 'function')
                callback(); 
        }
        
    },

    //Limpia la lista de viajes en pantalla
    limpiar: function () {
        $('#tbody_viajes').html('');
    },

    //Busca viajes del manifiesto
    buscarViajes: function (p_identificador, consulta = null) {
        var viaje = '';
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MANIFIESTO_VIAJES_TB/Viajes?id_manifiesto=' + p_identificador + '&viaje=' + viaje,
            success: function (data) {
                manifiesto_viaje.limpiar();
                $.each(data, function (i, v) {
                    var html = '<tr>';
                    html += '<td>' + v.Nombre + '</td>';
                    if (consulta === null) {
                        html += '<td><button id="btn_borrar_viaje" class="btn btn-elegant waves-effect waves-light" data-placement="Bottom" data-toggle="tooltip" ' +
                            'onclick="manifiesto_viaje.borrarViajes(this)" title="Eliminar" type="button"><i class="fas fa-trash-alt"></i></button></td>';
                    }
                    html += '</tr>';
                    $('#tbody_viajes').append(html);
                });
            },
            failure: function (data) {
                console.log("fail", data);
            },
            error: function (data) {
                swal({
                    title: "Viajes",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                });
            }
        });
    },

    //Elimina los viajes del manifiesto
    delete: function (id, callback) {
        var viajes = [];
        //var last = $('#tb_viajes > tbody > tr:last');

        // if(last.length > 0){

            $('#tb_viajes > tbody > tr').each(function (i, v) {
            var data = {
                'Id': id,
                'Nombre': v.firstElementChild.textContent,
                'MVI_USU_REGISTRO': $('#TMA_USU_REGISTRO').val()
            };
            viajes.push(data);
            });
            $.post(
            "/api/TX_MANIFIESTO_VIAJES_TB/BorrarViaje/",
            { 'id': id, 'viajes': viajes },
            function (data) {
                if (data != null) {
                    manifiesto_viaje.guardar(data, callback);
                }
            })
            .fail(function (data) {
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                } else {
                    swal({
                        title: "Manifiesto - Viajes",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",
                    });
                }
            });
        // }else{
        //    if (typeof (callback) == 'function')
        //         callback(); 
        // }

        
    }
};
$(document).ready(function () {
    manifiesto_viaje.init();
});