﻿var diferencia = {

    init: function () {       
         $("#frm_diferencia").attr('style', 'display:none');
         $("#btn_regresar").attr('style', 'display:none');
         $("#btn_actualizar").attr('style', 'display:none');
         $("#btn_limpiar").attr('style', 'display:none');

         diferencia.getEstadosDiferencia();
         diferencia.getTipoDiferencia();
         diferencia.getCausas();


         diferencia.LimpiarFiltrosInv();

         //Disparo evento de consulta de guía cuando dan enter
        $('#in_flr_itf_inventario').bind("enterKey",function(e){
          diferencia.DTF_Listar();
        });
        $('#in_flr_itf_inventario').keyup(function(e){
          if(e.keyCode == 13)
          {
            $(this).trigger("enterKey");
          }
        });

    },


    onfocus: function () {
        if ($("#lbl_id").hasClass('lblfocus'))
            $("#lbl_id").removeClass('lblfocus');
    },

    //lista el inventario
    DTF_Listar: function(){
        //Buscar lista de diferencias según filtros
          var p_inventario = $("#in_flr_itf_inventario").val();
        

          $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_DIFERENCIAS_TOMA_TB/GetDiferencias?cod_inventario='+ p_inventario,
                //data: postdata,
                success: function (data) {
                    
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Ajustes de Inventario",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                     }else{
                        $("#in_flr_inventario").val(data[0]["INVENTARIO"]);
                        $("#lbl_invt").addClass('active');
                        //Obtenemos la plantilla
                        $('#th_Diferencias').show();
                        $("#th_Diferencias").attr('style', 'display:block');
                        var templateText = $("#DTF_table-template").html();
                        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                        var tableTemplate = Handlebars.compile(templateText);
                        $("#Diferencias_table1body").html('');
                         $.each(data, function (index, elem) {
                            $("#Diferencias_table1body").append(tableTemplate(elem));
                         })
                     }
                }
                
            });












        // //Obtenemos la plantilla
        // $('#th_Diferencias').show();
        // $("#th_Diferencias").attr('style', 'display:block');
        // var templateText = $("#DTF_table-template").html();
        // //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        // var tableTemplate = Handlebars.compile(templateText);
        // $("#Diferencias_table1body").html('');
        // $.getJSON('/api/TX_DIFERENCIAS_TOMA_TB/GetDiferencias?cod_inventario='+ p_inventario).then(function (data) {
            
        //      if (typeof data === 'string' || data instanceof String){
        //                 swal({
        //                   title: "Manifiesto",
        //                   text: data,
        //                   icon: "error",
        //                   button: "OK!",
        //                 })
        //      }else{
        //          $.each(data, function (index, elem) {
        //             $("#Diferencias_table1body").append(tableTemplate(elem));
        //          })
        //      }
           
        // });
    },

    //Muestra la lista de inventarios en un modal.
    MostrarListaInventarios: function(){
        $("#myModalInventarios").modal('show');
        //Obtenemos la plantilla
        var templateText = $("#Inventario_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#Inventario_tablebody").html('');
        $.getJSON('/api/TX_DIFERENCIAS_TOMA_TB/GetInventarios').then(function (data) {
           
                $.each(data, function (index, elem) {
                $("#Inventario_tablebody").append(tableTemplate(elem));
                })
             
           
        });
    },

    //Selecciona el inventario del modal y carga los datos en los campos de filtro
    EscogerInvent: function (id, nombre) {
        $("#in_flr_itf_inventario").val(id);
        $("#in_flr_inventario").val(nombre);  
        principal.activeLabels();
        $("#myModalInventarios").modal('hide');
        diferencia.DTF_Listar();
    },

    //Limpia pantalla
    LimpiarFiltrosInv: function () {
        $("#in_flr_itf_inventario").val("");
        $("#in_flr_inventario").val(""); 
        principal.deactivateLabels();
        $("#Diferencias_table1body").html("");
        $("#in_flr_itf_inventario").focus();
    },

    //Consulta datos para editar
    DFT_ConsultarDatos: function (cod_diferencia) {      
        var postdata = { id: cod_diferencia };
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_DIFERENCIAS_TOMA_TB/GetDiferenciaDetalle?cod_diferencia='+ cod_diferencia,
            data: postdata,
            success: function (data) {
                if (data !== null) {

                    //trae los datos de la mercancia
                    principal.activeLabels();
                    diferencia.DFT_MuestraDatos(data[0]);

                    $("#th_Diferencias").attr('style', 'display:none');                   
                    $("#div_filters").attr('style', 'display:none');     
                    $("#frm_diferencia").attr('style', 'display:block');

                    $("#btn_regresar").attr('style', 'display:block');
                    $("#btn_actualizar").attr('style', 'display:block');
                    $("#btn_limpiar").attr('style', 'display:none'); 

                    $("#lbl_tipodif").removeClass('active');
                    $("#lbl_estado").removeClass('active');
                    $("#lbl_solucion").removeClass('active'); 
                    $("#lbl_causa").removeClass('active'); 
                    
                }
            },
            failure: function (data) {
                
                console.log("fail");
            },
            error: function (data) {

                console.log("error consulta");
            }
        });
    },

    //Carga los datos del inventario a editar 
    DFT_MuestraDatos: function (p_inventario) {       
        for (var campo in p_inventario) {
            
            if(p_inventario[campo] != null){
                principal.setValueByName(campo, p_inventario[campo],'frm_diferencia');
            }  
        }      
    },


    DFT_limpiar: function () {
        principal.resetAllFields('detalles_diferencia');
        principal.deactivateLabels();    
    },

    DFT_Regresar: function(){
        $("#frm_diferencia").attr('style', 'display:none');
        $("#btn_regresar").attr('style', 'display:none');
        $("#btn_actualizar").attr('style', 'display:none');
        $("#btn_limpiar").attr('style', 'display:none');

        $("#th_Diferencias").attr('style', 'display:block');
        $("#div_filters").attr('style', 'display:block'); 

        diferencia.DTF_Listar();
    },


    getCausas: function (valor) {
        if (valor === "") {
            valor = "0";
        }
        principal.getData(
            "/api/TX_DIFERENCIAS_TOMA_TB/GetCausas",
            function (data) {
                $("#s_causas").html(principal.arrayToOptionsSelected(data, valor));
            }
        )
    },

    getEstadosDiferencia: function (valor) {
        if (valor === "") {
            valor = "0";
        }
        principal.getData(
            "/api/TX_DIFERENCIAS_TOMA_TB/GetEstados",
            function (data) {
                $("#DTF_ESTADO").html(principal.arrayToOptionsSelected(data, valor));
            }
        )
    },

    getTipoDiferencia: function (valor) {
        if (valor === "") {
            valor = "0";
        }
        principal.getData(
            "/api/TX_DIFERENCIAS_TOMA_TB/GetTiposDif",
            function (data) {
                $("#DTF_TIPO_DIFERENCIA").html(principal.arrayToOptionsSelected(data, valor));
            }
        )
    },


    DFT_Actualizar: function(){
        $("#UserMERX").val($("#UsReg").val());

        $("#CONSECUTIVO").attr('disabled', false);
        $("#CONSIGNATARIO").attr('disabled', false);
        $("#DTF_MER_ID_MERCANCIA").attr('disabled', false);
        $("#DTF_BULTOS").attr('disabled', false);
        $("#DTF_LOCALIZACION").attr('disabled', false);

        var id = $('#DTF_ID').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_diferencia').serializeArray());
            $("#CONSECUTIVO").attr('disabled', true);
            $("#CONSIGNATARIO").attr('disabled', true);
            $("#DTF_MER_ID_MERCANCIA").attr('disabled', true);
            $("#DTF_BULTOS").attr('disabled', true);
            $("#DTF_LOCALIZACION").attr('disabled', true);   
            $.ajax({
                url: '/api/TX_DIFERENCIAS_TOMA_TB/',
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Actualización de Ajuste",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{
                        swal({
                        title: "Ajustes de Inventario",
                        text: "Se actualizó correctamente la información",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                           
                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Ajustes de Inventario",
                text: "El ID de la inventario no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    }
};

$(document).ready(function () {
    diferencia.init();
})