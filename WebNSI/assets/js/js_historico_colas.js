﻿var historicocolas = {
pickedup : null,
    init: function () {
      historicocolas.QUE_limpiar();
        $('#rack-tab').on('click', function () { 
             historicocolas.QUE_limpiar();
        }); 

        $('#carrusel-tab').on('click', function () { 
             historicocolas.QUE_Listar("AC");
             historicocolas.QUE_limpiar();
        });  

        $('#salidacarrusel-tab').on('click', function () { 
             historicocolas.QUE_Listar("SC");
             historicocolas.QUE_limpiar();
        });  
        $('#salidarack-tab').on('click', function () { 
             historicocolas.QUE_Listar("SR");
             historicocolas.QUE_limpiar(); 
        }); 
        $('#salidaSZ-tab').on('click', function () { 
             historicocolas.QUE_Listar("SZ");
             historicocolas.QUE_limpiar();
        }); 

        $('#in_flr_que_ien_entradaAR').bind("cut copy paste",function(e) {
             e.preventDefault();
         });
        $('#in_flr_que_ien_entradaAC').bind("cut copy paste",function(e) {
             e.preventDefault();
         });
        $('#in_flr_que_ien_entradaSC').bind("cut copy paste",function(e) {
             e.preventDefault();
         }); 
        $('#in_flr_que_ien_entradaSR').bind("cut copy paste",function(e) {
             e.preventDefault();
         });
        $('#in_flr_que_ien_entradaSZ').bind("cut copy paste",function(e) {
             e.preventDefault();
         });    
    },

  // Accion al hacer click en la línea
  QUE_Funcionalidad_Click: function(tipo,consecutivo, linea, activeTab){  
     //Limpia tabla de mercancias y la oculta
     $("#MERC_"+activeTab+"_table1body").html("");
     $('#th_merc'+ activeTab).hide();

    //regresa al color negro antes de haberlo seleccionado
       if (historicocolas.pickedup != null) {
           historicocolas.pickedup.css( "background-color", "#3C3C3C" );
       }
       //cambia el color de la fila a gris
       $( linea ).css( "background-color", "grey" );

      historicocolas.pickedup = $( linea );

       // carga la info de la  columna 11 en el campo agente
        $("#in_agente" + activeTab).val("");
        $("#in_agente" + activeTab).val(linea.find("td").eq(11).html());

        // carga la info de la  columna 12 en el campo importador
        $("#in_importador" + activeTab).val("");
        $("#in_importador" + activeTab).val(linea.find("td").eq(12).html());

        principal.activeLabels();

        var cb = function(){
            historicocolas.QUE_CheckearTarimas(activeTab);
        };

       historicocolas.QUE_Listar_Mercancias(consecutivo,activeTab,cb);
    },
  
//Métodos que van al api a listar 
    QUE_Listar: function(activeTab,callback){ 
          $("#in_agente" + activeTab).val("");
          $("#in_importador" + activeTab).val("");                
          var p_consecutivo = $("#in_flr_que_ien_entrada" + activeTab).val();
          var p_tipoCola = activeTab;
          if (p_consecutivo == ""){
            p_consecutivo = null;
          }
          
            $.getJSON('/api/TX_COLAS_TB/GetColasHistorico?consecutivo='+ p_consecutivo + '&tipoCola=' + p_tipoCola).then(function (data) {
                if(data.length == 0 && p_consecutivo != null){
                    var titlemsj = "";
                    switch(activeTab) {
                      case "AR":
                        titlemsj = "Colas Aforo Rack";
                        break;
                      case "SR":
                        titlemsj = "Colas Salida Rack";
                        break;
                      case "AC":
                        titlemsj = "Colas Aforo Carrusel";
                        break;
                      case "SC":
                        titlemsj = "Colas Salida Carrusel";
                        break;
                      case "SZ":
                        titlemsj = "Colas Salida Zona Franca";
                        break;
                      default:
                        titlemsj = "Histórico Colas";
                    }
                    swal({
                          title: titlemsj,
                          text: "No existe información para el consecutivo dado.",
                          icon: "error",
                          button: "OK!",
                        })
                }else{
                    //Obtenemos la plantilla
                    $('#th_ColaAforo' + activeTab).show();
                    $("#th_ColaAforo" + activeTab).attr('style', 'display:block');
                    var templateText = $("#QUE_"+ activeTab+"_table-template").html();
                    //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                    var tableTemplate = Handlebars.compile(templateText);
                    $("#Cola"+ activeTab +"_table1body").html('');
                    
                    $.each(data, function (index, elem) {
                      $("#Cola"+ activeTab+"_table1body").append(tableTemplate(elem));
                    })

                    $('#th_merc'+ activeTab).hide();

                    $("#Cola"+activeTab+"_Table > tbody > tr").on("click", function(e) {
                    //if($("#aforocarrusel-tab").hasClass("active")){
                      var consecutivo = $(this).find("td").eq(0).html();
                      historicocolas.QUE_Funcionalidad_Click("Cola"+activeTab, consecutivo, $( this ),activeTab);
                    //}
                    });  

                    if(typeof(callback) == "function"){
                        callback();
                    }
                }               
            });            
    },

//Método que va al api a listar las mercancias 
    QUE_Listar_Mercancias: function(consecutivo_p, activeTab, callback){
        //Obtenemos la plantilla
        $('#th_merc'+activeTab).show();
        $("#th_merc" + activeTab ).attr('style', 'display:block');
        var templateText = $("#MERC_"+activeTab+"_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#MERC_"+activeTab+"_table1body").html('');
        $.getJSON('/api/TX_COLAS_TB/GetMercCola?consecutivo='+ consecutivo_p).then(function (data) {
            $.each(data, function (index, elem) {
                $("#MERC_"+activeTab+"_table1body").append(tableTemplate(elem));
            })
            if(typeof(callback) == "function"){
                callback();
            }
        });
    },

//Funcionalidad de checkear las mercancías
    QUE_CheckearTarimas: function(activeTab){
        $('#MERC_'+activeTab+'_Table tbody tr').each(function(){
            var accion = $(this).find('td span input[type="checkbox"]');
            var checkbox = accion.attr('id');

            if(!accion.prop('checked') && $('#' + checkbox).val() == "S"){
                
                $('#' + checkbox).prop('checked', true);
            }

            if(accion.prop('checked') && $('#' + checkbox).val() == "N"){
                
                $('#' + checkbox).prop('checked', false);
            }
        });
    },

//Métodos para limpiar 
    QUE_limpiar: function () {
         $("#in_flr_que_ien_entradaAR").val("");
         $("#in_agenteAR").val("");
         $("#in_importadorAR").val("");
         $("#ColaAforoAR_table1body").html("");
         $("#MERC_AR_table1body").html("");
         $('#th_mercAR').hide();

         $("#in_flr_que_ien_entradaSR").val("");
         $("#in_agenteSR").val("");
         $("#in_importadorSR").val("");
         $("#ColaAforoSR_table1body").html("");
         $("#MERC_SR_table1body").html("");
         $('#th_mercSR').hide();

         $("#in_flr_que_ien_entradaAC").val("");
         $("#in_agenteAC").val("");
         $("#in_importadorAC").val("");
         $("#ColaAforoAC_table1body").html("");
         $("#MERC_AC_table1body").html("");
         $('#th_mercAC').hide();

         $("#in_flr_que_ien_entradaSC").val("");
         $("#in_agenteSC").val("");
         $("#in_importadorSC").val("");
         $("#ColaAforoSC_table1body").html("");
         $("#MERC_SC_table1body").html("");
         $('#th_mercSC').hide();

         $("#in_flr_que_ien_entradaSZ").val("");
         $("#in_agenteSZ").val("");
         $("#in_importadorSZ").val("");
         $("#ColaAforoSZ_table1body").html("");
         $("#MERC_SZ_table1body").html("");
         $('#th_mercSZ').hide();
                  
         historicocolas.QUE_Listar("AR"); 
    }
};

$(document).ready(function () {
    historicocolas.init();
})