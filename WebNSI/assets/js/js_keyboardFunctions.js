﻿
function FuncionalidadTeclas() {
    $(function () {
        //$(document).keyup(function (e) {
        $('input').bind('keydown', function (e) {
            var key = (e.keyCode ? e.keyCode : e.charCode);
            switch (key) {
                case 120:
                    //navigateUrl($('@Url.Action("Index")'));
                    alert('Apreto F9');
                    e.PreventDefault();
                    break;
                case 13:
                    var nxtIdx = $('input').index(this) + 1;
                    console.log('Enter: ' + nxtIdx);
                    $(":input:eq(" + nxtIdx + ")").focus();
                    e.PreventDefault();
                    break;
                case 9:
                    var input = $('input:focus');
                    var index = input.index(this);
                    console.log('index: ' + index);
                    var nxtIdx = $('input').index(this) + 1;
                    console.log('Tab: ' + nxtIdx);
                    $(":input:eq(" + nxtIdx + ")").focus();
                    e.PreventDefault();
                    break;



                default:;
            }
        });
        function navigateUrl(jObj) {
            window.location.href = $(jObj).attr("href");
        }
    });
}

//Listener para activar la navegacion con el Enter como si fuera el TAB (funcional Juan Jose Salazar)
function TecladoFuncionalidadPersonalizada() {
    //$('input:first').focus();
    $('input:visible:enabled:first').focus();
    var tabindex = 1;
    $(document).keypress(function (event) {
        tabindex = event.target.tabIndex
        // var nuevo = $(this).
        var keycode = (event.keyCode ? event.keyCode : event.which);
        //console.log('keycode ' + keycode)
        //Valido que sea el codigo de Enter
        if (keycode == 13) { //onEnter
            tabindex++;
            //while element exist or it's readonly and tabindex not reached max do
            while (($("[TabIndex='" + tabindex + "']").length == 0 || $("[TabIndex='" + tabindex + "']:not([readonly])").length == 0) && tabindex != 150) {
                tabindex++;
            }
            //Mientras los campos esten en disabled se los brinca hasta que encuente uno que no
            while($("[TabIndex='" + tabindex + "']").prop('disabled') == true){
                tabindex ++;
            }
            if (tabindex == 150) { tabindex = 1 } //reseting tabindex if finished
            $("[TabIndex='" + tabindex + "']").focus()
            return false;
        }
    });
};

function TecladoFuncionalidadIngresoMercancia() {
    $('#tma_manifiesto').focus();
    var tabindex = 1;
    $(document).keypress(function (event) {
        tabindex = event.target.tabIndex;
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == 13) {
            if($('#TMA_MOD_TRANSPORTE').val() == 4){
                if(tabindex == 2){
                    tabindex = 6
                }else{
                    tabindex++;
                    //while element exist or it's readonly and tabindex not reached max do
                    while (($("[TabIndex='" + tabindex + "']").length == 0 || $("[TabIndex='" + tabindex + "']:not([readonly])").length == 0) && tabindex != 150) {
                        tabindex++;
                    }
                    //Mientras los campos esten en disabled se los brinca hasta que encuente uno que no
                    while($("[TabIndex='" + tabindex + "']").prop('disabled') == true){
                        tabindex ++;
                    }
                    if (tabindex == 150) { tabindex = 1 } //reseting tabindex if finished
                }
            }else{
                tabindex++;
                //while element exist or it's readonly and tabindex not reached max do
                while (($("[TabIndex='" + tabindex + "']").length == 0 || $("[TabIndex='" + tabindex + "']:not([readonly])").length == 0) && tabindex != 150) {
                    tabindex++;
                }
                //Mientras los campos esten en disabled se los brinca hasta que encuente uno que no
                while($("[TabIndex='" + tabindex + "']").prop('disabled') == true){
                    tabindex ++;
                }
                if (tabindex == 150) { tabindex = 1 } //reseting tabindex if finished
            }
            $("[TabIndex='" + tabindex + "']").focus()
            return false;
        }
    });
};

function TecladoFuncionalidadTarimas() {
    var tabindex = 1;
    $(document).keypress(function (event) {
        tabindex = event.target.tabIndex;
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == 13) {
            if($('#MER_TIP_CODIGO_IATA').val() == 7){
                if(tabindex == 12){
                    tabindex = 14
                }else{
                    tabindex++;
                    //while element exist or it's readonly and tabindex not reached max do
                    while (($("[TabIndex='" + tabindex + "']").length == 0 || $("[TabIndex='" + tabindex + "']:not([readonly])").length == 0) && tabindex != 150) {
                        tabindex++;
                    }
                    //Mientras los campos esten en disabled se los brinca hasta que encuente uno que no
                    while($("[TabIndex='" + tabindex + "']").prop('disabled') == true){
                        tabindex ++;
                    }
                    if (tabindex == 150) { tabindex = 1 } //reseting tabindex if finished
                }
            }else{
                tabindex++;
                //while element exist or it's readonly and tabindex not reached max do
                while (($("[TabIndex='" + tabindex + "']").length == 0 || $("[TabIndex='" + tabindex + "']:not([readonly])").length == 0) && tabindex != 150) {
                    tabindex++;
                }
                //Mientras los campos esten en disabled se los brinca hasta que encuente uno que no
                while($("[TabIndex='" + tabindex + "']").prop('disabled') == true){
                    tabindex ++;
                }
                if (tabindex == 150) { tabindex = 1 } //reseting tabindex if finished
            }
            $("[TabIndex='" + tabindex + "']").focus()
            return false;
        }
    });
};


