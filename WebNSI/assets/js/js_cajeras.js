﻿var cajera = {

    init: function () {
        
         $("#detalles-cajera").attr('style', 'display:none');
         $("#btn_regresar").attr('style', 'display:none');
         $("#btn_actualizar").attr('style', 'display:none');
         $("#btn_limpiar").attr('style', 'display:none');
         $("#btn_guardar").attr('style', 'display:none');
         cajera.CJR_Listar();

    },

    CJR_Listar: function(){
        //Buscar lista de mercancías según filtros
          var p_cajera = $("#in_flr_cjr_cajera").val();
        //Obtenemos la plantilla
        $('#th_Cajeras').show();
        $("#th_Cajeras").attr('style', 'display:block');
        var templateText = $("#CJR_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#Cajeras_table1body").html('');
        $.getJSON('/api/TX_CAJERAS_TB/GetCajeras?cod_cajera='+ p_cajera).then(function (data) {
            $.each(data, function (index, elem) {
                $("#Cajeras_table1body").append(tableTemplate(elem));
            })
        });
    },

    CJR_BuscarFiltro: function () {
        if($("#in_flr_cjr_cajera").val() == ""){
            cajera.CJR_Listar();
        }
        principal.filterTable_Input('in_flr_cjr_cajera', 'Cajeras_Table', 0);
    },

    CJR_Guardar: function(){
        $('#CJR_Usu_registro').val($('#CJR_USU').val());

        $("#CJR_CAJERA").attr('disabled', false);
        $('[name= "CJR_Principal"]').is(":checked") == true ? $('[name= "CJR_Principal"]').val('S') : $('[name= "CJR_Principal"]').val('N');
        $('[name= "CJR_Activo"]').is(":checked") == true ? $('[name= "CJR_Activo"]').val('S') : $('[name= "CJR_Activo"]').val('N');
        $('[name= "CJR_Doble_impr"]').is(":checked") == true ? $('[name= "CJR_Doble_impr"]').val('S') : $('[name= "CJR_Doble_impr"]').val('N');
 

        var id = $('#CJR_CAJERA').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_cajera').serializeArray());
            $("#CJR_CAJERA").attr('disabled', true);   
            $.ajax({
                url: '/api/TX_CAJERAS_TB/',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Ingreso de cajera",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{
                        swal({
                        title: "Cajeras",
                        text: "Se ingresó correctamente la información",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            cajera.CJR_Regresar();                           
                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Cajeras",
                text: "El ID de la cajera no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    },

    CJR_Actualizar: function(){

        $('#CJR_Usu_modifico').val($('#CJR_USU').val());
        $("#CJR_CAJERA").attr('disabled', false);
        $('[name= "CJR_Principal"]').is(":checked") == true ? $('[name= "CJR_Principal"]').val('S') : $('[name= "CJR_Principal"]').val('N');
        $('[name= "CJR_Activo"]').is(":checked") == true ? $('[name= "CJR_Activo"]').val('S') : $('[name= "CJR_Activo"]').val('N');
        $('[name= "CJR_Doble_impr"]').is(":checked") == true ? $('[name= "CJR_Doble_impr"]').val('S') : $('[name= "CJR_Doble_impr"]').val('N');
 

        var id = $('#CJR_CAJERA').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_cajera').serializeArray());
            $("#CJR_CAJERA").attr('disabled', true);   
            $.ajax({
                url: '/api/TX_CAJERAS_TB/' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Actualización de cajera",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{
                        swal({
                        title: "Cajeras",
                        text: "Se actualizó correctamente la información",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            // if(cambioR == 'false'){
                                //cajera.CJR_limpiarFiltros();
                                cajera.CJR_Regresar();
                            // }
                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Cajeras",
                text: "El ID de la cajera no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    },


    CJR_ConsultarDatos: function (p_cajera) {      
        var postdata = { id: p_cajera };
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_CAJERAS_TB/GetCajeras?cod_cajera='+ p_cajera,
            data: postdata,
            success: function (data) {
                if (data !== null) {

                    //trae los datos de la mercancia
                     cajera.ActiveCamposCajera();
                     cajera.CJR_MuestraDatos(data[0]);
                    //$("#divwithscroll2").attr('style', 'display:none');
                    $("#th_Cajeras").attr('style', 'display:none');                   
                    $("#div_filters").attr('style', 'display:none');     
                    $("#detalles-cajera").attr('style', 'display:block');
                    $("#btn_regresar").attr('style', 'display:block');
                    $("#btn_actualizar").attr('style', 'display:block');
                    $("#btn_limpiar").attr('style', 'display:none'); 
                    $("#btn_nueva_cajera").attr('style', 'display:none'); 
                   
                }
            },
            failure: function (data) {
                
                console.log("fail");
            },
            error: function (data) {

                console.log("error consulta cajeras");
            }
        });
    },

    CJR_MuestraDatos: function (p_cajeras) {       
        for (var campo in p_cajeras) {
            if(campo === "CJR_Principal"){
                if(p_cajeras[campo] === "S"){
                    p_cajeras[campo] = true;
                } else{
                    p_cajeras[campo] = false;
                }
            }
            if(campo === "CJR_Activo"){
                if(p_cajeras[campo] === "S"){
                    p_cajeras[campo] = true;
                } else{
                    p_cajeras[campo] = false;
                }
            }
            if(campo === "CJR_Doble_impr"){
                if(p_cajeras[campo] === "S"){
                    p_cajeras[campo] = true;
                } else{
                    p_cajeras[campo] = false;
                }
            }
            if(p_cajeras[campo] != null){
                principal.setValueByName(campo, p_cajeras[campo],'frm_cajera');
            }  
        }      
    },

    CJR_Regresar: function(){
        $("#detalles-cajera").attr('style', 'display:none');
        $("#btn_regresar").attr('style', 'display:none');
        $("#btn_actualizar").attr('style', 'display:none');
        $("#btn_guardar").attr('style', 'display:none');
        $("#btn_limpiar").attr('style', 'display:none');
        $("#th_Cajeras").attr('style', 'display:block');
        $("#div_filters").attr('style', 'display:block');   
        $("#btn_nueva_cajera").attr('style', 'display:block');  
        cajera.CJR_limpiarFiltros();    
    },

    CJR_Nueva: function(){
        $("#detalles-cajera").attr('style', 'display:block');
        $("#btn_regresar").attr('style', 'display:block');
        $("#btn_guardar").attr('style', 'display:block');
        $("#btn_limpiar").attr('style', 'display:block');
        $("#btn_actualizar").attr('style', 'display:none');
        $("#th_Cajeras").attr('style', 'display:none');
        $("#div_filters").attr('style', 'display:none');   
        $("#btn_nueva_cajera").attr('style', 'display:none');
        cajera.CJR_limpiar();      
    },

    CJR_Borrar: function(p_cajera){
        swal({
          title: "Está seguro?",
          text: "Eliminará la información de la cajera.",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            cajera.CJR_Eliminar(p_cajera);                     
          } 
        });        
    },

     CJR_Eliminar: function(id){
        if (id.length > 0) {         
            $.ajax({
                url: '/api/TX_CAJERAS_TB/' + id,
                type: 'DELETE',
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Eliminar cajera",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{
                        swal({
                            title: "Cajeras",
                            text: "Se eliminó correctamente la información",
                            icon: "success",
                            button: "OK!",

                            }).then((value) => {

                                cajera.CJR_limpiarFiltros();
                        });
                    }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Cajeras",
                text: "El ID de la cajera no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    },

    CJR_limpiarFiltros: function () {
        $("#in_flr_cjr_cajera").val("");
        cajera.DeactiveCamposCajera();
       cajera.CJR_Listar();
    },

    CJR_limpiar: function () {
        $("#CJR_CAJERA").val("");
        $("#CJR_CAJERA").attr('disabled', false);
        $("#CJR_Principal").prop('checked', false);
        $("#CJR_Activo").prop('checked', false);
        $("#CJR_Doble_impr").prop('checked', false);       
        cajera.DeactiveCamposCajera();      
    },

    ActiveCamposCajera: function () {
        $('label').addClass('active');
    },

    DeactiveCamposCajera: function () {
        $('label').removeClass('active');
    }

};

$(document).ready(function () {
    cajera.init();
})