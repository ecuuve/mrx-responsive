﻿//Metodos para el funcionamiento con WebAppi
var manifiesto = {

    init: function () {
       // principal.hidemenu();
        $('#th_manifiesto').hide();
        $('#div_filters').hide();
        TecladoFuncionalidadPersonalizada();
        manifiesto.BuscarManifiestoEnter();
        principal.DefaultDateHour();
        manifiesto.listenerFilterPuertos('txt_filtro_puerto', 's_mma_puertos');
        manifiesto.listenerFiltersManifiestos('txt_filtro_transportista', 's_mma_transportista');
        manifiesto.cambiarEstadoBotones(false, false, true, true, false, true, true, false, true);
        manifiesto.CargaControlesManifiesto();
        manifiesto.limpia();

        if($('#in_mma_manifiesto').val()!="")
        {
            manifiesto.buscarManifiesto($('#in_mma_manifiesto').val());
            $("#in_mma_manifiesto").attr('disabled', true);
        }
    },
    onfocus: function () {
        if ($("#lbl_mma_manifiesto").hasClass('lblfocus'))
            $("#lbl_mma_manifiesto").removeClass('lblfocus');
    },

    //Carga selects del formulario de manifiesto
    CargaControlesManifiesto: function () {
        manifiesto.getModalidadTransporte();
        manifiesto.getTransportistas();
        manifiesto.getAduanas();
        manifiesto.getEstados();        
        manifiesto.getPuertosTotal(); 
        manifiesto.CargaFechasValidas();                                  
    },

    // Carga el rango de fechas validas para un manifiesto
    CargaFechasValidas: function(){
        var today = new Date();
        var minDate = today.setDate(today.getDate()-1);
        $("#dtp_mma_fechaManifiesto").kendoDatePicker({
          format: "dd/MM/yyyy",
          value: new Date(),
          min: new Date(minDate),
          max: new Date(),
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });
        
        $("#dtp_mma_fechaManIngreso").kendoDatePicker({
          format: "dd/MM/yyyy",
          value: new Date(),
          min: new Date(minDate),
          max: new Date(),
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });

        $("#dtp_mma_fechaManifiesto").data("kendoDatePicker").enable();
        $("#dtp_mma_fechaManIngreso").data("kendoDatePicker").enable();
    },

    //Regresa a la pantalla principal del formulario manifiesto
    Regresar: function () {
        //principal.hidemenu();
        $("#th_manifiesto").hide();
        $('#div_filters').hide();
        manifiesto.cambiarEstadoBotones(false, false, true, true, false, true, true, false,true);
        manifiesto.CargaControlesManifiesto();
        $('#detalles-manifiesto').show();
        principal.activeLabels();
        manifiesto.CargaFechasValidas();
    },

    //Metodo para carga la lista de manifiestos
    Refrescar: function () {
        principal.hidemenu();
        $('#detalles-manifiesto').hide();
        manifiesto.limpia();
        $('#th_manifiesto').show();
        $('#div_filters').show();
        manifiesto.limpiarCamposFiltrosManifiesto();
        manifiesto.buscarManifiesto($('#in_mma_manifiesto').val());
        manifiesto.cambiarEstadoBotones(false, true, true, true, true, false, true, true,true);
    },

    //Listener del cambio en el textbox de manifiesto que haria la llamada al metodo ajax para buscar el manifiesto
    BuscarManifiestoEnter: function () {
        $('#in_mma_manifiesto').change(function () {
            manifiesto.buscarManifiesto($('#in_mma_manifiesto').val());
        });
    },

    //Cambia estado de botones dependiendo de la accion
    cambiarEstadoBotones: function (listar, guardar, editar, cerrar, limpiar, filtro, guias, regresar,contenedores) {
        $('#btn_listar_manifiesto').prop('disabled', listar);
        $('#btn_guardar_manifiesto').prop('disabled', guardar);
        $('#btn_editar_manifiesto').prop('disabled', editar);
        $('#btn_cerrar_manifiesto').prop('disabled', cerrar);
        $('#btn_limpiar_manifiesto').prop('disabled', limpiar);
        $('#btn_filters_manifiesto').prop('disabled', filtro);
        $('#btn_guias_manifiesto').prop('disabled', guias);
        $('#btn_contenedores_manifiesto').prop('disabled', contenedores);
        if (regresar == true) {
            $("#btn_regresar_manifiesto").show();
        }
        else {
            $("#btn_regresar_manifiesto").hide();
        }
    },

    //Metodo para crear un nuevo manifiesto
    GuardarManifiesto: function () {
        //Busco si esta chequeado para asignarle el valor
        $("#UserMERX").val($("#UsReg").val());
        $('[name= "TMA_VIAJE_CONTINGENCIA"]').is(":checked") == true ? $('[name= "TMA_VIAJE_CONTINGENCIA"]').val('S') : $('[name= "TMA_VIAJE_CONTINGENCIA"]').val('N');
        $("#dtp_mma_fechaManifiesto").data("kendoDatePicker").enable();
        $("#dtp_mma_fechaManIngreso").data("kendoDatePicker").enable();
        $.post(
                "/api/TX_MANIFIESTOS_TB/",
                 $('#frm_manifiesto').serialize(),
                function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Manifiesto",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{

                        swal({
                        title: "Manifiesto",
                        text: "Manifiesto guardado exitosamente",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            manifiesto.limpia();
                        });
                    }
                })
                .fail(function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                    $("#dtp_mma_fechaManifiesto").attr("disabled", true);
                    $("#dtp_mma_fechaManIngreso").attr("disabled", true);
                })
        ;
    },

    //Ejecuta la actualizacion del Manifiesto
    Actualizar: function () {
        var id = $('#in_mma_manifiesto').val();
        if (id.length > 0) {

            $("#in_mma_manifiesto").attr('disabled', false);
            $("#in_mma_guia_madre").attr('disabled', false);
            $("#dtp_mma_fechaManifiesto").attr("disabled", false);
            $("#dtp_mma_fechaManIngreso").attr("disabled", false);
            $("#s_mma_estado").attr('disabled', false);
            $('[name= "TMA_VIAJE_CONTINGENCIA"]').is(":checked") == true ? $('[name= "TMA_VIAJE_CONTINGENCIA"]').val('S') : $('[name= "TMA_VIAJE_CONTINGENCIA"]').val('N');
            $("#UserMERX").val($("#UsReg").val());

            var data = principal.jsonForm($('#frm_manifiesto').serializeArray());
            $.ajax({
                url: '/api/TX_MANIFIESTOS_TB/' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Manifiesto",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                         $("#in_mma_manifiesto").attr('disabled', true);
                        $("#dtp_mma_fechaManifiesto").attr("disabled", true);
                        $("#dtp_mma_fechaManIngreso").attr("disabled", true);
                    }else{

                        swal({
                        title: "Manifiesto",
                        text: "Se actualizó correctamente el manifiesto",
                        icon: "success",
                        button: "OK!",

                        })
                        $("#in_mma_manifiesto").attr('disabled', true);
                        $("#dtp_mma_fechaManifiesto").attr("disabled", true);
                        $("#dtp_mma_fechaManIngreso").attr("disabled", true);
                    }  
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                    $("#in_mma_manifiesto").attr('disabled', true);
                    $("#dtp_mma_fechaManifiesto").attr("disabled", true);
                    $("#dtp_mma_fechaManIngreso").attr("disabled", true);
                }
            })
        }
        else {
            swal({
                title: "Manifiesto",
                text: "El ID del manifiesto no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    },

    // Método que cierra el manifiesto
    Cerrar: function () {
        var id = $('#in_mma_manifiesto').val();
        if (id.length > 0) {

            $.ajax({
                url: '/api/TX_MANIFIESTOS_TB/' + id,
                type: 'DELETE',
                contentType: "application/json",
                success: function (data) {
                    if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Manifiesto",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                    }else{

                        swal({
                        title: "Manifiesto",
                        text: "Manifiesto " + id + " se ha cerrado! ",
                        icon: "success",
                        button: "OK!",

                        }).then((value) => {
                            manifiesto.limpia();
                        });

                    }
                    
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                }
            })
        }
        else {
            swal({
                title: "Manifiesto",
                text: "El ID del manifiesto no puede ser vacío",
                icon: "error",
                button: "OK!",

            })
        }
    },

    //Accion del boton de la lista de manifiesto que me carga la informacion en la pagina principal
    Editar: function (id) {
        principal.hidemenu();
        manifiesto.limpia();
        manifiesto.buscarManifiesto(id);
        $("#th_manifiesto").hide();
        $('#div_filters').hide();
        $('#detalles-manifiesto').show();
        manifiesto.cambiarEstadoBotones(false, true, false, false, false, true, false, false);   
    },

    //Funcion Ajax para la busqueda de datos de MANIFIESTO
    buscarManifiesto: function (p_manifiesto) {
        //if (p_manifiesto != null) {
            //var postdata = { tma_manifiesto_p: p_manifiesto };
           var p_fch_manifiesto = ""; //$("#dtp_mma_fechaManifiesto").val();
           var p_aerolinea = null;//$("#s_mma_transportista").val();

            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                //url: '/api/TX_MANIFIESTOS_TB/Manifiesto',
                url: '/api/TX_MANIFIESTOS_TB/Manifiesto?tma_manifiesto_p='+p_manifiesto+ '&tma_fch_manifiesto_p='+p_fch_manifiesto+ '&tma_aer_naerolinea_p='+p_aerolinea,
                //data: postdata,
                success: function (data) {
                    if (data.length == 1) {
                        //Valido que el estado del manifiesto no sea cerrado
                        if (data[0].MTA_ESTADO != 'C') {
                            manifiesto.MuestraManifiesto(data[0]);                         
                            manifiesto.cambiarEstadoBotones(false, true, false, false, false, true, false, false,false);
                            $("#in_mma_manifiesto").attr('disabled', true);
                             $("#dtp_mma_fechaManifiesto").data("kendoDatePicker").enable(false);
                             $("#dtp_mma_fechaManIngreso").data("kendoDatePicker").enable(false);
                              $("#s_mma_estado").attr('disabled', false);
                        }
                        else {
                            manifiesto.limpia();
                            swal({
                                title: "Manifiesto",
                                text: "Estado de manifiesto " + data[0].MTA_MANIFIESTO + " es cerrado! ",
                                icon: "warning",
                                button: "OK!",

                            });
                        }

                    }else{

                         //Obtenemos la plantilla
                        var templateText = $("#tb-manifiesto-template").html();
                        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                        var tableTemplate = Handlebars.compile(templateText);
                        $("#tb_manifiestos").html('');
                        //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                            $.each(data, function (index, elem) {
                                $("#tb_manifiestos").append(tableTemplate(elem));
                            })
                        //});
                    }
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    swal({
                        title: "Manifiesto",
                        text: "Error Buscando el manifiesto " + data.MMA_MANIFIESTO,
                        icon: "error",
                        button: "OK!",

                    });
                    console.log("error");
                }
            });
        //}
    },


    FiltrosManifiesto: function () {
        $('#div_filters').show();
    },

    OcultarFiltrosManifiesto: function () {
        $('#div_filters').hide();
    },

    //Ejecuta los filtros que se hayan seleccionado en la pantalla de Manifiesto
    BusquedaFiltros: function () {
        //manifiesto.Refrescar();
        principal.filterTable_Input('in_flr_manifiesto', 'tb_manifiesto', 0);
        principal.filterTable_Input('dtp_flr_fechaManifiesto', 'tb_manifiesto', 5);
        principal.filterTable_select('s_flr_transportista', 'tb_manifiesto', 2);
    },

    //Obtiene datos de puertos transporte para llenar select
    getPuertos: function (filtro, cb) {
        principal.get(
            "/api/TX_MANIFIESTOS_TB/Puertos",
            { 'filtro': filtro },
            cb
        )
    },

        getPuertosTotal: function () {
        principal.getData(
            "/api/TX_MANIFIESTOS_TB/PuertosTotal",
            function (data) {
                $("#s_mma_puertos").html(principal.arrayToOptions(data));
            }
        )
    },
                              
                          
                                                
    //Obtiene datos de transportista para llenar select
    getTransportistas: function () {
        principal.getData(
            "/api/TX_MANIFIESTOS_TB/Transportista",
            function (data) {
                $("#s_mma_transportista").html(principal.arrayToOptions(data));
            }
        )
    },

    //Obtiene datos de Estados transporte para llenar select
    getEstados: function () {
        principal.getData(
            "/api/TX_MANIFIESTOS_TB/Estados",
            function (data) {
                $("#s_mma_estado").html(principal.arrayToOptionsSelected(data, 'A'));
            }
        )
    },

    //Obtiene datos de modalidad transporte para llenar select
    getModalidadTransporte: function () {
        principal.getData(
            "/api/TX_MANIFIESTOS_TB/ModalidadTransporte",
            function (data) {
                $("#s_mma_modTransporte").html(principal.arrayToOptions(data));
            }
        )
    },

    //Obtiene datos de aduana para llenar select
    getAduanas: function () {
        principal.getData(
            "/api/TX_MANIFIESTOS_TB/Aduanas",
            function (data) {
                $("#s_mma_aduanaRecibe").html(principal.arrayToOptions(data));
                $("#s_mma_aduanaEnvia").html(principal.arrayToOptions(data));
            }
        )
    },

    //limpia campos del formulario de manifiesto
    limpia: function () {
        if ($('#MANIFIESTO_ID').val() != "") {
            $('#MANIFIESTO_ID').val($('#in_mma_manifiesto').val());
            principal.resetAllFields('detalles_manifiesto');
            principal.deactivateLabels();
            $('#in_mma_manifiesto').val($('#MANIFIESTO_ID').val());
            $('#MANIFIESTO_ID').val("");
            // $('#s_mma_aduanaRecibe').val("5");
            // $('#s_mma_aduanaEnvia').val("5");
            $('#s_mma_estado').val("A");
            $("#s_mma_estado").attr('disabled', true);

        } else {
            principal.resetAllFields('detalles_manifiesto');
            principal.deactivateLabels();
            // $('#s_mma_aduanaRecibe').val("5");
            // $('#s_mma_aduanaEnvia').val("5");
            $('#s_mma_estado').val("A");
            $("#s_mma_estado").attr('disabled', true);
        }
        $("#in_mma_manifiesto").attr('disabled', false);
        $("#lbl_mma_manifiesto").addClass('active');
        $('#in_mma_manifiesto').focus();
        
        manifiesto.cambiarEstadoBotones(false, false, true, true, false, true, true, false, true);
        var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd/MM/yyyy');
        $("#dtp_mma_fechaManifiesto").val(todayDate);
        $("#dtp_mma_fechaManIngreso").val(todayDate);
        $("#dtp_mma_fechaManifiesto").data("kendoDatePicker").enable(true);
        $("#dtp_mma_fechaManIngreso").data("kendoDatePicker").enable(true);
    },

    //Limpiar campos filtros
    limpiarCamposFiltrosManifiesto: function () {
        $('#in_flr_manifiesto').val("");
        $('#dtp_flr_fechaManifiesto').val("");
        $('#s_flr_transportista').val("0");
    },
      
    //Busca lo que escriben en el filtro para mostrar en el select  
    listenerFiltersManifiestos: function (id_txtbox, id_cmbox) {
        $('#' + id_txtbox).keyup(function () {
            var filter = $(this).val();
            $('select#' + id_cmbox + '>option').each(function () {
                var text = $(this).text().toLowerCase();
                if (text.indexOf(filter) !== -1) {
                    $(this).show(); $(this).prop('selected', true);
                }
                else {
                    $(this).hide();
                }
            });
        });
    },

    //Busca lo que escriben en el filtro puerto para mostrar en el select
    listenerFilterPuertos: function (id_txtbox, id_cmbox) {
        $('#' + id_txtbox).keyup(function () {
            var filter = $(this).val();
            manifiesto.getPuertos(filter,
            function (data) {
                $("#s_mma_puertos").empty()
                $("#s_mma_puertos").html(principal.arrayToOptionsSelected(data, 0));
            }
                );
        });
    },

    //Asignacion de la informacion devuelta en los campos de la pantalla manifiestos
    MuestraManifiesto: function (p_manifiesto) {
        for (var campo in p_manifiesto) {
            if(campo == "MTA_VIAJE_CONTINGENCIA"){
                if(p_manifiesto[campo] == "Y"){
                    p_manifiesto[campo] = true;
                } else{
                    p_manifiesto[campo] = false;
                }
            }
            if(p_manifiesto[campo] != null){
                principal.setValueByName(campo, p_manifiesto[campo],'frm_manifiesto');
            }
        }

        //Agrego class Active a todos los labels
        principal.activeLabels();
    },

    //Pasa a la pantalla de contenedor  
    pasarContenedor: function () {
        var manifiesto = $('#in_mma_manifiesto').val();
        window.location.href = "/Contenedor/Index/" + manifiesto;
    },

    //Pasa a la pantalla de guías    
    pasarGuia: function () {
        var manifiesto = $('#in_mma_manifiesto').val();
        window.location.href = "/Guias/Index/" + manifiesto;
    }
};

$(document).ready(function () {
    manifiesto.init();
})


