var cons_desconsolidacion = {
	init: function(){
		cons_desconsolidacion.getDesconsolidaciones();
	},
	getDesconsolidaciones: function(){
		var entrada = '';
		var guia = $('#consDesconsolid_guia').val(); 
    	var url = '/api/TX_DESCONSOLIDACIONES_TB/Desconsolidaciones?entrada=' + entrada + '&guia=' + guia;
    	var cb = function(data){
    		var templateText = $("#consDesconsolid_table-template").html();
              //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
            var tableTemplate = Handlebars.compile(templateText);
            $("#consDesconsolid_tablebody").html('');
              //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
            $.each(data, function (index, elem) {
                $("#consDesconsolid_tablebody").append(tableTemplate(elem));
            });
    	};
    	principal.ajax('GET',url,cb,"Desconsolidaciones");
	},
	filtrar: function(input){
        if(input.value.length > 5){
            cons_desconsolidacion.getDesconsolidaciones();
        }
    },
    limpiar: function(){
    	$('#consDesconsolid_guia').val('');
    	cons_desconsolidacion.getDesconsolidaciones();
    },
    imprimir: function(guia){
        var url = '/api/TX_DESCONSOLIDACIONES_TB/Imprimir?entrada=' + '' + '&guia=' + guia;
        var cb = function(data){
            //window.open(data.DSC_ARCHIVO, '_blank'); 
            cons_desconsolidacion.Actualizar(data.DSC_ID);
        };
        principal.ajax('GET',url,cb,"Desconsolidaciones");
    },
    Actualizar: function (id) {
        if (id.length > 0) {
        	var data = {
        		'DSC_ID': id,
        		'USUARIO': $('#USUARIO').val()
        	};
            $.ajax({
                url: '/api/TX_DESCONSOLIDACIONES_TB/Actualizar?id=' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                   if(data != null){
	                   swal({
	                        title: "Consulta Desconsolidaciones",
	                        text: "Se actualizó correctamente la Consulta Desconsolidaciones",
	                        icon: "success",
	                        button: "OK!",
	                    });
                       cons_desconsolidacion.getDesconsolidaciones();
	               }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                    else{
	                	swal({
	                      title: "Consulta Desconsolidaciones",
	                      text: data.responseJSON.Message,
	                      icon: "error",
	                      button: "OK!",
	                    });
	                }
                }
            });
        }
        else {
            swal({
                title: "Consulta Desconsolidaciones",
                text: "El ID del producto no puede ser vacío",
                icon: "error",
                button: "OK!",

            });
        }
    },
};
$(document).ready(function () {
    cons_desconsolidacion.init();
});