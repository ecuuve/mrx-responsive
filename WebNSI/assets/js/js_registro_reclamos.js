﻿var registro_reclamos = {
     //go : false,
    init: function () {
        registro_reclamos.LimpiarCamposRegistroReclamos();
        registro_reclamos.limpiarFiltros();
        registro_reclamos.CargaFechas();
        registro_reclamos.CargarControlesPrevios();
        registro_reclamos.Eventos();
    },

    onfocus: function () {
        if ($("#lbl_rec_reclamo").hasClass('lblfocus'))
            $("#lbl_rec_reclamo").removeClass('lblfocus');
    },

    LimpiarCamposRegistroReclamos: function () {
        principal.resetAllFields('detInfo_RegistroReclamos');
        //principal.resetAllFields('container-fluid');
        TecladoFuncionalidadPersonalizada();
        $("#btn_actualizar_reclamo").attr('style', 'display:none');
        $("#btn_registrar_reclamo").attr('style', 'display:block');
        registro_reclamos.limpiarFiltros();

    },

    CargarControlesPrevios: function () {
        registro_reclamos.getSistema();
        registro_reclamos.getMotivo();
        registro_reclamos.getMoneda();
        registro_reclamos.getResolucion();
        registro_reclamos.getResolucionOps();
        registro_reclamos.getVia();
    },

    Eventos: function () {
        $("#REC_SISTEMA").on("change", function (e) {
            //registro_reclamos.getConsecutivoData();
        });

        $("#consecutivo").focusout( function(e) {
            if($('#btn_actualizar_reclamo').css('display') == 'none'){
                 registro_reclamos.getConsecutivoData();
            }         
        });
    },

    getConsecutivoData: function () {
        var rec_sistema = $('#REC_SISTEMA').find(":selected").val();
        var consecutivo = $("#consecutivo").val();

        if (rec_sistema.length > 0 && consecutivo.length > 0) {
            this.existeConsecutivo(rec_sistema, consecutivo, function (go) {
                if (go) {
                    $.ajax({
                        "url": "/api/TX_REGISTRO_RECLAMOS_TB/Consecutivo",
                        method: 'POST',
                        dataType: 'json',
                        data: { rec_sistema: rec_sistema, consecutivo: consecutivo },
                        success: function (data) {
                            if (jQuery.type(data) == "object") {
                                principal.activeLabels();
                                registro_reclamos.MuestraDatosRegistroReclamos(data);
                            } else {
                                swal({
                                    title: "Registro de Reclamos",
                                    text: data,
                                    icon: "error",
                                    button: "OK!",
                                });
                            }
                        }
                    });
                }
             })
        }
    },


    existeConsecutivo: function (rec_sistema, consecutivo, callback) {
       var go = false;
        $.ajax({
            "url": "/api/TX_REGISTRO_RECLAMOS_TB/ExisteConsecutivo",
            method: 'POST',
            dataType: 'json',
            data: { rec_sistema: rec_sistema, consecutivo: consecutivo },
            success: function (data) {
                if (data == "N") {
                    go = true;
                } else if (data == "S") {
                    // swal({
                    //   title: "Registro Reclamos",
                    //   text: "El consecutivo ya existe desea continuar?",
                    //   icon: "warning",
                    //   buttons: true,
                    //   dangerMode: true,
                    // })
                    // .then((willDelete) => {
                    //   if (willDelete) {
                    //     go = true;                    
                    //   }else{
                    //     $("#consecutivo").focus();
                    //   } 
                    // });     
                    if (confirm("Ya existe reclamo para el consecutivo: #"+consecutivo+", desea continuar?")) {
                        go = true;
                    } else {
                        $("#consecutivo").focus();
                    }
                } else {
                    swal({
                        title: "Registro de Reclamos",
                        text: data,
                        icon: "error",
                        button: "OK!",
                    });
                }
                callback(go);
            },
            error: function () {
                callback(go);
            }
        });
    },

    getDatos: function (filtro, cb, api) {
        principal.get(
            api,
            { 'filtro': filtro },
            cb
        )
    },

    //Controles iniciales
    getSistema: function () {
        principal.getData(
            "/api/TX_REGISTRO_RECLAMOS_TB/Sistema",
            function (data) {
                $("#s_rec_sistema").html(principal.arrayToOptionsSelectedWithDefault(data, "MX"));
                $("#REC_SISTEMA").html(principal.arrayToOptionsSelectedWithDefault(data, "MX"));
            }
        )
    },

    getMotivo: function () {
        principal.getData(
            "/api/TX_REGISTRO_RECLAMOS_TB/Motivo",
            function (data) {
                $("#REC_MOTIVO").html(principal.arrayToOptionsSelectedWithDefault(data, "", ""));
            }
        )
    },

    getMoneda: function () {
        principal.getData(
            "/api/TX_REGISTRO_RECLAMOS_TB/Moneda",
            function (data) {
                $("#REC_TIPO_MONEDA").html(principal.arrayToOptionsSelectedWithDefault(data, "", ""));
            }
        )
    },

    getResolucion: function () {
        principal.getData(
            "/api/TX_REGISTRO_RECLAMOS_TB/Resolucion",
            function (data) {
                $("#REC_RESOLUCION").html(principal.arrayToOptionsSelectedWithDefault(data, "A"));
            }
        )
    },

    getResolucionOps: function () {
        principal.getData(
            "/api/TX_REGISTRO_RECLAMOS_TB/ResolucionOps",
            function (data) {
                $("#REC_RESOLUCION_OPS").html(principal.arrayToOptionsSelectedWithDefault(data, "", ""));
            }
        )
    },

    getVia: function () {
        principal.getData(
            "/api/TX_REGISTRO_RECLAMOS_TB/Via",
            function (data) {
                $("#REC_VIA_NOTIF").html(principal.arrayToOptionsSelectedWithDefault(data, "", ""));
            }
        )
    },

    CargaFechas: function () {
        
    registro_reclamos.KendoDate($("#s_rec_fecha_presentacion"),'');
    registro_reclamos.KendoDate($("#REC_FECHA_PRESENTACION"), new Date());
    registro_reclamos.KendoDate($("#REC_FCH_RECIBE_OPS"), new Date());
    registro_reclamos.KendoDate($("#REC_FCH_JUSTIF_OPS"), new Date());
    registro_reclamos.KendoDate($("#REC_FCH_RESOLUC_OPS"), new Date());
    registro_reclamos.KendoDate($("#REC_FCH_NOTIF"), new Date());
    registro_reclamos.KendoDate($("#REC_FCH_ENTREGA_CHEQUE"), new Date());
    registro_reclamos.KendoDate($("#REC_FCH_ENTREGA_CLIENTE"), new Date());

    $("#s_rec_fecha_presentacion").val("");

        
    },

    KendoDate: function (control, value) {
        control.kendoDatePicker({
            format: "dd/MM/yyyy",
            value: value,
            dateInput: true,
            // min: new Date(minDate),
            // max: new Date(),
            month: {
                empty: '<span class="k-state-disabled">#=data.value#</span>'
            }
        });
    },

// Procesos de consulta e inserción
    BuscarRegistroReclamos: function () {
        var s_rec_reclamo = $("#s_rec_reclamo").val();
        var s_rec_fecha_presentacion = $("#s_rec_fecha_presentacion").val();
        var s_rec_sistema = $("#s_rec_sistema").val();
        var s_consecutivo = $("#s_consecutivo").val();

        registro_reclamos.MostrarRegistroReclamos(s_rec_reclamo, s_rec_fecha_presentacion, s_rec_sistema, s_consecutivo);
    },

    MostrarRegistroReclamos: function (s_rec_reclamo, s_rec_fecha_presentacion, s_rec_sistema, s_consecutivo) {
        $.ajax({
            method: 'POST',
            dataType: 'JSON',
            //contentType: 'application/json',
            url: '/api/TX_REGISTRO_RECLAMOS_TB/BuscarRegistroReclamos', //?s_rec_reclamo='+ s_rec_reclamo + '&s_rec_fecha_presentacion='+ s_rec_fecha_presentacion+'&s_rec_sistema='+ s_rec_sistema + '&s_consecutivo='+ s_consecutivo,
            data: { s_rec_reclamo: s_rec_reclamo, s_rec_fecha_presentacion: s_rec_fecha_presentacion, s_rec_sistema: s_rec_sistema, s_consecutivo: s_consecutivo },
            success: function (data) {
                if (data !== null) {
                    console.log(data);
                    if (principal.isArray(data) && data.length > 1) {
                        registro_reclamos.MostrarListaRegistroReclamos(data);
                    } else {
                        if (typeof data === 'string' || data instanceof String) {
                            swal({
                                title: "Registro de Reclamos",
                                text: data,
                                icon: "error",
                                button: "OK!",
                            })
                        } else {

                            if (principal.isArray(data) && data.length > 0){
                                data = data[0];
                            //if (data["REC_RECLAMO"] != 0) 
                                //trae los datos de la queja
                                principal.activeLabels();
                                registro_reclamos.MuestraDatosRegistroReclamos(data);
                                //registro_reclamos.MuestraDatos //tx_qry_reclamos_sc_pr
                                $("#btn_actualizar_reclamo").attr('style', 'display:block');
                                $("#btn_registrar_reclamo").attr('style', 'display:none');
                            } else {
                                swal({
                                    title: "Registro de Reclamos",
                                    text: "No existen datos para los valores consultados.",
                                    icon: "error",
                                    button: "OK!",
                                })

                            }
                        }

                    }
                }
            },
            failure: function (data) {

                console.log("fail");
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(textStatus);
                //console.log("error consulta guía");
            }
        });
    },

    MostrarListaRegistroReclamos: function (data) {
        var templateText = $("#registro_reclamos_table-template").html();
        var tableTemplate = Handlebars.compile(templateText);
        $("#registro_reclamos_tablebody").html('');
        $.each(data, function (index, elem) {
            $("#registro_reclamos_tablebody").append(tableTemplate(elem));
        })
        $("#myModalRegistroReclamos").modal('show');
    },

    MostrarRegistroReclamosSeleccionado: function (s_rec_reclamo) {
        registro_reclamos.MostrarRegistroReclamos(s_rec_reclamo, "", "", "");
        $("#myModalRegistroReclamos").modal('hide');
    },

    GuardarReclamo: function () {
         $("#USER_MERXReg").val($("#UsReg").val());
        console.log($('#frm_registro_reclamos,#frm_investigacion,#frm_seguimiento').serialize());
        $.ajax({
            url: "/api/TX_REGISTRO_RECLAMOS_TB/Guardar",
            method: "POST",
            data: $('#frm_registro_reclamos,#frm_investigacion,#frm_seguimiento').serialize(),
            dataType: "JSON",
            success: function (data) {
                if (typeof data === 'string' || data instanceof String) {
                    swal({
                        title: "Registro de Reclamos",
                        text: data,
                        icon: "error",
                        button: "OK!"
                    })
                } else {
                    swal({
                        title: "Registro de Reclamos",
                        text: "Registro guardado exitosamente.",
                        icon: "success",
                        button: "OK!"
                    }).then((value) => {
                        registro_reclamos.LimpiarCamposRegistroReclamos();
                    });
                }
            },
            fail: function (data) {
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                }
            }
        });
    },

    ActualizarReclamo: function () {
     $("#USER_MERXReg").val($("#UsReg").val());
     $('[name= "REC_APLICA_POLIZA"]').is(":checked") == true ? $('[name= "REC_APLICA_POLIZA"]').val('A') : $('[name= "REC_APLICA_POLIZA"]').val('D');
     $('[name= "REC_DECISION_INS"]').is(":checked") == true ? $('[name= "REC_DECISION_INS"]').val('A') : $('[name= "REC_DECISION_INS"]').val('D');
        $.ajax({
            url: "/api/TX_REGISTRO_RECLAMOS_TB/Actualizar",
            method: "POST",
            data: $('#frm_registro_reclamos,#frm_investigacion,#frm_seguimiento').serialize(),
            dataType: "JSON",
            success: function (data) {
                if (typeof data === 'string' || data instanceof String) {
                    swal({
                        title: "Registro de Reclamos",
                        text: data,
                        icon: "error",
                        button: "OK!",
                    })
                } else {
                    swal({
                        title: "Registro de Reclamos",
                        text: "Registro actualizado exitosamente. Id de registro: #" + $("#REC_RECLAMO_REG").val(),
                        icon: "success",
                        button: "OK!",
                    }).then((value) => {
                        registro_reclamos.LimpiarCamposRegistroReclamos();
                    });
                }
            },
            fail: function (data) {
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                }
            }
        });
    },

    MuestraDatosRegistroReclamos: function (data) {
        for (var campo in data) {           
            
            if(data[campo] != null){
                principal.setValueByName(campo, data[campo],'frm_registro_reclamos');
                principal.setValueByName(campo, data[campo], 'frm_investigacion');
                principal.setValueByName(campo, data[campo], 'frm_seguimiento');
            }  
        }
        $("#REC_RECLAMO_REG").val(data["REC_RECLAMO"]);
        $("#REC_RECLAMO_OPS").val(data["REC_RECLAMO"]);
        $("#REC_RECLAMO_SEG").val(data["REC_RECLAMO"]);

        $("#REC_APLICA_POLIZA").prop("checked", data["REC_APLICA_POLIZA"] == "S");
        $("#REC_DECISION_INS").prop("checked", data["REC_DECISION_INS"] == "A");
    },
    
    limpiarFiltros: function () {
        $("#s_rec_reclamo").val("");
        $("#s_rec_sistema").val("");
        $("#s_consecutivo").val("");
        $("#s_rec_fecha_presentacion").val("");
        principal.resetAllFields('detInfo_registro_reclamos');
        principal.resetAllFields('detInfo_investigacion');
        principal.resetAllFields('detInfo_seguimiento');
        principal.ActivarTab('registro_reclamos');
        $("#s_rec_reclamo").focus();
        $("#btn_actualizar_reclamo").attr('style', 'display:none');
        $("#btn_registrar_reclamo").attr('style', 'display:block');
        registro_reclamos.CargaFechas();
        principal.deactivateLabels();
         $('#lbl_rec_reclamo').addClass('active');
    }

};

$(document).ready(function () {
    registro_reclamos.init();
})