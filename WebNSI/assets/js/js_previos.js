var previos = {

    init: function () {
        TecladoFuncionalidadPersonalizada();
        previos.Prev_ConsultarTiquete();
        previos.CargarControlesPrevios();
        //previos.listenerFilter('prev_ien_cod_importador', 'prev_ien_importador', 'api/TX_IMPORTADORES_TB/Importador?id=');
        //TecladoFuncionalidadPersonalizada();
        previos.BuscarImportadorEnter();
        previos.BuscarAgenciaEnter();
        previos.BuscarAgenteEnter();
        previos.Prev_CargaFechas();
        $('#myModal2').on('hidden.bs.modal', function (e) {
            $(this)
                .find("input,textarea,select")
                .val('')
                .end()
        });
        $('#myModal').on('hidden.bs.modal', function (e) {
            $(this)
                .find("input,textarea,select")
                .val('')
                .end()
        })

        $('#prev_ien_tipocliente').change(function () {
            var tiposelec  = $('#prev_ien_tipocliente').val();
            if( tiposelec == 2 ){
               previos.cambiarEstadoCampos(true, true, true, false, false, false, false);
                $("#prev_ien_cod_agencia").focus();
                $('#prev_ien_cod_importador').val("");
                $('#prev_ien_importador').val("");
            }
            if( tiposelec == 4 ){
               previos.cambiarEstadoCampos(false, false, false, true, true, true, true);
                $("#prev_ien_cod_importador").focus();
                $('#prev_ien_cod_agencia').val("");
                $('#prev_ien_cod_agente').val("");
                previos.getAgencia();
            }  
        });

        // $('#prev_ien_cod_asistente').change(function () {
        //     var tipo  = $('#prev_ien_cod_asistente').val();
        //     var agen = $("#prev_ien_cod_agencia").val();
        //     if( tipo > 0 && agen > 0 ){

        //         var p_agencia = $("#prev_ien_cod_agencia").val();
        //             principal.getData(
        //                 "api/TX_IMPORTADORES_TB/Agencia?id=" + p_agencia,
        //                 function (data) {
        //                     $("#prev_ien_agencia").html(principal.arrayToOptionsSelected(data, 0));
        //                 }
        //             )
                   
        //            previos.getAgente();
        //     }
           
        // });

        $('#prev_ien_agencia').change(function () {
           if( $("#prev_ien_cod_asistente").val() > 0 ){
                 previos.getAgente();
            }else{
                swal({
                  title: "Previos",
                  text: "Debe seleccionar el tipo Agente o Asistente",
                  icon: "error",
                  button: "OK!",
                }).then((value) => {
                     previos.getAgencia();
                });
            }  
        });
    },


    onfocus: function () {
        if ($("#lbl_guia_original").hasClass('lblfocus'))
            $("#lbl_guia_original").removeClass('lblfocus');
    },
    
    cambiarEstadoCampos: function (codImp, Imp, btnImp, codAgencia, Agencia, codAgente, Agente) {
        $('#prev_ien_cod_importador').prop('disabled', codImp);
        $('#prev_ien_importador').prop('disabled', Imp);
        $('#btn_filtrar_importador').prop('disabled', btnImp);
        $('#prev_ien_cod_agencia').prop('disabled', codAgencia);
        $('#prev_ien_agencia').prop('disabled', Agencia);
        $('#prev_ien_cod_agente').prop('disabled', codAgente);
        $('#prev_ien_agente').prop('disabled', Agente);
    },

    CargarControlesPrevios: function () {
        previos.getTipoPersona();
        //previos.getImportadores();
        previos.getTipoRevision();
        previos.getTipoCliente();
        previos.getAgencia();
        previos.getAutoridades();
        //previos.getAgente();
    },

    listenerFilter: function (id_txtbox, id_cmbox, api) {

        $('#' + id_txtbox).keyup(function () {
            var filter = $(this).val();
            if (filter !== undefined) {
                previos.getDatos(
                    filter,
                    function (data) {
                        $("#" + id_cmbox).empty();
                        $("#" + id_cmbox).html(principal.arrayToOptionsSelected(data, 0));
                    },
                    api+filter
                );
            }
        });
    },

    getDatos: function (filtro, cb, api) {
        principal.get(
            api,
            { 'filtro': filtro },
            cb
        )
    },

    getImportadores: function () {
        principal.getData(
            "/api/TX_IMPORTADORES_TB/Importadores",
            function (data) {
                $("#prev_ien_importador").html(principal.arrayToOptionsSelectedWithDefault(data,0));
            }
        )
    },

    getTipoRevision: function () {
        principal.getData(
            "/api/TX_IMPORTADORES_TB/TipoRevision",
            function (data) {
                $("#prev_ien_tiporevision").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        )
    },

    getAutoridades: function () {
        principal.getData(
            "/api/TX_IMPORTADORES_TB/Autoridades",
            function (data) {
                $("#prev_autoridades").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        )
    },

    getTipoCliente: function () {
        principal.getData(
            "/api/TX_IMPORTADORES_TB/TipoCliente",
            function (data) {
                $("#prev_ien_tipocliente").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        )
    },

    getTipoPersona: function () {
        principal.getData(
            "/api/TX_IMPORTADORES_TB/TipoPersona",
            function (data) {
                $("#prev_ien_cod_asistente").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        )
    },

    BuscarAgenciaEnter: function () {       
        $('#prev_ien_cod_agencia').change(function () {
            if( $("#prev_ien_cod_asistente").val() > 0 ){
                    var p_agencia = $("#prev_ien_cod_agencia").val();
                    principal.getData(
                        "/api/TX_IMPORTADORES_TB/Agencia?id=" + p_agencia,
                        function (data) {
                            $("#prev_ien_agencia").html(principal.arrayToOptionsSelected(data, 0));
                            previos.getAgente();
                        }
                    )
                   
                   
                }else{
                    swal({
                      title: "Previos",
                      text: "Debe seleccionar el tipo Agente o Asistente",
                      icon: "error",
                      button: "OK!",
                    }).then((value) => {
                        $('#prev_ien_cod_agencia').val("");
                        previos.getAgencia();
                    });
                }  
        });
    },

    getAgencia: function () {
        var p_agencia = $("#prev_ien_cod_agencia").val();
        principal.getData(
            "/api/TX_IMPORTADORES_TB/Agencia?id=" + p_agencia,
            function (data) {
                $("#prev_ien_agencia").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        )
    },

    getAgente: function () {
        var p_agente = $("#prev_ien_cod_agente").val();
        var p_tipo = $("#prev_ien_cod_asistente").val();
        var p_agencia = $("#prev_ien_agencia").val();
        principal.getData(
            "/api/TX_IMPORTADORES_TB/Agente?id=" + p_agente+'&agencia='+p_agencia+'&tipo='+p_tipo,
            function (data) {
                $("#prev_ien_agente").html(principal.arrayToOptionsSelectedWithDefault(data, 0));
            }
        )
    },

    BuscarAgenteEnter: function () {       
        $('#prev_ien_cod_agente').change(function () {
            var p_agente = $("#prev_ien_cod_agente").val();
            var p_tipo = $("prev_ien_cod_asistente").val();
            var p_agencia = $("#prev_ien_agencia").val();
            principal.getData(
                "/api/TX_IMPORTADORES_TB/Agente?id=" + p_agente+'&agencia='+p_agencia+'&tipo='+p_tipo,
                function (data) {
                    $("#prev_ien_agente").html(principal.arrayToOptionsSelected(data, 0));
                }
            )
            
        });
    },
    
     //Listener del cambio en el textbox de importador que haria la llamada al metodo ajax para buscar el importador
    BuscarImportadorEnter: function () {       
        $('#prev_ien_cod_importador').change(function () {
            $('#prev_ien_importador').val("");
            var p_codimportador = $('#prev_ien_cod_importador').val();
            var p_tipo = 'U';
            $.getJSON('/api/TX_IMPORTADORES_TB/Importador?id=' + p_codimportador+'&tipo='+p_tipo).then(function (data) {
                if (typeof data === 'string' || data instanceof String){
                        swal({
                          title: "Importadores",
                          text: data,
                          icon: "error",
                          button: "OK!",
                        })
                }else{


                    if(data.length > 0){
                       $.each(data, function (index, elem) {
                        $('#prev_ien_cod_importador').val(elem.Id);
                        $('#prev_ien_importador').val(elem.Nombre);
                        previos.ActiveCamposPrevios();
                        }) 
                    }else{

                        swal({
                              title: "Importadores",
                              text: "No existen datos para los valores consultados.",
                              icon: "error",
                              button: "OK!",
                            })
                    }
                }
            });
            
        });
    },

    BuscarImportador: function () {
       
        var p_codimportador = "";
        var p_nomimportador = "";
        //var p_popup = 0;
        $('.modal-body #prev_flt_cod_importador,.modal-body textarea').each(function () {
            p_codimportador = $(this).val();
        });
        $(".modal-body #prev_flt_nom_importador,.modal-body textarea").each(function () {
            p_nomimportador = $(this).val();
        });

        //if (p_codimportador !== "" || p_nomimportador !== "") {
        //    p_popup = 1;
        //}

        //Obtenemos la plantilla
        var templateText = $("#Prev_Importador_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $(".modal-body #Prev_Importador_table1body,.modal-body textarea").each(function () {
            $(this).html('');
        });
        //$("#Prev_Importador_table1body").html('');
        if (p_codimportador !== "" || p_nomimportador !== "") {
            //var p_tipo = "F";
            $.getJSON('/api/TX_IMPORTADORES_TB/Importador?id=' + p_codimportador+'&tipo='+p_nomimportador).then(function (data) {
                $.each(data, function (index, elem) {
                    $("#Prev_Importador_table1body").append(tableTemplate(elem));
                })
            });
        }
        else {
            $.getJSON('/api/TX_IMPORTADORES_TB/Importadores?id=' +"" +'&nombre='+"").then(function (data) {
                $.each(data, function (index, elem) {
                    $("#Prev_Importador_table1body").append(tableTemplate(elem));
                })
                $("#divProcessing").hide();
            });
        }
    },

    Prev_BuscarPrevio: function () {
        var p_guiaOr = "";
        var p_previo = "";
        var p_movimiento = "";
        var hiddden = "";
        var p_popup = 0;
        $('.modal-body #prev_flt_movimiento,.modal-body textarea').each(function () {
            p_movimiento = $(this).val();
        });
        $('.modal-body #prev_flt_entrada,.modal-body textarea').each(function () {
            p_previo = $(this).val();
        });
        $(".modal-body #prev_flt_guia_original,.modal-body textarea").each(function () {
            p_guiaOr = $(this).val();
        });

        $(".modal-body #prev_popup,.modal-body textarea").each(function () {
            hiddden = $(this).val();
        });

        if (hiddden === 'S') {
            p_popup = 1;
        }

        if (p_previo === "" && p_guiaOr === "" && p_movimiento === "") {
            p_previo = $("#in_flr_ien_entrada").val();
            p_guiaOr = $("#in_flr_guia_original").val();
            p_movimiento = $("#in_flr_movimiento").val();
        }
    
        previos.Prev_ConsultarDatos(p_previo, p_guiaOr, p_popup,p_movimiento);
    },

    Prev_ConsultarDatos: function (p_entrada, p_guiaOr,p_popup,p_movimiento) {
        var postdata = { id: p_entrada, guia: p_guiaOr, movimiento: p_movimiento };
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MERCANCIAS_TB_Previos/EntradaPrevio?id=' + p_entrada + '&guia=' + p_guiaOr + '&movimiento'+ p_movimiento,
            data: postdata,
            success: function (data) {
                if (data.length!==0) {
                    if (p_popup === 1) {
                        $(".modal-body #Prev_Entrada_table1body,.modal-body textarea").each(function () {
                            $(this).html('');
                        });

                        previos.Prev_Listar(data);

                    } else if (data.length > 1) {
                        $(".modal-body #Prev_Entrada_table1body,.modal-body textarea").each(function () {
                            $(this).html('');
                        });
                        $(".modal-body #prev_popup,.modal-body textarea").each(function () {
                            $(this).val('S');
                        });
                        previos.Prev_Listar(data);
                    }
                    else {
                        //trae los datos de la entrada
                        previos.ActiveCamposPrevios();
                        previos.Prev_MuestraDatos(data);
                        previos.Prev_Tarimas_Listar();
                    }
                }
                else {
                    swal({
                        title: "Previos",
                        text: "Registro de entrada no encontrado",
                        icon: "info",
                        button: "OK!",
                        timer: 5000,
                    })
                }
            },
            failure: function (data) {

                swal({
                    title: "Previos",
                    text: "Error al consultar:" + data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                    timer: 5000,
                })
            },
            error: function (data) {

                swal({
                    title: "Previos",
                    text: "Error al consultar:"  + data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                    timer: 5000,
                })
            }
        });
    },

    Prev_ConsultarTiquete: function () {
        var p_usuario = $('#UsPrev').val();
        var postdata = { id: p_usuario};
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MERCANCIAS_TB_Previos/TiqueteSala?id=' + p_usuario ,
            data: postdata,
            success: function (data) {
                if (data !== null) {
                        previos.Prev_MuestraDatosTiquete(data);
                    }
            },
            failure: function (data) {

                swal({
                    title: "Tiquete",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                    timer: 5000,
                })
            },
            error: function (data) {

                swal({
                    title: "Tiquete",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                    timer: 5000,
                })
            }
        });
    },

    Prev_Listar: function (data) {

        var p_entrada =$("#in_flr_ien_entrada").val();
        var p_guiaOr = $("#in_flr_guia_original").val();
        var p_movimiento = $("#in_flr_movimiento").val();

        //Obtenemos la plantilla

        var templateText = $("#Prev_Entrada_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#Prev_Entrada_table1body").html('');

        if (data !== null) {
            $.each(data, function (index, elem) {
                $("#Prev_Entrada_table1body").append(tableTemplate(elem));
            })
        }
        else {
            $.getJSON('/api/TX_MERCANCIAS_TB_Previos/EntradaPrevio?id=' + p_entrada + '&guia=' + p_guiaOr+ '&movimiento=' + p_movimiento).then(function (data) {
                $.each(data, function (index, elem) {
                    $("#Prev_Entrada_table1body").append(tableTemplate(elem));
                })
            });
        }

        $("#myModal2").modal('show');
    },

    Prev_CargaFechas: function () {
        var today = new Date();
        var minDate = today.setDate(today.getDate() - 1);
        // $("#dtp_ien_fch_impreso").kendoDatePicker({
        //     format: "MM/dd/yyyy",
        //     value: new Date(),
        //     min: new Date(minDate),
        //     max: new Date(),
        //     month: {
        //         empty: '<span class="k-state-disabled">#=data.value#</span>'
        //     }
        // });

        // $("#dtp_ien_fch_actual").kendoDatePicker({
        //     format: "MM/dd/yyyy",
        //     value: new Date(),
        //     min: new Date(minDate),
        //     max: new Date(),
        //     month: {
        //         empty: '<span class="k-state-disabled">#=data.value#</span>'
        //     }
        // });
    },

    Prev_Ent_Escoger: function (prm_entrada) {
        var p_entrada = prm_entrada;//$("#in_flr_ien_entrada").val();
        var p_guiaOr = "";//$("#in_flr_guia_original").val();

        previos.Prev_ConsultarDatos(p_entrada, p_guiaOr);
        previos.Prev_Tarimas_Listar();

        $("#myModal2").modal('hide');
    },

    Prev_MuestraDatosTiquete: function (p_entradatiquete) {
        for (var campo in p_entradatiquete[0]) {
            if (campo != null) {
                principal.setValueByName(campo, p_entradatiquete[0][campo], 'frm_guiafiltroaforo');
            }
        }
    },

    Prev_MuestraDatos: function (p_entrada) {
        for (var campo in p_entrada[0]) {
            if (campo != null) {
                principal.setValueByName(campo, p_entrada[0][campo], 'frm_guiaaforo');
            }
        }
    },

    Prev_MuestraDatosRevision: function (p_codimportador) {
        $("#myModal").modal('hide');
        var p_tipo = 'U';
        $.getJSON('/api/TX_IMPORTADORES_TB/Importador?id=' + p_codimportador+'&tipo='+p_tipo).then(function (data) {
            $.each(data, function (index, elem) {
                $('#prev_ien_cod_importador').val(elem.Id);
                $('#prev_ien_importador').val(elem.Nombre);
                previos.ActiveCamposPrevios();
            })
        });      
    },

    Prev_Tarimas_Listar: function () {
        var entrada = $("#prev_ien_entrada").val();
        //Obtenemos la plantilla
        var templateText = $("#Prev_table-template").html();
        //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
        var tableTemplate = Handlebars.compile(templateText);
        $("#Prev_table1body").html('');
        $.getJSON('/api/TX_MERCANCIAS_TB_Previos/EntradaDetallePrevio?id=' + entrada).then(function (data) {
            $.each(data, function (index, elem) {
                $("#Prev_table1body").append(tableTemplate(elem));
            })
            previos.checkearTarimas();
        });     
    },

    LimpiarCamposPrevios: function () {
        principal.resetAllFields('detInfo_previos');
        principal.resetAllFields('detInfo_revision');
        principal.resetAllFields('detInfo_tiquete_previos');
        $("#Prev_table1body").html('');
        TecladoFuncionalidadPersonalizada();
        $("#in_flr_guia_original").focus();
        previos.Prev_ConsultarTiquete();
        previos.DeactiveCamposPrevios();
        $('#lbl_guia_original').addClass('active');
    },

    LimpiarCamposPreviosRev: function () {
        principal.resetAllFields('detInfo_revision');
        TecladoFuncionalidadPersonalizada();
        previos.cambiarEstadoCampos(false, false, false, false, false, false, false);
        previos.getAgencia();       
    },

    limpiarFiltrosPrevios: function () {
        $("#in_flr_ien_entrada").val("");
        $("#in_flr_guia_original").val("");
        $("#in_flr_movimiento").val("");
        previos.LimpiarCamposPrevios();
    },

    limpiarFiltrosPreviosLista: function () {
        $('.modal-body input,.modal-body textarea').each(function () {
            $(this).val('');
        });
        $('.modal-body label,.modal-body textarea').each(function () {
            $(this).removeClass('active');
        });
        previos.Prev_BuscarPrevio();
    },

    PrevMostrarListaImportador: function () {
        $("#myModal").modal('show');
        $("#divProcessing").show();
        previos.BuscarImportador();     
    },

    limpiarFiltrosPreviosImportador: function () {
        $('.modal-body input,.modal-body textarea').each(function () {
            $(this).val('');
        });
        $('.modal-body label,.modal-body textarea').each(function () {
            $(this).removeClass('active');
        });
        previos.BuscarImportador();
    },

    ActiveCamposPrevios: function () {
        $('label').addClass('active');
    },

    DeactiveCamposPrevios: function () {
        $('label').removeClass('active');
    },

    imprimir: function(){
        var cb = function(){
            var fecha_tiquete =  $('#dtp_ien_fch_impreso').val();
            var fecha_actual = $('#dtp_ien_fch_actual').val();
            var parts = fecha_tiquete.split("T");
            var hora = parts[1];
            var parts1 = fecha_actual.split("T");
            var hora_actual = parts1[1];
            var data = {
                'QUE_IEN_ENTRADA':            $('#prev_ien_entrada').val(),
                'QUE_AGA_NAGENCIA_ADUANAL':   $('#prev_ien_agencia :selected').val(),
                'QUE_IMPORTADOR':             $('#prev_ien_cod_importador').val(),
                'QUE_CONSIGNATARIO':          $('#prev_ien_nombre_importador').val(),
                'QUE_TIPO_EXAMEN':            $('#prev_ien_tiporevision :selected').val(),
                'QUE_TIPO_CLIENTE':           $('#prev_ien_tipocliente :selected').val(),
                'QUE_TIPO_PERSONA':           $('#prev_ien_cod_asistente :selected').val(),
                'QUE_AUTORIZADO':             'S',
                'QUE_COD_AGENTE':             $('#prev_ien_agente :selected').val(),
                'QUE_USU_USUARIO_GENERO':     $("#UsPrev").val(),
                'QUE_TIQUETE':                $('#prev_ien_tiquete').val(),
                'QUE_HORA_TIQUETE':           $('#dtp_ien_fch_impreso').val(),
                'QUE_HORA_INICIO_SC':         $('#dtp_ien_fch_actual').val(),
                'QUE_TIPO_TIQUETE':           $('#prev_ien_tipocola').val(),
                'AUTORIDAD':                  $('#prev_ien_autoridad').val()
            };
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_MERCANCIAS_TB_Previos/GuardarCola' ,
                data: JSON.stringify(data),
                success: function (data) {
                    if (data !== null) {
                        var previo = parseInt(data);
                        window.open("http://merxalmacenadora/WebEtiquetaNCI/Reporte.aspx?Previo=" + previo);
                        //window.open("http://webherediatest/WebEtiquetaNCI/Reporte.aspx?Previo=" + previo);
                        }
                        previos.limpiarFiltrosPrevios();
                        $('#revision-tab').removeClass('active');
                        $('#guias-tab').addClass('active');

                        $('#guias').addClass('show');
                        $('#revision').removeClass('active');
                        $('#guias').addClass('active');
                            
                },
                failure: function (data) {
                    swal({
                        title: "Error",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!"
                    });
                },
                error: function (data) {
                    if(data.responseJSON.Message){
                        swal({
                            title: "Error",
                            text: data.responseJSON.Message,
                            icon:  "error",
                            button: "OK!"
                        });
                    }
                }
            });
        };
        previos.ActualizarTarimas(cb);
    },

    ActualizarTarimas: function(callback){
        var tarimas = [];
        $('#Prev_table1 tbody tr').each(function(){
            var accion = $(this).find('td span input[type="checkbox"]');
            if(accion.prop('checked')){
                var tarima = $(this).find('td:first-child');
                tarima = tarima.text();
                tarimas.push(tarima);
            }
        });
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_MERCANCIAS_TB_Previos/ActualizaTarimas' ,
            data: JSON.stringify(tarimas),
            success: function (data) {
                if (data !== null) {
                     console.log("Actualizó las tarimas.");
                    // swal({
                    //     title: "Éxito",
                    //     icon: "success",
                    //     text: "Tarimas actualizadas correctamente"
                    // });
                }
                if(typeof(callback) == 'function'){
                    callback();
                }
            },
            failure: function (data) {
                swal({
                    title: "Error",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",
                    timer: 5000,
                });
            },
            error: function (data) {
                if(data.responseJSON.Message){
                    swal({
                        title: "Error",
                        text: data.responseJSON.Message,
                        icon:  "error",
                        button: "OK!",
                        timer: 5000,
                    });
                }
            }
        });
    },

    checkearTarimas: function(){
        $('#Prev_table1 tbody tr').each(function(){
            var accion = $(this).find('td span input[type="checkbox"]');
            if(!accion.prop('checked')){
                var checkbox = accion.attr('id');
                $('#' + checkbox).prop('checked', true);
            }
        });
        $('#chk_escoger').prop('checked', true);
    }
};

$(document).ready(function () {
    previos.init();
})