var prod_nc = {
	init: function(){
        TecladoFuncionalidadPersonalizada();
        prod_nc.limpiaFiltros();
		//Inicializa la pantalla como se desea
		prod_nc.cargaInicial();
		//Si se coloca un numero de consecutivo el va y busca la guia, el cliente y demas informacion relacionada que pueda cargar en el formulario
		$('#pnc_consecutivo').blur(function () {
			prod_nc.validacionConsecutivo($('#pnc_consecutivo').val(), $('#PNC_SISTEMA').val());
		});
		//Inicializa el componente de fecha
		$("#pnc_fecha").kendoDatePicker({
          format: "dd/MM/yyyy",
          value: new Date(),
          dateInput: true,
          month: {
            empty: '<span class="k-state-disabled">#=data.value#</span>'
          }
        });
        // $('input[type="number"]').bind("cut copy paste",function(e) {
        //      e.preventDefault();
        //  });

	},

    //Método para focus de los inputs iniciales
    onfocus: function () {
        if ($("#lbl_pnc_identificador").hasClass('lblfocus'))
            $("#lbl_pnc_identificador").removeClass('lblfocus');
    },


	//Inicializa la pantalla con todos los combobox cargados y el manejo de botones idoneo a ese momento
	cargaInicial: function(){
		prod_nc.getUbicaciones();
		prod_nc.getSistemas();
		prod_nc.getCategorias();
		prod_nc.getDepartamentos();
		prod_nc.getTipoProblema();
		prod_nc.getDetectadoPor();
		prod_nc.getEventoHallazgo();
		prod_nc.getDecisionCliente();
        prod_nc.getSistemasListado();
		if($('#encabezado-tab').hasClass('active'))
			prod_nc.getEstado('E');
		else
			prod_nc.getEstado('I');
		$('#btn_regresar_producto').hide();
	},
	//Valida que el consecutivo exista y que no posea una pnc ya asociada
	validacionConsecutivo: function(consecutivo, sistema){
        if(consecutivo != ""){
            if(sistema == 0)
                sistema = "NA";
            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_PROD_NC_TB/ValidacionConsecutivo?consecutivo='+consecutivo+ '&sistema='+sistema,
                //data: postdata,
                success: function (data) {
                   if(data != null){
                    $('#pnc_guia').val(data.GuiaOriginal);
                    $('#pnc_cliente').val(data.Importador);
                   }
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    if(data.responseJSON.Message == "true"){
                         swal({
                              title: "El consecutivo ya tiene asociado un PNC. ¿Desea Continuar?",
                              // text: "Once deleted, you will not be able to recover this imaginary file!",
                              icon: "warning",
                              buttons: true,
                              dangerMode: true,
                            })
                            .then((willDelete) => {
                              if (willDelete) {
                               prod_nc.infoConsecutivo(consecutivo, sistema);
                              } else {
                                principal.resetAllFields('frm_productoNC');
                              }
                            });
                    }else{
                        if(data.responseJSON.Message == "false"){
                            prod_nc.infoConsecutivo(consecutivo, sistema);
                        }else{
                            swal({
                                title: "Consecutivo",
                                text: data.responseJSON.Message,
                                icon: "error",
                                button: "OK!",

                            });
                        }
                    }
                }
            });
        }
	},
	//Extra informacion ya suministrada a base de datos mediante el consecutivo y lo coloca en pantalla
	infoConsecutivo: function(consecutivo, sistema){
		$.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/api/TX_PROD_NC_TB/InfoConsecutivo?consecutivo='+consecutivo+ '&sistema='+sistema,
                //data: postdata,
                success: function (data) {
                   if(data != null){
	                   	$('#pnc_guia').val(data.GuiaOriginal);
	                   	$('#pnc_cliente').val(data.Importador);
                   }else{
	                   	swal({
		                    title: "Consecutivo",
		                    text: "Error al traer información del consecutivo",
		                    icon: "error",
		                    button: "OK!",

		                });
                   }
                },
                failure: function (data) {
                    console.log("fail");
                },
                error: function (data) {
                    swal({
                        title: "Consecutivo",
                        text: data.responseJSON.Message,
                        icon: "error",
                        button: "OK!",
                    });
                }
            });
	},
	//Lista las Ubicaciones
	getUbicaciones: function () {
        principal.getData("/api/TX_PROD_NC_TB/GetUbicacion",function (data) {$("#pnc_ubicacion").html(principal.arrayToOptionsSelected(data,'A'));});
    },
    // Lista los sistemas
    getSistemas: function () {
        principal.getData("/api/TX_PROD_NC_TB/GetSistema",function (data) {$("#PNC_SISTEMA").html(principal.arrayToOptionsSelected(data,'M'));});
    },
    //Lista las categorias
    getCategorias: function () {
        principal.getData("/api/TX_PROD_NC_TB/GetCategorias",function (data) {$("#pnc_categoria").html(principal.arrayToOptions(data));});
    },
    //Lista los departamentos
    getDepartamentos: function () {
        principal.getData("/api/TX_PROD_NC_TB/GetDepartamentos",function (data) {$("#pnc_departamento").html(principal.arrayToOptions(data));});
    },
    //Lista los tipos de problemas
    getTipoProblema: function () {
        principal.getData("/api/TX_PROD_NC_TB/GetTipoProblema",
        	function (data) {$("#pnc_tipo_problema").html(principal.arrayToOptions(data));});
    },
    //Lista el combobox de detectado por
    getDetectadoPor: function () {
        principal.getData("/api/TX_PROD_NC_TB/GetDetectadoPor",function (data) {$("#pnc_detectado").html(principal.arrayToOptions(data));});
    },
    //Lista el combo box de forma como se encontro el pnc
    getEventoHallazgo: function () {
        principal.getData("/api/TX_PROD_NC_TB/GetEventoHallazgo",function (data) {$("#pnc_hallazgo").html(principal.arrayToOptions(data));});
    },
    //Lista el combo perteneciente a la decision del cliente
    getDecisionCliente: function () {
        principal.getData("/api/TX_PROD_NC_TB/GetDecisionCliente",function (data) {$("#pnc_decision_cliente").html(principal.arrayToOptions(data));});
    },
    //Lista los estados
    getEstado: function (pantalla) {
        principal.getData("/api/TX_PROD_NC_TB/GetEstado?pantalla=" + pantalla,function (data) {$("#frm_productoNC #PNC_ESTADO").html(principal.arrayToOptions(data)); $('#PNC_ESTADO').val('C');});
    },
     // Lista los sistemas
    getSistemasListado: function () {
        principal.getData("/api/TX_PROD_NC_TB/GetSistema",function (data) {$("#pnc_flr_sistema").html(principal.arrayToOptionsSelected(data,'M'));});
    },
    //Guarda un pnc nuevo en la base de datos
    guardar: function(){
    	$.post(
            "/api/TX_PROD_NC_TB/GuardarProductoNC/",
             $('#frm_productoNC').serialize(),
            function (data) {
                if(data != null){
                    $('#pnc_identificador').val(data);
                    swal({
	                    title: "Producto No Conforme",
	                    text: "Producto No Conforme guardado exitosamente",
	                    icon: "success",
	                    button: "OK!",
	                    }).then((value) => {
                            prod_nc.mostrarPestañas();
	         //               	principal.resetAllFields('frm_productoNC');
	         //                var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd/MM/yyyy');
	    					// $("#pnc_fecha").val(todayDate);

	                    });
                }
            })
            .fail(function (data) {
                if (data.responseJSON.ModelState) {
                    principal.processErrorsPopUp(data.responseJSON.ModelState);
                }else{
                	swal({
                      title: "Producto No Conforme",
                      text: data.responseJSON.Message,
                      icon: "error",
                      button: "OK!",
                    });
                }
            });
    },
    //Actualiza un pnc especifico seleccionado desde el listado
    Actualizar: function () {
        var id = $('#pnc_identificador').val();
        if (id.length > 0) {
            var data = principal.jsonForm($('#frm_productoNC').serializeArray());
            $.ajax({
                url: '/api/TX_PROD_NC_TB/ActualizaEncabezado?id=' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: function (data) {
                   if(data != null){
	                   swal({
	                        title: "Producto No Conforme",
	                        text: "Se actualizó correctamente el Producto No Conforme",
	                        icon: "success",
	                        button: "OK!",
	                    });
	                   principal.resetAllFields('frm_productoNC');
	               }
                },
                error: function (data) {
                    if (data.responseJSON.ModelState) {
                        principal.processErrorsPopUp(data.responseJSON.ModelState);
                    }
                    else{
	                	swal({
	                      title: "Producto No Conforme",
	                      text: data.responseJSON.Message,
	                      icon: "error",
	                      button: "OK!",
	                    });
	                }
                }
            });
        }
        else {
            swal({
                title: "Producto No Conforme",
                text: "El ID del producto no puede ser vacío",
                icon: "error",
                button: "OK!",

            });
        }
    },
    //Manejo de botones entre el listar y el formulario
    Refrescar: function () {
        if($('#tbl_productoNC').hasClass('fullHidden'))
            $('#tbl_productoNC').removeClass('fullHidden')
        if($('#div_productoNC_filtros').hasClass('fullHidden'))
            $('#div_productoNC_filtros').removeClass('fullHidden')
        $('#frm_productoNC').hide();
        $('#tbl_productoNC').show();
        $('#div_productoNC_filtros').show();
        $('#btn_listar_productos').hide();
        $('#btn_guardar_producto').hide();
        $('#btn_editar_producto').hide();
        $('#btn_regresar_producto').show();
        prod_nc.buscar();
    },
    //Ejecuta la busqueda de un pnc mediante filtros especificos
    buscar: function () {
       var identificador = $('#pnc_flr_identificador').val();
       var sistema = $('#pnc_flr_sistema').val();
       var consecutivo = $('#pnc_flr_consecutivo').val();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_PROD_NC_TB/Productos?identificador='+identificador+ '&sistema='+sistema+ '&consecutivo='+consecutivo,
            success: function (data) {
                //Obtenemos la plantilla
                var templateText = $("#productoNC_table-template").html();
                //Se compila, y la plantilla ya compilado puede usarse para actualizar la pantalla
                var tableTemplate = Handlebars.compile(templateText);
                $("#productoNC_tablebody").html('');
                //$.getJSON('/api/TX_MANIFIESTOS_TB').then(function (data) {
                    $.each(data, function (index, elem) {
                        $("#productoNC_tablebody").append(tableTemplate(elem));
                    });
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Productos",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
                console.log("error");
            }
        });
    },
    //limpia campos tanto de filtrado como del formulario
    limpiaFiltros: function(){
    	$('#pnc_flr_identificador').val('');
        $('#pnc_flr_sistema').val('');
        $('#pnc_flr_consecutivo').val('');
        prod_nc.buscar();
        principal.resetAllFields('frm_productoNC');
        if(!$('#li_detallepnc').hasClass('fullHidden'))
    		$('#li_detallepnc').addClass('fullHidden');
    	if(!$('#li_investigacionpnc').addClass('fullHidden'))
    		$('#li_investigacionpnc').removeClass('fullHidden');
    	if(!$('#li_verificacionPNC').hasClass('fullHidden'))
    		$('#li_verificacionPNC').addClass('fullHidden');
        if(!$('#btn_editar_producto').hasClass('fullHidden'))
            $('#btn_editar_producto').addClass('fullHidden');
        if($('#btn_guardar_producto').hasClass('fullHidden'))
            $('#btn_guardar_producto').removeClass('fullHidden');

         principal.deactiveLabelsInput();
        $('#pnc_identificador').focus();
        
    },
    //manejo de botones de cambio de listado a formulario
    regresar: function(){
    	$('#pnc_flr_identificador').val('');
        $('#pnc_flr_sistema').val('');
        $('#pnc_flr_consecutivo').val('');
    	$('#btn_regresar_producto').hide();
    	$('#btn_listar_productos').show();
    	$('#btn_guardar_producto').show();
    	$('#btn_editar_producto').show();
    	$('#frm_productoNC').show();
        $('#tbl_productoNC').hide();
        $('#div_productoNC_filtros').hide();
    },
    //Funcionalidad de editar un pnc especifico
    editar: function(id){
    	prod_nc.buscarProducto(id);
    	$('#btn_regresar_producto').hide();
    	$('#btn_listar_productos').show();
    	$('#btn_guardar_producto').show();
    	$('#btn_editar_producto').show();
        $("#tbl_productoNC").hide();
        $('#div_productoNC_filtros').hide();
        $('#frm_productoNC').show();
        if($('#btn_editar_producto').hasClass('fullHidden'))
            $('#btn_editar_producto').removeClass('fullHidden');
        if(!$('#btn_guardar_producto').hasClass('fullHidden'))
            $('#btn_guardar_producto').addClass('fullHidden');
        principal.resetAllFields('div_productoNC_filtros');
        prod_nc.mostrarPestañas();
    },
    // una vez dado un identificador se trae el pnc correspondiente y se pinta toda la informacion correspondiente en los campos del formulario
    buscarProducto: function (p_identificador) {
    	principal.resetAllFields('frm_productoNC');
    	var sistema = '';
    	var consecutivo = '';
		$.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: '/api/TX_PROD_NC_TB/Productos?identificador='+p_identificador+ '&sistema='+sistema+ '&consecutivo='+consecutivo,
            success: function (data) {
            	$.each(data[0], function(i,v){
            		principal.setValueByName(i, v,'frm_productoNC');
            	}); 
                 principal.activeLabelsInput();
            },
            failure: function (data) {
                console.log("fail");
            },
            error: function (data) {
                swal({
                    title: "Productos",
                    text: data.responseJSON.Message,
                    icon: "error",
                    button: "OK!",

                });
            }
        });
    },
    //Funcionalidad de mostrar pestañas una vez exista un identificador en la pestaña de encabezado
    mostrarPestañas:function(){
    	if($('#li_detallepnc').hasClass('fullHidden'))
    		$('#li_detallepnc').removeClass('fullHidden');
    	if($('#li_investigacionpnc').hasClass('fullHidden'))
    		$('#li_investigacionpnc').removeClass('fullHidden');
    	if($('#li_verificacionPNC').hasClass('fullHidden'))
    		$('#li_verificacionPNC').removeClass('fullHidden');
    },
    filtrar: function(input){
        if(input.value.length > 1){
            prod_nc.buscar();
        }else{
            if(input.value.length = 0)
            {
                prod_nc.buscar();
            }
        }
    }
};
$(document).ready(function () {
    prod_nc.init();
});